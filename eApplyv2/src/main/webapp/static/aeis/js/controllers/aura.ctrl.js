AeisApp.controller('aura',['$scope', '$rootScope', '$location', '$timeout','$window','auraTransferInitiateSvc', 'fetchAuraTransferData','auraRespSvc', 'auraPostSvc','auraInputSvc','deathCoverService','tpdCoverService','ipCoverService', 'PersistenceService', 'submitAuraSvc','ngDialog','persoanlDetailService','saveEapplyData','clientMatchSvc','submitEapplySvc','fetchUrlSvc', 'fetchPersoanlDetailSvc', 'appData', 'fetchAppNumberSvc', 'fetchIndustryList','$q',
                                     function($scope, $rootScope, $location, $timeout,$window,auraTransferInitiateSvc,fetchAuraTransferData,auraRespSvc,auraPostSvc,auraInputSvc,deathCoverService,tpdCoverService,ipCoverService, PersistenceService, submitAuraSvc, ngDialog,persoanlDetailService,saveEapplyData,clientMatchSvc,submitEapplySvc,fetchUrlSvc, fetchPersoanlDetailSvc, appData, fetchAppNumberSvc, fetchIndustryList, $q){
	$scope.urlList = fetchUrlSvc.getUrlList();
  $scope.datePattern='/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i';
  $scope.freeText = {};
  $scope.healthQuestionIndex = null;
  $scope.isAuraErroneous = false;
  $scope.erroneousSections = [];
  $scope.go = function (path) {
		if($scope.heightval && $scope.weightVal) {
			$scope.submitHeightWeightQuestion($scope.healthQuestionIndex).then(function() {
				$timeout(function(){
					$location.path(path);
				}, 10);
			}, function(err) {
				console.log(err);
			});
		} else {
			$timeout(function(){
				$location.path(path);
			}, 10);
		}
  };
  
	$scope.serachText = '';
  $scope.heightDropdownOpt = ['cm', 'Feet'];
  $scope.heightDropDown = $scope.heightDropdownOpt[0];
  $scope.heightOptions= $scope.heightOptions == undefined ? 'm.cm' : $scope.heightOptions;

  $scope.weightDropdownOpt = ['Kilograms', 'Pound', 'Stones'];
  $scope.weightDropDown = $scope.weightDropdownOpt[0];
  $scope.weighOptions=$scope.weighOptions == undefined ? 'kg' : $scope.weighOptions;
  $scope.weightMaxLen = 3;

	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
		ngDialog.openConfirm({
	            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
	            plain: true,
	            className: 'ngdialog-theme-plain custom-width'
	        }).then(function(){
	        	$location.path("/landing");
	        }, function(e){
	        	if(e=='oncancel'){
	        		return false;
	        	}
	        });
    };

 // added for session expiry
    /*$timeout(callAtTimeout, 900000);
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/

   /* var timer;
    angular.element($window).bind('mouseover', function(){
    	timer = $timeout(function(){
    		sessionStorage.clear();
    		localStorage.clear();
    		$location.path("/sessionTimeOut");
    	}, 900000);
    }).bind('mouseout', function(){
    	$timeout.cancel(timer);
    });*/

    $scope.quotePageDetails = {}; // will be replaced
    $scope.coverDetails = {}; // will be replaced
    $scope.deathAddnlDetails = {}; // will be replaced
    $scope.tpdAddnlDetails = {}; // will be replaced
    $scope.ipAddnlDetails = {}; // will be replaced
    $scope.changeCoverOccDetails = {}; // will be replaced

    angular.extend($scope.quotePageDetails, appData.getAppData());
    angular.extend($scope.coverDetails, $scope.quotePageDetails);
    angular.extend($scope.deathAddnlDetails, $scope.quotePageDetails.addnlDeathCoverDetails);
    angular.extend($scope.tpdAddnlDetails, $scope.quotePageDetails.addnlTpdCoverDetails);
    angular.extend($scope.ipAddnlDetails, $scope.quotePageDetails.addnlIpCoverDetails);
    angular.extend($scope.changeCoverOccDetails, $scope.quotePageDetails.occupationDetails);
    // Industry name
    $scope.quotePageDetails.industryName = fetchIndustryList.getIndustryName($scope.quotePageDetails.occupationDetails.industryCode);
    
	  // $scope.coverDetails = PersistenceService.getChangeCoverDetails();
    $scope.personalDetails = fetchPersoanlDetailSvc.getMemberDetails() || {};
    // $scope.changeCoverOccDetails = PersistenceService.getChangeCoverOccDetails();
    // $scope.deathAddnlDetails = PersistenceService.getDeathAddnlCoverDetails();
    // $scope.tpdAddnlDetails = PersistenceService.getTpdAddnlCoverDetails();
    // $scope.ipAddnlDetails = PersistenceService.getIpAddnlCoverDetails();
	//console.log($scope.personalDetails);

    //console.log(auraInputSvc);
    
    if($scope.quotePageDetails.occupationDetails.smoker == undefined){
  		if($scope.changeCoverOccDetails.smokerQuestion == "1"){
  			$scope.quotePageDetails.occupationDetails.smoker = "Yes";
	     }else{
	    	 $scope.quotePageDetails.occupationDetails.smoker = "No";
	     }
  	}
    
    auraInputSvc.setSmoker($scope.quotePageDetails.occupationDetails.smoker);
  	auraInputSvc.setFund('AEIS');
  	auraInputSvc.setMode('change');
    //setting deafult vaues for testing
  	auraInputSvc.setName($scope.personalDetails.personalDetails.firstName+" "+$scope.personalDetails.personalDetails.lastName);
  	//auraInputSvc.setAge(moment().diff(moment($scope.coverDetails.dob, 'DD-MM-YYYY'), 'years')) ;
  	auraInputSvc.setAge($scope.coverDetails.age);
  	auraInputSvc.setAppnumber($scope.coverDetails.appNum)
  	if(parseFloat($scope.deathAddnlDetails.deathFixedAmt) > parseFloat($scope.coverDetails.existingDeathAmt)){
  		auraInputSvc.setDeathAmt(parseFloat($scope.deathAddnlDetails.deathFixedAmt));
		}else{
			auraInputSvc.setDeathAmt(0);
		}

		if(parseFloat($scope.tpdAddnlDetails.tpdFixedAmt) > parseFloat($scope.coverDetails.existingTpdAmt)){
			auraInputSvc.setTpdAmt(parseFloat($scope.tpdAddnlDetails.tpdFixedAmt));
		}else{
			auraInputSvc.setTpdAmt(0);
		}
  	
  	if(parseFloat($scope.ipAddnlDetails.ipInputTextValue) > parseFloat($scope.coverDetails.existingIPAmount)){
  		auraInputSvc.setIpAmt(parseFloat($scope.ipAddnlDetails.ipFixedAmt));
  	}else if((parseFloat($scope.ipAddnlDetails.ipInputTextValue) <= parseFloat($scope.coverDetails.existingIPAmount)) && $scope.ipIncreaseFlag){
  		auraInputSvc.setIpAmt(parseFloat($scope.ipAddnlDetails.ipFixedAmt));
  	}else{
  		auraInputSvc.setIpAmt(0);
  	}  	
  
  	auraInputSvc.setWaitingPeriod($scope.ipAddnlDetails.waitingPeriod)
  	auraInputSvc.setBenefitPeriod($scope.ipAddnlDetails.benefitPeriod)
  	auraInputSvc.setMemberType("INDUSTRY OCCUPATION")
  	if($scope.changeCoverOccDetails && $scope.changeCoverOccDetails.gender ){
  		auraInputSvc.setGender($scope.changeCoverOccDetails.gender);
  	}else if($scope.personalDetails && $scope.personalDetails.gender){
  		auraInputSvc.setGender($scope.personalDetails.gender);
  	}

  	auraInputSvc.setClientname('metaus')
  	auraInputSvc.setIndustryOcc($scope.changeCoverOccDetails.industryCode+":"+$scope.changeCoverOccDetails.occupation)
  /*	if($scope.changeCoverOccDetails.citizenQue=='Yes'){
  		auraInputSvc.setCountry('Australia')
  	}else{
  		auraInputSvc.setCountry($scope.changeCoverOccDetails.citizenQue)
  	} */
  	//auraInputSvc.setCountry(null); // residency que removed from quote page, so hard-coded
  	auraInputSvc.setSalary($scope.changeCoverOccDetails.salary)
  	auraInputSvc.setFifteenHr($scope.changeCoverOccDetails.fifteenHr)
  	auraInputSvc.setLastName($scope.personalDetails.personalDetails.lastName)
  	auraInputSvc.setFirstName($scope.personalDetails.personalDetails.firstName)
  	auraInputSvc.setDob($scope.personalDetails.personalDetails.dateOfBirth)
  	var termFlag = false;
    $scope.personalDetails.existingCovers = $scope.personalDetails.existingCovers || {}; 
    $scope.personalDetails.existingCovers.cover = $scope.personalDetails.existingCovers.cover || [];
  	for(var k = 0; k < $scope.personalDetails.existingCovers.cover.length; k++){
  		if(($scope.personalDetails.existingCovers.cover[k].exclusions && $scope.personalDetails.existingCovers.cover[k].exclusions != '') ||
  				($scope.personalDetails.existingCovers.cover[k].loading && $scope.personalDetails.existingCovers.cover[k].loading != '' &&
  						parseFloat($scope.personalDetails.existingCovers.cover[k].loading) > 0)){
  			termFlag = true;
  			break;
  		}
  	}
  	if(termFlag){
  		auraInputSvc.setExistingTerm(true);
  	} else{
  		auraInputSvc.setExistingTerm(false);
  	}
  	///////////////
  	//below values has to be set from quote page, as of now taking from existing insurance
  /*
  	 auraInputSvc.setAge(50)
  	 auraInputSvc.setAppnumber(14782223482374216)
  	auraInputSvc.setDeathAmt(200000)
  	auraInputSvc.setTpdAmt(200000)
  	auraInputSvc.setIpAmt(2000)
  	auraInputSvc.setWaitingPeriod('30 Days')
  	auraInputSvc.setBenefitPeriod('2 Years')
  	auraInputSvc.setGender('Male')
  	auraInputSvc.setIndustryOcc('026:Transfer')
  	auraInputSvc.setCountry('Australia')
  	auraInputSvc.setSalary('100000')
  	auraInputSvc.setFifteenHr('Yes')   */

	 fetchAuraTransferData.requestObj($scope.urlList.auraTransferDataUrl).then(function(response) {
  		$scope.auraResponseDataList = response.data.sections;
  		console.log(response.data)
  		angular.forEach($scope.auraResponseDataList, function (Object,index) {
  			console.log($scope.auraResponseDataList)
  			$scope.questionAlias = Object.sectionName
  			$scope.questionlist = Object.questions
  			if($scope.questionAlias=='QG=Personal Details'){
  			$scope.healthQuestionIndex = index+1;
  				$scope.auraResponseDataList[index].sectionStatus=true;
  			}else{
  				var keepGoing = true;
  				angular.forEach($scope.questionlist, function (Object,index1) {
  					if(Object.questionId=='4'){
  						Object.branchComplete=false;
  					}
  					if(keepGoing && Object.branchComplete){
  						$scope.auraResponseDataList[index].sectionStatus=true;
  						keepGoing = false;
  					}
  				});
  			}
  		});
  		//console.log($scope.auraResponseDataList)
  		$scope.updateHeightWeightValue();

  	});

  	 $scope.inchValue  = function(value){
  		console.log(value)
  		$scope.heightin = value;
  	 }
  	 $scope.meterValue  = function(value,answer, questionObj){
  		if(!angular.isUndefined(value)){
  			$scope.heightinMeter = value;
  	  		$scope.heightval = value;
  	  		questionObj.error = false;
  		}else{
  			questionObj.error = true;
  		}


  	 }

	$scope.heighOptions  = function(value){
  		console.log(value)
  		if(value=='cm'){
  			$scope.heightOptions='m.cm'
  		}else{
  			$scope.heightval = '';
  			$scope.heightOptions='ft.in'
  		}
  	 }
  	$scope.weightValue = function(value,answer,questionObj){

  		if(!angular.isUndefined(value)){
  	  		$scope.weightVal = value;
  	  		questionObj.error = false;
  		}else if(!angular.isUndefined(value) && value==''){
  			questionObj.error = true;
  		}

  	}
  	$scope.weightOptions= function(value,answer){
  		console.log(value)
  		if(value=='Kilograms'){
  			$scope.weighOptions='kg';
  			$scope.weightDropDown = 'Kilograms';
  			$scope.weightMaxLen = 3;
  		}else if(value=='Pound'){
  			$scope.weighOptions='lb';
  			$scope.weightDropDown = 'Pound';
  			$scope.weightMaxLen = 3;
  		}else if(value=='Stones'){
  			$scope.weighOptions='st.lb';
  			$scope.weightDropDown = 'Stones';
  			$scope.weightMaxLen = 100;
  		}
  	}

  	$scope.lbsValue = function(value){
  		console.log(value)
  		$scope.lbsval = value;
  	}

  	 $scope.submitHeightWeightQuestion = function(sectionIndex){
  	    var defer = $q.defer();
  		angular.forEach($scope.auraResponseDataList, function (Object,index) {
  			angular.forEach(Object.questions, function (Object) {
  				var serviceCallRequired= false;
  				if(Object.classType=='HeightQuestion' && $scope.heightval){
  					questionObj= Object;
      				if($scope.heightOptions=='m.cm'){
      					questionObj.arrAns=[];
          	   			questionObj.arrAns[0]= $scope.heightOptions;
          	   			questionObj.arrAns[1]= '0';
          	   			questionObj.arrAns[2]= $scope.heightval;
      				}else if($scope.heightOptions=='ft.in'){
      					console.log($scope.heightOptions)
      					console.log($scope.heightinMeter)
      					console.log($scope.heightin)
      					questionObj.arrAns=[];
          	   			questionObj.arrAns[0]= $scope.heightOptions;
          	   			questionObj.arrAns[1]= $scope.heightval;
          	   			questionObj.arrAns[2]= $scope.heightin;
      				}

      	   			serviceCallRequired= true;
      			}else if(Object.classType=='WeightQuestion' && $scope.weightVal){
      				questionObj= Object;
      				console.log($scope.weightDropDown)
      				console.log($scope.weighOptions)
      				console.log($scope.weightVal)
      				if($scope.weightDropDown=='Stones'){
      					console.log($scope.weightVal)
      					console.log($scope.lbsval)
      					questionObj.arrAns=[];
          	   			questionObj.arrAns[0]= 'st.lb';
          	   			questionObj.arrAns[1]= $scope.weightVal;
          	   			questionObj.arrAns[2]= $scope.lbsval;
      				}else if($scope.weightDropDown=='Kilograms'){
      					questionObj.arrAns=[];
          	   			questionObj.arrAns[0]= 'kg';
          	   			questionObj.arrAns[1]= $scope.weightVal;
      				}else if($scope.weightDropDown=='Pound'){
      					questionObj.arrAns=[];
          	   			questionObj.arrAns[0]= 'lb';
          	   			questionObj.arrAns[1]= $scope.weightVal;
      				}

      	   			serviceCallRequired= true;
      			}
  				if(serviceCallRequired && questionObj.arrAns.length>1){
  					 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};
  					console.log($scope.auraRes)
          			 auraRespSvc.setResponse($scope.auraRes)
          			auraPostSvc.reqObj($scope.urlList.auraPostUrl).then(function(response) {
               			//questionObj.auraAnswer = response.data.auraAnswer
          				$scope.updateQuestionList(questionObj.questionId,response.data.changedQuestion,response.data)
          				$scope.updateHeightWeightValue();
          				var keepGoing = true;
                  		angular.forEach($scope.auraResponseDataList[index].questions, function (Object,index1) {
                  			console.log(Object)
                  			$scope.setprogressiveError($scope.auraResponseDataList[sectionIndex].questions[index1])
                  			if(Object.questionId=='4'){
        						Object.branchComplete=true;
        					}
                  			if(keepGoing && !Object.branchComplete){
                  				keepGoing = false;
                  			}

                  		});
                  		console.log(keepGoing)
                  		if(keepGoing){
                  			index = index+1;
                  			console.log(index)
                      		$scope.auraResponseDataList[index].sectionStatus=true;
                  		}
                  		defer.resolve({});
                   	}, function () {
                   		console.log('failed');
                   		defer.reject({});
                   	});
  				}else{
  					angular.forEach($scope.auraResponseDataList[sectionIndex].questions, function (Object,index1) {
  						$scope.setprogressiveError($scope.auraResponseDataList[sectionIndex].questions[index1])
  					});

  				}

      		});
  		});
  		return defer.promise;
   	 };
   	 //updating height and weight questions
   	 $scope.updateHeightWeightValue = function(){

   		angular.forEach($scope.auraResponseDataList, function (Object,index) {
  			$scope.questionAlias = Object.sectionName
  			$scope.questionlist = Object.questions
  			if($scope.questionAlias=='QG=Health Questions'){
  				$scope.healthQuestion = $scope.auraResponseDataList[index].questions;
  				 angular.forEach($scope.healthQuestion, function (question) {
 					if(question.classType=='HeightQuestion'){
 						if(question.answerTest.indexOf('Metres')!==-1){

 							$scope.heightval=parseInt(question.answerTest.split(",")[0].split("Metres")[0])*100 + parseInt(question.answerTest.split(",")[1].split("cm")[0]);
 							$scope.heightDropDown='cm'
 							$scope.heightOptions='m.cm'
 						}
 						if(question.answerTest.indexOf('Feet')!==-1){
 							$scope.heightDropDown='Feet'
 							$scope.heightval=question.answerTest.substring(0,question.answerTest.indexOf('Feet')-1)
 							$scope.inches= question.answerTest.substring(question.answerTest.indexOf(',')+2,question.answerTest.indexOf('in')-1);
 							$scope.heightOptions='ft.in'
 						}

 					}else if(question.classType=='WeightQuestion'){
 						if(question.answerTest.indexOf('Kilograms')!==-1){
 							console.log(question.answerTest)
 							$scope.weightDropDown='Kilograms';
 							$scope.weightVal=question.answerTest.substring(0,question.answerTest.indexOf('Kilograms')-1);
 							$scope.weighOptions='kg';
 						}
 						if(question.answerTest.indexOf('Pound')!==-1){
 							console.log(question.answerTest)
 							$scope.weightDropDown='Pound';
 							$scope.weightVal=question.answerTest.substring(0,question.answerTest.indexOf('Pound')-1);
 							$scope.weighOptions='lb';
 						}
 						if(question.answerTest.indexOf('Stones')!==-1){
 							console.log(question.answerTest)
 							$scope.weightDropDown='Stones';
 							$scope.weightVal=question.answerTest.substring(0,question.answerTest.indexOf('Stones')-1);
 							$scope.weighOptions='st.lb';
 							$scope.lbsval=question.answerTest.substring(question.answerTest.indexOf(',')+2,question.answerTest.indexOf('lbs')-1);
 						}
 					}
 				});
  			}
   		});


   	 }

  	$scope.collapseUncollapse = function (index,sectionName){
  		console.log($scope.auraResponseDataList)
  		if(sectionName=='QG=Health Questions'){
  			$scope.submitHeightWeightQuestion(index);
  		}else{
  			var keepGoing = true;
      		angular.forEach($scope.auraResponseDataList[index].questions, function (Object,index1) {
      			console.log(Object)
      			$scope.setprogressiveError($scope.auraResponseDataList[index].questions[index1])
      			if(keepGoing && !Object.branchComplete){
      				keepGoing = false;
      			}

      		});
      		console.log(keepGoing)
      		if(keepGoing){
      			index = index+1;
      			console.log(index)
      			if($scope.auraResponseDataList[index].sectionStatus){
      				index = index+1;
      			}
          		$scope.auraResponseDataList[index].sectionStatus=true;

      		}
  		}

  		//console.log($scope.auraResponseDataList)
  	}

  	$scope.stateChanged = function (qId) {
  		if($scope.answers[qId]){ //If it is checked
  			 console.log('id>>'+qId)
  	   }
  	}
  	$scope.updateFreeText = function (answerValue, questionObj){
  		console.log('answer>'+answerValue)
  		questionObj.arrAns = [];
  		questionObj.arrAns[0]=answerValue;
  		questionObj.arrAns[1]='accept';
 		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};
 		auraRespSvc.setResponse($scope.auraRes)
		  auraPostSvc.reqObj($scope.urlList.auraPostUrl).then(function(response) {
			  $scope.updateQuestionList(questionObj.questionId , response.data.changedQuestion, response.data)
		  });
  	}
  	$scope.updateQuestionList = function(questionId,changedQuestion, questionObj){
  		$scope.isAuraErroneous = false;
  		angular.forEach($scope.auraResponseDataList, function (Object,index) {
  			$scope.questionlist = Object.questions
  			angular.forEach($scope.questionlist, function (Obj,index1) {
          if(Obj && changedQuestion) {
    				if(Obj.questionId == changedQuestion.questionId){
    					$scope.auraResponseDataList[index].questions[index1]= changedQuestion
    					 $scope.progressive(questionId,changedQuestion,questionObj);
    					console.log($scope.auraResponseDataList[index])
    				}
          }
  			});
  		})
  	}

  	 $scope.updateRadio = function (answerValue, questionObj){
  		console.log('answer>'+answerValue)
  		if(questionObj.classType === 'DateQuestion') {
  			var pattern =/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;

  	  		if(!pattern.test(answerValue)) {
	  	  		questionObj.error = true;
	  			return false;
  	  		}
  		}/*else if(questionObj.classType==='RangeQuestion'){
  			var pattern =/^(0|[1-9][0-9]*)$/;
  			if(!pattern.test(answerValue)) {
	  	  		questionObj.error = true;
	  			return false;
  	  		}
  		}else if(questionObj.questionId===14){
  			var pattern =/^(0|[1-9][0-9]*)$/;
  			if(!pattern.test(answerValue)) {
	  	  		questionObj.error = true;
	  			return false;
  	  		}
  		}*/
  		else if(!answerValue && answerValue !=0) {
  			questionObj.error = true;
  			return false;
  		}

  		questionObj.answerTest = answerValue;

  		questionObj.arrAns[0]=answerValue;
  		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};
  		  auraRespSvc.setResponse($scope.auraRes)
  		  auraPostSvc.reqObj($scope.urlList.auraPostUrl).then(function(response) {
  		console.log(response.data.changedQuestion)
  		$scope.updateQuestionList(questionObj.questionId, response.data.changedQuestion, response.data)
  			//auraQuestionlist
      	}, function () {
      		console.log('failed');
      	});
  	 };

  	 $scope.progressive = function(questionid,changedQuestion,questionObj){
  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) {
  			if(changedQuestion.questionAlias== Object.sectionName){
  				$scope.questionIndex = $scope.auraResponseDataList[selectedIndex].questions.indexOf(changedQuestion)
  				angular.forEach($scope.auraResponseDataList[selectedIndex].questions, function (baseQuestionObj, index) {
  					if(index<=$scope.questionIndex){
  						if(index==$scope.questionIndex){
  							var keepGoing = true;
  	  						$scope.subprogressive($scope.auraResponseDataList[selectedIndex].questions[index],questionid,keepGoing,questionObj)
  						}else{
  							$scope.setprogressiveError($scope.auraResponseDataList[selectedIndex].questions[index])
  						}


  					}

  				});
  			}

  		});
   	 };

   	$scope.setprogressiveError =  function(baseQuestionObj){
   		if(baseQuestionObj && baseQuestionObj.questionComplete){
   			angular.forEach(baseQuestionObj.auraAnswer, function (auraAnswer, selectedIndex) {
   				angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions, function (subauraQustion, subSelectedIndex) {
   					if(subauraQustion.questionComplete){
   						$scope.setprogressiveError(subauraQustion);
   					}else{
   						baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex].error=true;
   						$scope.keepGoing = false;
   					}
   				});

   	   		});
   		}else if(baseQuestionObj && baseQuestionObj.classType=='HeightQuestion' && (!angular.isUndefined($scope.heightval) && $scope.heightval.length>0)){
   			baseQuestionObj.error = false;
   		}else if(baseQuestionObj && baseQuestionObj.classType=='WeightQuestion' && (!angular.isUndefined($scope.weightVal)  && $scope.weightVal.length>0)){
   			baseQuestionObj.error = false;
   		}else if(baseQuestionObj){
   			baseQuestionObj.error = true;
   		}

   	}
   	$scope.subprogressive =  function(baseQuestionObj,questionid,keepGoing, updatedQuestionObj){
   		if(baseQuestionObj && baseQuestionObj.questionId<questionid){
   			angular.forEach(baseQuestionObj.auraAnswer, function (auraAnswer, selectedIndex) {
   				angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions, function (subauraQustion, subSelectedIndex) {
   					if(!subauraQustion.questionComplete && subauraQustion.questionId<questionid){
   						baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex].error=true;
   					}else if(subauraQustion.questionComplete && subauraQustion.questionId<questionid){
   						$scope.subprogressive(subauraQustion,questionid,keepGoing,updatedQuestionObj)
   					}else if(subauraQustion.questionComplete &&  subauraQustion.questionId==questionid){
   						var arrayAnswer = null;
   			   			if(subauraQustion.answerTest.indexOf(',')!==-1){
   			   				arrayAnswer = subauraQustion.answerTest.split(',');
   			   			}else if(!angular.isUndefined(subauraQustion.answerTest)){
   			   				arrayAnswer=[];
   			   				arrayAnswer[0] = subauraQustion.answerTest;
   			   			}
   			   			angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex].auraAnswer, function (answer, answerIndex) {
   			   				console.log(arrayAnswer[arrayAnswer.length-1].indexOf(answer.answerValue))
   			   				if(arrayAnswer[arrayAnswer.length-1].indexOf(answer.answerValue)!==-1){
   			   					$scope.checkboxProgressive(baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex],answerIndex);
   			   				}
   			   			});
   					}
   				});
   	   		});
   		}else if(baseQuestionObj && baseQuestionObj.questionId==questionid && baseQuestionObj.answerTest==updatedQuestionObj.answerTest ){
   			var arrayAnswer = null;
   			if(baseQuestionObj.answerTest.indexOf(',')!==-1){
   				arrayAnswer = baseQuestionObj.answerTest.split(',');
   			}else if(!angular.isUndefined(baseQuestionObj.answerTest)){
   				arrayAnswer=[];
   				arrayAnswer[0] = baseQuestionObj.answerTest;
   			}
   			angular.forEach(baseQuestionObj.auraAnswer, function (answer, answerIndex) {
   				console.log(arrayAnswer[arrayAnswer.length-1].indexOf(answer.answerValue))
   				if(arrayAnswer[arrayAnswer.length-1].indexOf(answer.answerValue)!==-1){
   					$scope.checkboxProgressive(baseQuestionObj,answerIndex);
   				}
   			});

   				//answerID
   				//answerValue
   		}

   	}

   	$scope.checkboxProgressive = function(baseQuestionObj, answerIndex ){
      if(baseQuestionObj) {
     		angular.forEach(baseQuestionObj.auraAnswer, function (answer, index) {
     			if(index<answerIndex){
     				angular.forEach(baseQuestionObj.auraAnswer[index].childQuestions, function (subauraQustion, subSelectedIndex) {
     					$scope.setprogressiveError(subauraQustion)

     				});
     			}
     		});
      }
   	}




  	$scope.searchValSubmit = function (answerValue, questionObj,parentQuestionObj){
  		console.log(questionObj.answerValue.answerValue)
  		//console.log(questionObj)
  		console.log(parentQuestionObj)
  		questionObj.arrAns=[];
  		questionObj.arrAns[0] = questionObj.answerValue.answerValue;
  		 $scope.auraRes={"questionID":parentQuestionObj.questionId,"auraAnswers":questionObj.arrAns};
  		auraRespSvc.setResponse($scope.auraRes)
		  auraPostSvc.reqObj($scope.urlList.auraPostUrl).then(function(response) {
			  $scope.updateQuestionList(questionObj.questionId, response.data.changedQuestion,response.data)
		  });
  	}

  	 $scope.updateCheckBox = function (answerValue, questionObj,index){
  		 console.log(questionObj.answerTest);
  		 if(questionObj.arrAns.length==0){
  			var answerArray = questionObj.answerTest.split(',');
  			if(answerArray.length>1){
  				questionObj.arrAns = answerArray;
  			}else{
  				questionObj.arrAns[0] = questionObj.answerTest;
  			}
  		 }


  		 var addtoArray=true;
  		if(answerValue.indexOf('None of the above') !== -1){
  			questionObj.arrAns=[];
  			questionObj.arrAns[0]= answerValue;
  		}else{
  			for (var i=0;i<questionObj.arrAns.length;i++){
  				//to remove white space
  				questionObj.arrAns[i] = $.trim(questionObj.arrAns[i])
  				console.log(questionObj.arrAns[i])
  				 console.log(answerValue)
  				  if(questionObj.arrAns[i]==answerValue ){
                   	questionObj.arrAns.splice(i, 1);
                   	addtoArray=false
                  }
            }
            if(addtoArray==true){
           	 questionObj.arrAns.splice(index, 0, answerValue);
            }
  		}
         //removing none of the above, if there are other answers
  		if(questionObj.arrAns.length>1){
  			for (var i=0;i<questionObj.arrAns.length;i++){
      			if(questionObj.arrAns[i].indexOf('None of the above') !== -1){
      				questionObj.arrAns.splice(i, 1);
      			}

  			}
  		}
         $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};
         console.log(JSON.stringify(questionObj.arrAns))
         auraRespSvc.setResponse($scope.auraRes)
         auraPostSvc.reqObj($scope.urlList.auraPostUrl).then(function(response) {
    			console.log(response.data)
    			$scope.updateQuestionList(questionObj.questionId,response.data.changedQuestion,response.data)

       	}, function () {
       		console.log('failed');
           	});
      	 };

      	$scope.checkIncompleteQuestion = function (baseQuestionObj, keepGoing){
       		if(baseQuestionObj.questionComplete && keepGoing){
       			angular.forEach(baseQuestionObj.auraAnswer, function (auraAnswer, selectedIndex) {
       				angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions, function (subauraQustion, subSelectedIndex) {
       					if(subauraQustion.questionComplete){
       						$scope.setprogressiveError(subauraQustion);
       					}else if(!subauraQustion.questionComplete){
       						$scope.keepGoing = false;
       					}
       				});

       	   		});
       		}else if(baseQuestionObj.classType=='HeightQuestion' && (!angular.isUndefined($scope.heightval) && $scope.heightval.length>0)){
       			$scope.keepGoing = true;
       		}else if(baseQuestionObj.classType=='WeightQuestion' && (!angular.isUndefined($scope.weightVal)  && $scope.weightVal.length>0)){
       			$scope.keepGoing = true;
       		}else{
       			$scope.keepGoing = false;
       		}

      	}

      	 $scope.proceedToNext = function (){
      		 //check if questions are answered
      		$scope.coverDetails.lastSavedOn = '';
      		$scope.erroneousSections = [];
       		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) {
       			$scope.keepGoing = true;
      			angular.forEach($scope.auraResponseDataList[selectedIndex].questions, function (baseQuestionObj, index) {
      				if($scope.keepGoing){
      					$scope.checkIncompleteQuestion($scope.auraResponseDataList[selectedIndex].questions[index],$scope.keepGoing)
      				}

      			});
      			if(!$scope.keepGoing) {
                    $scope.erroneousSections.push(Object.sectionName);
                  }
      		});
       		$scope.isAuraErroneous = $scope.erroneousSections.length > 0;
      		if(!$scope.isAuraErroneous){
      		    $scope.submitHeightWeightQuestion($scope.healthQuestionIndex).then(function() {
      			submitAuraSvc.requestObj($scope.urlList.submitAuraUrl).then(function(response) {
      				$scope.auraResponses = response.data;
      				 if(response.status == 200) {
      				if($scope.auraResponses.overallDecision!='DCL'){
      					clientMatchSvc.reqObj($scope.urlList.clientMatchUrl).then(function(clientMatchResponse) {
      						if(clientMatchResponse.data.matchFound){
      							$scope.clientmatchreason = '';
      							$scope.auraResponses.clientMatched = clientMatchResponse.data.matchFound
      							$scope.auraResponses.overallDecision='RUW'
      								$scope.information = clientMatchResponse.data.information
      								angular.forEach($scope.information, function (infoObj,index) {
      									if(index==$scope.information.length-1){
      										$scope.clientmatchreason = $scope.clientmatchreason+ infoObj.system +' client match : '+infoObj.key+", "
      										$scope.clientmatchreason = $scope.clientmatchreason.substring(0,$scope.clientmatchreason.lastIndexOf(','))
      									}else{
      										$scope.clientmatchreason = $scope.clientmatchreason+ infoObj.system +' client match : '+infoObj.key+", "
      									}

      								})
      							$scope.auraResponses.clientMatchReason= $scope.clientmatchreason;
      							$scope.auraResponses.specialTerm = false;
      						}

          					appData.setChangeCoverAuraDetails($scope.auraResponses);
                    //$scope.setHeightWeight();
          					$location.path('/summary');
          				});
      				}else if($scope.auraResponses.overallDecision == 'DCL'){
      					if($scope.coverDetails != null && $scope.changeCoverOccDetails != null && $scope.deathAddnlDetails != null &&
      		    				$scope.tpdAddnlDetails != null && $scope.ipAddnlDetails != null && $scope.auraResponses != null && $scope.personalDetails != null){
      		    			$scope.coverDetails.lastSavedOn = '';
      		    			$scope.details={};
      		    			$scope.details.addnlDeathCoverDetails = $scope.deathAddnlDetails;
      		    			$scope.details.addnlTpdCoverDetails = $scope.tpdAddnlDetails;
      		    			$scope.details.addnlIpCoverDetails = $scope.ipAddnlDetails;
      		    			$scope.details.occupationDetails = $scope.changeCoverOccDetails;
      		    			var temp = angular.extend($scope.details,$scope.coverDetails);
      		    			var aura = angular.extend(temp,$scope.auraResponses);
      		    			var submitObject = angular.extend(aura, $scope.personalDetails);
      		    			submitObject.smoker = $scope.details.occupationDetails.smoker;
      		    			auraRespSvc.setResponse(submitObject);
      		    			submitEapplySvc.submitObj($scope.urlList.submitEapplyUrl).then(function(response) {
      		            		appData.setPDFLocation(response.data.clientPDFLocation);
      		            		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
      		            		appData.setNpsUrl(response.data.npsTokenURL);
      		            		$location.path('/changedecline');
      		            	}, function(err){
      		            		console.log('Error saving the cover ' + err);
      		            	});
      		            }
      				}
      				 }
         		 });
         		 }, function(err) {
							console.log(err);
						});
  			}
      		 console.log($scope.erroneousSections);
             if($scope.erroneousSections.length)
               $scope.formatErrorMsg();
      	 }

      	 $scope.formatErrorMsg = function() {
             for( var v=0; v < $scope.erroneousSections.length; v++) {
               $scope.erroneousSections[v] = $scope.erroneousSections[v].replace('QG=', '');
             }

         }
      	// Save
      	var appNum;
        appNum = fetchAppNumberSvc.getAppNumber();
      	 $scope.saveAura = function (){
      		if($scope.coverDetails != null && $scope.changeCoverOccDetails != null &&  $scope.deathAddnlDetails != null && $scope.tpdAddnlDetails != null && $scope.ipAddnlDetails != null &&  $scope.personalDetails != null){
        		if($scope.heightval && $scope.weightVal) {
							$scope.submitHeightWeightQuestion($scope.healthQuestionIndex).then(function() {
								$scope.saveAuraDetails();
							}, function(err) {
								console.log(err);
							});
						} else {
							$scope.saveAuraDetails();
						}
        	}
          //$scope.heightval = $scope.heightval || null;
          //$scope.weightVal = $scope.weightVal || null;
          //if($scope.heightval != null || $scope.weightVal != null)
            //$scope.submitHeightWeightQuestion(2);
       };
         $scope.saveAuraDetails = function() {
           $scope.coverDetails.lastSavedOn = 'AuraPage';
        		$scope.details={};
        		$scope.details.addnlDeathCoverDetails = $scope.deathAddnlDetails;
    			$scope.details.addnlTpdCoverDetails = $scope.tpdAddnlDetails;
    			$scope.details.addnlIpCoverDetails = $scope.ipAddnlDetails;
    			$scope.details.occupationDetails = $scope.changeCoverOccDetails;
    			
    			//$scope.auraDetails
        		var temp = angular.extend($scope.details,$scope.coverDetails);
            	var saveAuraObject = angular.extend(temp, $scope.personalDetails);
            	
            	if($scope.changeCoverOccDetails.smoker == 'Yes') {
            		saveAuraObject.occupationDetails.smokerQuestion = 1;
  			   	}else{
  			   		saveAuraObject.occupationDetails.smokerQuestion = 2;
  			   	}
            	auraRespSvc.setResponse(saveAuraObject)
    	        saveEapplyData.reqObj($scope.urlList.saveEapplyUrl, saveAuraObject).then(function(response) {
                console.log(response.data);
                $scope.auraSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+ appNum +'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
    	        });
         }
       $scope.auraSaveAndExitPopUp = function (hhText) {

			var dialog1 = ngDialog.open({
				    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary"  ng-dialog-class="ngdialog-theme-plain"  ng-click="closeThisDialog()">Finish &amp; Close Window </button></div></div>',
					className: 'ngdialog-theme-plain custom-width',
					preCloseCallback: function(value) {
					       var url = "/landing"
					       $location.path( url );
					       return true
					},
					plain: true
			});
			dialog1.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};


      	$scope.clickToOpen = function (hhText) {
      		console.log(hhText)
			var dialog = ngDialog.open({
				/*template: '<p>'+hhText+'</p>' +
					'<div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(1)">Close Me</button></div>',*/
					template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain"  ng-click="closeThisDialog()" style="width: 100%;font-weight: normal !important; font-size: 18px !important; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">Close</button></div><div class="col-sm-4"></div></div></div>',
					className: 'ngdialog-theme-plain',
					plain: true
			});
			dialog.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};


    }]);
   /*Aura Page Controller Ends*/
