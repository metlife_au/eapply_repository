package au.com.metlife.eapply.repositories.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

  /** 
    * CREATE TABLE "EAPPDB"."TBL_CORP_FUND" (
	*		"PARTNERNAME" VARCHAR(50) NOT NULL, 
	*		"FUNDCODE" VARCHAR(50) NOT NULL, 
	*		"ADMIN_CODE" VARCHAR(50) NOT NULL, 
	*		"BROKER_NAME" VARCHAR(500) NOT NULL, 
	*		"BROKER_EMAIL" VARCHAR(500) NOT NULL, 
	*		"BROKER_CONTACTNUMBER" INTEGER, 
	*		"EMPLOYER_CONTACTNUMBER" INTEGER, 
	*		"EMPLOYER_EMAIL" VARCHAR(500), 
	*		"EMPLOYER_KEY_CONTACTNAME" VARCHAR(500), 
	*		"STATUS" VARCHAR(50) NOT NULL, 
	*		"MOD_DAT" TIMESTAMP, 
	*		"MOD_USR" VARCHAR(500), 
	*		"CREATED_DATE" TIMESTAMP NOT NULL, 
	*		"ID" Integer
	*	)
	*	ORGANIZE BY ROW
	*	DATA CAPTURE NONE 
	*	IN "EAPPIDX"
	*	COMPRESS NO;
	*
	* ALTER TABLE "EAPPDB"."TBL_CORP_FUND" ADD CONSTRAINT "CC1511268679339" PRIMARY KEY ("ID");
	*
	* ALTER TABLE "EAPPDB"."TBL_CORP_FUND" ADD CONSTRAINT "CC1511754569357" UNIQUE ("FUNDCODE");
	*
	* COMMENT ON TABLE "EAPPDB"."TBL_CORP_FUND" IS 'Added for Corporate';
	*
	*/

@Entity
@Table(name="TBL_CORP_FUND", schema="eappdb")
public class CorporateFund {
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="TBL_CORP_FUND_SEQ")
	@SequenceGenerator(name="TBL_CORP_FUND_SEQ",sequenceName = "EAPPDB.TBL_CORP_FUND_SEQ", initialValue=1000, allocationSize=100)
	private Integer id;
	
	@Column(name="PARTNERNAME")
	private String partnerName;
	
	@Column(name="FUNDCODE")
	private String fundCode;

	@Column(name="ADMIN_CODE")
	private String adminCode;
	
	@Column(name="BROKER_NAME")
	private String brokerName;
	
	@Column(name="BROKER_EMAIL")
	private String brokerEmail;
	
	@Column(name="BROKER_CONTACTNUMBER")
	private String brokercontactNum;
	
	@Column(name="EMPLOYER_CONTACTNUMBER")
	private String employerContactNum;
	
	@Column(name="EMPLOYER_EMAIL")
	private String employerEmail;
	
	@Column(name="EMPLOYER_KEY_CONTACTNAME")
	private String employerKeyContactNm;
	
	@Column(name="STATUS")
	private String status;
	
	@Column(name="MOD_DAT")
	private Timestamp lastUpdtDate;
	
	@Column(name="MOD_USR")
	private String lastUpdtUser;
	
	@Column(name="CREATED_DATE")
	private Timestamp createdDate;
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy = "corporateFundId")
	private List<CorporateFundCat> corpFundCategories;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	
	
	
	public List<CorporateFundCat> getCorpFundCategories() {
		return corpFundCategories;
	}
	public void setCorpFundCategories(List<CorporateFundCat> corpFundCategories) {
		this.corpFundCategories = corpFundCategories;
	}
	public String getFundCode() {
		return fundCode;
	}
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
	public String getAdminCode() {
		return adminCode;
	}
	public void setAdminCode(String adminCode) {
		this.adminCode = adminCode;
	}
	public String getBrokerName() {
		return brokerName;
	}
	public void setBrokerName(String brokerName) {
		this.brokerName = brokerName;
	}
	public String getBrokerEmail() {
		return brokerEmail;
	}
	public void setBrokerEmail(String brokerEmail) {
		this.brokerEmail = brokerEmail;
	}
	
	public String getEmployerEmail() {
		return employerEmail;
	}
	public void setEmployerEmail(String employerEmail) {
		this.employerEmail = employerEmail;
	}
	public String getEmployerKeyContactNm() {
		return employerKeyContactNm;
	}
	public void setEmployerKeyContactNm(String employerKeyContactNm) {
		this.employerKeyContactNm = employerKeyContactNm;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Timestamp getLastUpdtDate() {
		return lastUpdtDate;
	}
	public void setLastUpdtDate(Timestamp lastUpdtDate) {
		this.lastUpdtDate = lastUpdtDate;
	}
	public String getLastUpdtUser() {
		return lastUpdtUser;
	}
	public void setLastUpdtUser(String lastUpdtUser) {
		this.lastUpdtUser = lastUpdtUser;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public String getBrokercontactNum() {
		return brokercontactNum;
	}
	public void setBrokercontactNum(String brokercontactNum) {
		this.brokercontactNum = brokercontactNum;
	}
	public String getEmployerContactNum() {
		return employerContactNum;
	}
	public void setEmployerContactNum(String employerContactNum) {
		this.employerContactNum = employerContactNum;
	}
	/*public List<CorporateFundCat> getCorpFundCategories() {
		return corpFundCategories;
	}
	public void setCorpFundCategories(List<CorporateFundCat> corpFundCategories) {
		this.corpFundCategories = corpFundCategories;
	}*/
	/*@Override
	public String toString() {
		return "CorporateFund [id=" + id + ", partnerName=" + partnerName + ", fundCode=" + fundCode + ", adminCode="
				+ adminCode + ", brokerName=" + brokerName + ", brokerEmail=" + brokerEmail + ", brokercontactNum="
				+ brokercontactNum + ", employerContactNum=" + employerContactNum + ", employerEmail=" + employerEmail
				+ ", employerKeyContactNm=" + employerKeyContactNm + ", status=" + status + ", lastUpdtDate="
				+ lastUpdtDate + ", lastUpdtUser=" + lastUpdtUser + ", createdDate=" + createdDate
				+ ", corpFundCategories=" + corpFundCategories + "]";
	}*/
	@Override
	public String toString() {
		return "CorporateFund [id=" + id + ", partnerName=" + partnerName + ", corpFundCategories=" + corpFundCategories
				+ ", adminCode=" + adminCode + ", brokerName=" + brokerName + ", brokerEmail=" + brokerEmail
				+ ", brokercontactNum=" + brokercontactNum + ", employerContactNum=" + employerContactNum
				+ ", employerEmail=" + employerEmail + ", employerKeyContactNm=" + employerKeyContactNm + ", status="
				+ status + ", lastUpdtDate=" + lastUpdtDate + ", lastUpdtUser=" + lastUpdtUser + ", createdDate="
				+ createdDate + "]";
	}
	
	
	
	
	

}
