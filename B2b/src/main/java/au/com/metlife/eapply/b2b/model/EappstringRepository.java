package au.com.metlife.eapply.b2b.model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface  EappstringRepository extends JpaRepository<Eappstring, String>{
	public Eappstring findById(Long id);	
	public Eappstring findBySecureToken(String secureToken);	
}
