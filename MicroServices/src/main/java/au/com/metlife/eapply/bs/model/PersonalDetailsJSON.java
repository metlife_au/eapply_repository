package au.com.metlife.eapply.bs.model;

import java.io.Serializable;

public class PersonalDetailsJSON implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6586317269621305137L;

	private String applicantSubType;
	
	private String dateOfBirth;
	
	private String firstName;
	
	private String gender;
	
	private String lastName;
	
	private String priority;
	
	private String smoker;
	
	private String title;

	public String getApplicantSubType() {
		return applicantSubType;
	}

	public void setApplicantSubType(String applicantSubType) {
		this.applicantSubType = applicantSubType;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getSmoker() {
		return smoker;
	}

	public void setSmoker(String smoker) {
		this.smoker = smoker;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	

}
