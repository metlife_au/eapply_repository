(function (angular) {
	'use strict';
	/* global channel, inputData, token, moment */
	/**
	 * @ngdoc controller
	 * @name guildApp.controller:summaryCtrl
	 *
	 * @description
	 * This is summaryCtrl description
	 */
	angular
	.module('guildApp')
	.controller('nonMemberSummary', nonMemberSummary);
	nonMemberSummary.$inject=['$scope', '$rootScope', '$location','$timeout','$window', 'fetchUrlSvc', 'ngDialog', 'appData', 'fetchPersoanlDetailSvc', 'calcDeathAmtSvc', 'calcTPDAmtSvc', 'calcIPAmtSvc', 'saveEapplyData', 'submitEapplySvc','PersistenceService', 'auraRespSvc', 'fetchIndustryListGuild', 'auraInputSvc', 'submitAuraSvc', 'clientMatchSvc', '$q'];
		function nonMemberSummary($scope, $rootScope, $location, $timeout, $window, fetchUrlSvc, ngDialog, appData, fetchPersoanlDetailSvc, calcDeathAmtSvc, calcTPDAmtSvc, calcIPAmtSvc, saveEapplyData, submitEapplySvc, PersistenceService, auraRespSvc, fetchIndustryListGuild, auraInputSvc, submitAuraSvc, clientMatchSvc, $q) {
			var vm = this;
		vm.handleBtnKeyPress = function(event) {
			  // Check to see if space or enter were pressed
			if (event.key === ' ' || event.key === 'Enter') {
			  // Prevent the default action to stop scrolling when space is pressed
			  event.preventDefault();
			  toggleButton(event.target);
			}
		};
		function toggleButton(element) {
				  // Check to see if the button is pressed
			var pressed = (element.getAttribute('aria-pressed') === 'true');
				  // Change aria-pressed to the opposite state
			element.setAttribute('aria-pressed', !pressed);
		}
		

		  vm.CCS = {};
		  vm.getAuraDetails = function() {
		    var defer = $q.defer();
		    auraInputSvc.setFund('GUIL');         
		    auraInputSvc.setAppnumber(parseInt(vm.CCS.applicatioNumber));
		    auraInputSvc.setClientname('metaus');       
		    auraInputSvc.setLastName(vm.personalDetails.lastName);
		    auraInputSvc.setFirstName(vm.personalDetails.firstName);
		    auraInputSvc.setDob(vm.personalDetails.dateOfBirth);
		    submitAuraSvc.requestObj(vm.urlList.submitAuraUrl).then(function(response) {                           
		      vm.auraResponses = response.data;   
		      if(vm.auraResponses.overallDecision!=='DCL'){
		        clientMatchSvc.reqObj(vm.urlList.clientMatchUrl).then(function(clientMatchResponse) {
		          if(clientMatchResponse.data.matchFound){
		            vm.clientmatchreason = '';
		            vm.auraResponses.clientMatched = clientMatchResponse.data.matchFound;
		            vm.auraResponses.overallDecision='RUW';
		              vm.information = clientMatchResponse.data.information;
		              angular.forEach(vm.information, function (infoObj,index) {                        
		                if(index===vm.information.length-1){
		                  vm.clientmatchreason = vm.clientmatchreason+ infoObj.system +' client match : '+infoObj.key+', ';
		                  vm.clientmatchreason = vm.clientmatchreason.substring(0,vm.clientmatchreason.lastIndexOf(','))  ;                        
		                }else{
		                  vm.clientmatchreason = vm.clientmatchreason+ infoObj.system +' client match : '+infoObj.key+', ' ; 
		                }                                       
		                
		              });
		            vm.auraResponses.clientMatchReason= vm.clientmatchreason;
		            vm.auraResponses.specialTerm = false;
		          }
		          appData.setChangeCoverAuraDetails(vm.auraResponses);
		          defer.resolve(vm.auraResponses);          
		        });
		      }
		    }, function(err) {
		      defer.reject(err);
		    });
		    return defer.promise;
		  };

		  angular.extend(vm.CCS, appData.getAppData());
		  $window.scrollTo(0, 0);
		  vm.urlList = fetchUrlSvc.getUrlList();
		  
		  vm.olddeathCoverPremium =vm.CCS.applicant.cover[0].cost;
		  vm.oldtpdCoverPremium = vm.CCS.applicant.cover[1].cost;
		  vm.oldipCoverPremium = vm.CCS.applicant.cover[2].cost;
		  
		  vm.inputDetails = fetchPersoanlDetailSvc.getMemberDetails() || {};

		  vm.personalDetails = vm.inputDetails.personalDetails || {};
		  vm.contactDetails = vm.inputDetails.contactDetails || {};
		  vm.CCS.auraDisabled = (vm.CCS.auraDisabled === 'false' || vm.CCS.auraDisabled === false) ? false : true;
		  
		  if(vm.CCS.applicant.contactType === '1') {
			vm.contactType = 'Mobile';
		  }else if(vm.CCS.applicant.contactType === '2') {
			vm.contactType = 'Home';
		  }else if(vm.CCS.applicant.contactType === '3') {
			vm.contactType = 'Work';
		  }else{
			vm.contactType = '';
		  }
		  vm.auraDetails = null;
		  vm.init = function() {
			var defer = $q.defer();
			vm.CCS.responseObject = vm.auraDetails.responseObject;
		    vm.CCS.applicant.birthDate = moment(vm.CCS.applicant.birthDate, 'DD/MM/YYYY').format('DD/MM/YYYY');
		    
		    if(vm.CCS.applicant.contactType === '1') {
				vm.contactType = 'Mobile';
			  }else if(vm.CCS.applicant.contactType === '2') {
				vm.contactType = 'Home';
			  }else if(vm.CCS.applicant.contactType === '3') {
				vm.contactType = 'Work';
			  }else{
				vm.contactType = '';
			  }

		    // Industry name
		    //vm.CCS.applicant.occupationCode = fetchIndustryListMtaa.getIndustryName(vm.CCS.applicant.industryType);
		    vm.CCS.applicant.cover[0].coverDecision = vm.auraDetails.deathDecision;
		    vm.CCS.applicant.cover[1].coverDecision = vm.auraDetails.tpdDecision;
		    vm.CCS.applicant.cover[2].coverDecision = vm.auraDetails.ipDecision;
		    vm.CCS.applicant.cover[0].coverExclusion = vm.auraDetails.deathExclusions;
		    vm.CCS.applicant.cover[1].coverExclusion = vm.auraDetails.tpdExclusions;
		    vm.CCS.applicant.cover[2].coverExclusion = vm.auraDetails.ipExclusions;
		    vm.CCS.applicant.cover[0].loading = vm.auraDetails.deathLoading;
		    vm.CCS.applicant.cover[1].loading = vm.auraDetails.tpdLoading;
		    vm.CCS.applicant.cover[2].loading = vm.auraDetails.ipLoading;
		    // Death loading calculation
		  	if(vm.auraDetails != null && vm.auraDetails.overallDecision === 'ACC' && vm.auraDetails.deathLoading && vm.auraDetails.deathLoading > 0){
		      	var deathReqObject = {
		          		'age': vm.CCS.applicant.age,
		          		'fundCode': 'GUIL',
		          		'gender': vm.CCS.applicant.gender,
		          		'deathOccCategory': vm.CCS.applicant.cover[0].occupationRating,
		          		'smoker': false,
		          		'deathUnitsCost': null,
		          		'premiumFrequency': vm.CCS.applicant.cover[0].frequencyCostType,
		          		'manageType': 'NMCOVER',
		          		'deathCoverType': vm.CCS.applicant.cover[0].coverCategory 
		          	};
		      	if(vm.CCS.applicant.cover[0].coverCategory  === 'DcFixed'){
		      		deathReqObject['deathFixedAmount'] = parseInt(vm.CCS.applicant.cover[0].additionalUnit)- parseInt(vm.CCS.applicant.cover[0].existingCoverAmount);
		      	} else if(vm.CCS.applicant.cover[0].coverCategory  === 'DcUnitised'){
		      		deathReqObject['deathUnits'] = parseInt(vm.CCS.applicant.cover[0].additionalUnit)- parseInt(vm.CCS.applicant.cover[0].fixedUnit);
		      	}

		      	calcDeathAmtSvc.calculateAmount(vm.urlList.calculateDeathUrl,deathReqObject).then(function(res){
		      		var deathResponse = res.data[0];
		      		vm.CCS.deathLoadingCost = deathResponse.cost;
		      		vm.CCS.applicant.cover[0].cost = parseFloat(vm.CCS.applicant.cover[0].cost) +
		      														((parseFloat(vm.auraDetails.deathLoading)/100) * parseFloat(vm.CCS.deathLoadingCost));
		      		vm.CCS.totalPremium = parseFloat(vm.CCS.applicant.cover[0].cost) + parseFloat(vm.CCS.applicant.cover[1].cost) + parseFloat(vm.CCS.applicant.cover[2].cost);
		      		vm.CCS.deathLoadingCost = (parseFloat(vm.auraDetails.deathLoading)/100) * parseFloat(vm.CCS.deathLoadingCost);
		      		vm.CCS.applicant.cover[0].policyFee=Math.round((vm.CCS.deathLoadingCost + 0.00001)*100)/100;
		      		defer.resolve(res);
		      	}, function(err){
		      		console.log('Error occured during calculating loading ' + JSON.stringify(err));
		      		defer.reject(err);
		      	});
		      }else{
		    	  vm.CCS.applicant.cover[0].policyFee = null;
		      }

		  	// TPD loading calculation
		  	if(vm.auraDetails != null && vm.auraDetails.overallDecision === 'ACC' && vm.auraDetails.tpdLoading && vm.auraDetails.tpdLoading > 0){
		      	var tpdReqObject = {
		          		'age': vm.CCS.age,
		          		'fundCode': 'GUIL',
		          		'gender': vm.CCS.applicant.gender,
		          		'tpdOccCategory': vm.CCS.applicant.cover[1].occupationRating,
		          		'smoker': false,
		          		'tpdUnitsCost': null,
		          		'premiumFrequency': vm.CCS.applicant.cover[1].frequencyCostType,
		          		'manageType': 'NMCOVER',
		          		'tpdCoverType': vm.CCS.applicant.cover[1].coverCategory 
		          	};
		      	if(vm.CCS.applicant.cover[1].coverCategory === 'TPDFixed'){
		      		tpdReqObject['tpdFixedAmount'] = parseInt(vm.CCS.applicant.cover[1].additionalUnit)- parseInt(vm.CCS.applicant.cover[1].existingCoverAmount);
		      	} else if(vm.CCS.applicant.cover[1].coverCategory === 'TPDUnitised'){
		      		tpdReqObject['tpdUnits'] = parseInt(vm.CCS.applicant.cover[1].additionalUnit)- parseInt(vm.CCS.applicant.cover[1].fixedUnit);
		      	}

		      	calcTPDAmtSvc.calculateAmount(vm.urlList.calculateTpdUrl,tpdReqObject).then(function(res){
		      		var tpdResponse = res.data[0];
		      		vm.CCS.tpdLoadingCost = tpdResponse.cost;
		      		vm.CCS.applicant.cover[1].cost = parseFloat(vm.CCS.applicant.cover[1].cost) +
		      														((parseFloat(vm.auraDetails.tpdLoading)/100) * parseFloat(vm.CCS.tpdLoadingCost));
		      		vm.CCS.totalPremium = parseFloat(vm.CCS.applicant.cover[0].cost) + parseFloat(vm.CCS.applicant.cover[1].cost) + parseFloat(vm.CCS.applicant.cover[2].cost);
		      		vm.CCS.tpdLoadingCost = (parseFloat(vm.auraDetails.tpdLoading)/100) * parseFloat(vm.CCS.tpdLoadingCost);
		      		vm.CCS.applicant.cover[1].policyFee=Math.round((vm.CCS.tpdLoadingCost + 0.00001)*100)/100;
		      		defer.resolve(res);
		      	}, function(err){
		      		console.log('Error occured during calculating loading ' + JSON.stringify(err));
		      		defer.reject(err);
		      	});
		      }else{
		    	  vm.CCS.applicant.cover[1].policyFee=null;
		      }

		  	// IP loading calculation
		  	if(vm.auraDetails != null && vm.auraDetails.overallDecision === 'ACC' && vm.auraDetails.ipLoading && vm.auraDetails.ipLoading > 0){
		      	var ipReqObject = {
		      		'age': vm.CCS.applicant.age,
		      		'fundCode': 'GUIL',
		      		'gender': vm.CCS.applicant.gender,
		      		'ipOccCategory': vm.CCS.applicant.cover[2].occupationRating,
		      		'smoker': false,
		      		'ipUnitsCost': null,
		      		'premiumFrequency': vm.CCS.applicant.cover[2].frequencyCostType,
		      		'manageType': 'NMCOVER',
		      		'ipCoverType': 'IpUnitised',
		      		'ipWaitingPeriod': vm.CCS.applicant.cover[2].existingIpWaitingPeriod,
		      		'ipBenefitPeriod': vm.CCS.applicant.cover[2].existingIpBenefitPeriod,
		      		'ipFixedAmount':parseInt(vm.CCS.applicant.cover[2].additionalUnit)- parseInt(vm.CCS.applicant.cover[2].existingCoverAmount)
		      	};

		      	calcIPAmtSvc.calculateAmount(vm.urlList.calculateIpUrl,ipReqObject).then(function(res){
		      		var ipResponse = res.data[0];
		      		vm.CCS.ipLoadingCost = ipResponse.cost;
		      		vm.CCS.applicant.cover[2].cost = parseFloat(vm.CCS.applicant.cover[2].cost) +
		      														((parseFloat(vm.auraDetails.ipLoading)/100) * parseFloat(vm.CCS.ipLoadingCost));
		      		vm.CCS.totalPremium = parseFloat(vm.CCS.applicant.cover[0].cost) + parseFloat(vm.CCS.applicant.cover[1].cost) + parseFloat(vm.CCS.applicant.cover[2].cost);
		      		vm.CCS.ipLoadingCost = (parseFloat(vm.auraDetails.ipLoading)/100) * parseFloat(vm.CCS.ipLoadingCost);
		      		vm.CCS.applicant.cover[2].policyFee=Math.round((vm.CCS.ipLoadingCost + 0.00001)*100)/100;
		      		defer.resolve(res);
		      	}, function(err){
		      		console.log('Error occured during calculating loading ' + JSON.stringify(err));
		      		defer.reject(err);
		      	});
		      }else{
		    	  vm.CCS.applicant.cover[2].policyFee=null;
		      }
		  };

		  if(Object.keys(appData.getChangeCoverAuraDetails()).length) {
		    vm.auraDetails = appData.getChangeCoverAuraDetails();
		    vm.init();
		  } else if(!vm.CCS.auraDisabled) {
		    vm.getAuraDetails().then(function(result) {
		      vm.auraDetails = result;
		      vm.init();
		    });
		  }
		  else
			  {
			  // Industry name
			    vm.CCS.applicant.occupationCode = fetchIndustryListGuild.getIndustryName(vm.CCS.applicant.industryType);
			  }
		  
		  vm.go = function (path) {
			  vm.resetPremiumValue();
		    $location.path(path);
		  };

		  vm.navigateToLandingPage = function () {
		    ngDialog.openConfirm({
		        template:'<div class=\'ngdialog-content\'><div class="modal-body"><!-- Row starts --><div class=\'row  rowcustom  \'><div class=\'col-sm-8\'><p> Are you sure you want to navigate to Home Page?</p> ' + 
		        			'</div> </div></div></br><div class=\'ngdialog-footer mt0\'><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\'confirm()\'>Ok</button>' +
		        			'<button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-dialog-controller=\'SecondModalCtrl\' ng-click=\"closeThisDialog(\'oncancel\')\">Cancel</button></div></div>',
		        plain: true,
		        className: 'ngdialog-theme-plain custom-width'
		      }).then(function() {
		        $location.path('/landing');
		      }, function(e){
		        if(e==='oncancel') {
		          return false;
		        }
		      });
		   };

		   vm.summarySaveAndExitPopUp = function (hhText) {
		      var dialog1 = ngDialog.open({
		           template: '<div class=\'ngdialog-content\'><div class=\'modal-header\'><h4 id=\'myModalLabel\' class=\'modal-title aligncenter\'>Application saved</h4></div>' + 
		           '<div class=\'modal-body\'><!-- Row starts --><div class=\'row  rowcustom  \'>  <div class=\'col-sm-12\'>  <p class=\'aligncenter\'>  </p><div id=\'tips_text\'>'+hhText+'</div>  ' + 
		           '<p></p>  </div></div><!-- Row ends --></div><div class=\'ngdialog-buttons aligncenter\'><button type=\'button\' class=\'btn btn-primary\' ng-dialog-class=\'ngdialog-theme-plain\' ng-click=\'closeThisDialog()\'>Finish &amp; Close Window </button></div></div>',
		         className: 'ngdialog-theme-plain custom-width',
		         preCloseCallback: function(value) {
		                var url = '/landing';
		                $location.path( url );
		                return true;
		         },
		         plain: true
		      });
		      dialog1.closePromise.then(function (data) {
		        console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		      });
		   };

		   vm.saveSummary = function() {
			   vm.resetPremiumValue();
		     vm.CCS.lastSavedOn = 'NonMemberSummaryPage';
		     var saveSummaryObject = angular.copy(vm.CCS);
		     saveEapplyData.reqObj(vm.urlList.saveEapplyUrlNew, saveSummaryObject).then(function(response) {
		       console.log(response.data);
		       vm.summarySaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+ vm.CCS.applicatioNumber +'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
		     });
		   };
		   var ackCheckCCGC;
		   var ackCheckLE;
		   vm.navigateToDecision = function() {
		     ackCheckCCGC = $('#generalConsentLabel').hasClass('active');

		     if(vm.auraDetails != null){
		      if(vm.auraDetails.specialTerm != null && vm.auraDetails.specialTerm === true){
		       ackCheckLE = $('#lodadingExclusionLabel').hasClass('active');
		        }
		     }

		     if(ackCheckCCGC && ((vm.auraDetails != null && !vm.auraDetails.specialTerm) || (vm.auraDetails != null && vm.auraDetails.specialTerm && ackCheckLE) || vm.auraDetails===null)){
		       vm.CCS.LEFlag = false;
		       vm.CCGCackFlag = false;
		       $rootScope.$broadcast('disablepointer');
		       vm.CCS.lastSavedOn = '';
		       var submitSummaryObject = angular.copy(vm.CCS);
		       auraRespSvc.setResponse(submitSummaryObject);
		       submitEapplySvc.submitObj(vm.urlList.submitEapplyUrlNew, submitSummaryObject).then(function(response) {
		         if(response.data) {
		        	 appData.setPDFLocation(response.data.clientPDFLocation);
		        	 appData.setNpsUrl(response.data.npsTokenURL);
		             if(vm.auraDetails!=null){
		               if(vm.auraDetails.overallDecision === 'ACC'){
		                     if(vm.auraDetails.specialTerm){
		                       $location.path('/nonMemberaspcltermsacc');
		                     }else{
		                       $location.path('/nonMemberaccept');
		                     }
		                   } else if(vm.auraDetails.overallDecision === 'DCL'){
		                     $location.path('/nonMemberdecline');
		                   } else if(vm.auraDetails.overallDecision === 'RUW'){
		                     $location.path('/nonMemberunderwriting');
		                   }
		             }else{
		               $location.path('/nonMemberaccept');
		             }
		         } else {
		                 $window.scrollTo(0, 0);
		                 $rootScope.$broadcast('enablepointer');
		                 throw {message: 'No data found'};
		             }
		       }, function(err){
		         vm.errorOccured = true;
		         $window.scrollTo(0, 0);
		             $rootScope.$broadcast('enablepointer');
		       });
		     } else{
		         if(ackCheckCCGC){
		           vm.CCGCackFlag = false;
		         }else{
		           vm.CCGCackFlag = true;
		         }
		         if(ackCheckLE){
		           vm.CCS.LEFlag = false;
		         }else{
		           vm.CCS.LEFlag = true;
		         }
		         vm.scrollToUncheckedElement();
		     }
		   };

		   vm.scrollToUncheckedElement = function(){
			   var ids;
			   var elements;
		     if(vm.auraDetails!=null && vm.auraDetails.specialTerm){
		       elements = [ackCheckLE, ackCheckCCGC];
		        ids = ['lodadingExclusionLabel', 'generalConsentLabel'];
		     } else{
		       elements = [ackCheckCCGC];
		        ids = ['generalConsentLabel'];
		     }
		   };

		   vm.checkAckStateGC = function(){
		     $timeout(function(){
		       ackCheckCCGC = $('#generalConsentLabel').hasClass('active');
		         if(ackCheckCCGC){
		           vm.CCGCackFlag = false;
		           vm.CCS.ackCheck = true;
		         }else{
		           vm.CCGCackFlag = true;
		           vm.CCS.ackCheck = false;
		         }
		     }, 10);
		   };

		   vm.checkAckStateLE = function(){
		     $timeout(function(){
		       ackCheckLE = $('#lodadingExclusionLabel').hasClass('active');

		         if(ackCheckLE){
		           vm.CCS.LEFlag = false;
		         }else{
		           vm.CCS.LEFlag = true;
		         }
		     }, 10);
		   };
		   
		   vm.resetPremiumValue = function(){
			   
			  vm.CCS.applicant.cover[0].cost = vm.olddeathCoverPremium;
			  vm.CCS.applicant.cover[1].cost =vm.oldtpdCoverPremium;
			  vm.CCS.applicant.cover[2].cost = vm.oldipCoverPremium;
			   
			  };
		   

	}
})(angular);