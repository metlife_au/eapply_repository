package au.com.metlife.eapply;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.FileReader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import au.com.metlife.eapply.bs.controller.EapplyServicesController;
import au.com.metlife.eapply.bs.model.EappHistory;
import au.com.metlife.eapply.bs.service.EapplyService;
import au.com.metlife.eapply.bs.serviceimpl.EapplyServiceImpl;
import au.com.metlife.eapply.quote.utility.SystemProperty;
import au.com.metlife.eapply.utils.TestUtils;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MicroServicesApplication.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EapplyServiceControllerTest extends TestUtils {

	@InjectMocks
	private EapplyServicesController eapplyServicesController;

	@InjectMocks
	private EapplyServiceImpl service;

	private MockMvc mockMvc;

	@MockBean
	private EapplyService eapplyService;

	/*
	 * Created testData to store the string json value from createTestData()
	 */
	String testData = null;

	/*
	 * Created fileName to store the json file name and pass it to
	 * createTestData()
	 */
	String fileName = null;
	
	String fileName1="b4fb3ce62429dff976a4b088277d69459f5f6151b932db4f8aa7e667cc097e12f55c8b69fea5f238";
    String fileName2="D:/shared/pdf/HOSTPLUS_MsHOSTHOSTB_1517994854245.pdf";
   

	@Autowired
	private WebApplicationContext context;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
		EappHistory eappHistory = new EappHistory();
		Mockito.when(eapplyService.createHistory(eappHistory)).thenReturn(eappHistory);
		 
		String swissmediumfont = new ClassPathResource("2A09CD_2_0.ttf").getFile().getAbsolutePath();
		String swisslightfont = new ClassPathResource("2A09CD_0_0.ttf").getFile().getAbsolutePath();
		String swissthinfont = new ClassPathResource("2A09CD_1_0.ttf").getFile().getAbsolutePath();

		SystemProperty sysProp = SystemProperty.getInstance();
		sysProp.setProperty("swissmediumfont", swissmediumfont);
		sysProp.setProperty("swisslightfont", swisslightfont);
		sysProp.setProperty("swissthinfont", swissthinfont);
	}

	@Test
	public void checkDuplicateUWApplicationsHostTest() throws Exception {
		mockMvc.perform(post("/checkDuplicateUWApplications").param("fundCode", "HOST").param("lastName", "HOSTB")
				.param("clientRefNo", "7").param("manageTypeCC", "CCOVER").param("dob", "1/11/1958")
				.param("firstName", "HOST").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andReturn().getResponse().equals(false);

	}

	@Test
	public void checkDuplicateUWApplicationsHostTest1() throws Exception {
		mockMvc.perform(post("/checkDuplicateUWApplications").param("fundCode", "CCOVER").param("lastName", "HOSTB")
				.param("clientRefNo", "7").param("manageTypeCC", "CCOVER").param("dob", "1/11/1958")
				.param("firstName", "HOST").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andReturn().getResponse().equals(false);

	}

	@Test
	public void checkDuplicateUWApplicationsAeisTest() throws Exception {
		mockMvc.perform(post("/checkDuplicateUWApplications").param("fundCode", "AEIS")
				.param("lastName", "DontloadAEIS").param("clientRefNo", "AEIS7").param("manageTypeCC", "CCOVER")
				.param("dob", "1/12/1958").param("firstName", "PVTTestAEIS").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().equals(false);

	}

	@Test
	public void checkDuplicateUWApplicationsTest() throws Exception {
		assertThat(service.checkDupUWApplication("CCOVER", "", "", "", "", ""), is(notNullValue()));
		mockMvc.perform(post("/checkDuplicateUWApplications").param("fundCode", "CCOVER").param("lastName", "")
				.param("clientRefNo", "").param("manageTypeCC", "").param("dob", "1/12/1958").param("firstName", "")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn().getResponse()
				.equals(true);
	}

	// Senario 1

	@Test
	public void eapplySubmitHost() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
		fileName = "eapplySubmitHost.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySubmitCare() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
		fileName = "eapplySubmitCare.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}
	
	/*@Test
	public void eapplySubmitCorp() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
		fileName = "eapplySubmitCorp.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}*/

	@Test
	public void eapplySubmitAeis() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
		fileName = "eapplySubmitAeis.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySaveHost() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
		fileName = "eapplySubmitHost.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySaveCare() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
		fileName = "eapplySubmitCare.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySaveAeis() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
		fileName = "eapplySubmitAeis.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	// Senario 2

	@Test
	public void eapplySubmitTCoverHost() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitTCoverHost.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySubmitTCoverCare() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitTCoverCare.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySubmitTCoverAeis() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitTCoverAeis.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySaveTCoverHost() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitTCoverHost.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySaveTCoverCare() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitTCoverCare.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySaveTCoverAeis() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitTCoverAeis.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	// Senario 3

	@Test
	public void eapplySubmitUWCoverHost() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitUWCoverHost.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySubmitUWCoverCare() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitUWCoverCare.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySubmitUWCoverAeis() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitUWCoverAeis.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySaveUWCoverHost() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitUWCoverHost.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySaveUWCoverCare() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitUWCoverCare.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySaveUWCoverAeis() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitUWCoverAeis.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	// Senario 4

	@Test
	public void eapplySubmitCANCoverAeis() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitCANCoverAeis.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySubmitCANCoverCare() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitCANCoverCare.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySubmitCANCoverHost() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitCANCoverHost.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySaveCANCoverAeis() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitCANCoverAeis.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySaveCANCoverCare() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitCANCoverCare.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySaveCANCoverHost() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitCANCoverHost.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	// Senario 5

	@Test
	public void eapplySubmitSCoverHost() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitSCoverHost.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySubmitSCoverCare() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitSCoverCare.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySubmitSCoverAeis() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitSCoverAeis.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySaveSCoverHost() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitSCoverHost.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySaveSCoverCare() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitSCoverCare.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	@Test
	public void eapplySaveSCoverAeis() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitSCoverAeis.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}

	// Senario 6

	@Test
	public void eapplySubmitICoverHost() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitICoverHost.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());
	}

	@Test
	public void eapplySubmitICoverCare() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitICoverCare.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());
	}

	@Test
	public void eapplySubmitICoverAeis() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitICoverAeis.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());
	}

	@Test
	public void eapplySaveICoverHost() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitICoverHost.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());
	}

	@Test
	public void eapplySaveICoverCare() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitICoverCare.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());
	}

	@Test
	public void eapplySaveICoverAeis() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitICoverAeis.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());
	}

/*	@Test
	public void eapplyFileUpload() throws Exception { // D:\
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5qWXdPREEwT0N3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbFpUWXhPRGt4TnkwMFpqWTVMVFE1TUdJdFlqTXlaUzFqTWprNU9HTXdZbVppTWpnaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS43SW1ZODRJWXlrR3hobERTNXpOMF82VUtHbEZqYnBPZEIyOS1TRkZJeTQ4";
		File file = new File("D:\\uploadfile.jpg");
		FileInputStream fi1 = new FileInputStream(file);
		
		 * MockMultipartFile upload = new MockMultipartFile("upload",
		 * "Penguins.jpg", "image/jpeg",
		 * "C:\\Users\\Public\\Pictures\\Sample Pictures\\Penguins.jpg\\"
		 * .getBytes());
		 
		MockMultipartFile upload = new MockMultipartFile(file.getName(), fi1);
		mockMvc.perform(MockMvcRequestBuilders.fileUpload("/fileUpload").file(upload)
				.header("Content-Type", "uploadfile.jpg").header("Authorization", authId)).andExpect(status().is(200));
	}*/

	@Test
	public void eapplySubmitAura() throws Exception {
		String input = "{\"fund\":\"HOST\",\"mode\":\"TransferCover\",\"age\":59,\"name\":\"HOST HOSTB\",\"appnumber\":1516621017413,\"gender\":\"Female\",\"country\":\"Australia\",\"fifteenHr\":\"Yes\",\"deathAmt\":1,\"tpdAmt\":1,\"ipAmt\":1,\"waitingPeriod\":\"30 Days\",\"benefitPeriod\":\"2 Years\",\"industryOcc\":\"003:Amusement Parlour/Centre - Owner/Manager/Supervisor\",\"salary\":\"156123\",\"clientname\":\"metaus\",\"lastName\":\"HOSTB\",\"firstName\":\"HOST\",\"dob\":\"01/11/1958\",\"existingTerm\":false,\"memberType\":\"None\"}";
		mockMvc.perform(post("/submitAura").contentType(MediaType.APPLICATION_JSON).content(input)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}



	

	@Test
	public void checkExpireApplication() throws Exception {
		mockMvc.perform(post("/expireApplication").param("applicationNumber", "1515504414745")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn().getResponse()
				.equals(true);

	}
	@Test
	public void checkExpireApplicationFalse() throws Exception {
		mockMvc.perform(post("/expireApplication").param("applicationNumber", "1515504414745234234234234234")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn().getResponse()
				.equals(false);

	}

	@Test
	public void getRetieveApps() throws Exception {
		mockMvc.perform(get("/retieveApps").param("fundCode", "HOST").param("clientRefNo", "7")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	// Senario new1

			@Test
			public void eapplySubmitHost1() throws Exception {
				String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
				fileName = "EapplyJson.json";
				String testEapplyInputData = createEapplyInputTestData(fileName);
				mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
						.content(testEapplyInputData)).andExpect(status().isOk());

			}

			// Senario new2 including document

			@Test
			public void eapplySubmitHost2() throws Exception {
				String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
				fileName = "EapplySubmitDoc.json";
				String testEapplyInputData = createEapplyInputTestData(fileName);
				mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
						.content(testEapplyInputData)).andExpect(status().isOk());

			}

			// Senario new2 including document but client no 7

			@Test
			public void eapplySubmitHost21() throws Exception {
				String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
				fileName = "EapplySubmitDoc7.json";
				String testEapplyInputData = createEapplyInputTestData(fileName);
				mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
						.content(testEapplyInputData)).andExpect(status().isOk());

			}

			// Senario new3 including document for UWCOVER
		
			@Test
			public void eapplySubmitHostCategory() throws Exception {
				String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
				fileName = "eapplyCategory.json";
				JSONParser parser = new JSONParser();
				Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
				JSONObject jsonObject = (JSONObject) object;
				System.out.println(jsonObject.toString());
				mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
						.content(jsonObject.toString())).andExpect(status().isOk());
		
			}
			
			@Test
			public void eapplySubmitCoverTypes() throws Exception {
				String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
				fileName = "eapplyCoverTypes.json";
				JSONParser parser = new JSONParser();
				Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
				JSONObject jsonObject = (JSONObject) object;
				System.out.println(jsonObject.toString());
				mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
						.content(jsonObject.toString())).andExpect(status().isOk());
		
			}
			
			@Test
			public void eapplySubmitRUW() throws Exception {
				String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
				fileName = "ruwEapplySubmit.json";
				JSONParser parser = new JSONParser();
				Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
				JSONObject jsonObject = (JSONObject) object;
				System.out.println(jsonObject.toString());
				mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
						.content(jsonObject.toString())).andExpect(status().isOk());
		
			}
			
			@Test
			public void eapplySubmitStatewideConvertMaintain() throws Exception {
				String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
				fileName = "eApplySubmitConvertMaintain.json";
				JSONParser parser = new JSONParser();
				Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
				JSONObject jsonObject = (JSONObject) object;
				System.out.println(jsonObject.toString());
				mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
						.content(jsonObject.toString()))
				.andExpect(status().isOk())
				 .andDo(print());
		
			}

		
			
}
