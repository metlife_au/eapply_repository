/**
 * 
 */
package pages.metlife;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;
import uimap.metlife.CycberSourceLoginPageObjects;



/**
 * @author samba
 * 
 */
public class CyberSourceLoginDetailsPage extends MasterPage {
	
	/**
	 * @param scriptHelper
	 */
	public CyberSourceLoginDetailsPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		
	}
	
	public CyberSourceLoginDetailsPage loginandvalidateCyberSource() throws Exception {
		enterLoginDetails();
		return new CyberSourceLoginDetailsPage(scriptHelper);
	} 
	
	private void enterLoginDetails() throws Exception {
		
		String strMerchantId = dataTable.getData("MetLife_Data", "MerchantId");
		String strLoginName = dataTable.getData("MetLife_Data", "login.name");
		String strLoginPassword = dataTable.getData("MetLife_Data", "login.password");
		String Appnum = dataTable.getData("MetLife_Data", "AppNumber");
		String ExptRsnCd = dataTable.getData("MetLife_Data", "ExptRsnCd");
		if(driver.findElement(By.cssSelector("#orgcookiediv>a")).isDisplayed()==true){
				driver.findElement(By.cssSelector("div[id='orgcookiediv'] a[href='#']")).click();
			}
		    tryAction(waitForClickableElement(CycberSourceLoginPageObjects.txtMerchid),"SET","Merchant id",strMerchantId);
			sleep(1000);
			tryAction(waitForClickableElement(CycberSourceLoginPageObjects.txtUsername),"SET","Login User Name ",strLoginName);
			driver.findElement(CycberSourceLoginPageObjects.txtPassword).sendKeys(strLoginPassword);
			report.updateTestLog("Set Password ", "Password is entered successfully", Status.PASS);
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(CycberSourceLoginPageObjects.btnLogin )).click().perform();
			sleep(1000);
			driver.switchTo().alert().accept();	
			sleep(1000);
			tryAction(waitForClickableElement(CycberSourceLoginPageObjects.lnkTextTs),"Click","Transaction Search ");
			tryAction(waitForClickableElement(CycberSourceLoginPageObjects.lnkTextGs),"Click","General Search ");
			tryAction(fluentWaitElement(CycberSourceLoginPageObjects.rdSearchType), "clkradio", "Search Field Type");
			tryAction(fluentWaitElement(CycberSourceLoginPageObjects.selSearchField),"DropDownSelect", "Search Field", "Merchant Reference Number");
			tryAction(waitForClickableElement(CycberSourceLoginPageObjects.txtSearchValue), "SET", "Application Number",Appnum);
			tryAction(waitForClickableElement(CycberSourceLoginPageObjects.txtSearchValue), "TAB", "Application Number");
			tryAction(waitForClickableElement(CycberSourceLoginPageObjects.btnSearch),"Click","Submit Search ");
			if(driver.getPageSource().contains("No transaction records matched your search criteria.")){
		    	report.updateTestLog("Application number Search Results", "Entry Not Found in Cyber Source", Status.FAIL);	
		       }
			else{
			String StrReasonCode=driver.findElement(CycberSourceLoginPageObjects.txtReasonCode).getText();
			String StrRsnCd=StrReasonCode.trim();
			//assertEqualText(StrRsnCd, ExptRsnCd);
			if(StrRsnCd.equals(ExptRsnCd))
			{
			report.updateTestLog("Transaction Search in Cyber Source", "Transaction is successfully processed", Status.PASS);
			}
			else
			{
			report.updateTestLog("Transaction Search in Cyber Source", "Transaction is not processed successfully ", Status.FAIL);	
			}
			}
	}
}
