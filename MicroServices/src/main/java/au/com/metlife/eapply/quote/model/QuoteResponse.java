/**
 * 
 */
package au.com.metlife.eapply.quote.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 199306
 *
 */
public class QuoteResponse implements Serializable{

	private String coverType;
	
	private BigDecimal coverAmount;
	
	private BigDecimal cost;
	
	private String fulDeathAmount;
	private String fulTPDAmount;
	private String ipFulAmount;
	private boolean dcAalStatus;
	private boolean tpdAalStatus;
	private boolean ipAalStatus;
	public String getCoverType() {
		return coverType;
	}

	public void setCoverType(String coverType) {
		this.coverType = coverType;
	}

	public BigDecimal getCoverAmount() {
		return coverAmount;
	}

	public void setCoverAmount(BigDecimal coverAmount) {
		this.coverAmount = coverAmount;
	}

	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	public String getFulDeathAmount() {
		return fulDeathAmount;
	}

	public void setFulDeathAmount(String fulDeathAmount) {
		this.fulDeathAmount = fulDeathAmount;
	}

	public String getFulTPDAmount() {
		return fulTPDAmount;
	}

	public void setFulTPDAmount(String fulTPDAmount) {
		this.fulTPDAmount = fulTPDAmount;
	}

	public String getIpFulAmount() {
		return ipFulAmount;
	}

	public void setIpFulAmount(String ipFulAmount) {
		this.ipFulAmount = ipFulAmount;
	}

	public boolean isDcAalStatus() {
		return dcAalStatus;
	}

	public void setDcAalStatus(boolean dcAalStatus) {
		this.dcAalStatus = dcAalStatus;
	}

	public boolean isTpdAalStatus() {
		return tpdAalStatus;
	}

	public void setTpdAalStatus(boolean tpdAalStatus) {
		this.tpdAalStatus = tpdAalStatus;
	}

	public boolean isIpAalStatus() {
		return ipAalStatus;
	}

	public void setIpAalStatus(boolean ipAalStatus) {
		this.ipAalStatus = ipAalStatus;
	}


}
