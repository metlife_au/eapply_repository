package pages.metlife.Angular.HOSTPLUS;

import org.openqa.selenium.By;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import com.ibm.IExtendedSecurity.SSLSecurity;

import supportlibraries.ScriptHelper;
public class Eview extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public Eview(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public Eview HOSTPLUS_Login() {
		enterlogindetails();
		return new Eview(scriptHelper);
	} 
	
	private void enterlogindetails() {

try
{
	
	String fundname = dataTable.getData("Eapply_Data", "fundname");
	String FirstName = dataTable.getData("Eapply_Data", "FirstName");
	String LastName = dataTable.getData("Eapply_Data", "LastName");
	
	driver.get("https://www.e2e.eservice.metlife.com.au/wps/portal");
	sleep(500);
	driver.findElement(By.name("MLPAuLoginPortletFormID")).sendKeys("deepam");
	sleep(500);
	report.updateTestLog("Username", "Eview Username Entered", Status.PASS);
	driver.findElement(By.name("MLPAuLoginPortletFormPassword")).sendKeys("Metlife888");	
	sleep(500);
	report.updateTestLog("Password", "Eview Password Entered", Status.PASS);
	driver.findElement(By.className("submit")).click();
	sleep(500);
	report.updateTestLog("Eview Login", "Submit button Clicked", Status.PASS);
	Actions actions = new Actions(driver);
	WebElement equery = driver.findElement(By.xpath("//strong[text()='eQuery']"));
	actions.moveToElement(equery);
	sleep(500);
	WebElement underwritingPDF = driver.findElement(By.xpath("//ul[@class='fleft']/li/a[text()='Underwriting PDFs']"));
	actions.moveToElement(underwritingPDF);
	actions.click().build().perform();
	report.updateTestLog("Underwriting PDF:", "Underwriting PDF selected", Status.PASS);
	sleep(500);
	Select fund=new Select(driver.findElement(By.name("fundId")));		
	fund.selectByVisibleText(fundname);			
	report.updateTestLog("Fund Name:", "Fund: "+fundname+" is Selected", Status.PASS);
	sleep(250);
	driver.findElement(By.id("ns_Z7_B0SGQO48A8RD30AKB94J6J10G0_fName")).sendKeys(FirstName);
	sleep(500);	
	report.updateTestLog("Firstname:",  FirstName+" is entered", Status.PASS);
	driver.findElement(By.id("ns_Z7_B0SGQO48A8RD30AKB94J6J10G0_sName")).sendKeys(LastName);
	sleep(500);
	report.updateTestLog("LastName:",  LastName+" is entered", Status.PASS);
	driver.findElement(By.xpath("//button[@class='submit']/span")).click();
	sleep(500);
	report.updateTestLog("Search Button:","Search button is Clicked", Status.PASS);
	
	 StringBuilder Campaign  = new StringBuilder("HOST1217WG-");
	 StringBuilder Campaigncode= Campaign.append("1512637410130");
	  String Campaignc = Campaigncode.toString();
	System.out.println("Campaign-->" +Campaignc);
	
	//String CampaignApp = driver.findElement(By.xpath("//*[@id='ns_Z7_B0SGQO48A8RD30AKB94J6J10G0_reportResults']/table/tbody/tr[3]/td[5]")).getText();
	String CampaignApp = driver.findElement(By.xpath("//td[text()='"+Campaignc+"']")).getText();
	System.out.println("Camp-->" +CampaignApp);
	if(CampaignApp.contains(Campaignc)){		
		report.updateTestLog("PDF Verification", "Application is generated in Eview", Status.PASS);
	}else{
		report.updateTestLog("PDF Verification", "Application is generated in Eview", Status.FAIL);
	}
	//HOST1217WG-1512637410130
	 
}catch(Exception e){
	report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
}
	}
}
