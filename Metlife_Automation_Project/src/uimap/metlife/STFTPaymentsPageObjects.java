package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class STFTPaymentsPageObjects extends PageObjectLibrary{
		
	public static final By rddebit = getObjectIdentifaction("stft.payment.debit.radio.css");
	public static final By rdcredit = getObjectIdentifaction("stft.payment.credit.radio.css");
	public static final By txtaccountname = getObjectIdentifaction("stft.payment.accountname.text.id");
	public static final By txtaccountnumber = getObjectIdentifaction("stft.payment.accountnumber.text.id");
	public static final By txtbsb = getObjectIdentifaction("stft.payment.bsb.text.id");	
	public static final By chkacknowledge = getObjectIdentifaction("stft.payment.authorise.checkbox.id");
	public static final By btnsubmit = getObjectIdentifaction("stft.payment.next.button.css");
	public static final By rdvisa = getObjectIdentifaction("stft.payment.visa.radio.css");
	public static final By rdmaster = getObjectIdentifaction("stft.payment.master.radio.css");
	public static final By rdamex = getObjectIdentifaction("stft.payment.amex.radio.css");
	public static final By rddc = getObjectIdentifaction("stft.payment.dc.radio.css");
	public static final By txtccnumber = getObjectIdentifaction("stft.payment.ccnumber.text.id");
	public static final By txtccname = getObjectIdentifaction("stft.payment.ccnname.text.id");
	public static final By selCCExpMonth = getObjectIdentifaction("stft.payment.ccexpmonth.dropdown.css");
	public static final By selCcExpYr = getObjectIdentifaction("stft.payment.ccexpyear.dropdown.css");
	public static final By txtAppnum = getObjectIdentifaction("stft.decision.appnum.text.xpath");
}
