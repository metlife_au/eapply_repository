(function (){

	$( "#date-of-birth" ).datepicker({
		dateFormat: "dd/mm/yy",
		changeMonth: true,
		changeYear: true,
		minDate: "-100Y",
		maxDate: "+0D",
		yearRange: "-100:+0",
		showOn: "both",
		buttonText: "<i class='fa fa-calendar'></i>"
    });


})();




/**
 * Makes "skip to content" link work correctly in IE9, Chrome, and Opera
 * for better accessibility.
 *
 * @link http://www.nczonline.net/blog/2013/01/15/fixing-skip-to-content-links/
 */

( function() {
	var ua = navigator.userAgent.toLowerCase();

	if ( ( ua.indexOf( 'webkit' ) > -1 || ua.indexOf( 'opera' ) > -1 || ua.indexOf( 'msie' ) > -1 ) &&
		document.getElementById && window.addEventListener ) {

		window.addEventListener( 'hashchange', function() {
			var element = document.getElementById( location.hash.substring( 1 ) );

			if ( element ) {
				if ( ! /^(?:a|select|input|button|textarea)$/i.test( element.nodeName ) ) {
					element.tabIndex = -1;
				}

				element.focus();
			}
		}, false );
	}
} )();




$(document).ready(function () {

	// Hide all Timeline list items before displaying them
	$("#showMoreList li").css({"display":"none"});

	// Mobile detect
    var isMobile = window.matchMedia("only screen and (max-width: 760px)");

    if (isMobile.matches) {
        $("#sticky_tabs").stick_in_parent({offset_top: 50});
    } else {
    	//$("#sticky_tabs").stick_in_parent();
    }


	size_li = $("#showMoreList li").size();
	x=3;
	$('#showMoreList li:lt('+x+')').show();

	$('#loadMore').click(function () {

		x= (x+5 <= size_li) ? x+5 : size_li;
		$('#showMoreList li:lt('+x+')').show();

		// Hide Load More button if max items reached
		if (x >= size_li) {
			$('#loadMore').addClass('hide');
		}

	});

	$('#showLess').click(function () {
		x=(x-5<0) ? 3 : x-5;
		$('#showMoreList li').not(':lt('+x+')').hide();
	});



	$('.accordionHeader').on("click", function () {
		var acc_con = $(this).next('.accordionContent');

		if (acc_con.is(':visible')) {
			$(this).removeClass('acc-open');
			acc_con.slideUp(300);
		} else {
			$(this).addClass('acc-open');
			acc_con.slideDown(300);
		}
	});


// browser window scroll (in pixels) after which the "back to top" link is shown
	var btpscroll_offset = 300,
//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
	btpscroll_offset_opacity = 1200,
//duration of the top scrolling animation (in ms)
	btpscroll_scroll_top_duration = 700,
//grab the "back to top" link
	$back_to_top = $('.cd-top');

//hide or show the "back to top" link
	$(window).scroll(function(){
		( $(this).scrollTop() > btpscroll_offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');

		if( $(this).scrollTop() > btpscroll_offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});

//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, btpscroll_scroll_top_duration
		);
	});

});
