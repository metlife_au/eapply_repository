package vicsuper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;

public class VicSuper__Login extends MasterPage {
	static String firstName;
	static String lastName;
    /**
    * @param scriptHelper
    */
    public VicSuper__Login(ScriptHelper scriptHelper) {
          super(scriptHelper);
          // TODO Auto-generated constructor stub
    }

    public VicSuper__Login VicSuper_Login() {
          enterlogindetails();
          return new VicSuper__Login(scriptHelper);
    }
    WebDriverWait wait = new WebDriverWait(driver, 20);
    private void enterlogindetails() {

        String XML = dataTable.getData("VicSuper_Login", "XML");
        String landingpage = dataTable.getData("VicSuper_Login", "TC_ID");
  //    String Browser = dataTable.getData("General_Data", "Browser");
        try{
            if(!landingpage.equalsIgnoreCase("VicSuper_LandingPage")) {

      	  DateFormat dateFormat = new SimpleDateFormat("HHmmssddmmYY");
  			Date date = new Date();
  			String DateTime = dateFormat.format(date);
  			System.out.println("Date---");
  			System.out.println(DateTime);


      	  String[] part;
      	  String before;
			String part2;
			String after;
			String[] value;

			//Unique Client No
			part = XML.split("<clientRefNumber>");
			before = part[0];
			part2 = part[1];
			value = part2.split("</clientRefNumber>");
			String ClientNo = value[0];
			after = value[1];

			ClientNo = "CRN"+DateTime;

			XML = before+"<clientRefNumber>"+ClientNo+"</clientRefNumber>"+after;

			part = XML.split("<firstName>");
			before = part[0];
			part2 = part[1];
			value = part2.split("</firstName>");
			firstName = value[0];
			after = value[1];

			firstName = "FN"+DateTime;

			XML = before+"<firstName>"+firstName+"</firstName>"+after;

			part = XML.split("<lastName>");
			before = part[0];
			part2 = part[1];
			value = part2.split("</lastName>");
			lastName = value[0];
			after = value[1];

			lastName = "LN"+DateTime;

			XML = before+"<lastName>"+lastName+"</lastName>"+after;
		///COPY PASTE XML
      /*	StringSelection stringSelection = new StringSelection(XML);
      	Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);

		   //native key strokes for CTRL, V and ENTER keys
          Robot robot = new Robot();
          wait.until(ExpectedConditions.elementToBeClickable(By.id("input_string_id"))).click();
          robot.keyPress(KeyEvent.VK_CONTROL);
          robot.keyPress(KeyEvent.VK_V);
          robot.keyRelease(KeyEvent.VK_V);
          robot.keyRelease(KeyEvent.VK_CONTROL);
          sleep(1000);*/


              //******Input the XML String******\\


             wait.until(ExpectedConditions.elementToBeClickable(By.id("input_string_id"))).sendKeys(XML);
             report.updateTestLog("XML String", "XML String is entered", Status.PASS);
            }else {
          	  wait.until(ExpectedConditions.elementToBeClickable(By.id("input_string_id"))).sendKeys(XML);
          	  report.updateTestLog("XML String", "XML String is entered", Status.PASS);
        /*  	///COPY PASTE XML
            	StringSelection stringSelection = new StringSelection(XML);
            	Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);

    		   //native key strokes for CTRL, V and ENTER keys
                Robot robot = new Robot();
                wait.until(ExpectedConditions.elementToBeClickable(By.id("input_string_id"))).click();
                robot.keyPress(KeyEvent.VK_CONTROL);
                robot.keyPress(KeyEvent.VK_V);
                robot.keyRelease(KeyEvent.VK_V);
                robot.keyRelease(KeyEvent.VK_CONTROL);
                sleep(1000);
                report.updateTestLog("XML String", "XML String is entered", Status.PASS);*/
            }
       /*
      	  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'PROCEED TO NEXT PAGE')]"))).click();
            report.updateTestLog("Proceed To Next Page", "Proceed To Next Page button is Clicked", Status.PASS);*/






				//****Proceed to Next page button is Clicked*****\\

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'PROCEED TO NEXT PAGE')]"))).click();
              report.updateTestLog("Proceed To Next Page", "Proceed To Next Page button is Clicked", Status.PASS);


              //****Web response button click*****\\


              String inputXML = wait.until(ExpectedConditions.elementToBeClickable(By.id("input_string_id"))).getText();
              if ( (inputXML.trim().length()<7) || (inputXML.equalsIgnoreCase("null")) ) {
              	report.updateTestLog("Input XML", "Invalid InputXML - null response", Status.FAIL);

				}




              sleep(500);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Rwd Application')]"))).click();
              report.updateTestLog("RWD Application", "RWD Application button is Clicked", Status.PASS);
        sleep(3500);
        quickSwitchWindows();
        sleep(8000);
          report.updateTestLog("HOSTPLUS Login", "Sucessfully Logged into the application", Status.PASS);
          }catch(Exception e){
                report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
          }
    }


}
