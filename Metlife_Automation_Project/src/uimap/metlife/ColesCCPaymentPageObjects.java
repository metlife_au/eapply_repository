package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ColesCCPaymentPageObjects extends PageObjectLibrary {
	public static final By rdCreditCard = getObjectIdentifaction("coles.cc.payment.creditcard.radio.css");
	public static final By rdBankAccount = getObjectIdentifaction("coles.cc.payment.bankaccount.radio.css");
	public static final By txtAccountName = getObjectIdentifaction("coles.cc.payment.cc.accountname.txt.id");
	public static final By txtAccountNumber = getObjectIdentifaction("coles.cc.payment.cc.accountnumber.txt.id");
	public static final By txtBsbCode = getObjectIdentifaction("coles.cc.payment.cc.bsbcode.txt.id");
	public static final By txtBsbNumber = getObjectIdentifaction("coles.cc.payment.cc.bsbno.txt.id");
	public static final By rdVisaCard = getObjectIdentifaction("coles.cc.payment.visacardtype.radio.css");
	public static final By rdMasterCard = getObjectIdentifaction("coles.cc.payment.mastercardtype.radio.css");
	public static final By txtCCNumber = getObjectIdentifaction("coles.cc.payment.cc.ccnumber.text.css");
	public static final By txtCCHoldername = getObjectIdentifaction("coles.cc.payment.cc.ccholdername.text.css");
	public static final By SelCCExpMonth = getObjectIdentifaction("coles.cc.payment.cc.ccexpmonth.dropdown.css");
	public static final By SelCCExpYear = getObjectIdentifaction("coles.cc.payment.cc.ccexpyear.dropdown.css");
	public static final By chkAuthorise = getObjectIdentifaction("coles.cc.payment.authorise.checkbox.id");
	public static final By btnSubmit = getObjectIdentifaction("coles.cc.payment.submit.button.css");
	public static final By txtAreaOutput = getObjectIdentifaction("coles.cc.output.textarea.css");
}
