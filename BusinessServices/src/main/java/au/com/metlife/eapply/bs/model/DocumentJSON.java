package au.com.metlife.eapply.bs.model;

public class DocumentJSON {
	
	private String name;
	
	private String fileLocations;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private String docType;
	
	private boolean status;

	

	public String getFileLocations() {
		return fileLocations;
	}

	public void setFileLocations(String fileLocations) {
		this.fileLocations = fileLocations;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	

}
