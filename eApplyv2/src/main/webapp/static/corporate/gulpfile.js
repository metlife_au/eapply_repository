var gulp = require('gulp');
var del = require('del');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var ngAnnotate = require('gulp-ng-annotate');
var jshint = require('gulp-jshint');
var gulpDocs = require('gulp-ngdocs');
var connect = require('gulp-connect');

gulp.task('clean', function (cb) {
  return del([
    // everything inside the dist folder
    './dist/**/*'
  ], cb);
});

gulp.task('lint', function() {
  return gulp.src([
    'src/app/app.module.config.js',
    'src/app/app.module.js',
    'src/app/app.module.routes.js',
    'src/app/appDataModel.js',
    'src/app/filters.js',
    'src/app/storagehandler.js',
    'src/app/validation.js',
    'src/app/**/*.js'
  ])
    .pipe(jshint())
    .pipe(jshint.reporter('default', { verbose: true }))
    .pipe(jshint.reporter('fail'));
});

gulp.task('lib-script-minify', function () {
  gulp.src([
    '../../resources/js/lib/jQuery/jquery.3.2.1.min.js',
    '../../resources/js/lib/jQuery/jquery-ui.min.js',
    '../../resources/js/lib/bootstrap/bootstrap-4.0.0-beta.bundle.min.js',
    '../../resources/js/lib/utils/moment.js',
    '../../resources/js/lib/angular.js',
    '../../resources/js/lib/angular-idle.min.js',
    '../../resources/js/lib/angular-ui-router.min.js',
    '../../resources/js/lib/ngDialog.js',
    '../../static/common/services/eApplyInterceptor.js'
  ])
    .pipe(sourcemaps.init())
    .pipe(concat('lib.js'))
    //.pipe(ngAnnotate())
    //.pipe(uglify({output: {ascii_only:true}}))
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist'));
});

gulp.task('app-script-minify', function () {
  gulp.src([
    'src/app/app.module.config.js',
    'src/app/app.module.js',
    'src/app/app.module.routes.js',
    'src/app/appDataModel.js',
    'src/app/**/*.js'])
    .pipe(sourcemaps.init())
    .pipe(concat('app.js'))
    .pipe(ngAnnotate())
    .pipe(uglify({output: {ascii_only:true}}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist'));
});

gulp.task('ngdocs', [], function () {
  return gulp.src(['src/app/app.module.config.js', 'src/app/app.module.js', 'src/app/app.module.routes.js', 'src/app/**/*.js'])
    .pipe(gulpDocs.process())
    .pipe(gulp.dest('./docs'));
});

gulp.task('viewdoc', function() {
  connect.server({
    root: './docs',
    livereload: false,
    fallback: 'docs/index.html',
    port: 8083
  });
});

gulp.task('build', ['clean', 'lint', 'lib-script-minify', 'app-script-minify', 'ngdocs'], function () {});