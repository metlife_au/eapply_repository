package au.com.metlife.eapply.bs.constants;

public interface EapplicationPdfConstants {
	
	// PDF Type
    String UNDER_WRITTING = "UW";
    //  PDF Type Label
    String UNDER_WRITTING_LABEL = "##UW##";
    // Partner Types
    String PARTNER_MILLI = "MILLI";
    
    String PARTNER_OAMPS = "OAMPS";
    String PARTNER_ALI = "ALI";
    String PARTNER_YBR = "YBR";
    String PARTNER_STFT = "STFT";
    // Applicant TYPE
    String APPLICANT_TYPE_PRIMARY = "primary";
    
    // Line of Business
    String LOB_INDIVIDUAL = "Individual";
    String LOB_INSTITUTIONAL = "Institutional";
    
    // Colors
    String COLOR_PURPLE = "purple";
    
    // Cover Codes
    String COVER_CODE_KIDS = "202";
    
    String COVER_CODE_DEATH = "199";
    
    String COVER_CODE_DOWN_SALE = "201";
    
    // Decision
    String DECISION_DCL = "DCL";
    
    String APPLICANT_CHILD = "child";
    String APPLICANT_SECONDARY= "secondary";
    
    String APPLICANT_PRIMARY = "primary";
    
    String DECISION_RUW = "RUW";
    
    // Aura Question Type
    String QG_DEFAULT = "";
    
    String QG_KIDS_COVER = "QG=Kids Cover";
    
    String QG_KIDS_COVER_OPTION = "QG=Kids cover option";
    
    String QG_GENERAL_QUESTIONS = "QG=General Questions";
    
    String QG_PERSONAL_DETAILS = "QG=Personal Details";
    
    String QG_ELIGIBILITY_CHECK = "QG=Eligibility Check";
    
    String QG_YOUR_PERSONAL_DETAILS = "100000";
    
    String QG_TRANSFER_DETAILS = "100009";
    
    String QG_WORK_RATING_DETAILS = "100011";
    
    String QG_LIFE_EVENT_DETAILS = "100010";
    
    String QG_OCCUPATION_DETAILS = "QG=Occupation Details";
    
    String QG_YOUR_OCCUPATION_DETAILS = "100001";
    
    String QG_FAMILY_HISTORY = "QG=Family History";
    
    String QG_YOUR_FAMILY_HISTORY = "100004";
    
    String QG_EMPLOYMENT_DETAILS = "QG=Your employment";
    
    String QG_YOUR_EMPLOYMENT_DETAILS = "100001";
    
    String QG_HEALTH_QUESTIONS = "QG=Health Questions";
    
    String QG_YOUR_HEALTH_QUESTIONS = "100002";
    
    String QG_TRANSFER_QUESTIONS = "QG=Transfer Questions";
    
    String QG_YOUR_MEDICAL_QUESTIONS = "100006";
    
    String QG_LIFESTYLE_QUESTIONS = "QG=Lifestyle Questions";
    
    String QG_YOUR_LIFESTYLE_QUESTIONS = "100005";
    
    String QG_INSURANCE_DETAILS = "QG=Insurance Details";
    
    String QG_INSURANCE_HISTORY = "QG=Insurance History";
    
    String QG_YOUR_INSURANCE_HISTORY = "100007";
    
    String QG_PREGNANCY = "QG=Pregnancy";    
    
    String QG_YOUR_PREGNANCY = "100003";   
    
    // Labels
    String FAMILY_DISCOUNT_LABEL = "Family Discount:";
    
    String MULTICOVER_DISCOUNT_LABEL = "Multicover Discount:";
    
    String SPECIAL_CONDITION_SECTION_LABEL = "Special Condition(s):";
    
    // Messages
    String COVER_DECLINE_TEXT = "Sorry, this cover has been declined based on your answers to the health and lifestyle questionnaire.";
    
    String FAMILY_DISCOUNT_TEXT = "Family discount applied.";
    
    String MEMBER_DISCOUNT_TEXT = "Member discount applied.";
    
    String APPLIED_LOADING_CONTACT_TEXT = "These loadings take into account your health and lifestyle. Please call us on 1300 392 679 if you'd like to know more.";
    
    String APPLIED_EXCLUSION_CONTACT_TEXT = "If you'd like to know more about this, Please call us on 1300 392 679.";
    
    String APPLIED_TEXT = " applied";

    String MULTI_COVER_DISCOUNT_TEXT = " multi-cover discount of $";
    
    String TEXT_EXCLUSION_LABEL = "";
    
    String TEXT_LOADING_LABEL = "";
    
    String STR_YOUR = "Your ";
    String LOADING_AMOUNT_TXT = " premium includes a loading of $";
    
    
    String PERCENTAGE_LOADING1 = "Loadings of ";
    
    String PERCENTAGE_LOADING2 = "apply to total cover :";
    
    // Frequency Type
    String FREQUENCY_TYPE_DEFAULT = "Monthly";
    
    // Discount Types
    String DISCOUNT_FAMILY = "Family";
    String DISCOUNT_MULTICOVER = "Multicover";
    
}
