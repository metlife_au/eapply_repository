/**
 * 
 */
package au.com.metlife.eapply.quote.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 199306
 *
 */
public class RuleModel implements Serializable{
	
	private int age;
	
	private String fundCode;
	
	private String gender;
	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	private String deathOccCategory;
	
	private String tpdOccCategory;
	
	private String ipOccCategory;
	
	private boolean smoker;
	
	private int deathUnits;
	
	private BigDecimal deathFixedAmount;
	
	private BigDecimal deathFixedCost;
	
	private BigDecimal deathUnitsCost;
	
	private int tpdUnits;
	
	private BigDecimal tpdFixedAmount;
	
	private BigDecimal tpdFixedCost;
	
	private BigDecimal tpdUnitsCost;
	
	public BigDecimal getTpdFixedAmount() {
		return tpdFixedAmount;
	}

	public void setTpdFixedAmount(BigDecimal tpdFixedAmount) {
		this.tpdFixedAmount = tpdFixedAmount;
	}

	public BigDecimal getTpdFixedCost() {
		return tpdFixedCost;
	}

	public void setTpdFixedCost(BigDecimal tpdFixedCost) {
		this.tpdFixedCost = tpdFixedCost;
	}

	public BigDecimal getTpdUnitsCost() {
		return tpdUnitsCost;
	}

	public void setTpdUnitsCost(BigDecimal tpdUnitsCost) {
		this.tpdUnitsCost = tpdUnitsCost;
	}

	private int ipUnits;
	
	private BigDecimal ipFixedAmount;
	
	private BigDecimal ipFixedCost;
	
	private BigDecimal ipUnitsCost;
	
	private String premiumFrequency;
	
	private String memberType;
	
	private String deathCoverType;
	
	private String tpdCoverType;
	
	private String ipCoverType;
	
	private String manageType;
	
	private String ipBenefitPeriod;
	
	private String ipWaitingPeriod;
	
	private BigDecimal deathTransferAmount;
	
	private BigDecimal tpdTransferAmount;
	
	private BigDecimal ipTransferAmount;
	
	private BigDecimal deathExistingAmount;
	
	private BigDecimal tpdExistingAmount;
	
	private BigDecimal ipExistingAmount;
	
	private String exDeathCoverType;
	
	private String exTpdCoverType;
	
	private String exIpCoverType;
	
	
	public String getExDeathCoverType() {
		return exDeathCoverType;
	}

	public void setExDeathCoverType(String exDeathCoverType) {
		this.exDeathCoverType = exDeathCoverType;
	}

	public String getExTpdCoverType() {
		return exTpdCoverType;
	}

	public void setExTpdCoverType(String exTpdCoverType) {
		this.exTpdCoverType = exTpdCoverType;
	}

	public String getExIpCoverType() {
		return exIpCoverType;
	}

	public void setExIpCoverType(String exIpCoverType) {
		this.exIpCoverType = exIpCoverType;
	}

	public BigDecimal getDeathExistingAmount() {
		return deathExistingAmount;
	}

	public void setDeathExistingAmount(BigDecimal deathExistingAmount) {
		this.deathExistingAmount = deathExistingAmount;
	}

	public BigDecimal getTpdExistingAmount() {
		return tpdExistingAmount;
	}

	public void setTpdExistingAmount(BigDecimal tpdExistingAmount) {
		this.tpdExistingAmount = tpdExistingAmount;
	}

	public BigDecimal getIpExistingAmount() {
		return ipExistingAmount;
	}

	public void setIpExistingAmount(BigDecimal ipExistingAmount) {
		this.ipExistingAmount = ipExistingAmount;
	}

	public BigDecimal getDeathTransferAmount() {
		return deathTransferAmount;
	}

	public void setDeathTransferAmount(BigDecimal deathTransferAmount) {
		this.deathTransferAmount = deathTransferAmount;
	}

	public BigDecimal getTpdTransferAmount() {
		return tpdTransferAmount;
	}

	public void setTpdTransferAmount(BigDecimal tpdTransferAmount) {
		this.tpdTransferAmount = tpdTransferAmount;
	}

	public BigDecimal getIpTransferAmount() {
		return ipTransferAmount;
	}

	public void setIpTransferAmount(BigDecimal ipTransferAmount) {
		this.ipTransferAmount = ipTransferAmount;
	}

	public String getIpBenefitPeriod() {
		return ipBenefitPeriod;
	}

	public void setIpBenefitPeriod(String ipBenefitPeriod) {
		this.ipBenefitPeriod = ipBenefitPeriod;
	}

	public String getIpWaitingPeriod() {
		return ipWaitingPeriod;
	}

	public void setIpWaitingPeriod(String ipWaitingPeriod) {
		this.ipWaitingPeriod = ipWaitingPeriod;
	}

	public String getManageType() {
		return manageType;
	}

	public void setManageType(String manageType) {
		this.manageType = manageType;
	}

	public String getDeathCoverType() {
		return deathCoverType;
	}

	public void setDeathCoverType(String deathCoverType) {
		this.deathCoverType = deathCoverType;
	}

	public String getTpdCoverType() {
		return tpdCoverType;
	}

	public void setTpdCoverType(String tpdCoverType) {
		this.tpdCoverType = tpdCoverType;
	}

	public String getIpCoverType() {
		return ipCoverType;
	}

	public void setIpCoverType(String ipCoverType) {
		this.ipCoverType = ipCoverType;
	}

	public String getDeathOccCategory() {
		return deathOccCategory;
	}

	public void setDeathOccCategory(String deathOccCategory) {
		this.deathOccCategory = deathOccCategory;
	}

	public String getTpdOccCategory() {
		return tpdOccCategory;
	}

	public void setTpdOccCategory(String tpdOccCategory) {
		this.tpdOccCategory = tpdOccCategory;
	}

	public String getIpOccCategory() {
		return ipOccCategory;
	}

	public void setIpOccCategory(String ipOccCategory) {
		this.ipOccCategory = ipOccCategory;
	}

	public boolean isSmoker() {
		return smoker;
	}

	public void setSmoker(boolean smoker) {
		this.smoker = smoker;
	}

	public int getDeathUnits() {
		return deathUnits;
	}

	public void setDeathUnits(int deathUnits) {
		this.deathUnits = deathUnits;
	}

	public BigDecimal getDeathFixedAmount() {
		return deathFixedAmount;
	}

	public void setDeathFixedAmount(BigDecimal deathFixedAmount) {
		this.deathFixedAmount = deathFixedAmount;
	}

	public BigDecimal getDeathFixedCost() {
		return deathFixedCost;
	}

	public void setDeathFixedCost(BigDecimal deathFixedCost) {
		this.deathFixedCost = deathFixedCost;
	}

	public BigDecimal getDeathUnitsCost() {
		return deathUnitsCost;
	}

	public void setDeathUnitsCost(BigDecimal deathUnitsCost) {
		this.deathUnitsCost = deathUnitsCost;
	}

	public int getTpdUnits() {
		return tpdUnits;
	}

	public void setTpdUnits(int tpdUnits) {
		this.tpdUnits = tpdUnits;
	}

	public int getIpUnits() {
		return ipUnits;
	}

	public void setIpUnits(int ipUnits) {
		this.ipUnits = ipUnits;
	}

	public BigDecimal getIpFixedAmount() {
		return ipFixedAmount;
	}

	public void setIpFixedAmount(BigDecimal ipFixedAmount) {
		this.ipFixedAmount = ipFixedAmount;
	}

	public BigDecimal getIpFixedCost() {
		return ipFixedCost;
	}

	public void setIpFixedCost(BigDecimal ipFixedCost) {
		this.ipFixedCost = ipFixedCost;
	}

	public BigDecimal getIpUnitsCost() {
		return ipUnitsCost;
	}

	public void setIpUnitsCost(BigDecimal ipUnitsCost) {
		this.ipUnitsCost = ipUnitsCost;
	}

	public String getPremiumFrequency() {
		return premiumFrequency;
	}

	public void setPremiumFrequency(String premiumFrequency) {
		this.premiumFrequency = premiumFrequency;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getFundCode() {
		return fundCode;
	}

	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}
}
