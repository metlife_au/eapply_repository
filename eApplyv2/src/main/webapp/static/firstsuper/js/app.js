// create the module and name it CareSuperApp
// also include ngRoute for all our routing needs
var FirstSuperApp = angular.module('eapplymain', ['ngRoute','ngResource','ngFileUpload','ngDialog', 'sessionTimeOut', 'globalExceptionhandler', 'restAPI', 'appDataModel', 'eApplyInterceptor']);
// configure our routes
FirstSuperApp.config(function($routeProvider,$httpProvider, IdleProvider, KeepaliveProvider, $templateRequestProvider) {
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.interceptors.push('eapplyrouter');
	$templateRequestProvider.httpOptions({_isTemplate: true});

    $routeProvider
	    .when('/', {
        templateUrl : 'static/firstsuper/landingpage.html',
        controller  : 'login',
  			reloadOnSearch: false,
        resolve: {
         urls:function(fetchUrlSvc){
           var path = 'CareSuperUrls.properties';
           return fetchUrlSvc.getUrls(path);
         }
        }
	    })
	    .when('/landing', {
	        templateUrl : 'static/firstsuper/landingpage.html',
	        controller  : 'login',
	        resolve: {
	    	   urls:function(fetchUrlSvc) {
             		var path = 'CareSuperUrls.properties';
	    		   	return fetchUrlSvc.getUrls(path);
	    	   }
	        }
	    })
	    .when('/quote', {
	        templateUrl : 'static/firstsuper/coverDetails_changeCover.html',
	        controller  : 'quote'
	    })
	    
	    //nonmember
	    
	    .when('/nonmember/:mode', {
	        templateUrl : 'static/firstsuper/coverDetails_NonMember.html',
	        controller  : 'nonmember',
	        	resolve: {
	    			urls:function(fetchUrlSvc){
	    				var path = 'CareSuperUrls.properties';
	    				return fetchUrlSvc.getUrls(path);
	    			} 
	           	}
	    })
	    
	    .when('/newmemberquote/:mode', {
	      templateUrl : 'static/firstsuper/newMember_changeCover.html',
	       controller  : 'newmemberquote'
	    })
	   .when('/quotecancel/:mode', {
	        templateUrl : 'static/firstsuper/coverDetails_cancelCover.html',
	        controller  : 'quotecancel'
	    })
	    .when('/retrievesavedapplication', {
	    	templateUrl : 'static/firstsuper/retrive_applications.html',
	    	controller  : 'retrievesavedapplication'
	    })
	    .when('/retrievesavedapplicationNonMem/:mode', {
	    	templateUrl : 'static/firstsuper/retrive_applications.html',
	    	controller  : 'retrievesavedapplication',
        	resolve: {
    			urls:function(fetchUrlSvc){
    				var path = 'CareSuperUrls.properties';
    				return fetchUrlSvc.getUrls(path);
    			} 
           	}
	    })
	     .when('/quotetransfer/:mode', {
	    	 templateUrl : 'static/firstsuper/coverDetails_transfer.html',
	        controller  :  'quotetransfer'
	    })
	     .when('/quoteoccchange/:mode', {
	        templateUrl : 'static/firstsuper/coverDetails_updateDetails.html',
	        controller  : 'quoteoccupdate'
	    })

	    /*Special cover*/
	    .when('/quotespecial/:mode', {
	        templateUrl : 'static/firstsuper/coverDetails_specialCover.html',
	        controller  : 'quotespecial'
	    })
	    .when('/auraspecial/:mode', {
	        templateUrl : 'static/firstsuper/aura_special_offer.html',
	        controller  : 'auraspecialoffer'
	    })
	    .when('/specialoffersummary/:mode', {
	        templateUrl : 'static/firstsuper/special_offer_confirmation.html',
	        controller  : 'specialoffersummary'
	    })
	     .when('/specialofferaccept', {
	        templateUrl : 'static/firstsuper/special_decision_accepted.html',
	        controller  : 'specialOfferAcceptCtlr'
	    })
	     .when('/specialofferdecline', {
	        templateUrl : 'static/firstsuper/special_decision_decline.html',
	        controller  : 'specialOfferDeclineCtlr'
	    })
	    /*Special cover*/
	     .when('/lifeevent/:mode', {
	        templateUrl : 'static/firstsuper/coverDetails_LifeEvents.html',
	        controller  : 'lifeevent'
	     })
	     .when('/auralifeevent/:mode', {
	        templateUrl : 'static/firstsuper/aura_lifeEvent.html',
	        controller  : 'auraLifeEvent'
	     })
	     .when('/lifeeventaccept', {
	        templateUrl : 'static/firstsuper/lifeEvents_desicion_accepted.html',
	        controller  : 'lifeEventAcceptController'
	    })
	    .when('/lifeeventdecline', {
	        templateUrl : 'static/firstsuper/lifeEvents_decision_decline.html',
	        controller  : 'lifeEventDeclineController'
	    })
	    .when('/lifeeventunderwriting', {
	        templateUrl : 'static/firstsuper/lifeEvents_decision_ruw.html',
	        controller  : 'lifeEventRUWController'
	    })
	     .when('/auratransfer/:mode', {
	    	 templateUrl : 'static/firstsuper/aura_transferCover.html',
	        controller  : 'auratransfer'
	    })
	    .when('/auraocc/:mode', {
	    	 templateUrl : 'static/firstsuper/aura_updateDetails.html',
	        controller  : 'auraocc'
	    })
	    .when('/auraspecialoffer/:mode', {
	    	 templateUrl : 'static/firstsuper/aura_special_offer.html',
	        controller  : 'auraspecialoffer'
	    })
	    .when('/aura', {
	        templateUrl : 'static/firstsuper/aura.html',
	        controller  : 'aura'
	    })
	    .when('/auracancel/:mode', {
	        templateUrl : 'static/firstsuper/aura_cancelCover.html',
	        controller  : 'auracancel'
	    })
	    .when('/summary', {
	        templateUrl : 'static/firstsuper/confirmation.html',
	        controller  : 'summary'
	    })
	    .when('/changeaccept', {
	        templateUrl : 'static/firstsuper/changeCover_decision_accepted.html',
	        controller  : 'changeCoverAcceptController'
	    })
	    .when('/changedecline', {
	        templateUrl : 'static/firstsuper/changeCover_decision_decline.html',
	        controller  : 'changeCoverDeclineController'
	    })
	    .when('/changeunderwriting', {
	        templateUrl : 'static/firstsuper/changeCover_decision_ruw.html',
	        controller  : 'changeCoverRUWController'
	    })
	    .when('/changeaspcltermsacc', {
	        templateUrl : 'static/firstsuper/changeCover_decision_AcceptWithSpclTerms.html',
	        controller  : 'changeCoverACCSpclTermsController'
	    })
	    .when('/changemixedaccept', {
	        templateUrl : 'static/firstsuper/changeCover_decision_MixedAcceptance.html',
	        controller  : 'changeCoverMixedACCController'
	    })
	     .when('/requirements/:clm', {
	        templateUrl : 'claimtracker/events.html',
	        controller  : 'requirements'
	    })
	    .when('/newsummary', {
	        templateUrl : 'static/firstsuper/newMember_confirmation.html',
	        controller  : 'newsummary'
	    })
	    .when('/workRatingSummary/:mode', {
	        templateUrl : 'static/firstsuper/workRating_confirmation.html',
	        controller  : 'workRatingSummary'
	    })

	    .when('/workRatingAccept', {
	        templateUrl : 'static/firstsuper/workRating_decision_accepted.html',
	        controller  : 'workRatingAcceptController'
	    })

	    .when('/workRatingDecline', {
	        templateUrl : 'static/firstsuper/workRating_decision_decline.html',
	        controller  : 'workRatingDeclineController'
	    })

	    .when('/workRatingMaintain', {
	        templateUrl : 'static/firstsuper/workRating_decision_maintained.html',
	        controller  : 'workRatingMaintainController'
	    })

	    .when('/transferSummary/:mode', {
	        templateUrl : 'static/firstsuper/transfer_confirmation.html',
	        controller  : 'transferSummary'
	    })

	    .when('/transferAccept', {
	        templateUrl : 'static/firstsuper/transfer_decision_accepted.html',
	        controller  : 'transferAcceptController'
	    })

	    .when('/transferDecline', {
	        templateUrl : 'static/firstsuper/transfer_decision_decline.html',
	        controller  : 'transferDeclineController'
	    })
	    .when('/newmemberaccept', {
	        templateUrl : 'static/firstsuper/newMember_decision_accepted.html',
	        controller  : 'newMemberAcceptController'
	    })
	    .when('/newmemberdecline', {
	        templateUrl : 'static/firstsuper/newMember_decision_decline.html',
	        controller  : 'newMemberDeclineController'
	    })
	    .when('/cancelConfirmation', {
	        templateUrl : 'static/firstsuper/cancel_confirmation.html',
	        controller  : 'cancelConfirmController'
	    })
	    .when('/cancelDecision', {
	        templateUrl : 'static/firstsuper/cancel_decision.html',
	        controller  : 'cancelController'
	    })
	    .when('/sessionTimeOut', {
	        templateUrl : 'static/firstsuper/timeout.html',
	        controller  : 'timeOutController'
	    })
	    .when('/cancelDecisionRuw', {
	        templateUrl : 'static/firstsuper/cancel_decision_ruw.html',
	        controller  : 'cancelRuwController'
	    })
	    .when('/cancelDecisionDecline', {
	        templateUrl : 'static/firstsuper/cancel_decision_decline.html',
	        controller  : 'cancelDecisionDeclineController'
	    })
	    .when('/lifeeventsummary/:mode', {
	        templateUrl : 'static/firstsuper/confirmation_LifeEvents.html',
	        controller  : 'lifeeventsummary'
	    })
	    .when('/convertMaintain/:mode', {
	        templateUrl : 'static/firstsuper/coverDetails_convertCover.html',
	        controller  : 'quoteConvert'
	    })
	    .when('/convertSummary', {
	        templateUrl : 'static/firstsuper/convert_confirmation.html',
	        controller  : 'convertSummary'
	    })

	     .when('/convertDecision', {
	        templateUrl : 'static/firstsuper/convertMaintain_decision_accepted.html',
	        controller  : 'convertAcceptController'
	    })
	    
	    //nonmember
	    .when('/auraNonMember/:mode', {
	        templateUrl : 'static/firstsuper/auraNonMember.html',
	        controller  : 'auraNonMember'
	    })
	    .when('/nonMemberSummary/:mode', {
            templateUrl : 'static/firstsuper/nonMember_confirmation.html',
            controller  : 'nonMemberSummary'
        })
        .when('/nonMemberaccept', {
	        templateUrl : 'static/firstsuper/nonMember_decision_accepted.html',
	        controller  : 'nonMemberAcceptController'
	    })
	    .when('/nonMemberdecline', {
	        templateUrl : 'static/firstsuper/nonMember_decision_decline.html',
	        controller  : 'nonMemberDeclineController'
	    })
	    .when('/nonMemberunderwriting', {
	        templateUrl : 'static/firstsuper/nonMember_decision_ruw.html',
	        controller  : 'nonMemberRUWController'
	    })
	    .when('/nonMemberaspcltermsacc', {
	        templateUrl : 'static/firstsuper/nonMember_decision_AcceptWithSpclTerms.html',
	        controller  : 'nonMemberACCSpclTermsController'
	    })
	    .when('/nonMembermixedaccept', {
	        templateUrl : 'static/firstsuper/nonMember_decision_MixedAcceptance.html',
	        controller  : 'nonMemberMixedACCController'
	    })

});

FirstSuperApp.run( function($location) {
	if( memberStatus == "newApp") {
		$location.path('/nonmember/1');
		
	}
	if( memberStatus == "savedApp") {
		$location.path('/retrievesavedapplicationNonMem/3');
		
	}
});
FirstSuperApp.constant('APP_CONSTANTS', function () {
	return {
		'emailFormat': /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
	}
});
FirstSuperApp.run(function($rootScope, $window) {
	$rootScope.logout = function() {
      $window.close();
    }
});
