package au.com.metlife.eapply.bs.model;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
public interface  DocumentEappRepository extends JpaRepository<Document, Long>{
	
	public List<Document> findByEapplicationid(Long eapplicationid);  
	@Transactional
	@Modifying
	@Query("delete  FROM Document doc  where doc.eapplicationid = ?1 ")
    public void deleteAllByEapplicationID(Long eapplicationid);
	
}