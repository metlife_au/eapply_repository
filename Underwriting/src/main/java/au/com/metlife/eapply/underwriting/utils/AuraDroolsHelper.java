package au.com.metlife.eapply.underwriting.utils;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.DecisionTableConfiguration;
import org.drools.builder.DecisionTableInputType;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatelessKnowledgeSession;
import au.com.metlife.eapply.underwriting.model.InstProductRuleBO;
import au.com.metlife.eapply.underwriting.model.PostProcessRuleBO;
import au.com.metlife.eapply.underwriting.model.RefProductAuraQuestionFilterBO;

public class AuraDroolsHelper {
	
	
	public static RefProductAuraQuestionFilterBO invokeRuleProductAuraQuestMapping (RefProductAuraQuestionFilterBO refProdAuraQustFilter, String ruleFileLocation) {
		KnowledgeBase knowledgeBase = null;
		StatelessKnowledgeSession knowledgeSession = null;
		try {
			ruleFileLocation=ruleFileLocation+refProdAuraQustFilter.getRuleFileName();
			
			if (null != refProdAuraQustFilter && null != refProdAuraQustFilter.getKbase()) {
				knowledgeBase = refProdAuraQustFilter.getKbase();
				knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
			} else {
				knowledgeBase = readKnowledgeBase(ruleFileLocation);
				knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
			}
			knowledgeSession.execute(refProdAuraQustFilter);
		} catch (Throwable throwable) {
			
		}
		return refProdAuraQustFilter;
	}
	
	public static PostProcessRuleBO invokepostProcessRule (PostProcessRuleBO postProcessRuleBO, String ruleFileLocation) {
		KnowledgeBase knowledgeBase = null;
		StatelessKnowledgeSession knowledgeSession = null;
		try {
			ruleFileLocation=ruleFileLocation+postProcessRuleBO.getRuleFileName();
			
			if (null != postProcessRuleBO && null != postProcessRuleBO.getKbase()) {
				knowledgeBase = postProcessRuleBO.getKbase();
				knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
			} else {
				knowledgeBase = readKnowledgeBase(ruleFileLocation);
				knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
			}
			knowledgeSession.execute(postProcessRuleBO);
		} catch (Throwable throwable) {
			
		}
		return postProcessRuleBO;
	}
	
	public static InstProductRuleBO invokeInstProductBusinessRule(InstProductRuleBO instProductRuleBO, String ruleFileLocation){
		KnowledgeBase kbase=null;
		StatelessKnowledgeSession ksession=null;
		try {
			ruleFileLocation=ruleFileLocation+instProductRuleBO.getRuleFileName();
			if(instProductRuleBO.getKbase()!=null){
				kbase=instProductRuleBO.getKbase();
				ksession = kbase.newStatelessKnowledgeSession();
			}else{
				kbase = readKnowledgeBase(ruleFileLocation);
				ksession = kbase.newStatelessKnowledgeSession();
			}
			if(instProductRuleBO!=null){
				ksession.execute(instProductRuleBO);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(ksession!=null){
				ksession=null;
			}
		}
		return instProductRuleBO;
	}
	
	public static KnowledgeBase getRuleSession(String ruleFileLoc,String ruleFileName){
		KnowledgeBase kbase=null;
		try {
			ruleFileLoc=ruleFileLoc+ruleFileName;
			kbase = readKnowledgeBase(ruleFileLoc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return kbase;
	}

	/**
	 * This method will read the rules from the sample.xls file.
	 * Each row in the excel sheet will represent as a business rule.
	 *
	 * @return KnowledgeBase object
	 * @throws Exception
	 */

	private static KnowledgeBase readKnowledgeBase(String ruleFileLoc) throws Exception {
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
		DecisionTableConfiguration config = KnowledgeBuilderFactory.newDecisionTableConfiguration();
		config.setInputType(DecisionTableInputType.XLS);
		kbuilder.add(ResourceFactory.newFileResource(ruleFileLoc), ResourceType.DTABLE, config);
		KnowledgeBuilderErrors errors = kbuilder.getErrors();
		if (errors.size() > 0) {
			for (KnowledgeBuilderError error: errors) {
				System.err.println(error);
			}
			throw new IllegalArgumentException("Could not parse knowledge.");
		}
		KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
		kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());
		return kbase;
	}

}
