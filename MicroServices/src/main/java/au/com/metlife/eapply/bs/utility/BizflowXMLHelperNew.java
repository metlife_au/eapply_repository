package au.com.metlife.eapply.bs.utility;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.xml.bind.JAXBContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.metlife.eapply.bs.bizflowmodel.Address;
import au.com.metlife.eapply.bs.bizflowmodel.AddressRole;
import au.com.metlife.eapply.bs.bizflowmodel.AddressRoleList;
import au.com.metlife.eapply.bs.bizflowmodel.AttachmentType;
import au.com.metlife.eapply.bs.bizflowmodel.CitizenshipList;
import au.com.metlife.eapply.bs.bizflowmodel.Contact;
import au.com.metlife.eapply.bs.bizflowmodel.ContactProfile;
import au.com.metlife.eapply.bs.bizflowmodel.Contract;
import au.com.metlife.eapply.bs.bizflowmodel.ContractList;
import au.com.metlife.eapply.bs.bizflowmodel.Coverage;
import au.com.metlife.eapply.bs.bizflowmodel.ElectronicAddress;
import au.com.metlife.eapply.bs.bizflowmodel.ElectronicAddressRole;
import au.com.metlife.eapply.bs.bizflowmodel.ElectronicAddressRoleList;
import au.com.metlife.eapply.bs.bizflowmodel.Email;
import au.com.metlife.eapply.bs.bizflowmodel.FileAttachment;
import au.com.metlife.eapply.bs.bizflowmodel.Health;
import au.com.metlife.eapply.bs.bizflowmodel.IdentifierList;
import au.com.metlife.eapply.bs.bizflowmodel.IdentifierValue;
import au.com.metlife.eapply.bs.bizflowmodel.InterestedPartyReferences;
import au.com.metlife.eapply.bs.bizflowmodel.Limit;
import au.com.metlife.eapply.bs.bizflowmodel.LoadingListType;
import au.com.metlife.eapply.bs.bizflowmodel.LoadingValue;
import au.com.metlife.eapply.bs.bizflowmodel.MessageHeader;
import au.com.metlife.eapply.bs.bizflowmodel.OrganizationReferences;
import au.com.metlife.eapply.bs.bizflowmodel.Party;
import au.com.metlife.eapply.bs.bizflowmodel.PartyInterest;
import au.com.metlife.eapply.bs.bizflowmodel.PartyRole;
import au.com.metlife.eapply.bs.bizflowmodel.PersonReferences;
import au.com.metlife.eapply.bs.bizflowmodel.Phone;
import au.com.metlife.eapply.bs.bizflowmodel.PhoneRole;
import au.com.metlife.eapply.bs.bizflowmodel.PhoneRoleList;
import au.com.metlife.eapply.bs.bizflowmodel.PolicyInsuredReferences;
import au.com.metlife.eapply.bs.bizflowmodel.PolicyNewBusinessOrderProcess;
import au.com.metlife.eapply.bs.bizflowmodel.PolicyOrder;
import au.com.metlife.eapply.bs.bizflowmodel.Sender;
import au.com.metlife.eapply.bs.bizflowmodel.SendingSystem;
import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.model.Applicant;
import au.com.metlife.eapply.bs.model.ContactDetails;
import au.com.metlife.eapply.bs.model.Cover;
import au.com.metlife.eapply.bs.model.CoverInfo;
import au.com.metlife.eapply.bs.model.Document;
import au.com.metlife.eapply.bs.model.EappInput;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.quote.utility.QuoteConstants;
import au.com.metlife.eapply.repositories.model.CorporateFund;
import au.com.metlife.eapply.repositories.model.CorporateFundCat;
import au.com.metlife.eapply.underwriting.response.model.Decision;
import au.com.metlife.eapply.underwriting.response.model.Insured;

/*
 * Author 175373
 */

public class BizflowXMLHelperNew {

	private BizflowXMLHelperNew(){
		
	}
	private static final Logger log = LoggerFactory.getLogger(BizflowXMLHelper.class);
	
	public static String createXMLForBizFlow(Eapplication applicationDTO, EappInput eappInput,Properties property,CorporateFund corpFund){
		log.info("Create xml for BizflowMTAA start");
		String xmlOutput = null;
		xmlOutput = marshallingObj(preparePolicyNewBusinessOrderProcessObj(applicationDTO, eappInput,property,corpFund));
		log.info("Create xml for BizflowMTAA finish {}",xmlOutput);
		return xmlOutput;
		
	}
	
	public  static String marshallingObj(PolicyNewBusinessOrderProcess request){
		log.info("Marhalling object in BizflowXMLHelperMTAA start");
		String outputXML=null;
		try {
			JAXBContext context = JAXBContext.newInstance(PolicyNewBusinessOrderProcess.class);
		    javax.xml.bind.Marshaller m = context.createMarshaller();
		    m.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, true);	
		   		    
		    ByteArrayOutputStream os = new ByteArrayOutputStream(); 
		    m.marshal(request, os);		    
		    byte[] b=os.toByteArray();  
		    
		    outputXML=new String(b);
		    outputXML=outputXML.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
		    log.info("outputXML>> {}",outputXML);
		    os.close();
	    
		 } catch (Exception e) {
	          log.error("Error in marshalling object {}",e);
		 }
		log.info("Marhalling object finish");
		return outputXML;
	}	
	
	
	public static PolicyNewBusinessOrderProcess preparePolicyNewBusinessOrderProcessObj(Eapplication applicationDTO, EappInput eappInput,Properties property,CorporateFund corpfund){
		log.info("Method preparePolicyNewBusinessOrderProcessObj in BizflowXMLHelperMTAA start");
		Party party = null;
		List<FileAttachment> fileAttachmentList = null;
		Applicant applicantDTO = null;
		List<Cover> coverList = null;
		
		String ex_death_amt = "0";
		String ex_tpd_amt = "0";
		String ex_trauma_amt = "0";
		String ex_ip_amt = "0";
		String ex_wait_period = null;
		String ex_ben_period = null;
		String salPercentage = null;
		String add_wait_period = null;
		String add_ben_period = null;
		ContactDetails contactInfo = null;
		
		if(eappInput.getApplicant() != null && !eappInput.getApplicant().getCover().isEmpty()) {
			for(Iterator iterator = eappInput.getApplicant().getCover().iterator(); iterator.hasNext();) {
				CoverInfo coverInfo = (CoverInfo)iterator.next();
				if(coverInfo != null && ("1").equalsIgnoreCase(coverInfo.getBenefitType())) {
					ex_death_amt = coverInfo.getExistingCoverAmount().toString();
				} else if(coverInfo != null && ("2").equalsIgnoreCase(coverInfo.getBenefitType())) {
					ex_tpd_amt = coverInfo.getExistingCoverAmount().toString();
				}else if(coverInfo != null && ("3").equalsIgnoreCase(coverInfo.getBenefitType())) {
					ex_trauma_amt = coverInfo.getExistingCoverAmount().toString();
				}else if(coverInfo != null && ("4").equalsIgnoreCase(coverInfo.getBenefitType())) {
					ex_ip_amt = coverInfo.getExistingCoverAmount().toString();
					ex_wait_period = coverInfo.getExistingIpWaitingPeriod();
					ex_ben_period = coverInfo.getExistingIpBenefitPeriod();
				}
			}
		}
		
				
		PolicyNewBusinessOrderProcess policyNewBusinessOrderProcess = new PolicyNewBusinessOrderProcess();
		
		MessageHeader messageHeader = new MessageHeader();
		Sender sender = new Sender();
		sender.setRoleCode("FundOrganization");
		OrganizationReferences organizationReferences = new OrganizationReferences();
		organizationReferences.setOrganizationReference("ORG1");
		sender.setOrganizationReferences(organizationReferences);		
		Contact contact = new Contact();
		Email email = new Email();
		email.setEmailAddress(eappInput.getApplicant().getEmailId());
		contact.setEmail(email);
		sender.setContact(contact);
		messageHeader.setSender(sender);		
		SendingSystem sendingSystem = new SendingSystem();
		sendingSystem.setVendorProductName("EAPPLY");
		messageHeader.setSendingSystem(sendingSystem);	
		messageHeader.setACORDStandardVersionCode("AML_1_0_0");
		messageHeader.setCorrelationId("00000000-0000-0000-0000-000000000000");	
		
		messageHeader.setMessageId(UUID.randomUUID().toString());/*need to check UUid logic*/
		messageHeader.setMessageDateTime(new Timestamp(new Date().getTime()));
		policyNewBusinessOrderProcess.setMessageHeader(messageHeader);
		PolicyOrder policyOrder = new PolicyOrder();
		Contract contract = new Contract();
		contract.setTransactionFunctionCode("CreateApplication");
		
		if("UWCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
			contract.setApplicationTypeCode("TRWS");
         }else if(MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
        	 contract.setApplicationTypeCode("LVET");
         }else if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
        	 contract.setApplicationTypeCode("MCOV");
         }else if("CANCOVER".equalsIgnoreCase(applicationDTO.getRequesttype())){
        	 contract.setApplicationTypeCode("REDC");
         }else if(("CCOVER".equalsIgnoreCase(applicationDTO.getRequesttype()))&& getCoverOptions(applicationDTO)){
        	 contract.setApplicationTypeCode("REDC");
         }else if(MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) || MetlifeInstitutionalConstants.NCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
        	 contract.setApplicationTypeCode("SPEC");
         }else if((null !=  ex_death_amt && Double.parseDouble(ex_death_amt.trim())>0) || (null !=  ex_ip_amt && Double.parseDouble(ex_ip_amt.trim())>0) || (null !=  ex_trauma_amt && Double.parseDouble(ex_trauma_amt.trim())>0)
         		||(null !=  ex_tpd_amt && Double.parseDouble(ex_tpd_amt.trim())>0)
          ){
        	 contract.setApplicationTypeCode("ADDC");
         }else{
        	 contract.setApplicationTypeCode("NEWA"); 
         }	
		
		contract.setDescription(null) ;
		contract.setLineOfBusinessCode("GroupLife");
		contract.setPolicyNumber(null) ;
		contract.setPolicyStatus(null);
		contract.setPolicyStatusDescription(null) ;
		contract.setExternalReferenceNumber(null) ;
		contract.setCaseNumber(null);
		contract.setContractNumber(applicationDTO.getApplicationumber());
			
					
		if("ACC".equalsIgnoreCase(applicationDTO.getAppdecision())){
			if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && "CARE".equalsIgnoreCase(applicationDTO.getPartnercode())){
				contract.setContractStatus(MetlifeInstitutionalConstants.REFERRED);
			}else{
				contract.setContractStatus("Accept");
			}			
		}else if("RUW".equalsIgnoreCase(applicationDTO.getAppdecision())){
			contract.setContractStatus(MetlifeInstitutionalConstants.REFERRED);
		}else if("DCL".equalsIgnoreCase(applicationDTO.getAppdecision())){
			contract.setContractStatus("Decline");
			
		}		
		contract.setContractStatusDescription(null);
		contract.setEffectiveDate(applicationDTO.getCreatedate());
		contract.setMembershipTypeCode(eappInput.getApplicant().getMemberType());
		contract.setCancellationTypeCode(null);
		contract.setCancellationReasonCode(null) ;
		contract.setProductVariant(null);
		/*contracts*/
		List<ContractList> contracts = new ArrayList<>();
		ContractList contractList = new ContractList();
		contractList.setLifeEvent(eappInput.getApplicant().getLifeEvent());
		contracts.add(contractList);
		contract.setContractList(contracts);
		/*party*/
		List<Party> partyList = new ArrayList<>();
		party = new Party();
		party.setKey("ORG1");
		party.setPartyType("Organization");
		if(!"CORP".equalsIgnoreCase(applicationDTO.getPartnercode())) {
		party.setPartyUID(applicationDTO.getPartnercode());
		}
		else {
			if(corpfund!=null) {
				party.setPartyUID(corpfund.getFundCode());
			}
		}
		partyList.add(party);
		party = new Party();
		party.setKey("ORG2");
		party.setPartyType("Administrator");
		if(!"CORP".equalsIgnoreCase(applicationDTO.getPartnercode())) {
		party.setPartyUID(applicationDTO.getPartnercode());
		}
		else {
			if(corpfund!=null) {
				party.setPartyUID(corpfund.getFundCode());
			}
		}
		partyList.add(party);
		
		for (Iterator iter = applicationDTO.getApplicant().iterator(); iter.hasNext();) {
			applicantDTO = (Applicant) iter.next();
			if(applicantDTO!=null && "primary".equalsIgnoreCase(applicantDTO.getApplicanttype())){
				party = new Party();
				party.setKey("PERS1");
				party.setPartyType("Person");
				party.setPartyUID(applicantDTO.getClientrefnumber());
				if(eappInput.getApplicant()!=null){
					party.setLastName(eappInput.getApplicant().getLastName());
					party.setGender(eappInput.getApplicant().getGender());
				}
				
				party.setFirstName(applicantDTO.getFirstname());
				party.setPrefixCode(applicantDTO.getTitle());
				
				party.setBirthDate(eappInput.getApplicant().getBirthDate());
				party.setOccupation(applicantDTO.getOccupation());
				party.setPartyFundJoinDate(applicantDTO.getDatejoinedfund());
				
						
				PartyRole partyRole = new PartyRole();
				ContactProfile contactProfile = new ContactProfile();
				List<AddressRoleList> addressRoles = new ArrayList<>();
				AddressRoleList addressRoleList = new AddressRoleList();
				AddressRole addressRole = new AddressRole() ;
				Address address = new Address();
				/*defaulted to home as we don't ask address type in eapply*/
				address.setAddressKey("Home");
				if(eappInput.getApplicant()!= null && eappInput.getApplicant().getContactDetails()!= null) {
					contactInfo = eappInput.getApplicant().getContactDetails();
					address.setAddressLine1(contactInfo.getAddressLine1());
					address.setAddressLine2(contactInfo.getAddressLine2());
					if(!"CORP".equalsIgnoreCase(eappInput.getPartnerCode())) {
						address.setCountryName(contactInfo.getCountry());
					}else {
						if(!"OTH".equalsIgnoreCase(contactInfo.getState())) {
							address.setCountryName("AUSTRALIA");
						}
						else {
							address.setCountryName("OVERSEAS");
						}
					}
					address.setPostalCode( contactInfo.getPostCode());
					address.setStateOrProvinceName(contactInfo.getState());
					address.setSuburbName(contactInfo.getSuburb());
					addressRole.setAddress(address);
					addressRoleList.setAddressRole(addressRole);	
					addressRoles.add(addressRoleList);
					contactProfile.setAddressRoleList(addressRoles);
				}
							
				
				List<PhoneRoleList> phones = new ArrayList<>();
				PhoneRoleList phoneRoleList = new PhoneRoleList();
				
				PhoneRole phoneRole = new PhoneRole();
				Phone phone = new Phone();
			  		        
		        if(phone.getPhoneNumber()==null && eappInput.getApplicant().getContactDetails()!=null) {
		        	phone.setPhoneNumber(eappInput.getApplicant().getContactDetails().getPreferedContactNumber());
					phone.setPreferredContact("Yes");
					phone.setPhoneType(eappInput.getApplicant().getContactDetails().getPreferedContacType());
		        }		       
		     
		        
		        if(eappInput.getApplicant().getContactDetails()!= null && eappInput.getApplicant().getContactDetails().getPreferedContactTime() != null) {
		        	String preferedTime =eappInput.getApplicant().getContactDetails().getPreferedContactTime();
		        	if(preferedTime!= null && "1".equalsIgnoreCase(preferedTime)){
		        		preferedTime = "9am - 12pm";
					}else{
						preferedTime = "12pm - 6pm";
					}
		        	log.info("applicantDTO.getContactinfo().getPreferedContactTime()>> {}",preferedTime);
		    			    				    		 		
		    		if(preferedTime.contains("(")){
		    			preferedTime = preferedTime.substring(preferedTime.indexOf('(')+1,preferedTime.lastIndexOf(')'));
			    		String startTime= preferedTime.substring(0,preferedTime.indexOf('-'));
			    		String endTime= preferedTime.substring(preferedTime.indexOf('-')+1);			    		
			    		 phone.setStartTime(startTime.trim());			    		
			    		phone.setEndTime(endTime.trim());
		    		}else if(preferedTime.length()>9){
			    		String startTime= preferedTime.substring(0,4);			    		
			    		String endTime= preferedTime.substring(5,10);			    		
			    		 phone.setStartTime(startTime.trim());			    		
			    		phone.setEndTime(endTime.trim());
		    		}
		        }
		       
		        phoneRole.setPhone(phone);
		        phoneRoleList.setPhoneRole(phoneRole);
		        phones.add(phoneRoleList);
		        contactProfile.setPhoneRoleList(phones);
		        
		        List<ElectronicAddressRoleList> electronicAddresess = new ArrayList<>(); 
		        ElectronicAddressRoleList electronicAddressRoleList = new ElectronicAddressRoleList();
		        ElectronicAddressRole electronicAddressRole = new ElectronicAddressRole();		        
		        ElectronicAddress electronicAddress = new ElectronicAddress();
		        electronicAddress.setCategory("Personal");
		        electronicAddress.setEmail(eappInput.getApplicant().getEmailId());
		        electronicAddressRole.setElectronicAddress(electronicAddress);		        
		        electronicAddressRoleList.setElectronicAddressRole(electronicAddressRole);
		        electronicAddresess.add(electronicAddressRoleList);
		        contactProfile.setElectronicAddressRoleList(electronicAddresess);
		        
		        partyRole.setContactProfile(contactProfile);
		        
		        
		        if(eappInput.getApplicant()!=null && eappInput.getApplicant().getContactDetails() != null){
		        	List<CitizenshipList> citizens = new ArrayList<>();
					CitizenshipList citizenshipList = new CitizenshipList();
					if(eappInput.getApplicant().getContactDetails().getCountry()!=null && eappInput.getApplicant().getContactDetails().getCountry().trim().length()>0){
						citizenshipList.setCode(eappInput.getApplicant().getContactDetails().getCountry());
					}else{
						citizenshipList.setCode("Australia");
					}					
					citizens.add(citizenshipList);
					partyRole.setCitizenshipList(citizens);
		        }
				
				party.setPartyRole(partyRole);
				Health health = new Health();
				if("yes".equalsIgnoreCase(eappInput.getApplicant().getSmoker())){
					applicantDTO.setSmoker("Yes");
					
				}else if("No".equalsIgnoreCase(eappInput.getApplicant().getSmoker())){
					applicantDTO.setSmoker("No");
					
				}
				if(applicantDTO.getSmoker()!=null && "yes".equalsIgnoreCase(applicantDTO.getSmoker())){
					health.setSmokerIndicator("Yes");
				}else if(applicantDTO.getSmoker()!=null && "no".equalsIgnoreCase(applicantDTO.getSmoker())){
					health.setSmokerIndicator("No");
				}else{
					health.setSmokerIndicator("UNKNOWN");
				}
				
				party.setHealth(health);				
				/*coverage*/
				
				coverList = validCoverVector(applicantDTO.getCovers(),corpfund, eappInput.getPartnerCode()) ;
				List<Coverage> coverageList = new ArrayList<>();
				
				Decision decision2 = null;
				for (Iterator iterator = coverList.iterator(); iterator.hasNext();) {
					Cover coversDTO = (Cover) iterator.next();
					String add_unit = null;
					String e_unit = null;
					String e_amt = null;
					String add_amt = null;
					String tot_amt = null;
					String add_loading = null;
					String add_loading_cmt = null;
					String decision = null;
					String dec_reason = null;
					String fwd_cvr = null;
					String aal_cvr = null;
					
					String loadingAmt = null;
					String loadingType = null;

					if(coversDTO!=null){
						if(MetlifeInstitutionalConstants.DEATH.equalsIgnoreCase(coversDTO.getCovercode())){							
							
							if(eappInput.getApplicant() != null && !eappInput.getApplicant().getCover().isEmpty()) {
								for(Iterator itr = eappInput.getApplicant().getCover().iterator(); itr.hasNext();) {
									CoverInfo coverInfo = (CoverInfo)itr.next();
									if(coverInfo.getBenefitType()!= null && ("1").equalsIgnoreCase(coverInfo.getBenefitType())) {
										e_amt = coverInfo.getExistingCoverAmount().toString();
										e_unit = coverInfo.getFixedUnit();
										
										//Need to check this logic thoroughly
										if(coverInfo.getCoverCategory()!= null && !("DcFixed").equalsIgnoreCase(coverInfo.getCoverCategory()) ) {
											add_unit = coverInfo.getAdditionalUnit();
										}else if ((QuoteConstants.DC_UNITISED).equalsIgnoreCase(coverInfo.getCoverCategory()) && ("TCOVER").equalsIgnoreCase(eappInput.getRequestType())) {
											add_unit = coverInfo.getAdditionalUnit();
										}
										 if(("TCOVER").equalsIgnoreCase(eappInput.getRequestType())) {
											tot_amt = coverInfo.getTransferCoverAmount().toString();
										}else{
												tot_amt = coverInfo.getAdditionalCoverAmount();
										}
										if(("TCOVER").equalsIgnoreCase(eappInput.getRequestType())) {
											add_amt = coverInfo.getAdditionalCoverAmount();
										}else if(tot_amt!=null && e_amt!=null){
											add_amt = ((new BigDecimal(tot_amt)).subtract(new BigDecimal(e_amt))).toString();
										}
										if(tot_amt == null && add_amt == null ) {
											add_amt = "0";
											tot_amt = add_amt;
										}
									}
								}
							}
														
							decision2 = getReasons(eappInput, "Term");
							if(decision2!=null){
								 add_loading = decision2.getOriginalTotalDebitsValue();
								 add_loading_cmt = decision2.getTotalDebitsReason();
								 loadingAmt = decision2.getOriginalTotalDebitsValue();
								 loadingType = decision2.getTotalDebitsReason();
							}							 
							 coverageList.add(getCoverage(coversDTO, coversDTO.getCovercode(),e_amt, e_unit, add_unit, add_amt, tot_amt, add_loading, add_loading_cmt, decision, dec_reason, fwd_cvr, aal_cvr, null, null, null, null,loadingType,loadingAmt,eappInput,applicationDTO));
							 
						}else if("TPD".equalsIgnoreCase(coversDTO.getCovercode())){
							decision2 =getReasons(eappInput, "TPD");
							 if(decision2!=null){
								 add_loading = decision2.getOriginalTotalDebitsValue();
								 add_loading_cmt = decision2.getTotalDebitsReason();
								 loadingAmt = decision2.getOriginalTotalDebitsValue();
								 loadingType = decision2.getTotalDebitsReason();
							 }
							 if(eappInput.getApplicant() != null && !eappInput.getApplicant().getCover().isEmpty()) {
								 CoverInfo coverInfo = new CoverInfo();
									 for(Iterator itr = eappInput.getApplicant().getCover().iterator(); itr.hasNext();) {
											coverInfo = (CoverInfo) itr.next();
										 if(coverInfo.getBenefitType()!= null && ("2").equalsIgnoreCase(coverInfo.getBenefitType())) {
												e_amt = coverInfo.getExistingCoverAmount().toString();
												e_unit = coverInfo.getFixedUnit();
												
												//Need to check this logic thoroughly
												
												if(coverInfo.getCoverCategory()!= null && !("TPDFixed").equalsIgnoreCase(coverInfo.getCoverCategory()) ) {
													add_unit = coverInfo.getAdditionalUnit();
												}else if ((QuoteConstants.TPD_UNITISED).equalsIgnoreCase(coverInfo.getCoverCategory()) && ("TCOVER").equalsIgnoreCase(eappInput.getRequestType())) {
													add_unit = coverInfo.getAdditionalUnit();
												}
												if(("TCOVER").equalsIgnoreCase(eappInput.getRequestType())) {
													tot_amt = coverInfo.getTransferCoverAmount().toString();
												}else{
														tot_amt = coverInfo.getAdditionalCoverAmount();
												}
												if(("TCOVER").equalsIgnoreCase(eappInput.getRequestType())) {
													add_amt = coverInfo.getAdditionalCoverAmount();
												}else if(tot_amt!=null && e_amt!=null){
													add_amt = ((new BigDecimal(tot_amt)).subtract(new BigDecimal(e_amt))).toString();
												}
												if(tot_amt == null && add_amt == null ) {
													add_amt = "0";
													tot_amt = add_amt;
												}
											
											}
									 }
							 }
							 
							 coverageList.add(getCoverage(coversDTO, coversDTO.getCovercode(),e_amt, e_unit, add_unit, add_amt, tot_amt, add_loading, add_loading_cmt, decision, dec_reason, fwd_cvr, aal_cvr, null, null, null, null,loadingType,loadingAmt,eappInput,applicationDTO));
							 
						}else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
							decision2 =getReasons(eappInput, "IP");
							if(decision2!=null){
								add_loading = decision2.getOriginalTotalDebitsValue();
								add_loading_cmt = decision2.getTotalDebitsReason();
								loadingAmt = decision2.getOriginalTotalDebitsValue();
								loadingType = decision2.getTotalDebitsReason();
							}
							
														 
							 if (ex_wait_period!=null && ex_wait_period.toUpperCase().contains( "DAYS")) {
									ex_wait_period =  ex_wait_period.substring( 0, ex_wait_period.toUpperCase().indexOf( "DAYS")).trim();
								}else if(ex_wait_period!=null && ex_wait_period.toUpperCase().contains( MetlifeInstitutionalConstants.YEARS)){
									int yesrs = Integer.parseInt( ex_wait_period.substring( 0, ex_wait_period.toUpperCase().indexOf( MetlifeInstitutionalConstants.YEARS)).trim());
					                yesrs = 365 * yesrs;
					                ex_wait_period =  Integer.toString(yesrs);
								}
								if (ex_ben_period!=null && ex_ben_period.toUpperCase().contains( "DAYS")) {
									ex_ben_period= ex_ben_period.substring( 0, ex_ben_period.toUpperCase().indexOf( "DAYS")).trim()+ "MTH";
								}else if(ex_ben_period!=null && ex_ben_period.toUpperCase().contains( MetlifeInstitutionalConstants.YEARS)){
									ex_ben_period = ex_ben_period.substring( 0, ex_ben_period.toUpperCase().indexOf( MetlifeInstitutionalConstants.YEARS)).trim()+ "YR";
								}else if(ex_ben_period!=null && ex_ben_period.toUpperCase().contains( "AGE")){
									ex_ben_period = ex_ben_period.toUpperCase().replaceAll( " ", "");
								}
							 
								 if(eappInput.getApplicant() != null && !eappInput.getApplicant().getCover().isEmpty()) {
									 CoverInfo coverInfo = new CoverInfo();
									 for(Iterator itr = eappInput.getApplicant().getCover().iterator(); itr.hasNext();) {
										 coverInfo = (CoverInfo) itr.next();
											 if(coverInfo.getBenefitType()!= null && ("4").equalsIgnoreCase(coverInfo.getBenefitType())) {
													e_amt = coverInfo.getExistingCoverAmount().toString();
													e_unit = coverInfo.getFixedUnit();
										
													if(coverInfo.getCoverCategory()!= null  
															&& eappInput.getRequestType() != null && QuoteConstants.MTYPE_CCOVER.equalsIgnoreCase (eappInput.getRequestType() )) {
														add_unit = coverInfo.getAdditionalUnit();
													}
													
													if(("TCOVER").equalsIgnoreCase(eappInput.getRequestType())) {
														tot_amt = coverInfo.getTransferCoverAmount().toString();
													}else{
															tot_amt = coverInfo.getAdditionalCoverAmount();
													}
													
													if(("TCOVER").equalsIgnoreCase(eappInput.getRequestType())) {
														add_amt = coverInfo.getAdditionalCoverAmount();
													}else if(tot_amt!=null && e_amt!=null){
														add_amt = ((new BigDecimal(tot_amt)).subtract(new BigDecimal(e_amt))).toString();
													}
													
													if(tot_amt == null && add_amt == null ) {
														add_amt = "0";
														tot_amt = add_amt;
													}
													
													if(eappInput.getRequestType() != null ) {
														add_wait_period = coverInfo.getAdditionalIpWaitingPeriod();
					                                    add_ben_period = coverInfo.getAdditionalIpBenefitPeriod();
													}else {
														add_wait_period = "";
					                                    add_ben_period = "";
													}
													
													if (add_wait_period!=null && add_wait_period.toUpperCase().contains( "DAYS")) {
														add_wait_period =  add_wait_period.substring( 0, add_wait_period.toUpperCase().indexOf( "DAYS")).trim();
													}else if(add_wait_period!=null && add_wait_period.toUpperCase().contains( MetlifeInstitutionalConstants.YEARS)){
														int yesrs = Integer.parseInt( add_wait_period.substring( 0, add_wait_period.toUpperCase().indexOf( MetlifeInstitutionalConstants.YEARS)).trim());
											            yesrs = 365 * yesrs;
											            add_wait_period = Integer.toString(yesrs);
													}
													if (add_ben_period!=null && add_ben_period.toUpperCase().contains( "DAYS")) {
														add_ben_period= add_ben_period.substring( 0, add_ben_period.toUpperCase().indexOf( "DAYS")).trim()+ "MTH";
													}else if(add_ben_period!=null && add_ben_period.toUpperCase().contains( MetlifeInstitutionalConstants.YEARS)){
														add_ben_period = add_ben_period.substring( 0, add_ben_period.toUpperCase().indexOf( MetlifeInstitutionalConstants.YEARS)).trim()+ "YR";
													}else if(add_ben_period!=null && add_ben_period.toUpperCase().contains( "AGE")){
														add_ben_period = add_ben_period.toUpperCase().replaceAll( " ", "");
													}
													
													 if( coverInfo.getOptionalUnit()!=0){
														 salPercentage=  Integer.toString(coverInfo.getOptionalUnit());	
													 }	
										
											 }
									 }
									 
							 }
												  
								 coverageList.add(getCoverage(coversDTO, coversDTO.getCovercode(),e_amt, e_unit, add_unit, add_amt, tot_amt, add_loading, add_loading_cmt, decision, dec_reason, fwd_cvr, aal_cvr, ex_wait_period, ex_ben_period, add_wait_period, add_ben_period,loadingType,loadingAmt,eappInput,applicationDTO));  
						}		
						
						
					}
					
				}
				contract.setCoverage(coverageList);
				if(salPercentage!=null){
					IdentifierList identifierList = new IdentifierList();
					List<IdentifierValue> identifierValueList = new ArrayList<>();
					IdentifierValue identifierValue = new IdentifierValue();
					identifierValue.setKey("SaleryPercentage");
					log.info("salPercentage>> {}",salPercentage);
					if(null!=salPercentage && salPercentage.contains("85%")){
						identifierValue.setData("85");
					}else if(null!=salPercentage && salPercentage.contains("75%")){
						identifierValue.setData("75");
					}else if(null!=salPercentage && salPercentage.contains("90%")){
						identifierValue.setData("90");
					}else{
						identifierValue.setData(salPercentage);
					}
					identifierValueList.add(identifierValue);
					identifierList.setIdentifierValue(identifierValueList);			
					party.setIdentifierList(identifierList);
				}
				
				partyList.add(party);		
				
			}
			
		}
		List<PartyInterest> partyInterestList = new ArrayList<>();
		
		PartyInterest partyInterest = new PartyInterest();
		partyInterest.setRoleCode("FundAdministrator");
		InterestedPartyReferences interestedPartyReferences = new InterestedPartyReferences();
		PersonReferences personReferences = new PersonReferences();
		personReferences.setPersonReference("ORG2");
		interestedPartyReferences.setPersonReferences(personReferences);
		partyInterest.setInterestedPartyReferences(interestedPartyReferences);
		partyInterestList.add(partyInterest);
		partyInterest = new PartyInterest();
		partyInterest.setRoleCode("FundOrganization");
		interestedPartyReferences = new InterestedPartyReferences();
		personReferences = new PersonReferences();
		personReferences.setPersonReference("ORG1");
		interestedPartyReferences.setPersonReferences(personReferences);
		partyInterest.setInterestedPartyReferences(interestedPartyReferences);
		partyInterestList.add(partyInterest);
		
		contract.setPartyInterest(partyInterestList);
		contract.setParty(partyList);
		String bizFlowBasePath = null;
		String eApplyFileBasePath = null;
		String fileName = null;
		Document document = null;
		AttachmentType attachmentType = null;
		FileAttachment fileAttachment = null;
		
		if(applicationDTO.getDocuments()!=null && !(applicationDTO.getDocuments().isEmpty())){
			fileAttachmentList = new ArrayList<>();
			for (Iterator iterator = applicationDTO.getDocuments().iterator(); iterator.hasNext();) {
				document = (Document) iterator.next();
				if(document!=null){
					attachmentType = new AttachmentType();
					fileAttachment = new FileAttachment();
					bizFlowBasePath=  property.getProperty("FILE_UPLOAD_PATH_BIZ");
					eApplyFileBasePath =property.getProperty("FILE_UPLOAD_PATH");
					fileName = document.getDocLoc().substring(eApplyFileBasePath.length());
					attachmentType.setAttachmentTypeCode("Application Form");
					attachmentType.setDocumentURI(bizFlowBasePath+fileName);
					fileAttachment.setAttachmentType(attachmentType);
					fileAttachmentList.add(fileAttachment);
				}
				
			}
		}			
		contract.setFileAttachment(fileAttachmentList);
		policyOrder.setContract(contract);
		policyNewBusinessOrderProcess.setPolicyOrder(policyOrder);
		log.info("Prepare Policy new business order project object fiiinishhh");
		return policyNewBusinessOrderProcess;
	}
	
	public static List validCoverVector(List<Cover> coverVec,CorporateFund corpfund, String partnerCode) {
		log.info("Valid cover vector start");
     
		ArrayList updatedVector = null;
      
      if (null != coverVec) {
          updatedVector = new ArrayList();
          if(corpfund!=null && corpfund.getCorpFundCategories()!=null) {
          	for(CorporateFundCat coverreq:corpfund.getCorpFundCategories()) {
          for (int itr = 0; itr < coverVec.size(); itr++) {
              Cover coversDTO =  coverVec.get( itr);
    
            	if (null != coversDTO
                        && null!=coversDTO.getCoverdecision() && coversDTO.getCoverdecision().length()>0) {
                    if (coversDTO.getCovercode().equalsIgnoreCase( MetlifeInstitutionalConstants.DEATH)
                            && null != coversDTO.getCoverdecision() && "Y".equalsIgnoreCase(coverreq.getDeathRequired().toString())) {
                        updatedVector.add( coversDTO);
                    }
                    if (coversDTO.getCovercode().equalsIgnoreCase( "TPD")
                            && null != coversDTO.getCoverdecision() && "Y".equalsIgnoreCase(coverreq.getTpdRequired().toString())) {
                        updatedVector.add( coversDTO);
                    }
                    if (coversDTO.getCovercode().contains("TRAUMA")
                            && null != coversDTO.getCoverdecision()) {
                        updatedVector.add( coversDTO);
                    }
                    if (coversDTO.getCovercode().equalsIgnoreCase( "IP")
                            && null != coversDTO.getCoverdecision() && "Y".equalsIgnoreCase(coverreq.getIpRequired().toString() )) {
                        updatedVector.add( coversDTO);
                    }
                }
            	}
            }
          }
            else {
            	 for (int itr = 0; itr < coverVec.size(); itr++) {
                     Cover coversDTO =  coverVec.get( itr);
                     
                     String coverDecision  =  null;
                     if(partnerCode != null && ("MTAA").equalsIgnoreCase(partnerCode)) {
                    	 checkDecision(coversDTO);
                     }
                     coverDecision = coversDTO.getCoverdecision();
                     
		              if (null != coverDecision && coverDecision.length()>0) {
		                  if (coversDTO.getCovercode().equalsIgnoreCase( MetlifeInstitutionalConstants.DEATH) && null != coverDecision) {
		                      updatedVector.add( coversDTO);
		                  }
		                  if (coversDTO.getCovercode().equalsIgnoreCase( "TPD") && null != coverDecision) {
		                      updatedVector.add( coversDTO);
		                  }
		                  if (coversDTO.getCovercode().contains("TRAUMA") && null != coversDTO.getCoverdecision()) {
		                      updatedVector.add( coversDTO);
		                  }
		                  if (coversDTO.getCovercode().equalsIgnoreCase( "IP") && null != coverDecision) {
		                      updatedVector.add( coversDTO);
		                  }
		              }	
            	 }
          }        
      }
      log.info("Valid cover vector finish");
      return updatedVector;
  }
	
	public static String checkDecision(Cover coversDTO) {
		String coverDecision = coversDTO.getCoverdecision();
		
		if(null == coversDTO.getCoverdecision() && coversDTO.getExistingcoveramount().compareTo(coversDTO.getAdditionalcoveramount()) == 1) {
				coversDTO.setCoverdecision(MetlifeInstitutionalConstants.ACC);
			}		
			
		return coverDecision;
	}
	
	public static Decision getReasons(EappInput eappInput, String coverType){
		log.info("Get reasons start");
		Decision decision= null;
		if (null != eappInput && null!=eappInput.getResponseObject() && null != eappInput.getResponseObject().getClientData()
                && null != eappInput.getResponseObject().getClientData().getListInsured()) {
            for (int itr = 0; itr < eappInput.getResponseObject().getClientData().getListInsured().size(); itr++) {
                Insured insured =  eappInput.getResponseObject().getClientData().getListInsured().get( itr);
                if (null != insured.getListOverallDec()) {
                    for (int index = 0; index < insured.getListOverallDec().size(); index++) {
                         decision =  insured.getListOverallDec().get( index);
                       
                        if (null != decision
                        		&& (decision.getProductName().contains(coverType) && 
                                		null != decision.getOriginalTotalDebitsValue() && decision.getOriginalTotalDebitsValue().length() > 0 && new BigDecimal(decision.getOriginalTotalDebitsValue()).doubleValue() > 25)) {
                            	break;
                        }
                    }
                }
            }
		}
		log.info("Get reasons finish");
		return decision;
	}
	
	private static Boolean getCoverOptions(Eapplication applicationDTO){
		log.info("Get cover options start");
   	Boolean isReduceCancel = Boolean.FALSE;
   	StringBuilder strBuffer = new StringBuilder();
   	for (Iterator iter = applicationDTO.getApplicant().iterator(); iter.hasNext();) {
			Applicant applicantDTO = (Applicant) iter.next();
			if(null!=applicantDTO && null!=applicantDTO.getCovers() && !(applicantDTO.getCovers().isEmpty())){
				for (Iterator iterator = applicantDTO.getCovers().iterator(); iterator
						.hasNext();) {
					Cover coversDTO = (Cover) iterator.next();	
					if(null!=coversDTO){						
						if(null!=coversDTO.getCoveroption() && "Decrease your cover".equalsIgnoreCase(coversDTO.getCoveroption())){
							strBuffer.append("DECREASE");
						}
						if(null!=coversDTO.getCoveroption() && coversDTO.getCoveroption().contains(MetlifeInstitutionalConstants.CANCEL)){
							strBuffer.append(MetlifeInstitutionalConstants.CANCEL);
						}else if(null!=coversDTO.getCoveroption() && coversDTO.getCoveroption().contains(MetlifeInstitutionalConstants.COVER_OPTION_INCREASE)){
							strBuffer.append(MetlifeInstitutionalConstants.COVER_OPTION_INCREASE);
						}
					}
						
					
				}
			}
		}    	
   	if(!strBuffer.toString().contains(MetlifeInstitutionalConstants.COVER_OPTION_INCREASE) && (strBuffer.toString().contains("DECREASE") || strBuffer.toString().contains(MetlifeInstitutionalConstants.CANCEL))){			
   		isReduceCancel = Boolean.TRUE;
		}
	log.info("Get cover options finish");
   	return isReduceCancel;
   }
	
	private static Coverage getCoverage(Cover coversDTO, String coverType,
			String e_amt,String e_unit,String add_unit,String add_amt,String tot_amt,String add_loading,String add_loading_cmt,String decision, String dec_reason,String fwd_cvr,String aal_cvr,
			String ex_wait_period, String ex_ben_period, String add_wait_period,String add_ben_period, String loadingType, String loadingAmt, EappInput eappInput,Eapplication applicationDTO){
		log.info("Get coverage start");

		Coverage coverage = new Coverage();
		if(coverType.equalsIgnoreCase(coversDTO.getCovercode())){
			coverage.setTypeCode(coversDTO.getCovercode());
			if("RUW".equalsIgnoreCase(coversDTO.getCoverdecision())){
				coverage.setCoverageStatusCode(MetlifeInstitutionalConstants.REFERRED);
			}else if("DCL".equalsIgnoreCase(coversDTO.getCoverdecision())){
				if(MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) || MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) || MetlifeInstitutionalConstants.NCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) || MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
					coverage.setCoverageStatusCode("Ineligible");
				}else{
					coverage.setCoverageStatusCode("Decline");
				}				
			}else if("ACC".equalsIgnoreCase(coversDTO.getCoverdecision())){
				if(MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) || MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) || MetlifeInstitutionalConstants.NCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) || MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
					coverage.setCoverageStatusCode("Eligible");
				}else{
					coverage.setCoverageStatusCode("Accept");
				}						
				
			}		
			
			coverage.setItemAmount(Double.parseDouble(coversDTO.getCost()));
			
			PolicyInsuredReferences policyInsuredReferences = new PolicyInsuredReferences();
			policyInsuredReferences.setKey("PERS1");
			coverage.setPolicyInsuredReferences(policyInsuredReferences);			
			List<Limit> limitList = new ArrayList<>();
			int itr = 0;
			while (itr<3) {
				Limit limit = new Limit();
				if(itr==0){
					limit.setLimitAppliesCode("ExistingCover");							
					limit.setLimitAmount(e_amt);
					limit.setLimitUnits(e_unit);
					if(null!=coversDTO.getCovercategory() && coversDTO.getCovercategory().contains(MetlifeInstitutionalConstants.UNITISED)){
						limit.setCalculationMethodCode(MetlifeInstitutionalConstants.UNITISED);
					}else if(null!=coversDTO.getCovercategory() && coversDTO.getCovercategory().contains(MetlifeInstitutionalConstants.FIXED)){
						limit.setCalculationMethodCode(MetlifeInstitutionalConstants.FIXED);
					}else{
						limit.setCalculationMethodCode(coversDTO.getCovercategory());
					}					
					limit.setOccupationalRatingCode(coversDTO.getOccupationrating());
					limit.setWaitingPeriod(ex_wait_period);
					limit.setBenefitPeriod(ex_ben_period);
					limit.setPremium(null);
					limit.setPremiumFrequency(null);				
				}else if(itr==1){
					limit.setLimitAppliesCode("AdditionalCover");
					if(tot_amt!=null && e_amt!=null && new BigDecimal(tot_amt).doubleValue()>0 && new BigDecimal(tot_amt).subtract(new BigDecimal(e_amt)).doubleValue()<0){
						limit.setLimitAmount((new BigDecimal(tot_amt).subtract(new BigDecimal(e_amt))).toString());
					}else{
						limit.setLimitAmount(add_amt);
					}
					
					limit.setLimitUnits(add_unit);
					if(null!=coversDTO.getCovercategory() && coversDTO.getCovercategory().contains(MetlifeInstitutionalConstants.UNITISED)){
						limit.setCalculationMethodCode(MetlifeInstitutionalConstants.UNITISED);
					}else if(null!=coversDTO.getCovercategory() && coversDTO.getCovercategory().contains(MetlifeInstitutionalConstants.FIXED)){
						limit.setCalculationMethodCode(MetlifeInstitutionalConstants.FIXED);
					}else{
						limit.setCalculationMethodCode(coversDTO.getCovercategory());
					}	
					limit.setOccupationalRatingCode(coversDTO.getOccupationrating());
					limit.setWaitingPeriod(add_wait_period);
					limit.setBenefitPeriod( add_ben_period);
					limit.setPremiumFrequency(coversDTO.getFrequencycosttype());
				}else if(itr==2){
					limit.setLimitAppliesCode("TotalCover");							
					limit.setLimitAmount(tot_amt);
					
					
					limit.setLimitUnits(add_unit);
					
					if(null!=coversDTO.getCovercategory() && coversDTO.getCovercategory().contains(MetlifeInstitutionalConstants.UNITISED)){
						limit.setCalculationMethodCode(MetlifeInstitutionalConstants.UNITISED);
					}else if(null!=coversDTO.getCovercategory() && coversDTO.getCovercategory().contains(MetlifeInstitutionalConstants.FIXED)){
						limit.setCalculationMethodCode(MetlifeInstitutionalConstants.FIXED);
					}else{
						limit.setCalculationMethodCode(coversDTO.getCovercategory());
					}	
					limit.setOccupationalRatingCode(coversDTO.getOccupationrating());
					limit.setWaitingPeriod(add_wait_period);
					limit.setBenefitPeriod( add_ben_period);
					
					limit.setPremium(Double.parseDouble(coversDTO.getCost()));
					
					limit.setPremiumFrequency(coversDTO.getFrequencycosttype());
				}								
				itr=itr+1;
				limitList.add(limit);
			}
			if(aal_cvr!=null && aal_cvr.trim().length()>0){
				Limit limit = new Limit();
				limit.setLimitAppliesCode("AAL");							
				limit.setLimitAmount(aal_cvr);
				limitList.add(limit);
			}
			if(fwd_cvr!=null && fwd_cvr.trim().length()>0){
				Limit limit = new Limit();
				limit.setLimitAppliesCode("FUL");							
				limit.setLimitAmount(fwd_cvr);
				limitList.add(limit);
			}
			coverage.setLimit(limitList);
			/*exclusions*/
			
			/*loading*/
			List<LoadingListType> loadingList = new ArrayList<>();
			LoadingListType loadingListType = new LoadingListType();
			if(loadingAmt!=null && loadingAmt.trim().length()>0){				
				loadingListType.setLoadingType(loadingType);				
				LoadingValue loadingValue = new LoadingValue();
				loadingValue.setType("Percentage");
				loadingValue.setData(loadingAmt);			
				loadingListType.setLoadingValue(loadingValue);
				loadingList.add(loadingListType);
				coverage.setLoadingListType(loadingList);
			}else{
				loadingList.add(loadingListType);
				coverage.setLoadingListType(loadingList);
			}			
			
		}
		log.info("Get coverage finish");
		return coverage;
	
	}
}
