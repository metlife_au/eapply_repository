package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import java.util.List;

import au.com.metlife.eapply.calculator.model.KidsVO;

public class ApplicantInfoIns implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private int totalExpense;
	private int totalDebts;
	private String salaryFrequency;
	private String spouseSalaryFrequency;
	private String totalExpenseFrequency;
	private String age;
	private String gender;
	private String livingWithWife;
	private String dependentChild;
	private String industryType;
	private String occupation;
	private String otherOccupation;
	private String salary;
	private String spouseSalary;
	private String mortage;	
	private List<CoverInfoIns> cover;
	private List<ExpenseInfoIns> expenses;
	private List<DebtsInfoIns> debts;	
	private String mortgageFrequency;
	private String mortgageAmount;
	private boolean nursingCosts;
	private boolean tpdWithoutIP;
	private List<KidsVO> childList;
	public List<KidsVO> getChildList() {
		return childList;
	}
	public void setChildList(List<KidsVO> childList) {
		this.childList = childList;
	}
	public boolean isNursingCosts() {
		return nursingCosts;
	}
	public void setNursingCosts(boolean nursingCosts) {
		this.nursingCosts = nursingCosts;
	}
	public boolean isTpdWithoutIP() {
		return tpdWithoutIP;
	}
	public void setTpdWithoutIP(boolean tpdWithoutIP) {
		this.tpdWithoutIP = tpdWithoutIP;
	}
	public String getMortgageFrequency() {
		return mortgageFrequency;
	}
	public void setMortgageFrequency(String mortgageFrequency) {
		this.mortgageFrequency = mortgageFrequency;
	}
	public String getMortgageAmount() {
		return mortgageAmount;
	}
	public void setMortgageAmount(String mortgageAmount) {
		this.mortgageAmount = mortgageAmount;
	}
	
	public int getTotalExpense() {
		return totalExpense;
	}
	public void setTotalExpense(int totalExpense) {
		this.totalExpense = totalExpense;
	}
	public int getTotalDebts() {
		return totalDebts;
	}
	public void setTotalDebts(int totalDebts) {
		this.totalDebts = totalDebts;
	}
	public String getSalaryFrequency() {
		return salaryFrequency;
	}
	public void setSalaryFrequency(String salaryFrequency) {
		this.salaryFrequency = salaryFrequency;
	}
	public String getSpouseSalaryFrequency() {
		return spouseSalaryFrequency;
	}
	public void setSpouseSalaryFrequency(String spouseSalaryFrequency) {
		this.spouseSalaryFrequency = spouseSalaryFrequency;
	}
	public String getTotalExpenseFrequency() {
		return totalExpenseFrequency;
	}
	public void setTotalExpenseFrequency(String totalExpenseFrequency) {
		this.totalExpenseFrequency = totalExpenseFrequency;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getLivingWithWife() {
		return livingWithWife;
	}
	public void setLivingWithWife(String livingWithWife) {
		this.livingWithWife = livingWithWife;
	}
	public String getDependentChild() {
		return dependentChild;
	}
	public void setDependentChild(String dependentChild) {
		this.dependentChild = dependentChild;
	}
	public String getIndustryType() {
		return industryType;
	}
	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getOtherOccupation() {
		return otherOccupation;
	}
	public void setOtherOccupation(String otherOccupation) {
		this.otherOccupation = otherOccupation;
	}
	public String getSalary() {
		return salary;
	}
	public void setSalary(String salary) {
		this.salary = salary;
	}
	public String getSpouseSalary() {
		return spouseSalary;
	}
	public void setSpouseSalary(String spouseSalary) {
		this.spouseSalary = spouseSalary;
	}
	public String getMortage() {
		return mortage;
	}
	public void setMortage(String mortage) {
		this.mortage = mortage;
	}
	public List<CoverInfoIns> getCover() {
		return cover;
	}
	public void setCover(List<CoverInfoIns> cover) {
		this.cover = cover;
	}
	public List<ExpenseInfoIns> getExpenses() {
		return expenses;
	}
	public void setExpenses(List<ExpenseInfoIns> expenses) {
		this.expenses = expenses;
	}
	public List<DebtsInfoIns> getDebts() {
		return debts;
	}
	public void setDebts(List<DebtsInfoIns> debts) {
		this.debts = debts;
	}
}
