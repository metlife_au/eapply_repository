package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrganizationReferences")

@XmlRootElement
public class OrganizationReferences {
	
	@XmlAttribute(name="organizationReference")
	 String organizationReference;

	public String getOrganizationReference() {
		return organizationReference;
	}

	public void setOrganizationReference(String organizationReference) {
		this.organizationReference = organizationReference;
	} 
	
	
}
