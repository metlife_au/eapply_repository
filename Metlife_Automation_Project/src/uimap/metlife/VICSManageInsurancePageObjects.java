/**
 * 
 */
package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

/**
 * @author sampath
 *
 */
public class VICSManageInsurancePageObjects extends PageObjectLibrary {

	/**
	 * 
	 */
	public VICSManageInsurancePageObjects() {
		// TODO Auto-generated constructor stub
		
	}	
	public static final By lnkChgInsuranceCover = getObjectIdentifaction("vics.manageinsurance.changeinsurancecover.link.css");
	public static final By lnkTransferExistingCover = getObjectIdentifaction("vics.manageinsurance.transferexistingcover.link.css");
	public static final By lnkIncreaseCoverDueToALifeEvent = getObjectIdentifaction("vics.manageinsurance.increasecoverduetoalifeevent.link.css");
	public static final By lnkTrackPreaviousApplications = getObjectIdentifaction("vics.manageinsurance.trackpreviousapplications.link.css");
	public static final By chkBoxConsent = getObjectIdentifaction("vics.manageinsurance.consent.chkbox.css");
	public static final By btnOk = getObjectIdentifaction("vics.manageinsurance.ok.btn.css");

}
