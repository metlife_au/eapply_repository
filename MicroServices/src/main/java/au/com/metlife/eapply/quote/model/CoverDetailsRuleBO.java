/**
 * 
 */
package au.com.metlife.eapply.quote.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.drools.KnowledgeBase;

/**
 * @author 199306
 *
 */
public class CoverDetailsRuleBO {

	private int age;
	private Double levelOfCommision;
	private int auraLoading;
	private int units;
	private int insuranceCategory;
	private int coverType;
	private Double iPMaxUnitsAllowed;
	private int deathUnits;
	private int tpdUnits;
	private double coverAmount;
	private int gender;
	private int smokerFlg;
	private int waitingPeriod;
	private int benefitPeriod;
	private double ttlDtOnly_DtTpdCover;
	private double totalDeath_TpdCover;
	private double totalIpCover;
	private double coverCost;
	private int costType;
	private double weeklyCost;
	private double yearlyCost;
	private double deathCostperUnit;
	private double tpdCostperUnit;
	private double calcWhiteCollarAmount;
	private double monthlyCost;
	private boolean ttlCvrExceedFlag=false;
	private boolean ageEligibleForDeathOnly=false;
	private boolean ageEligibleForDeathTpd=false;
	private boolean ageEligibleForIpCover=false;
	private boolean ageEligibleForTraumaCover=false;
	private String ruleFileName=null;
	private String insuranceCategoryStr=null;
	public static final int DEATH_TPD=0;
	public static final int DEATH_ONLY=1;
	public static final int IP_COVER=2;
	public static final int IP_COVER_SHORT=4;
         /*Added by Kamlesh*/
    public static final int TRAUMA_COVER=3;
	public static final int STANDARD_RATES=001;
	public static final int WHITE_COLLAR_RATES=002;
	public static final int PROFESSIONAL_RATES=003;
	public static final int WEEKLY_COST=0;
	public static final int MONTHLY_COST=1;
	public static final int YEARLY_COST=2;
	public static final int GENDER_FEMALE=0;
	public static final int GENDER_MALE=1;
	public static final int NON_SMOKER=0;
	public static final int SMOKER=1;
	public static final int OCUPATION_WHITE_COLOR=0;
	public static final int OCUPATION_NON_WORKING_SPOUSES=1;
	public static final int OCUPATION_LIGHT_SKILLED_MANUAL=2;
	public static final int OCUPATION_HEAVY_UNSKILLED_MANUAL=3;
	public static final int OCUPATION_SPECIAL_RISKS=4;
	public static final int OCUPATION_STANDARD=5;
	public static final int OCUPATION_PROFESSIONAL=6;
	public static final int WAITING_PERIOD_30=30;
	public static final int WAITING_PERIOD_45=45;
	public static final int WAITING_PERIOD_60=60;
	public static final int WAITING_PERIOD_65=65;
	public static final int WAITING_PERIOD_90=90;
	public static final int WAITING_PERIOD_820=820;
	public static final int WAITING_PERIOD_1_YEAR=111;
	public static final int WAITING_PERIOD_3_YEARS=333;
	public static final int BENEFIT_PERIOD_5=5;
	public static final int BENEFIT_PERIOD_2=2;
	public static final int BENEFIT_PERIOD_AGE_60=666;
	public static final int BENEFIT_PERIOD_AGE_65=665;
	public static final int BENEFIT_PERIOD_AGE_67=667;
    public static final int OCUPATION_STANDARD_PREFER = 100;
    public static final int OCUPATION_WHITECOLLAR_PREFER = 200;
    public static final int OCUPATION_PROFESSIONAL_PREFER = 300;
    public static final int OCUPATION_NONWORKING_PREFER=400;
    public static final int OCUPATION_LIGHTSKILLED_PREFER=500;
    public static final int OCUPATION_HEAVYUNSKILLED_PREFER=600;
    public static final int OCUPATION_SPEACIALRISK_PREFER = 700;
    private int existingCategoryPrefer;
    private int additionalCategoryPrefer;
    private int calcExistingOccupCategoryPrefer;
    private int calcAdditionalOccupCategoryPrefer;
    private String morePreferCategory;
    private int morePreferRatingCategory;
    private KnowledgeBase kbase = null;
	public static final int ABALONE_DRIVER=1;
	public static final int ABATTOIR_WORKER_SUPERVISOR_NO_MANUAL=2;
	public static final int UNITISED=0;
	public static final int FIXED=0;
	private String insuranceClass=null;
	private String occupCategory=null;
	private String custOccupation=null;
	private int unitOrFixed;
	private String ruleID=null;
	private double currentRuleVersion;
	private Map<String, String> occupAlisCodeMap = new HashMap<>();

	/**
	 * Added for CR2.10 & CR2.15
	 */
	private String dUOccupCategory;
	private String dFOccupCategory;
	private String dTPDUOccupCategory;
	private String dTPDFOccupCategory;
	private String iPUOccupCategory;
	private String iPFOccupCategory;
	private String traumaUnitOccCategory;
	private String traumaFixedOccCategory;
	private double maxIpCoverAmount=0;
	private double maxDC_DCTPDCoverAmount=0;
	private double annualSal=0;
	private double ipMonthlyBenefit=0;
	private double twoYearRate=0;
	private double age60Rate=0;
	private double deathMinAmount=0;
	private double deathMaxAmount=0;
	private double tpdMinAmount=0;
	private double tpdMaxAmount=0;
	private double ipMinAmount=0;
	private double ipMaxAmount=0;
	private double traumaMinAmount=0;
	private double traumaMaxAmount=0;
	private String fundCode=null;
	private String corpFundId=null;
	private int userAALunits;
	private String aALevelStatus=null;
	private double unitisedValue=0;
	private int aALLimit;
	private double totalCoverAmt;
	private double revisedCoverAmt;
	private int originalScale;
	private int revisedScale;
	private double maxThresholdCvrAmt;
	private List<String> occupationList =new ArrayList<>();
	private List<SelectItem> industryList=new ArrayList<>();
	private Map<String, String> industryMap=new HashMap<>();
	private boolean askManualQ=false;
	private String industryCode=null;
	private boolean deathAndTpdApplied=false;
	private double salaryPercentage=0.0;
    /*Added By Prasanna durga -- REIS changesprivate String memberType=null;
    Added By Prasanna durga -- REIS changesprivate String stateCode=null;*/
	private double stampDutyRate=0.0;
	private Map<String, String> industryAskManualMap=new HashMap<>();
	private Map<String, String> industryAskProfessionalMap=new HashMap<>();
	private boolean withInAALLimit=false;
	private boolean bandChange = false;
	private boolean aalStatus = false;
	private String occupationWarningMsg = null;
	private String stateCode=null;
	private String memberType=null;
	private String q8_PROFF_Q_Val=null;
	private String q9_PROFF_Q_Val=null;
	private String q10_PROFF_Q_Val=null;
	private boolean unitsed=false;
	private int ipUnits=0;
	private BigDecimal ipUnitMultipFactor=null;
	private Date tpdCoverStartDate=null;
	private boolean beforeAug2013=false;
	private int defaultDeathUnits;
	private int defaultTpdUnits;
	private String manageType=null;
	private BigDecimal ipStampDutyRate;
	private BigDecimal tpdStampDutyRate;
	private BigDecimal ipMultiplyPolicyDiscount;
	private boolean isDeathFilled=false;
	private boolean isTPDFilled=false;
	private boolean isIPFilled=false;
	public String getManageType() {
		return manageType;
	}

	public void setManageType(String manageType) {
		this.manageType = manageType;
	}

	public int getIpUnits() {
		return ipUnits;
	}

	public void setIpUnits(int ipUnits) {
		this.ipUnits = ipUnits;
	}

	public boolean isUnitsed() {
		return unitsed;
	}

	public void setUnitsed(boolean unitsed) {
		this.unitsed = unitsed;
	}

	public String getOccupationWarningMsg() {
		return occupationWarningMsg;
	}

	public void setOccupationWarningMsg(String occupationWarningMsg) {
		this.occupationWarningMsg = occupationWarningMsg;
	}

	public boolean isWithInAALLimit() {
		return withInAALLimit;
	}

	public void setWithInAALLimit(boolean withInAALLimit) {
		this.withInAALLimit = withInAALLimit;
	}

	public double getStampDutyRate() {
		return stampDutyRate;
	}

	public void setStampDutyRate(double stampDutyRate) {
		this.stampDutyRate = stampDutyRate;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public int getAALLimit() {
		return aALLimit;
	}

	public void setAALLimit(int limit) {
		aALLimit = limit;
	}

	public double getUnitisedValue() {
		return unitisedValue;
	}

	public void setUnitisedValue(double unitisedValue) {
		this.unitisedValue = unitisedValue;
	}

	public String getAALevelStatus() {
		return aALevelStatus;
	}

	public void setAALevelStatus(String levelStatus) {
		aALevelStatus = levelStatus;
	}

	public int getUserAALunits() {
		return userAALunits;
	}

	public void setUserAALunits(int userAALunits) {
		this.userAALunits = userAALunits;
	}

	/**
	 * @return the fundCode
	 */
	public String getFundCode() {
		return fundCode;
	}

	/**
	 * @param fundCode the fundCode to set
	 */
	public void setFundCode(String fundCode) {
		this.fundCode = fundCode;
	}

	/**
	 * @return the deathMaxAmount
	 */
	public double getDeathMaxAmount() {
		return deathMaxAmount;
	}

	/**
	 * @param deathMaxAmount the deathMaxAmount to set
	 */
	public void setDeathMaxAmount(double deathMaxAmount) {
		this.deathMaxAmount = deathMaxAmount;
	}

	/**
	 * @return the deathMinAmount
	 */
	public double getDeathMinAmount() {
		return deathMinAmount;
	}

	/**
	 * @param deathMinAmount the deathMinAmount to set
	 */
	public void setDeathMinAmount(double deathMinAmount) {
		this.deathMinAmount = deathMinAmount;
	}

	/**
	 * @return the ipMaxAmount
	 */
	public double getIpMaxAmount() {
		return ipMaxAmount;
	}

	/**
	 * @param ipMaxAmount the ipMaxAmount to set
	 */
	public void setIpMaxAmount(double ipMaxAmount) {
		this.ipMaxAmount = ipMaxAmount;
	}

	/**
	 * @return the ipMinAmount
	 */
	public double getIpMinAmount() {
		return ipMinAmount;
	}

	/**
	 * @param ipMinAmount the ipMinAmount to set
	 */
	public void setIpMinAmount(double ipMinAmount) {
		this.ipMinAmount = ipMinAmount;
	}

	/**
	 * @return the tpdMaxAmount
	 */
	public double getTpdMaxAmount() {
		return tpdMaxAmount;
	}

	/**
	 * @param tpdMaxAmount the tpdMaxAmount to set
	 */
	public void setTpdMaxAmount(double tpdMaxAmount) {
		this.tpdMaxAmount = tpdMaxAmount;
	}

	/**
	 * @return the tpdMinAmount
	 */
	public double getTpdMinAmount() {
		return tpdMinAmount;
	}

	/**
	 * @param tpdMinAmount the tpdMinAmount to set
	 */
	public void setTpdMinAmount(double tpdMinAmount) {
		this.tpdMinAmount = tpdMinAmount;
	}

	/**
	 * @return the traumaMaxAmount
	 */
	public double getTraumaMaxAmount() {
		return traumaMaxAmount;
	}

	/**
	 * @param traumaMaxAmount the traumaMaxAmount to set
	 */
	public void setTraumaMaxAmount(double traumaMaxAmount) {
		this.traumaMaxAmount = traumaMaxAmount;
	}

	/**
	 * @return the traumaMinAmount
	 */
	public double getTraumaMinAmount() {
		return traumaMinAmount;
	}

	/**
	 * @param traumaMinAmount the traumaMinAmount to set
	 */
	public void setTraumaMinAmount(double traumaMinAmount) {
		this.traumaMinAmount = traumaMinAmount;
	}

	/**
	 * @return the maxDC_DCTPDCoverAmount
	 */
	public double getMaxDC_DCTPDCoverAmount() {
		return maxDC_DCTPDCoverAmount;
	}

	/**
	 * @param maxDC_DCTPDCoverAmount the maxDC_DCTPDCoverAmount to set
	 */
	public void setMaxDC_DCTPDCoverAmount(double maxDC_DCTPDCoverAmount) {
		this.maxDC_DCTPDCoverAmount = maxDC_DCTPDCoverAmount;
	}

	/**
	 * @return the maxIpCoverAmount
	 */
	public double getMaxIpCoverAmount() {
		return maxIpCoverAmount;
	}

	/**
	 * @param maxIpCoverAmount the maxIpCoverAmount to set
	 */
	public void setMaxIpCoverAmount(double maxIpCoverAmount) {
		this.maxIpCoverAmount = maxIpCoverAmount;
	}

	/**
	 * @return the occupCategory
	 */
	public String getOccupCategory() {
		return occupCategory;
	}

	/**
	 * @param occupCategory the occupCategory to set
	 */
	public void setOccupCategory(String occupCategory) {
		this.occupCategory = occupCategory;
	}

	/**
	 * @return the coverType
	 */
	public int getCoverType() {
		return coverType;
	}

	/**
	 * @param coverType the coverType to set
	 */
	public void setCoverType(int coverType) {
		this.coverType = coverType;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * @return the insuranceCategory
	 */
	public int getInsuranceCategory() {
		return insuranceCategory;
	}

	/**
	 * @param insuranceCategory the insuranceCategory to set
	 */
	public void setInsuranceCategory(int insuranceCategory) {
		this.insuranceCategory = insuranceCategory;
	}

	/**
	 * @return the units
	 */
	public int getUnits() {
		return units;
	}

	/**
	 * @param units the units to set
	 */
	public void setUnits(int units) {
		this.units = units;
	}

	/**
	 * @return the coverAmount
	 */
	public double getCoverAmount() {
		return coverAmount;
	}

	/**
	 * @param coverAmount the coverAmount to set
	 */
	public void setCoverAmount(double coverAmount) {
		this.coverAmount = coverAmount;
	}

	/**
	 * @return the totalDeath_TpdCover
	 */
	public double getTotalDeath_TpdCover() {
		return totalDeath_TpdCover;
	}

	/**
	 * @param totalDeath_TpdCover the totalDeath_TpdCover to set
	 */
	public void setTotalDeath_TpdCover(double totalDeath_TpdCover) {
		this.totalDeath_TpdCover = totalDeath_TpdCover;
	}

	/**
	 * @return the ttlDtOnly_DtTpdCover
	 */
	public double getTtlDtOnly_DtTpdCover() {
		return ttlDtOnly_DtTpdCover;
	}

	/**
	 * @param ttlDtOnly_DtTpdCover the ttlDtOnly_DtTpdCover to set
	 */
	public void setTtlDtOnly_DtTpdCover(double ttlDtOnly_DtTpdCover) {
		this.ttlDtOnly_DtTpdCover = ttlDtOnly_DtTpdCover;
	}

	/**
	 * @return the totalIpCover
	 */
	public double getTotalIpCover() {
		return totalIpCover;
	}

	/**
	 * @param totalIpCover the totalIpCover to set
	 */
	public void setTotalIpCover(double totalIpCover) {
		this.totalIpCover = totalIpCover;
	}

	/**
	 * @return the ttlCvrExceedFlag
	 */
	public boolean isTtlCvrExceedFlag() {
		return ttlCvrExceedFlag;
	}

	/**
	 * @param ttlCvrExceedFlag the ttlCvrExceedFlag to set
	 */
	public void setTtlCvrExceedFlag(boolean ttlCvrExceedFlag) {
		this.ttlCvrExceedFlag = ttlCvrExceedFlag;
	}

	/**
	 * @return the ageEligibleForDeathOnly
	 */
	public boolean isAgeEligibleForDeathOnly() {
		return ageEligibleForDeathOnly;
	}

	/**
	 * @param ageEligibleForDeathOnly the ageEligibleForDeathOnly to set
	 */
	public void setAgeEligibleForDeathOnly(boolean ageEligibleForDeathOnly) {
		this.ageEligibleForDeathOnly = ageEligibleForDeathOnly;
	}

	/**
	 * @return the ageEligibleForDeathTpd
	 */
	public boolean isAgeEligibleForDeathTpd() {
		return ageEligibleForDeathTpd;
	}

	/**
	 * @param ageEligibleForDeathTpd the ageEligibleForDeathTpd to set
	 */
	public void setAgeEligibleForDeathTpd(boolean ageEligibleForDeathTpd) {
		this.ageEligibleForDeathTpd = ageEligibleForDeathTpd;
	}

	/**
	 * @return the ageEligibleForIpCover
	 */
	public boolean isAgeEligibleForIpCover() {
		return ageEligibleForIpCover;
	}

	/**
	 * @param ageEligibleForIpCover the ageEligibleForIpCover to set
	 */
	public void setAgeEligibleForIpCover(boolean ageEligibleForIpCover) {
		this.ageEligibleForIpCover = ageEligibleForIpCover;
	}

	/**
	 * @return the coverCost
	 */
	public double getCoverCost() {
		return coverCost;
	}

	/**
	 * @param coverCost the coverCost to set
	 */
	public void setCoverCost(double coverCost) {
		this.coverCost = coverCost;
	}

	/**
	 * @return the monthlyCost
	 */
	public double getMonthlyCost() {
		return monthlyCost;
	}

	/**
	 * @param monthlyCost the monthlyCost to set
	 */
	public void setMonthlyCost(double monthlyCost) {
		this.monthlyCost = monthlyCost;
	}

	/**
	 * @return the weeklyCost
	 */
	public double getWeeklyCost() {
		return weeklyCost;
	}

	/**
	 * @param weeklyCost the weeklyCost to set
	 */
	public void setWeeklyCost(double weeklyCost) {
		this.weeklyCost = weeklyCost;
	}

	/**
	 * @return the yearlyCost
	 */
	public double getYearlyCost() {
		return yearlyCost;
	}

	/**
	 * @param yearlyCost the yearlyCost to set
	 */
	public void setYearlyCost(double yearlyCost) {
		this.yearlyCost = yearlyCost;
	}

	/**
	 * @return the costType
	 */
	public int getCostType() {
		return costType;
	}

	/**
	 * @param costType the costType to set
	 */
	public void setCostType(int costType) {
		this.costType = costType;
	}

	/**
	 * @return the gender
	 */
	public int getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(int gender) {
		this.gender = gender;
	}

	/**
	 * @return the smokerFlg
	 */
	public int getSmokerFlg() {
		return smokerFlg;
	}

	/**
	 * @param smokerFlg the smokerFlg to set
	 */
	public void setSmokerFlg(int smokerFlg) {
		this.smokerFlg = smokerFlg;
	}

	/**
	 * @return the benefitPeriod
	 */
	public int getBenefitPeriod() {
		return benefitPeriod;
	}

	/**
	 * @param benefitPeriod the benefitPeriod to set
	 */
	public void setBenefitPeriod(int benefitPeriod) {
		this.benefitPeriod = benefitPeriod;
	}

	/**
	 * @return the waitingPeriod
	 */
	public int getWaitingPeriod() {
		return waitingPeriod;
	}

	/**
	 * @param waitingPeriod the waitingPeriod to set
	 */
	public void setWaitingPeriod(int waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}

	/**
	 * @return the ruleFileName
	 */
	public String getRuleFileName() {
		return ruleFileName;
	}

	/**
	 * @param ruleFileName the ruleFileName to set
	 */
	public void setRuleFileName(String ruleFileName) {
		this.ruleFileName = ruleFileName;
	}

	private String coverTypeStr=null;


	public String getCoverTypeStr() {
		return coverTypeStr;
	}

	public void setCoverTypeStr(String coverTypeStr) {
		this.coverTypeStr = coverTypeStr;
	}

	/**
	 * @return the insuranceClass
	 */
	public String getInsuranceClass() {
		return insuranceClass;
	}

	/**
	 * @param insuranceClass the insuranceClass to set
	 */
	public void setInsuranceClass(String insuranceClass) {
		this.insuranceClass = insuranceClass;
	}

	/**
	 * @return the inuranceCategory
	 */


	/**
	 * @return the custOccupation
	 */
	public String getCustOccupation() {
		return custOccupation;
	}

	/**
	 * @param custOccupation the custOccupation to set
	 */
	public void setCustOccupation(String custOccupation) {
		this.custOccupation = custOccupation;
	}

	/**
	 * @return the unitOrFixed
	 */
	public int getUnitOrFixed() {
		return unitOrFixed;
	}

	/**
	 * @param unitOrFixed the unitOrFixed to set
	 */
	public void setUnitOrFixed(int unitOrFixed) {
		this.unitOrFixed = unitOrFixed;
	}

	public double getCalcWhiteCollarAmount() {
		return calcWhiteCollarAmount;
	}

	public void setCalcWhiteCollarAmount(double calcWhiteCollarAmount) {
		this.calcWhiteCollarAmount = calcWhiteCollarAmount;
	}

	/**
	 * @return the currentRuleVersion
	 */
	public double getCurrentRuleVersion() {
		return currentRuleVersion;
	}

	/**
	 * @param currentRuleVersion the currentRuleVersion to set
	 */
	public void setCurrentRuleVersion(double currentRuleVersion) {
		this.currentRuleVersion = currentRuleVersion;
	}

	/**
	 * @return the ruleID
	 */
	public String getRuleID() {
		return ruleID;
	}

	/**
	 * @param ruleID the ruleID to set
	 */
	public void setRuleID(String ruleID) {
		this.ruleID = ruleID;
	}

	/**
	 * @return the additionalCategoryPrefer
	 */
	public int getAdditionalCategoryPrefer() {
		return additionalCategoryPrefer;
	}

	/**
	 * @param additionalCategoryPrefer the additionalCategoryPrefer to set
	 */
	public void setAdditionalCategoryPrefer(int additionalCategoryPrefer) {
		this.additionalCategoryPrefer = additionalCategoryPrefer;
	}

	/**
	 * @return the calcAdditionalOccupCategoryPrefer
	 */
	public int getCalcAdditionalOccupCategoryPrefer() {
		return calcAdditionalOccupCategoryPrefer;
	}

	/**
	 * @param calcAdditionalOccupCategoryPrefer the calcAdditionalOccupCategoryPrefer to set
	 */
	public void setCalcAdditionalOccupCategoryPrefer(
			int calcAdditionalOccupCategoryPrefer) {
		this.calcAdditionalOccupCategoryPrefer = calcAdditionalOccupCategoryPrefer;
	}

	/**
	 * @return the calcExistingOccupCategoryPrefer
	 */
	public int getCalcExistingOccupCategoryPrefer() {
		return calcExistingOccupCategoryPrefer;
	}

	/**
	 * @param calcExistingOccupCategoryPrefer the calcExistingOccupCategoryPrefer to set
	 */
	public void setCalcExistingOccupCategoryPrefer(
			int calcExistingOccupCategoryPrefer) {
		this.calcExistingOccupCategoryPrefer = calcExistingOccupCategoryPrefer;
	}

	/**
	 * @return the existingCategoryPrefer
	 */
	public int getExistingCategoryPrefer() {
		return existingCategoryPrefer;
	}

	/**
	 * @param existingCategoryPrefer the existingCategoryPrefer to set
	 */
	public void setExistingCategoryPrefer(int existingCategoryPrefer) {
		this.existingCategoryPrefer = existingCategoryPrefer;
	}

	/**
	 * @return the morePreferCategory
	 */
	public String getMorePreferCategory() {
		return morePreferCategory;
	}

	/**
	 * @param morePreferCategory the morePreferCategory to set
	 */
	public void setMorePreferCategory(String morePreferCategory) {
		this.morePreferCategory = morePreferCategory;
	}

	/**
	 * @return the morePreferRatingCategory
	 */
	public int getMorePreferRatingCategory() {
		return morePreferRatingCategory;
	}

	/**
	 * @param morePreferRatingCategory the morePreferRatingCategory to set
	 */
	public void setMorePreferRatingCategory(int morePreferRatingCategory) {
		this.morePreferRatingCategory = morePreferRatingCategory;
	}

	public String getDUOccupCategory() {
		return dUOccupCategory;
	}

	public void setDUOccupCategory(String occupCategory) {
		dUOccupCategory = occupCategory;
	}

	public String getDFOccupCategory() {
		return dFOccupCategory;
	}

	public void setDFOccupCategory(String occupCategory) {
		dFOccupCategory = occupCategory;
	}

	public String getDTPDUOccupCategory() {
		return dTPDUOccupCategory;
	}

	public void setDTPDUOccupCategory(String occupCategory) {
		dTPDUOccupCategory = occupCategory;
	}

	public String getDTPDFOccupCategory() {
		return dTPDFOccupCategory;
	}

	public void setDTPDFOccupCategory(String occupCategory) {
		dTPDFOccupCategory = occupCategory;
	}

	public String getIPUOccupCategory() {
		return iPUOccupCategory;
	}

	public void setIPUOccupCategory(String occupCategory) {
		iPUOccupCategory = occupCategory;
	}

	public String getIPFOccupCategory() {
		return iPFOccupCategory;
	}

	public void setIPFOccupCategory(String occupCategory) {
		iPFOccupCategory = occupCategory;
	}

	/**
	 * @return the insuranceCategoryStr
	 */
	public String getInsuranceCategoryStr() {
		return insuranceCategoryStr;
	}

	/**
	 * @param insuranceCategoryStr the insuranceCategoryStr to set
	 */
	public void setInsuranceCategoryStr(String insuranceCategoryStr) {
		this.insuranceCategoryStr = insuranceCategoryStr;
	}

	public int getDeathUnits() {
		return deathUnits;
	}

	public void setDeathUnits(int deathUnits) {
		this.deathUnits = deathUnits;
	}

	public int getTpdUnits() {
		return tpdUnits;
	}

	public void setTpdUnits(int tpdUnits) {
		this.tpdUnits = tpdUnits;
	}

	/**
	 * @return the annualSal
	 */
	public double getAnnualSal() {
		return annualSal;
	}

	/**
	 * @param annualSal the annualSal to set
	 */
	public void setAnnualSal(double annualSal) {
		this.annualSal = annualSal;
	}

	/**
	 * @return the ipMonthlyBenefit
	 */
	public double getIpMonthlyBenefit() {
		return ipMonthlyBenefit;
	}

	/**
	 * @param ipMonthlyBenefit the ipMonthlyBenefit to set
	 */
	public void setIpMonthlyBenefit(double ipMonthlyBenefit) {
		this.ipMonthlyBenefit = ipMonthlyBenefit;
	}

	/**
	 * @return the age60Rate
	 */
	public double getAge60Rate() {
		return age60Rate;
	}

	/**
	 * @param age60Rate the age60Rate to set
	 */
	public void setAge60Rate(double age60Rate) {
		this.age60Rate = age60Rate;
	}

	/**
	 * @return the twoYearRate
	 */
	public double getTwoYearRate() {
		return twoYearRate;
	}

	/**
	 * @param twoYearRate the twoYearRate to set
	 */
	public void setTwoYearRate(double twoYearRate) {
		this.twoYearRate = twoYearRate;
	}

    public boolean isAgeEligibleForTraumaCover() {
        return ageEligibleForTraumaCover;
    }

    public void setAgeEligibleForTraumaCover(boolean ageEligibleForTraumaCover) {
        this.ageEligibleForTraumaCover = ageEligibleForTraumaCover;
    }

	/**
	 * @return the traumaFixedOccCategory
	 */
	public String getTraumaFixedOccCategory() {
		return traumaFixedOccCategory;
	}

	/**
	 * @param traumaFixedOccCategory the traumaFixedOccCategory to set
	 */
	public void setTraumaFixedOccCategory(String traumaFixedOccCategory) {
		this.traumaFixedOccCategory = traumaFixedOccCategory;
	}

	/**
	 * @return the traumaUnitOccCategory
	 */
	public String getTraumaUnitOccCategory() {
		return traumaUnitOccCategory;
	}

	/**
	 * @param traumaUnitOccCategory the traumaUnitOccCategory to set
	 */
	public void setTraumaUnitOccCategory(String traumaUnitOccCategory) {
		this.traumaUnitOccCategory = traumaUnitOccCategory;
	}

	public Double getIPMaxUnitsAllowed() {
		return iPMaxUnitsAllowed;
	}

	public void setIPMaxUnitsAllowed(Double maxUnitsAllowed) {
		iPMaxUnitsAllowed = maxUnitsAllowed;
	}

	/*public StatelessKnowledgeSession getKSession() {
		return kSession;
	}

	public void setKSession(StatelessKnowledgeSession session) {
		kSession = session;
	}*/

	public int getAuraLoading() {
		return auraLoading;
	}

	public void setAuraLoading(int auraLoading) {
		this.auraLoading = auraLoading;
	}	

	public Double getLevelOfCommision() {
		return levelOfCommision;
	}

	public void setLevelOfCommision(Double levelOfCommision) {
		this.levelOfCommision = levelOfCommision;
	}

	public double getDeathCostperUnit() {
		return deathCostperUnit;
	}

	public void setDeathCostperUnit(double deathCostperUnit) {
		this.deathCostperUnit = deathCostperUnit;
	}

	public double getTpdCostperUnit() {
		return tpdCostperUnit;
	}

	public void setTpdCostperUnit(double tpdCostperUnit) {
		this.tpdCostperUnit = tpdCostperUnit;
	}

	public KnowledgeBase getKbase() {
		return kbase;
	}

	public void setKbase(KnowledgeBase kbase) {
		this.kbase = kbase;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public double getTotalCoverAmt() {
		return totalCoverAmt;
	}

	public void setTotalCoverAmt(double totalCoverAmt) {
		this.totalCoverAmt = totalCoverAmt;
	}

	public int getOriginalScale() {
		return originalScale;
	}

	public void setOriginalScale(int originalScale) {
		this.originalScale = originalScale;
	}

	public int getRevisedScale() {
		return revisedScale;
	}

	public void setRevisedScale(int revisedScale) {
		this.revisedScale = revisedScale;
	}

	public double getRevisedCoverAmt() {
		return revisedCoverAmt;
	}

	public void setRevisedCoverAmt(double revisedCoverAmt) {
		this.revisedCoverAmt = revisedCoverAmt;
	}

	public double getMaxThresholdCvrAmt() {
		return maxThresholdCvrAmt;
	}

	public void setMaxThresholdCvrAmt(double maxThresholdCvrAmt) {
		this.maxThresholdCvrAmt = maxThresholdCvrAmt;
	}

	

	public boolean isAskManualQ() {
		return askManualQ;
	}

	public void setAskManualQ(boolean askManualQ) {
		this.askManualQ = askManualQ;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public boolean isDeathAndTpdApplied() {
		return deathAndTpdApplied;
	}

	public void setDeathAndTpdApplied(boolean deathAndTpdApplied) {
		this.deathAndTpdApplied = deathAndTpdApplied;
	}

	public double getSalaryPercentage() {
		return salaryPercentage;
	}

	public void setSalaryPercentage(double salaryPercentage) {
		this.salaryPercentage = salaryPercentage;
	}




	public boolean isBandChange() {
		return bandChange;
	}

	public void setBandChange(boolean bandChange) {
		this.bandChange = bandChange;
	}

	public List<String> getOccupationList() {
		return occupationList;
	}

	public void setOccupationList(List<String> occupationList) {
		this.occupationList = occupationList;
	}

	public Map<String, String> getOccupAlisCodeMap() {
		return occupAlisCodeMap;
	}

	public void setOccupAlisCodeMap(Map<String, String> occupAlisCodeMap) {
		this.occupAlisCodeMap = occupAlisCodeMap;
	}

	public Map<String, String> getIndustryAskManualMap() {
		return industryAskManualMap;
	}

	public void setIndustryAskManualMap(Map<String, String> industryAskManualMap) {
		this.industryAskManualMap = industryAskManualMap;
	}

	public Map<String, String> getIndustryAskProfessionalMap() {
		return industryAskProfessionalMap;
	}

	public void setIndustryAskProfessionalMap(
			Map<String, String> industryAskProfessionalMap) {
		this.industryAskProfessionalMap = industryAskProfessionalMap;
	}

	public List<SelectItem> getIndustryList() {
		return industryList;
	}

	public void setIndustryList(List<SelectItem> industryList) {
		this.industryList = industryList;
	}

	public Map<String, String> getIndustryMap() {
		return industryMap;
	}

	public void setIndustryMap(Map<String, String> industryMap) {
		this.industryMap = industryMap;
	}

	public boolean isAalStatus() {
		return aalStatus;
	}

	public void setAalStatus(boolean aalStatus) {
		this.aalStatus = aalStatus;
	}

	public String getQ10_PROFF_Q_Val() {
		return q10_PROFF_Q_Val;
	}

	public void setQ10_PROFF_Q_Val(String val) {
		q10_PROFF_Q_Val = val;
	}

	public String getQ8_PROFF_Q_Val() {
		return q8_PROFF_Q_Val;
	}

	public void setQ8_PROFF_Q_Val(String val) {
		q8_PROFF_Q_Val = val;
	}

	public String getQ9_PROFF_Q_Val() {
		return q9_PROFF_Q_Val;
	}

	public void setQ9_PROFF_Q_Val(String val) {
		q9_PROFF_Q_Val = val;
	}

	public BigDecimal getIpUnitMultipFactor() {
		return ipUnitMultipFactor;
	}

	public void setIpUnitMultipFactor(BigDecimal ipUnitMultipFactor) {
		this.ipUnitMultipFactor = ipUnitMultipFactor;
	}

	public Date getTpdCoverStartDate() {
		return tpdCoverStartDate;
	}

	public void setTpdCoverStartDate(Date tpdCoverStartDate) {
		this.tpdCoverStartDate = tpdCoverStartDate;
	}

	public boolean isBeforeAug2013() {
		return beforeAug2013;
	}

	public void setBeforeAug2013(boolean beforeAug2013) {
		this.beforeAug2013 = beforeAug2013;
	}

	public int getDefaultDeathUnits() {
		return defaultDeathUnits;
	}

	public void setDefaultDeathUnits(int defaultDeathUnits) {
		this.defaultDeathUnits = defaultDeathUnits;
	}

	public int getDefaultTpdUnits() {
		return defaultTpdUnits;
	}

	public void setDefaultTpdUnits(int defaultTpdUnits) {
		this.defaultTpdUnits = defaultTpdUnits;
	}

	public String getCorpFundId() {
		return corpFundId;
	}

	public void setCorpFundId(String corpFundId) {
		this.corpFundId = corpFundId;
	}

	public BigDecimal getIpStampDutyRate() {
		return ipStampDutyRate;
	}

	public void setIpStampDutyRate(BigDecimal ipStampDutyRate) {
		this.ipStampDutyRate = ipStampDutyRate;
	}

	public BigDecimal getTpdStampDutyRate() {
		return tpdStampDutyRate;
	}

	public void setTpdStampDutyRate(BigDecimal tpdStampDutyRate) {
		this.tpdStampDutyRate = tpdStampDutyRate;
	}

	public BigDecimal getIpMultiplyPolicyDiscount() {
		return ipMultiplyPolicyDiscount;
	}

	public void setIpMultiplyPolicyDiscount(BigDecimal ipMultiplyPolicyDiscount) {
		this.ipMultiplyPolicyDiscount = ipMultiplyPolicyDiscount;
	}

	public boolean isDeathFilled() {
		return isDeathFilled;
	}

	public void setDeathFilled(boolean isDeathFilled) {
		this.isDeathFilled = isDeathFilled;
	}

	public boolean isTPDFilled() {
		return isTPDFilled;
	}

	public void setTPDFilled(boolean isTPDFilled) {
		this.isTPDFilled = isTPDFilled;
	}

	public boolean isIPFilled() {
		return isIPFilled;
	}

	public void setIPFilled(boolean isIPFilled) {
		this.isIPFilled = isIPFilled;
	}


}
