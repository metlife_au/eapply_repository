package au.com.metlife.eapply.b2b.serviceimpl;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.eapply.b2b.model.Eappstring;
import au.com.metlife.eapply.b2b.model.EappstringRepository;
import au.com.metlife.eapply.b2b.service.B2bService;
import au.com.metlife.eapply.b2b.utility.B2bServiceHelper;

@Service
public class B2bServiceImpl implements B2bService {

	 private final EappstringRepository repository;
	 
	 @Autowired
	    public B2bServiceImpl(final EappstringRepository repository) {
	        this.repository = repository;
	    }
	 
	@Override
	@Transactional
	public Eappstring generateToken(String input, String outhToken) {
		// TODO Auto-generated method stub	
		Eappstring eappstring = new Eappstring(input,outhToken, "Active", new Date(), "");
		System.out.println("inside gen token");
		eappstring = repository.save(eappstring);
		return eappstring;
	}
	@Override
	@Transactional
	public Eappstring retriveClientData(String token){		
		//Eappstring eappstring = repository.findById(Long.parseLong(token));
		Eappstring eappstring =repository.findBySecureToken(token);
		System.out.println(eappstring.getSecurestring());		
		return eappstring;
	}
}
