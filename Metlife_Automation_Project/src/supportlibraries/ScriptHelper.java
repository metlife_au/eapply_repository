package supportlibraries;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.cognizant.framework.CraftDataTable;
import com.cognizant.framework.selenium.SeleniumReport;

/**
 * Wrapper class for common framework objects, to be used across the entire test
 * case and dependent libraries
 * 
 * @author Cognizant
 */
public class ScriptHelper {
	private final CraftDataTable dataTable;
	private final SeleniumReport report;
	private final WebDriver driver;
	private Map<String, Object> sessionThreadData = null;

	/**
	 * Constructor to initialize all the objects wrapped by the
	 * {@link ScriptHelper} class
	 * 
	 * @param dataTable
	 *            The {@link CraftDataTable} object
	 * @param report
	 *            The {@link SeleniumReport} object
	 * @param driver
	 *            The {@link WebDriver} object
	 */
	public ScriptHelper(CraftDataTable dataTable, SeleniumReport report,
			WebDriver driver) {
		this.dataTable = dataTable;
		this.report = report;
		this.driver = driver;
		
		if(this.sessionThreadData==null){
			this.sessionThreadData=new HashMap<String, Object>();
		}
	}

	public void storeSessionThreadData(String key, Object value) {
		sessionThreadData.put(key, value);
	}

	public String getStringValueFromSessionThreadData(String key) {
		return (String) sessionThreadData.get(key);
	}

	public Object getValueFromSessionThreadData(String key) {
		return sessionThreadData.get(key);
	}
	
	public Map<String, Object> getSessionThreadData(){
		return sessionThreadData;
	}
	
	/**
	 * Function to get the {@link CraftDataTable} object
	 * 
	 * @return The {@link CraftDataTable} object
	 */
	public CraftDataTable getDataTable() {
		return dataTable;
	}

	/**
	 * Function to get the {@link SeleniumReport} object
	 * 
	 * @return The {@link SeleniumReport} object
	 */
	public SeleniumReport getReport() {
		return report;
	}

	/**
	 * Function to get the {@link WebDriver} object
	 * 
	 * @return The {@link WebDriver} object
	 */
	public WebDriver getDriver() {
		return driver;
	}
}