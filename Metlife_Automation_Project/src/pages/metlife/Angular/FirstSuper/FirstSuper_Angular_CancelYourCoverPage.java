package pages.metlife.Angular.FirstSuper;


import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import org.openqa.selenium.Keys;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class FirstSuper_Angular_CancelYourCoverPage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public FirstSuper_Angular_CancelYourCoverPage CancelYourCoverCover_FirstSuper() {
		CancelYourCover();		
		confirmation();
		
		return new FirstSuper_Angular_CancelYourCoverPage(scriptHelper);
	}

	public FirstSuper_Angular_CancelYourCoverPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void CancelYourCover() {

		String EmailId = dataTable.getData("FirstSuper_CancelCover", "EmailId");
		String TypeofContact = dataTable.getData("FirstSuper_CancelCover", "TypeofContact");
		String ContactNumber = dataTable.getData("FirstSuper_CancelCover", "ContactNumber");
		String TimeofContact = dataTable.getData("FirstSuper_CancelCover", "TimeofContact");
		
		String CancelDeath = dataTable.getData("FirstSuper_CancelCover", "CancelDeath");
		String CancelTPD = dataTable.getData("FirstSuper_CancelCover", "CancelTPD");
		String CancelIP = dataTable.getData("FirstSuper_CancelCover", "CancelIP");
		
		
		
		try{
			
			
			
			//****Click on Cancel Your Cover Link*****\\
			
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h5[contains(text(),'Cancel your cover ')]"))).click();
			
			report.updateTestLog("Cancel Your Cover", "Cancel Your Cover Link is Clicked", Status.PASS);
		
			
			
			//*****Enter the Email Id*****\\	
			
			wait.until(ExpectedConditions.elementToBeClickable(By.name("cancelCoverEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("cancelCoverEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(500);

			//*****Click on the Contact Number Type****\\			
				
			Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));		
			Contact.selectByVisibleText(TypeofContact);			
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);
				
				
			//****Enter the Contact Number****\\
			
				sleep(250);
				wait.until(ExpectedConditions.elementToBeClickable(By.name("cancelCoverPhoneNo"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
				sleep(500);
				wait.until(ExpectedConditions.elementToBeClickable(By.name("cancelCoverPhoneNo"))).sendKeys(ContactNumber);
				sleep(250);
				report.updateTestLog("Contact NUmber", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);
				
				//***Select the Preferred time of Contact*****\\			
				
				if(TimeofContact.equalsIgnoreCase("Morning")){			
							
					driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[1]/label/span")).click();
					
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
				}			
				else{			
					driver.findElement(By.xpath("//div/label[text()=' What time of day do you prefer to be contacted? ']/../following-sibling::div/div/div[2]/label/span")).click();
					sleep(250);
					report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);		
				}			
				
							
				//****Contact Details-Continue Button*****\\		
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='submitForm(cancelContactDetailsForm);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue button", Status.PASS);
				sleep(1200);			
							
							
				//***************************************\\			
				//**********COVER CANCELLATION SECTION***********\\			
				//****************************************\\	
				
				if(CancelDeath.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='deathLable']/span"))).click();
					report.updateTestLog("Death Cover", "Death Cover is Selected for Cancellation", Status.PASS);
				}
				
				if(CancelTPD.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='tpdLable']/span"))).click();
					report.updateTestLog("TPD Cover", "TPD Cover is Selected for Cancellation", Status.PASS);
				}

				if(CancelIP.equalsIgnoreCase("Yes")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='salContinuanceLable']/span"))).click();
					report.updateTestLog("IP Cover", "IP Cover is Selected for Cancellation", Status.PASS);
				}
				
				//*****Click on Continue Button*******\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='submitForm(coverCancellationForm);']"))).click();
				report.updateTestLog("Cotinue", "Cotinue is selected after selecting the cancellation options", Status.PASS);
					
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}
		
		
	private void confirmation() {
		
		WebDriverWait wait=new WebDriverWait(driver,20);	
		String CancelReason = dataTable.getData("FirstSuper_CancelCover", "CancelReason");
		String CancelOtherReason = dataTable.getData("FirstSuper_CancelCover", "CancelOtherReason");
			try{
							
	//******Please provide reason for cancelling your cover by selecting one of the following options*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(text(),'"+CancelReason+"')]"))).click();
				report.updateTestLog("Cancel Reason", CancelReason+" is Selected", Status.PASS);
				sleep(500);
				
				if(CancelReason.equalsIgnoreCase("Other � please provide details")){
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-model='otherReasonObj.otherReason']"))).sendKeys(CancelOtherReason);
					sleep(250);
					report.updateTestLog("Cancel-Other Reason", CancelOtherReason+" is entered", Status.PASS);
					sleep(350);
				}
				
		   //*****Acknowledgement Label*******\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ackCancellationLabelId']/span"))).click();
				sleep(250);
				report.updateTestLog("Acknowledgement Label", "Checkbox is selected", Status.PASS);
				
				
				
			//******Click on Submit*******\\
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='cancelConfirmationPopUp()']"))).click();
				report.updateTestLog("Confirm", "Clicked on Confirm button", Status.PASS);
				sleep(1000);			
				
			//*****Confirmation Pop-Up*******\\
				
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='confirm()']"))).click();
				report.updateTestLog("Confirmation Pop-up", "Selected Yes on the popup window", Status.PASS);
				sleep(1000);
			
			//*******Feedback popup******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(),'No')]"))).click();
			report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
			sleep(1500);
			
			if(driver.getPageSource().contains("Application number")){
				
			
			//*****Fetching the Application Status*****\\
			
			String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
			sleep(500);
			
			//*****Fetching the Application Number******\\
			
			String App=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p")).getText();
			sleep(1000);
			
			
			
			//******Download the PDF***********\\
			properties = Settings.getInstance();	
			String StrBrowseName=properties.getProperty("currentBrowser");
			if (StrBrowseName.equalsIgnoreCase("Chrome")){
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
				sleep(1500);
	  		}
			
			
			//driver.findElement(By.xpath("//a[@class='underline']/strong")).click();
			
			
			
		  		if (StrBrowseName.equalsIgnoreCase("Firefox")){
		  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
		  			sleep(1500);
		  			Robot roboobj=new Robot();
		  			roboobj.keyPress(KeyEvent.VK_ENTER);	
		  		}
		  		
		  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
		  			Robot roboobj=new Robot();
		  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
		  				
		  			sleep(4000);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_TAB);
		  			roboobj.keyPress(KeyEvent.VK_ENTER);	
		  			
		  		}
			
			report.updateTestLog("PDF File", "PDF File downloaded",Status.PASS);
			
			sleep(1000);
		
			 
			  report.updateTestLog("Decision Page", "Application No: "+App,Status.PASS);
			
				
			 report.updateTestLog("Decision Page", "Application Status: "+Appstatus,Status.PASS);	
			}

			else{
				 report.updateTestLog("Application Declined", "Application is declined",Status.SCREENSHOT);
			}
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
	}

		
		
	
	
	}





