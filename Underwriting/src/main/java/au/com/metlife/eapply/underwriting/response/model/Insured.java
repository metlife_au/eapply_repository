package au.com.metlife.eapply.underwriting.response.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class Insured implements Serializable{
	private String age;
	private String bmi;
	private String cholesterol; 
	private String diastolic;
	private String gender;
	private String hdlCholesterol;
	private String height;
	private String interviewSubmitted;
	private String maritalStatus;
	private String name;
	private String percentComplete;
	private String smoker;
	private String systolic;
	private String uniqueId;
	private String waived;
	private String weight;
	
	private List<ItemData> listIntEngineVar;
	private List<ItemData> listExtEngineVar;
	private List<Product> listProduct;
	private List<Decision> listOverallDec;
	private List<Decision> listOriginalRuleDec;
	private List<Decision> listRuleDec;
	private List<Decision> listGenderAgeBMIDec;
	private List<Decision> listAgeCoverageDec;
	private List<Decision> listInsuredDec;
	private List<Impairment> listImpairmentDec;
	private List<Decision> listDepRiskDec;
	private List<Decision> listGenderAgeBMIRangeTable;
	private List<Decision> listGenderAgeBMIRangeBillTable;
	private List<Decision> listGenderAgeCalculatedBMIRangeMetlifeTable;
	
	
	public List<Decision> getListAgeCoverageDec() {
		
		if (null == listAgeCoverageDec) {
			listAgeCoverageDec = new ArrayList<Decision>();
		}
		return listAgeCoverageDec;
	}
	public String getBmi() {
		return bmi;
	}
	public void setBmi(String bmi) {
		this.bmi = bmi;
	}
	public String getCholesterol() {
		return cholesterol;
	}
	public void setCholesterol(String cholesterol) {
		this.cholesterol = cholesterol;
	}
	public String getDiastolic() {
		return diastolic;
	}
	public void setDiastolic(String diastolic) {
		this.diastolic = diastolic;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getHdlCholesterol() {
		return hdlCholesterol;
	}
	public void setHdlCholesterol(String hdlCholesterol) {
		this.hdlCholesterol = hdlCholesterol;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getInterviewSubmitted() {
		return interviewSubmitted;
	}
	public void setInterviewSubmitted(String interviewSubmitted) {
		this.interviewSubmitted = interviewSubmitted;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPercentComplete() {
		return percentComplete;
	}
	public void setPercentComplete(String percentComplete) {
		this.percentComplete = percentComplete;
	}
	public String getSmoker() {
		return smoker;
	}
	public void setSmoker(String smoker) {
		this.smoker = smoker;
	}
	public String getSystolic() {
		return systolic;
	}
	public void setSystolic(String systolic) {
		this.systolic = systolic;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getWaived() {
		return waived;
	}
	public void setWaived(String waived) {
		this.waived = waived;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public List<Decision> getListDepRiskDec() {
		
		if (null == listDepRiskDec) {
			listDepRiskDec = new ArrayList<Decision>();
		}
		return listDepRiskDec;
	}
	public List<ItemData> getListExtEngineVar() {
		
		if (null == listExtEngineVar) {
			listExtEngineVar = new ArrayList<ItemData>();
		}
		return listExtEngineVar;
	}
	public List<Decision> getListGenderAgeBMIDec() {
		
		if (null == listGenderAgeBMIDec) {
			listGenderAgeBMIDec = new ArrayList<Decision>();
		}
		return listGenderAgeBMIDec;
	}
	public List<Impairment> getListImpairmentDec() {
		
		if (null == listImpairmentDec) {
			listImpairmentDec = new ArrayList<Impairment>();
		}
		return listImpairmentDec;
	}
	public List<Decision> getListInsuredDec() {
		
		if (null == listInsuredDec) {
			listInsuredDec = new ArrayList<Decision>();
		}
		return listInsuredDec;
	}
	public List<ItemData> getListIntEngineVar() {
		
		if (null == listIntEngineVar) {
			listIntEngineVar = new ArrayList<ItemData>();
		}
		return listIntEngineVar;
	}
	public List<Decision> getListOriginalRuleDec() {
		
		if (null == listOriginalRuleDec) {
			listOriginalRuleDec = new ArrayList<Decision>();
		}
		return listOriginalRuleDec;
	}
	public List<Decision> getListOverallDec() {
		
		if (null == listOverallDec) {
			listOverallDec = new ArrayList<Decision>();
		}
		return listOverallDec;
	}
	public List<Product> getListProduct() {
		
		if (null == listProduct) {
			listProduct = new ArrayList<Product>();
		}
		return listProduct;
	}
	public List<Decision> getListRuleDec() {
		
		if (null == listRuleDec) {
			listRuleDec = new ArrayList<Decision>();
		}
		return listRuleDec;
	}
	public List<Decision> getListGenderAgeBMIRangeTable() {
		if (null == listGenderAgeBMIRangeTable) {
			listGenderAgeBMIRangeTable = new ArrayList<Decision>();
		}
		return listGenderAgeBMIRangeTable;
	}
	public void setListGenderAgeBMIRangeTable(
			List<Decision> listGenderAgeBMIRangeTable) {
		this.listGenderAgeBMIRangeTable = listGenderAgeBMIRangeTable;
	}
	public List<Decision> getListGenderAgeBMIRangeBillTable() {
		return listGenderAgeBMIRangeBillTable;
	}
	public void setListGenderAgeBMIRangeBillTable(
			List<Decision> listGenderAgeBMIRangeBillTable) {
		this.listGenderAgeBMIRangeBillTable = listGenderAgeBMIRangeBillTable;
	}
	public List<Decision> getListGenderAgeCalculatedBMIRangeMetlifeTable() {		
		if (null == listGenderAgeCalculatedBMIRangeMetlifeTable) {
			listGenderAgeCalculatedBMIRangeMetlifeTable = new ArrayList<Decision>();
		}
		return listGenderAgeCalculatedBMIRangeMetlifeTable;
	}
	public void setListGenderAgeCalculatedBMIRangeMetlifeTable(
			List<Decision> listGenderAgeCalculatedBMIRangeMetlifeTable) {
		this.listGenderAgeCalculatedBMIRangeMetlifeTable = listGenderAgeCalculatedBMIRangeMetlifeTable;
	}	
	
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
}