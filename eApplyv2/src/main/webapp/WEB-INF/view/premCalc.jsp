<!DOCTYPE html>
<html ng-app="premCalcApp">
<head>
<meta charset="ISO-8859-1">

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="shortcut icon" href="/eapply/static/vicsuper/images/favicon.ico">
<link href="/eapply/static/vicsuper/css/bootstrap.min.css" rel="stylesheet">
<link href="/eapply/static/vicsuper/css/bootstrap-select.css" rel="stylesheet">
<link href="/eapply/static/vicsuper/css/docs.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="screen" href="/eapply/static/vicsuper/css/datepicker.css" />


<!-- Custom styles for this template -->
<link href="/eapply/static/vicsuper/css/custom.css" rel="stylesheet">
<link href="/eapply/static/vicsuper/css/custom-ben.css" rel="stylesheet">
<link href="/eapply/static/vicsuper/css/ngDialog.css" rel="stylesheet">
<link rel="stylesheet" href="/eapply/static/vicsuper/css/ngDialog-theme-default.css">
<link rel="stylesheet" href="/eapply/static/vicsuper/css/ngDialog-theme-plain.css">

<title>Premium Calculator</title>
</head>

<%
String headerData = response.getHeader("InputData");
String domainName = response.getHeader("domainname");
%>
<script type="text/javascript">
var inputData = '<%=headerData%>';
var baseUrl = '<%= domainName%>';
</script>

<body>

<div ng-view autoscroll="true"></div>
<!-- <div id="body-mask" data-loading> -->
<!-- 	<div class="body-mask-loader"></div> -->
<!-- </div> -->

<!-- Placed at the end of the document so the pages load faster --> 
<script src="/eapply/resources/js/lib/angular.min.js"></script>
<script src="/eapply/resources/js/lib/angular-idle.min.js"></script>
<script src="/eapply/static/vicsuper/js/angular-route.js"></script>
<script src="/eapply/static/vicsuper/js/angular-resource.min.js"></script>
<script src="/eapply/static/vicsuper/js/jquery.min.js"></script> 
<!--  <script src="../../eapply/static/vicsuper/js/jquery-ui.js"></script> -->
<script src="/eapply/static/vicsuper/js/bootstrap.min.js"></script> 
<script src="/eapply/static/vicsuper/js/docs.min.js"></script>
<script src="/eapply/static/vicsuper/js/ng-file-upload-shim.js"></script>
<script src="/eapply/static/vicsuper/js/ng-file-upload.js"></script>
<script src="/eapply/static/vicsuper/js/ngDialog.js"></script>
<script src="/eapply/static/vicsuper/js/ui-bootstrap-tpls-2.5.0.min.js"></script>

<script src="/eapply/static/common/services/sessionTimeoutServiceVICT.js"></script>
<script src="/eapply/static/common/services/exceptionHandler.js"></script>
<script src="/eapply/static/common/services/storageHandler.js"></script>
<script src="/eapply/static/common/services/restAPI.js"></script>
<script src="/eapply/static/common/services/appDataModel.js"></script>
<script src="/eapply/static/common/services/eApplyInterceptor.js"></script>
<script type="text/javascript" src="/eapply/static/vicsuper/js/premCalcModule.js"></script>
 <script type="text/javascript" src="/eapply/static/vicsuper/js/controllers/premCalc.ctrl.js"></script>
 <script type="text/javascript" src="/eapply/static/vicsuper/js/moment.js"></script> 
<script type="text/javascript" src="/eapply/static/vicsuper/js/datetimepickercustom.js"></script> 
<script type="text/javascript" src="/eapply/static/vicsuper/js/bootstrap-select.js"></script> 
<script type="text/javascript" src="/eapply/static/vicsuper/js/services/servicePrem.js"></script>
<script type="text/javascript" src="/eapply/static/vicsuper/js/utils/filtersPremCalc.js"></script>
<script type="text/javascript" src="/eapply/static/vicsuper/js/utils/directivesPremCalc.js"></script>

<!-- CareSuperApp utilities -->


<script src="/eapply/static/vicsuper/js/custom.js"></script> 
<script src="/eapply/static/vicsuper/js/validation.js"></script>
<script>
//awful hack for angular-vertilize...
angular.element.prototype.outerWidth = function() {
    return jQuery(this[0]).outerWidth();
};
angular.element.prototype.height = function() {
    return jQuery(this[0]).height();
};
</script>
<script type="text/javascript" src="/eapply/static/vicsuper/js/utils/verticalHeight.js"></script>
</body>
</html>