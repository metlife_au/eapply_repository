package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the AURADETAILS database table.
 * 
 */
@Entity
@Table(name = "TBL_A_AURADETAILS", schema="EAPPDB")
public class Auradetail implements Serializable {
	private static final long serialVersionUID = 1L;

//	@Column(length=20)
//	private String auraform;
//
//	@Column(length=50)
//	private String auraversion;

	private Timestamp createdate;

//	@Column(nullable=false, length=47)
//	private String eapplicationid;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="TBL_A_AURADETAILS_SEQ")
	@SequenceGenerator(name="TBL_A_AURADETAILS_SEQ",sequenceName = "EAPPDB.TBL_A_AURADETAILS_SEQ", initialValue=1, allocationSize=100)
	private long id;

	@Column(length=50)
	private String lastsavedsection;

	private Timestamp lastupdatedate;
//
//	@Column(length=50)
//	private String percentcomplete;

	@Column(length=5)
	private String shortformstatus;

	public Auradetail() {
	}


	public Timestamp getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

//	public String getEapplicationid() {
//		return this.eapplicationid;
//	}
//
//	public void setEapplicationid(String eapplicationid) {
//		this.eapplicationid = eapplicationid;
//	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLastsavedsection() {
		return this.lastsavedsection;
	}

	public void setLastsavedsection(String lastsavedsection) {
		this.lastsavedsection = lastsavedsection;
	}

	public Timestamp getLastupdatedate() {
		return this.lastupdatedate;
	}

	public void setLastupdatedate(Timestamp lastupdatedate) {
		this.lastupdatedate = lastupdatedate;
	}


	public String getShortformstatus() {
		return this.shortformstatus;
	}

	public void setShortformstatus(String shortformstatus) {
		this.shortformstatus = shortformstatus;
	}

}