package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class WebserviceInputStringPageObjects extends PageObjectLibrary {
	public static final By txtInputString = getObjectIdentifaction("ing.servicePaylaod.txt.id");
	public static final By btnPlainText = getObjectIdentifaction("ing.nextPage.btn.css");
}
