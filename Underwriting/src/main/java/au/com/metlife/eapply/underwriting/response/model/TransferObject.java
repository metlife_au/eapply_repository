package au.com.metlife.eapply.underwriting.response.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;


public class TransferObject implements Serializable {
	
	private static final long serialVersionUID = 1L;	
	
	private int questionId;	
	private String partialQuestionId;
	private boolean completed;
	private String questionnaireId;
	private String clientName;
	private String questionType;
	private String questionText;
	private String questionNumber;	
	private String submittedAnswer;	
	private String validationMsg;
	private String questionAlias;
	private Boolean readOnly;
	private Boolean visible;
	private Boolean branchReadOnly;
	private Boolean branchVisible;
	private Boolean baseQuestion;
	

	private List <String> listDisplayValues;
	private List <String> listValues ;
	private List <String> listSubUnitValues ;
	private Map <String, String> mapDisplayValues ;	
	private Map <String, String> mapUnits ;	
	private List<String> toolTipsKeys;

	private List<Entity> vectorEntityQuestions ;
	private List<TransferObject> vectorSubQuestions ;
	private List<Answer> vectorAnswers;
	private String tempPartialQuestionId;
	
	public String getTempPartialQuestionId() {
		return tempPartialQuestionId;
	}
	public void setTempPartialQuestionId(String tempPartialQuestionId) {
		this.tempPartialQuestionId = tempPartialQuestionId;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public boolean isCompleted() {
		return completed;
	}
	public void setCompleted(boolean completed) {
		this.completed = completed;
	}
	public int getQuestionId() {
		return questionId;
	}
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}
	public String getQuestionnaireId() {
		return questionnaireId;
	}
	public void setQuestionnaireId(String questionnaireId) {
		this.questionnaireId = questionnaireId;
	}
	public String getQuestionNumber() {
		return questionNumber;
	}
	public void setQuestionNumber(String questionNumber) {
		this.questionNumber = questionNumber;
	}
	public String getQuestionText() {
		return questionText;
	}
	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}
	public String getQuestionType() {
		return questionType;
	}
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	

	public List<String> getListDisplayValues() {
		if(null == listDisplayValues){
			listDisplayValues = new ArrayList<String>();
		}
		return listDisplayValues;
	}

	public List<String> getListValues() {
		if(null == listValues){
			listValues = new ArrayList<String>();
		}
		return listValues;
	}
	public Map getMapUnits() {
		if(null == mapUnits){
			mapUnits = new Hashtable<String, String>();
		}
		return mapUnits;
	}
	public List<Entity> getVectorEntityQuestions() {
		if(null == vectorEntityQuestions){
			vectorEntityQuestions = new ArrayList<Entity>();
		}
		return vectorEntityQuestions;
	}
	public List<TransferObject> getVectorSubQuestions() {
		if(null == vectorSubQuestions)
			vectorSubQuestions = new ArrayList<TransferObject>();
		return vectorSubQuestions;
	}
	public List<String> getListSubUnitValues() {
		if(null == listSubUnitValues){
			listSubUnitValues = new ArrayList<String>();
		}
		return listSubUnitValues;
	}
	
	public String getSubmittedAnswer() {
		return submittedAnswer;
	}
	public void setSubmittedAnswer(String submittedAnswer) {
		this.submittedAnswer = submittedAnswer;
	}
	public String getValidationMsg() {
		return validationMsg;
	}	
	public Map<String, String> getMapDisplayValues() {
		if(null == mapDisplayValues){
			mapDisplayValues = new Hashtable<String, String>();
		}
		return mapDisplayValues;
	}
	public void setValidationMsg(String validationMsg) {
		this.validationMsg = validationMsg;
	}
	public List<Answer> getVectorAnswers() {
		if(null == vectorAnswers){
			vectorAnswers = new ArrayList<Answer>();
		}
		return vectorAnswers;
	}
	public String getQuestionAlias() {
		return questionAlias;
	}
	public void setQuestionAlias(String questionAlias) {
		this.questionAlias = questionAlias;
	}
	public List<String> getToolTipsKey() {
		if(null == toolTipsKeys){
			toolTipsKeys = new ArrayList<String>();
		}
		return toolTipsKeys;
	}
	public void setToolTipsKey(List<String> toolTipsKey) {
		this.toolTipsKeys = toolTipsKey;
	}
	
	public Boolean getReadOnly() {
		return readOnly;
	}
	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}
	public Boolean getBranchReadOnly() {
		return branchReadOnly;
	}
	public void setBranchReadOnly(Boolean branchReadOnly) {
		this.branchReadOnly = branchReadOnly;
	}
	public Boolean getBranchVisible() {
		return branchVisible;
	}
	public void setBranchVisible(Boolean branchVisible) {
		this.branchVisible = branchVisible;
	}
	public Boolean getVisible() {
		return visible;
	}
	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
	public Boolean getBaseQuestion() {
		return baseQuestion;
	}
	public void setBaseQuestion(Boolean baseQuestion) {
		this.baseQuestion = baseQuestion;
	}
	public String getPartialQuestionId() {
		return partialQuestionId;
	}
	public void setPartialQuestionId(String partialQuestionId) {
		this.partialQuestionId = partialQuestionId;
	}
		
}
