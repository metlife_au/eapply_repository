package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import com.gargoylesoftware.htmlunit.html.Keyboard;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.STFTBeneficiariesPageObjects;
import uimap.metlife.STFTGetQuotePageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.VOWQuotePageObjects;
import uimap.metlife.YBRCoverDetailPageObjects;
import uimap.metlife.YBRQuotePageObjects;
import uimap.metlife.YBRWebResponsePageObjects;

public class VOWQuotePage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public VOWQuotePage quoteProtect() {
		quoteProtectNow();
		return new VOWQuotePage(scriptHelper);
	}

	public VOWQuotePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void quoteProtectNow() {
	
	
		try {
			
		//	String parentHandle = driver.getWindowHandle();
			driver.manage().window().maximize();
			
			for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}
			
					
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(VOWQuotePageObjects.btnCalcQuote)).click().perform();
			Thread.sleep(6000);
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,150)", "");
			report.updateTestLog("Capturing the Auto poplation screenshot", "Values are Autopopuated", Status.SCREENSHOT);
			JavascriptExecutor jse1 = (JavascriptExecutor)driver;
			jse1.executeScript("window.scrollBy(0,450)", "");
			report.updateTestLog("Capturing the Auto poplation screenshot", "Values are Autopopuated", Status.SCREENSHOT);
			tryAction(waitForClickableElement(VOWQuotePageObjects.btnProceed), "Click", "Apply");
			
			driver.manage().window().maximize();
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
