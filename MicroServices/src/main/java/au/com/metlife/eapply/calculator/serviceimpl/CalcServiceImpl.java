package au.com.metlife.eapply.calculator.serviceimpl;

import org.drools.KnowledgeBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import au.com.metlife.eapply.bs.helper.EapplyServiceHelper;
import au.com.metlife.eapply.bs.model.InsuranceModel;
import au.com.metlife.eapply.bs.model.InsuranceRuleInfo;
import au.com.metlife.eapply.calculator.model.CalcExpensesVO;
import au.com.metlife.eapply.calculator.model.InsurCalculatorRuleBO;
import au.com.metlife.eapply.calculator.model.InsurCalculatorVO;
import au.com.metlife.eapply.calculator.model.KidsVO;
import au.com.metlife.eapply.calculator.model.SuperAnnuationVO;
import au.com.metlife.eapply.calculator.service.CalcService;

import au.com.metlife.eapply.quote.utility.CalculateHelper;

import au.com.metlife.eapply.quote.utility.EappRulesCacheProvider;
import au.com.metlife.eapply.quote.utility.QuoteConstants;


@Service
public class CalcServiceImpl implements CalcService {
	private static final Logger log = LoggerFactory.getLogger(CalcServiceImpl.class);
	
    
	@Autowired(required = true)
	EappRulesCacheProvider eappRulesCacheProvider;
	
	@Override
	public InsurCalculatorVO calculateInsuranceCost(InsuranceModel insuranceModel) {
		log.info("Calculate calculateInsuranceCost start");
		BigDecimal yearlySal=null;
		BigDecimal yearlyGrossSal=null;
		BigDecimal yearlySpouseSal=null;
		BigDecimal monthlyMortageVal =null;
		InsurCalculatorVO insurCalculatorVO=new InsurCalculatorVO();
		/*insurCalculatorVO.setAge(30);
		insurCalculatorVO.setRetirementAge(50);
		insurCalculatorVO.setSuperContribRate(new BigDecimal(9.25));
		insurCalculatorVO.setGrossSalVal(new BigDecimal(100000));
		insurCalculatorVO.setInflationRate(new BigDecimal(2.5));
		insurCalculatorVO.setRealInterestRate(new BigDecimal(4.0));
		insurCalculatorVO.setPartnerGrossSalVal(new BigDecimal(100000));
		insurCalculatorVO.setLivingWithSpouse(true);
		insurCalculatorVO.setExpensesVal(new BigDecimal(10000));
		insurCalculatorVO.setKidsCount(2);
		insurCalculatorVO.setAdultFactor(BigDecimal.ONE);
		insurCalculatorVO.setChildFactor(BigDecimal.ONE);
		insurCalculatorVO.setMiscFactor(BigDecimal.ONE);
		insurCalculatorVO.setIpBenefitPaidToSA(new BigDecimal(10.00));
		insurCalculatorVO.setRequiredIpCover(new BigDecimal(7500));
		insurCalculatorVO.setDependentSuppAge(18);
		insurCalculatorVO.setIpBenefitPerVal(2);
		insurCalculatorVO.setIncludeNursingCosts(true);
		insurCalculatorVO.setAnnualMedAndNursingCost(new BigDecimal(26400));*/
		/*insurCalculatorVO.setAge(ruleModel.getAge());
		insurCalculatorVO.setRetirementAge(ruleModel.getRetirementAge());
		insurCalculatorVO.setSuperContribRate(ruleModel.getSuperContribRate());
		insurCalculatorVO.setGrossSalVal(ruleModel.getGrossSalVal());
		insurCalculatorVO.setInflationRate(ruleModel.getInflationRate());
		insurCalculatorVO.setRealInterestRate(ruleModel.getRealInterestRate());
		insurCalculatorVO.setPartnerGrossSalVal(ruleModel.getPartnerGrossSalVal());
		insurCalculatorVO.setLivingWithSpouse(ruleModel.isLivingWithSpouse());
		insurCalculatorVO.setExpensesVal(ruleModel.getExpensesVal());
		insurCalculatorVO.setKidsCount(ruleModel.getKidsCount());
		insurCalculatorVO.setAdultFactor(ruleModel.getAdultFactor());
		insurCalculatorVO.setChildFactor(ruleModel.getChildFactor());
		insurCalculatorVO.setMiscFactor(ruleModel.getMiscFactor());
		insurCalculatorVO.setIpBenefitPaidToSA(ruleModel.getIpBenefitPaidToSA());
		insurCalculatorVO.setRequiredIpCover(ruleModel.getRequiredIpCover());
		insurCalculatorVO.setDependentSuppAge(ruleModel.getDependentSuppAge());
		insurCalculatorVO.setIpBenefitPerVal(ruleModel.getIpBenefitPerVal());
		insurCalculatorVO.setIncludeNursingCosts(ruleModel.isIncludeNursingCosts());
		insurCalculatorVO.setAnnualMedAndNursingCost(ruleModel.getAnnualMedAndNursingCost());*/
		
//		KidsVO k1=new KidsVO();
//		k1.setAge(2);
//		
//		KidsVO k2=new KidsVO();
//		k2.setAge(0);
//		
//		/*KidsVO k1=new KidsVO();
//		k1.setAge(ruleModel.getKiddetails().get(0).getAge());
//		
//		KidsVO k2=new KidsVO();
//		k2.setAge(ruleModel.getKiddetails().get(1).getAge());*/
//		List kidsList=new ArrayList<KidsVO>();
//		kidsList.add(k1);
//		kidsList.add(k2);
//		insurCalculatorVO.setKidsList(kidsList);
		CalculateHelper calculateHelper = new CalculateHelper();
		InsurCalculatorRuleBO insurCalculatorRuleBO = new InsurCalculatorRuleBO();
		if(insuranceModel.getFundId()!=null){
			String ruleNameAsPerFund;
			
			ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_CALCULATOR_CONFIG;
			insurCalculatorRuleBO.setRuleFileName(ruleNameAsPerFund);
			insurCalculatorRuleBO.setProductCode(insuranceModel.getFundId());
			
			insurCalculatorRuleBO=CalculateHelper.invokeInsuranceCalcRule(insurCalculatorRuleBO,eappRulesCacheProvider.getRuleMap(), eappRulesCacheProvider.getFilePath(),ruleNameAsPerFund);	
		}
		if(insuranceModel.getApplicant().getAge()!= null){
			insurCalculatorVO.setAge(Integer.parseInt(insuranceModel.getApplicant().getAge()));
		}
		if(insuranceModel.getApplicant().getGender()!= null){
			if(insuranceModel.getApplicant().getGender().equalsIgnoreCase("Male")){
				insurCalculatorVO.setGenderMale(true);
		    }else if (insuranceModel.getApplicant().getGender().equalsIgnoreCase("Female")){
			    insurCalculatorVO.setGenderFemale(true);
		    }
		}
		insurCalculatorVO.setGender(insuranceModel.getApplicant().getGender());		
		insurCalculatorVO.setDependentSuppAge(insuranceModel.getInsuranceRuleDetails().getDependentSuppAge());
		insurCalculatorVO.setInflationRate(insuranceModel.getInsuranceRuleDetails().getInflationRate());
		insurCalculatorVO.setIpBenefitPaidToSA(insuranceModel.getInsuranceRuleDetails().getIpBenefitPaidToSA());
		insurCalculatorVO.setIpReplaceRatio(insuranceModel.getInsuranceRuleDetails().getIpReplaceRatio());
		insurCalculatorVO.setRealInterestRate(insuranceModel.getInsuranceRuleDetails().getRealInterestRate());
		insurCalculatorVO.setRetirementAge(insuranceModel.getInsuranceRuleDetails().getRetirementAge());
		insurCalculatorVO.setSuperContribRate(insuranceModel.getInsuranceRuleDetails().getSuperContribRate());
		insurCalculatorVO.setIncomeSalPeriodVal(insurCalculatorRuleBO.getMyGrossSalDefFreq());
		insurCalculatorVO.setIncomePartnerSalPeriodVal(insurCalculatorRuleBO.getPartnerGrossSalDefFreq());
		insurCalculatorVO.setMortageRepaymentPeriodVal(insurCalculatorRuleBO.getMortgageRepDefFreq());
		insurCalculatorVO.setExpenses(insurCalculatorRuleBO.getOtherExpDefFreq());
		insurCalculatorVO.setIpBenefitPeriod(insurCalculatorRuleBO.getDefaultBenefitPeriod());
		insurCalculatorVO.setIpWaitingPeriod(insurCalculatorRuleBO.getDefaultWaitingPeriod());
		insurCalculatorVO.setFuneralCost(insuranceModel.getInsuranceRuleDetails().getFuneralCost());
		insurCalculatorVO.setAnnualMedAndNursingCost(insuranceModel.getInsuranceRuleDetails().getAnnualMedAndNursingCost());
		insurCalculatorVO.setAdultFactor(insurCalculatorRuleBO.getAdultFactor());
		insurCalculatorVO.setChildFactor(insurCalculatorRuleBO.getChildFactor());
		insurCalculatorVO.setMiscFactor(insurCalculatorRuleBO.getMiscFactor());
		if(insuranceModel.getApplicant().getMortage().equalsIgnoreCase("yes")){
		//insurCalculatorVO.setMortageRepaymentPeriodVal(insuranceModel.getApplicant().getMortgageFrequency());		
		monthlyMortageVal=calculateHelper.convertToMortageMonthlyAmount(insuranceModel.getApplicant().getMortgageFrequency(), new BigDecimal(insuranceModel.getApplicant().getMortgageAmount()));
		insurCalculatorVO.setMortageRepaymentVal(monthlyMortageVal);
		}else{
			if(insurCalculatorVO.getMortageRepaymentVal()==null){
				insurCalculatorVO.setMortageRepaymentVal(BigDecimal.ZERO);
			}
		}
		
		insurCalculatorVO.setIncludeNursingCosts(insuranceModel.getApplicant().isNursingCosts());
		insurCalculatorVO.setExcludeIP(insuranceModel.getApplicant().isTpdWithoutIP());
		//
		if(insuranceModel.getApplicant().getLivingWithWife().equalsIgnoreCase("yes")){
			insurCalculatorVO.setLivingWithSpouse(true);
			insurCalculatorVO.setLivingWithSpouseVal("yes");
		}else{
			insurCalculatorVO.setLivingWithSpouse(false);
			insurCalculatorVO.setLivingWithSpouseVal("No");
		}
		if(insurCalculatorVO.getMortageRepaymentVal()==null){
			insurCalculatorVO.setMortageRepaymentVal(BigDecimal.ZERO);
		}
		if(insurCalculatorVO.getDebtVal()==null){
			insurCalculatorVO.setDebtVal(BigDecimal.ZERO);
		}
		
		if(insuranceModel.getApplicant().getSalaryFrequency()!=null && EapplyServiceHelper.isNotNullandNotEmpty(insuranceModel.getApplicant().getSalary())){
			 yearlySal=calculateHelper.convertToYearlyAmount(insuranceModel.getApplicant().getSalaryFrequency(), new BigDecimal(insuranceModel.getApplicant().getSalary()));
			 insurCalculatorVO.setIncomeSalPeriodVal(insuranceModel.getApplicant().getSalaryFrequency());
			 insurCalculatorVO.setGrossSalVal(new BigDecimal(insuranceModel.getApplicant().getSalary()));
		}
		if(insuranceModel.getApplicant().getSpouseSalaryFrequency()!=null && EapplyServiceHelper.isNotNullandNotEmpty(insuranceModel.getApplicant().getSpouseSalary())){
			 yearlySpouseSal=calculateHelper.convertToYearlyAmount(insuranceModel.getApplicant().getSpouseSalaryFrequency(), new BigDecimal(insuranceModel.getApplicant().getSpouseSalary()));
			 insurCalculatorVO.setIncomePartnerSalPeriodVal(insuranceModel.getApplicant().getSpouseSalaryFrequency());
			 insurCalculatorVO.setPartnerGrossSalVal(new BigDecimal(insuranceModel.getApplicant().getSpouseSalary()));
		}
		 //yearlyGrossSal = yearlySal.add(yearlySpouseSal);
		 insurCalculatorVO.setGrossSalVal(yearlySal);
		 insurCalculatorVO.setExpensesVal(BigDecimal.valueOf(insuranceModel.getApplicant().getTotalExpense()));
		 insurCalculatorVO.setExpenses(insuranceModel.getApplicant().getTotalExpenseFrequency());
		 insurCalculatorVO.setDebtVal(BigDecimal.valueOf(insuranceModel.getApplicant().getTotalDebts()));
		 insurCalculatorVO.setKidsList(insuranceModel.getApplicant().getChildList());
		 if(insuranceModel.getApplicant().getDependentChild().equalsIgnoreCase("yes")){
				insurCalculatorVO.setDepenChildVal("yes");		
			}else{				
				insurCalculatorVO.setDepenChildVal("No");
			}
		calculateHelper.calculateRecommendedIpCover(insurCalculatorRuleBO, insurCalculatorVO,insuranceModel);
		if(insurCalculatorVO.getRecommendedIpCover()!=null && insurCalculatorVO.getRecommendedIpCover().compareTo(BigDecimal.ZERO)==0){
			insurCalculatorVO.setIpNotEligbileDueToSal(true);
			//setDisableIpSection(true);
		}else{
			insurCalculatorVO.setIpNotEligbileDueToSal(false);
			//setDisableIpSection(false);
		}
		if(null!=insuranceModel.getFundId() && QuoteConstants.PARTNER_CARE.equalsIgnoreCase(insuranceModel.getFundId())){
			if(insurCalculatorVO.isIpNotEligbileDueToSal() || insurCalculatorVO.isIpNotEligibleDueToAge() || (insurCalculatorVO.getFifteenHrsQAns()!=null && insurCalculatorVO.getFifteenHrsQAns().equalsIgnoreCase("No") && 
					insurCalculatorVO.getGrossSalVal()!=null && insurCalculatorVO.getGrossSalVal().compareTo(new BigDecimal("16000"))==-1)){
				insurCalculatorVO.setDisableIpSection(true);
				insurCalculatorVO.setRequiredIpCover(BigDecimal.ZERO);
				insurCalculatorVO.setRecommendedIpCover(BigDecimal.ZERO);
				insurCalculatorVO.setRecommendedIpCoverOld(null);
			}else{
				insurCalculatorVO.setDisableIpSection(false);
			}
		}else{
		if(insurCalculatorVO.isIpNotEligbileDueToSal() || insurCalculatorVO.isIpNotEligibleDueToAge() || (insurCalculatorVO.getFifteenHrsQAns()!=null && insurCalculatorVO.getFifteenHrsQAns().equalsIgnoreCase("No"))){
			insurCalculatorVO.setDisableIpSection(true);
			insurCalculatorVO.setRequiredIpCover(BigDecimal.ZERO);
			insurCalculatorVO.setRecommendedIpCover(BigDecimal.ZERO);
			insurCalculatorVO.setRecommendedIpCoverOld(null);
		}else{
			insurCalculatorVO.setDisableIpSection(false);
		}
		}
		calculateEligibleSuperAmt(insurCalculatorVO);
		calculateNursigCost(insurCalculatorVO);
		calculateTpdReplaceExpenses(insurCalculatorVO,eappRulesCacheProvider.getRuleMap());
		calculateDeathExpenses(insurCalculatorVO);
		calculateHelper.calculateRecommendedTPDCover(insurCalculatorRuleBO, insurCalculatorVO);
		calculateHelper.calculateRecommendedDeathCover(insurCalculatorRuleBO, insurCalculatorVO);
		
		
		return insurCalculatorVO;
	}
	/**
	 * @param insurCalculatorVO
	 */
	public void calculateNursigCost(InsurCalculatorVO insurCalculatorVO){
		int diffBwAgeAndRetireAge=0;
		try{
			if(insurCalculatorVO!=null && insurCalculatorVO.isIncludeNursingCosts()){
				diffBwAgeAndRetireAge=insurCalculatorVO.getRetirementAge()-insurCalculatorVO.getAge();
				if(diffBwAgeAndRetireAge!=0){
					BigDecimal cumulativeTNPV=BigDecimal.ZERO;
					for(int year=0;year<diffBwAgeAndRetireAge;year++){
						CalcExpensesVO calcExpensesVO=new CalcExpensesVO();
						
						calculateNursingExpenses(calcExpensesVO,insurCalculatorVO,year);
//						System.out.println(">>>>calcExpensesVO.getExpenses()>>"+calcExpensesVO.getExpenses());

						calculateNursingNPVAmt(calcExpensesVO,insurCalculatorVO,year);
//						System.out.println(">>>>calcExpensesVO.getNetNPVAmt()>>"+calcExpensesVO.getNetNPVAmt());
						cumulativeTNPV=cumulativeTNPV.add(calcExpensesVO.getNetNPVAmt());
					}
					insurCalculatorVO.setTtlNursingNpvAmt(cumulativeTNPV);
//					System.out.println(">>>>Final Nursing amt>>"+cumulativeTNPV);
				}
			}else if(insurCalculatorVO!=null){
				insurCalculatorVO.setTtlNursingNpvAmt(BigDecimal.ZERO);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	/**
	 * @param calcExpensesVO
	 * @param insurCalculatorVO
	 * @param year
	 * @return
	 */
	public InsurCalculatorVO calculateNursingExpenses(CalcExpensesVO calcExpensesVO,InsurCalculatorVO insurCalculatorVO,int year){
		BigDecimal A=null;/* 1 + inflation rate*/
		BigDecimal B=null; /* (1 + inflation rate) to the power of year*/
		try{
			if(insurCalculatorVO!=null && insurCalculatorVO.getAnnualMedAndNursingCost() !=null && insurCalculatorVO.getInflationRate()!=null){
				A=BigDecimal.ONE.add(convertToPercentage(insurCalculatorVO.getInflationRate()));
				B=new BigDecimal(Math.pow(A.doubleValue(), (year+1)));
				calcExpensesVO.setExpenses((insurCalculatorVO.getAnnualMedAndNursingCost().multiply(B)).setScale(0, BigDecimal.ROUND_HALF_UP)); 
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	/**
	 * @param calcExpensesVO
	 * @param insurCalculatorVO
	 * @param year
	 * @return
	 */
	public InsurCalculatorVO calculateNursingNPVAmt(CalcExpensesVO calcExpensesVO,InsurCalculatorVO insurCalculatorVO,int year){
//		BigDecimal A=null;/* 1 + insurCalculatorVO.getInflationRate() +  insurCalculatorVO.getRealInterestRate()*/
		/*SRKR:21/01/2014 Formulae changed 1 + insurCalculatorVO.getRealInterestRate()*/
		BigDecimal A=null;/* 1 + insurCalculatorVO.getInflationRate() +  insurCalculatorVO.getRealInterestRate()*/
		BigDecimal B=null;/* A to the power of year+2 */
		BigDecimal netNpvAmt=BigDecimal.ZERO;
		try{
//			A=BigDecimal.ONE.add(convertToPercentage(insurCalculatorVO.getInflationRate()).add(convertToPercentage(insurCalculatorVO.getRealInterestRate())));
			A=BigDecimal.ONE.add(convertToPercentage(insurCalculatorVO.getRealInterestRate()));
//			B=new BigDecimal(Math.pow(A.doubleValue(), (year+2))).setScale(10, BigDecimal.ROUND_HALF_UP);
			B=new BigDecimal(Math.pow(A.doubleValue(), (year))).setScale(10, BigDecimal.ROUND_HALF_UP);	
			if(calcExpensesVO.getExpenses()!=null){
				netNpvAmt=calcExpensesVO.getExpenses().divide(B,0,BigDecimal.ROUND_HALF_UP);
				calcExpensesVO.setNetNPVAmt(netNpvAmt);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	/**
	 * @param insurCalculatorVO
	 */
	public void calculateDeathExpenses(InsurCalculatorVO insurCalculatorVO){
		int diffBwAgeAndRetireAge=0;
		try{
			if(insurCalculatorVO!=null){
				calculateChildExpenseRatio(insurCalculatorVO);/*This method calculates the child Expense Ratio percentage*/
//				System.out.println(">>>insurCalculatorVO.getChildExpenseRatio>>>"+insurCalculatorVO.getChildExpenseRatio());
				
				calculateAdultExpenseRatio(insurCalculatorVO);/*This method calculates the adult Expense Ratio percentage*/
//				System.out.println(">>>insurCalculatorVO.getAdultExpenseRatio>>>"+insurCalculatorVO.getAdultExpenseRatio());
				
				calculateRatioOfCover(insurCalculatorVO);/*This method calculates the ratio of cover*/
//				System.out.println(">>>insurCalculatorVO.getRatioOfCover()>>>"+insurCalculatorVO.getRatioOfCover());

				diffBwAgeAndRetireAge=insurCalculatorVO.getRetirementAge()-insurCalculatorVO.getAge();
				if(diffBwAgeAndRetireAge!=0){
					BigDecimal cumulativeTNPV=BigDecimal.ZERO;
					for(int year=0;year<diffBwAgeAndRetireAge;year++){
						CalcExpensesVO calcExpensesVO=new CalcExpensesVO();
						
						calculateDeathExpenses(calcExpensesVO,insurCalculatorVO,year);
//						System.out.println(">>>>calcExpensesVO.getExpenses()>>"+calcExpensesVO.getExpenses());
						
						calculateLoopChildPercentage(calcExpensesVO,insurCalculatorVO,year);
//						System.out.println(">>>>calcExpensesVO.getChildPercentage()>>"+calcExpensesVO.getChildPercentage());
						
						calculateDeathReducesExpenses(calcExpensesVO,insurCalculatorVO,year);
//						System.out.println(">>>>calcExpensesVO.getReducedExpenses()>>"+calcExpensesVO.getReducedExpenses());
						
						calculateDeathNetNPVAmt(calcExpensesVO,insurCalculatorVO,year);
//						System.out.println(">>>>calcExpensesVO.getNetNPVAmt()>>"+calcExpensesVO.getNetNPVAmt());
						cumulativeTNPV=cumulativeTNPV.add(calcExpensesVO.getNetNPVAmt());
					}
					insurCalculatorVO.setTtlDeathNpvAmt(cumulativeTNPV);
					System.out.println(">>>>Final Death Amt>>"+cumulativeTNPV);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	/**
	 * @param calcExpensesVO
	 * @param insurCalculatorVO
	 * @param year
	 * @return
	 */
	public InsurCalculatorVO calculateDeathReducesExpenses(CalcExpensesVO calcExpensesVO,InsurCalculatorVO insurCalculatorVO,int year){
		BigDecimal A=null;/* 1 - calcExpensesVO.getChildPercentage - insurCalculatorVO.getAdultExpenseRatio()*/
		try{
			if(insurCalculatorVO!=null && insurCalculatorVO.getAdultExpenseRatio()!=null && calcExpensesVO.getChildPercentage()!=null){
				A=BigDecimal.ONE.subtract(calcExpensesVO.getChildPercentage().add(insurCalculatorVO.getAdultExpenseRatio()));
				calcExpensesVO.setReducedExpenses((calcExpensesVO.getExpenses().multiply(A)).setScale(0, BigDecimal.ROUND_HALF_UP)); 
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	/**
	 * This methods calculate death expenses for all the years between age and retirementage
	 * Formulae
	 * otherexpenesfortpd X (1 + inflation rate) to the power of Year
	 * @param calcExpensesVO
	 * @param insurCalculatorVO
	 * @param year
	 * @return
	 */
	public InsurCalculatorVO calculateDeathExpenses(CalcExpensesVO calcExpensesVO,InsurCalculatorVO insurCalculatorVO,int year){
		BigDecimal A=null;/* 1 + inflation rate*/
		BigDecimal B=null; /* (1 + inflation rate) to the power of year*/
		BigDecimal C=null;/* insurCalculatorVO.getExpensesVal() X insurCalculatorVO.getRatioOfCover() */
		BigDecimal yearlyExpenses;
		CalculateHelper calculateHelper = new CalculateHelper();
		try{
			if(insurCalculatorVO!=null && insurCalculatorVO.getExpensesVal()!=null && insurCalculatorVO.getInflationRate()!=null){
				A=BigDecimal.ONE.add(convertToPercentage(insurCalculatorVO.getInflationRate()));
				B=new BigDecimal(Math.pow(A.doubleValue(), year));
				/*Assumption is always the expenses are anually.. so converting if the user enters other than annual freq.type*/
				yearlyExpenses=calculateHelper.convertToYearlyAmount(insurCalculatorVO.getExpenses(), insurCalculatorVO.getExpensesVal());
				C=yearlyExpenses.multiply(insurCalculatorVO.getRatioOfCover());
				calcExpensesVO.setExpenses((C.multiply(B)).setScale(0, BigDecimal.ROUND_HALF_UP)); 
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	public InsurCalculatorVO calculateDeathNetNPVAmt(CalcExpensesVO calcExpensesVO,InsurCalculatorVO insurCalculatorVO,int year){
		BigDecimal A=null;/* 1 + insurCalculatorVO.getInflationRate() +  insurCalculatorVO.getRealInterestRate()*//*SRKR: 21/01/2014 Forumlae changed to 1 +  insurCalculatorVO.getRealInterestRate() */
		BigDecimal B=null;/* A to the power of year+1 */ /*SRKR: 21/01/2014 Forumlae changed to A to the power of year  */
		BigDecimal netNpvAmt=BigDecimal.ZERO;
		try{
//			A=BigDecimal.ONE.add(convertToPercentage(insurCalculatorVO.getInflationRate()).add(convertToPercentage(insurCalculatorVO.getRealInterestRate())));
			A=BigDecimal.ONE.add(convertToPercentage(insurCalculatorVO.getRealInterestRate()));
			
//			B=new BigDecimal(Math.pow(A.doubleValue(), (year+1))).setScale(10, BigDecimal.ROUND_HALF_UP);
			B=new BigDecimal(Math.pow(A.doubleValue(), (year))).setScale(10, BigDecimal.ROUND_HALF_UP);
			if(calcExpensesVO.getReducedExpenses()!=null){
				netNpvAmt=calcExpensesVO.getReducedExpenses().divide(B,0,BigDecimal.ROUND_HALF_UP);
				calcExpensesVO.setNetNPVAmt(netNpvAmt);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	/**
	 * @param insurCalculatorVO
	 */
	public void calculateTpdReplaceExpenses(InsurCalculatorVO insurCalculatorVO, Map ruleInfoMap){
		int diffBwAgeAndRetireAge=0;
		try{
			if(insurCalculatorVO!=null){
				calculateRatioOfCover(insurCalculatorVO);/*This method calculates the ratio of cover*/
//				System.out.println(">>>insurCalculatorVO.getRatioOfCover()>>>"+insurCalculatorVO.getRatioOfCover());
				
				calculateOtherExpensesForTPD(insurCalculatorVO);/*This method calculates the TPD Other Expenses*/
//				System.out.println(">>>insurCalculatorVO.getOtherExpensesForTPD>>>"+insurCalculatorVO.getOtherExpensesForTPD());
				
				calculateChildExpenseRatio(insurCalculatorVO);/*This method calculates the child Expense Ratio percentage*/
//				System.out.println(">>>insurCalculatorVO.getChildExpenseRatio>>>"+insurCalculatorVO.getChildExpenseRatio());
//				
//				calculateAdultExpenseRatio(insurCalculatorVO);/*This method calculates the adult Expense Ratio percentage*/
//				System.out.println(">>>insurCalculatorVO.getAdultExpenseRatio>>>"+insurCalculatorVO.getAdultExpenseRatio());
//				
//				calculateMiscExpenseRatio(insurCalculatorVO);/*This method calculates the Misc Expense Ratio percentage*/
//				System.out.println(">>>insurCalculatorVO.getMiscExpenseRatio>>>"+insurCalculatorVO.getMiscExpenseRatio());
				
				calculateIPForTPD(insurCalculatorVO,ruleInfoMap);/*This method calculates the IP amount to use in tpd replace covers*/
//				System.out.println(">>>>>insurCalculatorVO.getIpForTPD()>>>>>"+insurCalculatorVO.getIpForTPD());
//				System.out.println(">>>>>insurCalculatorVO.getInflationRate()>>>>>"+insurCalculatorVO.getInflationRate());
//				System.out.println(">>>>>insurCalculatorVO.getRealInterestRate()>>>>>"+insurCalculatorVO.getRealInterestRate());
//				System.out.println(">>>>>insurCalculatorVO.getIpBenefitPerVal()>>>>>"+insurCalculatorVO.getIpBenefitPerVal());
				diffBwAgeAndRetireAge=insurCalculatorVO.getRetirementAge()-insurCalculatorVO.getAge();
				if(diffBwAgeAndRetireAge!=0){
					BigDecimal cumulativeTPDExpenses=BigDecimal.ZERO;
					BigDecimal cumulativeTNPV=BigDecimal.ZERO;
					for(int year=0;year<diffBwAgeAndRetireAge;year++){
						CalcExpensesVO calcExpensesVO=new CalcExpensesVO();
						calculateTPDExpenses(calcExpensesVO,insurCalculatorVO,year);
						//System.out.println("Expenses--["+calcExpensesVO.getExpenses()+"]");
//						System.out.println(">>>>calcExpensesVO.getExpenses()>>"+calcExpensesVO.getExpenses());
						calculateLoopChildPercentage(calcExpensesVO,insurCalculatorVO,year);
//						System.out.println("ChildPerc--["+calcExpensesVO.getChildPercentage()+"]");
//						System.out.println(">>>>calcExpensesVO.getChildPercentage()>>"+calcExpensesVO.getChildPercentage());
						
						calculateTPDReducesExpenses(calcExpensesVO,insurCalculatorVO,year);
						//System.out.println("Red:Expenses--["+calcExpensesVO.getReducedExpenses()+"]");
//						System.out.println(">>>>calcExpensesVO.getReducedExpenses()>>"+calcExpensesVO.getReducedExpenses());
						
//						calculateLoopNPVAmount(calcExpensesVO,insurCalculatorVO,year);
//						System.out.println(">>>>calcExpensesVO.getNpvAmount()>>"+calcExpensesVO.getNpvAmount());
//						cumulativeTPDExpenses=cumulativeTPDExpenses.add(calcExpensesVO.getNpvAmount());
						
						calculateLoopIPAmount(calcExpensesVO,insurCalculatorVO,year);
//						System.out.println(">>>>calcExpensesVO.getIpBenefitAmount()>>"+calcExpensesVO.getIpBenefitAmount());
						
						calculateTPDNetExpenses(calcExpensesVO,insurCalculatorVO,year);
						//System.out.println(">>>>calcExpensesVO.getNetExpenses()>>"+calcExpensesVO.getNetExpenses());
						
						calculateTPDNetNPVAmt(calcExpensesVO,insurCalculatorVO,year);
						//System.out.println("Net Npv--["+calcExpensesVO.getNetNPVAmt()+"]");
//						System.out.println(">>>>calcExpensesVO.getNetNPVAmt()>>"+calcExpensesVO.getNetNPVAmt());
						cumulativeTNPV=cumulativeTNPV.add(calcExpensesVO.getNetNPVAmt());
					}
					insurCalculatorVO.setTtlTPDExpenses(cumulativeTPDExpenses);
					insurCalculatorVO.setTtlTpdNpvAmt(cumulativeTNPV);
					System.out.println(">>>>cumulativeTPDExpenses>>"+cumulativeTPDExpenses);
					System.out.println(">>>>Final Tpd AMt>>"+cumulativeTNPV);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	public InsurCalculatorVO calculateTPDNetNPVAmt(CalcExpensesVO calcExpensesVO,InsurCalculatorVO insurCalculatorVO,int year){
//		BigDecimal A=null;/* 1 + insurCalculatorVO.getInflationRate() +  insurCalculatorVO.getRealInterestRate()*/
		/*SRKR: 21/01/2014 Formulae changed*/
		BigDecimal A=null;/* 1 + insurCalculatorVO.getRealInterestRate()*/
		BigDecimal B=null;/* A to the power of year+1 *//*SRKR: 21/01/2014 Formulae changed A to the power of year */
		
		BigDecimal netNpvAmt=BigDecimal.ZERO;
		try{
//			A=BigDecimal.ONE.add(convertToPercentage(insurCalculatorVO.getInflationRate()).add(convertToPercentage(insurCalculatorVO.getRealInterestRate())));
			A=BigDecimal.ONE.add(convertToPercentage(insurCalculatorVO.getRealInterestRate()));
//			B=new BigDecimal(Math.pow(A.doubleValue(), (year+1))).setScale(10, BigDecimal.ROUND_HALF_UP);
			B=new BigDecimal(Math.pow(A.doubleValue(), (year))).setScale(10, BigDecimal.ROUND_HALF_UP);
			if(calcExpensesVO.getNetExpenses()!=null){
				netNpvAmt=calcExpensesVO.getNetExpenses().divide(B,0,BigDecimal.ROUND_HALF_UP);
//				System.out.println(">>"+netNpvAmt);
//				System.out.println(">>"+netNpvAmt.setScale(2));
				calcExpensesVO.setNetNPVAmt(netNpvAmt);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	/**
	 * @param calcExpensesVO
	 * @param insurCalculatorVO
	 * @param year
	 * @return
	 */
	public InsurCalculatorVO calculateTPDNetExpenses(CalcExpensesVO calcExpensesVO,InsurCalculatorVO insurCalculatorVO,int year){
		try{
			if(insurCalculatorVO!=null && calcExpensesVO.getReducedExpenses()!=null && calcExpensesVO.getIpBenefitAmount()!=null){
				calcExpensesVO.setNetExpenses(calcExpensesVO.getReducedExpenses().subtract(calcExpensesVO.getIpBenefitAmount()));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	/**
	 * @param calcExpensesVO
	 * @param insurCalculatorVO
	 * @param year
	 * @return
	 */
	public InsurCalculatorVO calculateLoopIPAmount(CalcExpensesVO calcExpensesVO,InsurCalculatorVO insurCalculatorVO,int year){
		BigDecimal A=null;/* 1 + insurCalculatorVO.getInflationRate()*/
		BigDecimal B=null;/* A to the power of year */
		BigDecimal ipBenefitAmt=BigDecimal.ZERO;
		try{
			if(insurCalculatorVO!=null && insurCalculatorVO.getInflationRate()!=null && insurCalculatorVO.getIpForTPD()!=null){
				if(year<insurCalculatorVO.getIpBenefitPerVal()){
					A=BigDecimal.ONE.add(convertToPercentage(insurCalculatorVO.getInflationRate()));
					B=new BigDecimal(Math.pow(A.doubleValue(), year)).setScale(10, BigDecimal.ROUND_HALF_UP);
//					System.out.println(">>insurCalculatorVO.getIpForTPD()>>"+insurCalculatorVO.getIpForTPD());
					ipBenefitAmt=(new BigDecimal(12).multiply(insurCalculatorVO.getIpForTPD().multiply(B))).setScale(0, BigDecimal.ROUND_HALF_UP);
					
					calcExpensesVO.setIpBenefitAmount(ipBenefitAmt);
				}else{
					calcExpensesVO.setIpBenefitAmount(ipBenefitAmt);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	/**
	 * @param calcExpensesVO
	 * @param insurCalculatorVO
	 * @param year
	 * @return
	 */
	public InsurCalculatorVO calculateLoopNPVAmount(CalcExpensesVO calcExpensesVO,InsurCalculatorVO insurCalculatorVO,int year){
		BigDecimal A=null;/* 1 + insurCalculatorVO.getInflationRate() +  insurCalculatorVO.getRealInterestRate()*/
		BigDecimal B=null;/* A to the power of year +1*/
		try{
			if(insurCalculatorVO!=null && insurCalculatorVO.getInflationRate()!=null && insurCalculatorVO.getRealInterestRate()!=null){
				A=BigDecimal.ONE.add(convertToPercentage(insurCalculatorVO.getInflationRate()).add(convertToPercentage(insurCalculatorVO.getRealInterestRate())));
				B=new BigDecimal(Math.pow(A.doubleValue(), (year+1))).setScale(10, BigDecimal.ROUND_HALF_UP);
				calcExpensesVO.setNpvAmount(calcExpensesVO.getReducedExpenses().divide(B,0,BigDecimal.ROUND_HALF_UP)); 
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	/**This method calculates the reduces expenses
	 * Formula
	 * @param calcExpensesVO
	 * @param insurCalculatorVO
	 * @param year
	 * @return
	 */
	public InsurCalculatorVO calculateTPDReducesExpenses(CalcExpensesVO calcExpensesVO,InsurCalculatorVO insurCalculatorVO,int year){
		BigDecimal A=null;/* 1 - calcExpensesVO.getChildPercentage*/
		try{
			if(insurCalculatorVO!=null && insurCalculatorVO.getOtherExpensesForTPD()!=null && insurCalculatorVO.getInflationRate()!=null){
				A=BigDecimal.ONE.subtract(calcExpensesVO.getChildPercentage());
				calcExpensesVO.setReducedExpenses((calcExpensesVO.getExpenses().multiply(A)).setScale(0, BigDecimal.ROUND_HALF_UP)); 
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	/**
	 * This method calculte child percentage for all the years between age and retirement age
	 * Formula
	 * @param calcExpensesVO
	 * @param insurCalculatorVO
	 * @param year
	 * @return
	 */
	public InsurCalculatorVO calculateLoopChildPercentage(CalcExpensesVO calcExpensesVO,InsurCalculatorVO insurCalculatorVO,int year){
		try{
			int childAge;
			BigDecimal childPerc=BigDecimal.ZERO;
			if(insurCalculatorVO!=null && insurCalculatorVO.getKidsList()!=null){
				for(KidsVO kidsVO:insurCalculatorVO.getKidsList()){
//					System.out.println("Kids age- ["+kidsVO.getAge()+"]");
				  if(null!=kidsVO.getAge()){
					if((kidsVO.getAge()+year)>insurCalculatorVO.getDependentSuppAge()){
						childPerc=childPerc.add(insurCalculatorVO.getChildExpenseRatio());
					}
				  }
				}
			}
			calcExpensesVO.setChildPercentage(childPerc);
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	/**
	 * This methods calculate expenses for all the years between age and retirementage
	 * Formulae
	 * otherexpenesfortpd X (1 + inflation rate) to the power of Year
	 * @param calcExpensesVO
	 * @param insurCalculatorVO
	 * @param year
	 * @return
	 */
	public InsurCalculatorVO calculateTPDExpenses(CalcExpensesVO calcExpensesVO,InsurCalculatorVO insurCalculatorVO,int year){
		BigDecimal A=null;/* 1 + inflation rate*/
		BigDecimal B=null; /* (1 + inflation rate) to the power of year*/
		try{
			if(insurCalculatorVO!=null && insurCalculatorVO.getOtherExpensesForTPD()!=null && insurCalculatorVO.getInflationRate()!=null){
				A=BigDecimal.ONE.add(convertToPercentage(insurCalculatorVO.getInflationRate()));
				B=new BigDecimal(Math.pow(A.doubleValue(), year));
				calcExpensesVO.setExpenses((insurCalculatorVO.getOtherExpensesForTPD().multiply(B)).setScale(0, BigDecimal.ROUND_HALF_UP)); 
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	/**
	 * This method calculates the Ip amount used in tpd replace expense calculation
	 * formula
	 * if excludeip is true
	 * 		then ipfortpd is zeor
	 * else
	 * 		ipcoveramount X (1 -averagetaxrate)X (1 - income pro. benefit paid to super)
	 * 	    SRKR:21/01/2014 Formula changed to
	 *      ipcoveramount X (1 -averagetaxrate)X Income Protection replacement ratio/(income pro. benefit paid to super + Income Protection replacement ratio)
	 * @param insurCalculatorVO
	 * @return
	 */
	public InsurCalculatorVO calculateIPForTPD(InsurCalculatorVO insurCalculatorVO,Map ruleInfoMap){
		BigDecimal ipForTpd=BigDecimal.ZERO;
		try{
			if(insurCalculatorVO!=null){
				if(insurCalculatorVO.isExcludeIP()){
					insurCalculatorVO.setIpForTPD(ipForTpd);
				}else{
					calculateAverageTaxRate(insurCalculatorVO,ruleInfoMap);
//					System.out.println(">>>>insurCalculatorVO.getAverageTaxRate()>>>>>"+insurCalculatorVO.getAverageTaxRate());
					if(insurCalculatorVO.getRequiredIpCover()!=null){
//						ipForTpd=((insurCalculatorVO.getRequiredIpCover().multiply(BigDecimal.ONE.subtract(insurCalculatorVO.getAverageTaxRate()))).multiply(BigDecimal.ONE.subtract(convertToPercentage(insurCalculatorVO.getIpBenefitPaidToSA())))).setScale(0, BigDecimal.ROUND_HALF_UP);
						/*SRKR: ipcoveramount X (1 -averagetaxrate)X Income Protection replacement ratio/(income pro. benefit paid to super + Income Protection replacement ratio)*/
						ipForTpd=((insurCalculatorVO.getRequiredIpCover().multiply
								(BigDecimal.ONE.subtract(insurCalculatorVO.getAverageTaxRate()))).multiply
								(convertToPercentage(insurCalculatorVO.getIpReplaceRatio()).divide
										(convertToPercentage(insurCalculatorVO.getIpReplaceRatio()).add(convertToPercentage(insurCalculatorVO.getIpBenefitPaidToSA())),10,BigDecimal.ROUND_HALF_UP))).setScale(0, BigDecimal.ROUND_HALF_UP);
						
						System.out.println(">>>>insurCalculatorVO.getRequiredIpCover()>>>>"+insurCalculatorVO.getRequiredIpCover());
						System.out.println(">>>>insurCalculatorVO.getAverageTaxRate()>>>>"+insurCalculatorVO.getAverageTaxRate());
						System.out.println(">>>>insurCalculatorVO.getIpReplaceRatio()>>>>"+insurCalculatorVO.getIpReplaceRatio());
						System.out.println(">>>>insurCalculatorVO.getIpBenefitPaidToSA()>>>>"+insurCalculatorVO.getIpBenefitPaidToSA());
					}
				}
				insurCalculatorVO.setIpForTPD(ipForTpd);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	/**
	 * @param insurCalculatorVO
	 * @return
	 */
	public InsurCalculatorVO calculateAverageTaxRate(InsurCalculatorVO insurCalculatorVO,Map ruleInfoMap){
		BigDecimal taxableAmount=null,avgTaxRate=BigDecimal.ZERO;
		double taxableAmt=0;
		try{
			if(insurCalculatorVO!=null && insurCalculatorVO.getRequiredIpCover()!=null){
				taxableAmount=new BigDecimal(12).multiply(insurCalculatorVO.getRequiredIpCover());
				if(taxableAmount!=null){
					taxableAmt=taxableAmount.doubleValue();
					if(taxableAmt>0){
						InsurCalculatorRuleBO insurCalculatorRuleBO = new InsurCalculatorRuleBO();
						insurCalculatorRuleBO.setTtlIncome(taxableAmt);
						/*InstProductRuleHelper.RULE_FILE_LOC = ConfigurationHelper.getConfigurationValue(InstCoverDetailsConstant.RULE_FILE_LOC, InstCoverDetailsConstant.RULE_FILE_LOC);
						if(ruleInfoMap!=null && ruleInfoMap.get(InstCoverDetailsConstant.RULE_NAME_INST_CALCULATOR_TAX)!=null){
							insurCalculatorRuleBO.setKbase((KnowledgeBase) ruleInfoMap.get(InstCoverDetailsConstant.RULE_NAME_INST_CALCULATOR_TAX));
						}
						insurCalculatorRuleBO.setRuleFileName(InstCoverDetailsConstant.RULE_NAME_INST_CALCULATOR_TAX);
						insurCalculatorRuleBO = PrimeSuperFundRuleHelper.invokeInsurCalRue(insurCalculatorRuleBO);*/
//						System.out.println(">insurCalculatorRuleBO.getYearlyTax()>>"+insurCalculatorRuleBO.getYearlyTax());;
						String ruleNameAsPerFund;
						
						ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_CALCULATOR_TAX;
						insurCalculatorRuleBO.setRuleFileName(ruleNameAsPerFund);
						insurCalculatorRuleBO=CalculateHelper.calculateAverageTaxRate(insurCalculatorRuleBO,eappRulesCacheProvider.getRuleMap(), eappRulesCacheProvider.getFilePath(),ruleNameAsPerFund);
						if(insurCalculatorRuleBO.getYearlyTax()!=null){
							avgTaxRate=insurCalculatorRuleBO.getYearlyTax().divide(taxableAmount,2,BigDecimal.ROUND_HALF_UP);
						}
					}
					insurCalculatorVO.setAverageTaxRate(avgTaxRate);
//					System.out.println(">avgTaxRate>>"+avgTaxRate);;
				}
			}else{
				System.out.println(">>>>>insurCalculatorVO is null or insurCalculatorVO.getRequiredIpCover() is null>>>>>"+insurCalculatorVO);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	/**
	 * Calculate Average tax rate
	 * @param insurCalculatorVO
	 * @return
	 */
	public InsurCalculatorVO calculateAverageTaxRateOld(InsurCalculatorVO insurCalculatorVO){
		BigDecimal taxableAmount=null;
		double taxableAmt=0,avgTaxRate=0;
		try{
			if(insurCalculatorVO!=null && insurCalculatorVO.getRequiredIpCover()!=null){
				taxableAmount=(new BigDecimal(12).multiply(insurCalculatorVO.getRequiredIpCover())).multiply(BigDecimal.ONE.subtract(convertToPercentage(insurCalculatorVO.getIpBenefitPaidToSA())));
				if(taxableAmount!=null){
					taxableAmt=taxableAmount.doubleValue();
					if(taxableAmt > 0 && taxableAmt <=20000 ){
						avgTaxRate=0;
					}else if(taxableAmt > 20001 && taxableAmt <=40000){
						avgTaxRate=0.10;
					}else if(taxableAmt > 40001 && taxableAmt <=80000){
						avgTaxRate=0.15;
					}else if(taxableAmt > 80001 && taxableAmt <=120000){
						avgTaxRate=0.15;
					}else if(taxableAmt > 120001 && taxableAmt <=180000){
						avgTaxRate=0.15;
					}else if(taxableAmt > 180001){
						avgTaxRate=0.40;
					}
					insurCalculatorVO.setAverageTaxRate(new BigDecimal(avgTaxRate).setScale(2, BigDecimal.ROUND_HALF_UP));
				}
			}else{
				System.out.println(">>>>>insurCalculatorVO is null or insurCalculatorVO.getRequiredIpCover() is null>>>>>"+insurCalculatorVO);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	/**
	 * This method calculates the child expense ratio
	 * Formula
	 * childExpenseRatio = childfactor/insurCalculatorVO.getTotalWeight()
	 * @param insurCalculatorVO
	 * @return
	 */
	public InsurCalculatorVO calculateChildExpenseRatio(InsurCalculatorVO insurCalculatorVO){
		BigDecimal childExpenseRatio=null;
		try{
			if(insurCalculatorVO!=null){
				if(insurCalculatorVO.getKidsList()!=null && insurCalculatorVO.getKidsList().size()>0){
					calculateAdultWeight(insurCalculatorVO);
//					System.out.println(">>>>insurCalculatorVO.getAdultWeight()>>"+insurCalculatorVO.getAdultWeight());
					calculateChildWeight(insurCalculatorVO);
//					System.out.println(">>>>insurCalculatorVO.getChildWeight()>>"+insurCalculatorVO.getChildWeight());
					calculateMiscWeight(insurCalculatorVO);
//					System.out.println(">>>>insurCalculatorVO.getMiscWeight()>>"+insurCalculatorVO.getMiscWeight());
					calculateTotalWeight(insurCalculatorVO);
//					System.out.println(">>>>insurCalculatorVO.getTotalWeight()>>"+insurCalculatorVO.getTotalWeight());
					childExpenseRatio=insurCalculatorVO.getChildFactor().divide(insurCalculatorVO.getTotalWeight(), 6,BigDecimal.ROUND_HALF_UP);
					insurCalculatorVO.setChildExpenseRatio(childExpenseRatio);
				}else{
					insurCalculatorVO.setChildExpenseRatio(BigDecimal.ZERO);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	/**
	 * This method calculates the adult expense ratio
	 * Formula
	 * childExpenseRatio = adultFactor/insurCalculatorVO.getTotalWeight()
	 * @param insurCalculatorVO
	 * @return
	 */
	public InsurCalculatorVO calculateAdultExpenseRatio(InsurCalculatorVO insurCalculatorVO){
		BigDecimal adultExpenseRatio=null;
		try{
			if(insurCalculatorVO!=null){
					calculateAdultWeight(insurCalculatorVO);
//					System.out.println(">>>>insurCalculatorVO.getAdultWeight()>>"+insurCalculatorVO.getAdultWeight());
					calculateChildWeight(insurCalculatorVO);
//					System.out.println(">>>>insurCalculatorVO.getChildWeight()>>"+insurCalculatorVO.getChildWeight());
					calculateMiscWeight(insurCalculatorVO);
//					System.out.println(">>>>insurCalculatorVO.getMiscWeight()>>"+insurCalculatorVO.getMiscWeight());
					calculateTotalWeight(insurCalculatorVO);
//					System.out.println(">>>>insurCalculatorVO.getTotalWeight()>>"+insurCalculatorVO.getTotalWeight());
					adultExpenseRatio=insurCalculatorVO.getAdultFactor().divide(insurCalculatorVO.getTotalWeight(), 6,BigDecimal.ROUND_HALF_UP);
					insurCalculatorVO.setAdultExpenseRatio(adultExpenseRatio);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	/**
	 * This method calculates the misc expense ratio
	 * Formula
	 * childExpenseRatio = miscFactor/insurCalculatorVO.getTotalWeight()
	 * @param insurCalculatorVO
	 * @return
	 */
	public InsurCalculatorVO calculateMiscExpenseRatio(InsurCalculatorVO insurCalculatorVO){
		BigDecimal miscExpenseRatio=null;
		try{
			if(insurCalculatorVO!=null){
					calculateAdultWeight(insurCalculatorVO);
//					System.out.println(">>>>insurCalculatorVO.getAdultWeight()>>"+insurCalculatorVO.getAdultWeight());
					calculateChildWeight(insurCalculatorVO);
//					System.out.println(">>>>insurCalculatorVO.getChildWeight()>>"+insurCalculatorVO.getChildWeight());
					calculateMiscWeight(insurCalculatorVO);
//					System.out.println(">>>>insurCalculatorVO.getMiscWeight()>>"+insurCalculatorVO.getMiscWeight());
					calculateTotalWeight(insurCalculatorVO);
//					System.out.println(">>>>insurCalculatorVO.getTotalWeight()>>"+insurCalculatorVO.getTotalWeight());
					miscExpenseRatio=insurCalculatorVO.getMiscFactor().divide(insurCalculatorVO.getTotalWeight(), 2,BigDecimal.ROUND_HALF_UP);
					insurCalculatorVO.setMiscExpenseRatio(miscExpenseRatio);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	/**
	 * This method calculates the adult weight
	 * Formulae:
	 * if living with spouse
	 * 		adultweight = 2 X adultfactor
	 * else 
	 * 		adultweight = adultfactor
	 * @param insurCalculatorVO
	 * @return
	 */
	public InsurCalculatorVO calculateAdultWeight(InsurCalculatorVO insurCalculatorVO){
		try{
			if(insurCalculatorVO!=null && insurCalculatorVO.getAdultFactor()!=null){
				if(insurCalculatorVO.isLivingWithSpouse()){
					insurCalculatorVO.setAdultWeight(new BigDecimal(2).multiply(insurCalculatorVO.getAdultFactor()));
				}else{
					insurCalculatorVO.setAdultWeight(insurCalculatorVO.getAdultFactor());
				}
			}else{
				System.out.println(">>>>>insurCalculatorVO is null or insurCalculatorVO.getAdultFactor() is null>>>>>"+insurCalculatorVO);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	
	/**
	 * This method calculates the child weight
	 * Formulae:
	 * childweight =  kidscount X childFactor
	 * @param insurCalculatorVO
	 * @return
	 */
	public InsurCalculatorVO calculateChildWeight(InsurCalculatorVO insurCalculatorVO){
		try{
			if(insurCalculatorVO!=null && insurCalculatorVO.getChildFactor()!=null){
					insurCalculatorVO.setChildWeight(insurCalculatorVO.getChildFactor().multiply(new BigDecimal(insurCalculatorVO.getKidsList().size())));
			}else{
				System.out.println(">>>>>insurCalculatorVO is null or insurCalculatorVO.getChildFactor() is null>>>>>"+insurCalculatorVO);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	
	/**
	 * This method calculates the misc weight
	 * Formulae:
	 * childweight =  miscFactor
	 * @param insurCalculatorVO
	 * @return
	 */
	public InsurCalculatorVO calculateMiscWeight(InsurCalculatorVO insurCalculatorVO){
		try{
			if(insurCalculatorVO!=null && insurCalculatorVO.getMiscFactor()!=null){
					insurCalculatorVO.setMiscWeight(insurCalculatorVO.getMiscFactor());
			}else{
				System.out.println(">>>>>insurCalculatorVO is null or insurCalculatorVO.getMiscFactor() is null>>>>>"+insurCalculatorVO);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	
	/**
	 * This method calculates the total weight
	 * Formulae:
	 * totalWeight = childweight+adultweight+miscweight
	 * @param insurCalculatorVO
	 * @return
	 */
	public InsurCalculatorVO calculateTotalWeight(InsurCalculatorVO insurCalculatorVO){
		try{
			if(insurCalculatorVO!=null && insurCalculatorVO.getAdultWeight()!=null && insurCalculatorVO.getChildWeight() !=null && insurCalculatorVO.getMiscWeight()!=null){
					insurCalculatorVO.setTotalWeight((insurCalculatorVO.getAdultWeight().add(insurCalculatorVO.getChildWeight())).add(insurCalculatorVO.getMiscWeight()));
			}else{
				System.out.println(">>>>>insurCalculatorVO is null or insurCalculatorVO.getAdultWeight() or insurCalculatorVO.getChildWeight() or insurCalculatorVO.getMiscWeight() is null>>>>>"+insurCalculatorVO);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	
	
	
	/**
	 * This method calculates the ratio of cover
	 * Formula:
	 * if living with spouse then grosssall/(grosssal+partnersal)
	 * else 100%
	 * @param insurCalculatorVO
	 */
	public InsurCalculatorVO calculateRatioOfCover(InsurCalculatorVO insurCalculatorVO){
		BigDecimal ratioOfCover=null;
		BigDecimal yearlyGrossSal,yearlyPartnerGrossSal;
		CalculateHelper calculateHelper = new CalculateHelper();
		try{
			if(insurCalculatorVO!=null){
				if(insurCalculatorVO.isLivingWithSpouse()){
					if(insurCalculatorVO.getGrossSalVal()!=null && insurCalculatorVO.getPartnerGrossSalVal()!=null){
						/*Assumption is always the gross sal is anually.. so converting if the user enters other than annual freq.type*/
						yearlyGrossSal=calculateHelper.convertToYearlyAmount(insurCalculatorVO.getIncomeSalPeriodVal(), insurCalculatorVO.getGrossSalVal());
						yearlyPartnerGrossSal=calculateHelper.convertToYearlyAmount(insurCalculatorVO.getIncomePartnerSalPeriodVal(), insurCalculatorVO.getPartnerGrossSalVal());
						BigDecimal totalFamilySal=yearlyGrossSal.add(yearlyPartnerGrossSal);
						ratioOfCover=yearlyGrossSal.divide(totalFamilySal, 4,BigDecimal.ROUND_HALF_UP);
					}else if(insurCalculatorVO.getGrossSalVal()!=null) {
						ratioOfCover=BigDecimal.ONE;
					}
				}else{
					ratioOfCover=BigDecimal.ONE;
				}
				insurCalculatorVO.setRatioOfCover(ratioOfCover);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	
	/**
	 * This method caclualtes the other expenses for TPD
	 * Formula:
	 * o/p of calculateRatioOfCover = ratioOfCover
	 * otherExpensesForTPD=ratioOfCover X annual expenses entered on screen
	 * @param insurCalculatorVO
	 * @return
	 */
	public InsurCalculatorVO calculateOtherExpensesForTPD(InsurCalculatorVO insurCalculatorVO){
		BigDecimal otherExpensesForTPD=null,yearlyExpenses;
		CalculateHelper calculateHelper = new CalculateHelper();
		try{
			if(insurCalculatorVO!=null && insurCalculatorVO.getRatioOfCover()!=null && insurCalculatorVO.getExpensesVal()!=null){
				/*Assumption is always the expenses are anually.. so converting if the user enters other than annual freq.type*/
				yearlyExpenses=calculateHelper.convertToYearlyAmount(insurCalculatorVO.getExpenses(), insurCalculatorVO.getExpensesVal());
				otherExpensesForTPD=yearlyExpenses.multiply(insurCalculatorVO.getRatioOfCover());
				insurCalculatorVO.setOtherExpensesForTPD(otherExpensesForTPD);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return insurCalculatorVO;
	}
	
	/**
	 * This method converts the percentage into decimal value
	 * for expample i/p is 50% o/p is 0.5
	 * 				i/p is 10% o/p is 0.01
	 * @param percentVal
	 * @return
	 */
	public BigDecimal convertToPercentage(BigDecimal percentVal){
		if(percentVal!=null){
			percentVal=percentVal.multiply(new BigDecimal(0.01));
		}
		return percentVal;
	}
	
	public void calculateEligibleSuperAmt(InsurCalculatorVO insurCalculatorVO){
		int diffBwAgeAndRetireAge=0;
		Calendar cal=null;
		List<SuperAnnuationVO> superAnnuationList=null;
		BigDecimal A;/*Gross Salary X ActualSuper Percentage*/
		BigDecimal B;/*1 + CPI Percentage*/
		BigDecimal C;/*B Power Year*/
		BigDecimal D;/*B + Real IR Percentage*/
		BigDecimal E;/*D Power Year+2*/
		BigDecimal ttlSuperNPVAmt=BigDecimal.ZERO,yearlyGrossSal;
		CalculateHelper calculateHelper = new CalculateHelper();
		try{
			if(insurCalculatorVO!=null){
				cal=Calendar.getInstance();
				diffBwAgeAndRetireAge=insurCalculatorVO.getRetirementAge()-insurCalculatorVO.getAge();
				superAnnuationList=new ArrayList<SuperAnnuationVO>();
				if(diffBwAgeAndRetireAge!=0){
					for(int year=0;year<diffBwAgeAndRetireAge;year++){
						SuperAnnuationVO superAnnuationVO=new SuperAnnuationVO();
						superAnnuationVO=getMinSuperPerc(superAnnuationVO,insurCalculatorVO.getMinSuperMap(),year,cal.get(Calendar.YEAR));
						superAnnuationVO=getActualSuperPerc(superAnnuationVO,insurCalculatorVO.getSuperContribRate());
						
						/*Assumption is always the gross sal is anually.. so converting if the user enters other than annual freq.type*/
						yearlyGrossSal=calculateHelper.convertToYearlyAmount(insurCalculatorVO.getIncomeSalPeriodVal(), insurCalculatorVO.getGrossSalVal());
						/*Calculate Super*/
						A=yearlyGrossSal.multiply(superAnnuationVO.getActualSuperPerc().multiply(new BigDecimal(0.01)));
						B=BigDecimal.ONE.add(insurCalculatorVO.getInflationRate().multiply(new BigDecimal(0.01)));
//						C=B.pow(year, new MathContext(2));
						C=new BigDecimal(Math.pow(B.doubleValue(), year));
						superAnnuationVO.setSuperContribAmt(A.multiply(C).setScale(0, BigDecimal.ROUND_HALF_UP));
//						D=B.add(insurCalculatorVO.getRealInterestRate().multiply(new BigDecimal(0.01)));
						/*SRKR; 21/01/2014 FOrmula changed 1 + Realinterestrate instead of  1 + (inflation rate+realinterest rate)*/
						D=BigDecimal.ONE.add(insurCalculatorVO.getRealInterestRate().multiply(new BigDecimal(0.01)));
						/*SRKR; 21/01/2014 FOrmula D to the power of year*/
//						E=new BigDecimal(Math.pow(D.doubleValue(), (year+2)));
						E=new BigDecimal(Math.pow(D.doubleValue(), (year)));
//						System.out.println(superAnnuationVO.getSuperContribAmt().divide(E,0,BigDecimal.ROUND_HALF_UP));
						superAnnuationVO.setSuperNPVAmt(superAnnuationVO.getSuperContribAmt().divide(E,0,BigDecimal.ROUND_HALF_UP));
//						System.out.println("A--"+A);
//						System.out.println("B--"+B);
//						System.out.println("C--"+C);
//						System.out.println(">>>>"+Math.pow(B.doubleValue(), year));
//						System.out.println(A.multiply(C));
//						System.out.println(superAnnuationVO.getSuperContribAmt());
						ttlSuperNPVAmt=ttlSuperNPVAmt.add(superAnnuationVO.getSuperNPVAmt());
						superAnnuationList.add(superAnnuationVO);
					}
				}
				System.out.println(">>ttlSuperNPVAmt>>>>"+ttlSuperNPVAmt);
				insurCalculatorVO.setTotalSuperAmt(ttlSuperNPVAmt);
			}	
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public SuperAnnuationVO getMinSuperPerc(SuperAnnuationVO superAnnuationVO, Map minSuperMap, int year,int currentYear){
		try{
			int yearFactor=currentYear+year;
			if(superAnnuationVO!=null && minSuperMap!=null){
				if(minSuperMap.get(""+yearFactor)!=null){
					superAnnuationVO.setMinSuperPerc((BigDecimal) minSuperMap.get(""+yearFactor));
				}else{
					superAnnuationVO.setMinSuperPerc((BigDecimal) minSuperMap.get("DEFAULT"));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return superAnnuationVO;
		
	}
	
	public SuperAnnuationVO getActualSuperPerc(SuperAnnuationVO superAnnuationVO, BigDecimal superContribRate){
		try{
			if(superAnnuationVO!=null && superAnnuationVO.getMinSuperPerc()!=null && superContribRate!=null){
				superAnnuationVO.setActualSuperPerc(superAnnuationVO.getMinSuperPerc().max(superContribRate));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return superAnnuationVO;
	}
	
	@Override
	public List<InsuranceRuleInfo> getInsuranceRule(String fundId) {
		log.info("Get getInsuranceRule start");
		InsurCalculatorRuleBO insurCalculatorRuleBO=null;
		List<InsuranceRuleInfo> list = new ArrayList<>();
		InsuranceRuleInfo insuranceRule = new InsuranceRuleInfo();
		try{
			if(fundId!=null){
				 insurCalculatorRuleBO = new InsurCalculatorRuleBO();
				
					String ruleNameAsPerFund;
					
					ruleNameAsPerFund=QuoteConstants.RULE_NAME_INST_CALCULATOR_CONFIG;
					insurCalculatorRuleBO.setRuleFileName(ruleNameAsPerFund);
					insurCalculatorRuleBO.setProductCode(fundId);
					
					insurCalculatorRuleBO=CalculateHelper.invokeInsuranceCalcRule(insurCalculatorRuleBO,eappRulesCacheProvider.getRuleMap(), eappRulesCacheProvider.getFilePath(),ruleNameAsPerFund);	
				}
					
			insuranceRule.setDependentSuppAge(insurCalculatorRuleBO.getDefaultDependSuppAge());
			insuranceRule.setInflationRate(insurCalculatorRuleBO.getDefaultInflationRate());
			insuranceRule.setIpBenefitPaidToSA(insurCalculatorRuleBO.getDefaultIPBenefitToSA());
			insuranceRule.setIpReplaceRatio(insurCalculatorRuleBO.getDefaultIpReplaceRatio());
			insuranceRule.setRealInterestRate(insurCalculatorRuleBO.getDefaultRealIntrRate());
			insuranceRule.setRetirementAge(insurCalculatorRuleBO.getDefaultRetirementAge());
			insuranceRule.setSuperContribRate(insurCalculatorRuleBO.getDefaultSuperContribRate());
			insuranceRule.setFuneralCost(insurCalculatorRuleBO.getDefaultFuneralCost());
			insuranceRule.setAnnualMedAndNursingCost(insurCalculatorRuleBO.getDefaultMedicalCost());
							
			list.add(insuranceRule);
		}catch(Exception e){
			log.error("Error in getInsuranceRule : {}",e);
		}
		log.info("Get getInsuranceRule finish");
		return list;
	}
	
	
	
}
