var appEndpoints = {
  "clientDataUrl":"getcustomerdata",
  "specialCvrEligUrl":"getDefaultUnitsForSpecialCover",
  "appNumUrl":"getUniqueNumber",
  "savedAppUrl":"checkSavedApplications",
  "cancelAppUrl":"expireApplication",
  "quoteUrl":"getIndustryList",
  "occupationUrl":"getOccupationName",
  "calculateUrl":"calculateAll",
  "newOccupationUrl":"getNewOccupationList",
  "maxLimitUrl":"getProductConfig",
  "convertCoverUrl":"convertCover",
  "calculateDeathUrl":"calculateDeath",
  "calculateTpdUrl":"calculateTPD",
  "calculateIpUrl":"calculateIP",
  "saveEapplyUrl":"saveEapply",
  "retrieveAppUrl":"retieveApplication",
  "auraTransferInitiateUrl":"initiate",
  "auraTransferDataUrl":"initiate",
  "auraPostUrl":"underwriting",
  "submitAuraUrl":"submitAura",
  "clientMatchUrl":"https://www.e2e.eapplication.metlife.com.au/services/ClientMatch",
  "submitEapplyUrl":"submitEapply",
  "transferCalculateUrl":"calculateTransfer",
  "retrieveSavedAppUrl":"retieveApps",
  "downloadUrl":"download",
  "printQuotePage":"printQuotePage",
  "fileUploadUrl": "fileUpload",
  "fileUploadPostUrl": "fileUploadPost",
  "accAppUrl": "checkAccApplications",
  "dupAppUrl": "checkDuplicateUWApplications",
  "lifeEvntEligUrl":"getDefaultUnitsForLifeEvent",
  "corporatePost":"corporate/fund",
  "fetchCategoryDetailUrl":"corporate/fund/cover",
  "calculatFUL":"calculateFUL",
  "memberDetailsUrlForDashboard": "corporate/memberdetails",
  "resendEmailUrlForDashboard":"corporate/email",
  "updateUrlForDashboard":"corporate/update",
  "decryptContent":"corporate/decryptFundCode",
  "saveCorporateEapply":"saveCorporateEapply",
  "saveEapplyUrlNew":"saveEapplyNew",
  "retrieveAppUrlNew":"retieveApplicationNew",
  "printQuotePageNew":"printQuotePageNew",
  "submitEapplyUrlNew":"submitEapplyNew",
  "fileUploadUrlNew": "fileUploadNew",
  "calculateAALUrl": "calculateAAL",
  "calculateINGDAll": "calculateINGDAll",
  "urlFilePath": 'CareSuperUrls.properties',
  "hostDecOccList": 'HostDecOccList.properties',
  "hostSplDecoccList": 'HostSpecialDecOccList.properties',
  "statewideDecOccList" : 'StatewideDecOccList.properties'
  };

  var eApplyInterceptor = angular.module('eApplyInterceptor', []);

  eApplyInterceptor.factory('eapplyrouter', function() {  
    return {
      request: function(config) {
        var context = '';
        if(!config.headers.Authorization) {
          config.headers.Authorization = inputData;
        }
        if(config.url.indexOf(appEndpoints.urlFilePath) !== 0 && (config.url.indexOf(appEndpoints.hostDecOccList)) !== 0 && (config.url.indexOf(appEndpoints.hostSplDecoccList)) !== 0 && (config.url.indexOf(appEndpoints.statewideDecOccList)) !== 0&& config._isTemplate) {
          config.url = config.url;
        } else if(config.url.indexOf(appEndpoints.urlFilePath) !== 0  && config.url.indexOf(appEndpoints.clientMatchUrl) !== 0 && (config.url.indexOf(appEndpoints.hostDecOccList)) !== 0 && (config.url.indexOf(appEndpoints.hostSplDecoccList)) !== 0 && (config.url.indexOf(appEndpoints.statewideDecOccList)) !== 0 && !config._isTemplate) {
          context = baseUrl.indexOf('localhost') === -1  ? 'ebusiness/' : '';
          config.url = baseUrl + context + appEndpoints[config.url];
        }
        else if(config.url.indexOf(appEndpoints.urlFilePath) === 0 && inputData) {
          //config.headers.Authorization = inputData;
        }
        return config;
      }
  };
  });