package pages.metlife;

import supportlibraries.ScriptHelper;
import uimap.metlife.ColesCCHealthAPageObjects;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

public class ColesCCHealthAPage extends MasterPage {
	WebDriverUtil driverUtil = null;
	
	public ColesCCHealthAPage enterColesCCHealthADetails() {
		fillColesCCHealthA();
		return new ColesCCHealthAPage(scriptHelper);
	} 

	public ColesCCHealthAPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		switchToActiveWindow();
	}

	public void fillColesCCHealthA() {
		try {

			switchToActiveWindow();
			driver.manage().window().maximize();
			tryAction(waitForClickableElement(ColesCCHealthAPageObjects.lnkNext),"Click", "Next");
		} catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
