package au.com.metlife.eapply.utility;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendFileEmail {
	private static final Logger log = LoggerFactory.getLogger(SendFileEmail.class);

	
	public void sendEmail(){
		log.info("Initialinzing Mail sending start");
		
	      /*Recipient's email ID needs to be mentioned.*/	     
		  String to = "akishore1@metlife.com";

	       /*Sender's email ID needs to be mentioned*/	     
		  String from = "metlife.service@ausmetlife.com";

	      /*Assuming you are sending email from localhost*/
	      String host = "cluster4out.us.messagelabs.com";

	      /*Get system properties*/
	      Properties properties = System.getProperties();

	      /*Setup mail server*/
	      properties.setProperty("mail.smtp.host", host);

	      /*Get the default Session object.*/
	      Session session = Session.getDefaultInstance(properties);

	      try {
	         /*Create a default MimeMessage object.*/
	         MimeMessage message = new MimeMessage(session);

	         /*Set From: header field of the header.*/
	         message.setFrom(new InternetAddress(from));

	         /*Set To: header field of the header.*/
	         message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));

	         /*Set Subject: header field*/
	         message.setSubject("This is the Subject Line!");

	         /*Create the message part */
	         BodyPart messageBodyPart = new MimeBodyPart();

	         /* Fill the message*/
	         /*messageBodyPart.setText("This is message body");*/
	         
	         messageBodyPart.setContent("<h1>This is actual message</h1>", "text/html");
	         
	         /*Create a multipar message*/
	         Multipart multipart = new MimeMultipart();

	         /*Set text message part*/
	         multipart.addBodyPart(messageBodyPart);

	         /*Part two is attachment*/
	         messageBodyPart = new MimeBodyPart();
	         String filename = "/shared/props/eapply2/eaply2.properties";
	          
	         DataSource source = new FileDataSource(filename);
	         messageBodyPart.setDataHandler(new DataHandler(source));
	         messageBodyPart.setFileName(filename);
	         multipart.addBodyPart(messageBodyPart);

	         /*Send the complete message parts*/
	         message.setContent(multipart );

	         /* Send message*/	        
	         Transport.send(message);
	         log.info("Sent message successfully....");
	      }catch (MessagingException mex) {
	         log.error("Error in messaging" +mex);
	      }
	}
	
	   public static void main(String [] args) {     
		   SendFileEmail sendFileEmail = new SendFileEmail();
		   sendFileEmail.sendEmail();
	   }
	}
