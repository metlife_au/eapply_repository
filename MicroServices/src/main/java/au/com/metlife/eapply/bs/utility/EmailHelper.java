package au.com.metlife.eapply.bs.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.metlife.eapplication.common.JSONException;

import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.model.Applicant;
import au.com.metlife.eapply.bs.model.Cover;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.EapplyInput;
import au.com.metlife.eapply.bs.pdf.pdfObject.PDFObject;
import au.com.metlife.eapply.corporate.service.CorpService;
import au.com.metlife.eapply.repositories.CorporateFundRepo;
import au.com.metlife.eapply.repositories.model.CorporateFund;
import au.com.metlife.eapply.repositories.model.CorporateFundCat;
import au.com.metlife.eapply.underwriting.response.model.Decision;
import au.com.metlife.eapply.underwriting.response.model.Insured;
import au.com.metlife.eapply.underwriting.response.model.ResponseObject;

public class EmailHelper {
	private static final Logger log = LoggerFactory.getLogger(EmailHelper.class);
	public static String callEmailService(au.com.metlife.eapply.email.MetLifeEmailVO evo, String authId,Properties properties) throws IOException, JSONException {
		log.info("Call email service start");
		/*String urlStr = null;   */
		String autherisationId = authId;
		URL url = new URL(properties.getProperty("emailservice"));
		 
		StringBuilder responseBuff = null;
		  String token = null;
		  ObjectMapper mapperObj = new ObjectMapper();
    	String jsonStr = mapperObj.writeValueAsString(evo);
    	log.info("jsonStr>> {}",jsonStr);
    	System.out.println("Email json before sending: "+jsonStr);
		  HttpURLConnection conn =
		      (HttpURLConnection) url.openConnection();
		  conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			autherisationId = new String(Base64.getEncoder().encode(("Bearer "+autherisationId).getBytes()));
			log.info("autherisationId> {}",autherisationId);
			conn.setRequestProperty("access_token", autherisationId);
			/*String temp = new String(Base64.getEncoder().encode(("Bearer "+token).getBytes()));	*/
		
			conn.setRequestProperty("Authorization", autherisationId);
			
			conn.connect();
			OutputStream os = conn.getOutputStream();
			/*OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");*/
			os.write(jsonStr.getBytes());
			os.flush();
			os.close();
			
			
			
		  if (conn.getResponseCode() != 200) {
		    throw new IOException(conn.getResponseMessage());
		  }

		 /* Buffer the result into a string*/
		  BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		  responseBuff = new StringBuilder();
		  String line;
		  while ((line = rd.readLine()) != null) {
		   log.info("line>> {}",line);
		   responseBuff.append(line);
		  }
		  rd.close();		 
		
		  /*jsObject = new JSONObject(responseBuff.toString());
		  token= (String)jsObject.get("message");
		  log.info("token>> {}",token);	*/		
		  conn.disconnect();	
		  log.info("Call email service finish");
		  return token;
		}

	
	public void sendEmail(PDFObject pdfObject,Eapplication applicationDTO, 
            String overAllDecision, String pdfAttachDir, String lob, Properties properties, String authId,CorporateFund corpfund, Boolean mixedDecision, String partnerCode) {
		log.info("Send email start");
       
		String brokerContactNum=null;
        ArrayList mailList = new ArrayList();
        HashMap placeHolders = new HashMap();       
        String emailID = null; 
        au.com.metlife.eapply.email.MetLifeEmailVO eVO = new au.com.metlife.eapply.email.MetLifeEmailVO();     
        String applicantName = "";
        Applicant applicantDTO = null;
        Cover coversDTO = null;
        StringBuilder uwDecision = new StringBuilder();
        String acceptedCover = null;
        String declineCover = null;
        String declineReason = null;
        StringBuilder uwritingDecisionBlurb = new StringBuilder();
        boolean isSpecialTerm = Boolean.FALSE;
        /*String isCorpEapply=null;*/
        Boolean isChangeNo = null;
        String vicSuperLogo = null;
        if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode()))
        {
        vicSuperLogo = properties.getProperty("logo_VICT");
        }
        //Boolean mixedDecision = Boolean.FALSE;
        List<Cover> validCvrList = null;
        try {   
//        	if(null!=eapplyInput && null!=eapplyInput.getResponseObject()){
//        		mixedDecision = isMixedDecision(eapplyInput.getResponseObject());
//        	}        	
            StringBuilder applicantBulder = null;
            if (null!=pdfObject && null != pdfObject.getMemberDetails()) {
                applicantBulder = new StringBuilder( "");
                
                
               //emailID = pdfObject.getMemberDetails().getEmailId();  
                emailID=applicationDTO.getApplicant().get(0).getEmailid();
                if (pdfObject.getMemberDetails().getTitle() != null) {
                	  eVO.setTitle( pdfObject.getMemberDetails().getTitle());
                    applicantBulder.append( pdfObject.getMemberDetails().getTitle()
                            + " ");
                }else{
                	eVO.setTitle("");
                }
                if (pdfObject.getMemberDetails().getMemberFName() != null) {
                	 eVO.setFirstName( pdfObject.getMemberDetails().getMemberFName() );
                    applicantBulder.append( pdfObject.getMemberDetails().getMemberFName()
                            + " ");
                }else{
                	eVO.setFirstName("");
                }
                if (pdfObject.getMemberDetails().getMemberLName() != null) {
                	 eVO.setLastName( pdfObject.getMemberDetails().getMemberLName());
                    applicantBulder.append( pdfObject.getMemberDetails().getMemberLName());
                }else{
                	 eVO.setLastName( "");
                }
            }
            if (null != applicantBulder) {
                applicantName = applicantBulder.toString();
            }            
       if(MetlifeInstitutionalConstants.INSTITUTIONAL.equalsIgnoreCase(lob)
    		   && (null != applicationDTO && null != applicationDTO.getApplicant())){
        	   
		        for (int itr = 0; itr < applicationDTO.getApplicant().size(); itr++) {
		        	applicantDTO = applicationDTO.getApplicant().get( itr); 
		        	validCvrList = coversSelected(applicantDTO);
		            if (null != applicantDTO) {
		            	  if ("RUW".equalsIgnoreCase( applicationDTO.getAppdecision())) {
		                        uwDecision.append( "Referred to Underwriting ");                           
		                     
		                    }
		            	  else if ("DCL".equalsIgnoreCase(applicationDTO.getAppdecision())) {
		            		  uwDecision.append( "Declined "); 		                                                                                 
		                        
		                    } else if ("ACC".equalsIgnoreCase( applicationDTO.getAppdecision()) 
		                    	&& (applicationDTO.getPartnercode()!=null && !"AEIS".equalsIgnoreCase(applicationDTO.getPartnercode()))) {
		                          uwDecision.append( MetlifeInstitutionalConstants.ACCEPT);   
		                 
		                    }
		            	/*if(validCvrList!=null && validCvrList.size()>0){*/
		            	  if(validCvrList!=null && !(validCvrList.isEmpty())){
		            		for (int covItr = 0; covItr < validCvrList.size(); covItr++) {
			                	coversDTO =  validCvrList.get( covItr);                   	

			                    if ("DCL".equalsIgnoreCase(applicationDTO.getAppdecision()) && (null != coversDTO.getCoverreason()
		                                && coversDTO.getCoverreason().length() > 0)) {	                                                                              
			                    				uwritingDecisionBlurb.append("\t"); 
			                                	if("DEATH".equalsIgnoreCase(coversDTO.getCovercode())){
						                    		uwritingDecisionBlurb.append("Death cover - ");
						                  		}else if("TPD".equalsIgnoreCase(coversDTO.getCovercode())){
						                  			uwritingDecisionBlurb.append("Total & permanent disablement cover - ");
						                  		}else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
						                  				uwritingDecisionBlurb.append(MetlifeInstitutionalConstants.INCOME_PROTECTION_COVER);
						                  			
						                  		}		                                			                               
			                                	
			                                	if(null!= coversDTO.getCoverreason() &&  coversDTO.getCoverreason().length() >0 /*&& !uwritingDecisionBlurb.toString().contains( coversDTO.getCoverReason())*/){
				                                	
				                                	if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains("##") ){			
				                                		String declinereasons = coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().lastIndexOf("##"));
				                                		declinereasons = declinereasons.replaceAll("##", " & ");
				                                		uwritingDecisionBlurb.append(declinereasons);			                                		
				                                		log.info("uwritingDecisionBlurb>> {}",declinereasons);
				                                	}else{
				                                		uwritingDecisionBlurb.append(coversDTO.getCoverreason());
				                                	}
				                                	
				                                }
			                                	//Commented as per Sonar fixes. The else block is unreachable
			                                	/*else if(null!= coversDTO.getCoverreason() &&  coversDTO.getCoverreason().length() >0 && !uwritingDecisionBlurb.toString().contains( coversDTO.getCoverReason())){
				                                	
				                                	if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains(",") ){	
				                                		uwritingDecisionBlurb.append(coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().lastIndexOf(',')));
				                                	}else{
				                                		uwritingDecisionBlurb.append(coversDTO.getCoverreason());
				                                	}
				                                	
				                                }else if(null!= coversDTO.getCoverreason() &&  coversDTO.getCoverreason().length() >0 && !uwritingDecisionBlurb.toString().contains( coversDTO.getCoverOrgDclReason())){
				                                	uwritingDecisionBlurb.append( "\n");                         	
				                                	if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains(",") ){	
				                                		uwritingDecisionBlurb.append(coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().lastIndexOf(',')));
				                                	}else{
				                                		uwritingDecisionBlurb.append(coversDTO.getCoverreason());
				                                	}
				                                	
				                            }*/
			                                /*if(htmlEmails!=null && htmlEmails.equalsIgnoreCase("TRUE")){
			                                	uwritingDecisionBlurb.append(MetlifeInstitutionalConstants.BREAK_LINE);  
			                                	SRKR: 29/09/2014: For HTML emails add br for new line
			                                }else{
			                                	uwritingDecisionBlurb.append("\n\n");  
			                                }*/	
		                                	
			                                	uwritingDecisionBlurb.append(MetlifeInstitutionalConstants.BREAK_LINE);
			                        
			                    }
			                    else if ("ACC".equalsIgnoreCase( applicationDTO.getAppdecision())
			                            && mixedDecision) {
			                    	 
			                    		acceptedCover = EapplyHelper.getDCLProductName(applicationDTO, applicantDTO.getCovers(), "ACC", false);                   
				                        declineCover = EapplyHelper.getDCLProductName(applicationDTO, applicantDTO.getCovers(),"DCL",false);			                    	
				                         declineReason = EapplyHelper.getDCLProductName(applicationDTO, applicantDTO.getCovers(),"DCL",true);
				                         
				                         uwDecision.append( MetlifeInstitutionalConstants.ACCEPT+acceptedCover);   
			                        break;
			                    }
			                    uwDecision.append(coversDTO.getCovertype());
			                    if(covItr == applicantDTO.getCovers().size()-2){
			                    	uwDecision.append(MetlifeInstitutionalConstants.AND);
			                    }else if(covItr != applicantDTO.getCovers().size()-1){
			                    	uwDecision.append(", ");
			                    }		                    
			                    
			                }
		            	}
		                
		                if("ACC".equalsIgnoreCase( applicationDTO.getAppdecision())
		                		&& ("true".equalsIgnoreCase(applicationDTO.getPaindicator() )|| pdfObject.isExclusionInd())){
		                	isSpecialTerm = Boolean.TRUE;                        	
                        }
		            }
		        }
       }
            
         
            eVO.setApplicationId( pdfObject.getApplicationNumber());            
            
             /*Email Parameters */
        
	        eVO.setFromSender(properties.getProperty("fromsenderEmail"));      
	        eVO.setApplicationtype(lob);        
	        placeHolders.put( "EMAIL_SMTP_HOST", properties.getProperty("email_host"));
	        placeHolders.put( "EMAIL_SMTP_PORT", properties.getProperty("email_port"));
	        /*placeHolders.put( MetlifeInstitutionalConstants.EMAIL_SMTP_USERID, applicationDTO.getEmailSmtpUserId());
	        placeHolders.put( MetlifeInstitutionalConstants.EMAIL_SMTP_PASS, applicationDTO.getEmailSmtpPass());*/
	        
	        mailList.add( emailID);
	     
	        if ("RUW".equalsIgnoreCase( overAllDecision)) {
	            eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_RUW));
	        } else if ("ACC".equalsIgnoreCase(overAllDecision)) {
                eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_ACC));
            } else if ("DCL".equalsIgnoreCase(overAllDecision)) {
            	/*SRKR:15/10/2014 life events and transfer events decline flow is applicable only for VICS
            	 * For HOST REIS it wont come here.
            	 * */
            	if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
            		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_UWCOVER_DCL));
            	}else if(MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
            		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_ICOVER_DCL));
            	}else if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
            		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_TCOVER_DCL));
            	}else if(MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
            		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_SCOVER_DCL));
            	}else if(MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
            		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_CCOVER_DCL));
            	}else{
            		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_DCL));
            	}
                
            }else{
            	 eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_ACC));
            }
	       
	     	        
	        String subject = null;

        	
      	  if ("RUW".equalsIgnoreCase( overAllDecision)) {
	        	  if(uwDecision.toString().contains(MetlifeInstitutionalConstants.REFERRED_TO_UNDERWRITING)){
	        		  uwDecision = uwDecision.replace(uwDecision.indexOf(MetlifeInstitutionalConstants.REFERRED_TO_UNDERWRITING), uwDecision.indexOf(MetlifeInstitutionalConstants.REFERRED_TO_UNDERWRITING)+25, "");  
	        	  }     		  
      		  
      		  	if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_RUW).contains(MetlifeInstitutionalConstants.ACCEPT_COVER) && null!=acceptedCover){
	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_RUW).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover));	        		 
	        	}else{
	        		subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_RUW);
	        	}
      		  
	          }else if ("DCL".equalsIgnoreCase(overAllDecision)) {
	        	
	        	if(uwDecision.toString().contains(MetlifeInstitutionalConstants.DECLINE)){
	        		  uwDecision = uwDecision.replace(uwDecision.indexOf(MetlifeInstitutionalConstants.DECLINE), uwDecision.indexOf(MetlifeInstitutionalConstants.DECLINE)+8, "");  
	        	}
	        	if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	        		subject=properties.getProperty("email_brk_subj_uwcover_dcl");
	        	}else if(MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	        		subject=properties.getProperty("email_brk_subj_icover_dcl");
	          	}else if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	          		subject=properties.getProperty("email_brk_subj_tcover_dcl");
	          	}else if(MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	          		subject=properties.getProperty("email_brk_subj_scover_dcl");
	          	}else if(MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	          		subject=properties.getProperty("email_brk_subj_ccover_dcl");
	          	}else{
	          		/*subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_DCL);*/
	          		if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_DCL).contains(MetlifeInstitutionalConstants.DECLINE_COVER) && null!=declineCover){
	        			 subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_DCL).replace(MetlifeInstitutionalConstants.DECLINE_COVER, declineCover);       		 
	 	        	}else{
	 	        		subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_DCL).replace(MetlifeInstitutionalConstants.DECLINE_COVER, "");
	 	        	}
	          	}
	        	
            }else if ("ACC".equalsIgnoreCase(overAllDecision) && (pdfObject.isExclusionInd() || pdfObject.isLoadingInd())) {	            	  
          	  if(uwDecision.toString().contains(MetlifeInstitutionalConstants.MIXED_ACCEPTANCE_AND_DECLINE)){
          		  uwDecision = uwDecision.replace(uwDecision.indexOf(MetlifeInstitutionalConstants.MIXED_ACCEPTANCE_AND_DECLINE), uwDecision.indexOf(MetlifeInstitutionalConstants.MIXED_ACCEPTANCE_AND_DECLINE)+29, MetlifeInstitutionalConstants.ACCEPT_); 
          	  }
          	  if(mixedDecision){
          		  subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_MXD_ACC);          		
          		  if(null!=subject && subject.contains(MetlifeInstitutionalConstants.SPECIAL_CONDITION)){
          			  subject=subject.replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION,MetlifeInstitutionalConstants.SPECIALTERMMSG);
          		  }                		  
          		log.info("subject special>> {}",subject);
          	  }else if(pdfObject.isExclusionInd() && pdfObject.isLoadingInd()){          		 
          		subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_LOAD_EXL_ACC);
          	  }else if(pdfObject.isExclusionInd()){
          		  subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_EXL_ACC);
          	  }else if(pdfObject.isLoadingInd()){
          		subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_LOAD_ACC);
          	  }
            }else if ("ACC".equalsIgnoreCase(overAllDecision) ) {
          	  if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
          		subject = properties.getProperty("email_brk_subj_uwcover_acc"); 
	              }else if(MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	            	  subject = properties.getProperty("email_brk_subj_icover_acc"); 
	              }else if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	            	  subject = properties.getProperty("email_brk_subj_tcover_acc"); 
	              }else if(MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	            	  subject = properties.getProperty("email_brk_subj_scover_acc"); 
	              }else if(MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && mixedDecision){
	            	  subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_MXD_ACC).replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, "");
	              }else{
	            	  if(uwDecision.toString().contains(MetlifeInstitutionalConstants.MIXED_ACCEPTANCE_AND_DECLINE)){
	            		  uwDecision = uwDecision.replace(uwDecision.indexOf(MetlifeInstitutionalConstants.MIXED_ACCEPTANCE_AND_DECLINE), uwDecision.indexOf(MetlifeInstitutionalConstants.MIXED_ACCEPTANCE_AND_DECLINE)+29, MetlifeInstitutionalConstants.ACCEPT_);
	            		  subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_LOAD_ACC);
	            	  }else{
	            			if(applicationDTO.getPartnercode()!=null && "AEIS".equalsIgnoreCase(applicationDTO.getPartnercode())) {
	            				String uwdec=uwDecision.toString();
	            				String removeAccept=uwdec.replaceAll("", "");
		        				 String tpddecision=removeAccept.replaceAll(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT, MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
	            				String newuwDecision=tpddecision.replaceAll(MetlifeInstitutionalConstants.SALARY_CONTINUANCE, MetlifeInstitutionalConstants.INCOME_PROTECTION);
	            				log.info("UW DECISION : {}",newuwDecision);
		  	        			subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, newuwDecision);	      
		  	        		}
	            			else {
	            		  subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_ACC);
	            			}
	            	  }
	            	 
	              }
            }else{
          	  subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_ACC);
            }
          
      if(partnerCode != null && "CORP".equalsIgnoreCase(partnerCode)) {
    	  isChangeNo= Boolean.FALSE;
      }
      else {
    	  isChangeNo = EapplyHelper.isNoChange(applicationDTO);
      }
	        if(MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && isChangeNo){
	        	subject = properties.getProperty("client_email_subj_nochange"); 
	        	 if(subject!=null && subject.contains(MetlifeInstitutionalConstants.COVER)){
	        		 subject = subject.replace(MetlifeInstitutionalConstants.COVER, EapplyHelper.getDCLProductName(applicationDTO, applicantDTO.getCovers(), "ACC", false));	        		 
	        	 }
	        } 
	        /*if(MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
		        if(subject!=null && subject.contains(MetlifeInstitutionalConstants.COVER)){
	       		 	subject = subject.replace(MetlifeInstitutionalConstants.COVER, EapplyHelper.getDCLProductName(applicantDTO.getCovers(), "ACC", false));	        		 
	       	 	}
	        }*/
	        
	        placeHolders.put( "TITLE", eVO.getTitle());
	        //THis code needs to be removed after vicsuper UAT
	        if (!(pdfObject.getMemberDetails().getClientRefNum().contains("MET") || pdfObject.getMemberDetails().getClientRefNum().contains("met"))
  	        		&& MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode())){
	        	placeHolders.put( "TESTEMAIL",emailID );
	        } else {
	        	placeHolders.put( "TESTEMAIL", "");
	        }
	        placeHolders.put( "FIRSTNAME", eVO.getFirstName());
	        placeHolders.put( "LASTNAME", eVO.getLastName());
	        if(applicationDTO.getPartnercode()!=null && ("HOST".equalsIgnoreCase(applicationDTO.getPartnercode()) || "FIRS".equalsIgnoreCase(applicationDTO.getPartnercode()) || "SFPS".equalsIgnoreCase(applicationDTO.getPartnercode())) && MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
      			pdfAttachDir=null;
      		} 
	      //Added for AEIS cancel cover scenario no attachment required.
	        if(applicationDTO.getPartnercode()!=null && ("AEIS".equalsIgnoreCase(applicationDTO.getPartnercode())) && (MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()))){
	        	pdfAttachDir=null;
	        }
	        
	        //Added for vicsuper - no attachements for UWcover By Purna
	        if((MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode())) && (MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()))){
	        	pdfAttachDir=null;
	        }
	        
	        placeHolders.put( "PDFLINK", pdfAttachDir);
	        placeHolders.put( "EMAIL_IND_SUBJECT_NAME", applicantName);
	        placeHolders.put(MetlifeInstitutionalConstants.EMAIL_SUBJECT, subject);
	        /*SRKR; Adde new variable member_no to show in emails, usefull for GUIL and ACCS*/
	        if(pdfObject!=null && pdfObject.getMemberDetails()!=null && pdfObject.getMemberDetails().getClientRefNum()!=null){
	        	placeHolders.put( "MEMBER_NO", pdfObject.getMemberDetails().getClientRefNum());
	        }
	        
	        if(MetlifeInstitutionalConstants.INSTITUTIONAL.equalsIgnoreCase(lob)){
	        	/*eVO.setCcReciepent();   */     	
	        	
	        	placeHolders.put(MetlifeInstitutionalConstants.UW_DECISION, uwDecision);
        		if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){        			
        			eVO.setEmailTemplateId(properties.getProperty("email_body_uwcover_acc"));	        			
	            }else if(MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	            	 eVO.setEmailTemplateId(properties.getProperty("email_body_icover_acc"));
	            }else if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	            	 eVO.setEmailTemplateId(properties.getProperty("email_body_tcover_acc"));
	            }else if(MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	            	 eVO.setEmailTemplateId(properties.getProperty("email_body_scover_acc"));
	            }else if(pdfObject.isLoadingInd() && pdfObject.isExclusionInd()){
        			eVO.setEmailTemplateId(properties.getProperty("email_body_load_excl_acc"));
        		}else if(pdfObject.isLoadingInd()){
        			eVO.setEmailTemplateId(properties.getProperty("email_body_load_acc"));
        		}else if(pdfObject.isExclusionInd()){
        			eVO.setEmailTemplateId(properties.getProperty("email_body_excl_acc"));
        		}     		        	
	        	placeHolders.put("UW_DECISION_BLURB", "");	
	        	/*SRKR: 25/11/2013 Added !RUW because in case of mix acceptance and client match
	        	 * mixed accpetance flag is true and overall decision is RUW so add one more condition to check
	        	 * if it is not RUW than check for Mixed acceptance*/
	 	        if(!"RUW".equalsIgnoreCase(overAllDecision) && mixedDecision && !"CORP".equalsIgnoreCase(applicationDTO.getPartnercode()) ){
	 	        	if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.ACCEPT_COVER) && null!=acceptedCover){
	 	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover));
	 	        		 
	 	        	}
	 	        	if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.DECLINE_COVER) && null!=declineCover){
	 	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.DECLINE_COVER, declineCover));
	 	        	}
	 	        	if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.DECLINE_REASONS) && null!=declineReason){
	 	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.DECLINE_REASONS, declineReason));
	 	        	}	
	 	        	
	 	        	if(isSpecialTerm){
	 	        		if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.SPECIAL_CONDITION)){
		 	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, MetlifeInstitutionalConstants.SPECIALTERMMSG));
		 	        	}
	 	        	}else{
	 	        		/*if(applicationDTO.getLoadingIndicator()  && eVO.getEmailTemplateId().contains(MetlifeInstitutionalConstants.SPECIAL_CONDITION) ){
		        			eVO.setEmailTemplateId(eVO.getEmailTemplateId().replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION,"with loadings"));
		        		}else*/ if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.SPECIAL_CONDITION)){
		 	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, ""));
		 	        	}
	 	        	}        	
	 	        	
	 	        }else if(!"RUW".equalsIgnoreCase(overAllDecision) && mixedDecision && "CORP".equalsIgnoreCase(applicationDTO.getPartnercode()) ){
	 	        	if(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.ACCEPT_COVER) && null!=acceptedCover && null!=declineCover && null!=declineReason){
	 	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover)
	 	        				.replace(MetlifeInstitutionalConstants.DECLINE_COVER, declineCover)
	 	        				.replace(MetlifeInstitutionalConstants.DECLINE_REASONS, declineReason));
	 	        		 
	 	        	}
	 	        }
	 	        
	 	        
	 	        
	 	        else if ("DCL".equalsIgnoreCase(overAllDecision)) {
	            	/*SRKR:15/10/2014 life events and transfer events decline flow is applicable only for VICS
	            	 * For HOST REIS it wont come here.
	            	 * */
	 	        	log.info("asdasd {}",properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_DCL));
	 	        	if(applicationDTO.getRequesttype()!=null && MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) ){ 	        		
	 	        		
	 	        		if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_UWCOVER_DCL).contains(MetlifeInstitutionalConstants.DECLINE_REASONS)){
	 	        			eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_UWCOVER_DCL).replace(MetlifeInstitutionalConstants.DECLINE_REASONS, uwritingDecisionBlurb.toString()));
		 	        	}
		 	        	
	            	}else if(applicationDTO.getRequesttype()!=null && MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) ){ 	        		
	 	        		
	 	        		if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_ICOVER_DCL).contains(MetlifeInstitutionalConstants.DECLINE_REASONS)){
	 	        			eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_ICOVER_DCL).replace(MetlifeInstitutionalConstants.DECLINE_REASONS, uwritingDecisionBlurb.toString()));
		 	        	}
		 	        	
	            	}else if(applicationDTO.getRequesttype()!=null && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
		 	        	if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_TCOVER_DCL).contains(MetlifeInstitutionalConstants.DECLINE_REASONS)){
		 	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_TCOVER_DCL).replace(MetlifeInstitutionalConstants.DECLINE_REASONS, uwritingDecisionBlurb.toString()));
		 	        	}
		 	        	
		        	}else if(applicationDTO.getRequesttype()!=null && MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
		 	        	if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_SCOVER_DCL).contains(MetlifeInstitutionalConstants.DECLINE_REASONS)){
		 	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_SCOVER_DCL).replace(MetlifeInstitutionalConstants.DECLINE_REASONS, uwritingDecisionBlurb.toString()));
		 	        	}		 	        	
		        	}else if(applicationDTO.getRequesttype()!=null && MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
		        		if (MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode())){ // Added for vicsuper for decline scenario - Purna
			 	        	eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_CCOVER_DCL));// Added for vicsuper for decline scenario - Purna
			 	        	
		        		} 
		        	}else{
		 	        	if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_DCL).contains(MetlifeInstitutionalConstants.DECLINE_REASONS)){
		 	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_DCL).replace(MetlifeInstitutionalConstants.DECLINE_REASONS, uwritingDecisionBlurb.toString()));
		 	        	}
		 	        	
	            	}	 	        	
	 	        	
	 	        }else if("RUW".equalsIgnoreCase(overAllDecision)){
	 	        	eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_RUW));
	 	        }
	 	        
	        }
	        

	        placeHolders.put( MetlifeInstitutionalConstants.APPLICATION_NUMBER, applicationDTO.getApplicationumber());	
	        

	        eVO.setEmailDataElementMap( placeHolders);     
	        //Code has been added for visuper UAT
	        if (!(pdfObject.getMemberDetails().getClientRefNum().contains("MET") || pdfObject.getMemberDetails().getClientRefNum().contains("met"))
	        		&& MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode())){
	        	
	        	String emailservice = properties.getProperty("emailservice");
	        	if (emailservice.contains("e2e")) {
	        		mailList = new ArrayList();
	        		mailList.add(properties.getProperty("vicsuper_test_email"));
	        	}
	        	
	        }
	        eVO.setToRecipents( mailList); 	     
	        /*String htmlEmails= null;*/
	        /*SRKR This flag determine whether to send html emails or not*/
	        eVO.setHtmlEmails(true);
	        /*SRKR: Added documments for APSD to fwd to fund administrator*/
	        /*if(applicationDTO.getDocuments()!=null){
	        	eVO.setDocumentList(applicationDTO.getDocuments());
	        }*/
	        if(MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && isChangeNo ){
	        	eVO.setEmailTemplateId(properties.getProperty("client_email_body_nochange"));
	        }
	        
	       /* bcc recipent*/
	       /*if((MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype())|| MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())|| MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())) && null!=applicationDTO.getBccReciepent() && applicationDTO.getBccReciepent().length()>0){
	        	 eVO.setBccReciepent(applicationDTO.getBccReciepent());
	        }else if(MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(applicationDTO.getRequestType()) && isChangeNo && null!=applicationDTO.getBccReciepent() && applicationDTO.getBccReciepent().length()>0){
	        	eVO.setEmailTemplateId(applicationDTO.getEmailBodyNoChange());
	        	eVO.setBccReciepent(applicationDTO.getBccReciepent());
	        	 placeHolders.put( MetlifeInstitutionalConstants.EMAIL_SUBJECT, subject);
	        }else if(null!=applicationDTO.getBccReciepent() && applicationDTO.getBccReciepent().length()>0){
	        	eVO.setBccReciepent(applicationDTO.getBccReciepent());
	        } */ 
	        
	       /*eVO.setDocumentList(null);*/ /*SRKR: Reset the documents so that it wont send for client.*/
	        
	        /**
	         * In institutional when the client email is present send the email to the client, 
	         */
	    	/*REI in the case of client email */
        	if(MetlifeInstitutionalConstants.INSTITUTIONAL.equalsIgnoreCase(lob)        						
        			&& 	null!=pdfObject.getMemberDetails().getClientEmailId() && pdfObject.getMemberDetails().getClientEmailId().length()>0){
        		mailList.clear();
        		/*SRKR:04/05/2015: Stopping client email for mixed acceptance: GEAU*/
        		if("ACC".equalsIgnoreCase(applicationDTO.getAppdecision())){
        			mailList.add(pdfObject.getMemberDetails().getClientEmailId()); 
        			if( mixedDecision){         				
        				if(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.SPECIAL_CONDITION) && pdfObject.isExclusionInd() && pdfObject.isLoadingInd()){
        					eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, MetlifeInstitutionalConstants.SPECIALTERMMSG));
        				}else if(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.SPECIAL_CONDITION) && !pdfObject.isExclusionInd() && pdfObject.isLoadingInd()){
        					eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, MetlifeInstitutionalConstants.SPECIALTERMMSG));
        				}else if(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.SPECIAL_CONDITION) && pdfObject.isExclusionInd() && !pdfObject.isLoadingInd()){
        					eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, MetlifeInstitutionalConstants.SPECIALTERMMSG));
        				}else if(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.SPECIAL_CONDITION)){
        					eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, MetlifeInstitutionalConstants.SPECIALTERMMSG));
        				}else{
        					eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD));
        				}
        				if(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.ACCEPT_COVER) && null!=acceptedCover){
         	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover));         	        		 
         	        	}
         	        	if(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.DECLINE_COVER) && null!=declineCover){
         	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.DECLINE_COVER, declineCover));
         	        		
         	        	}         				
        				
        				placeHolders.put(MetlifeInstitutionalConstants.UW_DECISION, "Acknowledgement");	
        				if(!"RUW".equalsIgnoreCase(overAllDecision) && mixedDecision && "CORP".equalsIgnoreCase(applicationDTO.getPartnercode()) ){
        	 	        	if(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.ACCEPT_COVER) && null!=acceptedCover && null!=declineCover && null!=declineReason){
        	 	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover)
        	 	        				.replace(MetlifeInstitutionalConstants.DECLINE_COVER, declineCover)
        	 	        				.replace(MetlifeInstitutionalConstants.DECLINE_REASONS, declineReason));
        	 	        		 
        	 	        	}
        				}
        				
        			}else{
    
        					if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
        						 eVO.setEmailTemplateId(properties.getProperty("client_email_body_uwcover_acc") );        						 
        						 subject=  	properties.getProperty("client_email_subj_uwcover_acc"); 						 
        					}else if(MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){        						
        						 eVO.setEmailTemplateId(properties.getProperty("client_email_body_icover_acc") );        						 
        						 subject=  	properties.getProperty("client_email_subj_icover_acc"); 
        					}else if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){       						 	
       						 	eVO.setEmailTemplateId(properties.getProperty("client_email_body_tcover_acc") );        						 
       						 	subject=  	properties.getProperty("client_email_subj_tcover_acc"); 
        					}else if(MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){       						 
        						eVO.setEmailTemplateId(properties.getProperty("client_email_body_scover_acc") );        						 
        						subject=  	properties.getProperty("client_email_subj_scover_acc"); 
        					}else if(pdfObject!=null && pdfObject.isExclusionInd() && pdfObject.isLoadingInd()){
        						eVO.setEmailTemplateId(properties.getProperty("client_email_body_load_exl"));  
        						subject=  	properties.getProperty("client_email_subj_load_exl"); 
        					}else if(pdfObject!=null && !pdfObject.isExclusionInd() && pdfObject.isLoadingInd()){
        						eVO.setEmailTemplateId(properties.getProperty("client_email_body_load"));
        						subject=  	properties.getProperty("client_email_subj_load"); 
        					}else if(pdfObject!=null && pdfObject.isExclusionInd() && !pdfObject.isLoadingInd()){
        						eVO.setEmailTemplateId(properties.getProperty("client_email_body_exl"));
        						subject=  	properties.getProperty("client_email_subj_exl"); 
        					}else if(applicationDTO!=null && mixedDecision && pdfObject!=null){
        						eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD));  
        						if(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.ACCEPT_COVER) && null!=acceptedCover){
                 	        		subject= properties.getProperty("client_email_subj_mxd").replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover);         	        		 
                 	        	}else{
        							subject= properties.getProperty("client_email_subj_mxd");         							
        						}        						
        					}else if(!MetlifeInstitutionalConstants.NCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && pdfObject!=null && !pdfObject.isExclusionInd() && !pdfObject.isLoadingInd()){
        						eVO.setEmailTemplateId(properties.getProperty("client_email_body_acc"));
        						subject=  	properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_SUBJ_ACC); 
        					}else if(MetlifeInstitutionalConstants.NCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && "ACC".equalsIgnoreCase(applicationDTO.getAppdecision())){        						
       						 	eVO.setEmailTemplateId(properties.getProperty("client_email_body_newMembr_eligible") );        						 
       						 	subject=  	properties.getProperty("client_email_subj_newMembr_eligible"); 
        					}

        					/*}else{
            				if(properties.getProperty("client_email_body_acc").contains(MetlifeInstitutionalConstants.SPECIAL_CONDITION) && pdfObject.isExclusionInd() && pdfObject.isLoadingInd()){
            					eVO.setEmailTemplateId(properties.getProperty("client_email_body_acc").replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, " Your cover has been accepted with an exclusion(s) and loading(s)  as advised in the Cover Summary."));
            				}else if(properties.getProperty("client_email_body_acc").contains(MetlifeInstitutionalConstants.SPECIAL_CONDITION) && !pdfObject.isExclusionInd() && pdfObject.isLoadingInd()){
            					eVO.setEmailTemplateId(properties.getProperty("client_email_body_acc").replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, " Your cover has been accepted with loading(s) as advised in the Cover Summary."));
            				}else if(properties.getProperty("client_email_body_acc").contains(MetlifeInstitutionalConstants.SPECIAL_CONDITION) && pdfObject.isExclusionInd() && !pdfObject.isLoadingInd()){
            					eVO.setEmailTemplateId(properties.getProperty("client_email_body_acc").replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, " Your cover has been accepted with an exclusion(s) as advised in the Cover Summary."));
            				}else if(properties.getProperty("client_email_body_acc").contains(MetlifeInstitutionalConstants.SPECIAL_CONDITION)){
            					eVO.setEmailTemplateId(properties.getProperty("client_email_body_acc").replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, ""));
            				}        				
        				}*/
        				placeHolders.put(MetlifeInstitutionalConstants.UW_DECISION, uwDecision);	
        			}  	        	
        			
        		
        		}else if("RUW".equalsIgnoreCase(applicationDTO.getAppdecision())){
        			mailList.add(pdfObject.getMemberDetails().getClientEmailId());         			
        			eVO.setEmailTemplateId(properties.getProperty("client_email_body_ruw"));
        			placeHolders.put(MetlifeInstitutionalConstants.UW_DECISION, uwDecision);	
        		}else if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && null==applicationDTO.getAppdecision()){
        			/*Added code for Update Work rating decision - 169682*/
        			mailList.add(pdfObject.getMemberDetails().getClientEmailId());         			
        			eVO.setEmailTemplateId(properties.getProperty("client_email_body_uwcover_wrkratingmaint") );
				 	subject=  	properties.getProperty("client_email_subj_uwcover_wrkratingmaint"); 
        			placeHolders.put(MetlifeInstitutionalConstants.UW_DECISION, uwDecision);	
        		}else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && null==applicationDTO.getAppdecision()){
        			/*Added code for Update Work rating decision - 169682*/
        			mailList.add(pdfObject.getMemberDetails().getClientEmailId());         			
        			eVO.setEmailTemplateId(properties.getProperty("client_email_body_cancel_acc") );
				 	subject=  	properties.getProperty("client_email_subj_cancel_acc"); 
        			placeHolders.put(MetlifeInstitutionalConstants.UW_DECISION, uwDecision);	
        		}else if("DCL".equalsIgnoreCase(applicationDTO.getAppdecision())){
        			mailList.add(pdfObject.getMemberDetails().getClientEmailId());         			
        			/*eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_DCL));*/        				
        			/*Added for decline reason's inside Client Email */        			
        			/* Added to implement decline for New member*/
        			if(MetlifeInstitutionalConstants.NCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && "DCL".equalsIgnoreCase(applicationDTO.getAppdecision())){  
        				eVO.setEmailTemplateId(properties.getProperty("client_email_body_newMembr_noteligible")); 					 	      						 
					 	subject=  	properties.getProperty("client_email_subj_newMembr_noteligible"); 
					}else if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && "DCL".equalsIgnoreCase(applicationDTO.getAppdecision())){
						eVO.setEmailTemplateId(properties.getProperty("client_email_body_tcover_dcl") );
					 	subject=  	properties.getProperty("client_email_subj_tcover_dcl"); 
					}else if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && "DCL".equalsIgnoreCase(applicationDTO.getAppdecision())){
						eVO.setEmailTemplateId(properties.getProperty("client_email_body_uwcover_dcl") );        						 
					 	subject=  	properties.getProperty("client_email_subj_uwcover_dcl"); 
					}else if(MetlifeInstitutionalConstants.FUND_HOST.equalsIgnoreCase(applicationDTO.getPartnercode()) && MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && MetlifeInstitutionalConstants.DCL.equalsIgnoreCase(applicationDTO.getAppdecision())){
						eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_ICOVER_DCL) );        						 
					 	subject=  	properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_SUBJ_ICOVER_DCL); 
					}else {
						if(!MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode()) && properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_DCL).contains(MetlifeInstitutionalConstants.DECLINE_REASONS) && null!=uwritingDecisionBlurb){
	     	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_DCL).replace(MetlifeInstitutionalConstants.DECLINE_REASONS, uwritingDecisionBlurb.toString()));     	        		
	     	        	}else if (MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode())
	     	        			&& MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && "DCL".equalsIgnoreCase(applicationDTO.getAppdecision())){ // Added for vicsuper for decline scenario - Purna
			 	        	eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_DCL));// Added for vicsuper for decline scenario - Purna
			 	        	
		        		} 
					}
        			placeHolders.put(MetlifeInstitutionalConstants.UW_DECISION, uwDecision);
        			
        		}
        		if(MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && isChangeNo){          			
        			mailList.add(pdfObject.getMemberDetails().getClientEmailId());   
        			eVO.setEmailTemplateId(properties.getProperty("client_email_body_nochange"));   
	    	        subject = properties.getProperty("client_email_subj_nochange"); 
	    	    }
        		/*if(null!=mailList && mailList.size()>0){*/
        		if(null!=mailList && !(mailList.isEmpty())){
        			/*For COrp Eapply Diff. email subj for Broker*/        			
    	        	  if ("RUW".equalsIgnoreCase( overAllDecision) && properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_SUBJ_RUW)!=null) {
    	        			 if("AEIS".equalsIgnoreCase(applicationDTO.getPartnercode())) {
    	        				 String uwdec=uwDecision.toString();
    	        				 String tpddecision=uwdec.replaceAll(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT, MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
    	          				String newuwDecision=tpddecision.replaceAll(MetlifeInstitutionalConstants.SALARY_CONTINUANCE, MetlifeInstitutionalConstants.INCOME_PROTECTION);
    	          				log.info("UW DECISION :  {}",newuwDecision);
    	          				subject=properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_SUBJ_RUW).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, newuwDecision);	
    	        			 }
    	        			 else {
    	        				  subject=properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_SUBJ_RUW);
    	        			 }
    	        
    	  	          }else if (!(MetlifeInstitutionalConstants.NCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) || MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) ||MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) ) && "DCL".equalsIgnoreCase(overAllDecision) && properties.getProperty("client_email_subj_dcl")!=null) {
    	  	        	 if (MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode())){ // Added for vicsuper fo decline scenario - Purna
    	  	        		 subject=properties.getProperty("email_brk_subj_ccover_dcl");
    	  	        	 }else if (MetlifeInstitutionalConstants.FUND_HOST.equalsIgnoreCase(applicationDTO.getPartnercode()) && MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){ // Added for vicsuper fo decline scenario - Purna
    	  	        		 subject=properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_SUBJ_ICOVER_DCL);
    	  	        	 }  else {
    	  	        		subject=properties.getProperty("client_email_subj_dcl"); 
    	  	        	 }
    	  	        	
    	              }
    	        	//Added for Cancel cover client mail acknowledged Hostplus
    	  	          else if("ACC".equalsIgnoreCase(overAllDecision) && properties.getProperty("client_email_subj_cancel_ack")!=null && properties.getProperty("client_email_body_cancel_ack")!=null && MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()))
    	  	        	{
    	  	        	  subject=properties.getProperty("client_email_subj_cancel_ack");
    	  	        	  eVO.setEmailTemplateId(properties.getProperty("client_email_body_cancel_ack") );
    	  	        	}
    	  	          else if ("ACC".equalsIgnoreCase(overAllDecision) && properties.getProperty("client_email_body_acc_spcl")!=null && !mixedDecision && (pdfObject.isExclusionInd() || pdfObject.isLoadingInd())) {
          				/*SRKR: From 17/10/2013 corp eapply onboarding created separate place holders for all possible combinations*/    	            	  
    	  	        	//Changes added for R2 Hostplus
    	            	  if(applicationDTO.getPartnercode()!=null && !("AEIS".equalsIgnoreCase(applicationDTO.getPartnercode()) || "HOST".equalsIgnoreCase(applicationDTO.getPartnercode()) || "FIRS".equalsIgnoreCase(applicationDTO.getPartnercode()) || "SFPS".equalsIgnoreCase(applicationDTO.getPartnercode()) || "CORP".equalsIgnoreCase(applicationDTO.getPartnercode())))
    	            	  {
    	            	  subject=properties.getProperty("client_email_subj_spl");
    	            	  eVO.setEmailTemplateId(properties.getProperty("client_email_body_acc_spcl") );
    	            	  }
    	            	  
    	              }else if ("ACC".equalsIgnoreCase(overAllDecision) && properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_SUBJ_ACC)!=null && !mixedDecision && !MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && !MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && !MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())&& !MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && !MetlifeInstitutionalConstants.NCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())) {    	            	  
    	            	  if(applicationDTO.getPartnercode()!=null && "AEIS".equalsIgnoreCase(applicationDTO.getPartnercode())) {
    	            		  String uwdec=uwDecision.toString();
    	            		  String tpddecision=uwdec.replaceAll(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT, MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
	            				String newuwDecision=tpddecision.replaceAll(MetlifeInstitutionalConstants.SALARY_CONTINUANCE, MetlifeInstitutionalConstants.INCOME_PROTECTION);
	            				log.info("UW DECISION - {}",newuwDecision);
		  	        			subject=properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_SUBJ_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, newuwDecision);	   
		  	        		}
    	            	  else {
    	            		  subject=properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_SUBJ_ACC); 
    	            	  }
    	            
    	              }else if ("ACC".equalsIgnoreCase(overAllDecision) && properties.getProperty("client_email_subj_mxd_acc")!=null && mixedDecision && !MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && !MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && !MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())&& !MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && !MetlifeInstitutionalConstants.NCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())) {    	            	  
    	            	  subject=properties.getProperty("client_email_subj_mxd_acc");
    	              }
    	        	  placeHolders.put(MetlifeInstitutionalConstants.EMAIL_SUBJECT, subject);
    	        	  placeHolders.put( MetlifeInstitutionalConstants.APPLICATION_NUMBER, applicationDTO.getApplicationumber());	
    	        	//Code has been added for visuper UAT
    	        	  if (!(pdfObject.getMemberDetails().getClientRefNum().contains("MET") || pdfObject.getMemberDetails().getClientRefNum().contains("met"))
    	  	        		&& MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode())){
    	        		  String emailservice = properties.getProperty("emailservice");
    	  	        	if (emailservice.contains("e2e")) {
    	  	        		mailList = new ArrayList();
    	  	        		mailList.add(properties.getProperty("vicsuper_test_email"));
    	  	        	}
    	  	        }
        			eVO.setToRecipents( mailList); 	  
          		  eVO.setCcReciepent(null);
          		  eVO.setEmailDataElementMap( placeHolders); 
          		  /*System.out.println("inside send email");*/
          		log.info("inside send email");
          		if(corpfund!=null) {
          			brokerContactNum=corpfund.getBrokercontactNum().toString();
          		}
          		if(eVO.getEmailTemplateId().contains("<%BROKERNUMBER%>")) {
          			eVO.setEmailTemplateId(eVO.getEmailTemplateId().replaceAll("<%BROKERNUMBER%>", brokerContactNum));
          		}
          		
          		//Added for Vicsuper fix your cover mail
          		if(MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())
          				&& MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode())
          				&& pdfObject.isConvertCheckPdf() && null!=properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_FIX_YOUR_COVER))
          		{
          			eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_BODY_FIX_YOUR_COVER)); 
          		}
          		
          		if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode())
          				&& (eVO.getEmailTemplateId()).contains(MetlifeInstitutionalConstants.VICSUPER_LOGO))
          		{
          			eVO.setEmailTemplateId((eVO.getEmailTemplateId()).replace(MetlifeInstitutionalConstants.VICSUPER_LOGO, vicSuperLogo));
          		}
          		
          		/*placeHolders.forEach((k,v) -> System.out.println("Key = "+ k + ", Value = " + v));*/
          		placeHolders.forEach((k,v) -> log.info("Key = {} and  Value = {}",k,v));
          		/*For work rating update, don't send attachment to client*/        
          		
          		log.info("client: {}",eVO.getEmailTemplateId());
          		if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
          			pdfAttachDir=null;
          		}
          		 /* MetLifeEmailService.getInstance().sendMail( eVO, pdfAttachDir);*/
          		  //callEmailService(eVO,authId,properties,null,pdfObject.getApplicationNumber());
          		callEmailService(eVO,authId,properties);
        		}
        		 
           }
        	
        	
        	
	            
        }/*catch(SendFailedException emailException){
        	emailException.printStackTrace();
        }*/ 
        catch (Exception e) {
        	e.printStackTrace();
        	log.error("Error in send mail: {}",e.getMessage());
        }
        log.info("Send email finish");
    }
	
	
	
	public void sendFundAdminEmail(PDFObject pdfObject,Eapplication applicationDTO, 
            String overAllDecision, String pdfAttachDir, String lob, Properties properties,String authId,CorporateFund corpfund, Boolean mixedDecision, String partnerCode){
		log.info("Send fund admin email start");
		String fundName = null;
        ArrayList mailList = new ArrayList();
        HashMap placeHolders = new HashMap();     
        String fundEmailID = null; 
        au.com.metlife.eapply.email.MetLifeEmailVO eVO = new au.com.metlife.eapply.email.MetLifeEmailVO();     
        String applicantName = "";
        Applicant applicantDTO = null;
        Cover coversDTO = null;
        StringBuilder uwDecision = new StringBuilder();
        StringBuilder hostRuwCover = new StringBuilder();
        String acceptedCover = null;
        String declineCover = null;
        String declineReason = null;
        StringBuilder uwritingDecisionBlurb = new StringBuilder();
        boolean isSpecialTerm = Boolean.FALSE;
        /*String isCorpEapply=null;*/
        Boolean isChangeNo = null;
        //Boolean mixedDecision = Boolean.FALSE;
        List<Cover> validCvrlist = null;
        String vicSuperLogo = null;
        if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode()))
        {
        vicSuperLogo = properties.getProperty("logo_VICT");
        }
        try {   
//        	if(null!=eapplyInput && null!=eapplyInput.getResponseObject()){
//        		mixedDecision = isMixedDecision(eapplyInput.getResponseObject());
//        	}
        	
            StringBuilder applicantBulder = null;
            if (null!=pdfObject && null != pdfObject.getMemberDetails()) {
                applicantBulder = new StringBuilder( "");
               
                
                /*Added for getting fund admin email from input XML*/
                /*fundEmailID = properties.getProperty("fundAdminstratorEmail");*/
                //fundEmailID = pdfObject.getMemberDetails().getEmailId();  
                fundEmailID = applicationDTO.getApplicant().get(0).getCustomerreferencenumber();
                if (pdfObject.getMemberDetails().getTitle() != null) {
                	  eVO.setTitle( pdfObject.getMemberDetails().getTitle());
                    applicantBulder.append( pdfObject.getMemberDetails().getTitle()
                            + " ");
                }else{
                	eVO.setTitle("");
                }
                if (pdfObject.getMemberDetails().getMemberFName() != null) {
                	 eVO.setFirstName( pdfObject.getMemberDetails().getMemberFName() );
                    applicantBulder.append( pdfObject.getMemberDetails().getMemberFName()
                            + " ");
                }else{
                	eVO.setFirstName("");
                }
                if (pdfObject.getMemberDetails().getMemberLName() != null) {
                	 eVO.setLastName( pdfObject.getMemberDetails().getMemberLName());
                    applicantBulder.append( pdfObject.getMemberDetails().getMemberLName());
                }else{
                	 eVO.setLastName( "");
                }
            }
            if (null != applicantBulder) {
                applicantName = applicantBulder.toString();
            }            
       if(MetlifeInstitutionalConstants.INSTITUTIONAL.equalsIgnoreCase(lob)
    		   && (null != applicationDTO && null != applicationDTO.getApplicant())){
        	   
		        for (int itr = 0; itr < applicationDTO.getApplicant().size(); itr++) {
		        	applicantDTO = applicationDTO.getApplicant().get( itr); 
		        	validCvrlist = coversSelected(applicantDTO);
		            if (null != applicantDTO) {
		            	  if ("RUW".equalsIgnoreCase( applicationDTO.getAppdecision())) {
		                        uwDecision.append( "Referred to Underwriting ");                           
		                     
		                    }
		            	  else if ("DCL".equalsIgnoreCase(applicationDTO.getAppdecision())) {
		            		  uwDecision.append( "Declined "); 		                                                                                 
		                        
		                    } else if ("ACC".equalsIgnoreCase( applicationDTO.getAppdecision()) 
		                    	) {
		                        uwDecision.append( MetlifeInstitutionalConstants.ACCEPT);        		                        
		                    }
		            	/*if(validCvrlist!=null && validCvrlist.size()>0){*/
		            	  if(validCvrlist!=null && !(validCvrlist.isEmpty())){
		            		for (int covItr = 0; covItr < validCvrlist.size(); covItr++) {
			                	coversDTO =  validCvrlist.get( covItr);                   	

			                    if ("DCL".equalsIgnoreCase(applicationDTO.getAppdecision()) && (null != coversDTO.getCoverreason()
		                                && coversDTO.getCoverreason().length() > 0)) {	                                                                              
			                    				uwritingDecisionBlurb.append("\t"); 
			                                	if("DEATH".equalsIgnoreCase(coversDTO.getCovercode())){
						                    		uwritingDecisionBlurb.append("Death cover - ");
						                  		}else if("TPD".equalsIgnoreCase(coversDTO.getCovercode())){
						                  			uwritingDecisionBlurb.append("Total & permanent disablement cover - ");
						                  		}
			                                	//MTAA Subject Change Starts
			                                	if(null != applicationDTO.getPartnercode() && ("MTAA").equalsIgnoreCase(applicationDTO.getPartnercode()) && ("IP").equalsIgnoreCase(coversDTO.getCovercode()) ){
			                                		uwritingDecisionBlurb.append(MetlifeInstitutionalConstants.SALARY_CONTINUANCE);
			                                	}else if("IP".equalsIgnoreCase(coversDTO.getCovercode())){
					                  				uwritingDecisionBlurb.append(MetlifeInstitutionalConstants.INCOME_PROTECTION_COVER);
			                                	}
			                                	//MTAA Subject Change Ends
			                                	if(null!= coversDTO.getCoverreason() &&  coversDTO.getCoverreason().length() >0 /*&& !uwritingDecisionBlurb.toString().contains( coversDTO.getCoverReason())*/){
				                                	
				                                	if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains("##") ){			
				                                		String declinereasons = coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().lastIndexOf("##"));
				                                		declinereasons = declinereasons.replaceAll("##", " & ");
				                                		uwritingDecisionBlurb.append(declinereasons);			                                		
				                                		log.info("uwritingDecisionBlurb>> {}",declinereasons);
				                                	}else{
				                                		uwritingDecisionBlurb.append(coversDTO.getCoverreason());
				                                	}
				                                	
				                                }
			                                	//Commented as per Sonar fixes. The else block is unreachable
			                                	/*else if(null!= coversDTO.getCoverreason() &&  coversDTO.getCoverreason().length() >0 && !uwritingDecisionBlurb.toString().contains( coversDTO.getCoverReason())){
				                                	
				                                	if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains(",") ){	
				                                		uwritingDecisionBlurb.append(coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().lastIndexOf(',')));
				                                	}else{
				                                		uwritingDecisionBlurb.append(coversDTO.getCoverreason());
				                                	}
				                                	
				                                }else if(null!= coversDTO.getCoverreason() &&  coversDTO.getCoverreason().length() >0 && !uwritingDecisionBlurb.toString().contains( coversDTO.getCoverOrgDclReason())){
				                                	uwritingDecisionBlurb.append( "\n");                         	
				                                	if(null!=coversDTO.getCoverreason() && coversDTO.getCoverreason().contains(",") ){	
				                                		uwritingDecisionBlurb.append(coversDTO.getCoverreason().substring(0, coversDTO.getCoverreason().lastIndexOf(',')));
				                                	}else{
				                                		uwritingDecisionBlurb.append(coversDTO.getCoverreason());
				                                	}
				                                	
				                            }*/
			                                if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode()))
			                                {
			                                	declineCover = EapplyHelper.getDCLProductName(applicationDTO, applicantDTO.getCovers(),"DCL",false);
			                                }
			                                	uwritingDecisionBlurb.append(MetlifeInstitutionalConstants.BREAK_LINE);
			                                	
			                        
			                    }
			                    else if ("ACC".equalsIgnoreCase( applicationDTO.getAppdecision())
			                            && mixedDecision) {
			                    	 
			                    		acceptedCover = EapplyHelper.getDCLProductName(applicationDTO, applicantDTO.getCovers(), "ACC", false);                   
				                        declineCover = EapplyHelper.getDCLProductName(applicationDTO, applicantDTO.getCovers(),"DCL",false);			                    	
				                         declineReason = EapplyHelper.getDCLProductName(applicationDTO, applicantDTO.getCovers(),"DCL",true);
				                         
				                         uwDecision.append( MetlifeInstitutionalConstants.ACCEPT+acceptedCover);   
			                        break;
			                    }else if("ACC".equalsIgnoreCase( applicationDTO.getAppdecision()) && !mixedDecision){
			                    	acceptedCover = EapplyHelper.getDCLProductName(applicationDTO, applicantDTO.getCovers(), "ACC", false);
			                    }
			                    uwDecision.append(coversDTO.getCovertype());
			                    if(covItr == applicantDTO.getCovers().size()-2){
			                    	uwDecision.append(MetlifeInstitutionalConstants.AND);
			                    }else if(covItr != applicantDTO.getCovers().size()-1){
			                    	uwDecision.append(", ");
			                    }			                    
			                    
			                    hostRuwCover.append(coversDTO.getCovertype());
			                    if(covItr == applicantDTO.getCovers().size()-2){
			                    	hostRuwCover.append(MetlifeInstitutionalConstants.AND);
			                    }else if(covItr != applicantDTO.getCovers().size()-1){
			                    	hostRuwCover.append(", ");
			                    }
			                    
			                }
		            	}
		                
		                if("ACC".equalsIgnoreCase( applicationDTO.getAppdecision())
		                		&& ("true".equalsIgnoreCase(applicationDTO.getPaindicator() )|| pdfObject.isExclusionInd())){
		                	isSpecialTerm = Boolean.TRUE;                        	
                        }
		            }
		        }
       }
            
         
            eVO.setApplicationId( pdfObject.getApplicationNumber());            
            
            /*Email Parameters */
        
	        eVO.setFromSender(properties.getProperty("fromsenderEmail"));      
	        eVO.setApplicationtype(lob);        
	        placeHolders.put( "EMAIL_SMTP_HOST", properties.getProperty("email_host"));
	        placeHolders.put( "EMAIL_SMTP_PORT", properties.getProperty("email_port"));
	       /* placeHolders.put( MetlifeInstitutionalConstants.EMAIL_SMTP_USERID, applicationDTO.getEmailSmtpUserId());
	        placeHolders.put( MetlifeInstitutionalConstants.EMAIL_SMTP_PASS, applicationDTO.getEmailSmtpPass());
	        
	        mailList.add( emailID); */
	        mailList.add( fundEmailID); 
	     
	        if ("RUW".equalsIgnoreCase( overAllDecision)) {
	            eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_RUW));
	        } else if ("ACC".equalsIgnoreCase(overAllDecision)) {
                eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_ACC));
            } else if ("DCL".equalsIgnoreCase(overAllDecision)) {
            	/*SRKR:15/10/2014 life events and transfer events decline flow is applicable only for VICS
            	 * For HOST REIS it wont come here.
            	 * */
            	if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
            		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_UWCOVER_DCL));
            	}else if(MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
            		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_ICOVER_DCL));
            	}else if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
            		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_TCOVER_DCL));
            	}else if(MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
            		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_SCOVER_DCL));
            	}else{
            		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_DCL));
            	}
                
            }else{
            	 eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_ACC));
            }
	       
	     	        
	        String subject = null;

        	
      	  if ("RUW".equalsIgnoreCase( overAllDecision)) {
	        	  if(uwDecision.toString().contains(MetlifeInstitutionalConstants.REFERRED_TO_UNDERWRITING)){
	        		  uwDecision = uwDecision.replace(uwDecision.indexOf(MetlifeInstitutionalConstants.REFERRED_TO_UNDERWRITING), uwDecision.indexOf(MetlifeInstitutionalConstants.REFERRED_TO_UNDERWRITING)+25, ""); 
	        		  if("HOST".equalsIgnoreCase(applicationDTO.getPartnercode()) || "FIRS".equalsIgnoreCase(applicationDTO.getPartnercode()) || "SFPS".equalsIgnoreCase(applicationDTO.getPartnercode())
	        				  || "MTAA".equalsIgnoreCase(applicationDTO.getPartnercode())){
	        			  acceptedCover= hostRuwCover.toString();
	        		  }
	        	  }
      		  if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_RUW).contains(MetlifeInstitutionalConstants.ACCEPT_COVER) && null!=acceptedCover){
      			  	subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_RUW).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover);	        		 
	        	}else{
	        			 if("AEIS".equalsIgnoreCase(applicationDTO.getPartnercode())) {
	        				 String uwdec=uwDecision.toString();
	        				 String tpddecision=uwdec.replaceAll(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT, MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
	          				String newuwDecision=tpddecision.replaceAll(MetlifeInstitutionalConstants.SALARY_CONTINUANCE, MetlifeInstitutionalConstants.INCOME_PROTECTION);
	          				log.info("UW DECISION -- {}",newuwDecision);
	          				subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_RUW).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, newuwDecision);	
	        			 }
	        		else {
	        			subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_RUW).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, "");
	        		}
	        		
	        	}
      		  
      		  
	      }else if ("DCL".equalsIgnoreCase(overAllDecision)) {	        	
	        	if(uwDecision.toString().contains(MetlifeInstitutionalConstants.DECLINE)){
	        		  uwDecision = uwDecision.replace(uwDecision.indexOf(MetlifeInstitutionalConstants.DECLINE), uwDecision.indexOf(MetlifeInstitutionalConstants.DECLINE)+8, "");  
	        	}
	        	if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	        		subject=properties.getProperty("email_brk_subj_uwcover_dcl");
	        	}else if(MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	        		subject=properties.getProperty("email_brk_subj_icover_dcl");
	        	}else if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	        		subject=properties.getProperty("email_brk_subj_tcover_dcl");
	        	}else if(MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	        		subject=properties.getProperty("email_brk_subj_scover_dcl");
	        	}else if(MetlifeInstitutionalConstants.NCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	        		subject=properties.getProperty("email_brk_subj_newmember_dcl");
	        	}else{
	        		/*subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_DCL);*/
	        		 if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_DCL).contains(MetlifeInstitutionalConstants.DECLINE_COVER) && null!=declineCover){
	        			 subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_DCL).replace(MetlifeInstitutionalConstants.DECLINE_COVER, declineCover);       		 
	 	        	}
	        		 else if(("HOST".equalsIgnoreCase(applicationDTO.getPartnercode()) || "FIRS".equalsIgnoreCase(applicationDTO.getPartnercode()) || "SFPS".equalsIgnoreCase(applicationDTO.getPartnercode()) 
	        				 || "MTAA".equalsIgnoreCase(applicationDTO.getPartnercode()))&&
	        				 properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_DCL).contains(MetlifeInstitutionalConstants.DECLINE_COVER) && null!=uwDecision ){
	        			 subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_DCL).replace(MetlifeInstitutionalConstants.DECLINE_COVER, uwDecision); 
	        		  }
	        		 
	        		 else{
	        			 if("AEIS".equalsIgnoreCase(applicationDTO.getPartnercode())) {
	        				 String uwdec=uwDecision.toString();
	        				 String tpddecision=uwdec.replaceAll(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT, MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
	          				String newuwDecision=tpddecision.replaceAll(MetlifeInstitutionalConstants.SALARY_CONTINUANCE, MetlifeInstitutionalConstants.INCOME_PROTECTION);
	          				log.info("UW DECISION >> {}",newuwDecision);
	          				subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_DCL).replace(MetlifeInstitutionalConstants.DECLINE_COVER, newuwDecision);	
	        			 }
	        			 else {
	 	        		subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_DCL).replace(MetlifeInstitutionalConstants.DECLINE_COVER, "");
	        			 }
	 	        	}
	        	}
	        	
            }else if (null==overAllDecision) {
            	if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	        		subject=properties.getProperty("email_brk_subj_uwcover_wrkrtnmaint");
	        	}else  if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	        		subject=properties.getProperty("email_brk_subj_cancelcover_acc");
	        	}
            }else if ("ACC".equalsIgnoreCase(overAllDecision) && (pdfObject.isExclusionInd() || pdfObject.isLoadingInd())) {	            	  
          	  if(uwDecision.toString().contains(MetlifeInstitutionalConstants.MIXED_ACCEPTANCE_AND_DECLINE)){
          		  uwDecision = uwDecision.replace(uwDecision.indexOf(MetlifeInstitutionalConstants.MIXED_ACCEPTANCE_AND_DECLINE), uwDecision.indexOf(MetlifeInstitutionalConstants.MIXED_ACCEPTANCE_AND_DECLINE)+29, MetlifeInstitutionalConstants.ACCEPT_); 
          	  }
          	  
          	
          	if(applicationDTO!=null && mixedDecision && pdfObject!=null && !"RUW".equalsIgnoreCase(overAllDecision) ){
          		/*if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.SPECIAL_CONDITION) && pdfObject.isExclusionInd() && pdfObject.isLoadingInd()){
					eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, MetlifeInstitutionalConstants.SPECIALTERMMSG));
				}else if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.SPECIAL_CONDITION) && !pdfObject.isExclusionInd() && pdfObject.isLoadingInd()){
					eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, MetlifeInstitutionalConstants.SPECIALTERMMSG));
				}else if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.SPECIAL_CONDITION) && pdfObject.isExclusionInd() && !pdfObject.isLoadingInd()){
					eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, MetlifeInstitutionalConstants.SPECIALTERMMSG));
				}else if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.SPECIAL_CONDITION)){
					eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, MetlifeInstitutionalConstants.SPECIALTERMMSG));
				}
				if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.ACCEPT_COVER) && null!=acceptedCover){					
					eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover));  	        		
 	        	}
 	        	if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.DECLINE_COVER) && null!=declineCover){
 	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.DECLINE_COVER, declineCover));
 	        		
 	        	}
 	        	if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).contains(MetlifeInstitutionalConstants.DECLINE_REASONS) && null!=declineReason){
 	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.DECLINE_REASONS, declineReason));
 	        	}*/
 	        	if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_MXD_ACC).contains(MetlifeInstitutionalConstants.ACCEPT_COVER) && null!=acceptedCover){
 	        		subject= properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_MXD_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover);         	        		 
 	        	}else{
					subject= properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_MXD_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, "");         							
				}  	        	
          	  }else if(pdfObject.isExclusionInd() && pdfObject.isLoadingInd()){          		 
          		/*subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_LOAD_EXL_ACC);*/
          		if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_LOAD_EXL_ACC).contains(MetlifeInstitutionalConstants.ACCEPT_COVER) && null!=acceptedCover){
          			 if("AEIS".equalsIgnoreCase(applicationDTO.getPartnercode())) {
        				 String uwdec=acceptedCover;
        				 String tpddecision=uwdec.replaceAll(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT, MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
          				String newuwDecision=tpddecision.replaceAll(MetlifeInstitutionalConstants.SALARY_CONTINUANCE, MetlifeInstitutionalConstants.INCOME_PROTECTION);
          				log.info("UW DECISION :- {}",newuwDecision);
          				subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_LOAD_EXL_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, newuwDecision);	
        			 }
          			 else {
          				subject= properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_LOAD_EXL_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover);    
          			 }
 	        	}else{
					subject= properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_LOAD_EXL_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, "");           							
				}
          	  }else if(pdfObject.isExclusionInd()){
          		 /* subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_EXL_ACC);*/
          		if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_EXL_ACC).contains(MetlifeInstitutionalConstants.ACCEPT_COVER) && null!=acceptedCover){
          			 if("AEIS".equalsIgnoreCase(applicationDTO.getPartnercode())) {
        				 String uwdec=acceptedCover;
        				 String tpddecision=uwdec.replaceAll(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT, MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
          				String newuwDecision=tpddecision.replaceAll(MetlifeInstitutionalConstants.SALARY_CONTINUANCE, MetlifeInstitutionalConstants.INCOME_PROTECTION);
          				log.info("UW DECISION :: {}",newuwDecision);
          				subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_EXL_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, newuwDecision);	
        			 }
          			 else {
 	        		subject= properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_EXL_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover);    
          			 }
 	        	}else{
					subject= properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_EXL_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, "");           							
				}
          	  }else if(pdfObject.isLoadingInd()){
          		/*subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_LOAD_ACC);*/
          		if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_LOAD_ACC).contains(MetlifeInstitutionalConstants.ACCEPT_COVER) && null!=acceptedCover){
 	        		subject= properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_LOAD_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover);         	        		 
 	        	}else{
					subject= properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_LOAD_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, "");           							
				}
          	  }
            }else if ("ACC".equalsIgnoreCase(overAllDecision) ) {
          	  if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
          		subject = properties.getProperty("email_brk_subj_uwcover_acc"); 
	              }else if(MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	            	  subject = properties.getProperty("email_brk_subj_icover_acc"); 
	              }else if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	            	  subject = properties.getProperty("email_brk_subj_tcover_acc"); 
	              }else if(MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	            	  subject = properties.getProperty("email_brk_subj_scover_acc"); 
	              }else if(MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && mixedDecision){
	            	  subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_MXD_ACC).replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, MetlifeInstitutionalConstants.SPECIALTERMMSG);
	              }else if(MetlifeInstitutionalConstants.NCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	            	  subject = properties.getProperty("email_brk_subj_newmember_acc"); 
	              }
          	  	//Added for Cancel cover client mail acknowledged Hostplus
	              else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_CANCEL_ACK)!=null ){
	            	if("AEIS".equalsIgnoreCase(applicationDTO.getPartnercode())) {
	            		if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_CANCEL_ACK).contains("<%CAN_COVER%>") && null!=acceptedCover) {
	            		String uwdec=acceptedCover;
	            		String tpddecision=uwdec.replaceAll(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT, MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
	          				String newuwDecision=tpddecision.replaceAll(MetlifeInstitutionalConstants.SALARY_CONTINUANCE, MetlifeInstitutionalConstants.INCOME_PROTECTION);
	          				log.info("UW DECISION::- {}",newuwDecision);
	          				subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_CANCEL_ACK).replace("<%CAN_COVER%>", newuwDecision);	
	            		}
	            		else {
	            			 subject = properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_CANCEL_ACK); 
	            		}
	            	}
	            	else {
	            		 subject = properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_CANCEL_ACK); 
	            	}
	            	 
	              }
	              else{
	            	  if(uwDecision.toString().contains(MetlifeInstitutionalConstants.MIXED_ACCEPTANCE_AND_DECLINE)){
	            		  uwDecision = uwDecision.replace(uwDecision.indexOf(MetlifeInstitutionalConstants.MIXED_ACCEPTANCE_AND_DECLINE), uwDecision.indexOf(MetlifeInstitutionalConstants.MIXED_ACCEPTANCE_AND_DECLINE)+29, MetlifeInstitutionalConstants.ACCEPT_); 
	            	  }
	            	  /*subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_LOAD_ACC);*/
	            	  if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_LOAD_ACC) != null && properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_LOAD_ACC).contains(MetlifeInstitutionalConstants.ACCEPT_COVER) && null!=acceptedCover && mixedDecision){
	            		  if("AEIS".equalsIgnoreCase(applicationDTO.getPartnercode())) {
		        				 String uwdec=acceptedCover;
		        				 String tpddecision=uwdec.replaceAll(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT, MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
		          				String newuwDecision=tpddecision.replaceAll(MetlifeInstitutionalConstants.SALARY_CONTINUANCE, MetlifeInstitutionalConstants.INCOME_PROTECTION);
		          				log.info(MetlifeInstitutionalConstants.UW_DECISION_CURLY,newuwDecision);
		          				subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_LOAD_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, newuwDecision);	
		        			 }
	            		  else {
	            		  subject= properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_LOAD_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover);       
	            		  }
		   	        	}
	            	  
	            	  if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_ACC).contains(MetlifeInstitutionalConstants.ACCEPT_COVER) && null!=acceptedCover){
	            		  if("AEIS".equalsIgnoreCase(applicationDTO.getPartnercode())) {
		        				 String uwdec=acceptedCover;
		        				 String tpddecision=uwdec.replaceAll(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT, MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
		          				String newuwDecision=tpddecision.replaceAll(MetlifeInstitutionalConstants.SALARY_CONTINUANCE, MetlifeInstitutionalConstants.INCOME_PROTECTION);
		          				log.info(MetlifeInstitutionalConstants.UW_DECISION_CURLY,newuwDecision);
		          				subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, newuwDecision);	
		        			 }
	            		  else {
	            		  subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover);	     
	            		  }
		  	        	}else{
		  	        		subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, "");  
		  	        	}
	              }
            }else{
          	  subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_ACC);
            	
          	  /*if ("ACC".equalsIgnoreCase(overAllDecision) && properties.getProperty(MetlifeInstitutionalConstants.CLIENT_EMAIL_SUBJ_ACC)!=null && !MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && !MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && !MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())&& !MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && !MetlifeInstitutionalConstants.NCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())) {    	            	  
          		  subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_ACC);
          	  }*/
             
            	if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_ACC).contains(MetlifeInstitutionalConstants.ACCEPT_COVER) && null!=acceptedCover){
            		subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover);	        		 
	        	}else{
	        		subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, "");  
	        	}	
             	
            }
       if(MetlifeInstitutionalConstants.FUND_HOST.equalsIgnoreCase(applicationDTO.getPartnercode()) && properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_MXD_ACC).contains(MetlifeInstitutionalConstants.ACCEPT_COVER) && null!=acceptedCover && mixedDecision){
       		 subject= properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_MXD_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover);         
         }
      
      	 if(partnerCode != null && "CORP".equalsIgnoreCase(partnerCode)) {
      		 isChangeNo= Boolean.FALSE;
         }
         else {
       	  isChangeNo = EapplyHelper.isNoChange(applicationDTO);
         }
	        if(MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && isChangeNo){
	        	subject = properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_NOCHANGE_ACC); 
	        	 if(subject!=null && subject.contains(MetlifeInstitutionalConstants.COVER)){
	        		 subject = subject.replace(MetlifeInstitutionalConstants.COVER, EapplyHelper.getDCLProductName(applicationDTO, applicantDTO.getCovers(), "ACC", false));	        		 
	        	 }	        	 
	        	 if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_NOCHANGE_ACC).contains(MetlifeInstitutionalConstants.ACCEPT_COVER) && null!=acceptedCover){
	        		  if("AEIS".equalsIgnoreCase(applicationDTO.getPartnercode())) {
	        				 String uwdec=acceptedCover;
	        				 String tpddecision=uwdec.replaceAll(MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT, MetlifeInstitutionalConstants.TOTAL_PERMANENT_DISABLEMENT);
	          				String newuwDecision=tpddecision.replaceAll(MetlifeInstitutionalConstants.SALARY_CONTINUANCE, MetlifeInstitutionalConstants.INCOME_PROTECTION);
	          				log.info(MetlifeInstitutionalConstants.UW_DECISION_CURLY,newuwDecision);
	          				subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, newuwDecision);	
	        			 }
	        		  else {
		        		subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_NOCHANGE_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover);
	        		  }
		        	}else{
		        		subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_NOCHANGE_ACC).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, "");  
		        	}
	        }   
	        
	        /*if(MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	        	subject = properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BRK_SUBJ_NOCHANGE_ACC); 
	        	 if(subject!=null && subject.contains(MetlifeInstitutionalConstants.COVER)){
	        		 subject = subject.replace(MetlifeInstitutionalConstants.COVER, EapplyHelper.getDCLProductName(applicantDTO.getCovers(), "ACC", false));	        		 
	        	 }
	        }*/
	        if(corpfund!=null) {
	        	fundName=corpfund.getPartnerName();
	        	if(subject.contains("<%PARTNERNAME%>")) {
	        		subject=subject.replace("<%PARTNERNAME%>", fundName);
	        	}
	        }
	        
	        if("ACC".equalsIgnoreCase(overAllDecision) && MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())
	        		&& MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode())
	        		&& subject.contains("<%COVERS%>"))
	        {
	        	subject = subject.replace("<%COVERS%>", hostRuwCover);
	        }
	        
	        placeHolders.put( "TITLE", eVO.getTitle());
	        //THis code needs to be removed after vicsuper UAT
	        if (!(pdfObject.getMemberDetails().getClientRefNum().contains("MET") || pdfObject.getMemberDetails().getClientRefNum().contains("met"))
  	        		&& MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode())){
	        	placeHolders.put( "TESTEMAIL",fundEmailID );
	        } else {
	        	placeHolders.put( "TESTEMAIL", "");
	        }
	        placeHolders.put( "FIRSTNAME", eVO.getFirstName());
	        placeHolders.put( "LASTNAME", eVO.getLastName());
	        placeHolders.put( "PDFLINK", pdfAttachDir);
	        placeHolders.put( "EMAIL_IND_SUBJECT_NAME", applicantName);
	        placeHolders.put(MetlifeInstitutionalConstants.EMAIL_SUBJECT, subject);
	        /*System.out.println("fundemailsubject"+subject);*/
	        log.info("fundemailsubject {}",subject);
	        /*SRKR; Adde new variable member_no to show in emails, usefull for GUIL and ACCS*/
	        if(pdfObject!=null && pdfObject.getMemberDetails()!=null && pdfObject.getMemberDetails().getClientRefNum()!=null){
	        	placeHolders.put( "MEMBER_NO", pdfObject.getMemberDetails().getClientRefNum());
	        }
	        
	        if(MetlifeInstitutionalConstants.INSTITUTIONAL.equalsIgnoreCase(lob)){
	        	/*eVO.setCcReciepent();*/        	
	        	
	        	placeHolders.put(MetlifeInstitutionalConstants.UW_DECISION, uwDecision);
	        	if("ACC".equalsIgnoreCase(overAllDecision)){
	        		if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){        			
	        			eVO.setEmailTemplateId(properties.getProperty("email_body_uwcover_acc"));	        			
		            }else if(MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
		            	 eVO.setEmailTemplateId(properties.getProperty("email_body_icover_acc"));
		            }else if(MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
		            	 eVO.setEmailTemplateId(properties.getProperty("email_body_tcover_acc"));
		            }else if(MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
		            	 eVO.setEmailTemplateId(properties.getProperty("email_body_scover_acc"));
		            }else if(MetlifeInstitutionalConstants.NCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
		            	eVO.setEmailTemplateId(properties.getProperty("email_body_newmember_acc"));
		            }
	        		//Added for Cancel cover client mail acknowledged Hostplus
		            else if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && properties.getProperty("email_body_cancel_ack")!=null ){
	        			eVO.setEmailTemplateId(properties.getProperty("email_body_cancel_ack"));
	        		}else if(pdfObject.isLoadingInd() && pdfObject.isExclusionInd()){
	        			eVO.setEmailTemplateId(properties.getProperty("email_body_load_excl_acc"));
	        		}else if(pdfObject.isLoadingInd()){
	        			eVO.setEmailTemplateId(properties.getProperty("email_body_load_acc"));
	        		}else if(pdfObject.isExclusionInd()){
	        			eVO.setEmailTemplateId(properties.getProperty("email_body_excl_acc"));
	        		} 
	        	}else if(null==overAllDecision){
	        		if(MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
	        			eVO.setEmailTemplateId(properties.getProperty("email_body_uwcover_wrmaint"));
		        		subject=properties.getProperty("email_brk_subj_uwcover_wrkrtnmaint");
		        	}else  if(MetlifeInstitutionalConstants.CANCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
		        		eVO.setEmailTemplateId(properties.getProperty("email_body_cancel_acc"));
		        		subject=properties.getProperty("email_brk_subj_cancelcover_acc");
		        	}
	        	}
        		    		        	
	        	
	        	/*SRKR: 25/11/2013 Added !RUW because in case of mix acceptance and client match
	        	 * mixed accpetance flag is true and overall decision is RUW so add one more condition to check
	        	 * if it is not RUW than check for Mixed acceptance*/
	 	        if(!"RUW".equalsIgnoreCase(overAllDecision) && mixedDecision ){
	 	        	String emailProp = null;
	 	        	if(null!=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD)){
	 	        		emailProp = properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD);
	 	        	}
	 	        	if(null!=emailProp)
	 	        	{
	 	        	if(isSpecialTerm){
	 	        		if(emailProp.contains(MetlifeInstitutionalConstants.SPECIAL_CONDITION)){
		 	        		//eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, MetlifeInstitutionalConstants.SPECIALTERMMSG));
	 	        			emailProp=emailProp.replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, MetlifeInstitutionalConstants.SPECIALTERMMSG);
		 	        	}
	 	        	}else{
	 	        		if(emailProp.contains(MetlifeInstitutionalConstants.SPECIAL_CONDITION)){
		 	        		//eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, ""));
	 	        			emailProp=emailProp.replace(MetlifeInstitutionalConstants.SPECIAL_CONDITION, "");
		 	        	}
	 	        	} 
	 	        	if(emailProp.contains(MetlifeInstitutionalConstants.ACCEPT_COVER) && null!=acceptedCover){
	 	        		//eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover));	
	 	        		emailProp=emailProp.replace(MetlifeInstitutionalConstants.ACCEPT_COVER, acceptedCover);	
	 	        	}
	 	        	if(emailProp.contains(MetlifeInstitutionalConstants.DECLINE_COVER) && null!=declineCover){
	 	        		//eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.DECLINE_COVER, declineCover));
	 	        		emailProp=emailProp.replace(MetlifeInstitutionalConstants.DECLINE_COVER, declineCover);	 	        		
	 	        	}
	 	        	if(emailProp.contains(MetlifeInstitutionalConstants.DECLINE_REASONS) && null!=declineReason){	 	        		
	 	        		//eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_MXD).replace(MetlifeInstitutionalConstants.DECLINE_REASONS, declineReason));
	 	        		if(declineReason.contains("##")){
	 	        			declineReason = declineReason.replaceAll("##", " ");
	 	        		}
	 	        		emailProp=emailProp.replace(MetlifeInstitutionalConstants.DECLINE_REASONS, declineReason);
	 	        		
	 	        	}
	 	        	eVO.setEmailTemplateId(emailProp);
	 	        	}
	 	        	
	 	        	log.info("fund_change: {} ",eVO.getEmailTemplateId());
	 	        }else if ("DCL".equalsIgnoreCase(overAllDecision)) {
	            	/*SRKR:15/10/2014 life events and transfer events decline flow is applicable only for VICS
	            	 * For HOST REIS it wont come here.
	            	 * */
	 	        	if(applicationDTO.getRequesttype()!=null && MetlifeInstitutionalConstants.UWCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) ){ 	       		
	 	        		
	 	        		if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_UWCOVER_DCL).contains(MetlifeInstitutionalConstants.DECLINE_REASONS)){
	 	        			eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_UWCOVER_DCL).replace(MetlifeInstitutionalConstants.DECLINE_REASONS, uwritingDecisionBlurb.toString()));
		 	        	}else{
		 	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_UWCOVER_DCL));
		 	        	}
		 	        	
	            	}else if(applicationDTO.getRequesttype()!=null && MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) ){ 	        		
	 	        		
	 	        		if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_ICOVER_DCL).contains(MetlifeInstitutionalConstants.DECLINE_REASONS)){
	 	        			eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_ICOVER_DCL).replace(MetlifeInstitutionalConstants.DECLINE_REASONS, uwritingDecisionBlurb.toString()));
		 	        	}
		 	        	
	            	}else if(applicationDTO.getRequesttype()!=null && MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
		 	        	if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_TCOVER_DCL).contains(MetlifeInstitutionalConstants.DECLINE_REASONS)){
		 	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_TCOVER_DCL).replace(MetlifeInstitutionalConstants.DECLINE_REASONS, uwritingDecisionBlurb.toString()));
		 	        	}else{
		 	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_TCOVER_DCL));
		 	        	}
		 	        	
		        	}else if(applicationDTO.getRequesttype()!=null && MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
		        		if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_SCOVER_DCL).contains(MetlifeInstitutionalConstants.DECLINE_REASONS)  && null!=coversDTO.getCoverreason()){
							eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_SCOVER_DCL).replace(MetlifeInstitutionalConstants.DECLINE_REASONS, uwritingDecisionBlurb.toString()));
						}
		 	        	else{
		 	        		eVO.setEmailTemplateId(properties.getProperty("email_body_scover_dcl_noreason"));
		 	        	}
		        	}else if(applicationDTO.getRequesttype()!=null && MetlifeInstitutionalConstants.NCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())){
		 	        	if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_NEWMEMBER_DCL).contains(MetlifeInstitutionalConstants.DECLINE_REASONS)){
		 	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_NEWMEMBER_DCL).replace(MetlifeInstitutionalConstants.DECLINE_REASONS, uwritingDecisionBlurb.toString()));
		 	        	}else{
		 	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_NEWMEMBER_DCL));
		 	        	}
		        	}else{
		 	        	if(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_DCL).contains(MetlifeInstitutionalConstants.DECLINE_REASONS)){
		 	        		eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_DCL).replace(MetlifeInstitutionalConstants.DECLINE_REASONS, uwritingDecisionBlurb.toString()));
		 	        	}
		 	        	
	            	}	 	        	
	 	        	
	 	        }else if("RUW".equalsIgnoreCase(overAllDecision)){
	 	        	eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_RUW));
	 	        }
	 	       placeHolders.put("UW_DECISION_BLURB", "");	
	        }
	        placeHolders.put( MetlifeInstitutionalConstants.APPLICATION_NUMBER, applicationDTO.getApplicationumber());	
	        
	        
	        
	      
	        /*placeHolders.forEach((k,v) -> System.out.println("Key = "+ k + ", Value = " + v));*/
	        placeHolders.forEach((k,v) -> log.info("Key = {} and Value = {} ",k,v));

	        eVO.setEmailDataElementMap( placeHolders); 
	      //Code has been added for visuper UAT
	        if (!(pdfObject.getMemberDetails().getClientRefNum().contains("MET") || pdfObject.getMemberDetails().getClientRefNum().contains("met"))
	        		&& MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode())){
	        	mailList = new ArrayList();
	        	mailList.add(properties.getProperty("vicsuper_test_email"));
	        }
	        eVO.setToRecipents( mailList); 	     
	       /*String htmlEmails= null;*/
	        /*SRKR This flag determine whether to send html emails or not*/
	        eVO.setHtmlEmails(true);
	        /*SRKR: Added documments for APSD to fwd to fund administrator*/
	        /*PD: Commented section to obtain only one PDF in email */
	       /* if(applicationDTO.getDocuments()!=null){
	        	eVO.setDocumentList(applicationDTO.getDocuments());
	        }*/
	        /*PD: Commented section to obtain only one PDF in email */
	        if(MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(applicationDTO.getRequesttype()) && isChangeNo ){
	        	eVO.setEmailTemplateId(properties.getProperty("email_body_nochange"));
	        }
	        
	        /*bcc recipent*/
	       /*if((MetlifeInstitutionalConstants.ICOVER.equalsIgnoreCase(applicationDTO.getRequesttype())|| MetlifeInstitutionalConstants.TCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())|| MetlifeInstitutionalConstants.SCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())) && null!=applicationDTO.getBccReciepent() && applicationDTO.getBccReciepent().length()>0){
	        	 eVO.setBccReciepent(applicationDTO.getBccReciepent());
	        }else if(MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(applicationDTO.getRequestType()) && isChangeNo && null!=applicationDTO.getBccReciepent() && applicationDTO.getBccReciepent().length()>0){
	        	eVO.setEmailTemplateId(applicationDTO.getEmailBodyNoChange());
	        	eVO.setBccReciepent(applicationDTO.getBccReciepent());
	        	 placeHolders.put( MetlifeInstitutionalConstants.EMAIL_SUBJECT, subject);
	        }else if(null!=applicationDTO.getBccReciepent() && applicationDTO.getBccReciepent().length()>0){
	        	eVO.setBccReciepent(applicationDTO.getBccReciepent());
	        } */ 
	        
	        /*MetLifeEmailService.getInstance().sendMail( eVO, pdfAttachDir);*/
	    	if(partnerCode!= null && "CORP".equalsIgnoreCase(partnerCode) && corpfund.getCorpFundCategories()!=null) {
      			String uwdecision=null;
      			StringBuilder uwnew = new StringBuilder();
      			String corpSubject=null;
      			for(CorporateFundCat coverRequired:corpfund.getCorpFundCategories()) {
      				if("Y".equalsIgnoreCase(coverRequired.getDeathRequired().toString())) {
      					if(uwnew.length()!=0) {
      						uwnew.append(",").append(MetlifeInstitutionalConstants.DEATH_CASE);
      					}
      					else {
      						if(applicantDTO!=null && "ACC".equalsIgnoreCase(applicationDTO.getAppdecision())) {
      					uwnew.append(MetlifeInstitutionalConstants.ACCEPT+MetlifeInstitutionalConstants.DEATH_CASE);
      						}
      						else {
      							uwnew.append(MetlifeInstitutionalConstants.DEATH_CASE);
      						}
      						}
      				} 
      				if("Y".equalsIgnoreCase(coverRequired.getTpdRequired().toString())) {
      					if(uwnew.length()!=0) {
      						uwnew.append(",").append("Total & Permanent Disability");
      					}
      					else {
      						if(applicantDTO!=null && "ACC".equalsIgnoreCase(applicationDTO.getAppdecision())) {
      						uwnew.append( MetlifeInstitutionalConstants.ACCEPT+"Total & Permanent Disability");
      						}
      						else {
          						uwnew.append("Total & Permanent Disability");
      						}
      						}
      					
      				}
      				if("Y".equalsIgnoreCase(coverRequired.getIpRequired().toString())) {
      					if(uwnew.length()!=0) {
      						uwnew.append(",").append(MetlifeInstitutionalConstants.INCOME_PROTECTION);
      					}
      					else {
      						if(applicantDTO!=null && "ACC".equalsIgnoreCase(applicationDTO.getAppdecision())) {
      						uwnew.append( MetlifeInstitutionalConstants.ACCEPT+MetlifeInstitutionalConstants.INCOME_PROTECTION);
      						}
      						else {
      							uwnew.append( MetlifeInstitutionalConstants.INCOME_PROTECTION);
      						}
      						}
      					
      				}
      			}
      			placeHolders.put(MetlifeInstitutionalConstants.UW_DECISION, uwnew.toString());
      			placeHolders.put(MetlifeInstitutionalConstants.UW_DECISION_BLURB, uwnew.toString());
      		}
	    	//Added for Vicsuper fix your cover mail
      		if(MetlifeInstitutionalConstants.CCOVER.equalsIgnoreCase(applicationDTO.getRequesttype())
      				&& MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode())
      				&& pdfObject.isConvertCheckPdf() && null!=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_FIX_YOUR_COVER)
      				&& null!=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_SUBJ_FIX_YOUR_COVER))
      		{
      			subject=properties.getProperty(MetlifeInstitutionalConstants.EMAIL_SUBJ_FIX_YOUR_COVER);
      			eVO.setEmailTemplateId(properties.getProperty(MetlifeInstitutionalConstants.EMAIL_BODY_FIX_YOUR_COVER));
      			placeHolders.put(MetlifeInstitutionalConstants.EMAIL_SUBJECT, subject);
      		}
	        
	    	if(MetlifeInstitutionalConstants.FUND_VICS.equalsIgnoreCase(applicationDTO.getPartnercode())
      				&& (eVO.getEmailTemplateId()).contains(MetlifeInstitutionalConstants.VICSUPER_LOGO))
      		{
      			eVO.setEmailTemplateId((eVO.getEmailTemplateId()).replace(MetlifeInstitutionalConstants.VICSUPER_LOGO, vicSuperLogo));
      		}
	        //callEmailService(eVO,authId,properties,applicationDTO.getPartnercode(),pdfObject.getApplicationNumber());
	        callEmailService(eVO,authId,properties);
	        log.info("fund: {}",eVO.getEmailTemplateId());
	            
        }/*catch(SendFailedException emailException){
        	emailException.printStackTrace();
        } */
        catch (Exception e) {
        	log.error("Error in send fund admin email: {}",e.getMessage());
        }
        log.info("Send fund admin email finsh"); 
    }
	
	private List<Cover> coversSelected(Applicant applicantDTO){
		log.info("Cover selected start");
    	Cover coversDTO = null;
    	List<Cover> validCvrList = new ArrayList<>();
    	/*if(null!=applicantDTO && null!=applicantDTO.getCovers() && applicantDTO.getCovers().size()>0){*/
    	if(null!=applicantDTO && null!=applicantDTO.getCovers() && !(applicantDTO.getCovers().isEmpty())){
    		for(int itr=0;itr<applicantDTO.getCovers().size();itr++){
    			coversDTO = applicantDTO.getCovers().get(itr);
    			if(null!=coversDTO && coversDTO.getCoverdecision()!=null && coversDTO.getCoverdecision().length()>0){
    				validCvrList.add(coversDTO);
    			}
    		}
    		//applicantDTO.setCovers(validCvrList);
    	}
    	//validCvrList = null;
    	log.info("Cover selected finish");
    	return validCvrList;
    }
	
	public Boolean isMixedDecision(ResponseObject responseObject){
		log.info("Mixed decision start");
		Boolean mixedDecision = Boolean.FALSE;
		StringBuilder productdecisionBuffer = new StringBuilder();
		Insured insured = null;
		for (int itr = 0; itr < responseObject.getClientData().getListInsured().size(); itr++) {
			insured = responseObject.getClientData().getListInsured().get(itr);					
			if (null != insured && null != insured.getListOverallDec()) {
				for (int insItr = 0; insItr < insured.getListOverallDec().size(); insItr++) {
					Decision decision = insured.getListOverallDec().get(insItr);
					if (null != decision && null != decision.getProductName() && !"".equalsIgnoreCase(decision.getProductName().trim())) {
						productdecisionBuffer.append(decision.getDecision());
					}
				}
			}
		}
		 if(null!=productdecisionBuffer && !"".equalsIgnoreCase(productdecisionBuffer.toString())
				 && (productdecisionBuffer.toString().contains("ACC") && productdecisionBuffer.toString().contains("DCL") && !productdecisionBuffer.toString().contains("RUW"))){ 
				 mixedDecision = Boolean.TRUE;    
		  }
		 log.info("Mixed decision finish");
		return mixedDecision;
	}
	/**
	 * @author Admin.kp
	 * @input pdf,properties,decision object details
	 * @param inputStream
	 * @return
	 * 
	 * 		This Method is used to send email for decline for vicsuper
	 */

	 public void sendDeclineEmail(PDFObject pdfObject,Eapplication applicationDTO, 
	            String overAllDecision, String pdfAttachDir, String lob, Properties properties, String authId,CorporateFund corpfund, Boolean mixedDecision, String partnerCode){
		 
		 sendEmail(pdfObject, applicationDTO, overAllDecision, pdfAttachDir, lob, properties, authId, corpfund, mixedDecision, partnerCode);
		 
	 }

}
