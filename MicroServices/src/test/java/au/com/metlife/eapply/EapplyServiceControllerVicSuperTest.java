package au.com.metlife.eapply;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.FileReader;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import au.com.metlife.eapply.bs.controller.EapplyServicesController;
import au.com.metlife.eapply.bs.model.EappHistory;
import au.com.metlife.eapply.bs.service.EapplyService;
import au.com.metlife.eapply.bs.serviceimpl.EapplyServiceImpl;
import au.com.metlife.eapply.quote.utility.SystemProperty;
import au.com.metlife.eapply.utils.TestUtils;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MicroServicesApplication.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EapplyServiceControllerVicSuperTest extends TestUtils {

	@InjectMocks
	private EapplyServicesController eapplyServicesController;

	@InjectMocks
	private EapplyServiceImpl service;

	private MockMvc mockMvc;

	@MockBean
	private EapplyService eapplyService;

	/*
	 * Created testData to store the string json value from createTestData()
	 */
	String testData = null;

	/*
	 * Created fileName to store the json file name and pass it to
	 * createTestData()
	 */
	String fileName = null;
	

	@Autowired
	private WebApplicationContext context;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
		EappHistory eappHistory = new EappHistory();
		Mockito.when(eapplyService.createHistory(eappHistory)).thenReturn(eappHistory);
		 
		String swissmediumfont = new ClassPathResource("2A09CD_2_0.ttf").getFile().getAbsolutePath();
		String swisslightfont = new ClassPathResource("2A09CD_0_0.ttf").getFile().getAbsolutePath();
		String swissthinfont = new ClassPathResource("2A09CD_1_0.ttf").getFile().getAbsolutePath();

		SystemProperty sysProp = SystemProperty.getInstance();
		sysProp.setProperty("swissmediumfont", swissmediumfont);
		sysProp.setProperty("swisslightfont", swisslightfont);
		sysProp.setProperty("swissthinfont", swissthinfont);
	}

	@Test
	public void checkDuplicateUWApplicationsVictTest() throws Exception {
		mockMvc.perform(post("/checkDuplicateUWApplications").param("fundCode", "VICT").param("lastName", "LASTVICT")
				.param("clientRefNo", "7").param("manageTypeCC", "CCOVER").param("dob", "1/11/1958")
				.param("firstName", "TESTVICT").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andReturn().getResponse().equals(false);

	}


	// Senario 1

	@Test
	public void eapplySubmitVict() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
		fileName = "eapplySubmitVict.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject.toString())).andExpect(status().isOk());

	}
	
	@Test
	public void eapplySubmitVictNoAura() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
		fileName = "eapplySubmitNoAuraVict.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject.toString())).andExpect(status().isOk());

	}

		

	@Test
	public void eapplySaveVict() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
		fileName = "eapplySubmitVict.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject.toString())).andExpect(status().isOk());

	}
	
	//unitized
	@Test
	public void eapplySubmitVictUnitized() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
		fileName = "eapplySubmitUnitsVict.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject.toString())).andExpect(status().isOk());

	}
	
	@Test
	public void eapplySubmitVictNoAuraUnitized() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
		fileName = "eapplySubmitNoAuraUnitsVict.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject.toString())).andExpect(status().isOk());

	}

		

	@Test
	public void eapplySaveVictUnitized() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
		fileName = "eapplySubmitUnitsVict.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject.toString())).andExpect(status().isOk());

	}


	// Senario 2

	@Test
	public void eapplySubmitTCoverVict() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitTCoverVict.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject.toString())).andExpect(status().isOk());

	}



	@Test
	public void eapplySaveTCoverVict() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitTCoverVict.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject.toString())).andExpect(status().isOk());

	}


	// Senario 3

	@Test
	public void eapplySubmitUWCoverVict() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitUWCoverVict.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject.toString())).andExpect(status().isOk());

	}


	@Test
	public void eapplySaveUWCoverVict() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitUWCoverVict.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject.toString())).andExpect(status().isOk());

	}


	// Senario 4

/*	@Test
	public void eapplySubmitCANCoverHost() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitCANCoverHost.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}


	@Test
	public void eapplySaveCANCoverHost() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitCANCoverHost.json";
		String testEapplyInputData = createEapplyInputTestData(fileName);
		mockMvc.perform(post("/saveEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(testEapplyInputData)).andExpect(status().isOk());

	}*/

	// Senario 5

	@Test
	public void eapplySubmitSCoverVict() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitSCoverVict.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject.toString())).andExpect(status().isOk());

	}

	// Senario 6

	@Test
	public void eapplySubmitICoverHost() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitICoverVict.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject.toString())).andExpect(status().isOk());
	}

	@Test
	public void eapplySaveICoverHost() throws Exception {
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5UUXlOVEV5TWl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKaFpUWm1NRE00WlMweE9HWTRMVFF6WmpNdE9URTRNaTB6WlRRd1l6SmhPR016TnpZaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5CYnRTLTNWSFp3STdJcmhxNjU1dFhLcGdpM2gyRy1pOU9vRWxTX1BOZkMw";
		fileName = "eapplySubmitICoverVict.json";
		JSONParser parser = new JSONParser();
		Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
		JSONObject jsonObject = (JSONObject) object;
		mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
				.content(jsonObject.toString())).andExpect(status().isOk());
	}


/*	@Test
	public void eapplyFileUpload() throws Exception { // D:\
		String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5qWXdPREEwT0N3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbFpUWXhPRGt4TnkwMFpqWTVMVFE1TUdJdFlqTXlaUzFqTWprNU9HTXdZbVppTWpnaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS43SW1ZODRJWXlrR3hobERTNXpOMF82VUtHbEZqYnBPZEIyOS1TRkZJeTQ4";
		File file = new File("D:\\uploadfile.jpg");
		FileInputStream fi1 = new FileInputStream(file);
		
		 * MockMultipartFile upload = new MockMultipartFile("upload",
		 * "Penguins.jpg", "image/jpeg",
		 * "C:\\Users\\Public\\Pictures\\Sample Pictures\\Penguins.jpg\\"
		 * .getBytes());
		 
		MockMultipartFile upload = new MockMultipartFile(file.getName(), fi1);
		mockMvc.perform(MockMvcRequestBuilders.fileUpload("/fileUpload").file(upload)
				.header("Content-Type", "uploadfile.jpg").header("Authorization", authId)).andExpect(status().is(200));
	}*/

	@Test
	public void eapplySubmitAura() throws Exception {
		String input = "{\"fund\":\"VICT\",\"mode\":\"change\",\"name\":\"TESTVICT LASTVICT\",\"age\":31,\"appnumber\":1522218696183,\"deathAmt\":772500,\"tpdAmt\":0,\"ipAmt\":0,\"waitingPeriod\":\"90 Days\",\"benefitPeriod\":\"2 Years\",\"memberType\":\"INDUSTRY OCCUPATION\",\"gender\":\"male\",\"clientname\":\"metaus\",\"industryOcc\":\"001:Business Analyst/Consultant\",\"country\":\"Australia\",\"salary\":\"200000\",\"fifteenHr\":\"Yes\",\"lastName\":\"LASTVICT\",\"firstName\":\"TESTVICT\",\"dob\":\"12/11/1986\",\"existingTerm\":true}";
		mockMvc.perform(post("/submitAura").contentType(MediaType.APPLICATION_JSON).content(input)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}



	

	@Test
	public void checkExpireApplication() throws Exception {
		mockMvc.perform(post("/expireApplication").param("applicationNumber", "1522216775758")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn().getResponse()
				.equals(true);

	}
	@Test
	public void checkExpireApplicationFalse() throws Exception {
		mockMvc.perform(post("/expireApplication").param("applicationNumber", "1522216775758234234234234234")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn().getResponse()
				.equals(false);

	}

	@Test
	public void getRetieveApps() throws Exception {
		mockMvc.perform(get("/retieveApps").param("fundCode", "VICT").param("clientRefNo", "7")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	// Senario new1

			/*@Test
			public void eapplySubmitHost1() throws Exception {
				String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
				fileName = "EapplyJson.json";
				String testEapplyInputData = createEapplyInputTestData(fileName);
				mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
						.content(testEapplyInputData)).andExpect(status().isOk());

			}

			// Senario new2 including document

			@Test
			public void eapplySubmitHost2() throws Exception {
				String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
				fileName = "EapplySubmitDoc.json";
				String testEapplyInputData = createEapplyInputTestData(fileName);
				mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
						.content(testEapplyInputData)).andExpect(status().isOk());

			}

			// Senario new2 including document but client no 7

			@Test
			public void eapplySubmitHost21() throws Exception {
				String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
				fileName = "EapplySubmitDoc7.json";
				String testEapplyInputData = createEapplyInputTestData(fileName);
				mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
						.content(testEapplyInputData)).andExpect(status().isOk());

			}

			// Senario new3 including document for UWCOVER
		
			@Test
			public void eapplySubmitHostCategory() throws Exception {
				String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
				fileName = "eapplyCategory.json";
				JSONParser parser = new JSONParser();
				Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
				JSONObject jsonObject = (JSONObject) object;
				System.out.println(jsonObject.toString());
				mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
						.content(jsonObject.toString())).andExpect(status().isOk());
		
			}
			
			@Test
			public void eapplySubmitCoverTypes() throws Exception {
				String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
				fileName = "eapplyCoverTypes.json";
				JSONParser parser = new JSONParser();
				Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
				JSONObject jsonObject = (JSONObject) object;
				System.out.println(jsonObject.toString());
				mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
						.content(jsonObject.toString())).andExpect(status().isOk());
		
			}
			
			@Test
			public void eapplySubmitRUW() throws Exception {
				String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
				fileName = "ruwEapplySubmit.json";
				JSONParser parser = new JSONParser();
				Object object = parser.parse(new FileReader(new ClassPathResource(fileName).getFile()));
				JSONObject jsonObject = (JSONObject) object;
				System.out.println(jsonObject.toString());
				mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
						.content(jsonObject.toString())).andExpect(status().isOk());
		
			}*/

			// Senario new3 including document for DCL

			/*@Test
			public void eapplySubmitHostUWCOVERDCL() throws Exception {
				String authId = "QmVhcmVyIGV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5leUpoZFdRaU9sc2ljM0J5YVc1bkxXSnZiM1F0WVhCd2JHbGpZWFJwYjI0aVhTd2ljMk52Y0dVaU9sc2ljbVZoWkNJc0luZHlhWFJsSWwwc0ltVjRjQ0k2TVRVeE5URTBNVE0zTnl3aVlYVjBhRzl5YVhScFpYTWlPbHNpVWs5TVJWOVVVbFZUVkVWRVgwTk1TVVZPVkNKZExDSnFkR2tpT2lKbU5qWTJNamc1T1MxaU1UYzNMVFEwTmpNdFlUTm1OUzA1WmpKak9XVTFObUl5WmpBaUxDSmpiR2xsYm5SZmFXUWlPaUowY25WemRHVmtMV0Z3Y0NKOS5PaUFIWnVsOURkaW9xamw4ZjBIRnhGNUdaUTVnOFBZZ3ZWT0VyMldFT3hJ";
				fileName = "EapplySubmitDCL.json";
				String testEapplyInputData = createEapplyInputTestData(fileName);
				mockMvc.perform(post("/submitEapply").header("Authorization", authId).contentType(MediaType.APPLICATION_JSON)
						.content(testEapplyInputData)).andExpect(status().isOk());

			}*/
			
}
