package au.com.metlife.eapply.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.metlife.eapplication.common.JSONException;
import com.metlife.eapplication.common.JSONObject;

import au.com.metlife.eapply.constants.EapplicationConstants;

public class EapplyHelper implements EapplicationConstants{

	private static final Logger log = LoggerFactory.getLogger(EapplyHelper.class);

	public static String callB2bDataService(String input, String baseurl, String token) {
        
		log.info("Calling B2bService with token sstart");
		String output = null;
		URL obj;
		try {
			obj = new URL(baseurl + "postb2bdata");
			token = new String(Base64.getEncoder().encode(("Bearer "+token).getBytes()));	
			input = input.trim()+"##"+token;
			log.info("input>>"+input);		
			HttpURLConnection httpCon = (HttpURLConnection) obj.openConnection();
			httpCon.setDoOutput(true);
			httpCon.setDoInput(true);
			httpCon.setUseCaches(false);
			
			
			httpCon.setRequestProperty(CONTENT_TYPE, APPLICATION_JSON);
			httpCon.setRequestProperty(ACCEPT, APPLICATION_JSON);
			httpCon.setRequestProperty(ACCESS_TOKEN, token);
			httpCon.setRequestProperty(AUTHORIZATION, token);						
			httpCon.setRequestMethod(POST);
			httpCon.connect();
			OutputStream os = httpCon.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, UTF_EIGHT);
			osw.write(input);
			osw.flush();
			osw.close();

			int responseCode = httpCon.getResponseCode();
			log.info("Response Code : " + responseCode);
			BufferedReader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
			String inputLine;
			StringBuffer responseBuff = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				log.info(inputLine);
				responseBuff.append(inputLine);
			}
			output = responseBuff.toString();
			in.close();

		} catch (MalformedURLException e) {
			log.error(MALFORMEDURL_ERR_INFO+e);
		} catch (ProtocolException e) {
			log.error(PROTOCOL_ERR_INFO+e);
		} catch (IOException e) {
			log.error(MALFORMED_URL_INFO+e);
		}
		log.info("Calling B2bService with token finish");
		return output;
	}

	public static String callB2bService(String input, String baseurl) {
		log.info("Calling B2bService without token start");
		
		String output = null;
		URL obj;
		JSONObject jsonObject = null;
		try {
			obj = new URL(baseurl + "oauth/token?grant_type=client_credentials&secureString="
					+ URLEncoder.encode(input, UTF_EIGHT));

			HttpURLConnection httpCon = (HttpURLConnection) obj.openConnection();
			httpCon.setDoOutput(true);
			httpCon.setDoInput(true);
			httpCon.setUseCaches(false);
			httpCon.setRequestProperty(CONTENT_TYPE, APPLICATION_JSON);
			httpCon.setRequestProperty(ACCEPT, APPLICATION_JSON);
			String encoded = Base64.getEncoder().encodeToString(("trusted-app:secret").getBytes(UTF_EIGHT));
			httpCon.setRequestProperty(AUTHORIZATION, "Basic " + encoded);

			httpCon.setRequestMethod(GET);
			httpCon.connect();
			OutputStream os = httpCon.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, UTF_EIGHT);
			osw.flush();
			osw.close();

			int responseCode = httpCon.getResponseCode();		
			BufferedReader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
			String inputLine;
			StringBuffer responseBuff = new StringBuffer();
			//input= input.replaceAll("\\s+", "");
			System.out.println("input before whitespace>>"+input);
			input = input.replaceAll("\n", "").replaceAll("\r", "");
			System.out.println("input after whitespace>>"+input);
			while ((inputLine = in.readLine()) != null) {				
				jsonObject = new JSONObject(inputLine);
				if(jsonObject.get(ACCESS_TOKEN)!=null){				
					output= callB2bDataService(input, baseurl, (String)jsonObject.get(ACCESS_TOKEN));
				}
				
				responseBuff.append(inputLine);
			}
			in.close();

		} catch (MalformedURLException e) {
			log.error(MALFORMEDURL_ERR_INFO+e);
		} catch (ProtocolException e) {
			log.error(PROTOCOL_ERR_INFO+e);
		} catch (IOException e) {
			log.error(MALFORMED_URL_INFO+e);
		} catch (JSONException e) {
			log.error("Json error"+e);
		}
		log.info("Calling B2bService without token finish");
		return output;
	}
	
	public static String callMemberSearchService(String memberid,String dob,String fundid, String baseurl, String input_identifier,String fundEmail) {
				
		String output = null;
		URL obj;
		
		String inputLine=null;		
		try {
			obj = new URL(baseurl);
			obj = new URL(baseurl+"?memberID="+memberid+"&fundID="+fundid+"&dob="+dob+"");
			HttpURLConnection httpCon = (HttpURLConnection) obj.openConnection();
			httpCon.setDoOutput(true);
			httpCon.setDoInput(true);
			httpCon.setUseCaches(false);
			httpCon.setRequestProperty(CONTENT_TYPE, APPLICATION_JSON);
			httpCon.setRequestProperty(ACCEPT, APPLICATION_JSON);
			httpCon.setRequestMethod(GET);			
			httpCon.connect();			
			int responseCode = httpCon.getResponseCode();
			log.info("responseCode>>"+responseCode);
			BufferedReader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
			
			StringBuffer responseBuff = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				log.info("inputLine>>"+inputLine);
				output =formMemberXML(memberid,dob,fundid,inputLine,input_identifier,fundEmail);		
				
			}
			in.close();

		} catch (MalformedURLException e) {
			log.error(MALFORMEDURL_ERR_INFO+e);
		} catch (ProtocolException e) {
			log.error(PROTOCOL_ERR_INFO+e);
		} catch (IOException e) {
			log.error(MALFORMED_URL_INFO+e);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("Calling B2bService without token finish");
		return output;
	}
	
	public static String updateb2bhoststring (String baseurl, String chordiantToken, String fundid){
		String output = null;
		URL obj;
		
		String inputLine=null;		
		try {
			obj = new URL(baseurl);
			obj = new URL(baseurl+"gethostdata?tokenid="+chordiantToken);
			HttpURLConnection httpCon = (HttpURLConnection) obj.openConnection();
			httpCon.setDoOutput(true);
			httpCon.setDoInput(true);
			httpCon.setUseCaches(false);
			httpCon.setRequestProperty(CONTENT_TYPE, APPLICATION_JSON);
			httpCon.setRequestProperty(ACCEPT, APPLICATION_JSON);
			httpCon.setRequestMethod(GET);			
			httpCon.connect();			
			int responseCode = httpCon.getResponseCode();
			log.info("responseCode>>"+responseCode);
			BufferedReader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
			
			StringBuffer responseBuff = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				log.info("inputLine>>"+inputLine);		
				System.out.println("inputLine>>" +inputLine);
				responseBuff.append(inputLine);
			}
			output = responseBuff.toString();			
			
			 if("HOST".equalsIgnoreCase(fundid)){
					String age = null;			
					System.out.println("Outoutsafd>>>"+output);	
					
					if(null != output && output.contains("<dateOfBirth>")){
						age = ""+EapplyHelper.getAgeNextBirthDay(output.substring(output.indexOf("<dateOfBirth>")+13, output.indexOf("</dateOfBirth>")));	
					}
						
					
					StringBuffer daa = new StringBuffer();
					daa.append("<ageCover>");
					daa.append(age);
					daa.append("</ageCover>");
					
					output = output.replace("</adminPartnerID>", "</adminPartnerID>"+daa);
				}
		        
		        if(output.contains(DAYS30)){			
		        	output = output.replace(DAYS30, THIRTY_DAYS);
				}else if(output.contains(DAYS45)){
					output = output.replace(DAYS45, FOURTYFIVE_DAYS);
				}else if(output.contains(DAYS60)){
					output = output.replace(DAYS60, SIXTY_DAYS);
				}else if(output.contains(DAYS90)){
					output = output.replace(DAYS90, NINETY_DAYS);
				}
				if(output.contains(YEARS2)){
					output = output.replace(YEARS2, TWO_YEARS);
				}else if(output.contains(YEARS5)){
					output = output.replace(YEARS5, FIVE_YEARS);
				}else if(output.contains(AGE65)){
					output = output.replace(AGE65, AGE_SIXTYFIVE);
				}else if(output.contains(AGE67)){
					output = output.replace(AGE67, AGE_SIXTYSEVEN);
				}
			output= callB2bService(output,baseurl);
			in.close();

		} catch (MalformedURLException e) {
			log.error(MALFORMEDURL_ERR_INFO+e);
		} catch (ProtocolException e) {
			log.error(PROTOCOL_ERR_INFO+e);
		} catch (IOException e) {
			log.error(MALFORMED_URL_INFO+e);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("Calling B2bService without token finish");
		return output;
	}
	
	public static String formMemberXML(String memberid,String dob,String fundid, String inputJson, String input_identifier,String fundEmail) throws JSONException{
		JSONObject jsonObject = null;
		String firstName = null;
		String dateJoinedCompany = null;
		String surname = null;
		String dateJoinedFund = null;
		String totalDeathUnits = null;
		String totalDeathAmount = null;
		String deathCategory = "Standard";		
		String tpdCategory = "Standard";
		String ipCategory = "Standard";		
		String totalTpdUnits = null;
		String totalTpdAmount = null;
		String totalIpUnits = null;
		String totalIpAmount = null;		
		String ipWaitingPeriod = null;
		String ipBenefitPeriod = null;
		String inputFileName = null;
		
		String gender = null;
		String line1 = null;		
		String line2 = null;
		String suburb = null;		
		String state = null;
		String postCode = null;		
		String country = null;
		String homePhone = null;
		String workPhone = null;
		String mobilePhone = null;	
		
		String deathType = "2";
		String tpdType = "2";
		String ipType = "2";
		
		
		//String json= "{"pk":0,"memberNumber":null,"dob":"1975-03-06","address_LINE_1":"Leve 1 Park Street","address_LINE_2":"","death_CATEGORY":"","date_JOINED_FUND":"1999-01-07","ip_WAITING_PERIOD":"","total_TPD_COVER":"0.00","date_JOINED_CO":"01/01/2000","total_DEATH_COVER":"0.00","total_IP_UNITS":"0","ip_BENEFIT_PERIOD":"","total_IP_COVER":"0.00","total_DEATH_UNITS":"0","input_FILE_NAME":"FIRS_201312021600.csv","total_TPD_UNITS":"0","first_NAME":"Fname38","country":"","home_PHONE":"292661200","work_PHONE":"","timestamp":null,"suburb":"Sydney","state":"NSW","gender":"","post_CODE":"2000","surname":"Sname38","tpd_CATEGORY":"","ip_CATEGORY":"","mobile":""}";
		jsonObject = new JSONObject(inputJson);	
		if(jsonObject.get("input_FILE_NAME")!=null){
			inputFileName = (String)jsonObject.get("input_FILE_NAME");
		}
		if(jsonObject.get("first_NAME")!=null){
			firstName = (String)jsonObject.get("first_NAME");
		}
		if(jsonObject.get("surname")!=null){
			surname = (String)jsonObject.get("surname");
		}
		if(jsonObject.get("date_JOINED_CO")!=null){
			dateJoinedCompany = (String)jsonObject.get("date_JOINED_CO");
		}
		if(jsonObject.get("date_JOINED_FUND")!=null){
			dateJoinedFund = (String)jsonObject.get("date_JOINED_FUND");
		}
		if(jsonObject.get("total_DEATH_UNITS")!=null){
			totalDeathUnits = (String)jsonObject.get("total_DEATH_UNITS");
		}
		if(jsonObject.get("total_DEATH_COVER")!=null){
			totalDeathAmount = (String)jsonObject.get("total_DEATH_COVER");
		}
		if(jsonObject.get("death_CATEGORY")!=null && !("").equalsIgnoreCase(jsonObject.get("death_CATEGORY").toString())){
			deathCategory = (String)jsonObject.get("death_CATEGORY");
		}
		if( !jsonObject.isNull("tpd_CATEGORY") && !("").equalsIgnoreCase(jsonObject.get("tpd_CATEGORY").toString())){
			tpdCategory = (String)jsonObject.get("tpd_CATEGORY");
		}
		if(jsonObject.get("ip_CATEGORY")!=null && !("").equalsIgnoreCase(jsonObject.get("ip_CATEGORY").toString() )){
			ipCategory = (String)jsonObject.get("ip_CATEGORY");
		}		
		if(jsonObject.get("total_TPD_UNITS")!=null){
			totalTpdUnits = (String)jsonObject.get("total_TPD_UNITS");
		}
		if(jsonObject.get("total_TPD_COVER")!=null){
			totalTpdAmount = (String)jsonObject.get("total_TPD_COVER");
		}		
		if(jsonObject.get("total_IP_UNITS")!=null){
			totalIpUnits = (String)jsonObject.get("total_IP_UNITS");
		}
		if(jsonObject.get("total_IP_COVER")!=null){
			totalIpAmount = (String)jsonObject.get("total_IP_COVER");
		}		
		if(jsonObject.get("ip_WAITING_PERIOD")!=null){
			ipWaitingPeriod = (String)jsonObject.get("ip_WAITING_PERIOD");
		}
		if(jsonObject.get("ip_BENEFIT_PERIOD")!=null){
			ipBenefitPeriod = (String)jsonObject.get("ip_BENEFIT_PERIOD");
		}	
		
		if(jsonObject.get("gender")!=null){
			gender = (String)jsonObject.get("gender");
		}
		if(jsonObject.get("address_LINE_1")!=null){
			line1 = (String)jsonObject.get("address_LINE_1");
		}
		if(jsonObject.get("address_LINE_2")!=null){
			line2 = (String)jsonObject.get("address_LINE_2");
		}
		if(jsonObject.get("suburb")!=null){
			suburb = (String)jsonObject.get("suburb");
		}		
		if(jsonObject.get("post_CODE")!=null){
			postCode = (String)jsonObject.get("post_CODE");
		}
		if(jsonObject.get("country")!=null){
			country = (String)jsonObject.get("country");
		}		
		if(jsonObject.get("home_PHONE")!=null){
			homePhone = (String)jsonObject.get("home_PHONE");
		}
		if(jsonObject.get("work_PHONE")!=null){
			workPhone = (String)jsonObject.get("work_PHONE");
		}		
		if(jsonObject.get("mobile")!=null){
			mobilePhone = (String)jsonObject.get("mobile");
		}
		if(jsonObject.get("ip_BENEFIT_PERIOD")!=null){
			ipBenefitPeriod = (String)jsonObject.get("ip_BENEFIT_PERIOD");
		}
		if(jsonObject.get("state")!=null){
			state = (String)jsonObject.get("state");
		}
		
		if(totalDeathUnits!=null && totalDeathUnits.trim().length()>0 && Integer.parseInt(totalDeathUnits)>0){
			deathType = "1";
		}
		if(totalTpdUnits!=null && totalTpdUnits.trim().length()>0 && Integer.parseInt(totalTpdUnits)>0){
			tpdType = "1";
		}
		if(totalIpUnits!=null && totalIpUnits.trim().length()>0 && Integer.parseInt(totalIpUnits)>0){
			ipType = "1";
		}
		
		String inputXML =	"<request>"+
				"<adminPartnerID>"+input_identifier+"</adminPartnerID>"+
				"<transRefGUID>84d55a38-6831-4b09-af5b-4b3cdb177657</transRefGUID>"+
				"<transType>2</transType>"+
				"<transDate>01/02/2011</transDate>"+
				"<transTime>02:14:04</transTime>"+
				"<fundID>"+fundid+"</fundID>"+
				"<fundEmail>"+fundEmail+"</fundEmail>"+
				"<policy>"+
				"<lineOfBusiness>Group</lineOfBusiness>"+
				"<partnerID>"+fundid+"</partnerID>"+
				"<applicant>"+
				"<dateJoined>"+dateFormatter(dateJoinedFund)+"</dateJoined>"+				
				"<clientRefNumber>"+memberid+"</clientRefNumber>"+
				"<applicantRole>1</applicantRole>"+
				"<memberType>Employed</memberType>"+
				"<personalDetails>"+
				"<firstName>"+firstName+"</firstName>"+
				"<lastName>"+surname+"</lastName>"+
				"<dateOfBirth>"+dob+"</dateOfBirth>"+
				"<gender>"+gender+"</gender>"+
				"<applicantSubType></applicantSubType>"+
				"<smoker>2</smoker>"+
				"<title></title>"+
				"<priority>1</priority>"+
				"</personalDetails>"+
				"<existingCovers>"+
				"<cover>"+
				"<occRating>"+deathCategory+"</occRating>"+
				"<benefitType>1</benefitType>"+
				"<type>"+deathType+"</type>"+				
				"<units>"+totalDeathUnits+"</units>"+
				"<amount>"+totalDeathAmount+"</amount>"+
				"<loading></loading>"+
				"<exclusions></exclusions>"+
				"</cover>"+
				"<cover>"+
				"<occRating>"+tpdCategory+"</occRating>"+
				"<benefitType>2</benefitType>"+
				"<type>"+tpdType+"</type>"+				
				"<units>"+totalTpdUnits+"</units>"+
				"<amount>"+totalTpdAmount+"</amount>"+
				"<loading></loading>"+
				"<exclusions></exclusions>"+
				"</cover>"+
				"<cover>"+
				"<occRating>"+ipCategory+"</occRating>"+
				"<benefitType>4</benefitType>"+
				"<type>"+ipType+"</type>"+				
				"<units>"+totalIpUnits+"</units>"+
				"<amount>"+totalIpAmount+"</amount>"+
				"<loading></loading>"+
				"<exclusions></exclusions>"+
				"<waitingPeriod>"+ipWaitingPeriod+"</waitingPeriod>"+
				"<benefitPeriod>"+ipBenefitPeriod+"</benefitPeriod>"+
				"</cover>"+
				"</existingCovers>"+
				"<address>"+
				"<addressType>2</addressType>"+
				"<line1>"+line1+"</line1>"+
				"<line2>"+line2+"</line2>"+
				"<suburb>"+suburb+"</suburb>"+
				"<state>"+state+"</state>"+
				"<postCode>"+postCode+"</postCode>"+
				"<country>"+country+"</country>"+
				"</address>"+
				"<contactDetails>"+
				"<emailAddress></emailAddress>"+
				"<mobilePhone>"+mobilePhone+"</mobilePhone>"+
				"<homePhone>"+homePhone+"</homePhone>"+
				"<workPhone>"+workPhone+"</workPhone>"+
				"<prefContact>1</prefContact>"+
				"<prefContactTime>1</prefContactTime>"+
				"</contactDetails>"+
				"</applicant>"+
				"</policy>"+
				"</request>";
		log.info("inputXML>>"+inputXML);	
		return inputXML;
	}
	public static String formNonMemberXML(String memberid,String dob,String fundid,String input_identifier,String firstName,String surName){
		//MTAA Change Starts
		String fundIdStr = null;
		String occRatStr = null;
		String adminPartner = null;
		String partnerId = null;
		if(fundid!= null && ("FIRS").equalsIgnoreCase(fundid)) {
			fundIdStr = "<fundID>FIRS</fundID>";
			occRatStr = OCCRATING_XML;
			adminPartner = "<adminPartnerID>FIRS</adminPartnerID>";
			partnerId = "<partnerID>FIRS</partnerID>";
		}else if(fundid!= null && ("GUIL").equalsIgnoreCase(fundid)) {
			fundIdStr = "<fundID>GUIL</fundID>";
			occRatStr = OCCRATING_XML_GUIL;
			adminPartner = "<adminPartnerID>INAN</adminPartnerID>";
			partnerId = "<partnerID>GUIL</partnerID>";
		}
		//MTAA Change Ends
		   String inputXmlForNvm =	"<request>"+
				   					adminPartner+
									"<transRefGUID>84d55a38-6831-4b09-af5b-4b3cdb177657</transRefGUID>"+
									"<transType>2</transType>"+
									"<transDate>01/02/2011</transDate>"+
									"<transTime>02:14:04</transTime>"+
									fundIdStr+
									"<fundEmail>test@test.com</fundEmail>"+
									"<policy>"+
									"<lineOfBusiness>Group</lineOfBusiness>"+
									partnerId+
									"<applicant>"+
									"<dateJoined>01/02/2011</dateJoined>"+				
									"<clientRefNumber>"+memberid+"</clientRefNumber>"+
									"<applicantRole>1</applicantRole>"+
									"<memberType>Employed</memberType>"+
									"<personalDetails>"+
									"<firstName>"+firstName+"</firstName>"+
									"<lastName>"+surName+"</lastName>"+
									"<dateOfBirth>"+dob+"</dateOfBirth>"+
									"<gender></gender>"+
									"<applicantSubType></applicantSubType>"+
									"<smoker>2</smoker>"+
									"<title></title>"+
									"<priority>1</priority>"+
									"</personalDetails>"+
									"<existingCovers>"+
									"<cover>"+
									occRatStr+
									"<benefitType>1</benefitType>"+
									TYPE_XML+				
									UNITS_XML+
									AMOUNT_XML+
									"<loading></loading>"+
									"<exclusions></exclusions>"+
									"</cover>"+
									"<cover>"+
									OCCRATING_XML+
									"<benefitType>2</benefitType>"+
									TYPE_XML+				
									UNITS_XML+
									AMOUNT_XML+
									"<loading></loading>"+
									"<exclusions></exclusions>"+
									"</cover>"+
									"<cover>"+
									OCCRATING_XML+
									"<benefitType>4</benefitType>"+
									TYPE_XML+				
									UNITS_XML+
									AMOUNT_XML+
									"<loading></loading>"+
									"<exclusions></exclusions>"+
									"<waitingPeriod>90 Days</waitingPeriod>"+
									"<benefitPeriod>2 Years</benefitPeriod>"+
									"</cover>"+
									"</existingCovers>"+
									"<address>"+
									"<addressType>2</addressType>"+
									"<line1></line1>"+
									"<line2></line2>"+
									"<suburb>Sydney</suburb>"+
									"<state>NSW</state>"+
									"<postCode>2000</postCode>"+
									"<country>Australia</country>"+
									"</address>"+
									"<contactDetails>"+
									"<emailAddress></emailAddress>"+
									"<mobilePhone></mobilePhone>"+
									"<homePhone></homePhone>"+
									"<workPhone></workPhone>"+
									"<prefContact>1</prefContact>"+
									"<prefContactTime>1</prefContactTime>"+
									"</contactDetails>"+
									"</applicant>"+
									"</policy>"+
									"</request>";
			return inputXmlForNvm;
	}
	
	public static String dateFormatter(String dateString){
		
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date date= df1.parse(dateString);
			dateString = df.format(date);
			
		}catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dateString;
	}
	
	public static int getAgeNextBirthDay(String dob) throws ParseException{
		String METHOD_NAME = "getAgeNextBirthDay";
		
		int age = 0;
		Calendar birth=null;
		Calendar today=null;
		Date birthDate=null;
		Date currentDate=null;
	
			if(dob!=null && !dob.equalsIgnoreCase("")){
			      birth = new GregorianCalendar();
			      today = new GregorianCalendar();
			      
			      SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy");
			      s.setLenient(false);
			      birthDate =  s.parse(dob);
			      
			      
			      currentDate = new Date(); //current date
			      if(birth!=null && today!=null && birthDate!=null && currentDate!=null){
				      birth.setTime(birthDate);
				      today.setTime(currentDate);
				      age = today.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
				     if(today.get(Calendar.DAY_OF_MONTH) == birth.get(Calendar.DAY_OF_MONTH) && today.get(Calendar.MONTH) == birth.get(Calendar.MONTH)){
				    	  age = age+1;
				    	  
				      }else if(today.get(Calendar.MONTH) < birth.get(Calendar.MONTH)){
				    	  age = age;
				     }else if (today.get(Calendar.MONTH) == birth.get(Calendar.MONTH) && today.get(Calendar.DAY_OF_MONTH) < birth.get(Calendar.DAY_OF_MONTH)) {
						
					}else{
				    	   age=age+1;
				    }
			      }
			}		
		
		return age;
	}
	/****
     * This method is used to Null Check a String Value and to avoid Null
     * pointer exception being thrown on runtime
     * 
     * @param strValue
     * @return
     */
    public static boolean isNullOrEmpty(final String strValue) {
      boolean isNull = false;
      if (strValue == null || strValue.length() == 0) {
        isNull = true;
      }
      return isNull;
    } 

    public static String checkSavedAppNonMem(String clientRefId, String baseurl, String fundId) {
    	log.info("Calling eApplyserviceCont checkSaveAppsNonMem");
	String output = null;
	URL obj;
	try {
		obj = new URL(baseurl + "checkSaveAppsNonMem"+"?fundCode="+fundId+"&clientRefNo="+clientRefId);	
		HttpURLConnection httpCon = (HttpURLConnection) obj.openConnection();
		httpCon.setDoOutput(true);
		httpCon.setDoInput(true);
		httpCon.setUseCaches(false);
		httpCon.setRequestProperty(CONTENT_TYPE, APPLICATION_JSON);
		httpCon.setRequestProperty(ACCEPT, APPLICATION_JSON);
		httpCon.setRequestMethod(GET);			
		httpCon.connect();			
		int responseCode = httpCon.getResponseCode();
		log.info("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
		String inputLine;
		StringBuffer responseBuff = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			log.info(inputLine);
			responseBuff.append(inputLine);
		}
		output = responseBuff.toString();
		in.close();

	} catch (MalformedURLException e) {
		log.error(MALFORMEDURL_ERR_INFO+e);
	} catch (ProtocolException e) {
		log.error(PROTOCOL_ERR_INFO+e);
	} catch (IOException e) {
		log.error(MALFORMED_URL_INFO+e);
	}
	log.info("Calling eApplyserviceCont checkSaveAppsNonMem");
	return output;}

}
