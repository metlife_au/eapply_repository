package au.com.metlife.eapply.bs.model;

import java.io.Serializable;

public class CoverInfoIns implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String occupationRating;

	public String getOccupationRating() {
		return occupationRating;
	}

	public void setOccupationRating(String occupationRating) {
		this.occupationRating = occupationRating;
	}

}
