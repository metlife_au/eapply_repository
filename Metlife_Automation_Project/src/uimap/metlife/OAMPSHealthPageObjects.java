package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class OAMPSHealthPageObjects extends PageObjectLibrary{
		
	public static final By txtheight = getObjectIdentifaction("OAMPS.health.height.text.id");
	public static final By txtweight = getObjectIdentifaction("OAMPS.health.weight.text.id");
	public static final By chklast3yrs = getObjectIdentifaction("OAMPS.health.3yrs.none.chkbox.css");
	public static final By chklast5yrs = getObjectIdentifaction("OAMPS.health.5yrs.none.chkbox.css");
	public static final By chkmedicaladvice = getObjectIdentifaction("OAMPS.health.diagnosed.none.chkbox.css");
	
	// exclusion
	public static final By chkmedicaladvicebone = getObjectIdentifaction("OAMPS.health.abturhealth.medicaladvice.bone.checkbox.css");
	public static final By chkmedicaladvicelossbone = getObjectIdentifaction("OAMPS.health.abturhealth.medicaladvice.lossbone.checkbox.css");
	public static final By rdadvicepractitionerYes= getObjectIdentifaction("OAMPS.health.abturhealth.medicaladvice.yes.radio.css");
	public static final By rdfracturesNo= getObjectIdentifaction("OAMPS.health.abturhealth.medicaladvice.no.radio.css");
	
	// loading
	public static final By chklast3yrslungissue = getObjectIdentifaction("OAMPS.health.3yrs.lung.chkbox.css");
	public static final By chklast3yrslungissueasthma = getObjectIdentifaction("OAMPS.health.3yrs.lung.asthma.chkbox.css");
	public static final By rdasthmaModerate= getObjectIdentifaction("OAMPS.health.3yrs.lung.asthma.condition.radio.css");
	public static final By rdasthmaworsenedNo= getObjectIdentifaction("OAMPS.health.3yrs.lung.asthma.worsened.radio.css");
	
	public static final By chkfamilyhistoryNo = getObjectIdentifaction("OAMPS.health.familyhistory.no.chkbox.css");
	public static final By chklifestyleNo = getObjectIdentifaction("OAMPS.health.lifestyle.no.chkbox.css");
	
	public static final By rdPregnantYes = getObjectIdentifaction("OAMPS.health.pregnant.yes.radio.css");
	public static final By rdPregnantNo = getObjectIdentifaction("OAMPS.health.pregnant.no.radio.css");
	
	public static final By txtalchohol = getObjectIdentifaction("OAMPS.health.alchohol.txt.id");
	public static final By btnEnter= getObjectIdentifaction("OAMPS.health.enter.btn.css");
	public static final By rdalchoholreduceNo = getObjectIdentifaction("OAMPS.health.alchohol.reduce.no.radio.css");
	public static final By rdhivNo = getObjectIdentifaction("OAMPS.health.hiv.no.radio.css");
	public static final By rdhivYes = getObjectIdentifaction("OAMPS.health.hiv.yes.radio.css");
	public static final By rdhivriskNo = getObjectIdentifaction("OAMPS.health.hiv.risk.no.radio.css");
	
	public static final By rdgeneralNo = getObjectIdentifaction("OAMPS.health.general.no.radio.css");
	public static final By rdinsuranceexclusionNo = getObjectIdentifaction("OAMPS.health.lifevent.no.radio.css");
	public static final By rdinsuranceclaimNo = getObjectIdentifaction("OAMPS.health.contemplating.no.radio.css");
	public static final By btnNext = getObjectIdentifaction("OAMPS.health.next.btn.css");
//	public static final By btnsummaryNext = getObjectIdentifaction("stft.summary.next.button.css");
}
