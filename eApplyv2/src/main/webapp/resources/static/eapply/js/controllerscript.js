// create the module and name it scotchApp
        // also include ngRoute for all our routing needs
    var scotchApp = angular.module('eapplymain', ['ngRoute']);

    // configure our routes
   scotchApp.config(function($routeProvider) {
        $routeProvider         

            .when('/', {
                templateUrl : 'eapply/eapplylogin.html',
                controller  : 'login'
            })
            .when('/landing', {
                templateUrl : 'eapply/landingpage.html',
                controller  : 'landing'
            })
            .when('/quote', {
                templateUrl : 'eapply/coverDetails_changeCover.html',
                controller  : 'quote'
            })
            .when('/aura', {
                templateUrl : 'eapply/aura.html',
                controller  : 'aura'
            })
            .when('/summary', {
                templateUrl : 'eapply/confirmation.html',
                controller  : 'summary'
            })
            .when('/descision', {
                templateUrl : 'eapply/decision.html',
                controller  : 'decision'
            })
             .when('/requirements/:clm', {
                templateUrl : 'claimtracker/events.html',
                controller  : 'requirements'
            })
         

            
    });
    
    scotchApp.controller('login',['$scope', '$location',  function($scope, $location){
        $scope.message = 'Look! I am an about page.';
        //$scope.claimNo = claimNo
        $scope.go = function ( path ) {
        	//claimNumService.setClaimNumber($scope.claimNo);
      	  $location.path( path );
      	};
    }]); 
    
    scotchApp.controller('landing',['$scope', '$location',  function($scope, $location){
        $scope.message = 'Look! I am an about page.';
        //$scope.claimNo = claimNo
        $scope.go = function ( path ) {
        	//claimNumService.setClaimNumber($scope.claimNo);
      	  $location.path( path );
      	};
    }]); 
    scotchApp.controller('quote',['$scope', '$location',  function($scope, $location){
        $scope.message = 'Look! I am an about page.';
        //$scope.claimNo = claimNo
        $scope.go = function ( path ) {
        	//claimNumService.setClaimNumber($scope.claimNo);
      	  $location.path( path );
      	};
      	$scope.coltwo = false;
      	$scope.toggleTwo = function() {
            $scope.coltwo = !$scope.coltwo;           
        };
      	$scope.colthree = false;
      	$scope.toggleThree = function() {
            $scope.colthree = !$scope.colthree;           
        };
    }]); 
    
    scotchApp.controller('aura',['$scope', '$location',  function($scope, $location){
        $scope.message = 'Look! I am an about page.';
        //$scope.claimNo = claimNo
        $scope.go = function ( path ) {        	
      	  $location.path( path );
      	};
      	$scope.collapseOne = true;
      	$scope.toggleOne = function() {
            $scope.collapseOne = !$scope.collapseOne;          
        };
      	$scope.collapseTwo = false;
      	$scope.toggleTwo = function() {
            $scope.collapseTwo = !$scope.collapseTwo;            
        };
        $scope.collapseThree = false;
      	$scope.toggleThree = function() {
            $scope.collapseThree = !$scope.collapseThree;            
        };
        $scope.collapseFour = false;
      	$scope.toggleFour = function() {
            $scope.collapseFour = !$scope.collapseFour;            
        };
        $scope.collapseFive = false;
      	$scope.toggleFive = function() {
            $scope.collapseFive = !$scope.collapseFive;            
        };
    }]); 
    
    scotchApp.controller('summary',['$scope', '$location',  function($scope, $location){
        $scope.message = 'Look! I am an about page.';
        //$scope.claimNo = claimNo
        $scope.go = function ( path ) {
        	//claimNumService.setClaimNumber($scope.claimNo);
      	  $location.path( path );
      	};
      	$scope.collapse = false;
      	$scope.toggle = function() {
            $scope.collapse = !$scope.collapse;           
        };      	
    }]); 
    
    scotchApp.controller('decision',['$scope', '$location',  function($scope, $location){
        $scope.message = 'Look! I am an about page.';
        //$scope.claimNo = claimNo
        $scope.go = function ( path ) {
        	//claimNumService.setClaimNumber($scope.claimNo);
      	  $location.path( path );
      	};
      	
    }]); 
    
    /* scotchApp.controller('login',['$scope', '$location','claimNumService',  function($scope, $location,claimNumService){
        $scope.message = 'Look! I am an about page.';
        //$scope.claimNo = claimNo
        $scope.go = function ( path ) {
        	claimNumService.setClaimNumber($scope.claimNo);
      	  $location.path( path );
      	};
    }]); 
    
     scotchApp.controller('viewClaimStatus', ['$scope', '$location', 'claimNumService', 'getclaimList', 'getEvents',   function($scope, $location, claimNumService, getclaimList, getEvents){
        $scope.message = 'Look! I am an about page.';
        getclaimList.claimList().then(function(response) {    	
        	$scope.claimDataList = response.data;
        });
        getEvents.eventList().then(function(response) {    	
        	$scope.claimReqList = response.data;
        });
        $scope.go = function ( path ) {
        	  $location.path( path );
        	};
        	$scope.claimNo = claimNumService.getClaimNumber();
    }]);
    
    scotchApp.controller('requirements', ['$scope', '$location', '$routeParams', 'getEventsList', 'getStatusList', function($scope, $location, $routeParams, getEventsList ,getStatusList){
        $scope.message = 'Look! I am an about page.';
        console.log($routeParams.clm)    
         getEventsList.claimEventList().then(function(response) {    	
        	$scope.claimDataList = response.data;
        });
        getStatusList.claimStatusList().then(function(response) {    	
        	$scope.claimStatusList = response.data;
        });
        $scope.go = function ( path ) {
        	  $location.path( path );
        	};
        
    }]);
    
    scotchApp.service('claimNumService', function() {
    	var _self = this;
    	_self.setClaimNumber = function (tempClaim) {
    		_self.claimNumber = tempClaim;
      }
    	_self.getClaimNumber = function () {
    	  return _self.claimNumber;
      }
    });
   
    
    scotchApp.factory('getclaimList', function($http, $q,claimNumService){    
        var defer = $q.defer();
       
        var myFactObj = {};
        myFactObj.claimList = function () {
        	
    		$http.post('http://10.173.62.192:8083/cmsService/method24',{ claimNo: claimNumService.getClaimNumber()}).then(function(response) {			
    			defer.resolve(response);
    		}, function(response) {
    			defer.reject(response);
    		});
    	 	return defer.promise;
        }
        
        return myFactObj;
    });
    
    
    scotchApp.factory('getEvents', function($http, $q,claimNumService){    
        var defer = $q.defer();
       
        var myFactObj = {};
        myFactObj.eventList = function () {
        	
    		$http.post('http://10.173.62.192:8083/cmsService/method23',{ claimNo: claimNumService.getClaimNumber()}).then(function(response) {			
    			defer.resolve(response);
    		}, function(response) {
    			defer.reject(response);
    		});
    	 	return defer.promise;
        }
        
        return myFactObj;
    });
    
    scotchApp.factory('getEventsList', function($http, $q,$routeParams){    
        var defer = $q.defer();
       
        var myFactObj = {};
        myFactObj.claimEventList = function () {
        	
    		$http.post('http://10.173.62.192:8083/cmsService/method23',{ claimNo: $routeParams.clm}).then(function(response) {			
    			defer.resolve(response);
    		}, function(response) {
    			defer.reject(response);
    		});
    	 	return defer.promise;
        }
        
        return myFactObj;
    });
    
    scotchApp.factory('getStatusList', function($http, $q,$routeParams){    
        var defer = $q.defer();
       
        var myFactObj = {};
        myFactObj.claimStatusList = function () {
        	
    		$http.post('http://10.173.62.192:8083/cmsService/method22',{ claimNo: $routeParams.clm}).then(function(response) {			
    			defer.resolve(response);
    		}, function(response) {
    			defer.reject(response);
    		});
    	 	return defer.promise;
        }
        
        return myFactObj;
    });*/
   