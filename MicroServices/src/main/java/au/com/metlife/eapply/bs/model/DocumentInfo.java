package au.com.metlife.eapply.bs.model;

import java.io.Serializable;

public class DocumentInfo implements Serializable{

	/**
	 * Author 175373
	 */
	private static final long serialVersionUID = 1L;

	private String docLoc;
	private String docType;
	private String name;
	private boolean status;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDocLoc() {
		return docLoc;
	}
	public void setDocLoc(String docLoc) {
		this.docLoc = docLoc;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
}
