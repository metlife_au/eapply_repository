package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class YBRSummaryPageObjects extends PageObjectLibrary{
	
	public static final By linkTitle1 = getObjectIdentifaction("ybr.webresponse.summary.dropdown1.css");
	public static final By txtLastName1 = getObjectIdentifaction("ybr.webresponse.summary.lastname1.text.id");
	
	public static final By linkTitle2 = getObjectIdentifaction("ybr.webresponse.summary.dropdown2.css");
	public static final By txtLastName2 = getObjectIdentifaction("ybr.webresponse.summary.lastname2.text.id");
	
	public static final By chkacknowledge = getObjectIdentifaction("ybr.webresponse.summary.acknowledge.checkbox.id");
	
	public static final By rddebit = getObjectIdentifaction("ybr.payment.debit.radio.css");
	public static final By rdcredit = getObjectIdentifaction("ybr.payment.credit.radio.css");
	public static final By txtaccountname = getObjectIdentifaction("ybr.payment.accountname.text.id");
	public static final By txtaccountnumber = getObjectIdentifaction("ybr.payment.accountnumber.text.id");
	public static final By txtbsb = getObjectIdentifaction("ybr.payment.bsb.text.id");	
	public static final By chkauthorise = getObjectIdentifaction("ybr.payment.authorise.checkbox.id");
	public static final By btnsubmit = getObjectIdentifaction("ybr.payment.protectnow.btn.css");
	public static final By btnclosewindow = getObjectIdentifaction("ybr.payment.closewindow.btn.css");
	public static final By rdvisa = getObjectIdentifaction("ybr.payment.visa.radio.css");
	public static final By rdmaster = getObjectIdentifaction("ybr.payment.master.radio.css");
	public static final By rdamex = getObjectIdentifaction("ybr.payment.amex.radio.css");
	public static final By txtccnumber = getObjectIdentifaction("ybr.payment.ccnumber.text.id");
	public static final By txtccname = getObjectIdentifaction("ybr.payment.ccnname.text.id");
	public static final By selCCExpMonth = getObjectIdentifaction("ybr.payment.ccexpmonth.dropdown.css");
	public static final By selCcExpYr = getObjectIdentifaction("ybr.payment.ccexpyear.dropdown.css");
	public static final By textAppnum = getObjectIdentifaction("ybr.decision.Appnumber.text.xpath");
}
