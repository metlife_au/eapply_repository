package au.com.metlife.eapply.utility;

import java.io.IOException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;





public class RSAUtil {
	private static final Logger log = LoggerFactory.getLogger(RSAUtil.class);
	
	    /**
	     * Encrypt Data
	     * @param data
	     * @throws IOException
	     */
	    public static String encryptData(String data, String fundid) throws Exception {
	    	log.info("Encryption data start");
	    	String returnString=null;
	        byte[] dataToEncrypt = data.getBytes();

	        try {
	        	byte[] encryptedData = null;
	            PublicKey pubKey = readPublicKeyFromFile(fundid);
	            Cipher cipher = Cipher.getInstance("RSA");
	            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
	            encryptedData = cipher.doFinal(dataToEncrypt);
	            Base64 b=new Base64();
	            returnString=new String(b.encode(encryptedData));

	        } catch (Exception e) {
	            log.error("Error in encryption data"+e);
	        }

	        log.info("Encryption data finish");
	        return returnString;
	    }

	    /**
	     * Encrypt Data
	     * @param data
	     * @throws IOException
	     */
	    public static String decryptData(String data, String fundid) throws Exception {
	        log.info("Decryption data start");
	    	String returnString=null;	   
	        	Base64 b=new Base64();
	        	log.info("data>>"+data);
	        	 byte[] dataToDescrypt=b.decode(data.getBytes());
	            /*PrivateKey privateKey = readPrivateKeyFromFile(PRIVATE_KEY_FILE);*/
	        	PrivateKey privateKey = readPrivateKeyFromFile(fundid);
	            Cipher cipher = Cipher.getInstance("RSA");
	            cipher.init(Cipher.DECRYPT_MODE, privateKey);
	            byte[] descryptedData = cipher.doFinal(dataToDescrypt);
	            returnString=new String(descryptedData);      

	            log.info("Decryption data finish");
	        return returnString;

	    }

	    public static PublicKey readPublicKeyFromFile(String fundid) throws Exception{
	    	log.info("Read public key from file start");
	    	Base64 b=new Base64();
	    	SystemProperty sysprop=SystemProperty.getInstance();
	    	X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(b.decode(sysprop.getProperty("PUBLIC_KEY_"+fundid).getBytes()));/*later get this from DB*/
	        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
	        PublicKey pubKey =keyFactory.generatePublic(pubKeySpec);
	        log.info("Read public key from file finish");
	        return pubKey;
	    }



	    public static PrivateKey readPrivateKeyFromFile(String fundid) throws Exception{
	    	        log.info("Read private key from file start");
	    			Base64 b=new Base64();
	    	    	SystemProperty sysprop=SystemProperty.getInstance();
	    	    	PKCS8EncodedKeySpec pubKeySpec = new PKCS8EncodedKeySpec(b.decode(sysprop.getProperty("PRIVATE_KEY_"+fundid).getBytes()));/*later get this from DB*/
	    	    	//PKCS8EncodedKeySpec pubKeySpec = new PKCS8EncodedKeySpec(b.decode("MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCblG5zQshAYHQ+YdSaQn3ENeSCkjCzy/PKOQW87hup0bMymbVI0GyUj48NGu1kVsKW7J1o/QAZqeSbzK01Yr6PdU2SPUU3FqJv6KkFDsLNTP4zx9Qk0GNEMQn0qn0goNudytJ6JT6fvl+xI0MemN0uKu9hMWKv9TeRA/PDtoM85CONLMlv6ilG3jaZOXL/FQrw9wt4HtNcuzoBuUJAz1REDdxvagai6IPWuSTEyogkdQ/lMwNBB3A/Z77dRfMNQdi601FZvWg4vLKtcfoYDoRurL2sv3kGXfHjvxHzI17/WgfaBPl/odQJFJ/pRZybJfRORAauMDepVWBgm6TA6sqLAgMBAAECggEAL3zrCbuErEHEuZXa8wsAoHdivIgq3XaPf1ZQbg6v2vHrJLLOJt+XhPEc1P83Hr1sv+yRT46KDUtSQEwFQppVWCyR9OVGzz3VbxsxHB4ZT9kEboG8O1BwPExLoaa4ctEmt89E14uuxcuyIbuFrT0zpfftAVm1qcfLojP2n/KBtH/jzUO8539VXw8UOd8VhX5yvy6g8SXuv986Z4opmkkKFzxkMVmtezz1mG9xfCeI4zanokRVr+AbhVcHsy38FHd4Kp4MUEBA50AqlsQKZS3hAEOkDKrdTH8VznbYQ0DQqFgij39iYbfRpt3b6VX233CpahF8XlF/WHL7aNvQieAmoQKBgQDfeCV8ar91CaQnu55iolbrdPXr2FFBh//LQMJyWygKkeQu0V+5OQAYGm4IdpMMWyjfua5tRU6eGIsN06snbEivI75AwJh5AlldFQv531ijR8a6f4pQiupyIkiTPSJmEFMfU2dNBoEqgK6wehWDhy3Nu2Ct0upoi+Dmypv862BQpwKBgQCyOkzxPylpx1S8saR9mAVTKkm1gr3bD3kCqCE72b5eoQid7lVCWFGNz1+3KOLnm6RnItDpD8Pv0sadjqAT04DbwUkWWm33qkSY0jWPfG5W6YOE8E5EY4pnD/rJHTBF22lCAp+yf9tBybj7EJCwvDxtSHjkdBn4JPM4/IqrSx1vfQKBgQCa+e21TSqd9iO70mfTK5ZwEFsERq6Dbde9k5nGSOWJ/wdqWJW0FI22KY4pnGH6bbX5AgKvkSMmjQrSefr2iigWHgmxSl/ze+eRoCkADx6fjgWKDS1VEiHpKVF/myQh+CxNS0AWsO4fFcMQbHpDDxp61PoFUoJyIp/ELrrjOjE/fQKBgGAKsrO+CZ8K30rfn3hel1/9hcLeNVj6uYcI+v+5krCDrpUMfb1GwovcmAOncL7GTI2XYWR2Lmvp9UWqOfD1JJdHTQ9So7e2dUi03k/4Ca2kIE9e/44y5b4sCkSeCgla9xmvOEeERq7o+Vmxz4atqRJq31qchfIGV1r6G++6hp4xAoGBAM1jT7rX2NhPBIGvOAjf7+E4zdGgy0DuJf9iIQ1j/J0on5yEt59BlVPLm9f/E02UvJ4Ujm+KlKJOqBeQW++WM7gPP8V7bAHX3KLbVuM//XkFL9jtq1cgE2l+j4De8UdIhXsam+ZYzv63xznE9h8zhW8yqRN6FfrW+4ORe9nuFIaN".getBytes()));/*later get this from DB*/
	    	    	/*PKCS8EncodedKeySpec pubKeySpec = new PKCS8EncodedKeySpec(b.decode(dao.getPEK().getBytes()));*/ /*later get this from DB*/

	    	        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
	    	        PrivateKey pk =keyFactory.generatePrivate(pubKeySpec);
	    	        log.info("Read private key from file finish");
	    	    	return pk;
	    }	    
	    
}
