package pages.metlife;


import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;
import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.FundInputStringPageObjects;

public class SFPSNewInsurancePage extends MasterPage {
	WebDriverUtil driverUtil = null;

	public SFPSNewInsurancePage enterSFPSPayload() {
		fillAndCalculateSFPSQuote();
		return new SFPSNewInsurancePage(scriptHelper);
	}

	public SFPSNewInsurancePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		//sleep(1000);
		//switchToActiveWindow();
	}

	public void fillAndCalculateSFPSQuote() {
		String inputPayload = null;
		inputPayload = createInputPayload();
		System.out.println(inputPayload);
		String strInputFund = dataTable.getData("MetLife_Data", "inputIdentifier");

		try {
			tryAction(fluentWaitElement(FundInputStringPageObjects.txtInputString), "SET", "Input Payload",
					inputPayload);
			tryAction(fluentWaitElement(FundInputStringPageObjects.drpdownInputIdetifier), "DropDownSelect",
					"Input Identifier", strInputFund);
			tryAction(fluentWaitElement(FundInputStringPageObjects.btnPlainText), "SplClick", "PlainText");
			//tryAction(fluentWaitElement(FundInputStringPageObjects.btnPlainText),"alertOk","");
			WebDriverWait wait = new WebDriverWait(driver, 3);
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			
		System.out.println(driver.switchTo().alert().getText());
			alert.accept();
			tryAction(fluentWaitElement(FundInputStringPageObjects.btnNewApplication), "Click", "New Application");
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
		/*String firstName = dataTable.getData("MetLife_Data", "FirstName");
		String dob = dataTable.getData("MetLife_Data", "DateOfBirth");
		String gender = dataTable.getData("MetLife_Data", "Gender");
		String coveramt = dataTable.getData("MetLife_Data", "CoverAmount");
		String flybuysmemeber = dataTable.getData("MetLife_Data", "FlyBuysMemeber");
		String joinflybuys = dataTable.getData("MetLife_Data", "JoinFlyBuys");
		String phoneNumber = dataTable.getData("MetLife_Data", "PreferredNumber");
		String strState = dataTable.getData("MetLife_Data", "State");
		String strSmoke = dataTable.getData("MetLife_Data", "personal.smoked");
		String strCoverAmt = dataTable.getData("MetLife_Data", "CoverAmount");
		String strPaymentFreq = dataTable.getData("MetLife_Data", "PaymentFrequency");
		System.out.println("The Premium Date is :: " + getPremiumDOB(-3));
		try {
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.btnNewApplication), "Click", "New Application");
			switchToActiveWindow();
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.txtFirstName), "SET", "First Name", firstName);
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.txtFirstName), "TAB", "First Name");
			Thread.sleep(1000);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.txtPhoneNumber), "SET", "Phone Number", phoneNumber);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.txtPhoneNumber), "TAB", "Phone Number");
			Thread.sleep(1000);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.txtDOB), "SET", "Date of birth", RandomDateOfBirth());
			if (gender.equalsIgnoreCase("Male")) {
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdMale), "clkradio", "Gender -" + gender);
			} else {
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdFemale), "clkradio", "Gender -" + gender);
			}
			if (strSmoke.equalsIgnoreCase("YES")) {
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdSmokedIn12MthsYes), "Click",
						"Have you Smoked in 12 months - Yes");

			} else {
				tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdSmokedIn12MthsNo), "Click",
						"Have you Smoked in 12 months - No");
			}
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.btnState), "specialDropdown", "State", strState);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.selCoverAmt), "specialDropdown", "Cover Amount",
					strCoverAmt);

			Thread.sleep(1000);
			tryAction(fluentWaitElement(ColesGetQuotePageObjects.selFrequency), "specialDropdown", "Payment Frequency",
					strPaymentFreq);
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdFlyBuysYes), "Click",
					"Are you a flybuys member - Yes");
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdFlyBuysNo), "Click",
					"Are you a flybuys member - No");
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.rdFlyBuysJoinNo), "Click",
					"Would you like to join flybuys - No");
			sleep();
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(ColesGetQuotePageObjects.lnkCalculateQuoteId)).click().perform();
			tryAction(waitForClickableElement(ColesGetQuotePageObjects.lnkApply), "Click", "Apply");

		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}*/
	}

	public String createInputPayload()
	{
		String lodge_fund_email = dataTable.getData("MetLife_Data", "lodge_fund_email");
		String userid = dataTable.getData("MetLife_Data", "userid");
		String user_password = dataTable.getData("MetLife_Data", "user_password");
		String lodge_fund_id = dataTable.getData("MetLife_Data", "lodge_fund_id");
		String lodge_mem_fname = dataTable.getData("MetLife_Data", "lodge_mem_fname");
		String lodge_mem_lname = dataTable.getData("MetLife_Data", "lodge_mem_lname");
		String lodge_mem_dob = dataTable.getData("MetLife_Data", "lodge_mem_dob");
		String lodge_mem_gender = dataTable.getData("MetLife_Data", "lodge_mem_gender");
		String lodge_e_default_unit_death = dataTable.getData("MetLife_Data", "lodge_e_default_unit_death");
		String lodge_e_default_unit_tpd = dataTable.getData("MetLife_Data", "lodge_e_default_unit_tpd");
		String lodge_e_default_unit_ip = dataTable.getData("MetLife_Data", "lodge_e_default_unit_ip");
		String lodge_e_default_fixed_death = dataTable.getData("MetLife_Data", "lodge_e_default_fixed_death");
		String lodge_e_default_fixed_tpd = dataTable.getData("MetLife_Data", "lodge_e_default_fixed_tpd");
		String lodge_e_opt_unit_death_amount = dataTable.getData("MetLife_Data", "lodge_e_opt_unit_death_amount");
		String lodge_e_opt_unit_tpd_amount = dataTable.getData("MetLife_Data", "lodge_e_opt_unit_tpd_amount");
		String lodge_e_opt_fixed_death_amount = dataTable.getData("MetLife_Data", "lodge_e_opt_fixed_death_amount");
		String lodge_e_opt_fixed_tpd_amount = dataTable.getData("MetLife_Data", "lodge_e_opt_fixed_tpd_amount");
		String lodge_e_opt_fixed_ip_amount = dataTable.getData("MetLife_Data", "lodge_e_opt_fixed_ip_amount");
		String address_line_1 = dataTable.getData("MetLife_Data", "address_line_1");
		String country = dataTable.getData("MetLife_Data", "country");
		String lodge_e_unit_rating_category = dataTable.getData("MetLife_Data", "lodge_e_unit_rating_category");
		String lodge_e_ip_rating_category = dataTable.getData("MetLife_Data", "lodge_e_ip_rating_category");
		String lodge_e_fixed_rating_category = dataTable.getData("MetLife_Data", "lodge_e_fixed_rating_category");
		String lodge_e_wait_period = dataTable.getData("MetLife_Data", "lodge_e_wait_period");
		String lodge_e_ben_period = dataTable.getData("MetLife_Data", "lodge_e_ben_period");
		String lodge_member_type = dataTable.getData("MetLife_Data", "lodge_member_type");
		String priority_client = dataTable.getData("MetLife_Data", "priority_client");
		String lodge_date_joined  = dataTable.getData("MetLife_Data", "lodge_date_joined");
		
		StringBuilder inputStringPayload = null;
		inputStringPayload = new StringBuilder();
		inputStringPayload.append("lodge_fund_email=");
		inputStringPayload.append(lodge_fund_email);
		inputStringPayload.append("$#$");
		inputStringPayload.append("userid=");
		inputStringPayload.append(userid);
		inputStringPayload.append("$#$");
		inputStringPayload.append("user_password=");
		inputStringPayload.append(user_password);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_fund_id=");
		inputStringPayload.append(lodge_fund_id);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_mem_fname=");
		inputStringPayload.append(lodge_mem_fname);
		inputStringPayload.append(getMonthNameMMM());
		inputStringPayload.append(getRandomText(7));
		inputStringPayload.append(getWeekDayNameEEE());
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_mem_lname=");
		inputStringPayload.append(lodge_mem_lname);
		inputStringPayload.append(getMonthNameMMM());
		inputStringPayload.append(getRandomText(7));
		inputStringPayload.append(getWeekDayNameEEE());
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_mem_dob=");
		inputStringPayload.append(getPremiumDOB(Integer.parseInt("-"+lodge_mem_dob)));
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_mem_gender=");
		inputStringPayload.append(lodge_mem_gender);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_cl_ref_no=");
		inputStringPayload.append(getRandomAlphaNumeric(8));
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_e_default_unit_death=");
		inputStringPayload.append(lodge_e_default_unit_death);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_e_default_unit_tpd=");
		inputStringPayload.append(lodge_e_default_unit_tpd);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_e_default_unit_ip=");
		inputStringPayload.append(lodge_e_default_unit_ip);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_e_default_fixed_death=");
		inputStringPayload.append(lodge_e_default_fixed_death);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_e_default_fixed_tpd=");
		inputStringPayload.append(lodge_e_default_fixed_tpd);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_e_opt_unit_death_amount=");
		inputStringPayload.append(lodge_e_opt_unit_death_amount);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_e_opt_unit_tpd_amount=");
		inputStringPayload.append(lodge_e_opt_unit_tpd_amount);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_e_opt_fixed_death_amount=");
		inputStringPayload.append(lodge_e_opt_fixed_death_amount);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_e_opt_fixed_tpd_amount=");
		inputStringPayload.append(lodge_e_opt_fixed_tpd_amount);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_e_opt_fixed_ip_amount=");
		inputStringPayload.append(lodge_e_opt_fixed_ip_amount);
		inputStringPayload.append("$#$");
		inputStringPayload.append("address_line_1=");
		inputStringPayload.append(address_line_1);
		inputStringPayload.append("$#$");
		inputStringPayload.append("country=");
		inputStringPayload.append(country);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_e_unit_rating_category=");
		inputStringPayload.append(lodge_e_unit_rating_category);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_e_ip_rating_category=");
		inputStringPayload.append(lodge_e_ip_rating_category);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_e_fixed_rating_category=");
		inputStringPayload.append(lodge_e_fixed_rating_category);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_e_wait_period=");
		inputStringPayload.append(lodge_e_wait_period);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_e_ben_period=");
		inputStringPayload.append(lodge_e_ben_period);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_member_type=");
		inputStringPayload.append(lodge_member_type);
		inputStringPayload.append("$#$");
		inputStringPayload.append("priority_client=");
		inputStringPayload.append(priority_client);
		inputStringPayload.append("$#$");
		inputStringPayload.append("lodge_date_joined=");
		inputStringPayload.append(lodge_date_joined);
		//System.out.println("The Payload is :: "+inputStringPayload.toString());
		return inputStringPayload.toString();
	}
}
