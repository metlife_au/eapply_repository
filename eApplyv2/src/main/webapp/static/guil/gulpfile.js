var gulp = require('gulp');
var del = require('del');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var ngAnnotate = require('gulp-ng-annotate');
var jshint = require('gulp-jshint');
var connect = require('gulp-connect');
var gulpDocs = require('gulp-ngdocs');

gulp.task('clean', function (cb) {
  return del([
    // everything inside the dist folder
    './dist/**/*'
  ], cb);
});

gulp.task('lint', function() {
  return gulp.src([
    'src/app/app.module.config.js',
    'src/app/app.module.js',
    'src/app/app.module.routes.js',
    'src/app/appDataModel.js',
    'src/app/modules/**/*.js',
    'src/app/services/**/*.js', 
    'src/app/utils/**/*.js', 
  ])
    .pipe(jshint())
    .pipe(jshint.reporter('default', { verbose: true }))
    .pipe(jshint.reporter('fail'));
});

gulp.task('lib-script-minify', function () {
  gulp.src([
    '../../resources/js/lib/file-upload/ng-file-upload-shim.js',
    '../../resources/js/lib/angular.min.js',
    '../../resources/js/lib/angular-sanitize.js',
    '../../resources/js/lib/angular-idle.min.js',
    '../../resources/js/lib/angular-ui-router.min.js',
    '../../resources/js/lib/jQuery/jquery.min.js',
    '../../resources/js/lib/bootstrap/bootstrap.min.js',
    '../../resources/js/lib/file-upload/ng-file-upload.js',
    '../../resources/js/lib/ngDialog.js',
    '../../resources/js/lib/utils/moment.js',
    '../../static/common/services/eApplyInterceptor.js'
  ])
    .pipe(sourcemaps.init())
    .pipe(concat('lib.min.js'))
    .pipe(ngAnnotate())
    .pipe(uglify({output: {ascii_only:true}}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist'));
});

gulp.task('app-script-minify', function () {
  gulp.src([    
    'src/app/app.module.js',
    'src/app/app.module.config.js',
    'src/app/app.module.routes.js',
    'src/app/appDataModel.js',
    
    '../../static/common/services/restAPI.js',
    'src/app/services/sessionTimeoutService.js',
    'src/app/services/guil.svc.js',
    
    'src/app/directives/app.directives.module.js',
    'src/app/utils/filters.js',
    
    'src/app/modules/**/*.js',
    
    'src/app/ui-components/metlife-corp-help-block/metlife-corp-help-block.dir.js',
    'src/app/ui-components/metlife-guil-header-nav/metlife-guil-header-nav.dir.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('app.min.js'))
    .pipe(ngAnnotate())
    .pipe(uglify({output: {ascii_only:true}}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist'));
});

gulp.task('build', ['clean','lint','lib-script-minify','app-script-minify'], function () {});