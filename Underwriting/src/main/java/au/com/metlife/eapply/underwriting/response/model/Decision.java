package au.com.metlife.eapply.underwriting.response.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Decision implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String productName;
	private String mat;
	private String decision;
	private String[] reasons;
	private String totalDebitsValue;
	private String originalTotalDebitsValue;
	private String totalDebitsReason;
	
	private List<Rating> listRating;
	private List<Exclusion> listExclusion;
	
	private int mExclusions = 0;
	private int lExclusions = 0;
	private List<String> plannedReason = null;
	private List<String> orgReason = null;;
	
		
	

	public List<String> getOrgReason() {
		return orgReason;
	}
	public void setOrgReason(List<String> orgReason) {
		this.orgReason = orgReason;
	}
	public List<String> getPlannedReason() {
		return plannedReason;
	}
	public void setPlannedReason(List<String> plannedReason) {
		this.plannedReason = plannedReason;
	}
	public int getLExclusions() {
		return lExclusions;
	}
	public void setLExclusions(int exclusions) {
		lExclusions = exclusions;
	}
	public int getMExclusions() {
		return mExclusions;
	}
	public void setMExclusions(int exclusions) {
		mExclusions = exclusions;
	}
	
	public String getDecision() {
		return decision;
	}
	public void setDecision(String decision) {
		this.decision = decision;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String[] getReasons(int arrayLength) {
		if (null == reasons) {
			reasons = new String[arrayLength];
		}
		return reasons;
	}
	public String[] getReasons() {
		return reasons;
	}
	public void setReasons(String[] reasons) {
			this.reasons = reasons;			 
			
	}
	public String getTotalDebitsReason() {
		return totalDebitsReason;
	}
	public void setTotalDebitsReason(String totalDebitsReason) {
		this.totalDebitsReason = totalDebitsReason;
	}
	public String getTotalDebitsValue() {
		return totalDebitsValue;
	}
	public void setTotalDebitsValue(String totalDebitsValue) {
		this.totalDebitsValue = totalDebitsValue;
	}
	public String getMat() {
		return mat;
	}
	public void setMat(String mat) {
		this.mat = mat;
	}
	public List<Rating> getListRating() {
		
		if (null == listRating) {
			listRating = new ArrayList<Rating>();
		}
		return listRating;
	}
	public List<Exclusion> getListExclusion() {
		
		if (null == listExclusion) {
			listExclusion = new ArrayList<Exclusion>();
		}
		return listExclusion;
	}
	public String getOriginalTotalDebitsValue() {
		return originalTotalDebitsValue;
	}
	public void setOriginalTotalDebitsValue(String originalTotalDebitsValue) {
		this.originalTotalDebitsValue = originalTotalDebitsValue;
	}
}
