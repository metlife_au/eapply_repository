<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="eapplymain" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="shortcut icon" href="../../eapply/static/hesta/images/favicon.ico">
<title>HESTA Super</title>

<!-- Bootstrap core CSS -->
<link href="../../eapply/static/hesta/css/bootstrap.min.css" rel="stylesheet">
<link href="../../eapply/static/hesta/css/bootstrap-select.css" rel="stylesheet">
<link href="../../eapply/static/hesta/css/docs.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="screen" href="../../eapply/static/hesta/css/datepicker.css" />


<!-- Custom styles for this template -->
<link href="../../eapply/static/hesta/css/custom.css" rel="stylesheet">
<link href="../../eapply/static/hesta/css/ngDialog.css" rel="stylesheet">
<link rel="stylesheet" href="../../eapply/static/hesta/css/ngDialog-theme-default.css">
<link rel="stylesheet" href="../../eapply/static/hesta/css/ngDialog-theme-plain.css">
<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../eapply/static/hesta/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.../../eapply/static/hesta/js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<%
String headerData = response.getHeader("InputData");
String domainName = response.getHeader("domainname");
%>
<script type="text/javascript">
var inputData = '<%=headerData%>';
var baseUrl = '<%= domainName%>';
</script>


<body>

<div ng-view></div>
<!-- Placed at the end of the document so the pages load faster --> 
<script src="../../eapply/static/hesta/js/angular.min.js"></script>
<script src="../../eapply/static/hesta/js/angular-route.js"></script>
<script src="../../eapply/static/hesta/js/angular-resource.min.js"></script>
<script src="../../eapply/static/hesta/js/jquery.min.js"></script> 
<!--  <script src="../../eapply/static/hesta/js/jquery-ui.js"></script> -->
<script src="../../eapply/static/hesta/js/bootstrap.min.js"></script> 
<script src="../../eapply/static/hesta/js/docs.min.js"></script>
<script src="../../eapply/static/hesta/js/ng-file-upload-shim.js"></script>
<script src="../../eapply/static/hesta/js/ng-file-upload.js"></script>
 <script src="../../eapply/static/hesta/js/ngDialog.js"></script>

<!-- CareSuperApp application file -->
<script src="../../eapply/static/hesta/js/app.js"></script>

<!-- CareSuperApp controllers -->
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/aura.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/auraOcc.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/auraSpecialOffer.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/auraTransfer.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/decision.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/landing.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/login.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/newMemberQuote.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/quote.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/quoteCancel.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/quoteOccUpdate.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/quoteTransfer.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/retrieveSavedApp.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/summary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/newMemberSummary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/workRatingSummary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/transferSummary.ctrl.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/DecisionController.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/cancelConfirmation.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/controllers/timeOutController.ctrl.js"></script>

<script type="text/javascript" src="../../eapply/static/hesta/js/moment.js"></script> 
<script type="text/javascript" src="../../eapply/static/hesta/js/datetimepickercustom.js"></script> 
<script type="text/javascript" src="../../eapply/static/hesta/js/bootstrap-select.js"></script> 

<!-- CareSuperApp Services -->
<script type="text/javascript" src="../../eapply/static/hesta/js/services/service.js"></script>

<!-- CareSuperApp utilities -->
<script type="text/javascript" src="../../eapply/static/hesta/js/utils/careSuperApp.filters.js"></script>
<script type="text/javascript" src="../../eapply/static/hesta/js/utils/directives.js"></script>

<script src="../../eapply/static/hesta/js/custom.js"></script> 
<script src="../../eapply/static/hesta/js/validation.js"></script> 

  
         <!--     <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
     <script type="text/javascript">

            function tagSession(){
                var tagValueElement = document.getElementById("tagValue");
                var tagValue = tagValueElement.value || "bala";               
                if  (typeof ruxitApi != "undefined") {
                    ruxitApi.tagSession(tagValue);                    
                }
            }

           
        </script> -->


</body>
</html>