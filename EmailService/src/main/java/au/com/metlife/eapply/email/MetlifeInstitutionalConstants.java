package au.com.metlife.eapply.email;

public class MetlifeInstitutionalConstants {
    
	public final static String FUND_ING = "INGD";
	
	public final static String FUND_APSD = "APSD";
	
	public final static String FUND_FIRS = "FIRS";
	
	public final static String FUND_HOST = "HOST";
	
    public final static String APP_NUM = "APPLICATIONNUMBER";
    
    public final static String APP_DECISION = "APP_DECISION";
    
    public static final String TITLE = "TITLE";
    
    public static final String FIRST_NAME = "FIRSTNAME";
    
    public static final String LAST_NAME = "LASTNAME";
    
    public static final String PRODUCTNAME = "PRODUCTNAME";
    
    public static final String PDFLINK = "PDFLINK";
    
    public static final String EMAIL_SMTP_PORT = "EMAIL_SMTP_PORT";
    
    public static final String EMAIL_SMTP_HOST = "EMAIL_SMTP_HOST";
    
    public static final String EMAIL_SMTP_USERID = "EMAIL_SMTP_USERID";
    
    public static final String EMAIL_SMTP_PASS = "EMAIL_SMTP_PASS";
    
    public static final String EMAIL_SUBJECT = "EMAIL_SUBJECT";
    
    public static final String EMAIL_PREFERRED_CONTACT_NUMBER = "EMAIL_PREFERRED_CONTACT_NUMBER";
    
    public static final String EMAIL_OTHER_CONTACT_NUMBER = "EMAIL_OTHER_CONTACT_NUMBER";
    
    public static final String EMAIL_PREFERRED_CONTACT_TIME = "EMAIL_PREFERRED_CONTACT_TIME";
    
    public static final String EMAIL_EMAIL_ADDRESS = "EMAIL_EMAIL_ADDRESS";
    
    public static final String MEMBER_NO = "MEMBER_NO";
    
    public static final String URL_RETRIEVE ="URL_RETRIEVE";
    
    public static final String EMAIL_IND_SUBJECT_NAME = "EMAIL_IND_SUBJECT_NAME";
    
    public final static String INSTITUTIONAL = "INSTITUTIONAL";
    
    public final static String ERROR_CODE = "500";
    
    public final static String RETRIEVEINSTAPPLIST = "retrieveInstitutionalApplicationList";
    
    public final static String RETRIEVEINSTAPP = "retrieveInstitutionalApplication";
    
    public final static String RETRIEVEAPP = "retrieveApplication";
    
    public final static String RETRIEVEAPPLICANT = "retrieveApplicant";
    
    public final static String RETRIEVECONTACTINFO = "retrieveContactInfo";
    
    public final static String RETRIEVECOVERS = "retrieveCovers";
    
    public final static String RETRIEVEFUNDINFO = "retrieveFundInfo";
    
    public final static String RETRIEVEAURADETAILS = "retrieveAuraDetails";
    
    public static final String BEFORERETRIEVE = "BEFORE retrieveApplication";
    
    public static final String AFTERRETRIEVE = "AFTER retrieveApplication";
    
    public static final String RETRIEVEAURA = "retrieveAuraDetails = >";
    
    public static final String RETRIEVEFUND = "retrieveFundInfo = >";
    
    public static final String RETRIEVERULE = "vecOfRuleInfo = >";
    
    // public static final String REFER_UW = "RUW";
    public static final String EMAIL = "Email";
    
    public static final String HOME_PH = "Home Phone";
    
    public static final String WORK_PH = "Work Phone";
    
    public static final String MOBILE_PH = "Mobile";
    
    public static final String COLOR_GRAY = "grey";
    
    public static final String COLOR_GREEN = "green";
    
    public static final String HELVETICA_BOLD = "HELVETICA_BOLD";
    
    public static final String FILE_NOT_FOUND_EXCEPTION = "10020";
    
    public static final String DOCUMENT_EXCEPTION = "10021";
    
    public static final String COLOR_BLACK = "black";
    
    public static final String COLOR_WHITE = "white";
    
    public static final String COLOR_RED = "red";
    
    public static final String COLOR_BLUE = "blue";
    
    public static final String BLANK = "";
    
    public static final String DATE_OF_APPL = "Date of Application:";
    
    public static final String PREFF_CON_DTS = "Preferred Contact Details";
    
    public static final String PREFF_CON_METHOD = "Preferred Contact Method:";
    
    public static final String PREFF_CON_TIME = "Preferred Contact Time:";
    
    public static final String MEMBER_DETAILS = "Member Details";
    
    public static final String TITLE_PDF = "Title";
    
    public static final String FIRST_NAME_PDF = "Firstname";
    
    public static final String SURNAME = "Surname";
    
    public static final String DOB = "Date of Birth";
    
    public static final String DJF = "Date Joined Fund";
    
    public static final String GENDER = "Gender";
    
    public static final String MEMBER_NUMBER = "Member number";
    
    public static final String UW_DECISION_BLURB = "UW_DECISION_BLURB";
    
    public static final String UW_DECISION = "UW_DECISION";
    
    public static final String EXCL_LOAD = "EXCL_LOAD";
    
    public static final String PRODUCT_DETAILS = "PRODUCT_DETAILS";
    
    public static final String FREQUENCY = "FREQUENCY";
    
    public static final String INS_APPLICATION_TYPE = "Institutional";
    
    public static final String INDV_APPLICATION_TYPE = "Individual";
    
    public static final String INDV_REPAYMENT_COVER = "Repayment Cover";
    
    public static final String INDV_REPAYMENT_COVER_NO = "181";
    
    public static final String LOAN_PROTECTION_CITI = "PRD027";
    
    public static final String INDV_LOAN_COVER_NO = "180";
    
    public static final String INDV_LOAN_COVER = "Loan Protection";
    
    public static final String DCL_TEXT_DEATH="Based on the information disclosed in your personal statement, we were unable to offer you additional Death. The following disclosures" +
																" contributed to this decision: ";
    
    public static final String DCL_TEXT_TPD="Based on the information disclosed in your personal statement, we were unable to offer you additional TPD. The following disclosures" +
																		" contributed to this decision: ";
    
    public static final String DCL_TEXT_DEATH_ADD = " Your additional Death cover has been included in the Death only cover component.";
    
	public static final String DCL_TEXT_TRAUMA ="Based on the information disclosed in your personal statement, we were unable to offer you additional Income Protection cover. The following disclosures" +
				" contributed to this decision: ";

	public static final String DCL_TEXT_IP ="Based on the information disclosed in your personal statement, we were unable to offer you additional Income Protection cover. The following disclosures" +
				" contributed to this decision: ";
    
    public static final String DCL_TEXT_OVERALL = "Based on the information disclosed in your personal statement, we were unable to offer you additional Insurance cover. The following conditions"
            + " contributed to this decision: ";    
    
    public static final String DECLARE_PRIVACY_POLICY_OWNER = "<br> <p>I acknowledge and consent to the following declarations below:</p><br> <ul style=\"list-style-type:disc;padding-left:20px;\"> <li>I have read and understood my Duty of Disclosure as explained in the PDS and have not withheld any information material to my application for insurance and understand this duty continues to apply until my application has been accepted;</li> <li>I confirm I have read the Insurer's Privacy Statement; and I consent to the collection, use and disclosure of my personal (including sensitive) information in accordance with the Privacy Statement; and</li> <li>The information provided in this application is true and correct.</li> </ul>";
    
    public static final String DECLARE_PRIVACY_COMPANY = "<br> <p>As representative of the company, I acknowledge and consent to the following declarations below:</p><br> <ul style=\"list-style-type:disc;padding-left:20px;\"> <li>I have read and understand the Product Disclosure Statement (PDS) (including the \"When will a benefit not be paid\" section) and my decision to purchase OAMPS Life is based on the PDS;</li> <li>I confirm I have read the Insurer's Privacy Statement; and I consent to the collection, use and disclosure of my personal (including sensitive) information in accordance with the Privacy Statement;</li> <li>I understand my OAMPS Life policy will not become effective until this application is accepted in writing; and</li> <li>I acknowledge that any direct debit arrangement is governed by the terms of the Direct Debit Request Service Agreement in the PDS and the terms and conditions of my policy.</li> </ul>";
    
    public static final String DECLARE_PRIVACY_SUPER = "<br><p>As representative of the superannuation trustee, I acknowledge and consent to the following declarations below:</p><br><ul style=\"list-style-type:disc;padding-left:20px;\">	<li>I have read and understand the Product Disclosure Statement (PDS) (including the \"When will a benefit not be paid\" section) and my decision to purchase OAMPS Life is based on the PDS;</li>	<li>I confirm I have read the Insurer's Privacy Statement; and I consent to the collection, use and disclosure of my personal (including sensitive) information in accordance with the Privacy Statement;</li><li>I understand my OAMPS Life policy will not become effective until this application is accepted in writing; and</li>	<li>I acknowledge that any direct debit arrangement is governed by the terms of the Direct Debit Request Service Agreement in the PDS and the terms and conditions of my policy.</li></ul>";
    
    public static final String DECLARE_PRIVACY_INDIVIDUAL = "<br><p>I acknowledge and consent to the following declarations below:</p><br><ul style=\"list-style-type:disc;padding-left:20px;\"><li>I have read and understand the Product Disclosure Statement (PDS) (including the \"When will a benefit not be paid\" section) and my decision to purchase OAMPS Life is based on the PDS;</li><li>I confirm I have read the Insurer's Privacy Statement; and I consent to the collection, use and disclosure of my personal (including sensitive) information in accordance with the Privacy Statement;</li><li>I understand my OAMPS Life policy will not become effective until this application is accepted in writing; and</li><li>I acknowledge that any direct debit arrangement is governed by the terms of the Direct Debit Request Service Agreement in the PDS and the terms and conditions of my policy.</li></ul>";
   
    public static final String POLICY_OWNER_COMPANY = "Company";
	public static final String POLICY_OWNER_SUPERANNUATION_TRUSTEE = "Superannuation trustee";
	public static final String POLICY_OWNER_ANOTHER_INDIVIDUAL = "Another individual";
	
	public static final String ALI_SIMPLE_INCOME_PROTECTION="ALI Simple Income Protection";
	
	public static final String ALI_GROUP_SIMPLE_INCOME_PROTECTION="ALI Group's Simple Income Protection";
	
	public static final String FUNERAL_ESTATE_PLAN="Funeral Estate Plan";
	
	public static final String FUNERAL_COVER_PLUS="Funeral Cover Plus";
	
	public static final String METL_FUNERAL_COVER_PLUS="MetLife Funeral Cover Plus";
	
	public static final String PREMIUM_FUNERAL_ESTATE_PLAN="Premium Funeral & Estate Plan";
	
	public static final String RULE_FILE_LOC="RULE_FILE_LOC";
	
	public static final String COVER_NO_219="219";
	
	public static final String COVER_NO_220="220";
	
	public static final String COVER_NO_DEATH_256="256";
	
	public static final String COVER_NO_TPD_257="257";
	
	public static final String ACC_CVRS_STR="$#$ACC_CVRS$#$";
	
	public static final String DCL_CVRS_STR="$#$DCL_CVRS$#$";
	
	public static final String AND_STR=" and ";
	
	public static final String ACC = "ACC";
	
	public static final String DCL = "DCL";
	
	public static final String HTMLEMAILS="HTMLEMAILS";
	
	public static final String GEN_KEY="432rerr32rhWR#432g";
	
	public static final  String DATE_OF_QUOT = "Date of Quotation";
	
	public static final String CONVERT_AND_MAINTAIN_COVER="Convert and maintain cover";
	
	public static final String SWITCH_COVER_TYPE="Switch cover type";
	
	public static final String COVER_OPTION_INCREASE="Increase";
	
	public static final String COVER_OPTION_DECREASE="Decrease";
	
    public static final String DC_CANCEL="DcCancel";
    
    public static final String TPD_CANCEL="TpdCancel";
    
    public static final String IP_CANCEL="IpCancel";
    
    public static final String NO_CHANGE="No change";
    
    public static final String ING_COPY_RIGHT = "<br><P style=\"padding:0 0 0 20px;line-height:17px;\">This information was prepared and sent on behalf of Diversa Trustees Limited ABN 49 006 421 638, AFSL 235153, RSE L0000635, the Trustee of the ING DIRECT Superannuation Fund ABN 13 355 603 448 (Fund) and the issuer of interest in the Fund. ING DIRECT Living Super is a product issued out of the Fund. ING DIRECT, a division of ING BANK (Australia) Limited ABN 24 000 893 292, AFSL 229823, is the Promoter of the Fund. Insurance cover is issued by MetLife Insurance Limited ABN 75 004 274 882 AFSL 238096 to the Trustee.<br></P>";
	
    public static final String KEY1 = "KEY1";
    public static final String KEY2 = "KEY2";
    public static final String P_NAME = "P_NAME";
    public static final String P_LAST_NAME = "P_LAST_NAME";
    public static final String S_LAST_NAME = "S_LAST_NAME";
    public static final String P_NAME1 = "P_NAME1";
    public static final String S_NAME = "S_NAME";
    
    public static final String INPUT_IDENT_ENCR="INPUT_IDENT_ENCR";
    public static final String INPUT_DATA_ENCR="INPUT_DATA_ENCR";
	
}
