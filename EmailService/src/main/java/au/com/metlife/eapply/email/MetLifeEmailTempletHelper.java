package au.com.metlife.eapply.email;
/* -------------------------------------------------------------------------------------------------    
 * Description:   Helper class for generating email template
 * -------------------------------------------------------------------------------------------------
 * Copyright @ 2009 MetLife, Inc. 
 * -------------------------------------------------------------------------------------------------
 * Author :    khirod.panda
 * Created:     April 22,2009
 * -------------------------------------------------------------------------------------------------
 * Modification Log:
 * -----------------
 * Date(MM/DD/YYYY)  july 03,2009        Author anand.kishore Description   code refactoring      
 *          
 * {Please place your information at the top of the list}
 * -------------------------------------------------------------------------------------------------
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


 

public class MetLifeEmailTempletHelper {
	//BEGIN_MARKER
	public static final String BEGIN_MARKER = "<%";
	//END_MARKER
	public static final String END_MARKER = "%>";
	//emailTempHelper
	private static MetLifeEmailTempletHelper emailTempHelper = null;
	// constructor
	private MetLifeEmailTempletHelper() {
	}
	
	public static MetLifeEmailTempletHelper getInstance() {
		if (emailTempHelper == null)
			emailTempHelper = new MetLifeEmailTempletHelper();
		return emailTempHelper;
	}

	/**
     * Description: This method defines indexes for email template
     * 
     * @param String word
     * 
     * @return List
     * @throws Exception 
     */
	private ArrayList getDataElementKeys(String word) throws Exception {
		ArrayList listOfHolders = new ArrayList();
		try {
			int beginIndex = 0;
			int endIndex = 0;
			int times = 0;
			String val = null;
			while (true) {
				beginIndex = word.indexOf(BEGIN_MARKER, beginIndex);
				if (beginIndex == -1) {
					break;
				} else {
					beginIndex += 2;
				}
				endIndex = word.indexOf(END_MARKER, beginIndex);
				if (endIndex == -1)
					break;

				try {
					val = word.substring(beginIndex, endIndex);
					if (val != null && !(val.trim().equals(""))) {
						listOfHolders.add(val);
					}
				} catch (Exception e) {
					e.printStackTrace();
					break;
				}

				
				times++;
				if (times > 20)
					break;

			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return listOfHolders;
	}	
	/**
     * Description: This method will be reffered by EmailService for replacing the data
	 * element keys with data element values
     * 
     * @param String word
     * 
     * @return List
     * 
     */
	public String getPopulatedTemplate(File tempFile, HashMap valForPlHolders)
			throws Exception {
		StringBuffer contents = new StringBuffer();
		BufferedReader input = null;
		try {
			if(tempFile.exists())			
			input = new BufferedReader(new FileReader(tempFile));
			String line = null; // not declared within while loop
			
			while ((line = input.readLine()) != null) {
				
				if (!line.trim().equals(MetLifeEmailConstants.EMPTY)) {
					line = getUpdatedLine(line, valForPlHolders);
				}
				contents.append(line);
				
				contents.append(System.getProperty(MetLifeEmailConstants.LINE_SEPERATOR));
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (IOException ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			try {
				if (input != null) {
					input.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
				throw ex;
			}
		}
		
		return contents.toString();
	}	
	/**
     * Description:  This function will be reffered internally ,for the given line which was
	 * read from the template file this function replaces the data element key
	 * with data element value, if value not found updates with null value
     * 
     * @param String line, HashMap valForPlHolders
     * 
     * @return String
     * 
     */
	private String getUpdatedLine(String line, HashMap valForPlHolders) {
		StringBuffer buff = null;
		String keyVal = null;
		String valToReplace = null;
		try {
			if (line.indexOf(BEGIN_MARKER) == -1)
				return line;
			ArrayList list = getDataElementKeys(line);
			for (int i = 0; i < list.size(); i++) {
				buff = new StringBuffer(BEGIN_MARKER);
				keyVal = (String) list.get(i);
				buff.append(keyVal);
				buff.append(END_MARKER);
				if (valForPlHolders.containsKey(keyVal)) {
					valToReplace = (String) valForPlHolders.get(keyVal);
					if(valToReplace!=null){
						line = line.replaceAll(buff.toString(), valToReplace);
					}else{
						line = line.replaceAll(buff.toString(), "");
					}
				
				} else {
					line = line.replaceAll(buff.toString(), MetLifeEmailConstants.NULL);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}		
		return line;
	}

}