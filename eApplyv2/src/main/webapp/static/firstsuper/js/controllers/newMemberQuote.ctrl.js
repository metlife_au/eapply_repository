/*New Member Change Cover Controller ,Progressive and Mandatory Validations Starts   */
FirstSuperApp.controller('newmemberquote',['$scope','$routeParams', '$location','$timeout','$window','QuoteService', 'OccupationService','persoanlDetailService','deathCoverService',
                                          'tpdCoverService','ipCoverService', 'CalculateService', 'NewOccupationService','auraInputService','CalculateDeathService',
                                          'CalculateTPDService', 'PersistenceService','ngDialog','auraResponseService', 'urlService',
                                          function($scope,$routeParams, $location,$timeout,$window,QuoteService, OccupationService,persoanlDetailService,deathCoverService,tpdCoverService,
                                        		  ipCoverService, CalculateService, NewOccupationService,auraInputService,CalculateDeathService,CalculateTPDService,
                                        		  PersistenceService,ngDialog,auraResponseService,urlService){
	$scope.urlList = urlService.getUrlList();
    $scope.emailFormat = /^[a-zA-Z]+[a-zA-Z0-9._-]+@[a-zA-Z]+\.[a-zA-Z.]{2,}$/;
    $scope.phoneNo = /^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/;
    $scope.eligibilityViolated = false;

    $scope.coltwo = false;
  	$scope.toggleTwo = function() {
        $scope.coltwo = !$scope.coltwo;
        $("a[data-target='#collapseTwo']").click();
    };
  	$scope.colthree = false;
  	$scope.toggleThree = function() {
        $scope.colthree = true;
        $("a[data-target='#collapseThree']").click()
    };
    
    /*$scope.toggleThreeDeath = function(){
  		$scope.colthree = true;
  		$("a[data-target='#collapseThree']").click();
  		$('#tpdsection').removeClass('active');
		$('#ipsection').removeClass('active');
		$("#death").css("display", "block");
		$("#tpd").css("display", "none");
		$("#sc").css("display", "none");
		$scope.isDeathDisabled = false;
		$scope.isTPDDisabled = true;
		$scope.isIPDisabled = true;
  	};*/
    
  	
  	
    $scope.collapse = false;
    	$scope.toggle = function() {
            $scope.collapse = !$scope.collapse;           
       };
    QuoteService.getList($scope.urlList.quoteUrl,"FIRS").then(function(res){
    	$scope.IndustryOptions = res.data;
    }, function(err){
    	console.log("Error while fetching industry options " + JSON.stringify(err));
    });
    $scope.isDeathDisabled = false;
    $scope.isTPDDisabled = false;
    $scope.isIPDisabled = false;
    $scope.otherOccupationObj = {'newOtherOccupation': ''};
/*
    var expandOrCollapseFlag = false;
    $scope.collapseSection = function(){
    	if(expandOrCollapseFlag && $scope.terminalIllnessClaim == "Yes"){
    		$scope.colthree = false;
    	} else if(expandOrCollapseFlag && $scope.terminalIllnessClaim == "No"){
    		$scope.colthree = true;
    		if($scope.tpdClaim == "Yes"){
    			$('#tpdsection').removeClass('active');
    			$('#ipsection').removeClass('active');
    			$("#death").css("display", "block");
    			$("#tpd").css("display", "none");
    			$("#sc").css("display", "none");
    			$scope.isDeathDisabled = false;
    		    $scope.isTPDDisabled = true;
    		    $scope.isIPDisabled = true;
    		} else if($scope.tpdClaim == "No"){
    			$('#tpdsection').addClass('active');
    			$('#ipsection').addClass('active');
    			$("#death").css("display", "block");
    			$("#tpd").css("display", "block");
    			$("#sc").css("display", "block");
    			$scope.isDeathDisabled = false;
    		    $scope.isTPDDisabled = false;
    		    $scope.isIPDisabled = false;
    		}
    	}
    };*/
    
    $scope.showHelp = function(msg){
    	$scope.modalShown = !$scope.modalShown;
    	$scope.tipMsg = msg;
    };
    
    $scope.invalidSalAmount = false;
    $scope.checkIPCover = function(){ 
    	if($scope.newMemberDeathCoverDetails.amount > (7 * parseInt($scope.newAnnualSalary))){
    		$scope.eligibilityViolated = true;
    	} else{
    		$scope.eligibilityViolated = false;
    	}
    	if(parseInt($scope.newAnnualSalary) == 0){
			$scope.invalidSalAmount = true;
		} else{
			$scope.invalidSalAmount = false;
		}
    	if(parseInt($scope.newAnnualSalary) < 16000 && $scope.workTimeAndSalary == "No"){
    		$('#ipsection').removeClass('active');
    		$("#sc").css("display", "none");
    	    $scope.isIPDisabled = true;
    	} else{
    		/*if(($scope.terminalIllnessClaim == undefined || $scope.terminalIllnessClaim == "No") && 
    				($scope.tpdClaim == undefined || $scope.tpdClaim == "No")){*/
    			$('#ipsection').addClass('active');
    			$("#sc").css("display", "block");
    			$scope.isIPDisabled = false;
    	/*	}*/
    	}
    	$scope.setOccupationCategory();
    };
    	            
    $scope.indexation= {
    		death: false,
    		tpd: false
    }
    $scope.setNewIndexation = function ($event) {
    	$event.stopPropagation();
    	$event.preventDefault();
    	$scope.indexation.death = $scope.indexation.tpd = !$scope.indexation.death;
    	if(!$scope.indexation.death) {
    		$("#newchangecvr-index-dth").parent().removeClass('active');
    		$("#newchangeCvr-indextpd-tpd").parent().removeClass('active');
    	}
    };
    
    $scope.syncRadios = function(val){
    	$scope.deathWarningFlag = false;
    	$scope.tpdWarningFlag = false;
    	$scope.newDeathrequireCover = "";
    	$scope.newTPDRequireCover = "";
    	$scope.coverAmountFlag = false;
    	$scope.coverAmountErrorMsg = "";
    	var radios = $('label[radio-sync]');
    	var data = $('input[data-sync]');
		data.filter('[data-sync="' + val + '"]').attr('checked','checked');
		if(val == 'Fixed'){
			showhide('dollar1','nodollar1');
			showhide('dollar','nodollar');
			$scope.newDeathcoverType = 'DcFixed';
			$scope.newTpdCoverType = 'TPDFixed';
		} else if(val == 'Unitised'){
			showhide('nodollar1','dollar1');
			showhide('nodollar','dollar');
			$scope.newDeathcoverType = 'DcUnitised';
			$scope.newTpdCoverType = 'TPDUnitised';
		}
		radios.removeClass('active');
		radios.filter('[radio-sync="' + val + '"]').addClass('active');
		$scope.calculateNewQuoteOnChange();
    };

    $scope.navigateToLandingPage = function (){
    	if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}
    }
    
 // added for session expiry
   /* $timeout(callAtTimeout, 900000); 
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/
  	
  /*  var timer;
    angular.element($window).bind('mouseover', function(){
    	timer = $timeout(function(){
    		sessionStorage.clear();
    		localStorage.clear();
    		$location.path("/sessionTimeOut");
    	}, 900000);
    }).bind('mouseout', function(){
    	$timeout.cancel(timer);
    });*/
    
    $scope.getOccupations = function(){
    	if(!$scope.newIndustry){
    		$scope.newIndustry = '';
    	}
    	OccupationService.getOccupationList($scope.urlList.occupationUrl,"FIRS",$scope.newIndustry).then(function(res){
    	//OccupationService.getOccupationList({fundId:"FIRS", induCode:$scope.newIndustry}, function(occupationList){
        	$scope.OccupationList = res.data;
        	$scope.newOccupation = "";
        	$scope.calculateNewQuoteOnChange();
       }, function(err){
    	   console.log("Error while fetching occupation options " + JSON.stringify(err));
       });
    };
    
    $scope.newMemberDeathCoverDetails = deathCoverService.getDeathCover();
	$scope.newMemberTpdCoverDetails = tpdCoverService.getTpdCover();
	$scope.newMemberIpCoverDetails = ipCoverService.getIpCover();
	
	if ($scope.newMemberDeathCoverDetails
			&& $scope.newMemberDeathCoverDetails.type) {
		if($scope.newMemberDeathCoverDetails.type == "1"){
			$scope.newDeathcoverType = "DcUnitised";
			showhide('nodollar1','dollar1');
			showhide('nodollar','dollar');
		} else if($scope.newMemberDeathCoverDetails.type == "2"){
			$scope.newDeathcoverType = "DcFixed";
		}
	}

	if ($scope.newMemberTpdCoverDetails
			&& $scope.newMemberTpdCoverDetails.type) {
		if($scope.newMemberTpdCoverDetails.type == "1"){
			$scope.newTpdCoverType = "TPDUnitised";
			showhide('nodollar1','dollar1');
			showhide('nodollar','dollar');
		} else if($scope.newMemberTpdCoverDetails.type == "2"){
			$scope.newTpdCoverType = "TPDFixed";
		}
	}
	
	/*if($scope.newMemberIpCoverDetails && $scope.newMemberIpCoverDetails.type){
		 $scope.newIPRequireCover = $scope.newMemberIpCoverDetails.units;
	}*/
	
	if($scope.newMemberIpCoverDetails && $scope.newMemberIpCoverDetails.waitingPeriod && $scope.newMemberIpCoverDetails.waitingPeriod != ''){
			$scope.waitingPeriodAddnl = $scope.newMemberIpCoverDetails.waitingPeriod;
	} else {
		$scope.waitingPeriodAddnl = '30 Days';
		}
	
	if($scope.newMemberIpCoverDetails && $scope.newMemberIpCoverDetails.benefitPeriod && $scope.newMemberIpCoverDetails.benefitPeriod != ''){
			$scope.benefitPeriodAddnl = $scope.newMemberIpCoverDetails.benefitPeriod;
	} else{
		$scope.benefitPeriodAddnl = '2 Years';
		}
	
	var newOccupationCategory;
	$scope.getNewMemberOccupation = function(){
  		var inputName = $scope.newIndustry + ":" + $scope.newOccupation;
  		NewOccupationService.getOccupation($scope.urlList.newOccupationUrl,"FIRS",inputName).then(function(result){
  		//NewOccupationService.getOccupation({fundId:"FIRS", occName:inputName},function(result){
  			newOccupationCategory = result.data;
  			$scope.newDeathOccupationCategory = newOccupationCategory[0].deathfixedcategeory;
  			$scope.newTpdOccupationCategory = newOccupationCategory[0].tpdfixedcategeory;
  			$scope.newIpOccupationCategory = newOccupationCategory[0].ipfixedcategeory;
  			
  			if($scope.newDeathOccupationCategory != "Professional" && $scope.newWithinOfficeQuestion == "Yes"){
  				$scope.newDeathOccupationCategory = "Office";
  				if(parseInt($scope.newAnnualSalary) > 100000 && ($scope.newTertiaryQuestion == "Yes" || $scope.newManagementRole == "Yes")){
  					$scope.newDeathOccupationCategory = "Professional";
  				}
  			}
  			if($scope.newTpdOccupationCategory != "Professional" && $scope.newWithinOfficeQuestion == "Yes"){
  				$scope.newTpdOccupationCategory = "Office";
  				if(parseInt($scope.newAnnualSalary) > 100000 && ($scope.newTertiaryQuestion == "Yes" || $scope.newManagementRole == "Yes")){
  					$scope.newTpdOccupationCategory = "Professional";
  				}
  			}
  			if($scope.newIpOccupationCategory != "Professional" && $scope.newWithinOfficeQuestion == "Yes"){
  				$scope.newIpOccupationCategory = "Office";
  				if(parseInt($scope.newAnnualSalary) > 100000 && ($scope.newTertiaryQuestion == "Yes" || $scope.newManagementRole == "Yes")){
  					$scope.newIpOccupationCategory = "Professional";
  				}
  			}
  			$scope.calculateNewQuoteOnChange();
  		}, function(e){
  			console.log("Error while calulating new member new occupation " + JSON.stringify(e));
  		});
  	};
	  	
  	$scope.setOccupationCategory = function(){
  		var deathCategory, tpdCategory, ipCategory;
  		
  		if(newOccupationCategory){
  			deathCategory = newOccupationCategory[0].deathfixedcategeory;
  			tpdCategory = newOccupationCategory[0].tpdfixedcategeory;
  			ipCategory = newOccupationCategory[0].ipfixedcategeory;
  		}
  		if(deathCategory != "Professional" && $scope.newWithinOfficeQuestion == "Yes"){
			$scope.newDeathOccupationCategory = "Office";
			if(parseInt($scope.newAnnualSalary) > 100000 && ($scope.newTertiaryQuestion == "Yes" || $scope.newManagementRole == "Yes")){
				$scope.newDeathOccupationCategory = "Professional";
			}
		}
		if(tpdCategory != "Professional" && $scope.newWithinOfficeQuestion == "Yes"){
			$scope.newTpdOccupationCategory = "Office";
			if(parseInt($scope.newAnnualSalary) > 100000 && ($scope.newTertiaryQuestion == "Yes" || $scope.newManagementRole == "Yes")){
				$scope.newTpdOccupationCategory = "Professional";
			}
		}
		if(ipCategory != "Professional" && $scope.newWithinOfficeQuestion == "Yes"){
			$scope.newIpOccupationCategory = "Office";
			if(parseInt($scope.newAnnualSalary) > 100000 && ($scope.newTertiaryQuestion == "Yes" || $scope.newManagementRole == "Yes")){
				$scope.newIpOccupationCategory = "Professional";
			}
		}
		$scope.checkIpCoverValue();
  	//	$scope.calculateNewQuoteOnChange();
  	};
	$scope.newWaitingPeriodOptions = ["30 Days", "60 Days", "90 Days"];
    $scope.newBenefitPeriodOptions = ['2 Years', '5 Years'];
    $scope.dcNewCoverAmount = 0.00;
	$scope.dcNewWeeklyCost = 0.00;
	$scope.tpdNewCoverAmount = 0.00;
	$scope.tpdNewWeeklyCost = 0.00;
	$scope.ipNewCoverAmount = 0.00;
	$scope.ipNewWeeklyCost = 0.00;
	$scope.totalNewWeeklyCost = 0.00;
	var newDynamicFlag = false;
	var newDeathAmount,newTPDAmount;
	var fetchAppnum = true;
	var appNum;
	var deathCallFlag = true;
	var tpdCallFlag = true;
	var IPAmount;
	var inputDetails = persoanlDetailService.getMemberDetails();
	$scope.newPersonalDetails = inputDetails[0].personalDetails;
	
	if(inputDetails[0] && inputDetails[0].contactDetails.emailAddress){
		$scope.newContactEmail = inputDetails[0].contactDetails.emailAddress;
	}
	if(inputDetails[0] && inputDetails[0].contactDetails.prefContactTime){
		if(inputDetails[0].contactDetails.prefContactTime == "1"){
			$scope.newContactPrefTime= "Morning (9am - 12pm)";
		}else{
			$scope.newContactPrefTime= "Afternoon (12pm - 6pm)";
		}
	}
	if(inputDetails[0] && inputDetails[0].contactDetails.prefContact){
		if(inputDetails[0].contactDetails.prefContact == "1"){
			$scope.preferredContactType= "Mobile";
			$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
			$scope.newContactPhone = inputDetails[0].contactDetails.mobilePhone;
		}else if(inputDetails[0].contactDetails.prefContact == "2"){
			$scope.preferredContactType= "Home";
			$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
			$scope.newContactPhone = inputDetails[0].contactDetails.homePhone;
		}else if(inputDetails[0].contactDetails.prefContact == "3"){
			$scope.preferredContactType= "Work";
			$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
			$scope.newContactPhone = inputDetails[0].contactDetails.workPhone;
		}
   }
	$scope.changePrefContactType = function(){
		if($scope.preferredContactType == "Home"){
			$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
			$scope.newContactPhone = inputDetails[0].contactDetails.homePhone;
		} else if($scope.preferredContactType == "Work"){
			$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
			$scope.newContactPhone = inputDetails[0].contactDetails.workPhone;
		} else if($scope.preferredContactType == "Mobile"){
			$scope.contactTypeOptions = ["Home", "Work", "Mobile"];
			$scope.newContactPhone = inputDetails[0].contactDetails.mobilePhone;
		} else {
      $scope.newContactPhone = '';
    }
	}
	var anb = parseInt(moment().diff(moment($scope.newPersonalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;
	
	$scope.calculateQuote = function(){
		if($scope.newDeathrequireCover == undefined || $scope.newDeathrequireCover == ""){
			$scope.newDeathrequireCover = 0;
    	}
    	if($scope.newTPDRequireCover == undefined || $scope.newTPDRequireCover == ""){
    		$scope.newTPDRequireCover = 0;
    	}
    	if($scope.newIPRequireCover == undefined || $scope.newIPRequireCover == ""){
    		$scope.newIPRequireCover = 0;
    	}
		            	var ruleModel = {
		                 		"age": anb,
		                 		"fundCode": "FIRS",
		                 		"gender": $scope.gender,
		                 		"deathOccCategory": $scope.newDeathOccupationCategory,
		                 		"tpdOccCategory": $scope.newTpdOccupationCategory,
		                 		"ipOccCategory": $scope.newIpOccupationCategory ,
		                 		"smoker": false,
		                 		"deathUnits": parseInt($scope.newDeathrequireCover),
		                 		"deathFixedAmount": parseInt($scope.newDeathrequireCover),
		                 		"deathFixedCost": null,
		                		"deathUnitsCost": null,
		                 		"tpdUnits": parseInt($scope.newTPDRequireCover),
		                 		"tpdFixedAmount": parseInt($scope.newTPDRequireCover),
		                 		"tpdFixedCost": null,
		                 		"tpdUnitsCost": null,
		                 		"ipUnits": parseInt($scope.newIPRequireCover),
		                 		"ipFixedAmount": parseInt($scope.newIPRequireCover),
		                		"ipFixedCost": null,
		                 		"ipUnitsCost": null,
		                 		"premiumFrequency": "Weekly",
		                 		"memberType": null,
		                 		"manageType": "NCOVER",
		                 		"deathCoverType": $scope.newDeathcoverType,
		                 		"tpdCoverType": $scope.newTpdCoverType,
		                 		"ipCoverType": "IpUnitised",
		                 		"ipWaitingPeriod": $scope.waitingPeriodAddnl,
		                 		"ipBenefitPeriod": $scope.benefitPeriodAddnl
		                 	};
		            	CalculateService.calculate(ruleModel,$scope.urlList.calculateUrl).then(function(res){
		            	//CalculateService.calculate({}, ruleModel, function(res){
		             		var newPremium = res.data;
		             		newDynamicFlag = true;
		             		for(var i = 0; i < newPremium.length; i++){
		             			if(newPremium[i].coverType == 'DcFixed' || newPremium[i].coverType == 'DcUnitised'){
		             				$scope.dcNewCoverAmount = newPremium[i].coverAmount;
		             				$scope.dcNewWeeklyCost = newPremium[i].cost;
		             			} else if(newPremium[i].coverType == 'TPDFixed' || newPremium[i].coverType == 'TPDUnitised'){
		             				$scope.tpdNewCoverAmount = newPremium[i].coverAmount;
		            				$scope.tpdNewWeeklyCost = newPremium[i].cost;
		            				} else if(newPremium[i].coverType == 'IpFixed' || newPremium[i].coverType == 'IpUnitised'){
		             				$scope.ipNewCoverAmount = newPremium[i].coverAmount;
		             				$scope.ipNewWeeklyCost = newPremium[i].cost;
		             			}
		             		}
		             		$scope.totalNewWeeklyCost = $scope.dcNewWeeklyCost+ $scope.tpdNewWeeklyCost+$scope.ipNewWeeklyCost;
		             		if(fetchAppnum){
		            			fetchAppnum = false;
		            			appNum = PersistenceService.getAppNumber();
		            		}
		             	}, function(err){
		             		console.log("Error calculating new member premium " + JSON.stringify(err));
		             	});
		             };
		             
 $scope.coverAmountFlag = false;
 $scope.coverAmountErrorMsg = "";		             
 $scope.deathWarningFlag = false;	
 $scope.deathErrorFlag = false;
 $scope.tpdErrorFlag = false;
 $scope.deathErrorMsg="";
 $scope.tpdErrorMsg="";
 $scope.tpdLimitMsg = "";
 $scope.tpdWarningFlag = false; 
 $scope.validateDeathTPDAmount = function(){
	// Death max validation
 	 if($scope.newDeathcoverType == "DcFixed"){
 		 var deathAllowableLimit = 7 * parseInt($scope.newAnnualSalary);
 		 if(parseInt($scope.newDeathrequireCover) < parseInt($scope.newMemberDeathCoverDetails.amount)){
 			$scope.deathWarningFlag = false; 
            $scope.deathLimitMsg = ""; 
 			$scope.deathErrorFlag = true;
 			$scope.deathErrorMsg="You cannot reduce your Death Only cover. Please re-enter your cover amount.";
 		 }else if((parseInt($scope.newDeathrequireCover) > deathAllowableLimit) && (deathAllowableLimit < 750000)){
 			$scope.deathErrorFlag = false;
 			$scope.deathErrorMsg="";
            $scope.deathWarningFlag = true;
            $scope.newDeathrequireCover = deathAllowableLimit;
            $scope.deathLimitMsg = "Death amount cannot be greter than 7 times your annual salary";
 		}else if((parseInt($scope.newDeathrequireCover) > 750000) && (deathAllowableLimit > 750000)){
 			 $scope.deathErrorFlag = false;
 			 $scope.deathErrorMsg="";
 			 $scope.deathWarningFlag = true; 
             $scope.newDeathrequireCover = 750000; 
             $scope.deathLimitMsg = "The selected benefit exceeds your maximum limit, we have adjusted it to your eligible maximum";
 		} else{
 			$scope.deathErrorFlag = false;
 			$scope.deathErrorMsg="";
 			$scope.deathWarningFlag = false; 
            $scope.deathLimitMsg = ""; 
 		}
 	 }else if($scope.newDeathcoverType == "DcUnitised"){
 		 var deathAllowableLimit = 7 * parseInt($scope.newAnnualSalary);
 		 if((parseInt($scope.newDeathrequireCover)*newDeathAmount) < parseInt($scope.newMemberDeathCoverDetails.amount)){
 			$scope.deathWarningFlag = false; 
            $scope.deathLimitMsg = ""; 
  			$scope.deathErrorFlag = true;
  			$scope.deathErrorMsg="You cannot reduce your Death Only cover. Please re-enter your cover amount.";
  		 }else if((parseInt($scope.newDeathrequireCover)*newDeathAmount > deathAllowableLimit) && (deathAllowableLimit < 750000)){
  			 $scope.deathErrorFlag = false;
  			 $scope.deathErrorMsg="";
             $scope.deathWarningFlag = true;
             $scope.newDeathrequireCover = Math.floor(deathAllowableLimit/newDeathAmount);
             $scope.deathLimitMsg = "Death amount cannot be greter than 7 times your annual salary";
  		} else if((parseInt($scope.newDeathrequireCover)*newDeathAmount > 750000) && (deathAllowableLimit > 750000)){
			 $scope.deathErrorFlag = false;
			 $scope.deathErrorMsg="";
  			 $scope.deathWarningFlag = true; 
             $scope.newDeathrequireCover = Math.floor(750000/newDeathAmount); 
             $scope.deathLimitMsg = "The selected benefit exceeds your maximum limit, we have adjusted it to your eligible maximum";
  		} else{
  			$scope.deathErrorFlag = false;
 			$scope.deathErrorMsg="";
  			$scope.deathWarningFlag = false; 
            $scope.deathLimitMsg = ""; 
  		}
	 }
 	
 	 // TPD max validation
 	 if($scope.newTpdCoverType == "TPDFixed"){
		var tpdAllowableLimit = 7 * parseInt($scope.newAnnualSalary);
 		if($scope.newTPDRequireCover != null && ($scope.newDeathrequireCover == null || $scope.newDeathrequireCover == "")){
 			 $scope.tpdErrorFlag = false;
			 $scope.tpdErrorMsg="";
 			 $scope.coverAmountFlag = true;
 	 		 $scope.coverAmountErrorMsg = "You cannot apply for TPD cover without Death Cover.";
 		}else if(parseInt($scope.newTPDRequireCover) < parseInt($scope.newMemberTpdCoverDetails.amount)){
 			 $scope.tpdErrorFlag = true;
  			 $scope.tpdErrorMsg="You cannot reduce your TPD Only cover. Please re-enter your cover amount.";
  			 $scope.tpdWarningFlag = false;
             $scope.tpdLimitMsg = "";
             $scope.coverAmountFlag = false;
	 		 $scope.coverAmountErrorMsg = "";
 		}else if((parseInt($scope.newTPDRequireCover) > tpdAllowableLimit) && (tpdAllowableLimit < 750000)){
 	 		 $scope.tpdErrorFlag = false;
			 $scope.tpdErrorMsg="";
             $scope.tpdWarningFlag = true;
             $scope.newTPDRequireCover = tpdAllowableLimit;
             $scope.tpdLimitMsg = "TPD amount cannot be greter than 7 times your annual salary";
             $scope.coverAmountFlag = false;
 	 		 $scope.coverAmountErrorMsg = "";
 		}else if((parseInt($scope.newTPDRequireCover) > 750000) && (tpdAllowableLimit > 750000)){
 			 $scope.tpdErrorFlag = false;
			 $scope.tpdErrorMsg="";
 		     $scope.tpdWarningFlag = true; 
             $scope.newTPDRequireCover = 750000; 
             $scope.tpdLimitMsg = "The selected benefit exceeds your maximum limit, we have adjusted it to your eligible maximum";
             $scope.coverAmountFlag = false;
 	 		 $scope.coverAmountErrorMsg = "";
 		} else if(parseInt($scope.newDeathrequireCover) <  parseInt($scope.newTPDRequireCover)){
 			if(anb < 30 && (parseInt($scope.newDeathrequireCover) < (parseInt($scope.newMemberDeathCoverDetails.amount) + (4 * newDeathAmount)))){
 				$scope.tpdErrorFlag = false;
 	  			 $scope.tpdErrorMsg="";
 	  			 $scope.tpdWarningFlag = false;
 	             $scope.tpdLimitMsg = "";
 	 	 		 $scope.coverAmountFlag = false;
 	 	 		 $scope.coverAmountErrorMsg = "";
 			} else{
 	 			$scope.tpdErrorFlag = false;
 	  			 $scope.tpdErrorMsg="";
 	  			 $scope.tpdWarningFlag = false;
 	             $scope.tpdLimitMsg = "";
 	 	 		 $scope.coverAmountFlag = true;
 	 	 		 $scope.coverAmountErrorMsg = "TPD amount should not be greater than your Death amount.Please re-enter.";
 			}
 	 	} else{
			$scope.tpdErrorMsg="";
 			$scope.tpdWarningFlag = false; 
 			$scope.tpdErrorFlag = false;
            $scope.tpdLimitMsg = "";
            $scope.coverAmountFlag = false;
	 		$scope.coverAmountErrorMsg = "";
 		}
 		
 	 } else if($scope.newTpdCoverType == "TPDUnitised"){
		var tpdAllowableLimit = 7 * parseInt($scope.newAnnualSalary);
 		if($scope.newTPDRequireCover != null && ($scope.newDeathrequireCover == null  || $scope.newDeathrequireCover == "")){
 			$scope.tpdErrorFlag = false;
  			$scope.tpdErrorMsg="";
 			$scope.coverAmountFlag = true;
 	 		$scope.coverAmountErrorMsg = "You cannot apply for TPD cover without Death Cover.";
 		 }else if((parseInt($scope.newTPDRequireCover)*newTPDAmount) < parseInt($scope.newMemberTpdCoverDetails.amount)){
 			 $scope.tpdErrorFlag = true;
  			 $scope.tpdErrorMsg="You cannot reduce your TPD Only cover. Please re-enter your cover amount.";
  			 $scope.tpdWarningFlag = false;
             $scope.tpdLimitMsg = "";
             $scope.coverAmountFlag = false;
	 		 $scope.coverAmountErrorMsg = ""; 
 		 }else if((parseInt($scope.newTPDRequireCover)*newTPDAmount > tpdAllowableLimit) && (tpdAllowableLimit < 750000)){
 	 		 $scope.tpdErrorFlag = false;
  			 $scope.tpdErrorMsg="";
             $scope.tpdWarningFlag = true;
             $scope.newTPDRequireCover = Math.floor(tpdAllowableLimit/newTPDAmount);
             $scope.tpdLimitMsg = "TPD amount cannot be greter than 7 times your annual salary";
             $scope.coverAmountFlag = false;
 	 		 $scope.coverAmountErrorMsg = "";
  		 }else if((parseInt($scope.newTPDRequireCover)*newTPDAmount > 750000) && (tpdAllowableLimit > 750000)){
  			 $scope.tpdErrorFlag = false;
  			 $scope.tpdErrorMsg="";
  			 $scope.tpdWarningFlag = true; 
             $scope.newTPDRequireCover = Math.floor(750000/newTPDAmount); 
             $scope.tpdLimitMsg = "The selected benefit exceeds your maximum limit, we have adjusted it to your eligible maximum";
             $scope.coverAmountFlag = false;
 	 		 $scope.coverAmountErrorMsg = "";
  		 } else if(parseInt($scope.newDeathrequireCover) <  parseInt($scope.newTPDRequireCover)){
 			if(anb < 30 && ((parseInt($scope.newDeathrequireCover) * newDeathAmount) < (parseInt($scope.newMemberDeathCoverDetails.amount) + (4 * newDeathAmount)))){
 				$scope.tpdErrorFlag = false;
	  			 $scope.tpdErrorMsg="";
 	 	 		 $scope.coverAmountFlag = false;
 	 	 		 $scope.coverAmountErrorMsg = "";
 	 	 		$scope.tpdWarningFlag = false;
	             $scope.tpdLimitMsg = "";
 			 } else{
 	 			 $scope.tpdErrorFlag = false;
	  			 $scope.tpdErrorMsg="";
 	 	 		 $scope.coverAmountFlag = true;
 	 	 		 $scope.coverAmountErrorMsg = "TPD amount should not be greater than your Death amount.Please re-enter.";
 	 	 		 $scope.tpdWarningFlag = false;
	             $scope.tpdLimitMsg = "";
 			 }
 	 	 }else{
  			$scope.tpdErrorFlag = false;
  			$scope.tpdErrorMsg="";
  			$scope.tpdWarningFlag = false; 
            $scope.tpdLimitMsg = ""; 
            $scope.coverAmountFlag = false;
 	 		$scope.coverAmountErrorMsg = "";
  		}
 		
 	 }
 	 $scope.calculateNewQuoteOnChange();
   };
	             
 $scope.calculateNewQuoteOnChange = function(){
   if(newDynamicFlag && !$scope.coverAmountFlag){
	   $scope.calculateQuote();
   }
 };   
 $scope.isNewDeathcoverTypeDisabled = false;
 $scope.isNewDeathrequireCoverDisabled = false;
 $scope.isNewTpdCoverTypeDisabled = false;
 $scope.isNewTPDRequireCoverDisabled = false;
 $scope.deathTpdcheckboxState = false;
 $scope.increaseCoverAmount = function(){
		$timeout(function(){
			$scope.deathTpdcheckboxState = $('#increaseCoverAmount').prop('checked'); 
			if($scope.deathTpdcheckboxState == true){
				$scope.isNewDeathcoverTypeDisabled = true;
			    $scope.isDeathIndexDisabled = true;
			    $scope.isNewDeathrequireCoverDisabled = true;
			    $scope.isNewTpdCoverTypeDisabled = true;
	            $scope.isTPDIndexDisabled = true;
	            $scope.isNewTPDRequireCoverDisabled = true;
			    $scope.newDeathrequireCover = 7 * ($scope.newAnnualSalary) ;
			    $scope.newTPDRequireCover = 7 * ($scope.newAnnualSalary) ;
			    $scope.newDeathcoverType = "DcFixed";
			    $scope.newTpdCoverType = "TPDFixed";
				 if(parseInt($scope.newDeathrequireCover) > 750000){
			 		$scope.newDeathrequireCover = 750000 ;
			 		$scope.deathWarningFlag = true;
			 		$scope.deathLimitMsg = "The selected benefit exceeds your maximum limit, we have adjusted it to your eligible maximum";
	    		 }
				 if(parseInt($scope.newTPDRequireCover) > 750000){
	    			$scope.newTPDRequireCover = 750000 ;
	    			$scope.tpdWarningFlag = true;
	    			$scope.tpdLimitMsg = "The selected benefit exceeds your maximum limit, we have adjusted it to your eligible maximum";
	    		}
          }else if($scope.deathTpdcheckboxState == false){
        		$scope.isNewDeathcoverTypeDisabled = false;
                $scope.isNewDeathrequireCoverDisabled = false;
                $scope.isNewTpdCoverTypeDisabled = false;
	            $scope.isNewTPDRequireCoverDisabled = false;
	            $scope.deathWarningFlag = false;
	            $scope.tpdWarningFlag = false;
	            $scope.newDeathrequireCover = '';
	            $scope.newTPDRequireCover = '';
	            $scope.deathLimitMsg = '';
	            $scope.tpdLimitMsg = '';
         }
	
	}, 10);
};

$scope.isUnitEmpty = function(value){
			  return ((value == "" || value == null) || value == "0");
	     }; 
	     
	     
$scope.IPWarningFlag = false;     			            			    		
/*$scope.validateIPUnit = function(){
		//$scope.newIpOccupationCategory = newOccupationCategory[0].ipfixedcategeory;
var newCat = newOccupationCategory[0].ipfixedcategeory;
		       if(newCat == "Professional"){
                       if(IPAmount > 24 ){
		                    $scope.newIPRequireCover = 24;
		                    $scope.IPWarningFlag = true;
		                  }else if(IPAmount <= 24 ){
		                	  $scope.IPWarningFlag = false;
		                	  $scope.newIPRequireCover = IPAmount;
		                  }
                     }else if(newCat == "Office"){
                         if(IPAmount > 17 ){
	                         $scope.newIPRequireCover = 17;
	                         $scope.IPWarningFlag = true;
			              }else if(IPAmount <= 17  ){
			            	 $scope.newIPRequireCover = IPAmount;
			            	 $scope.IPWarningFlag = false; 
			              }
                     }else if(newCat == "General"){
                         if(IPAmount > 12){
	                         $scope.newIPRequireCover = 12;
	                         $scope.IPWarningFlag = true;
	    	              }else if(IPAmount <= 12){
	    	            	  $scope.newIPRequireCover = IPAmount;
	    	            	  $scope.IPWarningFlag = false; 
	    	              }
	                 }
                     
	 $scope.calculateNewQuoteOnChange();
};*/

$scope.validateIPUnitOnChange = function(){
	//$scope.newIpOccupationCategory = newOccupationCategory[0].ipfixedcategeory;
		var ipMaxLimit;
		var newCat = newOccupationCategory[0].ipfixedcategeory;
		var ipMaxLimitDependOcc;
		if(newCat == "Professional"){
			ipMaxLimitDependOcc = 24;
		}else if(newCat == "Office"){
			ipMaxLimitDependOcc = 17;
		}else if(newCat == "General"){
			ipMaxLimitDependOcc = 12;
		}
	     
		// Checking for IP max
		  if(IPAmount < ipMaxLimitDependOcc){
			  ipMaxLimit =  IPAmount;
		  }else{
			  ipMaxLimit = ipMaxLimitDependOcc; 
		  }
		  
		  
		  if(parseInt($scope.newIPRequireCover) >ipMaxLimit ){
			  $scope.newIPRequireCover = ipMaxLimit; 
			  $scope.IPWarningFlag = true;
		  }else{
			  $scope.newIPRequireCover =  $scope.newIPRequireCover ;
		  }
        
                 
 $scope.calculateNewQuoteOnChange();
};

/*$scope.changeIPValue = function(){
	
		$scope.IPWarningFlag = false;
		$timeout(function(){
			ipCheckboxState = $('#ipsalarycheck').prop('checked');
			if(ipCheckboxState == true){
      		if(parseInt($scope.newAnnualSalary) < 423530){
      			IPAmount  = Math.ceil((0.85 * ($scope.newAnnualSalary/12)) / 425);
      			$scope.validateIPUnit();
      		} else if(parseInt($scope.newAnnualSalary) > 423530 && parseInt($scope.newAnnualSalary) < 623530){
      			IPAmount = Math.ceil((Math.round(((parseInt($scope.newAnnualSalary) - 423530)/12)*0.6) + 30000)/425);
      			$scope.validateIPUnit();
      		} else if(parseInt($scope.newAnnualSalary) > 623530){
      			IPAmount = 95;
      			$scope.validateIPUnit();
      		}
      	//	$scope.validateIPUnit();
      		$scope.checkIPCover();
  		} else if(ipCheckboxState == false){
  			$scope.newIPRequireCover = '';
  			$scope.IPWarningFlag = false;
  		}
		
		}, 10);
	};*/
		
   $scope.checkIpCoverValue = function(){
    	$scope.IPWarningFlag = false;
 		if(parseInt($scope.newAnnualSalary) < 423530){
 			IPAmount  = Math.ceil((0.85 * ($scope.newAnnualSalary/12)) / 425);
 			$scope.validateIPUnitOnChange();
 		} else if(parseInt($scope.newAnnualSalary) > 423530 && parseInt($scope.newAnnualSalary) < 623530){
 			IPAmount = Math.ceil((Math.round(((parseInt($scope.newAnnualSalary) - 423530)/12)*0.6) + 30000)/425);
 			$scope.validateIPUnitOnChange();
 		} else if(parseInt($scope.newAnnualSalary) > 623530){
 			IPAmount = 95;
 			$scope.validateIPUnitOnChange();
 		}
 	//	$scope.calculateNewQuoteOnChange();
    };
	    	
	
    // Progressive validation
    var newMemberContactFormFields = ['newContactEmail', 'newContactPhone','newContactPrefTime'];
    
    while($scope.newPersonalDetails){
		if($scope.newPersonalDetails.gender == null || $scope.newPersonalDetails.gender == ""){
			$scope.genderFlag =  false;
			$scope.gender = '';
			var newMemberOccupationFormFields = ['workTimeAndSalary',/*'areyouperCitzNewMemberQuestion',*/'newIndustry','newOccupation','newWithinOfficeQuestion','newTertiaryQuestion','newManagementRole','gender','newAnnualSalary'/*,'tpdClaim','terminalIllnessClaim'*/];
		    var newMemberOccupationOtherFormFields = ['workTimeAndSalary',/*'areyouperCitzNewMemberQuestion',*/'newIndustry','newOccupation','newOtherOccupation','newWithinOfficeQuestion','newTertiaryQuestion','newManagementRole','gender','newAnnualSalary'/*,'tpdClaim','terminalIllnessClaim'*/];
		} else{
			$scope.genderFlag =  true;
			$scope.gender = $scope.newPersonalDetails.gender;
			var newMemberOccupationFormFields = ['workTimeAndSalary',/*'areyouperCitzNewMemberQuestion',*/'newIndustry','newOccupation','newWithinOfficeQuestion','newTertiaryQuestion','newManagementRole','newAnnualSalary'/*,'tpdClaim','terminalIllnessClaim'*/];
		    var newMemberOccupationOtherFormFields = ['workTimeAndSalary',/*'areyouperCitzNewMemberQuestion',*/'newIndustry','newOccupation','newOtherOccupation','newWithinOfficeQuestion','newTertiaryQuestion','newManagementRole','newAnnualSalary'/*,'tpdClaim','terminalIllnessClaim'*/];
		}
		break;
	}
   /* var newMemberOccupationFormFields = ['workTimeAndSalary','areyouperCitzNewMemberQuestion','newIndustry','newOccupation','newWithinOfficeQuestion','newTertiaryQuestion','newManagementRole','newAnnualSalary','tpdClaim','terminalIllnessClaim'];
    var newMemberOccupationOtherFormFields = ['workTimeAndSalary','areyouperCitzNewMemberQuestion','newIndustry','newOccupation','newOtherOccupation','newWithinOfficeQuestion','newTertiaryQuestion','newManagementRole','newAnnualSalary','tpdClaim','terminalIllnessClaim'];*/
   var newMembercoverCalculatorFormFields =['newDeathcoverType','newDeathrequireCover','newTpdCoverType','newTPDRequireCover'];

    $scope.checkNewMemberPreviousMandatoryFields  = function (elementName,formName){
    	var newFormFields;
    	if(formName == 'newMemberContactForm'){
    		newFormFields = newMemberContactFormFields;
    	} else if(formName == 'newMemberOccupationForm'){
    		if($scope.newOccupation != undefined && $scope.newOccupation == 'Other'){
    			newFormFields = newMemberOccupationOtherFormFields;
    		} else{
    			newFormFields = newMemberOccupationFormFields;
    		}
    	} else if(formName == 'newMembercoverCalculatorForm'){
    		newFormFields = newMembercoverCalculatorFormFields;
    	}
      var inx = newFormFields.indexOf(elementName);
    	      if(inx > 0){
        for(var i = 0; i < inx ; i++){
          $scope[formName][newFormFields[i]].$touched = true;
        }
      }
    };
    
$scope.convertNewDeathUnitsToAmount = function(){
	var deathReqObject = {
    		"age": anb,
    		"fundCode": "FIRS",
    		"gender": $scope.gender,
    		"deathOccCategory": $scope.newDeathOccupationCategory,
    		"smoker": false,
    		"deathUnits": 1,
   		    "deathUnitsCost": null,
    		"premiumFrequency": "Weekly",
    		"memberType": null,
    		"manageType": "NCOVER",
    		"deathCoverType": "DcUnitised"
    	};
	if(deathCallFlag){
		CalculateDeathService.calculateAmount($scope.urlList.calculateDeathUrl,deathReqObject).then(function(res){
		//CalculateDeathService.calculateAmount({}, deathReqObject, function(res){
			deathCallFlag = false;
			newDeathAmount = res.data[0].coverAmount;
			$scope.validateDeathTPDAmount();
		}, function(err){
			console.log('Error while fetching death amount ' + err);
		});
	} else{
		$scope.validateDeathTPDAmount();
	}
};
    	
$scope.convertNewTPDUnitsToAmount = function(){
	var TPDReqObject = {
    		"age": anb,
    		"fundCode": "FIRS",
    		"gender":$scope.gender,
    		"tpdOccCategory": $scope.newTpdOccupationCategory,
    		"smoker": false,
    		"tpdUnits": 1,
    		"tpdUnitsCost": null,
    		"premiumFrequency": "Weekly",
    		"memberType": null,
    		"manageType": "NCOVER",
    		"tpdCoverType": "TPDUnitised",
    	};
	if(tpdCallFlag){
		CalculateTPDService.calculateAmount($scope.urlList.calculateTpdUrl,TPDReqObject).then(function(res){
		//CalculateTPDService.calculateAmount({}, TPDReqObject, function(res){
			tpdCallFlag = false;
			newTPDAmount = res.data[0].coverAmount;
			$scope.validateDeathTPDAmount();
		}, function(err){
			console.log('Error while fetching TPD amount ' + err);
		});
	} else{
		$scope.validateDeathTPDAmount();
	}
};
  //temp to test aura integration
    $scope.gotoAura = function () {
  	  $location.path('/auraspecialoffer/1');
  	};
   // validate fields "on continue"
  	$scope.newMemberFormSubmit =  function (form){
        if(!form.$valid){
      	  form.$submitted=true; 
  	  } else{
  		  if(form.$name == 'newMemberContactForm'){
      	    $scope.toggleTwo();
  		  } else if(form.$name == 'newMemberOccupationForm'){
  			  if(!$scope.eligibilityViolated){
  				  $scope.toggleThree();
  			  }
  		  }else if(form.$name == 'newMembercoverCalculatorForm'){
		          if(!$scope.coverAmountFlag) {
			 				$scope.calculateQuote();  
			 			}  
  		}
  	  }
      };
      
      $scope.newAckFlag = false;
	  var newAckCheck;
	  
	  $scope.checkNewAckState = function(){
	    	$timeout(function(){
	    		newAckCheck = $('#newTermsLabel').hasClass('active');
	    		if(newAckCheck){
	    			$scope.newAckFlag = false;
	    		} else{
	    			$scope.newAckFlag = true;
	    		}
	    	}, 10);
	    };
	    
	    
      $scope.saveNewDataForPersistence = function(){
	    	var newCoverObj = {};
	    	var newCoverStateObj = {};
	    	var newOccCoverObj = {};
	    	var newDeathAddnlObj={};
	    	var newTpdAddnlObj={};
	    	var newIpAddnlObj={};
	    	
	    	var selectedIndustry = $scope.IndustryOptions.filter(function(obj){
	    		return $scope.newIndustry == obj.key;
	    	});
	    	
	    	newCoverObj['name'] = $scope.newPersonalDetails.firstName+" "+$scope.newPersonalDetails.lastName;
	    	newCoverObj['dob'] = $scope.newPersonalDetails.dateOfBirth;
	    	newCoverObj['email'] = $scope.newContactEmail;
	    	newOccCoverObj['gender'] = $scope.gender;
	    	newCoverObj['contactType']=$scope.preferredContactType;
	    	newCoverObj['contactPhone'] = $scope.newContactPhone;
	    	newCoverObj['contactPrefTime'] = $scope.newContactPrefTime;
	    	
	    	newOccCoverObj['fifteenHr'] = $scope.workTimeAndSalary;
	    	/*newOccCoverObj['citizenQue'] = $scope.areyouperCitzNewMemberQuestion;*/
	    	newOccCoverObj['industryName'] = selectedIndustry[0].value;
	    	newOccCoverObj['industryCode'] = selectedIndustry[0].key;
	    	newOccCoverObj['occupation'] = $scope.newOccupation;
	    	newOccCoverObj['withinOfficeQue']= $scope.newWithinOfficeQuestion;
	    	newOccCoverObj['tertiaryQue']= $scope.newTertiaryQuestion;
	    	newOccCoverObj['managementRoleQue']= $scope.newManagementRole;
	    	newOccCoverObj['salary'] = $scope.newAnnualSalary;
	    	newOccCoverObj['otherOccupation'] = $scope.otherOccupationObj.newOtherOccupation;
	    	
	    	/*newCoverObj['previousTpdClaimQue'] = $scope.tpdClaim;
	    	newCoverObj['terminalIllClaimQue'] = $scope.terminalIllnessClaim;*/
	    	
	    	newCoverObj['newExistingDeathAmt'] = $scope.newMemberDeathCoverDetails.amount;
	    	newCoverObj['newExistingDeathUnits'] = $scope.newMemberDeathCoverDetails.units;
	    	newCoverObj['deathOccCategory'] = $scope.newDeathOccupationCategory;
	    	newCoverObj['tpdOccCategory'] = $scope.newTpdOccupationCategory;
	    	newCoverObj['newExistingIPUnits'] = $scope.newMemberIpCoverDetails.units;
	    	newCoverObj['ipOccCategory'] = $scope.newIpOccupationCategory;
	    	newCoverObj['newExistingTpdAmt'] = $scope.newMemberTpdCoverDetails.amount;
	    	newCoverObj['newExistingTPDUnits'] = $scope.newMemberTpdCoverDetails.units;
	    	//newCoverObj['ipcheckbox'] = ipCheckboxState;
	    	
	    	newDeathAddnlObj['newDeathCoverType'] = $scope.newDeathcoverType;
	    	newDeathAddnlObj['newDeathLabelAmt'] = $scope.dcNewCoverAmount;
	    	newDeathAddnlObj['newDeathUpdatedCover'] = $scope.newDeathrequireCover;
	    	newDeathAddnlObj['newDeathCoverPremium'] = $scope.dcNewWeeklyCost;
	    	
	    	newTpdAddnlObj['newTpdCoverType'] = $scope.newTpdCoverType;
	    	newTpdAddnlObj['newTpdLabelAmt'] = $scope.tpdNewCoverAmount;
	    	newTpdAddnlObj['newTpdUpdatedCover'] = $scope.newTPDRequireCover;
	    	newTpdAddnlObj['newTpdCoverPremium'] = $scope.tpdNewWeeklyCost;
	    	
	    	newIpAddnlObj['newWaitingPeriod'] = $scope.waitingPeriodAddnl;
	    	newIpAddnlObj['newBenefitPeriod'] = $scope.benefitPeriodAddnl;
	    	newIpAddnlObj['newIpLabelAmt'] = $scope.ipNewCoverAmount;
	    	newIpAddnlObj['newIpUpdatedCover'] = $scope.newIPRequireCover;
	    	newIpAddnlObj['newIpCoverPremium'] = $scope.ipNewWeeklyCost;
	    	newIpAddnlObj['newIpCoverType'] = 'IpUnitised';
	    	
	    	newCoverObj['totalPremium'] = $scope.totalNewWeeklyCost;
	    	
	    	newCoverObj['newDeathTpdcheckboxState'] = $scope.deathTpdcheckboxState;
	    	newCoverObj['newOccupationCategory'] = newOccupationCategory;
	    	newCoverObj['appNum'] = appNum;
	    	newCoverObj['newAckCheck'] = newAckCheck;
	    	newCoverObj['age'] = anb;
	    	newCoverObj['manageType'] = 'NCOVER';
	    	newCoverObj['partnerCode'] = 'FIRS';
	    	newCoverObj['freqCostType'] = 'Weekly';
	    	
	    	newCoverObj['lastSavedOn'] = 'NewMemberQuotepage';
	    	
	    	newCoverStateObj['isNewDeathcoverTypeDisabled'] = $scope.isNewDeathcoverTypeDisabled;
	    	newCoverStateObj['isNewDeathrequireCoverDisabled'] = $scope.isNewDeathrequireCoverDisabled;
	    	newCoverStateObj['isNewTpdCoverTypeDisabled'] = $scope.isNewTpdCoverTypeDisabled;
	    	newCoverStateObj['isNewTPDRequireCoverDisabled'] = $scope.isNewTPDRequireCoverDisabled;
	    	newCoverObj['indexationDeath'] = $scope.indexation.death;
	    	newCoverObj['indexationTpd'] = $scope.indexation.tpd;
	    	newCoverStateObj['newDynamicFlag'] = newDynamicFlag;
	    	/*newCoverStateObj['expandOrCollapseFlag'] = expandOrCollapseFlag;*/
	    	newCoverStateObj['isDeathDisabled'] = $scope.isDeathDisabled;
	    	newCoverStateObj['isTPDDisabled'] = $scope.isTPDDisabled;
	    	newCoverStateObj['isIPDisabled'] = $scope.isIPDisabled;
	    	
	    	
	    	PersistenceService.setNewMemberCoverDetails(newCoverObj);
	    	PersistenceService.setNewMemberCoverStateDetails(newCoverStateObj);
	    	PersistenceService.setNewMemberCoverOccDetails(newOccCoverObj);
	    	PersistenceService.setNewMemberDeathAddnlDetails(newDeathAddnlObj);
	    	PersistenceService.setNewMemberTpdAddnlDetails(newTpdAddnlObj);
	    	PersistenceService.setNewMemberIpAddnlDetails(newIpAddnlObj);
	    	
	    };
	    
	    $scope.goToAuraPage = function () {
	    	if(this.newMemberContactForm.$valid && this.newMemberOccupationForm.$valid && this.newMembercoverCalculatorForm.$valid && !$scope.eligibilityViolated){
	    	 $timeout(function(){
		    	newAckCheck = $('#newTermsLabel').hasClass('active');
	    		if(newAckCheck){
	    			$scope.saveNewDataForPersistence(); 
	    			$scope.newAckFlag = false;
	    	 $scope.gotoAura();
	    		}else{
	    			$scope.newAckFlag = true;
	    		}
		      }, 10);
	    	}
	    }; 
	    
	   /* $scope.saveNewMemberQuote =function(){
	    	$scope.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
	    	$scope.saveNewDataForPersistence();
    		$scope.newMemberQuoteObject =  PersistenceService.getNewMemberCoverDetails();
    		$scope.newOccDetails = PersistenceService.getNewMemberCoverOccDetails();
    		$scope.deathAddnlDetails=PersistenceService.getNewMemberDeathAddnlDetails();
    	    $scope.tpdAddnlDetails=PersistenceService.getNewMemberTpdAddnlDetails();
    	    $scope.ipAddnlDetails=PersistenceService.getNewMemberIpAddnlDetails();
    	    $scope.personalDetails = persoanlDetailService.getMemberDetails();
    	    
    	    
    	    if($scope.newMemberQuoteObject != null && $scope.newOccDetails != null && $scope.deathAddnlDetails != null && $scope.tpdAddnlDetails != null && $scope.ipAddnlDetails != null && $scope.personalDetails[0] != null){
    	    	$scope.details={};
        		$scope.details.addnlDeathCoverDetails = $scope.deathAddnlDetails;
    			$scope.details.addnlTpdCoverDetails = $scope.tpdAddnlDetails;
    			$scope.details.addnlIpCoverDetails = $scope.ipAddnlDetails;
    			$scope.details.occupationDetails = $scope.newOccDetails;
    	    	var coverDetails = angular.extend($scope.details,$scope.newMemberQuoteObject);
        	    var saveNewMemberQuoteObject = angular.extend(coverDetails , $scope.personalDetails[0]);
    	    	auraResponseService.setResponse(saveNewMemberQuoteObject)
    	        saveEapply.reqObj().then(function(response) {  
    	                console.log(response.data)
    	        });
    	    }
	    };*/
	    
	  /*  $scope.saveAndExitPopUp = function (hhText) {
	      	
			var dialog1 = ngDialog.open({
				    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
					className: 'ngdialog-theme-plain custom-width',
					preCloseCallback: function(value) {
					       var url = "/landing"
					       $location.path( url );
					       return true
					},
					plain: true
			});
			dialog1.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};*/
		
	   /* $scope.clickToOpen = function (hhText) {
	      	
			var dialog = ngDialog.open({
				template: '<p>'+hhText+'</p>' +
					'<div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(1)">Close Me</button></div>',
					template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="">Close</button></div></div>',
					className: 'ngdialog-theme-plain',
					plain: true
			});
			dialog.closePromise.then(function (data) {
				console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};*/
		
	    if($routeParams.mode == 2){
	    	var newExistingDetails = PersistenceService.getNewMemberCoverDetails();
	    	var newStateDetails = PersistenceService.getNewMemberCoverStateDetails();
	    	var newOccDetails = PersistenceService.getNewMemberCoverOccDetails();
    	    var deathAddnlDetails=PersistenceService.getNewMemberDeathAddnlDetails();
    	    var tpdAddnlDetails=PersistenceService.getNewMemberTpdAddnlDetails();
    	    var ipAddnlDetails=PersistenceService.getNewMemberIpAddnlDetails();
	    	
	    	$scope.newContactEmail = newExistingDetails.email;
	    	$scope.preferredContactType=newExistingDetails.contactType;
	    	$scope.newContactPhone = newExistingDetails.contactPhone;
	    	$scope.newContactPrefTime = newExistingDetails.contactPrefTime;
	    	
	    	$scope.workTimeAndSalary = newOccDetails.fifteenHr;
	    	/*$scope.areyouperCitzNewMemberQuestion = newOccDetails.citizenQue;*/
	    	$scope.newIndustry = newOccDetails.industryCode;
	    	//$scope.occupation = newExistingDetails.occupation;
	    	$scope.newWithinOfficeQuestion = newOccDetails.withinOfficeQue;
	    	$scope.newTertiaryQuestion = newOccDetails.tertiaryQue;
	    	$scope.newManagementRole = newOccDetails.managementRoleQue;
	    	$scope.gender = newOccDetails.gender;
	    	$scope.newAnnualSalary = newOccDetails.salary;
	    	$scope.otherOccupationObj.newOtherOccupation = newOccDetails.otherOccupation;
	    	
	    	/*$scope.tpdClaim = newExistingDetails.previousTpdClaimQue;
	    	$scope.terminalIllnessClaim = newExistingDetails.terminalIllClaimQue;*/
	    	
	    	$scope.newMemberDeathCoverDetails.amount = newExistingDetails.newExistingDeathAmt;
	    	$scope.newMemberDeathCoverDetails.units = newExistingDetails.newExistingDeathUnits;
	    	$scope.newMemberTpdCoverDetails.amount = newExistingDetails.newExistingTpdAmt;
	    	$scope.newMemberTpdCoverDetails.units = newExistingDetails.newExistingTPDUnits;
	    	$scope.newMemberIpCoverDetails.units = newExistingDetails.newExistingIPUnits;
	    	$scope.newIpOccupationCategory = newExistingDetails.ipOccCategory;
	    	$scope.newDeathOccupationCategory = newExistingDetails.deathOccCategory;
	    	$scope.newTpdOccupationCategory = newExistingDetails.tpdOccCategory;
	    	
	    	//ipCheckboxState = newExistingDetails.ipcheckbox;
	    	$scope.newDeathcoverType = deathAddnlDetails.newDeathCoverType;
	    	$scope.dcNewCoverAmount = deathAddnlDetails.newDeathLabelAmt;
	    	$scope.newDeathrequireCover = deathAddnlDetails.newDeathUpdatedCover;
	    	$scope.dcNewWeeklyCost = deathAddnlDetails.newDeathCoverPremium;
	    	
	    	$scope.newTpdCoverType = tpdAddnlDetails.newTpdCoverType;
	    	$scope.tpdNewCoverAmount = tpdAddnlDetails.newTpdLabelAmt;
	    	$scope.newTPDRequireCover = tpdAddnlDetails.newTpdUpdatedCover;
	    	$scope.tpdNewWeeklyCost = tpdAddnlDetails.newTpdCoverPremium;
	    	
	    	$scope.waitingPeriodAddnl = ipAddnlDetails.newWaitingPeriod;
	    	$scope.benefitPeriodAddnl = ipAddnlDetails.newBenefitPeriod;
	    	$scope.ipNewCoverAmount = ipAddnlDetails.newIpLabelAmt;
	    	$scope.newIPRequireCover = ipAddnlDetails.newIpUpdatedCover;
	    	$scope.ipNewWeeklyCost = ipAddnlDetails.newIpCoverPremium;
	    	
	    	$scope.totalNewWeeklyCost = newExistingDetails.totalPremium;
	    	newOccupationCategory = newExistingDetails.newOccupationCategory;
	    	appNum = newExistingDetails.appNum;
	    	newAckCheck = newExistingDetails.newAckCheck;
	    	$scope.deathTpdcheckboxState = newExistingDetails.newDeathTpdcheckboxState;
	    	
	    	
	    	$scope.isNewDeathcoverTypeDisabled = newStateDetails.isNewDeathcoverTypeDisabled;
	    	$scope.isNewDeathrequireCoverDisabled = newStateDetails.isNewDeathrequireCoverDisabled;
	    	$scope.isNewTpdCoverTypeDisabled = newStateDetails.isNewTpdCoverTypeDisabled;
	    	$scope.isNewTPDRequireCoverDisabled = newStateDetails.isNewTPDRequireCoverDisabled;
	        $scope.indexation.death = newStateDetails.indexationDeath;
	    	$scope.indexation.tpd = newStateDetails.indexationTpd;
	    	/*expandOrCollapseFlag = newStateDetails.expandOrCollapseFlag;*/
	    	newDynamicFlag = newStateDetails.newDynamicFlag;
	    	$scope.isDeathDisabled = newStateDetails.isDeathDisabled;
	    	$scope.isTPDDisabled = newStateDetails.isTPDDisabled;
	    	$scope.isIPDisabled = newStateDetails.isIPDisabled;
	    	
	    	/*if($scope.terminalIllnessClaim == 'No' && $scope.tpdClaim == "Yes"){
				  expandOrCollapseFlag = true;	
  			      $scope.toggleThreeDeath();
			  } else if($scope.terminalIllnessClaim == 'No' && $scope.tpdClaim == "No"){
				  expandOrCollapseFlag = true;	*/
  			      $scope.toggleThree();
			 /* }*/
	    	
  			    OccupationService.getOccupationList($scope.urlList.occupationUrl,"FIRS",$scope.newIndustry).then(function(res){
	    	//OccupationService.getOccupationList({fundId:"FIRS", induCode:$scope.newIndustry}, function(occupationList){
	        	$scope.OccupationList = res.data;
	        	var temp = $scope.OccupationList.filter(function(obj){
	        		return obj.occupationName == newOccDetails.occupation;
	        	});
	        	$scope.newOccupation = temp[0].occupationName;
	        }, function(err){
	        	console.log("Error while fetching occupation options " + JSON.stringify(err));
	        });
	    	/*if(ipCheckboxState){
	    		$('#ipsalarycheck').parent().addClass('active');
	    	}*/
	    	if($scope.deathTpdcheckboxState){
	    		$('#increaseCoverAmount').parent().addClass('active');	    		
	    	}
	    	
	    	if(newAckCheck){
	    	    $timeout(function(){
	    			$('#newTermsLabel').addClass('active');
	    			   });
	    			 }
	    	$scope.toggleTwo();
	    }
    $('.selectpicker').selectpicker();
        
        
    }]); 
 
    
 /*New Member Change Cover Controller ,Progressive and Mandatory Validations Ends   */

FirstSuperApp.directive('phoneOnly', function(){
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {

            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue ? inputValue.replace(/[^\d.-]/g,'') : null;

                if (transformedInput!=inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
            
            $(element).on("keyup", function() {
            	var TempVal=$(this).val().replace(" ","");
             	var regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
            	if( !regex.test(TempVal)) {
            		// element.controller('ngModel').$setValidity('required', false);
            		// element.controller('ngModel').$touched = true;
            	}
            	
            })
        }
    };
});