package au.com.metlife.eapply.ct.model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LoginAttemptRepository extends JpaRepository<au.com.metlife.eapply.ct.model.LoginAttempts, String>{
	public au.com.metlife.eapply.ct.model.LoginAttempts findByIpAddress(String ipAddress);	   
	/*public void saveOrUpdateLoginAttempts(LoginAttempts loginAttempts);*/
}
