package au.com.metlife.eapply.bs.bizflowmodel;

import java.sql.Timestamp;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement (name="MessageHeader")
public class MessageHeader {
	
	private Sender Sender;
	
	private SendingSystem SendingSystem;
	private String ACORDStandardVersionCode;
	
	private String MessageId;
	
	private String CorrelationId;
	
	private Timestamp MessageDateTime;
	
	public String getACORDStandardVersionCode() {
		return ACORDStandardVersionCode;
	}
	public void setACORDStandardVersionCode(String standardVersionCode) {
		ACORDStandardVersionCode = standardVersionCode;
	}
	@XmlElement(name="CorrelationId")
	public String getCorrelationId() {
		return CorrelationId;
	}
	public void setCorrelationId(String correlationId) {
		CorrelationId = correlationId;
	}
	@XmlJavaTypeAdapter(TimestampAdapter.class)
	@XmlElement(name="MessageDateTime")
	public Timestamp getMessageDateTime() {
		return MessageDateTime;
	}
	
	public void setMessageDateTime(Timestamp messageDateTime) {
		MessageDateTime = messageDateTime;
	}
	@XmlElement(name="MessageId")
	public String getMessageId() {
		return MessageId;
	}
	public void setMessageId(String messageId) {
		MessageId = messageId;
	}
	@XmlElement(name="Sender")
	public Sender getSender() {
		return Sender;
	}
	public void setSender(Sender sender) {
		Sender = sender;
	}
	@XmlElement(name="SendingSystem")
	public SendingSystem getSendingSystem() {
		return SendingSystem;
	}
	public void setSendingSystem(SendingSystem sendingSystem) {
		SendingSystem = sendingSystem;
	}
	
}
