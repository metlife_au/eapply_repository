package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.OAMPSGetQuotePageObjects;
import uimap.metlife.OAMPSHealthPageObjects;
import uimap.metlife.PersonalStatementPageObjects;




public class OAMPSHealthPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public OAMPSHealthPage yourHealth() {
		yourHealthDetails();
		return new OAMPSHealthPage(scriptHelper);
	}

	public OAMPSHealthPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void yourHealthDetails() {

		String height = dataTable.getData("MetLife_Data", "Height");
		String weight = dataTable.getData("MetLife_Data", "Weight");
		// exclusion
		String diagnosed = dataTable.getData("MetLife_Data", "Diagnosed");
		String Threeyears    = dataTable.getData("MetLife_Data", "3yrs");
		String Fiveyears    = dataTable.getData("MetLife_Data", "5yrs");
		
		String familyHistory = dataTable.getData("MetLife_Data", "FamilyHistory");
		String lifeStyle = dataTable.getData("MetLife_Data", "LifeStyleQuestions");
		String alchohol = dataTable.getData("MetLife_Data", "Alchohol.Quantity");
		String alchoholReduce = dataTable.getData("MetLife_Data", "Alchohol.Reduce");
		String hiv = dataTable.getData("MetLife_Data", "HIV");
		String hivRisk = dataTable.getData("MetLife_Data", "HIV.Risk");
		String general = dataTable.getData("MetLife_Data", "General.Questions");
		String insuranceExcl = dataTable.getData("MetLife_Data", "Exclusion");
		String insuranceClaim = dataTable.getData("MetLife_Data", "Claim");
		
		String gender = dataTable.getData("MetLife_Data", "Gender");
		String pregnant = dataTable.getData("MetLife_Data", "Pregnant");
	
		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
			tryAction(waitForClickableElement(OAMPSHealthPageObjects.txtheight), "SET", "Height", height);
			tryAction(waitForClickableElement(OAMPSHealthPageObjects.txtheight), "TAB", "Height");
			
			Thread.sleep(1000);
			
			tryAction(waitForClickableElement(OAMPSHealthPageObjects.txtweight), "SET", "Weight", weight);
			tryAction(waitForClickableElement(OAMPSHealthPageObjects.txtweight), "TAB", "Weight");
			
			if (Threeyears.equalsIgnoreCase("None")) {
			tryAction(waitForClickableElement(OAMPSHealthPageObjects.chklast3yrs),"Click", "Have you suffered,diagnosed in last 3 years - None");
			Thread.sleep(1000);
			// loading
			} else if(Threeyears.equalsIgnoreCase("Lung")) {
				
				tryAction(waitForClickableElement(OAMPSHealthPageObjects.chklast3yrslungissue),"Click", "Have you suffered from, diagnosed with/sought medical advice - Lung or breathing condition");
				Thread.sleep(200);
				tryAction(waitForClickableElement(OAMPSHealthPageObjects.chklast3yrslungissueasthma),"Click", "Have you suffered from, diagnosed with/sought medical advice - Asthma");		
				Thread.sleep(200);
				tryAction(fluentWaitElement(OAMPSHealthPageObjects.rdasthmaModerate), "clkradio", "Is your condition: Moderate" );
				Thread.sleep(200);
				tryAction(fluentWaitElement(OAMPSHealthPageObjects.rdasthmaworsenedNo), "clkradio", "Is your asthma worsened by your occupation" );
				
			}
			
			tryAction(waitForClickableElement(OAMPSHealthPageObjects.chklast5yrs),"Click", "Have you suffered,diagnosed in last 5 years - None");
			Thread.sleep(1000);
			
			if (diagnosed.equalsIgnoreCase("None")) {
			tryAction(waitForClickableElement(OAMPSHealthPageObjects.chkmedicaladvice),"Click", "Have you suffered from, diagnosed with/sought medical advice - None");
			Thread.sleep(1000);
						
			// exclusion
			} else if(diagnosed.equalsIgnoreCase("Bone")) {
				
				tryAction(waitForClickableElement(OAMPSHealthPageObjects.chkmedicaladvicebone),"Click", "Have you suffered from, diagnosed with/sought medical advice - Bone, joint or limp");
				Thread.sleep(200);
				tryAction(waitForClickableElement(OAMPSHealthPageObjects.chkmedicaladvicelossbone),"Click", "Have you suffered from, diagnosed with/sought medical advice - Loss");		
				Thread.sleep(200);
				tryAction(fluentWaitElement(OAMPSHealthPageObjects.rdadvicepractitionerYes), "clkradio", "Following  advice of regular practitioner" );
				Thread.sleep(200);
				tryAction(fluentWaitElement(OAMPSHealthPageObjects.rdfracturesNo), "clkradio", "Have you suffered any fractures" );
				Thread.sleep(200);
			}
			
			if (gender.equalsIgnoreCase("Female")) {
			tryAction(pregnant.equalsIgnoreCase("Yes")?waitForClickableElement(OAMPSHealthPageObjects.rdPregnantYes):waitForClickableElement(OAMPSHealthPageObjects.rdPregnantNo),"clkradio","Pregnant -" + pregnant);
			sleep();
			
			String strfamilyHistory = "input[value*='[11082]No']+*";
			String strlifeStyle = "input[id*='radio1-10:1']";
			String stralchoholReduce = "input[id*='radio1-12:1']";
			String strhivNo = "input[id*='radio1-13:1']";
			String strhivYes = "input[id*='radio1-13:0']";
			String strhivRisk = "input[id*='radio1-13_1_0:1']";
			String strgeneral = "input[id*='radio1-14:1']";
			String strinsuranceExclNo ="input[id*='radio1-15:1']";
			String strinsuranceClaimNo ="input[id*='radio1-16:1']";
			String strtxtalchohol = "ran1-11";
			String strEnter = "a[id='ran211-Lifestyle_Questions-Lifestyle_Questions-11']";
						
			tryAction(waitForClickableElement(By.cssSelector(strfamilyHistory)), "clkradio", "Family History");
			tryAction(fluentWaitElement(By.cssSelector(strlifeStyle)), "clkradio", "LifeStyle");
			tryAction(fluentWaitElement(By.cssSelector(stralchoholReduce)), "clkradio", "AlchoholReduce");
			tryAction(waitForClickableElement(By.id(strtxtalchohol)), "SET", "AlchoholQuantity", alchohol);
			tryAction(fluentWaitElement(By.cssSelector(strEnter)), "Click", "Apply");

			if (hiv.equalsIgnoreCase("No")) {
				tryAction(fluentWaitElement(By.cssSelector(strhivNo)), "clkradio", "HIV");
			} else {
				tryAction(fluentWaitElement(By.cssSelector(strhivYes)), "clkradio", "HIV");
			}
			
			if (hivRisk.equalsIgnoreCase("No")) {
			tryAction(fluentWaitElement(By.cssSelector(strhivRisk)), "clkradio", "HIVRisk");
			}
			tryAction(fluentWaitElement(By.cssSelector(strgeneral)), "clkradio", "General");
			tryAction(fluentWaitElement(By.cssSelector(strinsuranceExclNo)), "clkradio",
					"Insurance Exclusion");
			tryAction(fluentWaitElement(By.cssSelector(strinsuranceClaimNo)), "clkradio", "Insurance Details");			
			}
			else { 
			
				if (familyHistory.equalsIgnoreCase("No")) {
					tryAction(fluentWaitElement(OAMPSHealthPageObjects.chkfamilyhistoryNo), "clkradio", "Family History" );
				} else {
					tryAction(fluentWaitElement(OAMPSHealthPageObjects.chkfamilyhistoryNo), "clkradio", "Family History" );
				} // handled only 'No' condition so far 
				
				
				if (lifeStyle.equalsIgnoreCase("No")) {
					tryAction(fluentWaitElement(OAMPSHealthPageObjects.chklifestyleNo), "clkradio", "LifeStyle" );
				} else {
					tryAction(fluentWaitElement(OAMPSHealthPageObjects.chklifestyleNo), "clkradio", "LifeStyle" );
				} // handled only 'No' condition so far 
										
				tryAction(waitForClickableElement(OAMPSHealthPageObjects.txtalchohol), "SET", "AlchoholQuantity", alchohol);
				tryAction(waitForClickableElement(OAMPSHealthPageObjects.txtalchohol), "TAB", "AlchoholQuantity");
				
				tryAction(fluentWaitElement(OAMPSHealthPageObjects.btnEnter), "Click", "Apply");
				
				Thread.sleep(1000);
				
				if (alchoholReduce.equalsIgnoreCase("No")) {
					tryAction(fluentWaitElement(OAMPSHealthPageObjects.rdalchoholreduceNo), "clkradio", "AlchoholReduce" );
				} else {
					tryAction(fluentWaitElement(OAMPSHealthPageObjects.rdalchoholreduceNo), "clkradio", "AlchoholReduce" );
				}		
				
				if (hiv.equalsIgnoreCase("No")) {
					tryAction(fluentWaitElement(OAMPSHealthPageObjects.rdhivNo), "clkradio", "HIV" );
				} else {
					tryAction(fluentWaitElement(OAMPSHealthPageObjects.rdhivYes), "clkradio", "HIV" );
				}			
			
				Thread.sleep(2000);
				
				if (hivRisk.equalsIgnoreCase("No")) {
					tryAction(fluentWaitElement(OAMPSHealthPageObjects.rdhivriskNo), "clkradio", "HIVRisk" );
				} 		
							
				if (general.equalsIgnoreCase("No")) {
					tryAction(fluentWaitElement(OAMPSHealthPageObjects.rdgeneralNo), "clkradio", "General" );
				} else {
					tryAction(fluentWaitElement(OAMPSHealthPageObjects.rdgeneralNo), "clkradio", "General" );
				}			
				
				if (insuranceExcl.equalsIgnoreCase("No")) {
					tryAction(fluentWaitElement(OAMPSHealthPageObjects.rdinsuranceexclusionNo), "clkradio", "Insurance Exclusion" );
				} else {
					tryAction(fluentWaitElement(OAMPSHealthPageObjects.rdinsuranceexclusionNo), "clkradio", "Insurance Exclusion" );
				}			
				
				if (insuranceClaim.equalsIgnoreCase("No")) {
					tryAction(fluentWaitElement(OAMPSHealthPageObjects.rdinsuranceclaimNo), "clkradio", "Insurance Details" );
				} else {
					tryAction(fluentWaitElement(OAMPSHealthPageObjects.rdinsuranceclaimNo), "clkradio", "Insurance Details" );
				}
			
			}						
			
			Thread.sleep(1000);
			tryAction(fluentWaitElement(OAMPSHealthPageObjects.btnNext), "Click", "Next");
			Thread.sleep(4000);
			
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
