package au.com.metlife.eapply.mapper;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import au.com.metlife.eapply.business.entity.BrokerDet;
import au.com.metlife.eapply.business.entity.CorpPartner;
import au.com.metlife.eapply.business.entity.DeathCover;
import au.com.metlife.eapply.business.entity.EmployerDet;
import au.com.metlife.eapply.business.entity.FundCategory;
import au.com.metlife.eapply.business.entity.IPCover;
import au.com.metlife.eapply.business.entity.TPDCover;
import au.com.metlife.eapply.corporate.business.CorpRequest;
import au.com.metlife.eapply.repositories.model.CorporateFund;
import au.com.metlife.eapply.repositories.model.CorporateFundCat;


public class CorpServiceMapper {
	
	private CorpServiceMapper() {
	    throw new IllegalStateException("CorpServiceMapper class");
	  }
	
	public static CorpRequest mapRequestFromHttpRequest(HttpServletRequest request){
		CorpRequest corpRequest = new CorpRequest();
		
		corpRequest.setRequestURI(request.getRequestURI());
		corpRequest.setRequestURL(request.getRequestURL().toString());
		corpRequest.setServerName(request.getServerName());
		corpRequest.setServerPort(String.valueOf(request.getServerPort()));
		corpRequest.setServerContext(request.getContextPath());
		
		return corpRequest;
	}

	public static CorpPartner processMemberValues(CorporateFund corpFund) {
		
		CorpPartner partner = new CorpPartner();
		partner.setId(corpFund.getId().toString());
		partner.setFundCode(corpFund.getFundCode());
		partner.setAdminCode(corpFund.getAdminCode());
		partner.setPartnerName(corpFund.getPartnerName());
		partner.setStatus(corpFund.getStatus());
		partner.setCreatedDate(corpFund.getCreatedDate().toString());
		
		// Set broker information
		processBrokerDetails(partner, corpFund);
		
		// Set employer information
		processEmployerDetails(partner, corpFund);
		
		// set values for Fund Categories
		List<CorporateFundCat> fundCategoryList = corpFund.getCorpFundCategories();
		processFundCategory(partner, fundCategoryList);
		
		
		return partner;
	}

	private static void processFundCategory(CorpPartner partner, List<CorporateFundCat> fundCategories) {
		
		if(null!=fundCategories && !fundCategories.isEmpty()){
			List<FundCategory> fundCategoryList = fundCategories.stream().map(i-> CorpServiceMapper.setFundCategories(i, partner)).collect(Collectors.toList());
			partner.setFundCat(fundCategoryList.stream().toArray(FundCategory[]::new));
		}else{
			partner.setFundCat(new FundCategory[0]);
		}
		
		
	}
	
	public static FundCategory setFundCategories(CorporateFundCat corpFundCat, CorpPartner partner){
		
		FundCategory fundCat = new FundCategory();
		fundCat.setCorpFundId(partner.getId());
		fundCat.setId(corpFundCat.getId().toString());
		fundCat.setCategory(corpFundCat.getCategory().toString());
		fundCat.setCreatedDate(corpFundCat.getCreatedDate().toString());
		fundCat.setGlPolicyNum(corpFundCat.getGlPolicyNum());
		
		fundCat.setDeathReqd(corpFundCat.getDeathRequired().toString());
		fundCat.setIpReqd(corpFundCat.getIpRequired().toString());
		fundCat.setTpdReqd(corpFundCat.getTpdRequired().toString());
		
		// Setting details for death cover
		if("Y".equalsIgnoreCase(fundCat.getDeathReqd())) {
		processDeathCover(corpFundCat, fundCat);
		}
		// Setting details for TPD cover
		if("Y".equalsIgnoreCase(fundCat.getTpdReqd())) {
		processTPDCover(corpFundCat, fundCat);
		}
		// Setting details for IP cover
		if("Y".equalsIgnoreCase(fundCat.getIpReqd())) {
		processIPCover(corpFundCat, fundCat);
		}
		
		return fundCat;
	}

	private static void processIPCover(CorporateFundCat corpFundCat, FundCategory fundCat) {
		
		IPCover ipCover = new IPCover();
		
		ipCover.setIpAal(corpFundCat.getIpAal());
		ipCover.setIpBp(corpFundCat.getIpBp());
		ipCover.setIpFormula(corpFundCat.getIpFormula());
		ipCover.setIpMaxAge(corpFundCat.getIpMaxAge().toString());
		ipCover.setIpMaxSumIns(corpFundCat.getIpMaxSumIns());
		ipCover.setIpMinAge(corpFundCat.getIpMinAge().toString());
		ipCover.setIpMinSumIns(corpFundCat.getIpMinSumIns());
		ipCover.setIpSciPolicyNum(corpFundCat.getIpSciPolicyNum());
		ipCover.setIpSumInsType(corpFundCat.getIpSumInsType());
		ipCover.setIpWp(corpFundCat.getIpWp());
		
		fundCat.setIpCover(ipCover);
		
	}

	private static void processTPDCover(CorporateFundCat corpFundCat, FundCategory fundCat) {
			TPDCover tpdCover = new TPDCover();
			
			tpdCover.setTpdMaxAge(corpFundCat.getTpdMaxAge().toString());
			tpdCover.setTpdMinAge(corpFundCat.getTpdMinAge().toString());
			tpdCover.setTpdMinSumIns(corpFundCat.getTpdMinSumIns());
			tpdCover.setTpdMaxSumIns(corpFundCat.getTpdMaxSumIns());
			tpdCover.setTpdAal(corpFundCat.getTpdAal());
			
			fundCat.setTpdCover(tpdCover);
		
	}

	private static void processDeathCover(CorporateFundCat corpFundCat, FundCategory fundCat) {
		
		DeathCover deathCover = new DeathCover();
		deathCover.setDeathMinAge(corpFundCat.getDeathMinAge().toString());
		deathCover.setDeathMaxAge(corpFundCat.getDeathMaxAge().toString());
		deathCover.setDeathMinSumIns(corpFundCat.getDeathMinSumIns());
		deathCover.setDeathMaxSumIns(corpFundCat.getDeathMaxSumIns());
		deathCover.setDeathAal(corpFundCat.getDeathAal());
		fundCat.setDeathCover(deathCover);
		
	}

	private static void processEmployerDetails(CorpPartner member, CorporateFund corpFund) {
		EmployerDet employer = new EmployerDet();
		if(null!=corpFund.getEmployerContactNum()) {
		employer.setEmployerCnctNum(corpFund.getEmployerContactNum());
		}
		if(null!=corpFund.getEmployerEmail()) {
		employer.setEmployerEmail(corpFund.getEmployerEmail());
		}
		if(null!=corpFund.getEmployerKeyContactNm()) {
		employer.setEmployerKeyCnctName(corpFund.getEmployerKeyContactNm());
		}
		
		member.setEmployer(employer);
	}

	private static void processBrokerDetails(CorpPartner member, CorporateFund corpFund) {
		
		BrokerDet brokerDet = new BrokerDet();
		
		brokerDet.setBrokerName(corpFund.getBrokerName());
		brokerDet.setBrokerCnctNum(corpFund.getBrokercontactNum());
		brokerDet.setBrokerEmail(corpFund.getBrokerEmail());
		member.setBroker(brokerDet);
		
	}

	public static void getCorpFundAudits(CorporateFund corporateFund, String userId) {
		corporateFund.setLastUpdtDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		corporateFund.setLastUpdtUser(userId);
		
	}

	public static void getFundCategoryAudits(CorporateFundCat fundCategory, String userId) {
		fundCategory.setLastUpdtDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		fundCategory.setLastUpdtUser(userId);
		
	}


	public static void clearGeneralFundInfo(CorporateFund corporateFund) {
		corporateFund.setFundCode(null);
		corporateFund.setAdminCode(null);
		corporateFund.setPartnerName(null);
		corporateFund.setCreatedDate(null);
	}

	public static void clearEmployerDetails(CorporateFund corporateFund) {
		corporateFund.setEmployerContactNum(null);
		corporateFund.setEmployerEmail(null);
		corporateFund.setEmployerKeyContactNm(null);
		
	}

	public static void clearBrokerDetails(CorporateFund corporateFund) {
		corporateFund.setBrokercontactNum(null);
		corporateFund.setBrokerEmail(null);
		corporateFund.setBrokerName(null);
		
	}

	public static void setFundCategoriesForUpdate(FundCategory newCategory, CorporateFundCat category, String userId) {
		
		getFundCategoryAudits(category, userId);
		processCatGeneralInfoForUpdate(newCategory, category);
		if(newCategory.getTpdReqd().equalsIgnoreCase("Y")){
			processCatTPDCoverForUpdate(newCategory, category);
		}else{
			clearCatTPDCoverForUpdate(category);
		}
		if (newCategory.getDeathReqd().equalsIgnoreCase("Y")) {
			processCatDeathCoverForUpdate(newCategory, category);
		}else{
			clearCatDeathCoverForUpdate(category);
		}
		if (newCategory.getIpReqd().equalsIgnoreCase("Y")) {
			processCatIPCoverForUpdate(newCategory, category);
		}else{
			clearCatIPCoverForUpdate(category);
		}
		
		
	}

	private static void clearCatIPCoverForUpdate(CorporateFundCat corpFundCat) {
		corpFundCat.setIpMinAge(null);
		corpFundCat.setIpMaxAge(null);
		corpFundCat.setIpMinSumIns(null);
		corpFundCat.setIpMaxSumIns(null);
		corpFundCat.setIpSumInsType(null);
		corpFundCat.setIpFormula(null);
		corpFundCat.setIpAal(null);
		corpFundCat.setIpWp(null);
		corpFundCat.setIpBp(null);
		corpFundCat.setIpSciPolicyNum(null);
	}

	private static void clearCatDeathCoverForUpdate(CorporateFundCat corpFundCat) {
		corpFundCat.setDeathMinAge(null);
		corpFundCat.setDeathMaxAge(null);
		corpFundCat.setDeathMinSumIns(null);
		corpFundCat.setDeathMaxSumIns(null);
		corpFundCat.setDeathAal(null);
		
	}

	private static void clearCatTPDCoverForUpdate(CorporateFundCat corpFundCat) {
		corpFundCat.setTpdMinAge(null);
		corpFundCat.setTpdMaxAge(null);
		corpFundCat.setTpdMinSumIns(null);
		corpFundCat.setTpdMinAge(null);
		corpFundCat.setTpdAal(null);
		
	}

	private static void processCatIPCoverForUpdate(FundCategory newCategory, CorporateFundCat corpFundCat) {
		IPCover ipCover = newCategory.getIpCover();
		corpFundCat.setIpMinAge(Integer.valueOf(ipCover.getIpMinAge()));
		corpFundCat.setIpMaxAge(Integer.valueOf(ipCover.getIpMaxAge()));
		corpFundCat.setIpMinSumIns(ipCover.getIpMinSumIns());
		corpFundCat.setIpMaxSumIns(ipCover.getIpMaxSumIns());
		corpFundCat.setIpSumInsType(ipCover.getIpSumInsType());
		corpFundCat.setIpFormula(ipCover.getIpFormula());
		corpFundCat.setIpAal(ipCover.getIpAal());
		corpFundCat.setIpWp(ipCover.getIpWp());
		corpFundCat.setIpBp(ipCover.getIpBp());
		corpFundCat.setIpSciPolicyNum(ipCover.getIpSciPolicyNum());
		
	}

	private static void processCatDeathCoverForUpdate(FundCategory newCategory, CorporateFundCat corpFundCat) {
		
		DeathCover deathCover = newCategory.getDeathCover();
		corpFundCat.setDeathMinAge(Integer.valueOf(deathCover.getDeathMinAge()));
		corpFundCat.setDeathMaxAge(Integer.valueOf(deathCover.getDeathMaxAge()));
		corpFundCat.setDeathMinSumIns(deathCover.getDeathMinSumIns());
		corpFundCat.setDeathMaxSumIns(deathCover.getDeathMaxSumIns());
		corpFundCat.setDeathAal(deathCover.getDeathAal());
	}

	private static void processCatTPDCoverForUpdate(FundCategory newCategory, CorporateFundCat corpFundCat) {
		TPDCover tpdCover = newCategory.getTpdCover();
		corpFundCat.setTpdMinAge(Integer.valueOf(tpdCover.getTpdMinAge()));
		corpFundCat.setTpdMaxAge(Integer.valueOf(tpdCover.getTpdMaxAge()));
		corpFundCat.setTpdMinSumIns(tpdCover.getTpdMinSumIns());
		corpFundCat.setTpdAal(tpdCover.getTpdAal());
		corpFundCat.setTpdMaxSumIns(tpdCover.getTpdMaxSumIns());
		
	}

	private static void processCatGeneralInfoForUpdate(FundCategory fundCat, CorporateFundCat corpFundCat) {
		Date date = new Date();
		corpFundCat.setCategory(Integer.valueOf(fundCat.getCategory()));
		corpFundCat.setDeathRequired(fundCat.getDeathReqd().charAt(0));
		corpFundCat.setTpdRequired(fundCat.getTpdReqd().charAt(0));
		corpFundCat.setIpRequired(fundCat.getIpReqd().charAt(0));
		corpFundCat.setGlPolicyNum(fundCat.getGlPolicyNum());
		corpFundCat.setCreatedDate(new Timestamp(date.getTime()));
		
	}



}
