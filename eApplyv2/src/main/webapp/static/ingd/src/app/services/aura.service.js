export default class AuraAPI {
  constructor(AppConstants, $http, $q) {
    'ngInject';

    this._AppConstants = AppConstants;
    this._$http = $http;
    this._$q = $q;    
  }

/*
* Application Aura services are written here 
* to use through out the application
*/
  fetchAuraQuestions(path, auraData) {
    let deferred = this._$q.defer();
    this._$http({
      url: path,
      method: 'POST',
      data: auraData
    }).then(
      (res) => {
        deferred.resolve(res.data);
      },

      (err) => {
        deferred.reject(err);
      }
    );
    return deferred.promise;
  }

  submitAuraQuestions(path, auraAnswer) {
    let deferred = this._$q.defer();
    this._$http({
      url: path,
      method: 'POST',
      data: auraAnswer
    }).then(
      (res) => {
        deferred.resolve(res);
      },

      (err) => {
        deferred.reject(err);
      }
    );
    return deferred.promise;
  }

  submitAuraSession(path, auraData) {
    let deferred = this._$q.defer();
    this._$http({
      url: path,
      method: 'POST',
      data: auraData
    }).then(
      (res) => {
        this.setAuraSessionData(res.data);
        deferred.resolve(res);
      },

      (err) => {
        deferred.reject(err);
      }
    );
    return deferred.promise;
  }
  
  clientMatchSvc(path, clientData) {

	  let deferred = this._$q.defer();
	    this._$http({
	      url: path,
	      method: 'POST',
	      data: clientData
	    }).then(
	      (res) => {
	        deferred.resolve(res);
	      },

	      (err) => {
	        deferred.reject(err);
	      }
	    );
	    return deferred.promise;	  
  }

  setAuraSessionData(data) {
    this.auraSessionData = data;
  }

  getAuraSessionData() {
    return this.auraSessionData || null;
  }
}
