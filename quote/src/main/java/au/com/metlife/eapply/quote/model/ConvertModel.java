package au.com.metlife.eapply.quote.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class ConvertModel implements Serializable {
	
	private String exCoverType;
	
	private BigDecimal exCoverAmount;
	
	private BigDecimal convertedAmount;

	public String getExCoverType() {
		return exCoverType;
	}

	public void setExCoverType(String exCoverType) {
		this.exCoverType = exCoverType;
	}

	public BigDecimal getExCoverAmount() {
		return exCoverAmount;
	}

	public void setExCoverAmount(BigDecimal exCoverAmount) {
		this.exCoverAmount = exCoverAmount;
	}

	public BigDecimal getConvertedAmount() {
		return convertedAmount;
	}

	public void setConvertedAmount(BigDecimal convertedAmount) {
		this.convertedAmount = convertedAmount;
	}
	

}
