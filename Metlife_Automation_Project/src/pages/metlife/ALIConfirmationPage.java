package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ALIConfirmationsPageObjects;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.ColesPaymentPageObjects;
import uimap.metlife.OAMPSHealthPageObjects;
import uimap.metlife.STFTConfirmationPageObjects;
import uimap.metlife.STFTGetQuotePageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;

import javax.swing.JOptionPane;

public class ALIConfirmationPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public ALIConfirmationPage yourConfirmationEnter() {
		yourConfirmationDetails();
		return new ALIConfirmationPage(scriptHelper);
	}

	public ALIConfirmationPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void yourConfirmationDetails() {

		String lastname = dataTable.getData("MetLife_Data", "LastName");
		String title = dataTable.getData("MetLife_Data", "Title");
		String contact = dataTable.getData("MetLife_Data", "ContactNumber");
		String contactOther = dataTable.getData("MetLife_Data", "ContactOther");
		String email = dataTable.getData("MetLife_Data", "Email");
		String address1 = dataTable.getData("MetLife_Data", "Address1");
		String address2 = dataTable.getData("MetLife_Data", "Address2");
		String suburb = dataTable.getData("MetLife_Data", "Suburb");
		String state = dataTable.getData("MetLife_Data", "State");
		String postcode = dataTable.getData("MetLife_Data", "Postcode");
	
		try {
			Thread.sleep(2000);
			driver.manage().window().maximize();			
			for (String winHandle : driver.getWindowHandles()) {
			    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}
			
			//	JOptionPane.showMessageDialog(null,"ALERT MESSAGE","TITLE",JOptionPane.WARNING_MESSAGE);			
			tryAction(fluentWaitElement(ALIConfirmationsPageObjects.linkTitle), "DropDownSelect", "Title", title);
			Thread.sleep(1000);
////			
			tryAction(waitForClickableElement(ALIConfirmationsPageObjects.txtlastName), "SET", "Last Name", lastname);
			tryAction(waitForClickableElement(ALIConfirmationsPageObjects.txtlastName), "TAB", "Last Name");
			
			tryAction(waitForClickableElement(ALIConfirmationsPageObjects.txtContact), "SET", "Contact", contact);
			tryAction(waitForClickableElement(ALIConfirmationsPageObjects.txtContact), "TAB", "Contact");
			
			tryAction(waitForClickableElement(ALIConfirmationsPageObjects.txtcontactOther), "SET", "Contact", contactOther);
			tryAction(waitForClickableElement(ALIConfirmationsPageObjects.txtcontactOther), "TAB", "Contact");
			
//	
			tryAction(waitForClickableElement(ALIConfirmationsPageObjects.txtemail), "SET", "Email", email);
			tryAction(waitForClickableElement(ALIConfirmationsPageObjects.txtemail), "TAB", "Email");
//			
			tryAction(waitForClickableElement(ALIConfirmationsPageObjects.txtemailconfirm), "SET", "Conafirm Email", email);
			tryAction(waitForClickableElement(ALIConfirmationsPageObjects.txtemailconfirm), "TAB", "Confirm Email");
//			
			tryAction(waitForClickableElement(ALIConfirmationsPageObjects.txtaddress1), "SET", "Address 1", address1);
			tryAction(waitForClickableElement(ALIConfirmationsPageObjects.txtaddress1), "TAB", "Address 1");
//			
			tryAction(waitForClickableElement(ALIConfirmationsPageObjects.txtaddress2), "SET", "Address 2", address2	);
			tryAction(waitForClickableElement(ALIConfirmationsPageObjects.txtaddress2), "TAB", "Address 2");
//			
			tryAction(waitForClickableElement(ALIConfirmationsPageObjects.txtSuburb), "SET", "Suburb", suburb);
			tryAction(waitForClickableElement(ALIConfirmationsPageObjects.txtSuburb), "TAB", "Suburb");
//						
			Thread.sleep(1000);
//			
			tryAction(fluentWaitElement(ALIConfirmationsPageObjects.drpState), "DropDownSelect", "State", state);
			Thread.sleep(1000);
//			
			tryAction(fluentWaitElement(ALIConfirmationsPageObjects.txtPostcode), "SET", "Post Code", postcode);
			tryAction(fluentWaitElement(ALIConfirmationsPageObjects.txtPostcode), "TAB", "Post Code");
//			
			tryAction(waitForClickableElement(ALIConfirmationsPageObjects.chkacknowledge), "Click", "Acknowledge");
//			
			Thread.sleep(1000);
			
			tryAction(fluentWaitElement(ALIConfirmationsPageObjects.btnNext), "Click", "Next");
			Thread.sleep(4000);
			
						
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
