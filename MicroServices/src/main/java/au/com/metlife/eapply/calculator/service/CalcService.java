package au.com.metlife.eapply.calculator.service;

import java.util.List;

import au.com.metlife.eapply.bs.model.InsuranceModel;
import au.com.metlife.eapply.bs.model.InsuranceRuleInfo;
import au.com.metlife.eapply.calculator.model.InsurCalculatorVO;


public interface CalcService {

	InsurCalculatorVO calculateInsuranceCost(InsuranceModel insuranceModel);
	
	List<InsuranceRuleInfo> getInsuranceRule(String fundId);
}
