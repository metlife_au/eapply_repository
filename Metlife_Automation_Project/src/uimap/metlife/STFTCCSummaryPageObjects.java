package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class STFTCCSummaryPageObjects extends PageObjectLibrary{
	public static final By btnsummaryNext = getObjectIdentifaction("stft.cc.summary.next.button.css");
}
