/**
 * 
 */
package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

/**
 * @author sampa
 *
 */
public class EViewLoginPageObjects extends PageObjectLibrary {

	/**
	 * 
	 */
	public EViewLoginPageObjects() {
		// TODO Auto-generated constructor stub
		
	}	

	
	public static final By txtUserID = getObjectIdentifaction("eview.login.userid.txt.css");
	public static final By txtPassword = getObjectIdentifaction("eview.login.password.txt.css");
	public static final By btnLogin= getObjectIdentifaction("eview.login.login.btn.css");

}
