/**
 * 
 */
package pages.metlife.NEW;

import com.cognizant.framework.Status;

import supportlibraries.ScriptHelper;
import uimap.metlife.NEW.REISLoginPageObjects;

/**
 * @author Hemnath
 * 
 */
public class REIS_LoginPage extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public REIS_LoginPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public REIS_LoginPage enterREISlogin() {
		enterlogindetails();
		return new REIS_LoginPage(scriptHelper);
	} 
	
	private void enterlogindetails() {
		String MemberNo = dataTable.getData("REIS_Login", "MemberNo");
		String FirstName = dataTable.getData("REIS_Login", "FirstName");
		String SurName = dataTable.getData("REIS_Login", "SurName");			
		String DOB = dataTable.getData("REIS_Login", "DOB");
		String MemberType = dataTable.getData("REIS_Login", "MemberType");	
		
		
		try{
			
			tryAction(waitForClickableElement(REISLoginPageObjects.MemberNumber), "SET", "Member Number",MemberNo);
			tryAction(waitForClickableElement(REISLoginPageObjects.Firstname), "SET", "First Name",FirstName);
			tryAction(waitForClickableElement(REISLoginPageObjects.Surname), "SET", "SurName",SurName);
			tryAction(waitForClickableElement(REISLoginPageObjects.DateofBirth), "SET", "Date of Birth",DOB);
			tryAction(waitForClickableElement(REISLoginPageObjects.Membertype), "DropDownSelect", "Member Type",MemberType);
			tryAction(waitForClickableElement(REISLoginPageObjects.login), "Click", "New Application");
			sleep(3000);
			report.updateTestLog("REIS Login", "Sucessfully Logged into the application", Status.PASS);
			
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	
}
