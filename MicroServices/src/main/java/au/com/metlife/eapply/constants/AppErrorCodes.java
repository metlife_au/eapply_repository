package au.com.metlife.eapply.constants;

public enum AppErrorCodes {
	
	TIMEOUT_ERROR("552", EtoolKitServiceConstants.ERRMSG_SERVER_ERROR),
	SERVER_ERROR("551", EtoolKitServiceConstants.ERRMSG_SERVER_ERROR),
	SYSTEM_DOWN("553", EtoolKitServiceConstants.ERRMSG_SERVER_ERROR),
	COMMUNICATION_ERROR("554", EtoolKitServiceConstants.ERRMSG_SERVER_ERROR),
	BAD_REQUEST_ENUM("441", "Invalid ENUM value"),
	BAD_REQUEST_MISSING_DATA("442", "Mandatory field missing"),
	BAD_REQUEST_DATA_FORMAT("400", "Invalid format on data element"),
	BAD_REQUEST_REQUEST_FORMAT("400", "Invalid Request Format"),
	INVALID_URI_ERROR("404", "Invalid URI"),
	NO_DATA_FOUND("200", "Data not found"),
	DEFAULT("500", "ServerError");
	
	private String code;
	
	private String message;
	
	AppErrorCodes(String code, String msg){
		this.code = code;
		this.message = msg;
	}
	
	public String getCode(){
		return code;
	}
	
	public String getMessage(){
		return message;
	}

}
