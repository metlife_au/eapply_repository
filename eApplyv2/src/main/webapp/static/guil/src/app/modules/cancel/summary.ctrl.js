(function(angular){
	'use strict';
    /* global channel, moment */  
	angular
	.module('guildApp')
	.controller('cancelSummary', cancelSummary);
	cancelSummary.$inject=['$scope','$rootScope','$location','$timeout','$window','ngDialog','urlService','PersistenceService','submitEapply','auraResponseService'];
	function cancelSummary($scope, $rootScope,$location, $timeout, $window, ngDialog, urlService, PersistenceService,submitEapply, auraResponseService){
		
		var vm = this;
		vm.showOtherReason = false;
		vm.ackErrorFlag = false;
			
		vm.cancelReasonList = [
			'No longer require insurance',
			'Affordability constraints',
			'I have sufficient insurance in place',
			//'I don\u2019t wish to provide details',
			'I don\u0027t wish to provide details',
			'Other \u002d please provide details'
			];
		
		vm.go = function (path){
			$location.path( path );
	  	};
	  	
	  	vm.init = function(){
	  		vm.urlList = urlService.getUrlList();
	  		vm.TPD = PersistenceService.getCancelCoverDetails();
	  		vm.TPD.applicant.cancelOtherReason = '';
	  		vm.TPD.applicant.cancelReason = '';
	  	};
	  	
	  	vm.init();
	  	
	  	vm.checkReason = function(){
	  		
	  		if(vm.TPD.applicant.cancelReason === vm.cancelReasonList[4]){
	  			vm.showOtherReason = true;
	  		}else{
	  			vm.showOtherReason = false;
	  		}
	  		if(vm.TPD.applicant.cancelReason === ''){
	  			vm.cancellationReasonFlag = true;
	  		}else{
	  			vm.cancellationReasonFlag = false;
	  		}
	  	};
	  	
	  	vm.checkAckState = function(clicked){
	  		
	  		if(clicked){
	  			vm.cancelAcknowledgement = !vm.cancelAcknowledgement;
	  		}
	  		
	  		if(vm.cancelAcknowledgement){
	  			vm.ackErrorFlag = false;
	  		}else{
	  			vm.ackErrorFlag = true;
	  		}
	  		vm.cancellationAcknowledgeForm.$submitted = true;
	  	};
	  	
	  	
	  	vm.cancelConfirmationPopUp = function(){
	  		
	  		vm.checkReason();
	  		vm.checkAckState(false);
	  		
	  		if(vm.cancellationAcknowledgeForm.$valid && vm.cancelAcknowledgement){
	  			
	  			var templateContent = '<div class="ngdialog-content"><div class="modal-body"><div class="row  rowcustom"><div class="col-sm-12">';
	  			templateContent += '<p class="aligncenter">You are requesting to cancel your '+(vm.TPD.coverCancellation.death ? 'Death' : '')+(vm.TPD.coverCancellation.death && vm.TPD.coverCancellation.incomeProtection ? ' , ' : vm.TPD.coverCancellation.death && !vm.TPD.coverCancellation.incomeProtection ? ' and ' : '') + (vm.TPD.coverCancellation.tpd ? 'TPD' : '') + (vm.TPD.coverCancellation.tpd && vm.TPD.coverCancellation.incomeProtection ? ' and ' : '') + (vm.TPD.coverCancellation.incomeProtection ? 'Income Protection' : '')+'  cover.';
	  			templateContent += 'If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?</p>';
	  			templateContent += '</div></div></div><div class="ngdialog-buttons aligncenter">';
	  			templateContent += '<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain"  ng-click="confirm()">Yes</button>&nbsp;&nbsp;';
	  			templateContent += '<button type="button" class="btn btn-primary no-arrow" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button>';
	  			templateContent += '</div></div>';	  			
	  			
	  			ngDialog.openConfirm({
	                template: templateContent,
	                className: 'ngdialog-theme-plain',
	                plain: true
	             }).then(function (value) {
	                   vm.confirmCancel();
	                   return true;
	             }, function (value) {
	                 if(value === 'oncancel'){
	                     return false;
	                 }
	            });
	  		}
	  		
	  	};
	  	
	  	vm.confirmCancel = function(){  			
  			
  			if(vm.TPD.coverCancellation.death){
  				vm.TPD.applicant.cover[0].transferCoverAmount = 0;
  				vm.TPD.applicant.cover[0].cost = 0;
  				vm.TPD.applicant.cover[0].additionalUnit = 0;
  			}else{
  				vm.TPD.applicant.cover[0].transferCoverAmount = vm.TPD.applicant.cover[0].existingCoverAmount;
  				vm.TPD.applicant.cover[0].additionalUnit = vm.TPD.applicant.cover[0].fixedUnit;
  			}
  			
  			if(vm.TPD.coverCancellation.tpd){
  				vm.TPD.applicant.cover[1].transferCoverAmount = 0;
  				vm.TPD.applicant.cover[1].cost = 0;
  				vm.TPD.applicant.cover[1].additionalUnit = 0;
  			}else{
  				vm.TPD.applicant.cover[1].transferCoverAmount = vm.TPD.applicant.cover[1].existingCoverAmount;
  				vm.TPD.applicant.cover[1].additionalUnit = vm.TPD.applicant.cover[1].fixedUnit;
  			}
  			
  			if(vm.TPD.coverCancellation.incomeProtection){
  				vm.TPD.applicant.cover[2].transferCoverAmount = 0;
  				vm.TPD.applicant.cover[2].cost = 0;
  				vm.TPD.applicant.cover[2].additionalUnit = 0;
  				vm.TPD.applicant.cover[2].additionalCoverAmount = 0;
  				vm.TPD.applicant.cover[2].cost = 0;
  				vm.TPD.applicant.cover[2].additionalIpBenefitPeriod = '';
  				vm.TPD.applicant.cover[2].additionalIpWaitingPeriod = '';
  				vm.TPD.applicant.cover[2].existingIpBenefitPeriod = '';
  				vm.TPD.applicant.cover[2].existingIpWaitingPeriod = '';
  			}else{
  				vm.TPD.applicant.cover[2].transferCoverAmount = vm.TPD.applicant.cover[2].existingCoverAmount;
  			}
  			if(vm.TPD.applicant.cover[2].existingCoverAmount === 0){
  				vm.TPD.applicant.cover[2].additionalCoverAmount = 0;
  				vm.TPD.applicant.cover[2].cost = 0;
  				vm.TPD.applicant.cover[2].additionalIpBenefitPeriod = '';
  				vm.TPD.applicant.cover[2].additionalIpWaitingPeriod = '';
  				vm.TPD.applicant.cover[2].existingIpBenefitPeriod = '';
  				vm.TPD.applicant.cover[2].existingIpWaitingPeriod = '';
  				vm.TPD.applicant.cover[2].totalIpWaitingPeriod = '';
  				vm.TPD.applicant.cover[2].totalIpBenefitPeriod = '';
  			}
  			if(vm.TPD.applicant.cancelReason === vm.cancelReasonList[4]){
  				vm.TPD.applicant.cancelReason = 'Other';
  			}
  			
  			auraResponseService.setResponse(vm.TPD);
  			console.log(JSON.stringify(vm.TPD));
  			submitEapply.reqObj(vm.urlList.submitEapplyUrlNew).then(function(response) {
                 if(response.data) {
                     PersistenceService.setPDFLocation(response.data.clientPDFLocation);
                     PersistenceService.setNpsUrl(response.data.npsTokenURL);
                     vm.go('/cancelDecision');
                 } 
             }, function(err){
                 console.log('Error while submitting the application ' + JSON.stringify(err));
             });
	  	};
	}
})(angular);

 /* Cancel Summary Controller Ends  */
