package au.com.metlife.eapply.bs.serviceimpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import au.com.metlife.eapply.bs.model.TblOccMappingRuledata;
import au.com.metlife.eapply.bs.model.TblOccMappingRuledataRepository;
import au.com.metlife.eapply.bs.service.TblOccMappingRuledataService;

@Service
public class TblOccMappingRuledataServiceImpl implements TblOccMappingRuledataService {

	 private final TblOccMappingRuledataRepository repository;

	 @Autowired
	    public TblOccMappingRuledataServiceImpl(final TblOccMappingRuledataRepository repository) {
	        this.repository = repository;
	    }
	 
	
	public TblOccMappingRuledataRepository getRepository() {
		return repository;
	}


	@Override
	@Transactional
	public List<TblOccMappingRuledata> getOccupationList(String fundCode, String induCode) {
		List<TblOccMappingRuledata> tblOccMappingRuledata=repository.findByFundCodeAndIndustryCode(fundCode,induCode);
	
		return tblOccMappingRuledata;
	}
}
