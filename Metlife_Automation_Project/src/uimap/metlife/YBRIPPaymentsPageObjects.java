package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class YBRIPPaymentsPageObjects extends PageObjectLibrary{
		
	public static final By rddebit = getObjectIdentifaction("YBRIP.payment.bank.radio.css");
	public static final By txtaccountname = getObjectIdentifaction("YBRIP.payment.bank.accountname.text.id");
	public static final By txtaccountnumber = getObjectIdentifaction("YBRIP.payment.bank.accountnumber.text.id");
	public static final By txtbsbcode = getObjectIdentifaction("YBRIP.payment.bank.bsbcode.text.id");	
	public static final By txtbsbnumber = getObjectIdentifaction("YBRIP.payment.bank.bsbnumber.text.id");	
	public static final By chkacknowledge = getObjectIdentifaction("YBRIP.payment.agree.chkbox.css");
	public static final By btnsubmit = getObjectIdentifaction("YBRIP.payment.submit.btn.css");
	public static final By rdcredit = getObjectIdentifaction("YBRIP.payment.bank.credit.radio.css");
	public static final By rdvisa = getObjectIdentifaction("YBRIP.payment.bank.visa.radio.css");
	public static final By rdmaster = getObjectIdentifaction("YBRIP.payment.bank.master.radio.css");
	public static final By rdamex = getObjectIdentifaction("YBRIP.payment.bank.amex.radio.css");
	public static final By rddc = getObjectIdentifaction("YBRIP.payment.bank.dc.radio.css");
	public static final By txtccnumber = getObjectIdentifaction("YBRIP.payment.bank.ccnumber.text.id");
	public static final By txtccname = getObjectIdentifaction("YBRIP.payment.bank.ccname.text.id");
	public static final By selCCExpMonth = getObjectIdentifaction("YBRIP.payment.bank.expmonth.dropdown.css");
	public static final By selCcExpYr = getObjectIdentifaction("YBRIP.payment.bank.expyear.dropdown.css");
}
