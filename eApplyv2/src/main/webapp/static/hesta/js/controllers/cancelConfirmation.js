/*Landing Page Controller Starts*/    
CareSuperApp.controller('cancelConfirmController',['$scope', '$location','$timeout','$window','$routeParams','auraResponseService','PersistenceService','submitEapply','persoanlDetailService','deathCoverService','tpdCoverService','ipCoverService','ngDialog','urlService', 
                                           function($scope, $location,$timeout,$window,$routeParams,auraResponseService,PersistenceService,submitEapply,persoanlDetailService,deathCoverService,tpdCoverService,ipCoverService,ngDialog,urlService){
	$scope.urlList = urlService.getUrlList();
    //$scope.claimNo = claimNo
    $scope.go = function ( path ) {
    	$timeout(function(){
    		$location.path( path );	
    	}, 10);
  	};
  	$scope.navigateToLandingPage = function (){
    	if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}
    }
  	
 // added for session expiry
   /* $timeout(callAtTimeout, 900000); 
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/
  	
  	/* var timer;
     angular.element($window).bind('mouseover', function(){
     	timer = $timeout(function(){
     		sessionStorage.clear();
     		localStorage.clear();
     		$location.path("/sessionTimeOut");
     	}, 900000);
     }).bind('mouseout', function(){
     	$timeout.cancel(timer);
     });*/
     
  	$scope.ackCancelFlag = false;
  	$scope.checkAckState = function(){
    	$timeout(function(){
    		ackCheckCancel = $('#ackCancellationLabelId').hasClass('active');        	
        	if(ackCheckCancel){
        		$scope.ackCancelFlag = false;
        	}else{
        		$scope.ackCancelFlag = true;
        	}
    	}, 10);
    };
    $scope.cancelConfirmationPopUp = function (hhText) {
    	ackCheckCancel = $('#ackCancellationLabelId').hasClass('active');
    	if(ackCheckCancel){
    	ngDialog.openConfirm({
			template: '<div class="ngdialog-content"><div class="modal-body"><div class="row  rowcustom"><div class="col-sm-12"><p class="aligncenter">Are you sure you want to cancel all your insurance cover with HESTA Super?</p></div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain"  ng-click="confirm()">Yes</button>&nbsp;&nbsp;<button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
	        className: 'ngdialog-theme-plain',
	        plain: true
	     }).then(function (value) {
    		   $scope.confirmCancel();
		       return true;
		 }, function (value) {
			 if(value == 'oncancel'){
				 return false;
			 }
		});
    	} else{
  			$scope.ackCancelFlag = true;
  		}
	};
  	$scope.confirmCancel= function(){
  		ackCheckCancel = $('#ackCancellationLabelId').hasClass('active');
  		if(ackCheckCancel){
  			$scope.ackCancelFlag = false;
	  	    $scope.personalDetails = persoanlDetailService.getMemberDetails();
	  	    $scope.cancelContactDetails =PersistenceService.getCancelCoverDetails();
	  	 	$scope.deathCoverDetails = deathCoverService.getDeathCover();
	  	 	$scope.tpdCoverDetails = tpdCoverService.getTpdCover();
	  	 	$scope.ipCoverDetails = ipCoverService.getIpCover();
	  	 	
	  	 	if($scope.personalDetails[0] != null && $scope.cancelContactDetails != null && $scope.deathCoverDetails != null &&
	  	 			$scope.tpdCoverDetails != null && $scope.ipCoverDetails != null){
	  	 		var temp1 = angular.extend($scope.personalDetails[0],$scope.deathCoverDetails);
	  	 		var temp2 = angular.extend(temp1,$scope.tpdCoverDetails);
	  	 		var temp3 = angular.extend(temp2,$scope.ipCoverDetails);
	  	 		var submitObject = angular.extend(temp3,$scope.cancelContactDetails);
	  	 		auraResponseService.setResponse(submitObject);
				submitEapply.reqObj($scope.urlList.submitEapplyUrl).then(function(response) { 
					console.log(response.data);
					PersistenceService.setPDFLocation(response.data.clientPDFLocation);
					PersistenceService.setNpsUrl(response.data.npsTokenURL);
					$scope.go('/cancelDecision');
				}, function(err){
					console.log("Error while submitting the application " + JSON.stringify(err));
				});
	  	 	}
  		} else{
  			$scope.ackCancelFlag = true;
  		}
  	};
  	$scope.cancel= function(){
  		$scope.go('/landing');
  	};
  	
}]); 
/*Landing Page Controller Ends*/