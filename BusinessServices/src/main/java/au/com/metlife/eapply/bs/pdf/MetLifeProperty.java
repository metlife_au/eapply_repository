package au.com.metlife.eapply.bs.pdf;

/**
 * <p>
 * Property file reader using Properties class of java.util API .
 * </p>
 * 
 * @author Puneet Malode
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class MetLifeProperty {
    public final static String PACKAGE_NAME = "com.metlife.elodge.pdfgenerator"; //$NON-NLS-1$
    
    // Name given to this class.
    public final static String CLASS_NAME = "Property"; //$NON-NLS-1$
    
    private String fileName;
    
    public String getFileName() {

        return fileName;
    }
    
    // Set the property file name
    public void setFileName(String fileName) {

        this.fileName = fileName;
    }
    
    // Get the value for key
    public String getValue(String key) {

        final String METHOD_NAME = "getValue"; //$NON-NLS-1$
        FileInputStream inputStream = null;
        File file;
        String keyValue = null;
        try {
            file = new File( fileName);
            inputStream = new FileInputStream( file);
            Properties properties = new Properties();
            properties.load( inputStream);
            keyValue = properties.getProperty( key);
        } catch (FileNotFoundException e) {
        	e.printStackTrace();
        } catch (IOException e) {
        	e.printStackTrace();
        }
        return keyValue;
    }
    
}
