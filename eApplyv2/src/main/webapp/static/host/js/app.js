// create the module and name it CareSuperApp
// also include ngRoute for all our routing needs
var HostApp = angular.module('eapplymain', ['ngRoute','ngResource','ngFileUpload','ngDialog', 'sessionTimeOut', 'globalExceptionhandler', 'restAPI', 'appDataModel','ui.bootstrap','eApplyInterceptor','ngSanitize']);
// configure our routes
HostApp.config(function($routeProvider,$httpProvider, IdleProvider, KeepaliveProvider , $templateRequestProvider) {
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.interceptors.push('eapplyrouter');
	$templateRequestProvider.httpOptions({_isTemplate: true});


    $routeProvider
	    .when('/', {
        templateUrl : 'static/host/landingpage.html',
        controller  : 'login',
  			reloadOnSearch: false,
        resolve: {
         urls:function(fetchUrlSvc){
           var path = 'CareSuperUrls.properties';
           return fetchUrlSvc.getUrls(path);
         }
        }
	    })
	    .when('/landing', {
	        templateUrl : 'static/host/landingpage.html',
	        controller  : 'login',
	        resolve: {
	    	   urls:function(fetchUrlSvc) {
             		var path = 'CareSuperUrls.properties';
	    		   	return fetchUrlSvc.getUrls(path);
	    	   }
	        }
	    })
	    .when('/quote', {
	        templateUrl : 'static/host/coverDetails_changeCover.html',
	        controller  : 'quote'
	    })
	    .when('/newmemberquote/:mode', {
	      templateUrl : 'static/host/newMember_changeCover.html',
	       controller  : 'newmemberquote'
	    })
	   .when('/quotecancel/:mode', {
	        templateUrl : 'static/host/coverDetails_cancelCover.html',
	        controller  : 'quotecancel'
	    })
	    .when('/retrievesavedapplication', {
	    	templateUrl : 'static/host/retrive_applications.html',
	    	controller  : 'retrievesavedapplication'
	    })
	     .when('/quotetransfer/:mode', {
	    	 templateUrl : 'static/host/coverDetails_transfer.html',
	        controller  :  'quotetransfer'
	    })
	     .when('/quoteoccchange/:mode', {
	        templateUrl : 'static/host/coverDetails_updateDetails.html',
	        controller  : 'quoteoccupdate'
	    })

	    /*Special cover*/
	    .when('/quotespecial/:mode', {
	        templateUrl : 'static/host/coverDetails_specialCover.html',
	        controller  : 'quotespecial'
	    })
	    .when('/auraspecial/:mode', {
	        templateUrl : 'static/host/aura_special_offer.html',
	        controller  : 'auraspecialoffer'
	    })
	    .when('/specialoffersummary/:mode', {
	        templateUrl : 'static/host/special_offer_confirmation.html',
	        controller  : 'specialoffersummary'
	    })
	     .when('/specialofferaccept', {
	        templateUrl : 'static/host/special_decision_accepted.html',
	        controller  : 'specialOfferAcceptCtlr'
	    })
	     .when('/specialofferdecline', {
	        templateUrl : 'static/host/special_decision_decline.html',
	        controller  : 'specialOfferDeclineCtlr'
	    })
	    /*Special cover*/
	     .when('/lifeevent/:mode', {
	        templateUrl : 'static/host/coverDetails_LifeEvents.html',
	        controller  : 'lifeevent'
	     })
	     .when('/auralifeevent/:mode', {
	        templateUrl : 'static/host/aura_lifeEvent.html',
	        controller  : 'auraLifeEvent'
	     })
	     .when('/lifeeventaccept', {
	        templateUrl : 'static/host/lifeEvents_desicion_accepted.html',
	        controller  : 'lifeEventAcceptController'
	    })
	    .when('/lifeeventdecline', {
	        templateUrl : 'static/host/lifeEvents_decision_decline.html',
	        controller  : 'lifeEventDeclineController'
	    })
	    .when('/lifeeventunderwriting', {
	        templateUrl : 'static/host/lifeEvents_decision_ruw.html',
	        controller  : 'lifeEventRUWController'
	    })
	     .when('/auratransfer/:mode', {
	    	 templateUrl : 'static/host/aura_transferCover.html',
	        controller  : 'auratransfer'
	    })
	    .when('/auraocc/:mode', {
	    	 templateUrl : 'static/host/aura_updateDetails.html',
	        controller  : 'auraocc'
	    })
	    .when('/auraspecialoffer/:mode', {
	    	 templateUrl : 'static/host/aura_special_offer.html',
	        controller  : 'auraspecialoffer'
	    })
	    .when('/aura', {
	        templateUrl : 'static/host/aura.html',
	        controller  : 'aura'
	    })
	    .when('/auracancel/:mode', {
	        templateUrl : 'static/host/aura_cancelCover.html',
	        controller  : 'auracancel'
	    })
	    .when('/summary', {
	        templateUrl : 'static/host/confirmation.html',
	        controller  : 'summary'
	    })
	    .when('/changeaccept', {
	        templateUrl : 'static/host/changeCover_decision_accepted.html',
	        controller  : 'changeCoverAcceptController'
	    })
	    .when('/changedecline', {
	        templateUrl : 'static/host/changeCover_decision_decline.html',
	        controller  : 'changeCoverDeclineController'
	    })
	    .when('/changeunderwriting', {
	        templateUrl : 'static/host/changeCover_decision_ruw.html',
	        controller  : 'changeCoverRUWController'
	    })
	    .when('/changeaspcltermsacc', {
	        templateUrl : 'static/host/changeCover_decision_AcceptWithSpclTerms.html',
	        controller  : 'changeCoverACCSpclTermsController'
	    })
	    .when('/changemixedaccept', {
	        templateUrl : 'static/host/changeCover_decision_MixedAcceptance.html',
	        controller  : 'changeCoverMixedACCController'
	    })
	     .when('/requirements/:clm', {
	        templateUrl : 'claimtracker/events.html',
	        controller  : 'requirements'
	    })
	    .when('/newsummary', {
	        templateUrl : 'static/host/newMember_confirmation.html',
	        controller  : 'newsummary'
	    })
	    .when('/workRatingSummary/:mode', {
	        templateUrl : 'static/host/workRating_confirmation.html',
	        controller  : 'workRatingSummary'
	    })

	    .when('/workRatingAccept', {
	        templateUrl : 'static/host/workRating_decision_accepted.html',
	        controller  : 'workRatingAcceptController'
	    })

	    .when('/workRatingDecline', {
	        templateUrl : 'static/host/workRating_decision_decline.html',
	        controller  : 'workRatingDeclineController'
	    })

	    .when('/workRatingMaintain', {
	        templateUrl : 'static/host/workRating_decision_maintained.html',
	        controller  : 'workRatingMaintainController'
	    })

	    .when('/transferSummary/:mode', {
	        templateUrl : 'static/host/transfer_confirmation.html',
	        controller  : 'transferSummary'
	    })

	    .when('/transferAccept', {
	        templateUrl : 'static/host/transfer_decision_accepted.html',
	        controller  : 'transferAcceptController'
	    })

	    .when('/transferDecline', {
	        templateUrl : 'static/host/transfer_decision_decline.html',
	        controller  : 'transferDeclineController'
	    })
	    .when('/newmemberaccept', {
	        templateUrl : 'static/host/newMember_decision_accepted.html',
	        controller  : 'newMemberAcceptController'
	    })
	    .when('/newmemberdecline', {
	        templateUrl : 'static/host/newMember_decision_decline.html',
	        controller  : 'newMemberDeclineController'
	    })
	    .when('/cancelConfirmation', {
	        templateUrl : 'static/host/cancel_confirmation.html',
	        controller  : 'cancelConfirmController'
	    })
	    .when('/cancelDecision', {
	        templateUrl : 'static/host/cancel_decision.html',
	        controller  : 'cancelController'
	    })
	    .when('/sessionTimeOut', {
	        templateUrl : 'static/host/timeout.html',
	        controller  : 'timeOutController'
	    })
	    .when('/cancelDecisionRuw', {
	        templateUrl : 'static/host/cancel_decision_ruw.html',
	        controller  : 'cancelRuwController'
	    })
	    .when('/cancelDecisionDecline', {
	        templateUrl : 'static/host/cancel_decision_decline.html',
	        controller  : 'cancelDecisionDeclineController'
	    })
	    .when('/lifeeventsummary/:mode', {
	        templateUrl : 'static/host/confirmation_LifeEvents.html',
	        controller  : 'lifeeventsummary'
	    })
	    .when('/convertMaintain/:mode', {
	        templateUrl : 'static/host/coverDetails_convertCover.html',
	        controller  : 'quoteConvert'
	    })
	    .when('/convertSummary', {
	        templateUrl : 'static/host/convert_confirmation.html',
	        controller  : 'convertSummary'
	    })

	     .when('/convertDecision', {
	        templateUrl : 'static/host/convertMaintain_decision_accepted.html',
	        controller  : 'convertAcceptController'
	    })

});

HostApp.constant('APP_CONSTANTS', function () {
	return {
		'emailFormat': /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
	}
});

HostApp.run(function($rootScope, $window) {
	$rootScope.logout = function() {
      $window.close();
	};
	var ADRUM = ADRUM || null;
	if(ADRUM)
	ADRUM.ng.ngMonitor.init();
});
