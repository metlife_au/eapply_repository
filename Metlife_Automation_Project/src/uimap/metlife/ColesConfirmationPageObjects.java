package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ColesConfirmationPageObjects extends PageObjectLibrary {
	public static final By lblPolicyAccepted = getObjectIdentifaction("coles.confirmation.policyaccepted.label.css");
	public static final By lblPolicyNumber = getObjectIdentifaction("coles.confirmation.policynumber.label.css");
	public static final By lnkFinish = getObjectIdentifaction("coles.confirmation.finish.link.css");
	public static final By lnkHere = getObjectIdentifaction("coles.acceptance.application.linktext");
	public static final By txtAppnum = getObjectIdentifaction("coles.confirmation.appnum.text.xpath");
	public static final By lnkNo=getObjectIdentifaction("coles.survey.popup.no.link.id");
}
