package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class OAMPSDeclinePageObjects extends PageObjectLibrary{
	public static final By btnFinish = getObjectIdentifaction("OAMPS.decline.finish.btn.css");
}
