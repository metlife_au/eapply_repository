package pages.metlife.Angular.HOSTPLUS;


import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;

public class HOSTPLUS_Angular_LifeEvent extends MasterPage {
	WebDriverUtil driverUtil = null;

	public HOSTPLUS_Angular_LifeEvent LifeEvent_HOSTPLUS() {
		LifeEventHOSTPLUS();
		HealthandLifeStyle();
		confirmation();

		return new HOSTPLUS_Angular_LifeEvent(scriptHelper);
	}

	public HOSTPLUS_Angular_LifeEvent LifeEventNegative_HOSTPLUS() {
		LifeEventHOSTPLUSNegativeFlow();

		return new HOSTPLUS_Angular_LifeEvent(scriptHelper);
	}

	public HOSTPLUS_Angular_LifeEvent(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}

	public void LifeEventHOSTPLUS() {

		String EmailId = dataTable.getData("HOSTPLUS_LifeEvent", "EmailId");
		String TypeofContact = dataTable.getData("HOSTPLUS_LifeEvent", "TypeofContact");
		String ContactNumber = dataTable.getData("HOSTPLUS_LifeEvent", "ContactNumber");
		String TimeofContact = dataTable.getData("HOSTPLUS_LifeEvent", "TimeofContact");
		String Gender = dataTable.getData("HOSTPLUS_LifeEvent", "Gender");
		String FifteenHoursWork = dataTable.getData("HOSTPLUS_LifeEvent", "FifteenHoursWork");
		String RegularIncome = dataTable.getData("HOSTPLUS_LifeEvent", "RegularIncome");
		String WorkWithoutInjury = dataTable.getData("HOSTPLUS_LifeEvent", "RegularIncome");
		String WorkLimitation = dataTable.getData("HOSTPLUS_LifeEvent", "RegularIncome");
		String Citizen = dataTable.getData("HOSTPLUS_LifeEvent", "Citizen");
		String IndustryType = dataTable.getData("HOSTPLUS_LifeEvent", "IndustryType");
		String OccupationType = dataTable.getData("HOSTPLUS_LifeEvent", "OccupationType");
		String OtherOccupation = dataTable.getData("HOSTPLUS_LifeEvent", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("HOSTPLUS_LifeEvent", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("HOSTPLUS_LifeEvent", "TertiaryQual");
		String HazardousEnv = dataTable.getData("HOSTPLUS_LifeEvent", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("HOSTPLUS_LifeEvent", "OutsideOfcPercent");
		String LifeEvent = dataTable.getData("HOSTPLUS_LifeEvent", "LifeEvent");
		String LifeEvent_Date = dataTable.getData("HOSTPLUS_LifeEvent", "LifeEvent_Date");
		String LifeEvent_PrevApplyin1Year = dataTable.getData("HOSTPLUS_LifeEvent", "LifeEvent_PrevApplyin1Year");

		String Negative_Scenario=dataTable.getData("HOSTPLUS_LifeEvent", "Negative_Scenario");
		String AnnualSalary = dataTable.getData("HOSTPLUS_LifeEvent", "AnnualSalary");

		try{


			waitForPageTitle("testing", 8);
			//****Click on Life Event Link*****\\

			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Increase your Cover due to a life event']"))).click();
			report.updateTestLog("Life Event", "Life Event Link is Clicked", Status.PASS);


           sleep(4000);


				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Continue']"))).click();
			report.updateTestLog("Upload Document Evidence", "Upload Document Evidence continue button clicked", Status.PASS);

			if(driver.getPageSource().contains("You have previously saved application(s).")){
				sleep(500);
				quickSwitchWindows();
				sleep(1000);
				driver.findElement(By.xpath("//button[@ng-click='confirm()']")).click();
				report.updateTestLog("App Pop-up", "App popup clicked", Status.PASS);
				sleep(2000);


  		}

			//*****Agree to Duty of disclosure*******\\
			waitForPageTitle("testing", 5);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='dod']/parent::*"))).click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);


           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='privacy']/parent::*"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);

			//*****Enter the Email Id*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(300);

			//*****Click on the Contact Number Type****\\


			Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);


				//****Enter the Contact Number****\\

			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventPhone"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventPhone"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);

				//***Select the Preferred time of Contact*****\\

			if(TimeofContact.equalsIgnoreCase("Morning")){

				driver.findElement(By.xpath(".//input[@value='Morning (9am - 12pm)']/parent::*")).click();

				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}
			else{
				driver.findElement(By.xpath(".//input[@value='Afternoon (12pm - 6pm)']/parent::*")).click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}

	//***********Select the Gender******************\\

			if(Gender.equalsIgnoreCase("Male")){

				driver.findElement(By.xpath(".//input[@value='Male']/parent::*")).click();
				sleep(250);
				report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
			}
			else{
				driver.findElement(By.xpath(".//input[@value='Female']/parent::*")).click();
				sleep(250);
				report.updateTestLog("Gender", "Gender selected is: "+Gender, Status.PASS);
			}

				//****Contact Details-Continue Button*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='lifeEventFormSubmit(contactDetailsLifeEventForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);

			if(FifteenHoursWork.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='fifteenHrsQuestion ']/parent::*)[1]"))).click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

			}
		else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='fifteenHrsQuestion']/parent::*)[1]"))).click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

			}


				//*****Do you, directly or indirectly, own all or part of a business from which you earn your regular income?*******\\

					if(RegularIncome.equalsIgnoreCase("Yes")){

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessQuestion']/parent::*)[1]"))).click();
						sleep(500);
						report.updateTestLog("Regular Income Question", "Yes is Selected", Status.PASS);

					}
					else{

						wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessQuestion']/parent::*)[2]"))).click();
						sleep(500);
						report.updateTestLog("Regular Income Question", "No is Selected", Status.PASS);

					}

					//******Extra Question based on Regular Income*******\\
					sleep(500);
					if(RegularIncome.equalsIgnoreCase("Yes")){
						if(WorkWithoutInjury.equalsIgnoreCase("Yes")){

							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessYesQuestion']/parent::*)[1]"))).click();
							report.updateTestLog("Identifiable duties without Injury", "Selected Yes", Status.PASS);
							sleep(800);
						}
						else{

							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessYesQuestion']/parent::*)[2]"))).click();
							report.updateTestLog("Identifiable duties without Injury", "Selected No", Status.PASS);
							sleep(800);
						}
					}
					else{
						if(WorkLimitation.equalsIgnoreCase("Yes")){

							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessNoQuestion']/parent::*)[1]"))).click();
							report.updateTestLog("Work without Limitations", "Selected Yes", Status.PASS);
							sleep(800);
						}
						else{

							wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessNoQuestion']/parent::*)[2]"))).click();
							report.updateTestLog("Work without Limitations", "Selected No", Status.PASS);
							sleep(800);
						}
					}

					//*****Resident of Australia****\\

					/*if(Citizen.equalsIgnoreCase("Yes")){

						driver.findElement(By.xpath("(//input[@name='areyouperCitzLifeEventQuestion']/parent::*)[1]")).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);

						}
					else{
						driver.findElement(By.xpath("(//input[@name='areyouperCitzLifeEventQuestion']/parent::*)[2]")).click();
						sleep(250);
						report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);

						}*/

					//*****Industry********\\

					    sleep(500);
					    Select Industry=new Select(driver.findElement(By.name("lifeEventIndustry")));
					    Industry.selectByVisibleText(IndustryType);
					    sleep(800);
						report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
						sleep(500);

					//*****Occupation*****\\

						Select Occupation=new Select(driver.findElement(By.name("lifeEventOccupation")));
						Occupation.selectByVisibleText(OccupationType);
					    sleep(800);
						report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
						sleep(500);


					//*****Other Occupation *****\\

					if(OccupationType.equalsIgnoreCase("Other")){

						wait.until(ExpectedConditions.elementToBeClickable(By.name("lifeEventOtherOccupation"))).sendKeys(OtherOccupation);
						report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
						sleep(1000);
					}


					//*****Extra Questions*****\\

				    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


				//***Select if your office Duties are Undertaken within an Office Environment?\\

				    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

				    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='withinOfficeQuestion']/parent::*)[1]"))).click();
						report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);

				}
				else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='withinOfficeQuestion']/parent::*)[2]"))).click();
					report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

				}

				      //*****Do You Hold a Tertiary Qualification******\\

				        if(TertiaryQual.equalsIgnoreCase("Yes")){

				        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='tertiaryQuestion']/parent::*)[1]"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
				}
				   else{

					   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='tertiaryQuestion']/parent::*)[2]"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				}
				}



				else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){



				//******Do you work in hazardous environment*****\\

				if(HazardousEnv.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='hazardousLifeEventQuestion']/parent::*)[1]"))).click();
					report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
				}
				else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='hazardousLifeEventQuestion']/parent::*)[2]"))).click();
					report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
				}

				//******Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?*******\\

				if(OutsideOfcPercent.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='lifeEventOutsideOffice']/parent::*)[1]"))).click();
					report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
				}
				else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='lifeEventOutsideOffice']/parent::*)[2]"))).click();
					report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
				}
				sleep(800);
				//*****Extra Questions*****\\

			    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


			//***Select if your office Duties are Undertaken within an Office Environment?\\

			    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

			    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='withinOfficeQuestion']/parent::*)[1]"))).click();
					report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);

			}
			else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='withinOfficeQuestion']/parent::*)[2]"))).click();
				report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

			}

			      //*****Do You Hold a Tertiary Qualification******\\

			        if(TertiaryQual.equalsIgnoreCase("Yes")){

			        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='tertiaryQuestion']/parent::*)[1]"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
			}
			   else{

				   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='tertiaryQuestion']/parent::*)[2]"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
			}
			}


				}

		          sleep(800);

		        //*****What is your annual Salary******


				    wait.until(ExpectedConditions.elementToBeClickable(By.name("annualSalary"))).sendKeys(AnnualSalary);
				    sleep(500);
				    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
				    waitForPageTitle("testing", 3);


				//*****Click on Continue*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='lifeEventFormSubmit(occupationDetailsLifeEventForm);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
				sleep(1000);

				//********************************************************************************************************\\
				//****************************LIFE EVENT SECTION***********************************************************\\
				//*********************************************************************************************************\\


				//*****Please select the specific life event you are applying under to increase your cover*****\\

				Select Event=new Select(driver.findElement(By.name("event")));
				Event.selectByVisibleText(LifeEvent);
				sleep(500);
				report.updateTestLog("Life Event", LifeEvent+" is Selected", Status.PASS);

             if(Negative_Scenario.equalsIgnoreCase("No")){

            	//*****What is the date of the life event?*******\\

					driver.findElement(By.name("eventDate")).sendKeys(LifeEvent_Date);
					sleep(500);
					driver.findElement(By.xpath("//label[contains(text(),' What is the date of the life event?')]")).click();
					report.updateTestLog("Date of Life Event", LifeEvent_Date+" is entered", Status.PASS);
					sleep(800);


			//*****Have you successfully applied for an increase in cover due to a life event within the last 12 months
			//within this calendar year?********************\\

			if(LifeEvent_PrevApplyin1Year.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='eventAlreadyApplied']/parent::*)[1]"))).click();
				report.updateTestLog("Prior Request for Increase in Cover this year","Yes is Selected", Status.PASS);
				sleep(800);

			}

			else if(LifeEvent_PrevApplyin1Year.equalsIgnoreCase("No")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='eventAlreadyApplied']/parent::*)[2]"))).click();
				report.updateTestLog("Prior Request for Increase in Cover this year","No is Selected", Status.PASS);
				sleep(800);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='eventAlreadyApplied']/parent::*)[3]"))).click();
				report.updateTestLog("Prior Request for Increase in Cover this year","Unsure is Selected", Status.PASS);
				sleep(800);
			}

             }






             //*******Enter Annual salary******\\

                wait.until(ExpectedConditions.elementToBeClickable(By.name("annualSalary"))).sendKeys(AnnualSalary);;
				report.updateTestLog("Continue", "Clicked on Continue after entering Life event details", Status.PASS);
				sleep(1000);


				//*****Click on Continue*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='lifeEventFormSubmit(lifeEvent);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue after entering Life event details", Status.PASS);
				sleep(1000);




			sleep(10000);
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}

	public void HealthandLifeStyle() {

		String Restrictedillness=dataTable.getData("HOSTPLUS_LifeEvent", "Restrictedillness");
		String PriorClaim=dataTable.getData("HOSTPLUS_LifeEvent", "PriorClaim");
		String Diagnosedillness=dataTable.getData("HOSTPLUS_LifeEvent", "Diagnosedillness");
		String DeclinedApplication=dataTable.getData("HOSTPLUS_LifeEvent", "DeclinedApplication");

				try{

			if(driver.getPageSource().contains("Eligibility Check")){

			WebDriverWait wait=new WebDriverWait(driver,18);

  //******Are you restricted, due to illness or injury from carrying out any of the identifiable duties*****\\


			if(Restrictedillness.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[1]"))).click();
				report.updateTestLog("Restricted illness", "Yes is Selected", Status.PASS);
				waitForPageTitle("testing", 2);
			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[1]"))).click();
				report.updateTestLog("Restricted illness", "No is Selected", Status.PASS);
				waitForPageTitle("testing", 2);
			}
			sleep(800);

//******Are you contemplating or have you ever made a claim for sickness, accident or disability benefits*****\\


			if(PriorClaim.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[2]"))).click();
				report.updateTestLog("Prior Claim", "Yes is Selected", Status.PASS);
				waitForPageTitle("testing", 2);

			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[2]"))).click();
				report.updateTestLog("Prior Claim", "No is Selected", Status.PASS);
				waitForPageTitle("testing", 2);
			}
			sleep(800);

//******Have you been diagnosed with an illness that in a doctor�s opinion reduces your life expectancy to less than 3 years?*****\\


			if(Diagnosedillness.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[3]"))).click();
				report.updateTestLog("Diagnosed illness", "Yes is Selected", Status.PASS);
				waitForPageTitle("testing", 2);

			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[3]"))).click();
				report.updateTestLog("Diagnosed illness", "No is Selected", Status.PASS);
				waitForPageTitle("testing", 2);
			}
			sleep(800);

//******Have you had an application for Life, TPD, Trauma or Salary Continuance insurance declined by an insurer?*****\\


			if(DeclinedApplication.equalsIgnoreCase("Yes")){
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option1']/parent::*)[4]"))).click();
				report.updateTestLog("Declined Application", "Yes is Selected", Status.PASS);
				waitForPageTitle("testing", 2);

			}
			else{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@id='option2']/parent::*)[4]  "))).click();
				report.updateTestLog("Declined Application", "No is Selected", Status.PASS);
				waitForPageTitle("testing", 2);
			}
			sleep(800);

            //****Click on Continue****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='proceedNext()']"))).click();
			report.updateTestLog("Continue","Continue button is Clicked", Status.PASS);
			sleep(4000);


			}
		}

		catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}

	private void confirmation() {

		WebDriverWait wait=new WebDriverWait(driver,20);
		String DocumentaryEvidence = dataTable.getData("HOSTPLUS_LifeEvent", "DocumentaryEvidence");
		String AttachPath = dataTable.getData("HOSTPLUS_LifeEvent", "AttachPath");
			try{
				if(driver.getPageSource().contains("You must read and acknowledge the General Consent before proceeding")){

					driver.findElement(By.id("ngf-transferCvrFileUploadAddIdBtn")).sendKeys(AttachPath);
					report.updateTestLog("Attach Document", "Document Attached", Status.PASS);
					sleep(500);


				//*****Let the attachment load*****\\
				driver.findElement(By.xpath("//p[@class='color2']")).click();
				sleep(1000);

				//*****Add the Attachment*****\\

				WebElement add=driver.findElement(By.xpath("//span[contains(text(),'Add')]"));
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", add);
				sleep(1500);


					//******Agree to general Consent*******\\

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@id='option2']/parent::*"))).click();
					report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
					sleep(500);


				//******Click on Submit*******\\
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='navigateToDecision()']"))).click();
					report.updateTestLog("Submit", "Clicked on Submit button", Status.PASS);
					sleep(4000);

				}

				//*******Feedback popup******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='No']"))).click();
				report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
				sleep(1000);

				if(driver.getPageSource().contains("APPLICATION NUMBER")){


//				//*****Fetching the Application Status*****\\
//
//				String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
//				sleep(500);
//

					//*****Fetching the Application Status*****\\

					String Appstatus=driver.findElement(By.xpath("//a[@class='noarrow']/h4")).getText();
					sleep(500);


					if(!Appstatus.equalsIgnoreCase("LIFE EVENT COVER")){
				//*****Fetching the Application Number******\\

				String App=driver.findElement(By.xpath("//div[@class=' col-sm-6 aligncenter coverbg borderright ']/p")).getText();
				sleep(1000);



				//******Download the PDF***********\\
				properties = Settings.getInstance();
				String StrBrowseName=properties.getProperty("currentBrowser");
				if (StrBrowseName.equalsIgnoreCase("Chrome")){
					wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
					sleep(1500);
		  		}


				//driver.findElement(By.xpath("//a[@class='underline']/strong")).click();



			  		if (StrBrowseName.equalsIgnoreCase("Firefox")){
			  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();
			  			sleep(1500);
			  			Robot roboobj=new Robot();
			  			roboobj.keyPress(KeyEvent.VK_ENTER);
			  		}

			  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
			  			Robot roboobj=new Robot();
			  			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//strong[text()='Download ']"))).click();

			  			sleep(4000);
			  			roboobj.keyPress(KeyEvent.VK_TAB);
			  			roboobj.keyPress(KeyEvent.VK_TAB);
			  			roboobj.keyPress(KeyEvent.VK_TAB);
			  			roboobj.keyPress(KeyEvent.VK_TAB);
			  			roboobj.keyPress(KeyEvent.VK_ENTER);

			  		}

				report.updateTestLog("PDF File", "PDF File downloaded",Status.PASS);

				sleep(1000);


				  report.updateTestLog("Decision Page", "Application No: "+App,Status.PASS);


				 report.updateTestLog("Decision Page", "Application Status: "+Appstatus,Status.PASS);
				}

				else{
					 report.updateTestLog("Application Declined", "Application is declined",Status.SCREENSHOT);
				}
				}
				}catch(Exception e){
					report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
				}
		}



	public void LifeEventHOSTPLUSNegativeFlow() {

		String EmailId = dataTable.getData("HOSTPLUS_LifeEvent", "EmailId");
		String TypeofContact = dataTable.getData("HOSTPLUS_LifeEvent", "TypeofContact");
		String ContactNumber = dataTable.getData("HOSTPLUS_LifeEvent", "ContactNumber");
		String TimeofContact = dataTable.getData("HOSTPLUS_LifeEvent", "TimeofContact");
		String RegularIncome = dataTable.getData("HOSTPLUS_LifeEvent", "RegularIncome");
		String WorkWithoutInjury = dataTable.getData("HOSTPLUS_LifeEvent", "RegularIncome");
		String WorkLimitation = dataTable.getData("HOSTPLUS_LifeEvent", "RegularIncome");
		String Citizen = dataTable.getData("HOSTPLUS_LifeEvent", "Citizen");
		String IndustryType = dataTable.getData("HOSTPLUS_LifeEvent", "IndustryType");
		String OccupationType = dataTable.getData("HOSTPLUS_LifeEvent", "OccupationType");
		String OtherOccupation = dataTable.getData("HOSTPLUS_LifeEvent", "OtherOccupation");
		String OfficeEnvironment = dataTable.getData("HOSTPLUS_LifeEvent", "OfficeEnvironment");
		String TertiaryQual = dataTable.getData("HOSTPLUS_LifeEvent", "TertiaryQual");
		String HazardousEnv = dataTable.getData("HOSTPLUS_LifeEvent", "HazardousEnv");
		String OutsideOfcPercent = dataTable.getData("HOSTPLUS_LifeEvent", "OutsideOfcPercent");
		String LifeEvent = dataTable.getData("HOSTPLUS_LifeEvent", "LifeEvent");
		String LifeEvent_Date = dataTable.getData("HOSTPLUS_LifeEvent", "LifeEvent_Date");
		String Negative_Scenario=dataTable.getData("HOSTPLUS_LifeEvent", "Negative_Scenario");
		String dateerror=dataTable.getData("HOSTPLUS_LifeEvent", "dateerror");
		String Lifeeventerror=dataTable.getData("HOSTPLUS_LifeEvent", "Lifeeventerror");
		String FifteenHoursWork = dataTable.getData("HOSTPLUS_LifeEvent", "FifteenHoursWork");
		String AnnualSalary = dataTable.getData("HOSTPLUS_LifeEvent", "AnnualSalary");


		try{


	//****Click on Life Event Link*****\\
			waitForPageTitle("testing", 10);
			WebDriverWait wait=new WebDriverWait(driver,18);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[text()='Increase your Cover due to a life event']"))).click();

			report.updateTestLog("Life Event", "Life Event Link is Clicked", Status.PASS);

			 sleep(4000);

			 if(driver.getPageSource().contains("Before you decide to increase your cover due to Life Event,please ensure that you have a certified copy of one of the above Life Events(e.g. Marriage certificate)so that you can upload this to your application.If you do not have this you will not be able to proceed.")){
					sleep(500);
					quickSwitchWindows();
					sleep(1000);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Continue']"))).click();
				report.updateTestLog("Upload Document Evidence", "Upload Document Evidence continue button clicked", Status.PASS);
				}
		//	waitForPageTitle("testing", 8);
			/*if(driver.findElement(By.xpath("//button[@ng-click='confirm()']")).isDisplayed()){
				sleep(500);
				quickSwitchWindows();
				sleep(1000);
				driver.findElement(By.xpath("//button[@ng-click='confirm()']")).click();
				report.updateTestLog("App Pop-up", "App popup clicked", Status.PASS);
				sleep(2000);*/

			/*Robot roboobj=new Robot();
			sleep(100);
			roboobj.keyPress(KeyEvent.VK_TAB);
  			sleep(100);
  			roboobj.keyPress(KeyEvent.VK_TAB);
  			quickSwitchWindows();
  			roboobj.keyPress(KeyEvent.VK_ENTER);
  			roboobj.keyPress(KeyEvent.VK_ENTER);
  			roboobj.keyPress(KeyEvent.VK_ENTER);
  			sleep(100);*/
  	//	}

			//*****Agree to Duty of disclosure*******\\
			waitForPageTitle("testing", 5);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='dod']/parent::*"))).click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);


           //*****Agree to Privacy Statement*******\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@name='privacy']/parent::*"))).click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);

			//*****Enter the Email Id*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventEmail"))).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventEmail"))).sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: "+EmailId+" is entered", Status.PASS);
			sleep(300);

			//*****Click on the Contact Number Type****\\


			Select Contact=new Select(driver.findElement(By.xpath("//select[@ng-model='preferredContactType']")));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: "+TypeofContact+" is Selected", Status.PASS);
			sleep(250);


				//****Enter the Contact Number****\\

			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventPhone"))).sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(By.name("contactDetailsLifeEventPhone"))).sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: "+ContactNumber+" is Entered", Status.PASS);

				//***Select the Preferred time of Contact*****\\

			if(TimeofContact.equalsIgnoreCase("Morning")){

				driver.findElement(By.xpath(".//input[@value='Morning (9am - 12pm)']/parent::*")).click();

				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}
			else{
				driver.findElement(By.xpath(".//input[@value='Afternoon (12pm - 6pm)']/parent::*")).click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact+" is preferred time of Contact", Status.PASS);
			}

				//****Contact Details-Continue Button*****\\

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='lifeEventFormSubmit(contactDetailsLifeEventForm);']"))).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);

	if(FifteenHoursWork.equalsIgnoreCase("Yes")){

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='fifteenHrsQuestion ']/parent::*)[1]"))).click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);

			}
		else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='fifteenHrsQuestion']/parent::*)[1]"))).click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

			}

	//*****Do you, directly or indirectly, own all or part of a business from which you earn your regular income?*******\\

	if(RegularIncome.equalsIgnoreCase("Yes")){

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessQuestion']/parent::*)[1]"))).click();
		sleep(500);
		report.updateTestLog("Regular Income Question", "Yes is Selected", Status.PASS);

	}
	else{

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessQuestion']/parent::*)[2]"))).click();
		sleep(500);
		report.updateTestLog("Regular Income Question", "No is Selected", Status.PASS);

	}

	//******Extra Question based on Regular Income*******\\
	sleep(500);
	if(RegularIncome.equalsIgnoreCase("Yes")){
		if(WorkWithoutInjury.equalsIgnoreCase("Yes")){

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessYesQuestion']/parent::*)[1]"))).click();
			report.updateTestLog("Identifiable duties without Injury", "Selected Yes", Status.PASS);
			sleep(800);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessYesQuestion']/parent::*)[2]"))).click();
			report.updateTestLog("Identifiable duties without Injury", "Selected No", Status.PASS);
			sleep(800);
		}
	}
	else{
		if(WorkLimitation.equalsIgnoreCase("Yes")){

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessNoQuestion']/parent::*)[1]"))).click();
			report.updateTestLog("Work without Limitations", "Selected Yes", Status.PASS);
			sleep(800);
		}
		else{

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='ownBussinessNoQuestion']/parent::*)[2]"))).click();
			report.updateTestLog("Work without Limitations", "Selected No", Status.PASS);
			sleep(800);
		}
	}

	//*****Resident of Australia****\\

	/*if(Citizen.equalsIgnoreCase("Yes")){

		driver.findElement(By.xpath("(//input[@name='areyouperCitzLifeEventQuestion']/parent::*)[1]")).click();
		sleep(250);
		report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);

		}
	else{
		driver.findElement(By.xpath("(//input[@name='areyouperCitzLifeEventQuestion']/parent::*)[2]")).click();
		sleep(250);
		report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);

		}*/

					//*****Industry********\\

					    sleep(500);
					    Select Industry=new Select(driver.findElement(By.name("lifeEventIndustry")));
					    Industry.selectByVisibleText(IndustryType);
					    sleep(800);
						report.updateTestLog("Industry", "Industry Selected is: "+IndustryType, Status.PASS);
						sleep(500);

					//*****Occupation*****\\

						Select Occupation=new Select(driver.findElement(By.name("lifeEventOccupation")));
						Occupation.selectByVisibleText(OccupationType);
					    sleep(800);
						report.updateTestLog("Occupation", "Occupation Selected is: "+OccupationType, Status.PASS);
						sleep(500);


					//*****Other Occupation *****\\

					if(OccupationType.equalsIgnoreCase("Other")){

						wait.until(ExpectedConditions.elementToBeClickable(By.name("lifeEventOtherOccupation"))).sendKeys(OtherOccupation);
						report.updateTestLog("Other Occupation", "Other Occupation entered: "+OtherOccupation, Status.PASS);
						sleep(1000);
					}


//*****Extra Questions*****\\

				    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


				//***Select if your office Duties are Undertaken within an Office Environment?\\

				    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

				    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='withinOfficeQuestion']/parent::*)[1]"))).click();
						report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);

				}
				else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='withinOfficeQuestion']/parent::*)[2]"))).click();
					report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

				}

				      //*****Do You Hold a Tertiary Qualification******\\

				        if(TertiaryQual.equalsIgnoreCase("Yes")){

				        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='tertiaryQuestion']/parent::*)[1]"))).click();
							report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
				}
				   else{

					   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='tertiaryQuestion']/parent::*)[2]"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
				}
				}



				else if(driver.getPageSource().contains("Do you work in a hazardous environment and/or perform any duties of a manual nature?")){



				//******Do you work in hazardous environment*****\\

				if(HazardousEnv.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='hazardousLifeEventQuestion']/parent::*)[1]"))).click();
					report.updateTestLog("Hazardous Environment", "Selected Yes", Status.PASS);
				}
				else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='hazardousLifeEventQuestion']/parent::*)[2]"))).click();
					report.updateTestLog("Hazardous Environment", "Selected No", Status.PASS);
				}

				//******Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?*******\\

				if(OutsideOfcPercent.equalsIgnoreCase("Yes")){

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='lifeEventOutsideOffice']/parent::*)[1]"))).click();
					report.updateTestLog("20% work outside Ofice", "Selected Yes", Status.PASS);
				}
				else{

					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='lifeEventOutsideOffice']/parent::*)[2]"))).click();
					report.updateTestLog("20% work outside Ofice", "Selected No", Status.PASS);
				}
				sleep(800);
				//*****Extra Questions*****\\

			    if(driver.getPageSource().contains("Are you working in a senior management role or hold a tertiary qualification or a member of a professional institute or registered by a government body related to your profession?")){


			//***Select if your office Duties are Undertaken within an Office Environment?\\

			    if(OfficeEnvironment.equalsIgnoreCase("Yes")){

			    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='withinOfficeQuestion']/parent::*)[1]"))).click();
					report.updateTestLog("Work within Office Environment", "Selected Yes", Status.PASS);

			}
			else{

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='withinOfficeQuestion']/parent::*)[2]"))).click();
				report.updateTestLog("Work within Office Environment", "Selected No", Status.PASS);

			}

			      //*****Do You Hold a Tertiary Qualification******\\

			        if(TertiaryQual.equalsIgnoreCase("Yes")){

			        	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='tertiaryQuestion']/parent::*)[1]"))).click();
						report.updateTestLog("Tertiary Qualification", "Selected Yes", Status.PASS);
			}
			   else{

				   wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(.//input[@name='tertiaryQuestion']/parent::*)[2]"))).click();
					report.updateTestLog("Tertiary Qualification", "Selected No", Status.PASS);
			}
			}


				}

		          sleep(800);

		        //*****What is your annual Salary******



				    wait.until(ExpectedConditions.elementToBeClickable(By.name("annualSalary"))).sendKeys(AnnualSalary);
				    sleep(500);
				    report.updateTestLog("Annual Salary", "Annual Salary Entered is: "+AnnualSalary, Status.PASS);
				    waitForPageTitle("testing", 3);


				//*****Click on Continue*******\\

				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@ng-click='lifeEventFormSubmit(occupationDetailsLifeEventForm);']"))).click();
				report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
				sleep(1000);

				//********************************************************************************************************\\
				//****************************LIFE EVENT SECTION***********************************************************\\
				//*********************************************************************************************************\\


				//*****Please select the specific life event you are applying under to increase your cover*****\\

				Select Event=new Select(driver.findElement(By.name("event")));
				Event.selectByVisibleText(LifeEvent);
				sleep(500);
				report.updateTestLog("Life Event", LifeEvent+" is Selected", Status.PASS);




                    if(Negative_Scenario.equalsIgnoreCase("DateError")){

                    	//*****What is the date of the life event?*******\\

                    	driver.findElement(By.name("eventDate")).sendKeys(LifeEvent_Date);
						sleep(1000);
						driver.findElement(By.xpath("//label[contains(text(),' What is the date of the life event?')]")).click();
						report.updateTestLog("Date of Life Event", LifeEvent_Date+" is entered", Status.PASS);
						sleep(800);

						if(driver.getPageSource().contains(dateerror)){

							report.updateTestLog("Life Event Date Error Message", dateerror+" is Displayed", Status.PASS);
						}

						else{
							report.updateTestLog("Life Event Date Error Message", "Error Message is not Displayed", Status.FAIL);
						}
                    }

                    else if(Negative_Scenario.equalsIgnoreCase("PriorEventError")){


                    	//*****What is the date of the life event?*******\\
                    	driver.findElement(By.name("eventDate")).sendKeys(LifeEvent_Date);
						sleep(500);
						driver.findElement(By.xpath("//label[contains(text(),' What is the date of the life event?')]")).click();
						report.updateTestLog("Date of Life Event", LifeEvent_Date+" is entered", Status.PASS);
						sleep(800);

                    	//*****Have you successfully applied for an increase in cover due to a life event within the last 12 months
        				//within this calendar year?********************\\

                    	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='eventAlreadyApplied']/parent::*)[1]"))).click();
    					report.updateTestLog("Prior Request for Increase in Cover this year","Yes is Selected", Status.PASS);
    					sleep(800);


    					sleep(500);
    					if(driver.getPageSource().contains(Lifeeventerror)){
    						report.updateTestLog("Life Event Error Message",Lifeeventerror+" is Displayed", Status.PASS);
    					}

    					else{
							report.updateTestLog("Life Event Error Message", "Error Message is not Displayed", Status.FAIL);
						}
                    }



			sleep(1000);
			}catch(Exception e){
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}
		}


	}





