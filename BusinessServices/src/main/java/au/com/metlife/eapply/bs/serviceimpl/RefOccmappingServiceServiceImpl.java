package au.com.metlife.eapply.bs.serviceimpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import au.com.metlife.eapply.bs.model.RefOccmapping;
import au.com.metlife.eapply.bs.model.RefOccmappingRepository;
import au.com.metlife.eapply.bs.model.TblOccMappingRuledata;
import au.com.metlife.eapply.bs.model.TblOccMappingRuledataRepository;
import au.com.metlife.eapply.bs.service.RefOccmappingService;
import au.com.metlife.eapply.bs.service.TblOccMappingRuledataService;

@Service
public class RefOccmappingServiceServiceImpl implements RefOccmappingService {

	 private final RefOccmappingRepository repository;

	 @Autowired
	    public RefOccmappingServiceServiceImpl(final RefOccmappingRepository repository) {
	        this.repository = repository;
	    }
	 
	
	public RefOccmappingRepository getRepository() {
		return repository;
	}

	
	
	
   @Override
	public List<RefOccmapping> getNewOccupationName(String fundId, String occName) {
	   List<RefOccmapping> refOccmapping=repository.findByFundIdAndOccName(fundId, occName);
	   return refOccmapping;
	}
}
