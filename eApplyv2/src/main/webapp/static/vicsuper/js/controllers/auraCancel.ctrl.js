VicsuperApp.controller('auracancel',['$scope', '$rootScope','$location', '$timeout','$window','auraTransferInitiateService', 'getAuraTransferData','auraResponseService', 'auraPostfactory','auraInputService','deathCoverService','tpdCoverService','ipCoverService', 'PersistenceService', 'submitAura','ngDialog','persoanlDetailService','saveEapply','clientMatch','submitEapply','urlService',
                                     function($scope,$rootScope, $location, $timeout,$window,auraTransferInitiateService,getAuraTransferData,auraResponseService,auraPostfactory,auraInputService,deathCoverService,tpdCoverService,ipCoverService, PersistenceService, submitAura, ngDialog,persoanlDetailService,saveEapply,clientMatch,submitEapply,urlService){
	
	$rootScope.$broadcast('enablepointer');
	$scope.urlList = urlService.getUrlList();
    $scope.datePattern='/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i';
    $scope.go = function (path) {
    	$timeout(function(){
    		$location.path(path);
    	}, 10);
  	};
  	$scope.serachText = '';
    $scope.heightDropdownOpt = ['cm', 'Feet'];
    $scope.heightDropDown = $scope.heightDropdownOpt[0];
    $scope.heightOptions= $scope.heightOptions == undefined ? 'm.cm' : $scope.heightOptions;
    
    $scope.weightDropdownOpt = ['Kilograms', 'Pound', 'Stones'];
    $scope.weightDropDown = $scope.weightDropdownOpt[0];
    $scope.weighOptions=$scope.weighOptions == undefined ? 'kg' : $scope.weighOptions;
    $scope.weightMaxLen = 3;
    
    
	$scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
		ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    };
    
 // added for session expiry
    /*$timeout(callAtTimeout, 900000); 
  	function callAtTimeout() {
  		$location.path("/sessionTimeOut");
  	}*/
    
   /* var timer;
    angular.element($window).bind('mouseover', function(){
    	timer = $timeout(function(){
    		sessionStorage.clear();
    		localStorage.clear();
    		$location.path("/sessionTimeOut");
    	}, 900000);
    }).bind('mouseout', function(){
    	$timeout.cancel(timer);
    });*/
	
	
	$scope.coverDetails = PersistenceService.getChangeCoverDetails();
    $scope.personalDetails = persoanlDetailService.getMemberDetails();
    $scope.changeCoverOccDetails = PersistenceService.getChangeCoverOccDetails();
    $scope.deathAddnlDetails = PersistenceService.getDeathAddnlCoverDetails();
    $scope.tpdAddnlDetails = PersistenceService.getTpdAddnlCoverDetails();
    $scope.ipAddnlDetails = PersistenceService.getIpAddnlCoverDetails();
	//console.log($scope.personalDetails);
	
    //console.log(auraInputService); 	
  
  	auraInputService.setFund('VICT')
  	auraInputService.setMode('change')  	
    //setting deafult vaues for testing
  	auraInputService.setName($scope.coverDetails.name);	
  	auraInputService.setAge(moment().diff(moment($scope.coverDetails.dob, 'DD-MM-YYYY'), 'years')) ; 		
  	auraInputService.setAppnumber($scope.coverDetails.appNum)
  	auraInputService.setDeathAmt($scope.deathAddnlDetails.deathFixedAmt)
 	auraInputService.setTpdAmt($scope.tpdAddnlDetails.tpdFixedAmt)
  	auraInputService.setIpAmt($scope.ipAddnlDetails.ipFixedAmt)
  	auraInputService.setWaitingPeriod($scope.ipAddnlDetails.waitingPeriod)
  	auraInputService.setBenefitPeriod($scope.ipAddnlDetails.benefitPeriod) 
  	if($scope.personalDetails[0].memberType=="Personal"){
  		auraInputService.setMemberType("INDUSTRY OCCUPATION")
  	}else{
  		auraInputService.setMemberType("None")
  	} 	
  	if($scope.changeCoverOccDetails && $scope.changeCoverOccDetails.gender ){
  		auraInputService.setGender($scope.changeCoverOccDetails.gender);
  	}else if($scope.personalDetails && $scope.personalDetails.gender){
  		auraInputService.setGender($scope.personalDetails.gender);
  	}
  	
  	auraInputService.setClientname('metaus')
  	auraInputService.setIndustryOcc($scope.changeCoverOccDetails.industryCode+":"+$scope.changeCoverOccDetails.occupation)
  /*	if($scope.changeCoverOccDetails.citizenQue=='Yes'){
  		auraInputService.setCountry('Australia')
  	}else{
  		auraInputService.setCountry($scope.changeCoverOccDetails.citizenQue)
  	} */ 	
  	auraInputService.setCountry('Australia'); // residency que removed from quote page, so hard-coded
  	auraInputService.setSalary($scope.changeCoverOccDetails.salary)
  	auraInputService.setFifteenHr($scope.changeCoverOccDetails.fifteenHr)  	
  	auraInputService.setLastName($scope.personalDetails[0].personalDetails.lastName)
  	auraInputService.setFirstName($scope.personalDetails[0].personalDetails.firstName)
  	auraInputService.setDob($scope.personalDetails[0].personalDetails.dateOfBirth)
  	var termFlag = false;
  	for(var k = 0; k < $scope.personalDetails[0].existingCovers.cover.length; k++){
  		if(($scope.personalDetails[0].existingCovers.cover[k].exclusions && $scope.personalDetails[0].existingCovers.cover[k].exclusions != '') || 
  				($scope.personalDetails[0].existingCovers.cover[k].loading && $scope.personalDetails[0].existingCovers.cover[k].loading != '' &&
  						parseFloat($scope.personalDetails[0].existingCovers.cover[k].loading) > 0)){
  			termFlag = true;
  			break;
  		}
  	}
  	if(termFlag){
  		auraInputService.setExistingTerm(true);
  	} else{
  		auraInputService.setExistingTerm(false);
  	}
  	///////////////   	
  	//below values has to be set from quote page, as of now taking from existing insurance
  /*	
  	 auraInputService.setAge(50)  
  	 auraInputService.setAppnumber(14782223482374216)
  	auraInputService.setDeathAmt(200000)
  	auraInputService.setTpdAmt(200000)
  	auraInputService.setIpAmt(2000)
  	auraInputService.setWaitingPeriod('30 Days')
  	auraInputService.setBenefitPeriod('2 Years')   
  	auraInputService.setGender('Male')
  	auraInputService.setIndustryOcc('026:Transfer')
  	auraInputService.setCountry('Australia')
  	auraInputService.setSalary('100000')
  	auraInputService.setFifteenHr('Yes')   */	
  	
	 getAuraTransferData.requestObj($scope.urlList.auraTransferDataUrl).then(function(response) {    	
  		$scope.auraResponseDataList = response.data.sections;     
  		//console.log(response.data)      		
  		angular.forEach($scope.auraResponseDataList, function (Object,index) {
  			//console.log($scope.auraResponseDataList)
  			$scope.questionAlias = Object.sectionName
  			$scope.questionlist = Object.questions      			
  			if($scope.questionAlias=='QG=Health Questions'){
  				$scope.auraResponseDataList[index].sectionStatus=true;      				
  			}else{
  				var keepGoing = true;
  				angular.forEach($scope.questionlist, function (Object,index1) {      					
  					if(keepGoing && Object.branchComplete){
  						$scope.auraResponseDataList[index].sectionStatus=true;
  						keepGoing = false;
  					}
  				});
  			}
  		});
  		//console.log($scope.auraResponseDataList)
  		$scope.updateHeightWeightValue();
  		
  	});    	
  	
  	 $scope.inchValue  = function(value){ 
  		//console.log(value)
  		$scope.heightin = value;     		 
  	 }
  	 $scope.meterValue  = function(value,answer, questionObj){       		
  		if(!angular.isUndefined(value)){
  			$scope.heightinMeter = value;
  	  		$scope.heightval = value;
  	  		questionObj.error = false;
  		}else{
  			questionObj.error = true;
  		}
  		 
  		 
  	 }
 	
	$scope.heighOptions  = function(value){ 
  		//console.log(value)   
  		if(value=='cm'){
  			$scope.heightOptions='m.cm'
  		}else{
  			$scope.heightval = '';
  			$scope.heightOptions='ft.in'
  		}    		 
  	 }
  	$scope.weightValue = function(value,answer,questionObj){  		
  	
  		if(!angular.isUndefined(value)){   			
  	  		$scope.weightVal = value;
  	  		questionObj.error = false;
  		}else if(!angular.isUndefined(value) && value==''){
  			questionObj.error = true;
  		}
  		
  	}
  	$scope.weightOptions= function(value,answer){
  		//console.log(value)   
  		if(value=='Kilograms'){
  			$scope.weighOptions='kg';
  			$scope.weightDropDown = 'Kilograms';
  			$scope.weightMaxLen = 3;
  		}else if(value=='Pound'){
  			$scope.weighOptions='lb';
  			$scope.weightDropDown = 'Pound';
  			$scope.weightMaxLen = 3;
  		}else if(value=='Stones'){
  			$scope.weighOptions='st.lb';
  			$scope.weightDropDown = 'Stones';
  			$scope.weightMaxLen = 100;
  		}
  	}
  	
  	$scope.lbsValue = function(value){
  		//console.log(value)
  		$scope.lbsval = value;
  	}
  	
  	 $scope.submitHeightWeightQuestion = function(sectionIndex){		
  		angular.forEach($scope.auraResponseDataList, function (Object,index) {      			
  			angular.forEach(Object.questions, function (Object) {         			
  				var serviceCallRequired= false;      				
  				if(Object.classType=='HeightQuestion'){      					
  					questionObj= Object;
      				if($scope.heightOptions=='m.cm'){
      					questionObj.arrAns=[];
          	   			questionObj.arrAns[0]= $scope.heightOptions;
          	   			questionObj.arrAns[1]= '0';
          	   			questionObj.arrAns[2]= $scope.heightval; 
      				}else if($scope.heightOptions=='ft.in'){
      					//console.log($scope.heightOptions)
      					//console.log($scope.heightinMeter)
      					//console.log($scope.heightin)
      					questionObj.arrAns=[];
          	   			questionObj.arrAns[0]= $scope.heightOptions;
          	   			questionObj.arrAns[1]= $scope.heightval;
          	   			questionObj.arrAns[2]= $scope.heightin; 
      				}
  					
      	   			serviceCallRequired= true;
      			}else if(Object.classType=='WeightQuestion'){
      				questionObj= Object;
      				//console.log($scope.weightDropDown)
      				//console.log($scope.weighOptions)
      				//console.log($scope.weightVal)
      				if($scope.weightDropDown=='Stones'){
      					//console.log($scope.weightVal)
      					//console.log($scope.lbsval)
      					questionObj.arrAns=[];
          	   			questionObj.arrAns[0]= 'st.lb';
          	   			questionObj.arrAns[1]= $scope.weightVal; 
          	   			questionObj.arrAns[2]= $scope.lbsval;
      				}else if($scope.weightDropDown=='Kilograms'){       					
      					questionObj.arrAns=[];
          	   			questionObj.arrAns[0]= 'kg';
          	   			questionObj.arrAns[1]= $scope.weightVal; 
      				}else if($scope.weightDropDown=='Pound'){          					
      					questionObj.arrAns=[];
          	   			questionObj.arrAns[0]= 'lb';
          	   			questionObj.arrAns[1]= $scope.weightVal; 
      				}
      				
      	   			serviceCallRequired= true;
      			}
  				if(serviceCallRequired && questionObj.arrAns.length>1){
  					 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};  
  					//console.log($scope.auraRes)
          			 auraResponseService.setResponse($scope.auraRes)  
          			auraPostfactory.reqObj($scope.urlList.auraPostUrl).then(function(response) {
               			//questionObj.auraAnswer = response.data.auraAnswer            				
          				$scope.updateQuestionList(questionObj.questionId,response.data.changedQuestion,response.data)          	
          				$scope.updateHeightWeightValue();          				
          				var keepGoing = true;      		
                  		angular.forEach($scope.auraResponseDataList[index].questions, function (Object,index1) {      			
                  			//console.log(Object)
                  			$scope.setprogressiveError($scope.auraResponseDataList[sectionIndex].questions[index1])
                  			if(keepGoing && !Object.branchComplete){
                  				keepGoing = false;
                  			}
                  			
                  		});
                  		//console.log(keepGoing)
                  		if(keepGoing){
                  			index = index+1;
                  			//console.log(index)
                      		$scope.auraResponseDataList[index].sectionStatus=true;                      			
                  		}
                   	}, function () {
                   		//console.log('failed');
                   	});
  				}else{
  					angular.forEach($scope.auraResponseDataList[sectionIndex].questions, function (Object,index1) {
  						$scope.setprogressiveError($scope.auraResponseDataList[sectionIndex].questions[index1])
  					});
  					
  				}
      			
      		});
  		});    		
   	 }; 
   	 //updating height and weight questions
   	 $scope.updateHeightWeightValue = function(){
   		 
   		angular.forEach($scope.auraResponseDataList, function (Object,index) {
  			$scope.questionAlias = Object.sectionName
  			$scope.questionlist = Object.questions      			
  			if($scope.questionAlias=='QG=Health Questions'){     				
  				$scope.healthQuestion = $scope.auraResponseDataList[index].questions;
  				 angular.forEach($scope.healthQuestion, function (question) {
 					if(question.classType=='HeightQuestion'){      						  				
 						if(question.answerTest.indexOf('Metres')!==-1){	
 						
 							$scope.heightval=parseInt(question.answerTest.split(",")[0].split("Metres")[0])*100 + parseInt(question.answerTest.split(",")[1].split("cm")[0]);
 							$scope.heightDropDown='cm'
 							$scope.heightOptions='m.cm'
 						}      				
 						if(question.answerTest.indexOf('Feet')!==-1){      							
 							$scope.heightDropDown='Feet'
 							$scope.heightval=question.answerTest.substring(0,question.answerTest.indexOf('Feet')-1)
 							$scope.inches= question.answerTest.substring(question.answerTest.indexOf(',')+2,question.answerTest.indexOf('in')-1);
 							$scope.heightOptions='ft.in'
 						}
 						
 					}else if(question.classType=='WeightQuestion'){      						 
 						if(question.answerTest.indexOf('Kilograms')!==-1){   
 							//console.log(question.answerTest) 
 							$scope.weightDropDown='Kilograms';
 							$scope.weightVal=question.answerTest.substring(0,question.answerTest.indexOf('Kilograms')-1);      							
 							$scope.weighOptions='kg';
 						} 
 						if(question.answerTest.indexOf('Pound')!==-1){   
 							//console.log(question.answerTest) 
 							$scope.weightDropDown='Pound';
 							$scope.weightVal=question.answerTest.substring(0,question.answerTest.indexOf('Pound')-1);      							
 							$scope.weighOptions='lb';
 						}
 						if(question.answerTest.indexOf('Stones')!==-1){   
 							//console.log(question.answerTest) 
 							$scope.weightDropDown='Stones';
 							$scope.weightVal=question.answerTest.substring(0,question.answerTest.indexOf('Stones')-1);      							
 							$scope.weighOptions='st.lb';
 							$scope.lbsval=question.answerTest.substring(question.answerTest.indexOf(',')+2,question.answerTest.indexOf('lbs')-1);
 						}
 					}
 				});
  			}
   		});
   		
   		
   	 }
   	 
  	$scope.collapseUncollapse = function (index,sectionName){
  		//console.log($scope.auraResponseDataList)
  		if(sectionName=='QG=Health Questions'){
  			$scope.submitHeightWeightQuestion(index);
  		}else{
  			var keepGoing = true;      		
      		angular.forEach($scope.auraResponseDataList[index].questions, function (Object,index1) {      			
      			//console.log(Object)
      			$scope.setprogressiveError($scope.auraResponseDataList[index].questions[index1])
      			if(keepGoing && !Object.branchComplete){
      				keepGoing = false;
      			}
      			
      		});
      		//console.log(keepGoing)
      		if(keepGoing){
      			index = index+1;
      			console.log(index)
          		$scope.auraResponseDataList[index].sectionStatus=true;
      			
      		}
  		}  	
  		
  		//console.log($scope.auraResponseDataList)
  	}
  	
  	$scope.stateChanged = function (qId) {      		 
  		if($scope.answers[qId]){ //If it is checked
  			 cons//console.log>>'+qId)
  	   }
  	}
  	$scope.updateFreeText = function (answerValue, questionObj){
  		//console.log('answer>'+answerValue)
  		questionObj.arrAns = [];
  		questionObj.arrAns[0]=answerValue;
  		questionObj.arrAns[1]='accept';
 		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};  
 		auraResponseService.setResponse($scope.auraRes)	  
		  auraPostfactory.reqObj($scope.urlList.auraPostUrl).then(function(response) {
			  $scope.updateQuestionList(questionObj.questionId , response.data.changedQuestion, response.data)    	
		  });
  	}
  	$scope.updateQuestionList = function(questionId,changedQuestion, questionObj){
  		angular.forEach($scope.auraResponseDataList, function (Object,index) {
  			$scope.questionlist = Object.questions
  			angular.forEach($scope.questionlist, function (Object,index1) {
  				if(Object.questionId == changedQuestion.questionId){
  					$scope.auraResponseDataList[index].questions[index1]= changedQuestion  					
  					 $scope.progressive(questionId,changedQuestion,questionObj);
  					//console.log($scope.auraResponseDataList[index])
  				}
  			});
  		})
  	}
  	
  	 $scope.updateRadio = function (answerValue, questionObj){   
  		//console.log('answer>'+answerValue)
  		if(questionObj.classType === 'DateQuestion') {
  			var pattern =/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;

  	  		if(!pattern.test(answerValue)) {
	  	  		questionObj.error = true;
	  			return false;
  	  		}
  		}/*else if(questionObj.classType==='RangeQuestion'){
  			var pattern =/^(0|[1-9][0-9]*)$/;
  			if(!pattern.test(answerValue)) {
	  	  		questionObj.error = true;
	  			return false;
  	  		}
  		}else if(questionObj.questionId===14){
  			var pattern =/^(0|[1-9][0-9]*)$/;
  			if(!pattern.test(answerValue)) {
	  	  		questionObj.error = true;
	  			return false;
  	  		}
  		}*/
  		else if(!answerValue && answerValue !=0) {
  			questionObj.error = true;
  			return false;
  		}
  		
  		questionObj.answerTest = answerValue;       		
  		
  		questionObj.arrAns[0]=answerValue;
  		 $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};      		
  		  auraResponseService.setResponse($scope.auraRes)    		 
  		  
  		  auraPostfactory.reqObj($scope.urlList.auraPostUrl).then(function(response) {      			
  		//console.log(response.data.changedQuestion)
  		$scope.updateQuestionList(questionObj.questionId, response.data.changedQuestion, response.data)        			
  			//auraQuestionlist
      	}, function () {
      		//console.log('failed');
      	});
  	 };
  	 
  	 $scope.progressive = function(questionid,changedQuestion,questionObj){
  		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) { 
  			if(changedQuestion.questionAlias== Object.sectionName){
  				$scope.questionIndex = $scope.auraResponseDataList[selectedIndex].questions.indexOf(changedQuestion)
  				angular.forEach($scope.auraResponseDataList[selectedIndex].questions, function (baseQuestionObj, index) {  					
  					if(index<=$scope.questionIndex){
  						if(index==$scope.questionIndex){
  							var keepGoing = true;
  	  						$scope.subprogressive($scope.auraResponseDataList[selectedIndex].questions[index],questionid,keepGoing,questionObj)
  						}else{
  							$scope.setprogressiveError($scope.auraResponseDataList[selectedIndex].questions[index])
  						}
  						
  						
  					}
  					
  				});
  			}
  			
  		});
   	 };   	 
   	 
   	$scope.setprogressiveError =  function(baseQuestionObj){   		
   		if(baseQuestionObj.questionComplete){
   			angular.forEach(baseQuestionObj.auraAnswer, function (auraAnswer, selectedIndex) {  
   				angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions, function (subauraQustion, subSelectedIndex) { 
   					if(subauraQustion.questionComplete){
   						$scope.setprogressiveError(subauraQustion);   						
   					}else{
   						baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex].error=true;   						
   					}
   				});
   	   			
   	   		});
   		}else if(baseQuestionObj.classType=='HeightQuestion' && (!angular.isUndefined($scope.heightval) && $scope.heightval.length>0)){
   			baseQuestionObj.error = false;  
   		}else if(baseQuestionObj.classType=='WeightQuestion' && (!angular.isUndefined($scope.weightVal)  && $scope.weightVal.length>0)){
   			baseQuestionObj.error = false;  
   		}else{
   			baseQuestionObj.error = true;   			
   		}
   		
   	}   
   	$scope.subprogressive =  function(baseQuestionObj,questionid,keepGoing, updatedQuestionObj){
   		if(baseQuestionObj.questionId<questionid){
   			angular.forEach(baseQuestionObj.auraAnswer, function (auraAnswer, selectedIndex) { 
   				angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions, function (subauraQustion, subSelectedIndex) {
   					if(!subauraQustion.questionComplete && subauraQustion.questionId<questionid){
   						baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex].error=true;
   					}else if(subauraQustion.questionComplete && subauraQustion.questionId<questionid){
   						$scope.subprogressive(subauraQustion,questionid,keepGoing,updatedQuestionObj)
   					}else if(subauraQustion.questionComplete &&  subauraQustion.questionId==questionid){
   						var arrayAnswer = null;
   			   			if(subauraQustion.answerTest.indexOf(',')!==-1){
   			   				arrayAnswer = subauraQustion.answerTest.split(',');
   			   			}else if(!angular.isUndefined(subauraQustion.answerTest)){
   			   				arrayAnswer=[];
   			   				arrayAnswer[0] = subauraQustion.answerTest;
   			   			}
   			   			angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex].auraAnswer, function (answer, answerIndex) {
   			   				//console.log(arrayAnswer[arrayAnswer.length-1].indexOf(answer.answerValue))
   			   				if(arrayAnswer[arrayAnswer.length-1].indexOf(answer.answerValue)!==-1){
   			   					$scope.checkboxProgressive(baseQuestionObj.auraAnswer[selectedIndex].childQuestions[subSelectedIndex],answerIndex);
   			   				}
   			   			});
   					}
   				});
   	   		});
   		}else if(baseQuestionObj.questionId==questionid && baseQuestionObj.answerTest==updatedQuestionObj.answerTest ){
   			var arrayAnswer = null;
   			if(baseQuestionObj.answerTest.indexOf(',')!==-1){
   				arrayAnswer = baseQuestionObj.answerTest.split(',');
   			}else if(!angular.isUndefined(baseQuestionObj.answerTest)){
   				arrayAnswer=[];
   				arrayAnswer[0] = baseQuestionObj.answerTest;
   			}			
   			angular.forEach(baseQuestionObj.auraAnswer, function (answer, answerIndex) {
   				//console.log(arrayAnswer[arrayAnswer.length-1].indexOf(answer.answerValue))
   				if(arrayAnswer[arrayAnswer.length-1].indexOf(answer.answerValue)!==-1){
   					$scope.checkboxProgressive(baseQuestionObj,answerIndex);
   				}
   			});
   			
   				//answerID
   				//answerValue
   		}
   		
   	}
   	
   	$scope.checkboxProgressive = function(baseQuestionObj, answerIndex ){
   		angular.forEach(baseQuestionObj.auraAnswer, function (answer, index) {
   			if(index<answerIndex){
   				angular.forEach(baseQuestionObj.auraAnswer[index].childQuestions, function (subauraQustion, subSelectedIndex) {
   					$scope.setprogressiveError(subauraQustion)
   					
   				});
   			}
   		});
   	} 
	 

  	 
  	 
  	$scope.searchValSubmit = function (answerValue, questionObj,parentQuestionObj){
  		//console.log(questionObj.answerValue.answerValue)
  		//console.log(questionObj)
  		//console.log(parentQuestionObj)
  		questionObj.arrAns=[];
  		questionObj.arrAns[0] = questionObj.answerValue.answerValue;
  		 $scope.auraRes={"questionID":parentQuestionObj.questionId,"auraAnswers":questionObj.arrAns};
  		auraResponseService.setResponse($scope.auraRes)     		  
		  auraPostfactory.reqObj($scope.urlList.auraPostUrl).then(function(response) {			 
			  $scope.updateQuestionList(questionObj.questionId, response.data.changedQuestion,response.data)    		
		  });
  	}
  	 
  	 $scope.updateCheckBox = function (answerValue, questionObj,index){      	
  		 //console.log(questionObj.answerTest);
  		 if(questionObj.arrAns.length==0){
  			var answerArray = questionObj.answerTest.split(',');
  			if(answerArray.length>1){
  				questionObj.arrAns = answerArray;
  			}else{
  				questionObj.arrAns[0] = questionObj.answerTest;
  			}
  		 }
  		   		  
  		
  		 var addtoArray=true;
  		if(answerValue.indexOf('None of the above') !== -1){
  			questionObj.arrAns=[];
  			questionObj.arrAns[0]= answerValue;
  		}else{
  			for (var i=0;i<questionObj.arrAns.length;i++){               	 
  				//to remove white space
  				questionObj.arrAns[i] = $.trim(questionObj.arrAns[i])
  				//console.log(questionObj.arrAns[i])
  				 //console.log(answerValue)
  				  if(questionObj.arrAns[i]==answerValue ){
                   	questionObj.arrAns.splice(i, 1);
                   	addtoArray=false
                  }
            }
            if(addtoArray==true){
           	 questionObj.arrAns.splice(index, 0, answerValue);
            }
  		}            
         //removing none of the above, if there are other answers
  		if(questionObj.arrAns.length>1){
  			for (var i=0;i<questionObj.arrAns.length;i++){ 
      			if(questionObj.arrAns[i].indexOf('None of the above') !== -1){
      				questionObj.arrAns.splice(i, 1);
      			}
      		
  			}
  		}
         $scope.auraRes={"questionID":questionObj.questionId,"auraAnswers":questionObj.arrAns};
         //console.log(JSON.stringify(questionObj.arrAns))
         auraResponseService.setResponse($scope.auraRes)           
         auraPostfactory.reqObj($scope.urlList.auraPostUrl).then(function(response) {            	
    			//console.log(response.data)    			
    			$scope.updateQuestionList(questionObj.questionId,response.data.changedQuestion,response.data)         	 
   				
       	}, function () {
       		//console.log('failed');
           	});
      	 }; 
      	 
      	$scope.checkIncompleteQuestion = function (baseQuestionObj, keepGoing){       		
       		if(baseQuestionObj.questionComplete && keepGoing){
       			angular.forEach(baseQuestionObj.auraAnswer, function (auraAnswer, selectedIndex) {  
       				angular.forEach(baseQuestionObj.auraAnswer[selectedIndex].childQuestions, function (subauraQustion, subSelectedIndex) { 
       					if(subauraQustion.questionComplete){
       						$scope.setprogressiveError(subauraQustion);   						
       					}else if(!subauraQustion.questionComplete){
       						$scope.keepGoing = false; 						
       					}
       				});
       	   			
       	   		});
       		}else if(baseQuestionObj.classType=='HeightQuestion' && (!angular.isUndefined($scope.heightval) && $scope.heightval.length>0)){
       			$scope.keepGoing = true;  
       		}else if(baseQuestionObj.classType=='WeightQuestion' && (!angular.isUndefined($scope.weightVal)  && $scope.weightVal.length>0)){
       			$scope.keepGoing = true;  
       		}else{
       			$scope.keepGoing = false; 			
       		}     		
       	
      	}
      	 
      	 $scope.proceedToNext = function (){
      		 //check if questions are answered
      		$scope.coverDetails.lastSavedOn = '';
      		$scope.keepGoing = true;
      		angular.forEach($scope.auraResponseDataList, function (Object, selectedIndex) {      			
      			angular.forEach($scope.auraResponseDataList[selectedIndex].questions, function (baseQuestionObj, index) { 
      				if($scope.keepGoing){
      					$scope.checkIncompleteQuestion($scope.auraResponseDataList[selectedIndex].questions[index],$scope.keepGoing)  
      				}
      				    				
      			});
      			
      		});
      		//console.log($scope.keepGoing)
      		if($scope.keepGoing){
      			 $rootScope.$broadcast('disablepointer');
      			submitAura.requestObj($scope.urlList.submitAuraUrl).then(function(response) {         			      				
      				$scope.auraResponses = response.data    
      				if($scope.auraResponses.overallDecision!='DCL'){
      					clientMatch.reqObj($scope.urlList.clientMatchUrl).then(function(clientMatchResponse) {
      						if(clientMatchResponse.data.matchFound){
      							$scope.clientmatchreason = '';
      							$scope.auraResponses.clientMatched = clientMatchResponse.data.matchFound
      							$scope.auraResponses.overallDecision='RUW'
      								$scope.information = clientMatchResponse.data.information
      								angular.forEach($scope.information, function (infoObj,index) {      									
      									if(index==$scope.information.length-1){
      										$scope.clientmatchreason = $scope.clientmatchreason+ infoObj.system +' client match : '+infoObj.key+", "
      										$scope.clientmatchreason = $scope.clientmatchreason.substring(0,$scope.clientmatchreason.lastIndexOf(','))      										
      									}else{
      										$scope.clientmatchreason = $scope.clientmatchreason+ infoObj.system +' client match : '+infoObj.key+", "	
      									}     																	
      									
      								})
      							$scope.auraResponses.clientMatchReason= $scope.clientmatchreason;
      							$scope.auraResponses.specialTerm = false;
      						}    									
      						
          					PersistenceService.setChangeCoverAuraDetails($scope.auraResponses);          					
          					$location.path('/cancelConfirmation/1');
          				});
      				}else if($scope.auraResponses.overallDecision == 'DCL'){
      					if($scope.coverDetails != null && $scope.changeCoverOccDetails != null && $scope.deathAddnlDetails != null && 
      		    				$scope.tpdAddnlDetails != null && $scope.ipAddnlDetails != null && $scope.auraResponses != null && $scope.personalDetails[0] != null){
      		    			$scope.coverDetails.lastSavedOn = '';
      		    			$scope.details={};
      		    			$scope.details.addnlDeathCoverDetails = $scope.deathAddnlDetails;
      		    			$scope.details.addnlTpdCoverDetails = $scope.tpdAddnlDetails;
      		    			$scope.details.addnlIpCoverDetails = $scope.ipAddnlDetails;
      		    			$scope.details.occupationDetails = $scope.changeCoverOccDetails;
      		    			var temp = angular.extend($scope.details,$scope.coverDetails);
      		    			var aura = angular.extend(temp,$scope.auraResponses);
      		    			var submitObject = angular.extend(aura, $scope.personalDetails[0]);
      		    			auraResponseService.setResponse(submitObject);
      		    			submitEapply.reqObj($scope.urlList.submitEapplyUrl).then(function(response) {  
      		            		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
      		            		PersistenceService.setNpsUrl(response.data.npsTokenURL);
      		            		$location.path('/cancelDecisionDecline');
      		            	}, function(err){
      		            		//console.log('Error saving the cover ' + err);
      		            	});
      		            }
      				}			
         			
         		 });
  			}   		 
      		 
      	 }
      	// Save
      	var appNum;
        appNum = PersistenceService.getAppNumber();
      	 $scope.saveAura = function (){
      		$scope.auraSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+ appNum +'</STRONG><BR><BR> Keep this number handy in case you need to retrieve your application later.<BR><BR>');
      		if($scope.coverDetails != null && $scope.changeCoverOccDetails != null &&  $scope.deathAddnlDetails != null && $scope.tpdAddnlDetails != null && $scope.ipAddnlDetails != null &&  $scope.personalDetails[0] != null){
        		$scope.coverDetails.lastSavedOn = 'AuraCancelPage';
        		$scope.details={};
        		$scope.details.addnlDeathCoverDetails = $scope.deathAddnlDetails;
    			$scope.details.addnlTpdCoverDetails = $scope.tpdAddnlDetails;
    			$scope.details.addnlIpCoverDetails = $scope.ipAddnlDetails;
    			$scope.details.occupationDetails = $scope.changeCoverOccDetails;
        		//$scope.auraDetails
        		var temp = angular.extend($scope.details,$scope.coverDetails);
            	var saveAuraObject = angular.extend(temp, $scope.personalDetails[0]);
            	auraResponseService.setResponse(saveAuraObject)
            	 $rootScope.$broadcast('disablepointer');
    	        saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {  
    	                //console.log(response.data)
    	        });
        	}
       };
      
       $scope.auraSaveAndExitPopUp = function (hhText) {
	      	
			var dialog1 = ngDialog.open({
				    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
					className: 'ngdialog-theme-plain custom-width',
					preCloseCallback: function(value) {
					       var url = "/landing"
					       $location.path( url );
					       return true
					},
					plain: true
			});
			dialog1.closePromise.then(function (data) {
				//console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};
     
      	  
      	$scope.clickToOpen = function (hhText) {
      		//console.log(hhText)
			var dialog = ngDialog.open({
				/*template: '<p>'+hhText+'</p>' +
					'<div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(1)">Close Me</button></div>',*/
					template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" style="width: 100%;font-weight: normal !important; font-size: 18px !important; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">Close</button></div><div class="col-sm-4"></div></div></div>',
					className: 'ngdialog-theme-plain',
					plain: true
			});
			dialog.closePromise.then(function (data) {
				//console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
			});
		};
      	 

    }]);    
   /*Aura Page Controller Ends*/
