package au.com.metlife.eapply.underwriting.response.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RuleUnselectedAnswer implements Serializable{
	private String alias;
	private String severity;
	private String value;
	
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
