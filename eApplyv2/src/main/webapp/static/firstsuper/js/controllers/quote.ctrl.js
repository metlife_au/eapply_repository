/* Change Cover Controller,Progressive and Mandatory validations Starts  */
FirstSuperApp.controller('quote',['$scope', '$rootScope', '$routeParams', '$location', '$timeout', '$window', 'ngDialog', 'fetchUrlSvc', 'fetchPersoanlDetailSvc', 'fetchQuoteSvc', 'fetchOccupationSvc', 'fetchDeathCoverSvc', 'fetchTpdCoverSvc', 'fetchIpCoverSvc', 'MaxLimitService', 'CheckAccAppService','NewOccupationService', 'CalculateService', '$q', 'appData', 'fetchAppNumberSvc', 'saveEapplyData', 'printPageSvc', 'DownloadPDFSvc', 'auraRespSvc','propertyService','APP_CONSTANTS',
function($scope, $rootScope, $routeParams, $location, $timeout, $window, ngDialog, fetchUrlSvc, fetchPersoanlDetailSvc, fetchQuoteSvc, fetchOccupationSvc, fetchDeathCoverSvc, fetchTpdCoverSvc, fetchIpCoverSvc, MaxLimitService, CheckAccAppService, NewOccupationService, CalculateService, $q, appData, fetchAppNumberSvc, saveEapplyData, printPageSvc, DownloadPDFSvc, auraRespSvc,propertyService,APP_CONSTANTS) {

  $scope.QPD = {
    manageType: 'CCOVER',
    partnerCode: 'FIRS',
    lastSavedOn: 'Quotepage',
    auraDisabled: true,
    email: null,
    contactType: 1,
    contactPhone: null,
    contactPrefTime: null,
    dob: null,
    age: null,
    occupationDetails: {
      fifteenHr: null,
      /*citizenQue: null,*/
      industryCode: null,
      /*ownBussinessQues: null,
      ownBussinessYesQues: null,
      ownBussinessNoQues: null,*/
      withinOfficeQue: null,
      tertiaryQue: null,
      managementRoleQue: null,
      hazardousQue: null,
      gender: null,
      salary: null,
      otherOccupation: null,
      occupation: null
    },
    auraDisabled: null,
    existingDeathAmt: '0',
    existingDeathUnits: '0',
    deathOccCategory: null,
    freqCostType: null,
    ipIncreaseFlag: null,
    addnlDeathCoverDetails: {
      deathCoverName: null,
      deathCoverType: null,
      deathFixedAmt: null,
      deathInputTextValue: null,
      deathCoverPremium: null
    },
    existingTpdAmt: '0',
    existingTPDUnits: '0',
    tpdOccCategory: null,
    addnlTpdCoverDetails: {
      tpdCoverName: null,
      tpdCoverType: null,
      tpdFixedAmt: null,
      tpdInputTextValue: null,
      tpdCoverPremium: null
    },
    existingIPAmount: '0',
    existingIPUnits: null,
    ipOccCategory: null,
    ipcheckbox: null,
    indexationDeath: false,
    indexationTpd: false,
    addnlIpCoverDetails: {
      ipCoverName: null,
      ipCoverType: 'IpUnitised',
      ipFixedAmt: null,
      ipInputTextValue: null,
      ipCoverPremium: null,
      waitingPeriod: null,
      benefitPeriod: null
    },
    totalPremium: 0,
    appNum: null,
    ackCheck: false,
    dodCheck: false,
    privacyCheck: false
  };
  $scope.modelOptions = {updateOn: 'blur'};
	$scope.phoneNumbr = /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2|4|3|7|8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/;
	$scope.emailFormat = APP_CONSTANTS.emailFormat;
	$scope.regex = /[0-9]{1,3}/;
  $scope.deathCvrRegex = /^\d{1,8}(,\d{1,8}){0,9}?$/;
	$scope.isDCCoverRequiredDisabled = false;
	$scope.isDCCoverTypeDisabled = false;
	$scope.isTPDCoverRequiredDisabled = false;
	$scope.isTPDCoverTypeDisabled = false;

	$scope.isWaitingPeriodDisabled = false;
	$scope.isBenefitPeriodDisabled = false;
	$scope.isIPCoverRequiredDisabled = false;

	$scope.isIpSalaryCheckboxDisabled = false;
	$scope.ipWarningFlag = false;
	$scope.ackFlag = false;
	$scope.modalShown = false;
	$scope.invalidSalAmount = false;
	$scope.dcIncreaseFlag = false;
	$scope.tpdIncreaseFlag = false;
	$scope.QPD.ipIncreaseFlag = false;
	$scope.QPD.auraDisabled = false;
	$scope.disclaimerFlag = true;
  $scope.IndustryOptions = null;
  $scope.OccupationList = null;

	$scope.showWithinOfficeQuestion = false;
  $scope.showTertiaryQuestion = false;
  $scope.showHazardousQuestion = false;
  $scope.showOutsideOfficeQuestion = false;
	$scope.urlList = fetchUrlSvc.getUrlList();
	$scope.QPD.freqCostType = 'Weekly';
	$scope.ipWarning = false;
	$scope.QPD.ipcheckbox = false;
	$scope.QPD.eligibleFrStIp = false;

  $scope.QPD.isDeathDisabled = false;
  $scope.QPD.isTPDDisabled = false;
  $scope.QPD.isIPDisabled = false;


	/*Error Flags*/
	$scope.dodFlagErr = null;
	$scope.privacyFlagErr = null;
  $scope.deathErrorFlag = false;
  $scope.tpdErrorFlag = false;
  $scope.ipErrorFlag = false;
  $scope.eligibleFrStIpFlag = false;
  $scope.salMaxlimit= false;
  $scope.disableDeath24Hrs = false;
  $scope.disableTpd24Hrs = false;
  $scope.disableIP24Hrs = false;

  $scope.waitingPeriodOptions = ["30 Days", "60 Days", "90 Days"];
  $scope.benefitPeriodOptions = ['2 Years'];
  $scope.premiumFrequencyOptions = ['Monthly', 'Yearly', 'Weekly'];
  $scope.contactTypeOptions = [{
    text: 'Home',
    value: '2'
  }, {
    text: 'Work',
    value: '3'
  }, {
    text: 'Mobile',
    value: '1'
  }];

  $scope.FormOneFields = ['contactEmail', 'contactPhone','contactPrefTime','gender'];
  $scope.OccupationFormFields = ['fifteenHrsQuestion','industry','occupation','annualSalary'];
  $scope.OccupationOtherFormFields = ['fifteenHrsQuestion','industry','occupation','otherOccupation','annualSalary'];
  $scope.CoverCalculatorFormFields =['coverName','coverType','requireCover','tpdCoverName','tpdCoverType','TPDRequireCover'];

  $scope.personalDetails = {};
  $scope.contactDetails = {};
  $scope.isEmpty = function(value){
    return ((value == "" || value == null) || value == "0");
  };
  $scope.init = function() {
    var defer = $q.defer();
    $scope.deathCoverDetails = fetchDeathCoverSvc.getDeathCover();
		$scope.tpdCoverDetails = fetchTpdCoverSvc.getTpdCover();
		$scope.ipCoverDetails = fetchIpCoverSvc.getIpCover();

		$scope.QPD.deathOccCategory = $scope.deathCoverDetails.occRating || 'General';
		$scope.QPD.tpdOccCategory = $scope.tpdCoverDetails.occRating || 'General';
		$scope.QPD.ipOccCategory = $scope.ipCoverDetails.occRating || 'General';

    $scope.waitingPeriodOptions = ["30 Days", "60 Days", "90 Days"];
    $scope.benefitPeriodOptions = ['2 Years'];

    $scope.QPD.addnlIpCoverDetails.waitingPeriod = $scope.waitingPeriodOptions.indexOf($scope.ipCoverDetails.waitingPeriod) > -1 ? $scope.ipCoverDetails.waitingPeriod : '90 Days' || '90 Days';
    $scope.QPD.addnlIpCoverDetails.benefitPeriod = $scope.waitingPeriodOptions.indexOf($scope.ipCoverDetails.benefitPeriod) > -1 ? $scope.ipCoverDetails.benefitPeriod : '2 Years' || '2 Years';

    if($scope.deathCoverDetails.type == "1") {
      $scope.QPD.addnlDeathCoverDetails.deathCoverType = "DcUnitised";
      $scope.QPD.exDcCoverType = "DcUnitised";
      $scope.QPD.addnlDeathCoverDetails.deathInputTextValue = $scope.deathCoverDetails.units;
      $scope.QPD.existingDeathAmt = $scope.deathCoverDetails.amount || 0;
      $scope.QPD.existingDeathUnits = $scope.deathCoverDetails.units || 0;
      $timeout(function() {
        showhide('nodollar1','dollar1');
        showhide('nodollar','dollar');
      });
      
    } else if($scope.deathCoverDetails.type == "2") {
      $scope.QPD.addnlDeathCoverDetails.deathCoverType = "DcFixed";
      $scope.QPD.exDcCoverType = "DcFixed";
      $scope.QPD.addnlDeathCoverDetails.deathInputTextValue = $scope.deathCoverDetails.amount;
      $scope.QPD.existingDeathAmt = $scope.deathCoverDetails.amount || 0;
      $scope.isDCCoverTypeDisabled = true;
    }

    if($scope.tpdCoverDetails.type == "1") {
      $scope.QPD.addnlTpdCoverDetails.tpdCoverType = "TPDUnitised";
      $scope.QPD.exTpdCoverType = "TPDUnitised";
      $scope.QPD.addnlTpdCoverDetails.tpdInputTextValue = $scope.tpdCoverDetails.units;
      $scope.QPD.existingTpdAmt = $scope.tpdCoverDetails.amount || 0;
      $scope.QPD.existingTPDUnits = $scope.tpdCoverDetails.units || 0;
      $timeout(function() {
        showhide('nodollar1','dollar1');
        showhide('nodollar','dollar');
      });
      
    } else if($scope.tpdCoverDetails.type == "2") {
      $scope.QPD.addnlTpdCoverDetails.tpdCoverType = "TPDFixed";
      $scope.QPD.exTpdCoverType = "TPDFixed";
      $scope.QPD.addnlTpdCoverDetails.tpdInputTextValue = $scope.tpdCoverDetails.amount;
      $scope.QPD.existingTpdAmt = $scope.tpdCoverDetails.amount || 0;
      $scope.isTPDCoverTypeDisabled = true;
    }
    $scope.QPD.addnlIpCoverDetails.ipInputTextValue = $scope.ipCoverDetails.units;
    $scope.QPD.existingIPAmount = $scope.ipCoverDetails.amount || 0;
    $scope.QPD.existingIPUnits = $scope.ipCoverDetails.units;
    $scope.inputDetails = fetchPersoanlDetailSvc.getMemberDetails() || {};

    $scope.personalDetails = $scope.inputDetails.personalDetails || {};
    $scope.contactDetails = $scope.inputDetails.contactDetails || {};

    fetchQuoteSvc.getList($scope.urlList.quoteUrl,"FIRS").then(function(res){
			$scope.IndustryOptions = res.data;
		}, function(err){
			console.info("Error while fetching industry list " + JSON.stringify(err));
		});

    $scope.QPD.age = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;

    if($scope.deathCoverDetails && $scope.deathCoverDetails.benefitType && $scope.deathCoverDetails.benefitType == 1 ){
			if($scope.QPD.age <= 11 || $scope.QPD.age > 70){
				$('#deathsection').removeClass('active');
				$("#death").css("display", "none");
				$scope.QPD.isDeathDisabled = true;
        $scope.QPD.addnlDeathCoverDetails.deathInputTextValue = 0;
			}
		}
		if($scope.tpdCoverDetails && $scope.tpdCoverDetails.benefitType && $scope.tpdCoverDetails.benefitType == 2 ){
			if($scope.QPD.age <= 11 || $scope.QPD.age > 70){
				$('#tpdsection').removeClass('active');
				$("#tpd").css("display", "none");
				$scope.QPD.isTPDDisabled = true;
        $scope.QPD.addnlTpdCoverDetails.tpdInputTextValue = 0;
			}
		}
		if($scope.ipCoverDetails && $scope.ipCoverDetails.benefitType && $scope.ipCoverDetails.benefitType == 4 ){
			if($scope.QPD.age < 16 || $scope.QPD.age > 65){
				$('#ipsection').removeClass('active');
				$("#sc").css("display", "none");
				$scope.QPD.isIPDisabled = true;
        $scope.QPD.addnlIpCoverDetails.ipInputTextValue = 0;
			}
		}

		MaxLimitService.getMaxLimits($scope.urlList.maxLimitUrl, "FIRS", $scope.inputDetails.memberType, "CCOVER").then(function(res){
			var limits = res.data;
			$scope.annualSalForUpgradeVal = limits[0].annualSalForUpgradeVal;
    	$scope.DCMaxAmount = limits[0].deathMaxAmount;
    	$scope.TPDMaxAmount = limits[0].tpdMaxAmount;
    	$scope.IPMaxAmount = limits[0].ipMaxAmount;
    	$scope.annualSalMaxLimit = limits[0].annualSalMaxLimit;
      defer.resolve(res);
		}, function(error){
			console.info('Something went wrong while fetching limits ' + error);
      defer.reject(err);
		});
    return defer.promise;
  };

  $scope.checkAccApplication=function(manageTypeCC){
	  CheckAccAppService.checkAccApps($scope.urlList.accAppUrl,"FIRS",$scope.clientRefNumCCover,manageTypeCC,$scope.QPD.dob,$scope.personalDetails.firstName,$scope.personalDetails.lastName).then(function(res){
				console.log(res.data);
				var appDetails = res.data;
				
				$scope.disableDeath24Hrs = appDetails.deathOnlyStatus;
				$scope.disableTpd24Hrs = appDetails.tpdStatus;
				$scope.disableIP24Hrs = appDetails.ipStatus;
				$scope.deathDisableMsg = "You have had an application for Death cover accepted within the last 24 hours. You can apply for further cover only after 24 hours has passed since your last application was accepted.";
				$scope.tpdDisableMsg = "You have had an application for TPD cover accepted within the last 24 hours. You can apply for further cover only after 24 hours has passed since your last application was accepted.";
				$scope.ipDisabledMsg = "You have had an application for IP cover accepted within the last 24 hours. You can apply for further cover only after 24 hours has passed since your last application was accepted.";
				if($scope.disableDeath24Hrs)
					{
						$scope.QPD.addnlDeathCoverDetails.deathInputTextValue = 0;
					}
				if($scope.disableTpd24Hrs)
					{
						$scope.QPD.addnlTpdCoverDetails.tpdInputTextValue = 0;
					}
				if($scope.disableIP24Hrs)
					{
						$scope.QPD.addnlIpCoverDetails.ipInputTextValue = 0;
					}
			}, function(err){
				console.log("Error " + err);
			});

	}; 
  
  $scope.init().then(function() {
    angular.extend($scope.QPD, appData.getAppData());
    $scope.QPD.contactType = $scope.contactDetails.prefContact || '1';
   
   if($scope.QPD.contactType == "1") {
      $scope.QPD.contactPhone = $scope.contactDetails.mobilePhone;
    } else if($scope.QPD.contactType == "2") {
      $scope.QPD.contactPhone = $scope.contactDetails.homePhone;
    } else if($scope.QPD.contactType == "3") {
      $scope.QPD.contactPhone = $scope.contactDetails.workPhone;
    } else {
      $scope.QPD.contactPhone = '';
    }
    $scope.QPD.dob = moment($scope.QPD.dob, "DD/MM/YYYY").format('DD/MM/YYYY');
    if($scope.QPD.occupationDetails.fifteenHr == 'No') {
      $scope.disableIpCover();
    }
    if($scope.QPD.occupationDetails.industryCode) {
      fetchOccupationSvc.getOccupationList($scope.urlList.occupationUrl, "FIRS", $scope.QPD.occupationDetails.industryCode).then(function(res){
        $scope.OccupationList = res.data;
        $scope.getCategoryFromDB();
      }, function(err){
        console.info("Error while fetching occupations " + JSON.stringify(err));
      });
    }
    $scope.QPD.ipcheckbox = ($scope.QPD.ipcheckbox == 'true' || $scope.QPD.ipcheckbox == true) ? true : false;
    if($scope.QPD.ipcheckbox) {
      $scope.insureNinetyPercentIp();
    	$('#ipsalarycheck').parent().addClass('active');
		  $('#ipsalarycheck').attr('checked','checked');
    }
    $scope.$watch('QPD.dodCheck', function(newVal, oldVal) {
      $scope.togglePrivacy(newVal);
      $scope.toggleContact(newVal && $scope.QPD.privacyCheck);
      $scope.toggleOccupation(newVal && $scope.formOne.$valid && $scope.QPD.dodCheck && $scope.QPD.privacyCheck);
      $scope.toggleCoverCalc(newVal && $scope.formOne.$valid && $scope.occupationForm.$valid && $scope.QPD.dodCheck && $scope.QPD.privacyCheck);
    });

    $scope.$watch('QPD.privacyCheck', function(newVal, oldVal) {
      $scope.toggleContact(newVal);
      $scope.toggleOccupation(newVal && $scope.formOne.$valid && $scope.QPD.dodCheck && $scope.QPD.privacyCheck);
      $scope.toggleCoverCalc(newVal && $scope.formOne.$valid && $scope.occupationForm.$valid && $scope.QPD.dodCheck && $scope.QPD.privacyCheck);
    });

    $scope.$watch('formOne.$valid', function(newVal, oldVal) {
       $scope.toggleOccupation(newVal && $scope.QPD.dodCheck && $scope.QPD.privacyCheck);
       $scope.toggleCoverCalc(newVal && $scope.occupationForm.$valid && $scope.QPD.dodCheck && $scope.QPD.privacyCheck);
    });

    $scope.$watch('occupationForm.$valid', function(newVal, oldVal) {
    	if($scope.invalidSalAmount || $scope.salMaxlimit){
    		newVal = false;
    	 }
       $scope.toggleCoverCalc(newVal && $scope.formOne.$valid && $scope.QPD.dodCheck && $scope.QPD.privacyCheck);
    });
    $scope.inputDetails = fetchPersoanlDetailSvc.getMemberDetails() || {};
    $scope.clientRefNumCCover = $scope.inputDetails.clientRefNumber;
    $scope.checkAccApplication($scope.QPD.manageType);
    $scope.validateDeathTpdIpAmounts();
    //$scope.toggleIPCondition();
  }, function() {

  });
  $scope.preparePageModel = function() {
    //var tempData = appData.getQuoteData();
  }
  $scope.changePrefContactType = function() {
    if($scope.QPD.contactType == "1") {
			$scope.QPD.contactPhone = $scope.contactDetails.mobilePhone;
		} else if($scope.QPD.contactType == "2") {
			$scope.QPD.contactPhone = $scope.contactDetails.homePhone;
		} else if($scope.QPD.contactType == "3") {
			$scope.QPD.contactPhone = $scope.contactDetails.workPhone;
		} else {
      $scope.QPD.contactPhone = '';
    }
  };

  $scope.isCollapsible = function(targetEle, event) {
    var dodLabelCheck = $('#dodLabel').hasClass('active');
    var privacyLabelCheck = $('#privacyLabel').hasClass('active');
    if( targetEle == 'collapseprivacy' && !dodLabelCheck) {
      if($('#dodLabel').is(':visible'))
        $scope.dodFlagErr = true;
      event.stopPropagation();
      return false;
    } else if( targetEle == 'collapseOne' && (!dodLabelCheck || !privacyLabelCheck)) {
      if($('#privacyLabel').is(':visible'))
        $scope.privacyFlagErr = true;
      event.stopPropagation();
      return false;
    }  else if( targetEle == 'collapseTwo' && (!dodLabelCheck || !privacyLabelCheck || $("#collapseOne form").hasClass('ng-invalid'))) {
      if($("#collapseOne form").is(':visible'))
        $scope.onFormContinue($scope.formOne);
      event.stopPropagation();
      return false;
    }  else if( targetEle == 'collapseThree' && (!dodLabelCheck || !privacyLabelCheck || $("#collapseOne form").hasClass('ng-invalid') || $("#collapseTwo form").hasClass('ng-invalid'))|| $scope.invalidSalAmount || $scope.salMaxlimit) {
      if($("#collapseTwo form").is(':visible'))
        $scope.onFormContinue($scope.occupationForm);
      event.stopPropagation();
      return false;
    }
  }

  $scope.toggleOccupation = function(checkFlag) {
      if((checkFlag && $('#collapseTwo').hasClass('collapse in')) || (!checkFlag && !$('#collapseTwo').hasClass('collapse in')))
        return false;
      $("a[data-target='#collapseTwo']").click(); /* Can be improved */
  };

  $scope.toggleCoverCalc = function(checkFlag) {
      if((checkFlag && $('#collapseThree').hasClass('collapse in')) || (!checkFlag && !$('#collapseThree').hasClass('collapse in')))
        return false;
      $("a[data-target='#collapseThree']").click(); /* Can be improved */
  };

  $scope.togglePrivacy = function(checkFlag) {
      if((checkFlag && $('#collapseprivacy').hasClass('collapse in')) || (!checkFlag && !$('#collapseprivacy').hasClass('collapse in')))
        return false;
      $("a[data-target='#collapseprivacy']").click(); /* Can be improved */
  };

  $scope.toggleContact = function(checkFlag) {
      if((checkFlag && $('#collapseOne').hasClass('collapse in')) || (!checkFlag && !$('#collapseOne').hasClass('collapse in')))
        return false;
      $("a[data-target='#collapseOne']").click(); /* Can be improved */

  };
  $scope.checkDodState = function() {
    $timeout(function() {
      var dodLabelCheck = $('#dodLabel').hasClass('active');
      $scope.QPD.dodCheck = dodLabelCheck;
      $scope.dodFlagErr = !dodLabelCheck;
      if(dodLabelCheck) {
        $scope.togglePrivacy(true);
      } else {
        $scope.togglePrivacy(false);
        $scope.toggleContact(false);
        $scope.toggleOccupation(false);
        $scope.toggleCoverCalc(false);
      }
    }, 1);
  };

  $scope.checkPrivacyState  = function() {
    $timeout(function() {
      var privacyLabelCheck = $('#privacyLabel').hasClass('active');
      $scope.QPD.privacyCheck = privacyLabelCheck;
      $scope.privacyFlagErr = !privacyLabelCheck;
      if(privacyLabelCheck) {
        $scope.toggleContact(true);
      } else {
        $scope.toggleContact(false);
        $scope.toggleOccupation(false);
        $scope.toggleCoverCalc(false);
      }
    }, 1);
  };
  
  $scope.clickToOpen = function (hhText) {

	var dialog = ngDialog.open({
		template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>',
		className: 'ngdialog-theme-plain',
		plain: true
	});
	dialog.closePromise.then(function (data) {
		console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
	});
  };
	
  $scope.checkPreviousMandatoryFields  = function (elementName,formName) {
    var formFields;
    if(formName == 'formOne') {
      formFields = $scope.FormOneFields;
    } else if(formName == 'occupationForm') {
      if($scope.QPD.occupationDetails.occupation != undefined && $scope.QPD.occupationDetails.occupation == 'Other') {
        formFields = $scope.OccupationOtherFormFields;
      } else {
        formFields = $scope.OccupationFormFields;
      }
    } else if(formName == 'coverCalculatorForm') {
      formFields = $scope.CoverCalculatorFormFields;
    }
    var inx = formFields.indexOf(elementName);
    if(inx > 0){
      for(var i = 0; i < inx ; i++) {
        if($scope[formName][formFields[i]])
          $scope[formName][formFields[i]].$touched = true;
      }
    }
  };

  $scope.disableIpCover = function() {
    $scope.QPD.addnlIpCoverDetails.ipCoverName = 'No change';
    $scope.QPD.ipcheckbox = false;
    $scope.QPD.addnlIpCoverDetails.ipInputTextValue = parseInt($scope.ipCoverDetails.units);
    $scope.isIPCoverNameDisabled = true;
    $scope.isWaitingPeriodDisabled = true;
    $scope.isBenefitPeriodDisabled = true;
    $scope.isIPCoverRequiredDisabled = true;
    $scope.isIpSalaryCheckboxDisabled = true;
    $scope.ipWarning = true;
    $('#ipsalarycheck').removeAttr('checked');
    $('#ipsalarychecklabel').removeClass('active');
  }
  $scope.toggleIPCondition = function() {
    if($scope.QPD.isIPDisabled)
      return false;
    if($scope.QPD.occupationDetails.fifteenHr == 'No') {
      $scope.disableIpCover();
    } else {
      $scope.QPD.addnlIpCoverDetails.ipCoverName = '';
      $scope.QPD.addnlIpCoverDetails.ipInputTextValue = parseInt($scope.ipCoverDetails.units);
      $scope.isIPCoverNameDisabled = false;
      $scope.isWaitingPeriodDisabled = false;
      $scope.isBenefitPeriodDisabled = false;
      $scope.isIPCoverRequiredDisabled = false;
      $scope.isIpSalaryCheckboxDisabled = false;
      $scope.ipWarning = false;
    }
    $scope.renderOccupationQuestions();
    $scope.QPD.occupationDetails.withinOfficeQue = null;
    $scope.QPD.occupationDetails.tertiaryQue = null;
    $scope.QPD.occupationDetails.hazardousQue = null;
    $scope.QPD.occupationDetails.managementRoleQue = null;
  };

  $scope.checkAnnualSalary = function() {
	  $scope.salMaxlimit = false;
	  if(parseInt($scope.QPD.occupationDetails.salary) == 0){
	    $scope.invalidSalAmount = true;
	    $scope.toggleCoverCalc(false);
	  }else if(parseInt($scope.QPD.occupationDetails.salary) >= $scope.annualSalMaxLimit){
	    $scope.salMaxlimit=true;
	    $scope.toggleCoverCalc(false);
	  }else{
	    $scope.invalidSalAmount = false;
	    if(this.occupationForm.$valid){
	    	$scope.toggleCoverCalc(true);
	    }
	    }
      if(!$scope.invalidSalAmount && !$scope.salMaxlimit){
    	  $scope.renderOccupationQuestions();
      }
    /*if(parseInt($scope.annualSalary) == 0){
      $scope.invalidSalAmount = true;
    } else{
      $scope.invalidSalAmount = false;
    }
    if(!$scope.invalidSalAmount)
      $scope.renderOccupationQuestions();*/

    if($scope.QPD.ipcheckbox) {
      $scope.insureNinetyPercentIp();      
    }
  }

  $scope.insureNinetyPercentIp = function() {
    $scope.ipWarningFlag = false;
    var ipMaxLimits = $scope.IPMaxAmount/100;
    $timeout(function() {
      $scope.QPD.ipcheckbox = $('#ipsalarycheck').prop('checked');
      if($scope.QPD.ipcheckbox == true) {
        //$scope.QPD.addnlIpCoverDetails.ipInputTextValue  = Math.ceil(parseFloat((0.85 * ($scope.QPD.occupationDetails.salary/12)).toFixed(2)) / 100)*100;
    	  $scope.QPD.addnlIpCoverDetails.ipInputTextValue  = Math.ceil(parseFloat((0.85 * ($scope.QPD.occupationDetails.salary/12)).toFixed(2)) / 100);
        if($scope.QPD.addnlIpCoverDetails.ipInputTextValue < parseInt($scope.ipCoverDetails.units)) {
          $scope.QPD.addnlIpCoverDetails.ipInputTextValue = $scope.ipCoverDetails.units;
          $scope.ipWarningFlag = true;
        }
        else if($scope.QPD.addnlIpCoverDetails.ipInputTextValue > ipMaxLimits)
        	{
        	$scope.QPD.addnlIpCoverDetails.ipInputTextValue = ipMaxLimits;
        	}
        
        /*else if($scope.QPD.addnlIpCoverDetails.ipInputTextValue < parseInt($scope.ipCoverDetails.amount)) {
            $scope.QPD.addnlIpCoverDetails.ipInputTextValue = parseInt($scope.ipCoverDetails.amount);
        }*/ else {
          $scope.ipWarningFlag = false;
        }
        $scope.isIPCoverRequiredDisabled = true;
      } else {
        /*$scope.QPD.addnlIpCoverDetails.ipInputTextValue = parseInt($scope.ipCoverDetails.amount);*/
        $scope.QPD.addnlIpCoverDetails.ipInputTextValue = '';
        $scope.isIPCoverRequiredDisabled = false;
        $scope.ipWarningFlag = false;
      }
      $scope.validateDeathTpdIpAmounts();
    });
  };

  /*$scope.checkOwnBusinessQuestion = function() {
    $scope.QPD.occupationDetails.ownBussinessYesQues = null;
    $scope.QPD.occupationDetails.ownBussinessNoQues = null;
    if($scope.QPD.occupationDetails.ownBussinessQues == 'Yes'){
      $scope.OccupationFormFields = ['fifteenHrsQuestion','industry','occupation','annualSalary'];
      $scope.OccupationOtherFormFields = ['fifteenHrsQuestion','industry','occupation','otherOccupation','annualSalary'];
    } else if($scope.QPD.occupationDetails.ownBussinessQues == 'No'){
      $scope.OccupationFormFields = ['fifteenHrsQuestion','industry','occupation','annualSalary'];
      $scope.OccupationOtherFormFields = ['fifteenHrsQuestion','industry','occupation','otherOccupation','annualSalary'];
    }
  };*/

  $scope.getOccupations = function() {
    $scope.QPD.occupationDetails.occupation = "";
    $scope.QPD.occupationDetails.otherOccupation = '';
    $scope.QPD.occupationDetails.industryCode = $scope.QPD.occupationDetails.industryCode == '' ? null : $scope.QPD.occupationDetails.industryCode;
    fetchOccupationSvc.getOccupationList($scope.urlList.occupationUrl, "FIRS", $scope.QPD.occupationDetails.industryCode).then(function(res){
      $scope.OccupationList = res.data;
    }, function(err){
      console.info("Error while fetching occupations " + JSON.stringify(err));
    });
  };

  $scope.syncRadios = function(val) {
    var radios = $('label[radio-sync]');
    var data = $('input[data-sync]');
    data.filter('[data-sync="' + val + '"]').attr('checked','checked');
    if(val == 'Fixed') {
      $scope.deathErrorFlag = false;
      $scope.QPD.deathErrorMsg="";
      $scope.tpdErrorFlag = false;
      $scope.QPD.tpdErrorMsg ="";
      showhide('dollar1','nodollar1');
      showhide('dollar','nodollar');
      $scope.QPD.addnlDeathCoverDetails.deathCoverType = 'DcFixed';
      $scope.QPD.addnlTpdCoverDetails.tpdCoverType = 'TPDFixed';
    } else if(val == 'Unitised') {
      $scope.deathErrorFlag = false;
      $scope.QPD.deathErrorMsg="";
      $scope.tpdErrorFlag = false;
      $scope.QPD.tpdErrorMsg ="";
      showhide('nodollar1','dollar1');
      showhide('nodollar','dollar');
      $scope.QPD.addnlDeathCoverDetails.deathCoverType = 'DcUnitised';
      $scope.QPD.addnlTpdCoverDetails.tpdCoverType = 'TPDUnitised';
    }
    radios.removeClass('active');
    radios.filter('[radio-sync="' + val + '"]').addClass('active');
    $scope.QPD.addnlDeathCoverDetails.deathInputTextValue = null;
    $scope.QPD.addnlTpdCoverDetails.tpdInputTextValue = null;
    $scope.QPD.addnlDeathCoverDetails.deathInputTextValue = val == 'Unitised' ? $scope.deathCoverDetails.units : $scope.deathCoverDetails.amount;
    $scope.QPD.addnlTpdCoverDetails.tpdInputTextValue = val == 'Unitised' ? $scope.tpdCoverDetails.units : $scope.tpdCoverDetails.amount;
    $scope.getCategoryFromDB();
    $scope.validateDeathTpdIpAmounts();
  };

  $scope.navigateToLandingPage = function() {
    ngDialog.openConfirm({
        template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
        plain: true,
        className: 'ngdialog-theme-plain custom-width'
    }).then(function(){
      $location.path("/landing");
    }, function(e){
      if(e=='oncancel'){
        return false;
      }
    });
  };

  $scope.getCategoryFromDB = function(fromSelect) {
    $scope.QPD.occupationDetails.otherOccupation = '';
    if($scope.QPD.occupationDetails.occupation != undefined) {
      if(fromSelect) {
        $scope.QPD.occupationDetails.withinOfficeQue = null;
        $scope.QPD.occupationDetails.tertiaryQue = null;
        $scope.QPD.occupationDetails.hazardousQue = null;
        $scope.QPD.occupationDetails.managementRoleQue = null;                                                     
      }
      var occName = $scope.QPD.occupationDetails.industryCode + ":" + $scope.QPD.occupationDetails.occupation;
      NewOccupationService.getOccupation($scope.urlList.newOccupationUrl, "FIRS", occName).then(function(res){
        if($scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcFixed"){
          $scope.QPD.deathDBCategory = res.data[0].deathfixedcategeory;
        }else if($scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcUnitised"){
          $scope.QPD.deathDBCategory = res.data[0].deathunitcategeory;
        }
        if($scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDFixed"){
          $scope.QPD.tpdDBCategory = res.data[0].tpdfixedcategeory;
        }else if($scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDUnitised"){
          $scope.QPD.tpdDBCategory = res.data[0].tpdunitcategeory;
        }
        $scope.QPD.ipDBCategory = res.data[0].ipfixedcategeory;
        $scope.eligibleForStIp();
        $scope.renderOccupationQuestions();
      }, function(err){
        console.info("Error while getting category from DB " + JSON.stringify(err));
      });
    }
  };

  $scope.eligibleForStIp = function(){
	occ = $scope.QPD.occupationDetails.occupation;
	var occSearch = true;
	 propertyService.getOccList().then(function(response){
		 $scope.decOccList = response.data;
		 angular.forEach($scope.decOccList,function(value,key){
			  if(occSearch){
	    		  if(key.toLowerCase() === occ.toLowerCase()){
	    			  if(value.toLowerCase() == 'ip'){
	    				  $scope.QPD.eligibleFrStIp = true;
		    			  occSearch = false;
	    			  }
	    		  }else{
	    			  $scope.QPD.eligibleFrStIp = false;
			      }
			  }
		 });
    	  if($scope.QPD.eligibleFrStIp && $scope.QPD.addnlIpCoverDetails.benefitPeriod == 'Age 65'){
    		  $scope.eligibleFrStIpFlag = true;
    	  }else{
    		  $scope.eligibleFrStIpFlag = false;
    	  }
	});
  };
  
  $scope.calculateOnChange = function(){
    if(this.formOne.$valid && this.occupationForm.$valid && this.coverCalculatorForm.$valid){
    	 if(!$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag && !$scope.eligibleFrStIpFlag){
    		 $scope.calculate();
    	 }
    }
  };

  $scope.renderOccupationQuestions = function() {
    /*if($scope.QPD.occupationDetails.fifteenHr == 'Yes'){*/
      if($scope.QPD.occupationDetails.occupation){
        var selectedOcc = $scope.OccupationList.filter(function(obj){
          return obj.occupationName == $scope.QPD.occupationDetails.occupation;
        });
        var selectedOccObj = selectedOcc[0];

        if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'false') {
          $scope.showWithinOfficeQuestion = true;
          $scope.showTertiaryQuestion = true;
          /*$scope.showHazardousQuestion = false;*/
          $scope.showOutsideOfficeQuestion = false;
          $scope.OccupationFormFields = ['fifteenHrsQuestion','industry','occupation','withinOfficeQuestion','tertiaryQuestion','annualSalary'];
         /* if($scope.QPD.occupationDetails.ownBussinessQues == 'Yes'){
            $scope.OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessYesQuestion','areyouperCitzQuestion','industry','occupation','withinOfficeQuestion','tertiaryQuestion','annualSalary'];
          } else if($scope.QPD.occupationDetails.ownBussinessQues == 'No'){
            $scope.OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','ownBussinessNoQuestion','areyouperCitzQuestion','industry','occupation','withinOfficeQuestion','tertiaryQuestion','annualSalary'];
          }*/

          
        }else if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
        	$scope.showWithinOfficeQuestion = false;
            $scope.showTertiaryQuestion = false;
            /*$scope.showHazardousQuestion = true;*/
            $scope.showOutsideOfficeQuestion = true;
           
            if($scope.QPD.occupationDetails.managementRoleQue == 'Yes'){
                $scope.showWithinOfficeQuestion = true;
                $scope.showTertiaryQuestion = true;
                OccupationFormFields = ['fifteenHrsQuestion','industry','occupation','withinOfficeQuestion','tertiaryQuestion','annualSalary'];
            }else{
                $scope.showWithinOfficeQuestion = false;
                $scope.showTertiaryQuestion = false;
                OccupationFormFields = ['fifteenHrsQuestion','industry','occupation','annualSalary'];
            }
        }else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
        	 $scope.showWithinOfficeQuestion = false;
        	 $scope.showTertiaryQuestion = false;
        	 $scope.showOutsideOfficeQuestion = true;
        	 OccupationFormFields = ['fifteenHrsQuestion',/*'areyouperCitzQuestion',*/'industry','occupation','outsideOffice','annualSalary'];
        }else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'false'){
        	 $scope.showWithinOfficeQuestion = false;
        	 $scope.showTertiaryQuestion = false;
        	 $scope.showOutsideOfficeQuestion = false;
        	 OccupationFormFields = ['fifteenHrsQuestion',/*'areyouperCitzQuestion'*/,'industry','occupation','annualSalary'];
        	 $scope.QPD.deathOccCategory = $scope.QPD.deathDBCategory;
        	 $scope.QPD.tpdOccCategory = $scope.QPD.tpdDBCategory;
        	 $scope.QPD.ipOccCategory = $scope.QPD.ipDBCategory;
        }
        	 
        	$scope.QPD.deathOccCategory = $scope.QPD.deathDBCategory;
        	$scope.QPD.tpdOccCategory = $scope.QPD.tpdDBCategory;
        	$scope.QPD.ipOccCategory = $scope.QPD.ipDBCategory;
        	
        	/*logic to get the upgrade occupation */
        	if($scope.QPD.occupationDetails.managementRoleQue == 'Yes'){
        		if($scope.QPD.deathOccCategory != '' && $scope.QPD.deathOccCategory.toLowerCase() == 'standard'){
	        	  $scope.QPD.deathOccCategory = 'Low Risk';
	        	}
	        	 
	        	if($scope.QPD.tpdOccCategory != '' && $scope.QPD.tpdOccCategory.toLowerCase() == 'standard'){
	        	  $scope.QPD.tpdOccCategory = 'Low Risk';
	        	}
	        	 
	        	if($scope.QPD.ipOccCategory != '' && $scope.QPD.ipOccCategory.toLowerCase() == 'standard'){
	        	  $scope.QPD.ipOccCategory = 'Low Risk';
	        	}
        	}
        	if($scope.QPD.occupationDetails.withinOfficeQue == 'Yes' && $scope.QPD.occupationDetails.tertiaryQue == 'Yes' && $scope.QPD.occupationDetails.salary && parseFloat($scope.QPD.occupationDetails.salary) >= parseFloat($scope.annualSalForUpgradeVal)){
        	    if($scope.QPD.deathOccCategory != '' && $scope.QPD.deathOccCategory.toLowerCase() == 'low risk'){
        		   $scope.QPD.deathOccCategory = 'Professional';
        	    }
	        	 
	        	if($scope.QPD.tpdOccCategory != '' && $scope.QPD.tpdOccCategory.toLowerCase() == 'low risk'){
	        		$scope.QPD.tpdOccCategory = 'Professional';
	        	}
	        	 
	        	if($scope.QPD.ipOccCategory != '' && $scope.QPD.ipOccCategory.toLowerCase() == 'low risk'){
	        		$scope.QPD.ipOccCategory = 'Professional';
	        	}
        	}
       }
      $scope.validateDeathTpdIpAmounts();
    $scope.customDigest();
    var _this = this;
    $timeout(function(){
      if(_this.formOne.$valid && _this.occupationForm.$valid && _this.coverCalculatorForm.$valid){
        $scope.calculate();
      }
    });
};
  // validate fields "on continue"
  $scope.onFormContinue =  function (form) {
   if(!form.$valid) {
     form.$submitted=true;
     if(form.$name == 'formOne'){
       $scope.toggleOccupation(false);
       $scope.toggleCoverCalc(false);
     } else if(form.$name == 'occupationForm'){
       $scope.toggleCoverCalc(false);
     }
   } else {
    if(form.$name == 'formOne') {
      $scope.toggleOccupation(true);
    } else if(form.$name == 'occupationForm') {
    	if($scope.invalidSalAmount || $scope.salMaxlimit){
    		$scope.toggleCoverCalc(false);
    	 }else{
    	    $scope.toggleCoverCalc(true);
    		}
    } else if(form.$name == 'coverCalculatorForm') {
        if(!$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag && !$scope.eligibleFrStIpFlag){
          $scope.calculate();
        }
      }
    }
  };

  $scope.calculate = function() {
    var ruleModel = {
      "age": $scope.QPD.age,
      "fundCode": "FIRS",
      "gender": $scope.QPD.occupationDetails.gender,
      "deathOccCategory": $scope.QPD.deathOccCategory,
      "tpdOccCategory": $scope.QPD.tpdOccCategory,
      "ipOccCategory": $scope.QPD.ipOccCategory,
      "smoker": false,
      "deathUnits": parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue),
      "deathFixedAmount": parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue),
      "deathFixedCost": null,
      "deathUnitsCost": null,
      "tpdUnits": parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue),
      "tpdFixedAmount": parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue),
      "tpdFixedCost": null,
      "tpdUnitsCost": null,
      "ipUnits": parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue),
      "ipFixedAmount": parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue),
      "ipFixedCost": null,
      "ipUnitsCost": null,
      "premiumFrequency": $scope.QPD.freqCostType,
      "memberType": null,
      "manageType": "CCOVER",
      "deathCoverType": $scope.QPD.addnlDeathCoverDetails.deathCoverType,
      "tpdCoverType": $scope.QPD.addnlTpdCoverDetails.tpdCoverType,
      "ipCoverType": "IpUnitised",
      "ipWaitingPeriod": $scope.QPD.addnlIpCoverDetails.waitingPeriod,
      "ipBenefitPeriod": $scope.QPD.addnlIpCoverDetails.benefitPeriod
    };
    CalculateService.calculate(ruleModel,$scope.urlList.calculateUrl).then(function(res) {
      var premium = res.data;
      $scope.dynamicFlag = true;
      for(var i = 0; i < premium.length; i++){
        if(premium[i].coverType == 'DcFixed'){
          $scope.QPD.addnlDeathCoverDetails.deathFixedAmt = premium[i].coverAmount || 0;
          $scope.QPD.addnlDeathCoverDetails.deathInputTextValue = premium[i].coverAmount || 0;
          $scope.QPD.addnlDeathCoverDetails.deathCoverPremium = premium[i].cost || 0;
        } else if(premium[i].coverType == 'TPDFixed'){
          $scope.QPD.addnlTpdCoverDetails.tpdFixedAmt = premium[i].coverAmount || 0;
          $scope.QPD.addnlTpdCoverDetails.tpdInputTextValue = premium[i].coverAmount || 0;
          $scope.QPD.addnlTpdCoverDetails.tpdCoverPremium = premium[i].cost || 0;
        } else if(premium[i].coverType == 'IpFixed'){
          $scope.QPD.addnlIpCoverDetails.ipFixedAmt = premium[i].coverAmount || 0;
          $scope.QPD.addnlIpCoverDetails.ipInputTextValue = premium[i].coverAmount || 0;
          $scope.QPD.addnlIpCoverDetails.ipCoverPremium = premium[i].cost || 0;
        }
        if(premium[i].coverType == 'DcUnitised'){
          $scope.QPD.addnlDeathCoverDetails.deathFixedAmt = premium[i].coverAmount || 0;
          $scope.QPD.addnlDeathCoverDetails.deathCoverPremium = premium[i].cost || 0;
        } else if(premium[i].coverType == 'TPDUnitised'){
          $scope.QPD.addnlTpdCoverDetails.tpdFixedAmt = premium[i].coverAmount || 0;
          $scope.QPD.addnlTpdCoverDetails.tpdCoverPremium = premium[i].cost || 0;
        } else if(premium[i].coverType == 'IpUnitised'){
          $scope.QPD.addnlIpCoverDetails.ipFixedAmt = premium[i].coverAmount || 0;
          $scope.QPD.addnlIpCoverDetails.ipCoverPremium = premium[i].cost || 0;
        }
      }
      $scope.QPD.totalPremium = parseFloat($scope.QPD.addnlDeathCoverDetails.deathCoverPremium)+ parseFloat($scope.QPD.addnlTpdCoverDetails.tpdCoverPremium)+parseFloat($scope.QPD.addnlIpCoverDetails.ipCoverPremium);
    }, function(err){
      console.info("Something went wrong while calculating..." + JSON.stringify(err));
    });
  };

  $scope.checkBenefitPeriod = function() {

    $scope.tempWaitingPeriod = $scope.waitingPeriodOptions.indexOf($scope.ipCoverDetails.waitingPeriod) > -1 ? $scope.ipCoverDetails.waitingPeriod : '90 Days' || '90 Days';
    $scope.tempBenefitPeriod = $scope.waitingPeriodOptions.indexOf($scope.ipCoverDetails.benefitPeriod) > -1 ? $scope.ipCoverDetails.benefitPeriod : '2 Years' || '2 Years';

    if($scope.tempWaitingPeriod == $scope.QPD.addnlIpCoverDetails.waitingPeriod && $scope.tempBenefitPeriod == $scope.QPD.addnlIpCoverDetails.benefitPeriod){
    	$scope.QPD.ipIncreaseFlag = false;
      $scope.disclaimerFlag = true;
    } else if(($scope.tempBenefitPeriod == '2 Years' && $scope.QPD.addnlIpCoverDetails.benefitPeriod == 'Age 65') ||
        ($scope.tempWaitingPeriod == '90 Days' && $scope.QPD.addnlIpCoverDetails.waitingPeriod == '60 Days') ||
        ($scope.tempWaitingPeriod == '90 Days' && $scope.QPD.addnlIpCoverDetails.waitingPeriod == '30 Days') ||
        ($scope.tempWaitingPeriod == '60 Days' && $scope.QPD.addnlIpCoverDetails.waitingPeriod == '30 Days')){
      $scope.QPD.ipIncreaseFlag = true;
      $scope.disclaimerFlag = true;
      $scope.QPD.auraDisabled = false;
    } else{
      $scope.QPD.ipIncreaseFlag = false;
      $scope.disclaimerFlag = false;
    }
    $scope.eligibleForStIp();
    $scope.validateDeathTpdIpAmounts();
  };

  $scope.checkIpCover = function() {
    //$scope.ipWarningFlag = false;
      if(parseInt($scope.QPD.occupationDetails.salary) <= 1000000){
        $scope.IPAmount  = Math.ceil((0.85 * ($scope.QPD.occupationDetails.salary/12)));
      } else if(parseInt($scope.QPD.occupationDetails.salary) > 1000000){
        $scope.IPAmount = Math.ceil((Math.round(((parseInt($scope.QPD.occupationDetails.salary))/12)*0.6) ));
      }
      $scope.IPAmount = $scope.IPMaxAmount < $scope.IPAmount ? $scope.IPMaxAmount : $scope.IPAmount;
      $scope.IPAmount = parseInt($scope.ipCoverDetails.amount) > $scope.IPAmount ? parseInt($scope.ipCoverDetails.amount) : $scope.IPAmount;
  };

  $scope.validateDeathTpdIpAmounts = function() {
    $scope.QPD.auraDisabled = true;
    $scope.deathErrorFlag = false;
    $scope.tpdErrorFlag = false;
    $scope.ipErrorFlag = false;
    $scope.deathDecOrCancelFlag = false;
    $scope.tpdDecOrCancelFlag = false;
    $scope.ipDecOrCancelFlag = false;
    $scope.ipWarningFlag = false;
    $scope.checkIpCover();
    // error check
    if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) > parseInt($scope.DCMaxAmount)){
      $scope.deathErrorFlag = true;
      $scope.deathErrorMsg="Your total death cover exceeds eligibility limit, maximum allowed for this product is  " + $scope.DCMaxAmount + ". Please re-enter your cover.";
    }
    if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) > parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue)){
      $scope.tpdErrorFlag = true;
      $scope.tpdErrorMsg = "TPD amount should not be greater than your Death amount.Please re-enter.";
    } else if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) > parseInt($scope.TPDMaxAmount)){
      $scope.tpdErrorFlag = true;
      $scope.tpdErrorMsg="Your total TPD cover exceeds eligibility limit, maximum allowed for this product is " + $scope.TPDMaxAmount + ". Please re-enter your cover.";
    }
    if(parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) > parseInt($scope.IPAmount/100) && !$scope.QPD.ipcheckbox) {
      $scope.QPD.addnlIpCoverDetails.ipInputTextValue = Math.ceil($scope.IPAmount/100);
      $scope.ipErrorFlag = false;
      $scope.ipErrorMsg="";
      $scope.ipWarningFlag = true;
    }
    //cover name check
    if(!$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag) {
      // Death cover
      var eXDcAmount = Math.ceil(parseInt($scope.deathCoverDetails.amount)/1000)*1000;
      if(!$scope.disableDeath24Hrs)
	  {
      if($scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcFixed") {
        if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) < eXDcAmount ) {
          $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'Decrease your cover';
          $scope.deathDecOrCancelFlag = true;
        } else if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) > eXDcAmount ) {
          $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'Increase your cover';
          $scope.QPD.auraDisabled = false;
        } else if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) == eXDcAmount ) {
          $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'No change';
        } else if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) == 0 && parseInt($scope.deathCoverDetails.amount) != 0) {
          $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'Cancel your cover';
          $scope.deathDecOrCancelFlag = true;
        }
      } else if($scope.QPD.addnlDeathCoverDetails.deathCoverType == "DcUnitised") {
        if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) < parseInt($scope.deathCoverDetails.units) ) {
          $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'Decrease your cover';
          $scope.deathDecOrCancelFlag = true;
        } else if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) > parseInt($scope.deathCoverDetails.units) ) {
          $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'Increase your cover';
          $scope.QPD.auraDisabled = false;
        } else if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) == parseInt($scope.deathCoverDetails.units) ) {
          $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'No change';
        } else if(parseInt($scope.QPD.addnlDeathCoverDetails.deathInputTextValue) == 0 && parseInt($scope.deathCoverDetails.units != 0)) {
          $scope.QPD.addnlDeathCoverDetails.deathCoverName = 'Cancel your cover';
          $scope.deathDecOrCancelFlag = true;
        }
      }
	  }

      // Tpd cover
      var eXTpdAmount = Math.ceil(parseInt($scope.tpdCoverDetails.amount)/1000)*1000;
      if(!$scope.disableTpd24Hrs)
	  {
      if($scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDFixed") {
        if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) < eXTpdAmount ) {
          $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'Decrease your cover';
          $scope.tpdDecOrCancelFlag = true;
        } else if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) > eXTpdAmount ) {
          $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'Increase your cover';
          $scope.QPD.auraDisabled = false;
        } else if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) == eXTpdAmount ) {
          $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'No change';
        } else if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) == 0 && eXTpdAmount != 0) {
          $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'Cancel your cover';
          $scope.tpdDecOrCancelFlag = true;
        }
      } else if($scope.QPD.addnlTpdCoverDetails.tpdCoverType == "TPDUnitised") {
        if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) < parseInt($scope.tpdCoverDetails.units) ) {
          $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'Decrease your cover';
          $scope.tpdDecOrCancelFlag = true;
        } else if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) > parseInt($scope.tpdCoverDetails.units) ) {
          $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'Increase your cover';
          $scope.QPD.auraDisabled = false;
        } else if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) == parseInt($scope.tpdCoverDetails.units) ) {
          $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'No change';
        } else if(parseInt($scope.QPD.addnlTpdCoverDetails.tpdInputTextValue) == 0 && parseInt($scope.tpdCoverDetails.units != 0)) {
          $scope.QPD.addnlTpdCoverDetails.tpdCoverName = 'Cancel your cover';
          $scope.tpdDecOrCancelFlag = true;
        }
      }
	  }

      // Ip cover
      /*var eXIpAmount = Math.ceil(parseInt($scope.ipCoverDetails.amount)/1000)*1000;*/
      var eXIpAmount = Math.ceil(parseInt($scope.ipCoverDetails.units));
      if(!$scope.disableIP24Hrs)
	  {
      if(parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) < eXIpAmount ) {
        $scope.QPD.addnlIpCoverDetails.ipCoverName = 'Decrease your cover';
        $scope.ipDecOrCancelFlag = true;
      } else if(parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) > eXIpAmount ) {
        $scope.QPD.addnlIpCoverDetails.ipCoverName = 'Increase your cover';
        $scope.QPD.auraDisabled = false;
      } else if(parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) == eXIpAmount ) {
        $scope.QPD.addnlIpCoverDetails.ipCoverName = 'No change';
      } else if(parseInt($scope.QPD.addnlIpCoverDetails.ipInputTextValue) == 0 && eXIpAmount != 0) {
        $scope.QPD.addnlIpCoverDetails.ipCoverName = 'Cancel your cover';
        $scope.ipDecOrCancelFlag = true;
      }
	  }

      if($scope.QPD.ipIncreaseFlag) {
    	$scope.QPD.addnlIpCoverDetails.ipCoverName = 'Increase your cover';
        $scope.QPD.auraDisabled = false;
      }
      // ends here
      $scope.customDigest();
      $timeout(function(){
        $scope.calculateOnChange();
      });
    }
 };

$scope.customDigest = function() {
  if ( $scope.$$phase !== '$apply' && $scope.$$phase !== '$digest' ) {
      $scope.$digest();
  }
}

  $scope.quoteSaveAndExitPopUp = function (hhText) {
   var dialog1 = ngDialog.open({
       template: '<div class="ngdialog-content"><div class="modal-header"><h4 id="myModalLabel" class="modal-title aligncenter">Application saved</h4></div><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Finish &amp; Close Window </button></div></div>',
       className: 'ngdialog-theme-plain custom-width',
       preCloseCallback: function(value) {
        var url = "/landing"
        $location.path( url );
        return true
       },
       plain: true
   });
   dialog1.closePromise.then(function (data) {
     console.info('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
   });
  };

  $scope.saveQuote = function() {
    $scope.QPD.lastSavedOn = 'Quotepage';
    $rootScope.$broadcast('disablepointer');
    var selectedIndustry = $scope.IndustryOptions.filter(function(obj) {
      return $scope.QPD.occupationDetails.industryCode == obj.key;
    });
    $scope.QPD.occupationDetails.industryName = selectedIndustry[0].value;
    $scope.QPD.industryName = selectedIndustry[0].value;
    $scope.QPD.industryCode = selectedIndustry[0].key;
    $scope.QPD.appNum = parseInt(fetchAppNumberSvc.getAppNumber());
    $scope.validateDeathTpdIpAmounts();
    $scope.QPD = angular.extend($scope.QPD, $scope.inputDetails);
    saveEapplyData.reqObj($scope.urlList.saveEapplyUrl, $scope.QPD).then(function(response) {
      console.log(response.data);
      $scope.quoteSaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+$scope.QPD.appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR>');
    });
  };

  $scope.goToAura =function() {
	  if(!($scope.disableDeath24Hrs && $scope.disableTpd24Hrs && $scope.disableIP24Hrs) && !($scope.QPD.isDeathDisabled && $scope.QPD.isTPDDisabled && $scope.QPD.isIPDisabled))
		{
    this.formOne.$submitted = true;
    this.occupationForm.$submitted = true;
    this.coverCalculatorForm.$submitted = true;
    $scope.validateDeathTpdIpAmounts();
    if(this.formOne.$valid && this.occupationForm.$valid && this.coverCalculatorForm.$valid) {
    	
      if(!$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag) {
        if($scope.deathDecOrCancelFlag || $scope.tpdDecOrCancelFlag || $scope.ipDecOrCancelFlag){
            $scope.decOrCancelCovers();
        } else {
          $scope.goTo();
        }
      }else{
        return false;
      }
   
    }
		 }else{
		        return false;
		    }
  };

  $scope.goTo = function() {
    if(!$scope.QPD.auraDisabled) {
      $timeout(function() {
        $scope.continueToNextPage('/aura');
      }, 10);
    } else {
      $timeout(function() {
        $scope.continueToNextPage('/summary');
      }, 10);
    }
  }
  $scope.continueToNextPage = function(path) {
    if(this.formOne.$valid && this.occupationForm.$valid && this.coverCalculatorForm.$valid) {
      var selectedIndustry = $scope.IndustryOptions.filter(function(obj){
        return $scope.QPD.occupationDetails.industryCode == obj.key;
      });
      $scope.QPD.occupationDetails.industryName = selectedIndustry[0].value;
      $scope.QPD.industryName = selectedIndustry[0].value;
      $scope.QPD.industryCode = selectedIndustry[0].key;
      $scope.QPD.appNum = parseInt(fetchAppNumberSvc.getAppNumber());
      appData.setAppData($scope.QPD);
      $location.path(path);
    }
  };

  $scope.generatePDF = function(){
    // var CCOccDetails = PersistenceService.getChangeCoverOccDetails();
    // var deathAddnlInfo = PersistenceService.getDeathAddnlCoverDetails();
    // var tpdAddnlInfo = PersistenceService.getTpdAddnlCoverDetails();
    // var ipAddnlInfo = PersistenceService.getIpAddnlCoverDetails();
    //   var personalInformation = persoanlDetailService.getMemberDetails();
    //   var quoteObj =  PersistenceService.getChangeCoverDetails();
    //   if(quoteObj != null && CCOccDetails != null && deathAddnlInfo != null && tpdAddnlInfo != null && ipAddnlInfo != null && personalInformation[0] != null ){
    //     var info={};
    //     info.addnlDeathCoverDetails = deathAddnlInfo;
    //     info.addnlTpdCoverDetails = tpdAddnlInfo;
    //     info.addnlIpCoverDetails = ipAddnlInfo;
    //     info.occupationDetails = CCOccDetails;
    //   var temp = angular.extend(info,quoteObj);
    //   var printObject = angular.extend(temp, personalInformation[0]);
    //   auraResponseService.setResponse(printObject);
      $rootScope.$broadcast('disablepointer');
    var selectedIndustry = $scope.IndustryOptions.filter(function(obj) {
      return $scope.QPD.occupationDetails.industryCode == obj.key;
    });
    $scope.QPD.occupationDetails.industryName = selectedIndustry[0].value;
    $scope.QPD.industryName = selectedIndustry[0].value;
    $scope.QPD.industryCode = selectedIndustry[0].key;
    $scope.QPD.appNum = parseInt(fetchAppNumberSvc.getAppNumber());
    //$scope.validateDeathTpdIpAmounts();
    $scope.QPD = angular.extend($scope.QPD, $scope.inputDetails);
    auraRespSvc.setResponse($scope.QPD);
      printPageSvc.reqObj($scope.urlList.printQuotePage).then(function(response) {
        appData.setPDFLocation(response.data.clientPDFLocation);
        $scope.downloadPDF();
    }, function(err){
      $rootScope.$broadcast('enablepointer');
      console.info("Something went wrong while generating pdf..." + JSON.stringify(err));
    });
  }
  
  $scope.downloadPDF = function(){
          var pdfLocation =null;
          var filename = null;
          var a = null;
        pdfLocation = appData.getPDFLocation();
        console.log(pdfLocation+"pdfLocation");
        filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
        a = document.createElement("a");
          document.body.appendChild(a);
          DownloadPDFSvc.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
          //DownloadPDFService.download({file_name: pdfLocation}, function(res){
          if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
                 window.navigator.msSaveBlob(res.data.response,filename);
             }else{
              var fileURL = URL.createObjectURL(res.data.response);
              a.href = fileURL;
              a.download = filename;
              a.click();
             }
             $rootScope.$broadcast('enablepointer');
        }, function(err){
          console.log("Error downloading the PDF " + err);
          $rootScope.$broadcast('enablepointer');
        });
      };
      
      // added for Decrease or Cancel cover popup "on continue" after calculate quote
		$scope.showDecreaseOrCancelPopUp = function (val){	
		   if(this.formOne.$valid && this.occupationForm.$valid && this.coverCalculatorForm.$valid){
			   if(!$scope.deathErrorFlag && !$scope.tpdErrorFlag && !$scope.ipErrorFlag && !$scope.eligibleFrStIpFlag){
				   if(val == null || val == "" || val == " "){
			   			hideTips();
			   		}else{
			   		 /*ackCheck = $('#termsLabel').hasClass('active');
			    		if(ackCheck){*/
			    			 $scope.ackFlag = false;
					   		 document.getElementById('mymodalDecCancel').style.display = 'block';
					   		 document.getElementById('mymodalDecCancelFade').style.display = 'block';
					   		 document.getElementById('decCancelMsg_text').innerHTML=val;	
			    		/*}else{
			    			$scope.ackFlag = true;
			    		}*/
			    		
			   		} 
			   }else{
				   return false;
			   }
		   }
	   	//	$scope.saveDataForPersistence();
	   	}
	    $scope.hideTips = function  (){
	   		if(document.getElementById('help_div')){
	   			document.getElementById('help_div').style.display = "none";
	   		}						
	   	}
      
  	$scope.decOrCancelCovers = function(){
  		if($scope.deathDecOrCancelFlag == true && $scope.tpdDecOrCancelFlag == true && $scope.ipDecOrCancelFlag == true){
  			$scope.decCancelCover = "Death, TPD & IP";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else if($scope.deathDecOrCancelFlag == true && $scope.tpdDecOrCancelFlag == true){
  			$scope.decCancelCover = "Death & TPD";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else if($scope.deathDecOrCancelFlag && $scope.ipDecOrCancelFlag){
  			$scope.decCancelCover = "Death & IP";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else if($scope.tpdDecOrCancelFlag == true && $scope.ipDecOrCancelFlag == true){
  			$scope.decCancelCover = "TPD & IP";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else if($scope.deathDecOrCancelFlag == true){
  			$scope.decCancelCover = "Death";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else if($scope.tpdDecOrCancelFlag == true){
  			$scope.decCancelCover = "TPD";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else if($scope.ipDecOrCancelFlag == true){
  			$scope.decCancelCover = "IP";
  			$scope.decCancelMsg="You are requesting to cancel/decrease your " + $scope.decCancelCover+  " cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?";
  		}else{
  			$scope.decCancelMsg="You are requesting to cancel your existing cover. If you decide to apply for cover in the future, you will need to supply general and health information as part of your application.";
  		}					
  		/*$scope.decCancelMsg="You are requesting to cancel your existing cover. If you decide to apply for cover in the future, you will need to supply general and health information as part of your application.";*/
  		$scope.showDecreaseOrCancelPopUp($scope.decCancelMsg);
  	};
      

}]);
