// create the module and name it CareSuperApp
// also include ngRoute for all our routing needs
var VicsuperApp = angular.module('eapplymain', ['ngRoute','ngResource','ngFileUpload','ngDialog', 'sessionTimeOut', 'globalExceptionhandler', 'restAPI', 'appDataModel','ui.bootstrap','eApplyInterceptor','ngSanitize']);
// configure our routes
VicsuperApp.config(function($routeProvider,$httpProvider, IdleProvider, KeepaliveProvider,$templateRequestProvider) {
    $httpProvider.defaults.withCredentials = true;
	$httpProvider.interceptors.push('sessionExpiryRedirection');
	 $httpProvider.interceptors.push('eapplyrouter');
	$templateRequestProvider.httpOptions({_isTemplate: true});
    $routeProvider
	    .when('/', {
        templateUrl : 'static/vicsuper/landingpage.html',
        controller  : 'login',
  			reloadOnSearch: false,
        resolve: {
         urls:function(fetchUrlSvc){
           var path = 'CareSuperUrls.properties';
           return fetchUrlSvc.getUrls(path);
         }
        }
	    })
	    .when('/landing', {
	        templateUrl : 'static/vicsuper/landingpage.html',
	        controller  : 'login',
	        resolve: {
	    	   urls:function(fetchUrlSvc) {
             		var path = 'CareSuperUrls.properties';
	    		   	return fetchUrlSvc.getUrls(path);
	    	   }
	        }
	    })
	    .when('/quote', {
	        templateUrl : 'static/vicsuper/coverDetails_changeCover.html',
	        controller  : 'quote'
	    })
	    .when('/newmemberquote/:mode', {
	      templateUrl : 'static/vicsuper/newMember_changeCover.html',
	       controller  : 'newmemberquote'
	    })
	   .when('/quotecancel/:mode', {
	        templateUrl : 'static/vicsuper/coverDetails_cancelCover.html',
	        controller  : 'quotecancel'
	    })
	    .when('/retrievesavedapplication', {
	    	templateUrl : 'static/vicsuper/retrive_applications.html',
	    	controller  : 'retrievesavedapplication'
	    })
	     .when('/quotetransfer/:mode', {
	    	 templateUrl : 'static/vicsuper/coverDetails_transfer.html',
	        controller  :  'quotetransfer'
	    })
	     .when('/quoteoccchange/:mode', {
	        templateUrl : 'static/vicsuper/coverDetails_updateDetails.html',
	        controller  : 'quoteoccupdate'
	    })

	    /*Special cover*/
	    .when('/quotespecial/:mode', {
	        templateUrl : 'static/vicsuper/coverDetails_specialCover.html',
	        controller  : 'quotespecial'
	    })
	    .when('/auraspecial/:mode', {
	        templateUrl : 'static/vicsuper/aura_special_offer.html',
	        controller  : 'auraspecialoffer'
	    })
	    .when('/specialoffersummary/:mode', {
	        templateUrl : 'static/vicsuper/special_offer_confirmation.html',
	        controller  : 'specialoffersummary'
	    })
	     .when('/specialofferaccept', {
	        templateUrl : 'static/vicsuper/special_decision_accepted.html',
	        controller  : 'specialOfferAcceptCtlr'
	    })
	     .when('/specialofferdecline', {
	        templateUrl : 'static/vicsuper/special_decision_decline.html',
	        controller  : 'specialOfferDeclineCtlr'
	    })
	    /*Special cover*/
	     .when('/lifeevent/:mode', {
	        templateUrl : 'static/vicsuper/coverDetails_LifeEvents.html',
	        controller  : 'lifeevent'
	     })
	     .when('/auralifeevent/:mode', {
	        templateUrl : 'static/vicsuper/aura_lifeEvent.html',
	        controller  : 'auraLifeEvent'
	     })
	     .when('/lifeeventaccept', {
	        templateUrl : 'static/vicsuper/lifeEvents_desicion_accepted.html',
	        controller  : 'lifeEventAcceptController'
	    })
	    .when('/lifeeventdecline', {
	        templateUrl : 'static/vicsuper/lifeEvents_decision_decline.html',
	        controller  : 'lifeEventDeclineController'
	    })
	    .when('/lifeeventunderwriting', {
	        templateUrl : 'static/vicsuper/lifeEvents_decision_ruw.html',
	        controller  : 'lifeEventRUWController'
	    })
	     .when('/auratransfer/:mode', {
	    	 templateUrl : 'static/vicsuper/aura_transferCover.html',
	        controller  : 'auratransfer'
	    })
	    .when('/auraocc/:mode', {
	    	 templateUrl : 'static/vicsuper/aura_updateDetails.html',
	        controller  : 'auraocc'
	    })
	    .when('/auraspecialoffer/:mode', {
	    	 templateUrl : 'static/vicsuper/aura_special_offer.html',
	        controller  : 'auraspecialoffer'
	    })
	    .when('/aura', {
	        templateUrl : 'static/vicsuper/aura.html',
	        controller  : 'aura'
	    })
	    .when('/auraIndex', {
	        templateUrl : 'static/vicsuper/aura_indexation.html',
	        controller  : 'auraIndexation'
	    })
	    .when('/auracancel/:mode', {
	        templateUrl : 'static/vicsuper/aura_cancelCover.html',
	        controller  : 'auracancel'
	    })
	    .when('/summary', {
	        templateUrl : 'static/vicsuper/confirmation.html',
	        controller  : 'summary'
	    })
	    .when('/changeaccept', {
	        templateUrl : 'static/vicsuper/changeCover_decision_accepted.html',
	        controller  : 'changeCoverAcceptController'
	    })
	    .when('/changedecline', {
	        templateUrl : 'static/vicsuper/changeCover_decision_decline.html',
	        controller  : 'changeCoverDeclineController'
	    })
	    .when('/changeunderwriting', {
	        templateUrl : 'static/vicsuper/changeCover_decision_ruw.html',
	        controller  : 'changeCoverRUWController'
	    })
	    .when('/changeaspcltermsacc', {
	        templateUrl : 'static/vicsuper/changeCover_decision_AcceptWithSpclTerms.html',
	        controller  : 'changeCoverACCSpclTermsController'
	    })
	    .when('/changemixedaccept', {
	        templateUrl : 'static/vicsuper/changeCover_decision_MixedAcceptance.html',
	        controller  : 'changeCoverMixedACCController'
	    })
	     .when('/requirements/:clm', {
	        templateUrl : 'claimtracker/events.html',
	        controller  : 'requirements'
	    })
	    .when('/newsummary', {
	        templateUrl : 'static/vicsuper/newMember_confirmation.html',
	        controller  : 'newsummary'
	    })
	    .when('/workRatingSummary/:mode', {
	        templateUrl : 'static/vicsuper/workRating_confirmation.html',
	        controller  : 'workRatingSummary'
	    })

	    .when('/workRatingAccept', {
	        templateUrl : 'static/vicsuper/workRating_decision_accepted.html',
	        controller  : 'workRatingAcceptController'
	    })

	    .when('/workRatingDecline', {
	        templateUrl : 'static/vicsuper/workRating_decision_decline.html',
	        controller  : 'workRatingDeclineController'
	    })

	    .when('/workRatingMaintain', {
	        templateUrl : 'static/vicsuper/workRating_decision_maintained.html',
	        controller  : 'workRatingMaintainController'
	    })

	    .when('/transferSummary/:mode', {
	        templateUrl : 'static/vicsuper/transfer_confirmation.html',
	        controller  : 'transferSummary'
	    })

	    .when('/transferAccept', {
	        templateUrl : 'static/vicsuper/transfer_decision_accepted.html',
	        controller  : 'transferAcceptController'
	    })

	    .when('/transferDecline', {
	        templateUrl : 'static/vicsuper/transfer_decision_decline.html',
	        controller  : 'transferDeclineController'
	    })
	    .when('/newmemberaccept', {
	        templateUrl : 'static/vicsuper/newMember_decision_accepted.html',
	        controller  : 'newMemberAcceptController'
	    })
	    .when('/newmemberdecline', {
	        templateUrl : 'static/vicsuper/newMember_decision_decline.html',
	        controller  : 'newMemberDeclineController'
	    })
	    .when('/cancelConfirmation', {
	        templateUrl : 'static/vicsuper/cancel_confirmation.html',
	        controller  : 'cancelConfirmController'
	    })
	    .when('/cancelDecision', {
	        templateUrl : 'static/vicsuper/cancel_decision.html',
	        controller  : 'cancelController'
	    })
	    .when('/sessionTimeOut', {
	        templateUrl : 'static/vicsuper/timeout.html',
	        controller  : 'timeOutController'
	    })
	    .when('/cancelDecisionRuw', {
	        templateUrl : 'static/vicsuper/cancel_decision_ruw.html',
	        controller  : 'cancelRuwController'
	    })
	    .when('/cancelDecisionDecline', {
	        templateUrl : 'static/vicsuper/cancel_decision_decline.html',
	        controller  : 'cancelDecisionDeclineController'
	    })
	    .when('/lifeeventsummary/:mode', {
	        templateUrl : 'static/vicsuper/confirmation_LifeEvents.html',
	        controller  : 'lifeeventsummary'
	    })
	    .when('/convertMaintain/:mode', {
	        templateUrl : 'static/vicsuper/coverDetails_convertCover.html',
	        controller  : 'quoteConvert'
	    })
	    .when('/convertSummary', {
	        templateUrl : 'static/vicsuper/convert_confirmation.html',
	        controller  : 'convertSummary'
	    })

	     .when('/convertDecision', {
	        templateUrl : 'static/vicsuper/convertMaintain_decision_accepted.html',
	        controller  : 'convertAcceptController'
	    })

});

VicsuperApp.constant('APP_CONSTANTS', function () {
	return {
		'emailFormat': /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
	}
});

VicsuperApp.run(function($rootScope, $window) {
	$rootScope.logout = function() {
      $window.close();
    }
	$rootScope.redirect = function() {
		    
	   		$window.location.href = ssologouturl;
	   								
	    }; 
});
