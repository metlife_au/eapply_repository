package au.com.metlife.pics.webserivce;


import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.config.OutflowConfiguration;

import au.com.metlife.exception.MetlifeException;


public class ELodgementOutflowConfiguration {
	private static final String PACKAGE_NAME = ELodgementOutflowConfiguration.class
			.getPackage().getName();

	private static final String CLASS_NAME = ELodgementOutflowConfiguration.class.getName();
	/**
	 * Fetch the properties of Configuration interms of Parameter for the given
	 * username through OutflowConfiguration Class
	 * 
	 * @param username
	 *            is of type String.
	 * @return param is of type Parameter with the username and Password type
	 *         settings.
	 * @throws MetlifeException
	 */
	public static Parameter getOutflowConfiguration(String username) throws MetlifeException {
		final String METHOD_NAME = "getOutflowConfiguration";
		//LogHelper.methodEntry(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);
		OutflowConfiguration ofc = null;
		
		try{
	    ofc = new OutflowConfiguration();
		ofc.setActionItems("UsernameToken Timestamp");
		ofc.setPasswordType("PasswordText");
		ofc.setUser(username);
		}catch (Exception e) {
			
			//if(LogHelper.isDebugLogOn()){
				e.printStackTrace();	
			//}
			throw new MetlifeException();
		
		}
		
		//LogHelper.methodExit(PACKAGE_NAME, CLASS_NAME, METHOD_NAME);
		return ofc.getProperty();
	}

}

