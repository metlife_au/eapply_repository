package uimap.metlife.NEW;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class NSFSLoginPageObjects extends PageObjectLibrary{
		
	public static final By XMLInput = getObjectIdentifaction("NSFS.Login.XMLInput.id");
	public static final By NextButton = getObjectIdentifaction("NSFS.Login.NextButton.xpath");
	public static final By NewApplication = getObjectIdentifaction("NSFS.Login.NewApplication.xpath");
	
}
