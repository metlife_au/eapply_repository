package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendingSystem")

@XmlRootElement (name="SendingSystem")
public class SendingSystem {
	
	private String VendorProductName;

	public String getVendorProductName() {
		return VendorProductName;
	}

	public void setVendorProductName(String vendorProductName) {
		VendorProductName = vendorProductName;
	}
	
}
