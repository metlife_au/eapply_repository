package au.com.metlife.eapply.bs.model;

import java.io.Serializable;

public class ExpenseInfoIns implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String expenseValue;
	private String expenseName;
	private String expenseFrequency;
	public String getExpenseValue() {
		return expenseValue;
	}
	public void setExpenseValue(String expenseValue) {
		this.expenseValue = expenseValue;
	}
	public String getExpenseName() {
		return expenseName;
	}
	public void setExpenseName(String expenseName) {
		this.expenseName = expenseName;
	}
	public String getExpenseFrequency() {
		return expenseFrequency;
	}
	public void setExpenseFrequency(String expenseFrequency) {
		this.expenseFrequency = expenseFrequency;
	}
	
}
