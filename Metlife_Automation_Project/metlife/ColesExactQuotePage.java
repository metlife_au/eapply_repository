package pages.metlife;

import supportlibraries.ScriptHelper;
import uimap.metlife.ColesExactQuotePageObjects;
import uimap.metlife.ColesHealthBPageObjects;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

public class ColesExactQuotePage extends MasterPage {
	WebDriverUtil driverUtil = null;
	
	public ColesExactQuotePage enterColesAcceptExtraQuote() {
		colesAcceptExactQuote();
		return new ColesExactQuotePage(scriptHelper);
	}
	
	public ColesExactQuotePage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		switchToActiveWindow();
	}

	public void colesAcceptExactQuote(){
		try {
			String tcName = dataTable.getData("MetLife_Data", "TC_ID");
			String strChgFlyBuys = dataTable.getData("MetLife_Data", "personal.changeFlybuys");
			String strChgJoinFlyBuys = dataTable.getData("MetLife_Data", "personl.changeJoinFlybuys");
			String strChgFlyBuysNumber = dataTable.getData("MetLife_Data", "personal.flybuysNumber");
			
			if(isElementPresent(ColesExactQuotePageObjects.chkAcknowledge))
				tryAction(waitForClickableElement(ColesExactQuotePageObjects.chkAcknowledge),"Click","I acknowledge the loading applied to my premium.");
			
			storeSessionThreadData("dyn_loadingPercentage",getAttribute(driver.findElement(ColesExactQuotePageObjects.hidTermLoadingId), "value"));
			storeSessionThreadData("dyn_TpdLoading",getAttribute(driver.findElement(ColesExactQuotePageObjects.hidTpdloadingId), "value"));
			storeSessionThreadData("dyn_exclusion",getAttribute(driver.findElement(ColesExactQuotePageObjects.hidExclusionId), "value"));
			sleep();
			
			if (!strChgFlyBuys.equalsIgnoreCase("none")) {
				if (strChgFlyBuys.equalsIgnoreCase("yes")) {

					tryAction(waitForClickableElement(ColesHealthBPageObjects.radChgFlybuysYes), "Click",
							"Are you a flybuys member? " + strChgFlyBuys);
					tryAction(waitForClickableElement(ColesHealthBPageObjects.txtFlybuysNumber), "click",
							"Please enter your flybuys number ");

					sleep();

					tryAction(waitForClickableElement(ColesHealthBPageObjects.txtFlybuysNumber), "SET",
							"Please enter your flybuys number ", strChgFlyBuysNumber);
					sleep();
					tryAction(waitForClickableElement(ColesHealthBPageObjects.txtFlybuysNumber), "TAB",
							"Please enter your flybuys number ");

				} else {
					tryAction(waitForClickableElement(ColesHealthBPageObjects.radChgFlybuysNo), "Click",
							"Are you a flybuys member? " + strChgFlyBuys);
					tryAction(
							strChgJoinFlyBuys.equalsIgnoreCase("yes")
									? waitForClickableElement(ColesHealthBPageObjects.radChgJoinFlybuysYes)
									: waitForClickableElement(ColesHealthBPageObjects.radChgJoinFlybuysNo),
							"Click", "Would you like to sign up for flybuys membership? " + strChgJoinFlyBuys);
					if (strChgJoinFlyBuys.equalsIgnoreCase("yes"))
						tryAction(waitForClickableElement(ColesHealthBPageObjects.chkboxAgree), "Click",
								"I agree to the flybuys Terms & Conditions and flybuys privacy policy. ");
				}
			}
			
			tryAction(driver.findElement(ColesExactQuotePageObjects.lnkNext),"MoveToElm","Next");
			sleep();
			tryAction(waitForClickableElement(ColesExactQuotePageObjects.lnkNext),"Click", "Next");
		}catch (Exception e) {
			report.updateTestLog("Error",
					"Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
}
