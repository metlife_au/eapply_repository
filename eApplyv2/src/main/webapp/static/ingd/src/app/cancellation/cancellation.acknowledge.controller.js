class CancelAckCtrl {
  constructor(CommonAPI, EapplyData, AppConstants, $state, $timeout, ngDialog) {
    'ngInject';
    this.appName = AppConstants.appName;
    this._$state = $state;
    this._$timeout = $timeout;
    this._CommonAPI = CommonAPI;
    this._EapplyData = EapplyData;
    this._ngDialog = ngDialog;
    this.cancelSubmitData = {};
    this.exDthCover = {};
    this.exTpdCover = {};
    this.exIpCover = {};		
    this.otherReasonError = false;
    this.cancelReasonoptions = {
      "option1": "No longer require insurance",
      "option2": "Affordability constraints",
      "option3": "I have sufficient insurance in place",
      "option4": "I don\u2019t wish to provide details",
      "option5": "Other"
    };
  }

  init() {
    this.urlList = this._CommonAPI.getUrlList();
	this.cancelSubmitData = this._EapplyData.getCancelData();
	
	this.cancelWarning =  (this.cancelSubmitData.coverCancellation.death ? "Death" : "") +
                          (this.cancelSubmitData.coverCancellation.death && this.cancelSubmitData.coverCancellation.tpd && this.cancelSubmitData.coverCancellation.incomeProtection ? " , " : this.cancelSubmitData.coverCancellation.death && this.cancelSubmitData.coverCancellation.tpd && !this.cancelSubmitData.coverCancellation.incomeProtection ? " and " : "") +
                          (this.cancelSubmitData.coverCancellation.tpd ? "TPD" : this.cancelSubmitData.coverCancellation.death && !this.cancelSubmitData.coverCancellation.tpd && this.cancelSubmitData.coverCancellation.incomeProtection ? " and " : "") +
                          (this.cancelSubmitData.coverCancellation.tpd && this.cancelSubmitData.coverCancellation.incomeProtection ? " and " : "") +
                          (this.cancelSubmitData.coverCancellation.incomeProtection ? "Income Protection" : "");
	}
	
	// Helpful hints show/hide using this method
  openHH(hhText) {
    this._ngDialog.open({
      template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title text-center text-uppercase" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row" style="margin:0px -35px;"><div class="col-sm-12"><p class="text-center"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn ingd-submit-button font-weight-normal text-white w-100" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>',
      className: 'ngdialog-theme-plain',
      showClose: false,
      plain: true
    });
	}

  updateCancellationReason(value) {
    this.cancelReason = value;
    if(this.cancelReason != 'option5'){
      this.cancelSubmitData.cancelotherreason ='';
    }
  }

 	previous() {
    this._EapplyData.setCancelData(this.cancelSubmitData);
    this._$state.go('app.cancellation');
	}
	
	confirm() {
	    const _this = this;
	    if(this.cancelAckForm.$valid) {
	      this._ngDialog.openConfirm({
	        template: '<div class="ngdialog-content"><div class="modal-body"><div class="row"><div class="col-12"><p class="text-left">You are requesting to cancel your '+ _this.cancelWarning +'  cover. If you decide to apply for cover in the future, you will need to supply health information as part of your application. Are you sure you want to continue?                </p></div></div><!-- Row ends --></div><div class="ngdialog-buttons text-center"><button type="button" class="btn ingd-submit-button font-weight-normal text-white px-4 m-2" ng-dialog-class="ngdialog-theme-plain"  ng-click="confirm()">Yes</button>&nbsp;&nbsp;<button type="button" class="btn ingd-submit-button font-weight-normal text-white px-4 m-2" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog(\'oncancel\')">No</button></div></div>',
	        className: 'ngdialog-theme-plain',
	        showClose: false,
	        plain: true
	      }).then(function (value) {
	        _this.confirmCancel();
	            return true;
	      }, function (value) {
	          if(value == 'oncancel'){
	              return false;
	          }
	      });
	    }	  
	}
	
	confirmCancel(){		
		
		if(this.cancelSubmitData.coverCancellation.death){
				this.cancelSubmitData.applicant.cover[0].additionalCoverAmount = 0;
				this.cancelSubmitData.applicant.cover[0].cost = 0;
			}else{
				this.cancelSubmitData.applicant.cover[0].additionalCoverAmount = this.cancelSubmitData.applicant.cover[0].existingCoverAmount;
			}
			
			if(this.cancelSubmitData.coverCancellation.tpd){
				this.cancelSubmitData.applicant.cover[1].additionalCoverAmount = 0;
				this.cancelSubmitData.applicant.cover[1].cost = 0;
			}else{
				this.cancelSubmitData.applicant.cover[1].additionalCoverAmount = this.cancelSubmitData.applicant.cover[1].existingCoverAmount;
			}
			
			if(this.cancelSubmitData.coverCancellation.incomeProtection){
				this.cancelSubmitData.applicant.cover[2].additionalCoverAmount = 0;
				this.cancelSubmitData.applicant.cover[2].cost = 0;
				this.cancelSubmitData.applicant.cover[2].additionalIpBenefitPeriod = "";
				this.cancelSubmitData.applicant.cover[2].additionalIpWaitingPeriod = "";
				this.cancelSubmitData.applicant.cover[2].existingIpBenefitPeriod = "";
				this.cancelSubmitData.applicant.cover[2].existingIpWaitingPeriod = "";
			}else{
				this.cancelSubmitData.applicant.cover[2].additionalCoverAmount = this.cancelSubmitData.applicant.cover[2].existingCoverAmount;
			}
			if(this.cancelSubmitData.applicant.cover[2].existingCoverAmount == 0){
				this.cancelSubmitData.applicant.cover[2].additionalCoverAmount = 0;
				this.cancelSubmitData.applicant.cover[2].cost = 0;
				this.cancelSubmitData.applicant.cover[2].additionalIpBenefitPeriod = "";
				this.cancelSubmitData.applicant.cover[2].additionalIpWaitingPeriod = "";
				this.cancelSubmitData.applicant.cover[2].existingIpBenefitPeriod = "";
				this.cancelSubmitData.applicant.cover[2].existingIpWaitingPeriod = "";
				this.cancelSubmitData.applicant.cover[2].totalIpWaitingPeriod = "";
				this.cancelSubmitData.applicant.cover[2].totalIpBenefitPeriod = "";
			}
			this.cancelSubmitData.applicant.cancelReason = this.cancelReasonoptions[this.cancelReason];
			
			if(this.cancelReason == 'option5'){
				this.cancelSubmitData.applicant.cancelOtherReason = this.cancelOtherReason;
			}
			
			this.calculate();
	}
	
	calculate(){
		
		const calculateJSON = {  
				   'age': this.cancelSubmitData.applicant.age,
				   'fundCode': 'INGD',
				   'gender': this.cancelSubmitData.applicant.gender,
				   'deathOccCategory': this.cancelSubmitData.applicant.cover[0].occupationRating,
				   'tpdOccCategory': this.cancelSubmitData.applicant.cover[1].occupationRating,
				   'ipOccCategory': this.cancelSubmitData.applicant.cover[2].occupationRating,
				   'smoker': false,
				   'deathFixedAmount': this.cancelSubmitData.applicant.cover[0].additionalCoverAmount,
				   'tpdFixedAmount': this.cancelSubmitData.applicant.cover[1].additionalCoverAmount,
				   'annualSalary': this.cancelSubmitData.applicant.annualSalary,
				   'ipFixedAmount': this.cancelSubmitData.applicant.cover[2].additionalCoverAmount,
				   'premiumFrequency': this.cancelSubmitData.applicant.cover[0].frequencyCostType,
				   'deathCoverType': this.cancelSubmitData.applicant.cover[0].coverCategory,
				   'tpdCoverType': this.cancelSubmitData.applicant.cover[1].coverCategory,
				   'ipCoverType': this.cancelSubmitData.applicant.cover[2].coverCategory,
				   'manageType': 'CANCOVER',
				   'ipBenefitPeriod': this.cancelSubmitData.applicant.cover[2].additionalIpBenefitPeriod,
				   'ipWaitingPeriod': this.cancelSubmitData.applicant.cover[2].additionalIpWaitingPeriod,
				   'stateCode': this.cancelSubmitData.applicant.contactDetails.state,
				   'deathExistingAmount': this.cancelSubmitData.applicant.cover[0].existingCoverAmount,			   
				   'tpdExistingAmount': this.cancelSubmitData.applicant.cover[1].existingCoverAmount,
				   'ipExistingAmount': this.cancelSubmitData.applicant.cover[2].existingCoverAmount,	
				}; 
		
		 console.log(JSON.stringify(calculateJSON));
		
		this._CommonAPI.calculateINGAll(this.urlList.calculateINGDAll, calculateJSON).then(
	    	      (res) => {
	    	    	  
	    	    	  var premium = res;	    	    	  
	    	    	  
	    	    	  for(var i = 0; i < premium.length; i++){
	    				   if(premium[i].coverType === 'DcTailored' || premium[i].coverType === 'DcAutomatic' && !this.cancelSubmitData.coverCancellation.death){
	    					   this.cancelSubmitData.applicant.cover[0].cost = premium[i].cost || 0;
	    				   }else if(premium[i].coverType === 'TPDTailored' || premium[i].coverType === 'TPDAutomatic' && !this.cancelSubmitData.coverCancellation.tpd){
	    					   this.cancelSubmitData.applicant.cover[1].cost = premium[i].cost || 0;
	    				   }else if(premium[i].coverType === 'IpTailored' && !this.cancelSubmitData.coverCancellation.incomeProtection){
	    					   this.cancelSubmitData.applicant.cover[2].cost = premium[i].cost || 0;
	    				   }
	    				}
	    	    	  
	    	    	  if(!this._CommonAPI.getAppNumber()){
	    	    		  this._CommonAPI.generateAppNumber(this.urlList.appNumUrl).then(
	    	    				  (res) => {
	    	    					  this.cancelSubmitData.applicatioNumber = this._CommonAPI.getAppNumber();
	    	    			  },
	    	    			  (err) => {
	    	    	              this.errors = err.data.errors;
	    	    	            }
	    	    		  );
	    	    	  }else{
	    	    		  this.cancelSubmitData.applicatioNumber = this._CommonAPI.getAppNumber();
	    	    	  }
	    	    	  
	    	    	  this.cancelSubmitData.totalMonthlyPremium = parseFloat(this.cancelSubmitData.applicant.cover[0].cost) + parseFloat(this.cancelSubmitData.applicant.cover[1].cost) + parseFloat(this.cancelSubmitData.applicant.cover[2].cost);
	    	    	  this.submit();
	    	      },
	    	      (err) => {
	    	        this.errors = err.data.errors;
	    	      }
	    	    );
		
	}
  
  submit() {
	  
	  console.log(JSON.stringify(this.cancelSubmitData));
	  
	  this._CommonAPI.submitEapply(this.urlList.submitEapplyUrlNew, this.cancelSubmitData).then(
              (res) => {
            	  this._EapplyData.setPdfLocation(res.clientPDFLocation);
            	  if(res.npsTokenUR){
            		  this._EapplyData.setNpsUrl(res.npsTokenURL);
            	  }	            	  
            	  this._$state.go('app.cancelDecision');
              }, 
              (err) => {
                console.log('Error while submitting transfer cover ' + err);
              }
       );
	  
  }
	
	close() {
    this._EapplyData.setCancelData(null);
    this._$state.go('app.landing');
  }
}

export default CancelAckCtrl;