package au.com.metlife.eapply.bs.bizflowmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PhoneRoleList")

@XmlRootElement
public class PhoneRoleList {	
	
	private PhoneRole PhoneRole;

	public PhoneRole getPhoneRole() {
		return PhoneRole;
	}

	public void setPhoneRole(PhoneRole phoneRole) {
		PhoneRole = phoneRole;
	}



	
	
	
}
