(function (angular) {
  'use strict';
  angular.module('mtaa.ui.constants', []).constant('mtaaUIConstants', (function () {
    var basePath = 'static/mtaa/src/';
    return {
      'path': {
        'base': basePath,
        'modules': basePath + 'app/modules/',
        'uiComponents': basePath + 'app/ui-components/',
        'images': basePath + 'app/assets/images/'
      },
      'navUrls': {
        'aboutUsUrl': 'http://mtaasuper.com.au/about-us',
        'contactUsUrl': 'http://www.mtaasuper.com.au/contact-us',
       },
      'onboardDetails': {
        'phoneNumber': /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2|4|3|7|8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/,
        'emailFormat': /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        'waitingPeriodOptions': ['30 Days','60 Days','90 Days'],
        'benefitPeriodOptions': ['2 Years', '5 Years', 'Age 65'],
        'titleOption':[{
    	    text: 'Mr',
    	    value: 'Mr'
    	  },{
    	    text: 'Mrs',
    	    value: 'Mrs'
    	  },{
    	    text: 'Ms',
    	    value: 'Ms'
    	  },{
    	    text: 'Dr',
    	    value: 'Dr'
    	  },{
    		 text: 'Miss',
        	 value: 'Miss'
    	  },{
	   		 text: 'Sir',
	    	 value: 'Sir'
    	  },{
			 text: 'Madam',
	    	 value: 'Madam'
    	  }
    	  ],
        'contactTypeOption':[{
      	    text: 'Home',
      	    value: '2'
      	  },{
      	    text: 'Work',
      	    value: '3'
      	  },{
      	    text: 'Mobile',
      	    value: '1'
        }],
  	  'addressTypeOption':[{
    	    text: 'Home',
    	    value: '2'
    	  },{
    	    text: 'Work',
    	    value: '3'
    	  }]
      }
    };
  })());
})(angular);