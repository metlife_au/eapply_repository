package pages.metlife.ECLAIMS;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Settings;
import com.cognizant.framework.Status;



import supportlibraries.ScriptHelper;
public class Claims_Angular_Login  extends MasterPage {

	/**
	 * @param scriptHelper
	 */
	public Claims_Angular_Login(ScriptHelper scriptHelper) {
		super(scriptHelper);
		// TODO Auto-generated constructor stub
	}
	
	public Claims_Angular_Login Eclaims_Login() {
		enterlogindetails();
		return new Claims_Angular_Login(scriptHelper);
	} 
	
	public void enterlogindetails() {
		
		
		try{		
			
			
			//******Input the URL String******\\
			
	//		WebDriverWait wait=new WebDriverWait(driver,18);
			
			String strApplicationURL = dataTable.getData("Eclaims_Login", "URL");
			// driver.get(properties.getProperty("ApplicationUrl"));

			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			
			sleep(500);
			properties = Settings.getInstance();
			String StrBrowseName=properties.getProperty("currentBrowser");
			
		  		if (StrBrowseName.equalsIgnoreCase("InternetExplorer")){
		  			System.out.println(StrBrowseName);
		  			driver.get(strApplicationURL);
		  	//		Dimension d = new Dimension(1410,1150);
		  			Dimension d = new Dimension(1500,1200);
		  			//Resize the current window to the given dimension
		  			driver.manage().window().setSize(d);	
		  		}
		  		else if (StrBrowseName.equalsIgnoreCase("Chrome")){
		  			System.out.println(StrBrowseName);
		  			driver.get(strApplicationURL);
		  		}
		  		else if (StrBrowseName.equalsIgnoreCase("Firefox")){
		  			System.out.println(StrBrowseName);
		  			driver.get(strApplicationURL);
		  		}
		
			report.updateTestLog("Invoke eclaims Application",
					"Invoked the application under test", Status.DONE);
			
		}catch(Exception e){
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}
	}
	
	
}