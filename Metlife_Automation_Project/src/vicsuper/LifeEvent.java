/*package vicsuper;

public class LifeEvent {

}*/
package vicsuper;

import java.math.BigDecimal;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import supportlibraries.ScriptHelper;
import vicsSuper_Objects.LandingPageObject;
import vicsSuper_Objects.LifeEventObject;

public class LifeEvent extends MasterPage {
	WebDriverWait wait = new WebDriverWait(driver, 20);
	WebDriverUtil driverUtil = null;
	//JavascriptExecutor js = (JavascriptExecutor) driver.getWebDriver();
	JavascriptExecutor js = (JavascriptExecutor) driver;
	public LifeEvent(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
	}
	String XML;
	String SenarioType;
	String DataSheet;
	String occRating1;
	String occRating2;
	String occRating3;
	String BenefitPeriod;


	public LifeEvent LifeEvent_vicsuper(String DataSheetName, String Type) {

		SenarioType = Type;
		XML = dataTable.getData("VicSuper_Login", "XML");
		DataSheet = DataSheetName;
		if (SenarioType.equalsIgnoreCase("Overall")) {
		} else if (SenarioType.equalsIgnoreCase("PremiumCalculation")) {
			PremiumRateInputValidation();
		} else if (SenarioType.equalsIgnoreCase("OccupationRating")) {
			occupationRatingInputValidation();
		} else {
			report.updateTestLog("Error Type", "Invalid Type value-" + SenarioType, Status.FAIL);
		}
		PersonalDetails();
		return new LifeEvent(scriptHelper);

	}

	public void PersonalDetails() {
sleep(3000);
String DutyOfDisclosure = null;
String PrivacyStatement = null;
if (SenarioType.equalsIgnoreCase("Overall")) {
DutyOfDisclosure = dataTable.getData(DataSheet, "DutyOfDisclosure");
PrivacyStatement = dataTable.getData(DataSheet, "PrivacyStatement");
}
		String EmailId = dataTable.getData(DataSheet, "EmailId");
		String TypeofContact = dataTable.getData(DataSheet, "TypeofContact");
		String ContactNumber = dataTable.getData(DataSheet, "ContactNumber");
		String TimeofContact = dataTable.getData(DataSheet, "TimeofContact");
		String Gender = dataTable.getData(DataSheet, "Gender");
		String FourteenHoursWork = dataTable.getData(DataSheet, "FourteenHoursWork");
		String Citizen = dataTable.getData(DataSheet, "Citizen");
		String EducationalInstitution = dataTable.getData(DataSheet, "EducationalInstitution");
		String EducationalDuties = dataTable.getData(DataSheet, "EducationalDuties");
		String TertiaryQual = dataTable.getData(DataSheet, "TertiaryQual");
		String AnnualSalary = dataTable.getData(DataSheet, "AnnualSalary");
		String LifeEvent = dataTable.getData(DataSheet, "LifeEvent");
		String LifeEvent_Date = dataTable.getData(DataSheet, "LifeEvent_Date");
		String LifeEvent_PrevApplyin1Year = dataTable.getData(DataSheet, "LifeEvent_PrevApplyin1Year");
		String AttachPath = dataTable.getData(DataSheet, "AttachPath");


		try {

			sleep(500);

			wait.until(ExpectedConditions.elementToBeClickable(LandingPageObject.lifeEvent)).click();
			sleep(4000);
			// Popup Validation
			/*
			 * if (driver.findElement(By.xpath("//button[text()='Continue']")).
			 * isDisplayed() == true) {
			 * wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
			 * "//button[text()='Continue']"))).click(); sleep(5000); }
			 */
			sleep(5000);

			// -----------------------------------------------------PersonalDetails------------------------------------------------------------------------//
			// *****Agree to Duty of disclosure*******\\
			if (SenarioType.equalsIgnoreCase("Overall")) {
				wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_dutyOfDisclosure));


				String[] ArrayHeaderDD;
				int flag = 1;

				String part2;
				String[] value;
				String HeaderDD = null;

				String[] parts = DutyOfDisclosure.split("---");
				String DDTxt = driver.findElement(By.xpath(".//*[@name='formduty']//p[2]/strong")).getText();
				 if (DDTxt.equalsIgnoreCase(parts[0])) {
					}
					else {
						flag =0;
						report.updateTestLog("Duty Of Disclosure", "<BR><B>Expected Value: </B>"+parts[0]+"<BR><B>Actual Value: </B>"+DDTxt, Status.FAIL);
					}

				parts = parts[1].split("\\n\\n");

				int j=3;
				for (int a = 0; a < 9; a++) {

						if ( !(a==3) ) {
							HeaderDD = driver.findElement(By.xpath("(.//*[@name='formduty']//p)["+j+"]")).getText();
							j++;

							 if (HeaderDD.equalsIgnoreCase(parts[a])) {
								}
								else {
									flag =0;
									report.updateTestLog("Duty Of Disclosure", "<BR><B>Expected Value: </B>"+parts[a]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
								}


						}
					else {
						String[] parts1 = parts[a].split("\\n");
						int k=1;
						for (int b = 0; b < 5; b++) {

							System.out.println("K="+k);
							if ( !(b==4) ) {
								 HeaderDD = driver.findElement(By.xpath(".//*[@name='formduty']//ul/li["+k+"]")).getText();


							}
							else {
								 HeaderDD = driver.findElement(By.xpath("(.//*[@name='formduty']//strong)[2]")).getText();

							}
							 if (HeaderDD.equalsIgnoreCase(parts1[b])) {

							 }
								else {
									flag =0;
									report.updateTestLog("Duty Of Disclosure", "<BR><B>Expected Value: </B>"+parts1[b]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
								}

							k++;

						}

					}


				}



						if (flag==0) {
							report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure - Statement is not as expected", Status.FAIL);
						}
						else {
							report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure - Statement is present as expected", Status.PASS);

						}

				}
			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_dutyOfDisclosure))
					.click();
			sleep(250);
			report.updateTestLog("Duty Of Disclosure", "Duty Of Disclosure checkbox is Selected", Status.PASS);

			// *****Agree to Privacy Statement*******\\

			if (SenarioType.equalsIgnoreCase("Overall")) {
				wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_privacyStatement));

				String[] ArrayHeaderDD;
				int flag = 1;

				String part2;
				String[] value;
				String HeaderDD = null;


				String[] parts = PrivacyStatement.split("---");
				String DDTxt = driver.findElement(By.xpath(".//*[@name='lifeEventPrivacyPolicyForm']//p[2]/strong")).getText();
				 if (DDTxt.equalsIgnoreCase(parts[0])) {

					}
					else {
						flag =0;
						report.updateTestLog("Privacy statement", "<BR><B>Expected Value: </B>"+parts[0]+"<BR><B>Actual Value: </B>"+DDTxt, Status.FAIL);
					}

				parts = parts[1].split("\\n\\n\\n");
				int j=3;
					for (int a = 0; a < 2; a++) {

				HeaderDD = driver.findElement(By.xpath("(.//*[@name='lifeEventPrivacyPolicyForm']//p)["+j+"]")).getText();

				 if (HeaderDD.equalsIgnoreCase(parts[a])) {

					}
					else {
						flag =0;
						report.updateTestLog("Privacy statement", "<BR><B>Expected Value: </B>"+parts[a]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
					}
				j++;
				}

					if (flag==0) {
						report.updateTestLog("Privacy statement", "Privacy statement - Statement is not as expected", Status.FAIL);

					}
					else {
						report.updateTestLog("Privacy statement", "Privacy statement - Statement is present as expected", Status.PASS);
					}

				}
			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_privacyStatement))
					.click();
			sleep(250);
			report.updateTestLog("Privacy Statement", "Privacy Statement checkbox is Selected", Status.PASS);

			// -----------------------------------------------------ContactDetails------------------------------------------------------------------------//

			// *****Enter the Email Id*****\\

			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_email)).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_email))
					.sendKeys(EmailId);
			report.updateTestLog("Email Id", "Email Id: " + EmailId + " is entered", Status.PASS);
			sleep(300);

			// *****Click on the Contact Number Type****\\

			Select Contact = new Select(driver.findElement(LifeEventObject.obj_PersonalDetails_contactType));
			Contact.selectByVisibleText(TypeofContact);
			report.updateTestLog("Contact Type", "Preferred Contact Type: " + TypeofContact + " is Selected",
					Status.PASS);
			sleep(250);

			// ****Enter the Contact Number****\\

			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_phNO))
					.sendKeys(Keys.chord(Keys.CONTROL, "a"));
			sleep(500);
			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_phNO))
					.sendKeys(ContactNumber);
			sleep(250);
			report.updateTestLog("Contact Number", "Contact Number: " + ContactNumber + " is Entered", Status.PASS);

			// ***Select the Preferred time of Contact*****\\

			if (TimeofContact.equalsIgnoreCase("Morning")) {
				wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_Morning))
						.click();
				sleep(250);
				report.updateTestLog("Time of Contact", TimeofContact + " is preferred time of Contact", Status.PASS);
			} else if (TimeofContact.equalsIgnoreCase("Afternoon")) {
				wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_Afternoon))
						.click();
				sleep(500);
				report.updateTestLog("Time of Contact", TimeofContact + " is preferred time of Contact", Status.PASS);

			} else {
				report.updateTestLog("DataTable", "Invalid value for TimeofContact", Status.FAIL);
			}

			/*
			 * //***********Select the Gender******************\\
			 *
			 * if (Gender.equalsIgnoreCase("Male")) {
			 * wait.until(ExpectedConditions.elementToBeClickable(
			 * LifeEventObject.obj_PersonalDetails_Morning)).click();
			 * sleep(250); report.updateTestLog("Time of Contact", Gender +
			 * " is selected", Status.PASS); } else if
			 * (Gender.equalsIgnoreCase("Female")) {
			 * wait.until(ExpectedConditions.elementToBeClickable(
			 * LifeEventObject.obj_PersonalDetails_Afternoon)).click();
			 * sleep(500); report.updateTestLog("Time of Contact", Gender +
			 * " is selected", Status.PASS);
			 *
			 * } else { report.updateTestLog("DataTable",
			 * "Invalid value for TimeofContact", Status.FAIL); }
			 */

			// ****Contact Details-Continue Button*****\\

			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_Continue_ContactDetail)).click();
			report.updateTestLog("Continue", "Clicked on Continue button after entering contact details", Status.PASS);
			sleep(1200);

			// -----------------------------------------------------Occupation------------------------------------------------------------------------//

			// *****Select the 14 Hours Question*******\\

			if (FourteenHoursWork.equalsIgnoreCase("Yes")) {
				wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_FourteenHrsYes))
						.click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected Yes", Status.PASS);
			} else if (FourteenHoursWork.equalsIgnoreCase("No")) {
				wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_FourteenHrsNo))
						.click();
				sleep(500);
				report.updateTestLog("Fifteen Hours Question", "Selected No", Status.PASS);

			} else {
				report.updateTestLog("DataTable", "Invalid value for FourteenHoursWork", Status.FAIL);
			}

			// *****Resident of Australia****\\
			if (Citizen.equalsIgnoreCase("Yes")) {
				wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_CitizenYes))
						.click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Citizen of Australia", Status.PASS);
			} else if (Citizen.equalsIgnoreCase("No")) {
				wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_CitizenNo))
						.click();
				sleep(250);
				report.updateTestLog("Citizen or PR", "Not a citizen of Australia", Status.PASS);
			} else {
				report.updateTestLog("DataTable", "Invalid value for Citizen", Status.FAIL);
			}

			// *****Are the duties of your regular occupation limited to
			// either:*****\\

			if (driver.getPageSource().contains("Are the duties of your regular occupation limited to either:")) {

				// ***Select duties which are undertaken within an Office
				// Environment?\\
				if (EducationalInstitution.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_AddQuest1aYes))
							.click();
					report.updateTestLog("Educational institution", "Selected Yes", Status.PASS);
					sleep(1000);
				} else if (EducationalInstitution.equalsIgnoreCase("No")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_AddQuest1aNo))
							.click();
					report.updateTestLog("Educational institution", "Selected No", Status.PASS);
					sleep(1000);
				} else {
					report.updateTestLog("DataTable", "Invalid value for EducationalInstitution", Status.FAIL);
				}

				// *****Educational duties performed within a school or other
				// educational institution******\\
				if (EducationalDuties.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_AddQuest1bYes))
							.click();
					report.updateTestLog("Educational duties", "Selected Yes", Status.PASS);
					sleep(1000);
				} else if (EducationalDuties.equalsIgnoreCase("No")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_AddQuest1bNo))
							.click();
					report.updateTestLog("Educational duties", "Selected No", Status.PASS);
					sleep(1000);
				} else {
					report.updateTestLog("DataTable", "Invalid value for EducationalDuties", Status.FAIL);
				}

			} else {
				report.updateTestLog("Additional Questions",
						"Are the duties of your regular occupation limited to either: is not present", Status.FAIL);

			}

			// Do you hold a tertiary qualification or work in a management role
			if (driver.getPageSource().contains("Do you:")) {

				// *****Do You Hold a Tertiary Qualification******\\
				if (TertiaryQual.equalsIgnoreCase("Yes")) {
					wait.until(
							ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_AddQuest2Yes))
							.click();
					report.updateTestLog("TertiaryQualification / ManagementRole", "Selected Yes", Status.PASS);
					sleep(1000);
				} else if (TertiaryQual.equalsIgnoreCase("No")) {
					wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_AddQuest2No))
							.click();
					report.updateTestLog("TertiaryQualification / ManagementRole", "Selected No", Status.PASS);
					sleep(1000);
				} else {
					report.updateTestLog("DataTable", "Invalid value for TertiaryQual", Status.FAIL);
				}

			} else {
				report.updateTestLog("Additional Questions",
						"Do you hold a tertiary qualification or work in a management role is not present",
						Status.FAIL);
			}

			// *****What is your annual Salary******

			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_AnnualSal)).clear();
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_AnnualSal))
					.sendKeys(AnnualSalary);
			sleep(250);
			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_AnnualSal))
					.sendKeys(Keys.ENTER);
			sleep(1000);
			report.updateTestLog("Annual Salary", "Annual Salary Entered is: " + AnnualSalary, Status.PASS);

			// *****Click on Continue*******\\

			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_Continue_Occupation)).click();
			report.updateTestLog("Continue", "Clicked on Continue after entering occupation details", Status.PASS);
			sleep(1000);

			// -----------------------------------------------------LifeEvent------------------------------------------------------------------------//

			Select Event = new Select(driver.findElement(LifeEventObject.obj_LifeEvent));
			Event.selectByVisibleText(LifeEvent);
			sleep(500);
			report.updateTestLog("Life Event", LifeEvent + " is Selected", Status.PASS);
			if (SenarioType.equalsIgnoreCase("Overall")) {
				String Negative_Scenario = dataTable.getData(DataSheet, "Negative_Scenario");
				String dateerror=dataTable.getData(DataSheet, "dateerror");
				String Lifeeventerror=dataTable.getData(DataSheet, "Lifeeventerror");

				if (!(Negative_Scenario.equalsIgnoreCase("No"))) {

					 if(Negative_Scenario.equalsIgnoreCase("DateError")){

	                 	//*****What is the date of the life event?*******\\

	                 	driver.findElement(By.name("eventDate")).sendKeys(LifeEvent_Date);
							sleep(1000);
							driver.findElement(By.xpath("//label[contains(text(),' What is the date of the life event?')]")).click();
							report.updateTestLog("Date of Life Event", LifeEvent_Date+" is entered", Status.PASS);
							sleep(1000);

							if(driver.getPageSource().contains(dateerror)){

								report.updateTestLog("Life Event Date Error Message", dateerror+" is Displayed", Status.PASS);
							}

							else{
								report.updateTestLog("Life Event Date Error Message", "Error Message is not Displayed", Status.FAIL);
							}
							return;
	                 }

	                 else if(Negative_Scenario.equalsIgnoreCase("PriorEventError")){


	                 	//*****What is the date of the life event?*******\\
	                 	driver.findElement(By.name("eventDate")).sendKeys(LifeEvent_Date);
							sleep(500);
							driver.findElement(By.xpath("//label[contains(text(),' What is the date of the life event?')]")).click();
							report.updateTestLog("Date of Life Event", LifeEvent_Date+" is entered", Status.PASS);
							sleep(800);

	                 	//*****Have you successfully applied for an increase in cover due to a life event within the last 12 months
	     				//within this calendar year?********************\\

	                 	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='eventAlreadyApplied']/parent::*)[1]"))).click();
	 					report.updateTestLog("Prior Request for Increase in Cover this year","Yes is Selected", Status.PASS);
	 					sleep(800);


	 					sleep(500);
	 					if(driver.getPageSource().contains(Lifeeventerror)){
	 						report.updateTestLog("Life Event Error Message",Lifeeventerror+" is Displayed", Status.PASS);
	 					}

	 					else{
								report.updateTestLog("Life Event Error Message", "Error Message is not Displayed", Status.FAIL);
							}
	                 }
					 return;

				}
			}

				// *****What is the date of the life event?*******\\

				driver.findElement(LifeEventObject.obj_DateOfEvent).sendKeys(LifeEvent_Date);
				sleep(500);
				driver.findElement(LifeEventObject.obj_DateOfEventHeader).click();
				report.updateTestLog("Date of Life Event", LifeEvent_Date + " is entered", Status.PASS);
				sleep(800);

				// *****Have you successfully applied for an increase in cover
				// due to a life event within the last 12 months
				// within this calendar year?********************\\

				if (LifeEvent_PrevApplyin1Year.equalsIgnoreCase("Yes")) {
					wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_IncLifeEventYes)).click();
					report.updateTestLog("Prior Request for Increase in Cover this year", "Yes is Selected",
							Status.PASS);
					sleep(800);

				}

				else if (LifeEvent_PrevApplyin1Year.equalsIgnoreCase("No")) {
					wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_IncLifeEventNo)).click();
					report.updateTestLog("Prior Request for Increase in Cover this year", "No is Selected",
							Status.PASS);
					sleep(800);
				} else {
					wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_IncLifeEventUnSure)).click();
					report.updateTestLog("Prior Request for Increase in Cover this year", "Unsure is Selected",
							Status.PASS);
					sleep(800);
				}



			driver.findElement(LifeEventObject.obj_Path).sendKeys(AttachPath);
				//wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_BrowseLifeEvent)).sendKeys(AttachPath);
				sleep(1000);

			/*wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_BrowseLifeEvent)).click();
			sleep(1000);
			StringSelection stringSelection1 = new StringSelection(AttachPath);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection1, null);

			// ****Attach the file******
			sleep(500);
			properties = Settings.getInstance();
			String StrBrowseName = properties.getProperty("currentBrowser");
			// native key strokes for CTRL, V and ENTER keys
			Robot robot = new Robot();

			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			sleep(1000);*/
			// Click Add
			driver.findElement(LifeEventObject.obj_AddLifeEvent).click();
			sleep(3000);

			// WebElement add=driver.findElement(LifeEventObject.obj_Add);
			// ((JavascriptExecutor)
			// driver).executeScript("arguments[0].click();", add);
			sleep(2000);
			report.updateTestLog("Evidence Attachment", "File is successfully attached", Status.PASS);
			sleep(1000);

			// *****Click on Continue*******\\

			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_Continue_LifeEvent)).click();
			report.updateTestLog("Continue", "Clicked on Continue after entering Life event details", Status.PASS);
			sleep(3000);

			// -----------------------------------------------------COVERCALCULATOR------------------------------------------------------------------------//
			if (SenarioType.equalsIgnoreCase("Overall")) {
				cover_Overall();
			} else if (SenarioType.equalsIgnoreCase("PremiumCalculation")) {
				cover_Premium();
			} else if (SenarioType.equalsIgnoreCase("OccupationRating")) {
				cover_OccupationRating();
			}else {
				report.updateTestLog("Invalid Scenario", "Invalid Scenario", Status.PASS);

			}
			/*if (!(Negative_Scenario.equalsIgnoreCase("No"))) {
				return;
			}*/

				if ((SenarioType.equalsIgnoreCase("PremiumCalculation"))) {
					return;
				}else if ((SenarioType.equalsIgnoreCase("Overall"))) {
					String SaveExit_Personal = dataTable.getData(DataSheet, "SaveExit_PersonalDetails");
					if (SaveExit_Personal.equalsIgnoreCase("Yes")) {
						wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_Save))
								.click();
						sleep(3000);
						// action.moveToElement(driver.findElement(LifeEventObject.obj_PersonalDetails_Save)).click().build().perform();

						report.updateTestLog("Save and Exit", "Save and Exit button is selected", Status.PASS);
						saveAndExitPopUP("PersonalDetails");
						validatePersonalDetail();
					} else {// *****Click on Continue*******\\

						wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_Continue)).click();
						sleep(4000);
						report.updateTestLog("PersonalDetails - Continue", "PersonalDetails - Continue button is clicked",
								Status.PASS);
						confirmation();
						}



				}
				else if (SenarioType.equalsIgnoreCase("OccupationRating")) {


					// *****Click on Continue*******\\

					wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_Continue)).click();
					sleep(4000);
					report.updateTestLog("PersonalDetails - Continue", "PersonalDetails - Continue button is clicked",
							Status.PASS);
					confirmation();
				}



		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}

	}

	private void confirmation() {
		sleep(3000);
		System.out.println("confirmation");
		String Confirmation_DetailsValidations = dataTable.getData(DataSheet, "Confirmation_DetailsValidations");
		System.out.println(Confirmation_DetailsValidations);
		if (Confirmation_DetailsValidations.equalsIgnoreCase("Yes")
				|| Confirmation_DetailsValidations.equalsIgnoreCase("Occupation")) {
			detailsValidation();
		}

		if (SenarioType.equalsIgnoreCase("OccupationRating")) {
			return;
		} else if (SenarioType.equalsIgnoreCase("Overall")) {

			String DecisionHeader = dataTable.getData(DataSheet, "DecisionHeader");
			String DecisionMsg = dataTable.getData(DataSheet, "DecisionMsg");
			String GeneralConsent = dataTable.getData(DataSheet, "GeneralConsent");
			try {// ******Agree to general Consent*******\\

				String part2;
				String[] value;
				String HeaderDD = null;
				int flag=1;
						wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_Confirmation_TermsConditionsCheckBox));
						String[] parts = GeneralConsent.split("\\n\\n");



						 int j=1;
						for (int a = 0; a < 4; a++) {

							if ( !(a==3) ) {
								HeaderDD = driver.findElement(By.xpath("(.//*[@id='collapse4']/div/div/div/p)["+j+"]")).getText();
								j++;

								 if (HeaderDD.equalsIgnoreCase(parts[a])) {

									}
									else {
										flag =0;
										report.updateTestLog("General Consent", "<BR><B>Expected Value: </B>"+parts[a]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
									}

							}
							else {
								System.out.println("<><><><><><>");

								String[] parts1 = parts[a].split("\\n");
								System.out.println("1)"+parts1[0]);
								System.out.println("2)"+parts1[1]);
								System.out.println("3)"+parts1[2]);
								System.out.println("4)"+parts1[3]);
								System.out.println("5)"+parts1[4]);
								System.out.println("6)"+parts1[5]);
								System.out.println("7)"+parts1[6]);
								System.out.println("8)"+parts1[7]);
								System.out.println("9)"+parts1[8]);
								System.out.println("10)"+parts1[9]);


								int k=1;
								for (int b = 0; b < 10; b++) {


										 HeaderDD = driver.findElement(By.xpath(".//*[@id='collapse4']/div/div/div/ul/li["+k+"]")).getText();

									 if (HeaderDD.equalsIgnoreCase(parts1[b])) {

									 }
										else {
											flag =0;
											report.updateTestLog("General Consent", "<BR><B>Expected Value: </B>"+parts1[b]+"<BR><B>Actual Value: </B>"+HeaderDD, Status.FAIL);
										}

									k++;

								}

							}
						}

						if (flag==0) {
							report.updateTestLog("General Consent", "General Consent - Statement is not as expected", Status.FAIL);

						}
						else {
							report.updateTestLog("General Consent", "General Consent - Statement is present as expected", Status.PASS);
						}

						sleep(2000);


				wait.until(ExpectedConditions
						.elementToBeClickable(LifeEventObject.obj_Confirmation_TermsConditionsCheckBox)).click();
				report.updateTestLog("General Consent", "selected the Checkbox", Status.PASS);
				sleep(3000);

				if ((SenarioType.equalsIgnoreCase("Overall"))) {
					String SaveExit_Confirmation = dataTable.getData(DataSheet, "SaveExit_Confirmation");
					if (SaveExit_Confirmation.equalsIgnoreCase("Yes")) {
						wait.until(
								ExpectedConditions.elementToBeClickable(LifeEventObject.obj_Confirmation_Save))
								.click();
						sleep(3000);
						report.updateTestLog("Save and Exit- Confirmation", "Save and Exit button is selected",
								Status.PASS);
						saveAndExitPopUP("Confirmation");
						validateChangeCoverConfirm();
						return;
					} else {
						// ****Click on Continue****\\
						sleep(3000);
						wait.until(
								ExpectedConditions.elementToBeClickable(LifeEventObject.obj_Confirmation_Submit))
								.click();
						report.updateTestLog("Submit", "Submit button is Clicked", Status.PASS);
						sleep(5000);



					}
				}
	//		------------------------------------------------------------------------------------------
			// *******Feedback popup******\\

			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_DecisionPopUP_No)).click();
			report.updateTestLog("Feedback", "Selected No on the Feedback popup", Status.PASS);
			sleep(2500);

			if (driver.getPageSource().contains("APPLICATION NUMBER")||driver.getPageSource().contains("Application Number")||driver.getPageSource().contains("Application number")) {

				// *****Fetching the Application Status*****\\

				String Appstatus = driver.findElement(LifeEventObject.obj_DecisionPopUP_Appstatus).getText();
				sleep(500);

				if (Appstatus.equals(DecisionHeader)) {
					report.updateTestLog("Decision Header", "Decision Header - "+Appstatus+" is present", Status.PASS);
				}else {
					report.updateTestLog("Decision Header", "<BR><B>Expected Value: </B>"+DecisionHeader+"<BR><B>Actual Value: </B>"+Appstatus, Status.FAIL);
				}

				String AppMsg = driver.findElement(LifeEventObject.obj_DecisionPopUP_AppMsg).getText();
				sleep(500);

				if (AppMsg.equals(DecisionMsg)) {
					report.updateTestLog("Decision Message", "Decision Message - "+AppMsg+" is present", Status.PASS);
				}else {
					report.updateTestLog("Decision Message", "<BR><B>Expected Value: </B>"+DecisionMsg+"<BR><B>Actual Value: </B>"+AppMsg, Status.FAIL);
				}

				// *****Fetching the Application Number******\\

				String App = driver.findElement(LifeEventObject.obj_Decision_App_No).getText();
				sleep(1000);

				wait.until(
						ExpectedConditions.presenceOfElementLocated(LifeEventObject.obj_Confirmation_Download))
						.click();
				sleep(3000);

				report.updateTestLog("PDF File", "PDF File downloaded", Status.PASS);

				sleep(1000);

				report.updateTestLog("Decision Page", "Application No: " + App, Status.PASS);
				sleep(500);

			}

			else {
				report.updateTestLog("Application Declined", "Application is declined", Status.FAIL);

			}
		//	---------------------------------------------------------------------------------------------------



				} catch (Exception e) {
				report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
			}

		}

	}

	public void detailsValidation() {

		if (SenarioType.equalsIgnoreCase("Overall")) {
			String Gender = dataTable.getData(DataSheet, "Gender");
			String EmailId = dataTable.getData(DataSheet, "EmailId");
			String TypeofContact = dataTable.getData(DataSheet, "TypeofContact");
			String ContactNumber = dataTable.getData(DataSheet, "ContactNumber");
			String TimeofContact = dataTable.getData(DataSheet, "TimeofContact");
			String FourteenHoursWork = dataTable.getData(DataSheet, "FourteenHoursWork");
			String Citizen = dataTable.getData(DataSheet, "Citizen");
			String EducationalInstitution = dataTable.getData(DataSheet, "EducationalInstitution");
			String EducationalDuties = dataTable.getData(DataSheet, "EducationalDuties");
			String TertiaryQual = dataTable.getData(DataSheet, "TertiaryQual");
			String AnnualSalary = dataTable.getData(DataSheet, "AnnualSalary");

			String LifeEvent = dataTable.getData(DataSheet, "LifeEvent");
			String LifeEvent_Date = dataTable.getData(DataSheet, "LifeEvent_Date");
			String LifeEvent_PrevApplyin1Year = dataTable.getData(DataSheet, "LifeEvent_PrevApplyin1Year");

			AnnualSalary = "$" + AnnualSalary;

			String XML = dataTable.getData("VicSuper_Login", "XML");

			String[] part;
			String part2;
			String[] value;

			// To get DOB
			part = XML.split("<dateOfBirth>");
			part2 = part[1];
			value = part2.split("</dateOfBirth>");
			String dob = value[0];

			// Existing Cover Death
			part = XML.split("<amount>");
			String part2Death = part[1];
			String part2Tpd = part[2];
			String part2IP = part[3];

			String[] valueDeath = part2Death.split("</amount>");
			String[] valueTpd = part2Tpd.split("</amount>");
			String[] valueIP = part2IP.split("</amount>");

			String Death = valueDeath[0];
			String TPD = valueTpd[0];
			String IP = valueIP[0];

			double dD = Double.parseDouble(Death);
			int D = (int) dD;
			double dT = Double.parseDouble(TPD);
			int T = (int) dT;
			double dI = Double.parseDouble(IP);
			int I = (int) dI;

			Death = "$" + D;
			TPD = "$" + T;
			IP = "$" + I;
			System.out.println(Death);
			System.out.println(TPD);
			System.out.println(IP);
			// First Name
			WebElement objFName = driver.findElement(LifeEventObject.obj_Confirmation_FirstName);
			fieldValidation("WebElement", objFName,  VicSuper__Login.firstName, "First Name");

			// Last Name
			WebElement objLName = driver.findElement(LifeEventObject.obj_Confirmation_LastName);
			fieldValidation("WebElement", objLName, VicSuper__Login.lastName, "Last Name");

			// Gender
			WebElement ObjGender = driver.findElement(LifeEventObject.obj_Confirmation_Gender);
			fieldValidation("WebElement", ObjGender, Gender, "Genderquestion");

			// Phone Number type
			WebElement objcontactType = driver.findElement(LifeEventObject.obj_Confirmation_ContactType);
			fieldValidation("WebElement", objcontactType, TypeofContact, "contactType");

			// PhoneNumber
			WebElement objContactNo = driver.findElement(LifeEventObject.obj_Confirmation_ContactNo);
			fieldValidation("WebElement", objContactNo, ContactNumber, "ContactNo");

			// Email
			WebElement objcontactEmail = driver.findElement(LifeEventObject.obj_Confirmation_Email);
			fieldValidation("WebElement", objcontactEmail, EmailId, "Email");
			js.executeScript("window.scrollBy(0,500)", "");
			// DOB
			WebElement objDOB = driver.findElement(LifeEventObject.obj_Confirmation_DOB);
			fieldValidation("WebElement", objDOB, dob, "DOB");

			// Morning Afternoon
			WebElement objTime = driver.findElement(LifeEventObject.obj_Confirmation_Time);
			String eletext = objTime.getText();
			System.out.println(eletext);
			System.out.println(TimeofContact);
			if (eletext.contains(TimeofContact)) {
				report.updateTestLog("Time of contact ", "Time of contact " + TimeofContact + " is displayed",
						Status.PASS);

			} else {
				report.updateTestLog("Time of contact ", "Time of contact " + TimeofContact + " is not displayed",
						Status.FAIL);

			}

			// Do you work more than 14 hours per week?

			WebElement ObjWorkHRS = driver.findElement(LifeEventObject.obj_Confirmation_HoursWork);
			fieldValidation("WebElement", ObjWorkHRS, FourteenHoursWork, "FourteenHoursWork");

			// Are you a citizen or permanent resident of Australia? Yes No

			WebElement objCitizen = driver.findElement(LifeEventObject.obj_Confirmation_Citizen);
			fieldValidation("WebElement", objCitizen, Citizen, "Citizen");

			// office Duties are Undertaken within an Office Environment?
			WebElement eduIns = driver.findElement(LifeEventObject.obj_Confirmation_AddQuest1a);
			fieldValidation("WebElement", eduIns, EducationalInstitution,
					"Are the duties of your regular occupation limited to Question1");

			WebElement eduDut = driver.findElement(LifeEventObject.obj_Confirmation_AddQuest1b);
			fieldValidation("WebElement", eduDut, EducationalDuties,
					"Are the duties of your regular occupation limited to Question2");

			// TertiaryQualification / ManagementRole

			WebElement objQual = driver.findElement(LifeEventObject.obj_Confirmation_TertiaryQualification);
			fieldValidation("WebElement", objQual, TertiaryQual, "TertiaryQualification / ManagementRole");

			// Current annual salary?
			WebElement objannualSalCCId = driver.findElement(LifeEventObject.obj_Confirmation_AnnualIncome);
			fieldValidation("Amount Validation", objannualSalCCId, AnnualSalary, "AnnualSalary");


			// Death Existing Amount

			WebElement deathEXTAmount = driver.findElement(LifeEventObject.obj_Confirmation_Death_ExistingCover);
			fieldValidation("Amount Validation", deathEXTAmount, Death, "Death Existing Cover");

			// Death Amount

			/*WebElement deathAmount = driver.findElement(LifeEventObject.obj_Confirmation_Death_NewCover);
			fieldValidation("Amount Validation", deathAmount, DeathAmount, "Death New Cover");*/
			// amountValidator("Death Amount",deathAmount,DeathAmount );

			/*
			 * // Death Weekly Cost WebElement DeathWeeklycost =
			 * driver.findElement(By.
			 * xpath("(//*[text()='Weekly cost'])[1]//following::p"));
			 * fieldValidation("WebElement",DeathWeeklycost,
			 * DeathWeeklyCost,"Death Weekly Cost");
			 */

			// TPD Amount

			// TPD Existing Amount

			WebElement tpdExtAmount = driver.findElement(LifeEventObject.obj_Confirmation_TPD_ExistingCover);
			fieldValidation("Amount Validation", tpdExtAmount, TPD, "TPD Existing Cover");

			// TPD Amount

			/*WebElement tpdAmount = driver.findElement(LifeEventObject.obj_Confirmation_TPD_NewCover);
			fieldValidation("Amount Validation", tpdAmount, TPDAmount, "TPD New Cover");*/
			// amountValidator("Death Amount",deathAmount,DeathAmount );

			// TPD Weekly Cost

			/*
			 * WebElement tpdWeeklycost = driver.findElement(By.
			 * xpath("(//*[text()='Weekly cost'])[2]//following::p"));
			 * fieldValidation("WebElement",tpdWeeklycost,
			 * TPDWeeklyCost,"TPD Weekly Cost");
			 */
//temp
			// IP Amount
/*
			// IP Existing Amount

			WebElement ipAmount = driver.findElement(LifeEventObject.obj_Confirmation_IP_ExistingCover);
			fieldValidation("Amount Validation", ipAmount, IP, "IP Existing Cover");

			// IP Amount

			WebElement ipExtAmount = driver.findElement(LifeEventObject.obj_Confirmation_IP_NewCover);
			fieldValidation("Amount Validation", ipExtAmount, IPAmount, "IP New Cover");
			// amountValidator("Death Amount",deathAmount,DeathAmount );
*/
			// Waiting Period

			WebElement ipWaitingPeriod = driver.findElement(LifeEventObject.obj_Confirmation_IP_WaitingPeriod);
			fieldValidation("WebElement", ipWaitingPeriod, "90 Days", "Waiting Period");
			// amountValidator("Death Amount",deathAmount,DeathAmount );

			// Benefit Period

			WebElement ipBenefitPeriod = driver.findElement(LifeEventObject.obj_Confirmation_IP_BenefitPeriod);
			fieldValidation("WebElement", ipBenefitPeriod, "2 Years", "Benefit Period");
			// amountValidator("Death Amount",deathAmount,DeathAmount );










			//Life Event
			 WebElement objlifeEvent = driver.findElement(LifeEventObject.obj_Confirmation_LifeEvent);
			 js.executeScript("arguments[0].scrollIntoView(true);", objlifeEvent);
			 fieldValidation("WebElement",objlifeEvent,LifeEvent,"Life Event");

			//Date of Event
			 WebElement objDateofEvent = driver.findElement(LifeEventObject.obj_Confirmation_DateofEvent);
			fieldValidation("WebElement",objDateofEvent,LifeEvent_Date,"Date of Event");

			//increase in cover due to a life event within this calendar year
			 WebElement objincCover = driver.findElement(LifeEventObject.obj_Confirmation_IncreaseCover);
			fieldValidation("WebElement",objincCover,LifeEvent_PrevApplyin1Year,"Increase in cover due to a life event within this calendar year");



		}

		else if (SenarioType.equalsIgnoreCase("OccupationRating")) {
			String OutcomeForDeath = dataTable.getData(DataSheet, "OutcomeForDeath");
			String OutcomeForTPD = dataTable.getData(DataSheet, "OutcomeForTPD");
			String OutcomeForIP = dataTable.getData(DataSheet, "OutcomeForIP");

			WebElement OccupationalCategory_Death = driver
					.findElement(LifeEventObject.obj_Confirmation_Death_OccupationalCategory);
			js.executeScript("arguments[0].scrollIntoView(true);", OccupationalCategory_Death);
			fieldValidation("WebElement", OccupationalCategory_Death, OutcomeForDeath, "OccupationalCategory - Death");

			WebElement OccupationalCategory_tpd = driver
					.findElement(LifeEventObject.obj_Confirmation_TPD_OccupationalCategory);
			fieldValidation("WebElement", OccupationalCategory_tpd, OutcomeForTPD, "OccupationalCategory - TPD");

			WebElement OccupationalCategory_ip = driver
					.findElement(LifeEventObject.obj_Confirmation_IP_OccupationalCategory);
			fieldValidation("WebElement", OccupationalCategory_ip, OutcomeForIP, "OccupationalCategory - IP");

		}

	}

	public void occupationRatingInputValidation() {
		System.out.println("starttttttttt-" + DataSheet);
		String ExistingDeath = dataTable.getData(DataSheet, "ExistingDeath");
		String ExistingTPD = dataTable.getData(DataSheet, "ExistingTPD");
		String ExistingIP = dataTable.getData(DataSheet, "ExistingIP");
		System.out.println("enddddddddddddd");
		String[] part;
		String OCC1;
		String OCC2;
		String OCC3;

		String[] value;

		// To get firstName
		part = XML.split("<occRating>");
		OCC1 = part[1];
		OCC2 = part[2];
		OCC3 = part[3];

		value = OCC1.split("</occRating>");
		occRating1 = value[0];
		value = OCC2.split("</occRating>");
		occRating2 = value[0];
		value = OCC3.split("</occRating>");
		occRating3 = value[0];
		if (!(occRating1.equals(ExistingDeath))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input Death. Inupt-"
					+ occRating1 + "/ Output-" + ExistingDeath + ".", Status.FAIL);
		}
		if (!(occRating2.equals(ExistingTPD))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input TPD. Inupt-" + occRating1
					+ "/ Output-" + ExistingTPD + ".", Status.FAIL);
		}
		if (!(occRating3.equals(ExistingIP))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input IP. Inupt-" + occRating1
					+ "/ Output-" + ExistingIP + ".", Status.FAIL);
		}
		System.out.println("///valend");
	}

	public void PremiumRateInputValidation() {

		String DOB = dataTable.getData(DataSheet, "DOB");
		String Rating = dataTable.getData(DataSheet, "Rating");
		String DeathAmount = dataTable.getData(DataSheet, "DeathAmount");
		String TPDAmount = dataTable.getData(DataSheet, "TPDAmount");
		String[] part;
		String OCC1;
		String OCC2;
		String OCC3;
		String Amount1;
		String Amount2;
		String DateOfBirth;
		String[] value;

		String AmtDeath;
		String AmtTPD;

		// To get DOB
		part = XML.split("<dateOfBirth>");
		DateOfBirth = part[1];
		value = DateOfBirth.split("</dateOfBirth>");
		String date = value[0];
		if (!(date.equals(DOB))) {
			report.updateTestLog("DOB",
					"DOB in XML does not match with the input DOB. Inupt-" + DOB + "/ Output-" + date + ".",
					Status.FAIL);
		}
		// To get occupation
		part = XML.split("<occRating>");
		OCC1 = part[1];
		OCC2 = part[2];
		OCC3 = part[3];

		value = OCC1.split("</occRating>");
		occRating1 = value[0];
		value = OCC2.split("</occRating>");
		occRating2 = value[0];
		value = OCC3.split("</occRating>");
		occRating3 = value[0];
		if (!(occRating1.equals(Rating))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input Death. Inupt-"
					+ occRating1 + "/ Output-" + Rating + ".", Status.FAIL);
		}
		if (!(occRating2.equals(Rating))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input TPD. Inupt-" + occRating1
					+ "/ Output-" + Rating + ".", Status.FAIL);
		}
		if (!(occRating3.equals(Rating))) {
			report.updateTestLog("Occrating", "OccRating in XML does not match with the input IP. Inupt-" + occRating1
					+ "/ Output-" + Rating + ".", Status.FAIL);
		}
		// To get unit Value
				part = XML.split("<amount>");
				Amount1 = part[1];
				Amount2 = part[2];

				value = Amount1.split("</amount>");
				AmtDeath = value[0];
				value = Amount2.split("</amount>");
				AmtTPD = value[0];

				if (!(AmtDeath.equals(DeathAmount))) {
					report.updateTestLog("Amount", "Amount in XML does not match with the input Death. Inupt-"
							+ AmtDeath + "/ Output-" + DeathAmount + ".", Status.FAIL);
				}
				if (!(AmtTPD.equals(TPDAmount))) {
					report.updateTestLog("Amount", "Amount in XML does not match with the input TPD. Inupt-" + AmtDeath
							+ "/ Output-" + TPDAmount + ".", Status.FAIL);
				}

	}

	public void cover_Overall() {
		String CostType = dataTable.getData(DataSheet, "CostType");
		String Negative_Scenario=dataTable.getData(DataSheet, "Negative_Scenario");
		String DeathUnits = dataTable.getData(DataSheet, "DeathUnits");
		String TPDUnits = dataTable.getData(DataSheet, "TPDUnits");
		// ******Click on Cost of Insurance as*******\\
		sleep(1000);
		Select COSTTYPE = new Select(driver.findElement(LifeEventObject.obj_PersonalDetails_PremiumFrequency));
		COSTTYPE.selectByVisibleText(CostType);
		sleep(250);
		report.updateTestLog("Cost of Insurance", "Cost frequency selected is: " + CostType, Status.PASS);
		sleep(500);

		// To Select - Extra cover required Death
		if (DeathUnits.equalsIgnoreCase("0")) {
			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_Fixed_DeathUnit0)).click();
			report.updateTestLog("Death Extra Cover", "Death Extra Cover - None is selected", Status.PASS);

		}

		else if (DeathUnits.equalsIgnoreCase("1")) {
			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_Fixed_DeathUnit1)).click();
			report.updateTestLog("Death Extra Cover", "Death Extra Cover - 1 Unit is selected", Status.PASS);

		} else if (DeathUnits.equalsIgnoreCase("2")) {
			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_Fixed_DeathUnit2)).click();
			report.updateTestLog("Death Extra Cover", "Death Extra Cover - 2 Unit is selected", Status.PASS);


		} else {
			report.updateTestLog("Data Table", "Invalid input for DeathUnits in Data Sheet. Please enter 0/1/2",
					Status.FAIL);
		}
		sleep(1000);
		// To Select - Extra cover required TDP
		if (TPDUnits.equalsIgnoreCase("0")) {
			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_Fixed_TPDUnit0)).click(); // Dhipika
			report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - None is selected", Status.PASS);
		}

		else if (TPDUnits.equalsIgnoreCase("1")) {
			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_Fixed_TPDUnit1)).click(); // Dhipika
			report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - 1 Unit is selected", Status.PASS);

		} else if (TPDUnits.equalsIgnoreCase("2")) {
			wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_Fixed_TPDUnit2)).click(); // Dhipika
			report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - 2 Unit is selected", Status.PASS);

		} else {
			report.updateTestLog("Data Table", "Invalid input for TPDUnits in Data Sheet. Please enter 0/1/2",
					Status.FAIL);
		}
		sleep(2500);
		// TPD > Death unit Validation
		Integer DU = Integer.parseInt(DeathUnits);
		Integer TPDU = Integer.parseInt(TPDUnits);
		if (TPDU > DU) {

			if (driver.getPageSource()
					.contains("Your TPD cover amount should not be greater than Death cover amount.")) {
				report.updateTestLog("TPD Validation",
						"Error Message 'Your TPD cover amount should not be greater than Death cover amount.' is displayed",
						Status.PASS);

				/*
				 * // To Select None value for Death
				 * wait.until(ExpectedConditions .elementToBeClickable(By.xpath(
				 * "(//input[@name='requireCover']/parent::*)[6]"))) .click();
				 * report.updateTestLog("Death Extra Cover",
				 * "Death Extra Cover - 2 Unit is selected", Status.PASS);
				 * sleep(1000);
				 */

			} else {
				report.updateTestLog("TPD Validation",
						"Error Message 'Your TPD cover amount should not be greater than Death cover amount.' is not displayed",
						Status.FAIL);
			}

		}


	}

	public void cover_Premium() {


		String Age = dataTable.getData(DataSheet, "Age");
			String AmountType = dataTable.getData(DataSheet, "AmountType");
			String DeathAmount = dataTable.getData(DataSheet, "DeathAmount");
			String DeathCost = dataTable.getData(DataSheet, "DeathCost");
			String TPDAmount = dataTable.getData(DataSheet, "TPDAmount");
			String TPDCost = dataTable.getData(DataSheet, "TPDCost");
			//String DeathTPD = dataTable.getData(DataSheet, "DeathTPD");

			String DeathCostPage;
			String TPDCostPage;
			String DeathTPDCostPage;
			Double Total = 0.00;



			// Select Cost Type
			Select COSTTYPE = new Select(driver.findElement(LifeEventObject.obj_PersonalDetails_PremiumFrequency));

			//DecimalFormat f = new DecimalFormat("##0.00");
			int AgeValue = Integer.parseInt(Age);
			if (AmountType.equalsIgnoreCase("Fixed")) {

				//To select Death and TPD as None
				wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_Fixed_DeathUnit0)).click();
				report.updateTestLog("Death Extra Cover", "Death Extra Cover - None is selected", Status.PASS);
				sleep(1000);

				wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_Fixed_TPDUnit0)).click(); // Dhipika
				report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - None is selected", Status.PASS);
				sleep(2500);


				COSTTYPE.selectByVisibleText("Yearly");
				sleep(3000);
				report.updateTestLog("Cost of Insurance", "Cost frequency-Yearly is selected", Status.PASS);

				WebElement Death = driver.findElement(LifeEventObject.obj_PersonalDetails_DeathCost);
				DeathCostPage = wait
						.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_DeathCost))
						.getText();
				double DeathCst = Double.parseDouble(DeathCost);
				//String DeathCost1 =  String.format("%.02f", DeathCst);

				amountValidation(DeathCostPage, DeathCst, "Death Cost");
				// Total += PageDeathCost;
				if ((AgeValue >= 14) && (AgeValue <= 64)) {
					WebElement TPD = driver.findElement(LifeEventObject.obj_PersonalDetails_TPDCost);
					TPDCostPage = wait
							.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_TPDCost))
							.getText();

					double TPDCst = Double.parseDouble(TPDCost);
					//String TPDCost1 = String.format("%.02f", TPDCst);

					amountValidation(TPDCostPage, TPDCst, "TPD Cost");
					// Total += PageTPDCost;
				}
				else {
					report.updateTestLog("TPD","TPD is not applicable since age is >65 or <14", Status.PASS);
				}
				/*DeathTPDCostPage = wait.until(
						ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_TotalCostYearly))
						.getText();
				double DeathTPDCst = Double.parseDouble(DeathTPD);
				DeathTPD = String.format("%.02f", DeathTPDCst);

				amountValidation(DeathTPDCostPage, DeathTPD, "Total Cost");*/

				COSTTYPE.selectByVisibleText("Monthly");
				sleep(3000);
				report.updateTestLog("Cost of Insurance", "Cost frequency-Monthly is selected", Status.PASS);

				DeathCostPage = wait
						.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_DeathCost))
						.getText();
				js.executeScript("arguments[0].scrollIntoView(true);", Death);

				double DeathCostmonthly = Double.parseDouble(DeathCost);
				DeathCostmonthly = DeathCostmonthly / 12;
				//String DeathCost2 = String.format("%.02f", DeathCostmonthly);

				amountValidation(DeathCostPage, DeathCostmonthly, "Death Cost");
				if ((AgeValue >= 14) && (AgeValue <= 64)) {
					TPDCostPage = wait
							.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_TPDCost))
							.getText();

					double TPDCostmonthly = Double.parseDouble(TPDCost);
					TPDCostmonthly = TPDCostmonthly / 12;
					//String TPDCost2 = String.format("%.02f", TPDCostmonthly);

					amountValidation(TPDCostPage, TPDCostmonthly, "TPD Cost");
				}
				else {
					report.updateTestLog("TPD","TPD is not applicable since age is >65 or <14", Status.PASS);
				}

				COSTTYPE.selectByVisibleText("Weekly");
				sleep(3000);
				report.updateTestLog("Cost of Insurance", "Cost frequency-Weekly is selected", Status.PASS);

				DeathCostPage = wait
						.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_DeathCost))
						.getText();
				js.executeScript("arguments[0].scrollIntoView(true);", Death);

				double DeathCostweekly = Double.parseDouble(DeathCost);
				DeathCostweekly = DeathCostweekly / 52;
				//String DeathCost3 = String.format("%.02f", DeathCostweekly);

				amountValidation(DeathCostPage, DeathCostweekly, "Death Cost");

				if ((AgeValue >= 14) && (AgeValue <= 64)) {
					TPDCostPage = wait
							.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_TPDCost))
							.getText();

					double TPDCostweekly = Double.parseDouble(TPDCost);
					TPDCostweekly = TPDCostweekly / 52;
					//String TPDCost3 = String.format("%.02f", TPDCostweekly);

					amountValidation(TPDCostPage, TPDCostweekly, "TPD Cost");
				}
				else {
					report.updateTestLog("TPD","TPD is not applicable since age is >65 or <14", Status.PASS);
				}

			} else if (AmountType.equalsIgnoreCase("unit")) {

				//To select Death and TPD as None
				wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_Unit_DeathUnit0)).click();
				report.updateTestLog("Death Extra Cover", "Death Extra Cover - None is selected", Status.PASS);
				sleep(1000);

				wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_Unit_TPDUnit0)).click(); // Dhipika
				report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - None is selected", Status.PASS);
				sleep(1000);

				COSTTYPE.selectByVisibleText("Weekly");
				sleep(3000);
				report.updateTestLog("Cost of Insurance", "Cost frequency-Weekly is selected", Status.PASS);

				//Cost - Amount Validation
				WebElement Death = driver.findElement(LifeEventObject.obj_PersonalDetails_DeathCost);
				DeathCostPage = wait
						.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_DeathCost))
						.getText();
				double DeathCst = Double.parseDouble(DeathCost);
				//String DeathCost1 =  String.format("%.02f", DeathCst);

				amountValidation(DeathCostPage, DeathCst, "Death Cost");

				sleep(3000);

				String DeathAmountPage = wait
						.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_DeathAmount))
						.getText();
				double DeathAmt = Double.parseDouble(DeathAmount);
				//String DeathAmt1 =  String.format("%.02f", DeathAmt);
				amountValidation(DeathAmountPage, DeathAmt, "Death Amount");



				// Total += PageDeathCost;
				if ((AgeValue >= 14) && (AgeValue <= 64)) {
					WebElement TPD = driver.findElement(LifeEventObject.obj_PersonalDetails_TPDCost);
					TPDCostPage = wait
							.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_TPDCost))
							.getText();

					double TPDCst = Double.parseDouble(TPDCost);
					//String TPDCost1 = String.format("%.02f", TPDCst);

					amountValidation(TPDCostPage, TPDCst, "TPD Cost");

					String TPDAmountPage = wait
							.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_TPDAmount))
							.getText();
					double TPDAmt = Double.parseDouble(TPDAmount);
					//String TPDAmt1 =  String.format("%.02f", TPDAmt);
					amountValidation(DeathAmountPage, TPDAmt, "TPD Amount");


				}
				else {
					report.updateTestLog("TPD","TPD is not applicable since age is >65 or <14", Status.PASS);
				}
				/*DeathTPDCostPage = wait.until(
						ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_TotalCostWeekly))
						.getText();
				double DeathTPDCst = Double.parseDouble(DeathTPD);
				DeathTPD = String.format("%.02f", DeathTPDCst);

				amountValidation(DeathTPDCostPage, DeathTPD, "Total Cost");*/



				//Monthly


				COSTTYPE.selectByVisibleText("Monthly");
				sleep(3000);
				report.updateTestLog("Cost of Insurance", "Cost frequency-Monthly is selected", Status.PASS);

				DeathCostPage = wait
						.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_DeathCost))
						.getText();
				js.executeScript("arguments[0].scrollIntoView(true);", Death);

				double DeathCostmonthly = Double.parseDouble(DeathCost);
				DeathCostmonthly = DeathCostmonthly * 4.34;
				//String DeathCost2 = String.format("%.02f", DeathCostmonthly);

				amountValidation(DeathCostPage, DeathCostmonthly, "Death Cost");


				DeathAmountPage = wait
						.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_DeathAmount))
						.getText();
				double DeathAmtMonthly = Double.parseDouble(DeathAmount);
				//String DeathAmtMonthly1 =  String.format("%.02f", DeathAmtMonthly);
				amountValidation(DeathAmountPage, DeathAmtMonthly, "Death Amount");



				if ((AgeValue >= 14) && (AgeValue <= 64)) {
					TPDCostPage = wait
							.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_TPDCost))
							.getText();

					double TPDCostmonthly = Double.parseDouble(TPDCost);
					TPDCostmonthly = TPDCostmonthly * 4.34;
					//String TPDCost2 = String.format("%.02f", TPDCostmonthly);

					amountValidation(TPDCostPage, TPDCostmonthly, "TPD Cost");


					String TPDAmountPage = wait
							.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_TPDAmount))
							.getText();
					double TPDAmtMonthly = Double.parseDouble(TPDAmount);
					//String TPDAmtMonthly1 =  String.format("%.02f", TPDAmtMonthly);
					amountValidation(DeathAmountPage, TPDAmtMonthly, "TPD Amount");

				}
				else {
					report.updateTestLog("TPD","TPD is not applicable since age is >65 or <14", Status.PASS);
				}


				//Yearly
				COSTTYPE.selectByVisibleText("Yearly");
				sleep(3000);
				report.updateTestLog("Cost of Insurance", "Cost frequency-Yearly is selected", Status.PASS);

				DeathCostPage = wait
						.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_DeathCost))
						.getText();
				js.executeScript("arguments[0].scrollIntoView(true);", Death);

				double DeathCostYearly = Double.parseDouble(DeathCost);
				DeathCostYearly = DeathCostYearly * 52;
				//String DeathCostYearly1 = String.format("%.02f", DeathCostYearly);

				amountValidation(DeathCostPage, DeathCostYearly, "Death Cost");

				DeathAmountPage = wait
						.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_DeathAmount))
						.getText();
				double DeathAmtYearly = Double.parseDouble(DeathAmount);
				//String DeathAmtYearly1 =  String.format("%.02f", DeathAmtYearly);
				amountValidation(DeathAmountPage, DeathAmtYearly, "Death Amount");


				if ((AgeValue >= 14) && (AgeValue <= 64)) {
					TPDCostPage = wait
							.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_TPDCost))
							.getText();

					double TPDCostYearly = Double.parseDouble(TPDCost);
					TPDCostYearly = TPDCostYearly * 52;
					//String TPDCostYearly1 = String.format("%.02f", TPDCostYearly);

					amountValidation(TPDCostPage, TPDCostYearly, "TPD Cost");


					String TPDAmountPage = wait
							.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_TPDAmount))
							.getText();
					double TPDAmtYearly = Double.parseDouble(TPDAmount);
					//String TPDAmtYearly1 =  String.format("%.02f", TPDAmtYearly);
					amountValidation(DeathAmountPage, TPDAmtYearly, "TPD Amount");

				}
				else {
					report.updateTestLog("TPD","TPD is not applicable since age is >65 or <14", Status.PASS);
				}




			} else {
				report.updateTestLog("Data Sheet", "Invalid Amount Type ", Status.FAIL);

			}
			// Select Cost Type




	}

	public void amountValidation(String DollarAmount, Double newAmount, String Field) {

		String Amount = String.format("%.02f", newAmount);
		System.out.println(DollarAmount);
		System.out.println(Amount);
		String[] part;
		String amount1;
		String[] value;
		String a1;
		String a2;
		String amount;
		Amount = "$" + Amount;
		if (DollarAmount.contains(",")) {

			// To get firstName
			String[] p = DollarAmount.split(",");
			String p1 = p[0];
			String p2 = p[1];
			DollarAmount = p1 + "" + p2;
		}

		if (DollarAmount.equalsIgnoreCase(Amount)) {
			report.updateTestLog(Field, Field + " :" + Amount + " is present", Status.PASS);
		} else {
			Double decAmount = newAmount-0.01;
			Double incAmount = newAmount+0.01;
			String dAmount = String.format("%.02f", decAmount);
			String iAmount = String.format("%.02f", incAmount);


			dAmount = "$" + dAmount;
			iAmount = "$" + iAmount;

			/*if (DollarAmount.contains(",")) {

				// To get firstName
				String[] p = DollarAmount.split(",");
				String p1 = p[0];
				String p2 = p[1];
				DollarAmount = p1 + "" + p2;
			}*/

			if ( (DollarAmount.equalsIgnoreCase(dAmount))||(DollarAmount.equalsIgnoreCase(iAmount)) ) {
				report.updateTestLog(Field, Field + " :" + Amount + " is present with 0.01 corrected value", Status.PASS);
			} else {

				report.updateTestLog(Field,
						Field + " Expected Value:" + Amount + " / Value Present in page:" + DollarAmount, Status.FAIL);
			}

		}

	}

	public void cover_OccupationRating() {

		wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_Fixed_DeathUnit0)).click();
		report.updateTestLog("Death Extra Cover", "Death Extra Cover - None is selected", Status.PASS);
		sleep(1000);

		wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_Fixed_TPDUnit0)).click(); // Dhipika
		report.updateTestLog("TPD Extra Cover", "TPD Extra Cover - None is selected", Status.PASS);
		sleep(3000);
	}

	public void saveAndExitPopUP(String Page) {
		//d
		if (driver.getPageSource().contains("Your application reference no")) {

			WebElement WErefNo = driver.findElement(LifeEventObject.obj_SaveReferenceNo);
			String refNo = WErefNo.getText();

			String[] part = refNo.split("Your application reference no. is ");
			String SE = part[1];

			String[] part1 = SE.split("\\n\\n");
			String ref = part1[0];

			report.updateTestLog("Reference No", "Reference No:" + ref, Status.PASS);
			if (Page.equalsIgnoreCase("PersonalDetails")) {
				driver.findElement(LifeEventObject.obj_SaveClose_personalDetails).click();
			}
			else if (Page.equalsIgnoreCase("Confirmation")) {
				driver.findElement(LifeEventObject.obj_SaveClose_Confirmation).click();
			}
			sleep(2000);
			report.updateTestLog("Finish and close", "Finish and close button is clicked", Status.PASS);
			return;
		} else {
			System.out.println("ref no not present");
			report.updateTestLog("Saved And Exit", "Application Saved pop-up does not appear", Status.FAIL);
		}
	}

	public void premiumValidation() {
		/*
		 * String DeathYN = dataTable.getData(DataSheet, "DeathYN"); String
		 * TPDYN = dataTable.getData(DataSheet, "TPDYN"); String IPYN =
		 * dataTable.getData(DataSheet, "IPYN"); String ExpectedDeathcover =
		 * dataTable.getData("VicSuper_EApplyPremium", "ExpectedDeathcover");
		 * String ExpectedTPDcover = dataTable.getData("VicSuper_EApplyPremium",
		 * "ExpectedTPDcover"); String ExpectedIPcover =
		 * dataTable.getData("VicSuper_EApplyPremium", "ExpectedIPcover");
		 * ExpectedDeathcover = "$" + ExpectedDeathcover; ExpectedTPDcover = "$"
		 * + ExpectedTPDcover; ExpectedIPcover = "$" + ExpectedIPcover;
		 *
		 * // ExpectedDeathcover = amountFormatter(ExpectedDeathcover);
		 * System.out.println("Formatted amount :" + ExpectedDeathcover);
		 *
		 * if (DeathYN.equalsIgnoreCase("Yes")) { WebElement Death =
		 * driver.findElement(LifeEventObject.obj_PersonalDetails_DeathAmount);
		 * fieldValidation("Amount Validation", Death, ExpectedDeathcover,
		 * "Death Cover Premium"); }
		 *
		 * if (TPDYN.equalsIgnoreCase("Yes")) { WebElement tpd =
		 * driver.findElement(LifeEventObject.obj_PersonalDetails_TPDAmount);
		 * fieldValidation("Amount Validation", tpd, ExpectedTPDcover,
		 * "TPD Cover Premium"); }
		 *
		 * if (IPYN.equalsIgnoreCase("Yes")) { WebElement ip =
		 * driver.findElement(LifeEventObject.obj_PersonalDetails_IPAmount);
		 * fieldValidation("Amount Validation", ip, ExpectedIPcover,
		 * "IP Cover Premium"); }
		 *
		 */}

	public void validatePersonalDetail() {



		String EmailId = dataTable.getData(DataSheet, "EmailId");
		String TypeofContact = dataTable.getData(DataSheet, "TypeofContact");
		String ContactNumber = dataTable.getData(DataSheet, "ContactNumber");
		String TimeofContact = dataTable.getData(DataSheet, "TimeofContact");
		String Gender = dataTable.getData(DataSheet, "Gender");
		String FourteenHoursWork = dataTable.getData(DataSheet, "FourteenHoursWork");
		String Citizen = dataTable.getData(DataSheet, "Citizen");
		String EducationalInstitution = dataTable.getData(DataSheet, "EducationalInstitution");
		String EducationalDuties = dataTable.getData(DataSheet, "EducationalDuties");
		String TertiaryQual = dataTable.getData(DataSheet, "TertiaryQual");
		String AnnualSalary = dataTable.getData(DataSheet, "AnnualSalary");
		String LifeEvent = dataTable.getData(DataSheet, "LifeEvent");
		String LifeEvent_Date = dataTable.getData(DataSheet, "LifeEvent_Date");
		String LifeEvent_PrevApplyin1Year = dataTable.getData(DataSheet, "LifeEvent_PrevApplyin1Year");
		String AttachPath = dataTable.getData(DataSheet, "AttachPath");
		String CostType=dataTable.getData(DataSheet, "CostType");
		String DeathUnits=dataTable.getData(DataSheet, "DeathUnits");
		String TPDUnits=dataTable.getData(DataSheet, "TPDUnits");
		String SaveExit_Personal=dataTable.getData(DataSheet, "SaveExit_PersonalDetails");

		sleep(2000);

		// *** Retrieving the saved APP**//
		wait.until(ExpectedConditions.elementToBeClickable(LandingPageObject.savesApplication)).click();
		report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application link is Clicked", Status.PASS);
		sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_OpenSavedApplication))
				.click();
		report.updateTestLog("Retrieve Saved Application", "Application is Retrieved", Status.PASS);
		sleep(4000);
		WebElement objDISCLOSURE1 = driver.findElement(LifeEventObject.obj_PersonalDetails_dutyOfDisclosure);
		fieldValidation("WebCheckBox", objDISCLOSURE1, "Checked", "DISCLOSURE1");
		js.executeScript("arguments[0].scrollIntoView(true);", objDISCLOSURE1);
		// *****Agree to Privacy Statement*******\\
		WebElement objDISCLOSURE2 = driver.findElement(LifeEventObject.obj_PersonalDetails_privacyStatement);
		fieldValidation("WebCheckBox", objDISCLOSURE2, "Checked", "DISCLOSURE2");
		js.executeScript("arguments[0].scrollIntoView(true);", objDISCLOSURE2);
		// Email
		WebElement objcontactEmail = driver.findElement(LifeEventObject.obj_PersonalDetails_email);
		fieldValidation("WebEdit", objcontactEmail, EmailId, "Email");

		// Phone Number type
		WebElement objcontactType = driver.findElement(LifeEventObject.obj_PersonalDetails_contactType);
		fieldValidation("WebList", objcontactType, TypeofContact, "ContactType");

		// PhoneNumber
		WebElement objpreferrednumCCId = driver.findElement(LifeEventObject.obj_PersonalDetails_phNO);
		fieldValidation("WebEdit", objpreferrednumCCId, ContactNumber, "Contact No");
		System.out.println("Ph----------------------------------------------------");

		// Morning Afternoon
		if (TimeofContact.equalsIgnoreCase("Morning")) {
			System.out.println("//Morn");
			WebElement objMorning = driver.findElement(LifeEventObject.obj_PersonalDetails_Morning);
			fieldValidation("WebButton", objMorning, TimeofContact, "Time of contact");
		} else {
			System.out.println("//AN");
			WebElement objAfternoon = driver.findElement(LifeEventObject.obj_PersonalDetails_Afternoon);
			fieldValidation("WebButton", objAfternoon, TimeofContact, "Time of contact");
		}

		// Male or Female

		if (Gender.equalsIgnoreCase("Male")) {
			System.out.println("//Male");
			WebElement ObjGenderMale = driver.findElement(LifeEventObject.obj_PersonalDetails_Male);
			fieldValidation("WebButton", ObjGenderMale, Gender, "Genderquestion");
		} else if (Gender.equalsIgnoreCase("Female")) {
			System.out.println("//Female");
			WebElement ObjSmokerquestionNo = driver.findElement(LifeEventObject.obj_PersonalDetails_Female);
			fieldValidation("WebButton", ObjSmokerquestionNo, Gender, "Genderquestion");
		} else {
			report.updateTestLog("Data table", "Invalid entry for Gender", Status.FAIL);

		}
		js.executeScript("arguments[0].scrollIntoView(true);", objpreferrednumCCId);
		// Do you work more than 14 hours per week?
		if (FourteenHoursWork.equalsIgnoreCase("Yes")) {
			WebElement objfifteenHrsQuestionYes = driver
					.findElement(LifeEventObject.obj_PersonalDetails_FourteenHrsYes);
			fieldValidation("WebButton", objfifteenHrsQuestionYes, FourteenHoursWork, "FourteenHoursWork");
		} else if (FourteenHoursWork.equalsIgnoreCase("No")) {
			WebElement objfifteenHrsQuestionNo = driver
					.findElement(LifeEventObject.obj_PersonalDetails_FourteenHrsNo);
			fieldValidation("WebButton", objfifteenHrsQuestionNo, FourteenHoursWork, "FourteenHoursWork");
		} else {
			report.updateTestLog("Data table", "Invalid entry for FourteenHoursWork", Status.FAIL);

		}

		// Are you a citizen or permanent resident of Australia? Yes No
		if (Citizen.equalsIgnoreCase("Yes")) {
			WebElement objCitizenYes = driver.findElement(LifeEventObject.obj_PersonalDetails_CitizenYes);
			fieldValidation("WebButton", objCitizenYes, Citizen, "Citizen");
		} else if (Citizen.equalsIgnoreCase("No")) {
			WebElement objCitizenNo = driver.findElement(LifeEventObject.obj_PersonalDetails_CitizenNo);
			fieldValidation("WebButton", objCitizenNo, Citizen, "Citizen");
		} else {
			report.updateTestLog("Data table", "Invalid entry for Citizen", Status.FAIL);

		}

		// office Duties are Undertaken within an Office Environment?
		if (EducationalInstitution.equalsIgnoreCase("Yes")) {
			WebElement eduInsYes = driver.findElement(LifeEventObject.obj_PersonalDetails_AddQuest1aYes);
			fieldValidation("WebButton", eduInsYes, EducationalInstitution, "EducationalInstitution");
		} else if (EducationalInstitution.equalsIgnoreCase("No")) {
			WebElement eduInsNo = driver.findElement(LifeEventObject.obj_PersonalDetails_AddQuest1aNo);
			fieldValidation("WebButton", eduInsNo, EducationalInstitution, "EducationalInstitution");
		} else {
			report.updateTestLog("Data table", "Invalid entry for EducationalInstitution", Status.FAIL);

		}

		if (EducationalDuties.equalsIgnoreCase("Yes")) {
			WebElement eduDutYes = driver.findElement(LifeEventObject.obj_PersonalDetails_AddQuest1bYes);
			fieldValidation("WebButton", eduDutYes, EducationalDuties, "EducationalDuties");
			js.executeScript("arguments[0].scrollIntoView(true);", eduDutYes);
		} else if (EducationalDuties.equalsIgnoreCase("No")) {
			WebElement eduDutNo = driver.findElement(LifeEventObject.obj_PersonalDetails_AddQuest1bNo);
			fieldValidation("WebButton", eduDutNo, EducationalDuties, "EducationalDuties");
			js.executeScript("arguments[0].scrollIntoView(true);", eduDutNo);
		} else {
			report.updateTestLog("Data table", "Invalid entry for EducationalDuties", Status.FAIL);

		}

		// TertiaryQualification / ManagementRole

		if (TertiaryQual.equalsIgnoreCase("Yes")) {
			WebElement objofficeEnvYes = driver.findElement(LifeEventObject.obj_PersonalDetails_AddQuest2Yes);
			fieldValidation("WebButton", objofficeEnvYes, TertiaryQual, "TertiaryQualification / ManagementRole");
		} else if (TertiaryQual.equalsIgnoreCase("No")) {
			WebElement objofficeEnvNo = driver.findElement(LifeEventObject.obj_PersonalDetails_AddQuest2No);
			fieldValidation("WebButton", objofficeEnvNo, TertiaryQual, "TertiaryQualification / ManagementRole");
		} else {
			report.updateTestLog("Data table", "Invalid entry for TertiaryQual", Status.FAIL);

		}

		// Current annual salary?
		WebElement objannualSalCCId = driver.findElement(LifeEventObject.obj_PersonalDetails_AnnualSal);
		fieldValidation("WebEdit", objannualSalCCId, AnnualSalary, "AnnualSalary");
		js.executeScript("arguments[0].scrollIntoView(true);", objannualSalCCId);

		//Life Event
		 WebElement objLifeEvent = driver.findElement(LifeEventObject.obj_LifeEvent);
		 fieldValidation("WebList",objLifeEvent,LifeEvent,"Life Event");

	//Date Of Life Event
		 WebElement objDateLifeEvent = driver.findElement(LifeEventObject.obj_DateOfEvent);
		 fieldValidation("WebEdit",objDateLifeEvent,LifeEvent_Date,"Date Of Life Event");

	//Increase in cover

			if(LifeEvent_PrevApplyin1Year.equalsIgnoreCase("Yes")){
				WebElement objpriorappYes = driver.findElement(LifeEventObject.obj_IncLifeEventYes);
				 fieldValidation("WebButton",objpriorappYes,LifeEvent_PrevApplyin1Year,"Increase in cover");
			}
			else if(LifeEvent_PrevApplyin1Year.equalsIgnoreCase("No")){
				WebElement objpriorappNo = driver.findElement(LifeEventObject.obj_IncLifeEventNo);
				 fieldValidation("WebButton",objpriorappNo,LifeEvent_PrevApplyin1Year,"Increase in cover");
			}
			else if(LifeEvent_PrevApplyin1Year.equalsIgnoreCase("Unsure")){
				WebElement objpriorappUnsure = driver.findElement(LifeEventObject.obj_IncLifeEventUnSure);
				 fieldValidation("WebButton",objpriorappUnsure,LifeEvent_PrevApplyin1Year,"Increase in cover");
			}
			else {
				 report.updateTestLog("Data table", "Invalid entry for TertiaryQual", Status.FAIL);

			}



		//Cover Type/ Frequency
			WebElement coverType = driver.findElement(LifeEventObject.obj_PersonalDetails_PremiumFrequency);
			js.executeScript("arguments[0].scrollIntoView(true);", coverType);
			fieldValidation("WebList",coverType,CostType,"Premiun frequency");

			 WebElement objcoverTypeFixed = driver.findElement(LifeEventObject.obj_PersonalDetails_Fixed);
			 fieldValidation("WebButton",objcoverTypeFixed,"Fixed","AmountType");



			// Death
			if (DeathUnits.equalsIgnoreCase("0")) {
				System.out.println("0");
				 WebElement objzeroUnit = driver.findElement(LifeEventObject.obj_PersonalDetails_Fixed_DeathUnit0);
				 fieldValidation("WebButton",objzeroUnit,DeathUnits,"DeathAmount");
			}

			else if (DeathUnits.equalsIgnoreCase("1")) {
				System.out.println("1");
				 WebElement objoneUnit = driver.findElement(LifeEventObject.obj_PersonalDetails_Fixed_DeathUnit1);
				 fieldValidation("WebButton",objoneUnit,DeathUnits,"DeathAmount");

			} else if (DeathUnits.equalsIgnoreCase("2")) {
				System.out.println("2");
				 WebElement objtwoUnit = driver.findElement(LifeEventObject.obj_PersonalDetails_Fixed_DeathUnit2);
				 fieldValidation("WebButton",objtwoUnit,DeathUnits,"DeathAmount");

			} else {
				report.updateTestLog("Data Table", "Invalid input for Death Units in Data Sheet. Please enter 0/1/2",
						Status.FAIL);
			}


			// TPD
			if (TPDUnits.equalsIgnoreCase("0")) {
				System.out.println("0");
				 WebElement objzeroUnitTPD = driver.findElement(LifeEventObject.obj_PersonalDetails_Fixed_TPDUnit0);
				 fieldValidation("WebButton",objzeroUnitTPD,TPDUnits,"TPD Amount");
			}

			else if (TPDUnits.equalsIgnoreCase("1")) {
				System.out.println("1");
				 WebElement objoneUnitTPD = driver.findElement(LifeEventObject.obj_PersonalDetails_Fixed_TPDUnit1);
				 fieldValidation("WebButton",objoneUnitTPD,TPDUnits,"TPD Amount");

			} else if (TPDUnits.equalsIgnoreCase("2")) {
				System.out.println("2");
				 WebElement objtwoUnitTPD = driver.findElement(LifeEventObject.obj_PersonalDetails_Fixed_TPDUnit2);
				 fieldValidation("WebButton",objtwoUnitTPD,TPDUnits,"TPD Amount");

			} else {
				report.updateTestLog("Data Table", "Invalid input for TPD Unit in Data Sheet. Please enter 0/1/2",
						Status.FAIL);
			}


	}

	public void validateChangeCoverConfirm() {
		sleep(2000);

		// *** Retrieving the saved APP**//
		wait.until(ExpectedConditions.elementToBeClickable(LandingPageObject.savesApplication)).click();
		report.updateTestLog("Retrieve Saved Application", "Retrieve Saved Application link is Clicked", Status.PASS);
		sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(LifeEventObject.obj_PersonalDetails_OpenSavedApplication))
				.click();
		report.updateTestLog("Retrieve Saved Application", "Application is Retrieved", Status.PASS);
		sleep(4000);

		WebElement objAgree = driver.findElement(By.xpath("//*[@id='generalConsentLabel']"));
		fieldValidation("WebCheckBox", objAgree, "UnChecked", "Agree");
		detailsValidation();

	}

	public static double round(double value, int numberOfDigitsAfterDecimalPoint) {
		BigDecimal bigDecimal = new BigDecimal(value);
		bigDecimal = bigDecimal.setScale(numberOfDigitsAfterDecimalPoint, BigDecimal.ROUND_HALF_UP);
		return bigDecimal.doubleValue();
	}

}
