package au.com.metlife.eapply.ct.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
@Entity
@Table(
        name="Login_Attempts",schema="EAPPDB")
public class LoginAttempts {
	
 @Id
 @GeneratedValue(strategy = GenerationType.AUTO)
private long id;
 @NotNull	  
 @Column(length=25)
 private String ipAddress;
   
 @Column
 private Character ipLocked;
 @NotNull	  
 @Column
 private Integer attempts;
 @Column
 private java.util.Date lastLogin;
 
public long getId() {
	return id;
}
public void setId(long id) {
	this.id = id;
}
public String getIpAddress() {
	return ipAddress;
}
public void setIpAddress(String ipAddress) {
	this.ipAddress = ipAddress;
}
public Character getIpLocked() {
	return ipLocked;
}
public void setIpLocked(Character ipLocked) {
	this.ipLocked = ipLocked;
}
public Integer getAttempts() {
	return attempts;
}
public void setAttempts(Integer attempts) {
	this.attempts = attempts;
}
public java.util.Date getLastLogin() {
	return lastLogin;
}
public void setLastLogin(java.util.Date lastLogin) {
	this.lastLogin = lastLogin;
}


}

