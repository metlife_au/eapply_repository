package pages.metlife;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.WSSHandlerConstants;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import com.cognizant.framework.Status;
import com.cognizant.framework.selenium.WebDriverUtil;

import au.com.metlife.quote.beans.JAXBHelper;
import au.com.metlife.quote.beans.QuoteOutput;
import au.com.metlife.webservices.QuotationServiceStub;
import supportlibraries.ScriptHelper;
import uimap.metlife.ColesConfirmationPageObjects;
import uimap.metlife.ColesGetQuotePageObjects;
import uimap.metlife.ColesHealthAPageObjects;
import uimap.metlife.ColesPaymentPageObjects;
import uimap.metlife.STFTConfirmationPageObjects;
import uimap.metlife.STFTGetQuotePageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTHealthPageObjects;
import uimap.metlife.STFTPaymentsPageObjects;

public class STFTPaymentsPage extends MasterPage {
	WebDriverUtil driverUtil = null;


	public STFTPaymentsPage yourPayment() {
		paymentDetails();
		return new STFTPaymentsPage(scriptHelper);
	}

	public STFTPaymentsPage(ScriptHelper scriptHelper) {
		super(scriptHelper);
		driverUtil = new WebDriverUtil(driver);
		sleep(1000);
		switchToActiveWindow();
	}	

	public void paymentDetails() {

		String cardType = dataTable.getData("MetLife_Data", "CardType");
		String ccType = dataTable.getData("MetLife_Data", "CcType");
		String expiryMonth = dataTable.getData("MetLife_Data", "ExpiryMonth");
		String expiryYear = dataTable.getData("MetLife_Data", "ExpiryYear");
		String aacountName = dataTable.getData("MetLife_Data", "AccountName");
		String accountNumber = dataTable.getData("MetLife_Data", "AccountNumber");
		String bsb = dataTable.getData("MetLife_Data", "BSB");
		String cYSTest = dataTable.getData("MetLife_Data", "CYSTest");
		try {
			switchToActiveWindow();
			driver.manage().window().maximize();
			
			if (cardType.equalsIgnoreCase("Debit")) {
			tryAction(fluentWaitElement(STFTPaymentsPageObjects.rddebit), "clkradio", "Debit");
			Thread.sleep(500);
			
			
			tryAction(waitForClickableElement(STFTPaymentsPageObjects.txtaccountname), "SET", "AccountName", aacountName);
			tryAction(waitForClickableElement(STFTPaymentsPageObjects.txtaccountname), "TAB", "AccountName");
			
			tryAction(waitForClickableElement(STFTPaymentsPageObjects.txtaccountnumber), "SET", "AccountNumber", accountNumber);
			tryAction(waitForClickableElement(STFTPaymentsPageObjects.txtaccountnumber), "TAB", "AccountNumber");
			
			tryAction(waitForClickableElement(STFTPaymentsPageObjects.txtbsb), "SET", "BSB", bsb);
			tryAction(waitForClickableElement(STFTPaymentsPageObjects.txtbsb), "TAB", "BSB");
			}
			else
			{
				
			 tryAction(fluentWaitElement(STFTPaymentsPageObjects.rdcredit), "clkradio", "Credit");
				Thread.sleep(500);
				
				switch(ccType){
				case "Visa":
					
					tryAction(fluentWaitElement(STFTPaymentsPageObjects.rdvisa), "clkradio", "Visa");
				    Thread.sleep(500);
					 break;	 
				case "Master":
					
				    tryAction(fluentWaitElement(STFTPaymentsPageObjects.rdmaster), "clkradio", "Master");
			        Thread.sleep(500);
					 break;
				case "Amex":
					
			        tryAction(fluentWaitElement(STFTPaymentsPageObjects.rdamex), "clkradio", "Amex");
		            Thread.sleep(500);
					 break;
				case "DC":
					
				    tryAction(fluentWaitElement(STFTPaymentsPageObjects.rddc), "clkradio", "DC");
			        Thread.sleep(500);
				}
			tryAction(waitForClickableElement(STFTPaymentsPageObjects.txtccnumber), "SET", "CCNumber", accountNumber);
			tryAction(waitForClickableElement(STFTPaymentsPageObjects.txtccnumber), "TAB", "CCNumer");
			sleep(1000);
			tryAction(waitForClickableElement(STFTPaymentsPageObjects.txtccname), "SET", "CCName", aacountName);
			tryAction(waitForClickableElement(STFTPaymentsPageObjects.txtccname), "TAB", "CCNumer");
			sleep(1000);
			tryAction(fluentWaitElement(STFTPaymentsPageObjects.selCCExpMonth),"specialDropdown", "Expiry Month", expiryMonth);
			sleep(2000);
			tryAction(fluentWaitElement(STFTPaymentsPageObjects.selCcExpYr),"specialDropdown", "Expiry Year", expiryYear);
			}

			tryAction(waitForClickableElement(STFTPaymentsPageObjects.chkacknowledge), "Click", "Acknowledge");
			
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(STFTPaymentsPageObjects.btnsubmit)).click().perform();
			Thread.sleep(2000);	
			switchToActiveWindow();
			driver.manage().window().maximize();
			if((waitForClickableElement(ColesConfirmationPageObjects.lnkNo)).isDisplayed()){
				tryAction(waitForClickableElement(ColesConfirmationPageObjects.lnkNo),"Click", "Survey NO");
				}
			//System.out.println(driver.findElement(STFTPaymentsPageObjects.txtAppnum).isDisplayed());
			if(driver.getPageSource().contains("Application number:")){
			String appNumber1=driver.findElement(STFTPaymentsPageObjects.txtAppnum).getText();
			String appNumber = appNumber1.substring(0, appNumber1.length() - 1); 
			dataTable.putData("MetLife_Data","AppNumber", appNumber);
			System.out.println(appNumber);	
			}
			switchToActiveWindow();
			driver.manage().window().maximize();	
			if (cYSTest.equalsIgnoreCase("Yes")) {	
				driver.get("https://ebctest.cybersource.com/ebctest/login/Login.do");
				driver.manage().window().maximize();
			}
		} catch (Exception e) {
			report.updateTestLog("Error", "Error in performing action " + e.getMessage(), Status.FAIL);
		}		
		
	}
}
