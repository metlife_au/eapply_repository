/*Transfer Summary Page Controller Starts*/
StateWideApp.controller('transferSummary',['$scope','$rootScope','$location','$timeout','$window','$routeParams', 'auraInputService','getAuraTransferData','submitAura','PersistenceService','persoanlDetailService','auraResponseService','ngDialog','submitEapply','urlService','RetrieveAppDetailsService','saveEapply',
                         function($scope, $rootScope,$location, $timeout,$window, $routeParams, auraInputService,getAuraTransferData,submitAura,PersistenceService,persoanlDetailService,auraResponseService,ngDialog,submitEapply,urlService,RetrieveAppDetailsService, saveEapply){

	$rootScope.$broadcast('enablepointer');
	$scope.urlList = urlService.getUrlList();
	$scope.svdRtrv = false;
    //$scope.claimNo = claimNo
    $scope.go = function (path) {
    	if(($scope.svdRtrv || $scope.transferCoverDetails.svRt) && path == '/quotetransfer/2')
    		{
    			path = "/quotetransfer/3";
    		}
    		if(($scope.svdRtrv || $scope.transferCoverDetails.svRt) && path == '/auratransfer/2')
    			{
    				path = "/auratransfer/3";
    			}
    	
    	$timeout(function(){
    		$location.path(path);
    	}, 10);
  	};
  	$scope.collapse = false;
  	$scope.toggle = function() {
        $scope.collapse = !$scope.collapse;
    };


    $scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
        	}*/
    	ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
     };

  // added for session expiry
	   /* $timeout(callAtTimeout, 900000);
	  	function callAtTimeout() {
	  		$location.path("/sessionTimeOut");
	  	}*/
    /* var timer;
     angular.element($window).bind('mouseover', function(){
     	timer = $timeout(function(){
     		sessionStorage.clear();
     		localStorage.clear();
     		$location.path("/sessionTimeOut");
     	}, 900000);
     }).bind('mouseout', function(){
     	$timeout.cancel(timer);
     });*/

    /* var ackCheckTRDD;
     var ackCheckTRPP;*/
     var ackCheckTRGC;


    $scope.transferCoverDetails=PersistenceService.gettransferCoverDetails();
    console.log($scope.transferCoverDetails);

    $scope.transferCoverAuraDetails=PersistenceService.getTransCoverAuraDetails();
    console.log($scope.transferCoverAuraDetails);

    $scope.personalDetails = persoanlDetailService.getMemberDetails();
    $scope.occupationDetails =PersistenceService.getTransferCoverOccDetails();
  	$scope.deathAddnlCvrDetails =PersistenceService.getTransferDeathAddnlDetails();
	$scope.tpdAddnlCvrDetails=PersistenceService.getTransferTpdAddnlDetails();
	$scope.ipAddnlCvrDetails=PersistenceService.getTransferIpAddnlDetails();
	$scope.uploadedFileDetails = PersistenceService.getUploadedFileDetails();

    $scope.submitTransferCover = function(){

    	/*ackCheckTRDD = $('#DutyOfDisclosureLabelTR').hasClass('active');
    	ackCheckTRPP = $('#privacyPolicyLabelTR').hasClass('active');*/
    	ackCheckTRGC = $('#generalConsentLabelTR').hasClass('active');

    	if(/*ackCheckTRDD && ackCheckTRPP &&*/ ackCheckTRGC){

    		/*$scope.TRDDackFlag = false;
    		$scope.TRPPackFlag = false;*/
    		$scope.TRGCackFlag = false;
    		if($scope.transferCoverDetails != null && $scope.deathAddnlCvrDetails != null && $scope.tpdAddnlCvrDetails != null && $scope.ipAddnlCvrDetails != null &&
    		      $scope.occupationDetails != null && $scope.transferCoverAuraDetails != null && $scope.personalDetails != null){
    			$rootScope.$broadcast('disablepointer');
    			$scope.transferCoverDetails.lastSavedOn = '';
    			$scope.details={};
    			$scope.details.occupationDetails =$scope.occupationDetails;
    			$scope.details.addnlDeathCoverDetails=$scope.deathAddnlCvrDetails;
    			$scope.details.addnlTpdCoverDetails=$scope.tpdAddnlCvrDetails;
    			$scope.details.addnlIpCoverDetails=$scope.ipAddnlCvrDetails;
    			$scope.details.transferDocuments = $scope.uploadedFileDetails;
    			var coverObject = angular.extend($scope.details,$scope.transferCoverDetails);
            	var auraObject = angular.extend(coverObject,$scope.transferCoverAuraDetails);
    			var submitObject = angular.extend(auraObject,$scope.personalDetails);
    			auraResponseService.setResponse(submitObject);
    			submitEapply.reqObj($scope.urlList.submitEapplyUrl).then(function(response) {
            		console.log(response.data);
            		PersistenceService.setPDFLocation(response.data.clientPDFLocation);
            		PersistenceService.setNpsUrl(response.data.npsTokenURL);
            		if($scope.transferCoverAuraDetails.overallDecision == 'ACC'){
                		$scope.go('/transferAccept');
                	}else if($scope.transferCoverAuraDetails.overallDecision == 'DCL'){
                		$scope.go('/transferDecline');
                	}
            	});
            }
    	}else{
    		/*if(ackCheckTRDD){
        		$scope.TRDDackFlag = false;
        	}else{
        		$scope.TRDDackFlag = true;
        	}

        	if(ackCheckTRPP){
        		$scope.TRPPackFlag = false;
        	}else{
        		$scope.TRPPackFlag = true;
        	}*/

        	if(ackCheckTRGC){
        		$scope.TRGCackFlag = false;
        	}else{
        		$scope.TRGCackFlag = true;
        	}
        	$scope.scrollToUncheckedElement();
    	}
    };

    $scope.scrollToUncheckedElement = function(){
		var elements = [/*ackCheckTRDD, ackCheckTRPP, */ackCheckTRGC];
		var ids = [/*'DutyOfDisclosureLabelTR', 'privacyPolicyLabelTR',*/ 'generalConsentLabelTR'];
    	for(var k = 0; k < elements.length; k++){
    		if(!elements[k]){
    			$('html, body').animate({
        	        scrollTop: $("#" + ids[k]).offset().top
        	    }, 1000);
    			break;
    		}
    	}
    };
    var appNum;
    appNum = PersistenceService.getAppNumber();
    $scope.saveTransferSummary = function(){
    	$scope.transferSummarySaveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Please note down this number as it will assist you in retrieving your saved application.<BR><BR>');
    	if($scope.transferCoverDetails != null && $scope.occupationDetails != null &&  $scope.deathAddnlCvrDetails != null && $scope.tpdAddnlCvrDetails != null && $scope.ipAddnlCvrDetails != null && $scope.transferCoverAuraDetails != null&& $scope.personalDetails != null){
    		$scope.transferCoverDetails.lastSavedOn = 'SummaryTransferPage';
    		$scope.details={};
    		$scope.details.occupationDetails =$scope.occupationDetails;
    		$scope.details.addnlDeathCoverDetails =$scope.deathAddnlCvrDetails;
    		$scope.details.addnlTpdCoverDetails=$scope.tpdAddnlCvrDetails;
    		$scope.details.addnlIpCoverDetails=$scope.ipAddnlCvrDetails;
    		$scope.details.transferDocuments = $scope.uploadedFileDetails;
    		var temp = angular.extend($scope.transferCoverDetails,$scope.details);
    		var aura = angular.extend(temp,$scope.transferCoverAuraDetails);
        	var saveTransferSummaryObject = angular.extend(aura, $scope.personalDetails);
        	auraResponseService.setResponse(saveTransferSummaryObject);
        	$rootScope.$broadcast('disablepointer');
	        saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {
	                console.log(response.data)
	        }, function(err){
	        	console.log("Error while saving.. " + JSON.stringify(err));
	        });
        }
    };

    $scope.transferSummarySaveAndExitPopUp = function (hhText) {

		var dialog1 = ngDialog.open({
			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
				className: 'ngdialog-theme-plain custom-width',
				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
				plain: true
		});
		dialog1.closePromise.then(function (data) {
			console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};

	if($routeParams.mode == 3){
    	var num = PersistenceService.getAppNumToBeRetrieved();

    	RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,num).then(function(res){
    		var result = res.data[0];
    		var coverDet = {}, occDet ={}, deathAddDet = {},tpdAddDet={}, ipAddDet={};
    		coverDet.firstName = result.personalDetails.firstName;
    		coverDet.lastName = result.personalDetails.lastName;
    		coverDet.name = result.personalDetails.firstName +" "+result.personalDetails.lastName;
    		coverDet.dob = result.dob;
    		coverDet.email = result.email;
    		coverDet.contactType = result.contactType;
    		coverDet.contactPhone = result.contactPhone;
    		coverDet.contactPrefTime = result.contactPrefTime;
    		occDet.gender = result.personalDetails.gender;
    		coverDet.transferDeathExistingAmt = result.existingDeathAmt;
    		coverDet.deathOccCategory = result.deathOccCategory;
    		coverDet.transferTpdExistingAmt = result.existingTpdAmt;
    		coverDet.tpdOccCategory = result.tpdOccCategory;
    		coverDet.transferIpExistingAmt = result.existingIPAmount;
    		coverDet.existingIPUnits = result.existingIPUnits;
    		coverDet.totalPremium = result.totalPremium;
    		coverDet.ipOccCategory = result.ipOccCategory;
    		coverDet.previousFundName = result.previousFundName;
    		coverDet.membershipNumber = result.membershipNumber;
    		coverDet.spinNumber = result.spinNumber;
    		coverDet.documentName = result.documentName;
    		coverDet.appNum = result.appNum;
    		coverDet.manageType = "TCOVER";
    		coverDet.partnerCode ="SFPS";
    		coverDet.freqCostType = result.freqCostType;
    		coverDet.documentName = (result.transferDocuments && result.transferDocuments.length > 0) ? 'Yes' : 'No';
    		$scope.uploadedFileDetails = coverDet.documentName === 'Yes' ? result.transferDocuments : [];
			$scope.transferCoverDetails = coverDet;

    		occDet.fifteenHr = result.occupationDetails.fifteenHr;
    		occDet.citizenQue = result.occupationDetails.citizenQue;
    		occDet.industryName = result.occupationDetails.industryName;
    		occDet.occupation = result.occupationDetails.occupation;
    		occDet.withinOfficeQue = result.occupationDetails.withinOfficeQue;
    		occDet.tertiaryQue = result.occupationDetails.tertiaryQue;
    		occDet.managementRoleQue = result.occupationDetails.managementRoleQue;
    		occDet.hazardousQue = result.occupationDetails.hazardousQue;
    		occDet.salary = result.occupationDetails.salary;
    		$scope.occupationDetails = occDet;

    		deathAddDet.deathTransferAmt = result.addnlDeathCoverDetails.deathTransferAmt;
    		deathAddDet.deathTransferCovrAmt = result.addnlDeathCoverDetails.deathTransferCovrAmt;
    		deathAddDet.deathTransferWeeklyCost = result.addnlDeathCoverDetails.deathTransferWeeklyCost;
    		$scope.deathAddnlCvrDetails = deathAddDet;

    		tpdAddDet.tpdTransferAmt = result.addnlTpdCoverDetails.tpdTransferAmt;
    		tpdAddDet.tpdTransferCovrAmt = result.addnlTpdCoverDetails.tpdTransferCovrAmt;
    		tpdAddDet.tpdTransferWeeklyCost = result.addnlTpdCoverDetails.tpdTransferWeeklyCost;
    		$scope.tpdAddnlCvrDetails = tpdAddDet;

    		ipAddDet.ipTransferAmt = result.addnlIpCoverDetails.ipTransferAmt;
    		ipAddDet.ipTransferCovrAmt = result.addnlIpCoverDetails.ipTransferCovrAmt;
    		ipAddDet.addnlTransferWaitingPeriod = result.addnlIpCoverDetails.addnlTransferWaitingPeriod;
    		ipAddDet.addnlTransferBenefitPeriod = result.addnlIpCoverDetails.addnlTransferBenefitPeriod;
    		ipAddDet.totalipwaitingperiod = result.addnlIpCoverDetails.totalipwaitingperiod;
    		ipAddDet.totalipbenefitperiod = result.addnlIpCoverDetails.totalipbenefitperiod;
    		ipAddDet.ipTransferWeeklyCost = result.addnlIpCoverDetails.ipTransferWeeklyCost;
    		$scope.ipAddnlCvrDetails = ipAddDet;
    		$scope.svdRtrv = true;
    		
    		PersistenceService.settransferCoverDetails(coverDet);
    		PersistenceService.setTransferCoverOccDetails(occDet);
    		PersistenceService.setTransferDeathAddnlDetails(deathAddDet);
            PersistenceService.setTransferTpdAddnlDetails(tpdAddDet);
            PersistenceService.setTransferIpAddnlDetails(ipAddDet);
            
            $scope.getAuradetails();

    	}, function(err){
    		console.error("Something went wrong while retrieving the details " + JSON.stringify(err));
    	});
	}

	$scope.getAuradetails = function()
    {

  		auraInputService.setFund('SFPS');
  	  	auraInputService.setMode('TransferCover');
  	    auraInputService.setAge(moment().diff(moment($scope.transferCoverDetails.dob, 'DD-MM-YYYY'), 'years')) ;
  	  	auraInputService.setName($scope.transferCoverDetails.name);
  	  	auraInputService.setAppnumber($scope.transferCoverDetails.appNum);

  	  	if($scope.occupationDetails && $scope.occupationDetails.gender ){
  	  		auraInputService.setGender($scope.occupationDetails.gender);
  	  	}
  	  	auraInputService.setCountry('Australia');
  	  	auraInputService.setFifteenHr($scope.occupationDetails.fifteenHr);
  	  	auraInputService.setDeathAmt(parseInt($scope.deathAddnlCvrDetails.deathTransferCovrAmt));
  	  	auraInputService.setTpdAmt(parseInt($scope.tpdAddnlCvrDetails.tpdTransferCovrAmt));
  	  	auraInputService.setIpAmt(parseInt($scope.ipAddnlCvrDetails.ipTransferCovrAmt));
  	  	/*auraInputService.setWaitingPeriod($scope.ipAddnlCvrDetails.addnlTransferWaitingPeriod);
  	  	auraInputService.setBenefitPeriod($scope.ipAddnlCvrDetails.addnlTransferBenefitPeriod) ;*/
  	  	auraInputService.setWaitingPeriod($scope.ipAddnlCvrDetails.totalipwaitingperiod);
	  	auraInputService.setBenefitPeriod($scope.ipAddnlCvrDetails.totalipbenefitperiod) ;
  	  	auraInputService.setIndustryOcc($scope.occupationDetails.industryCode+":"+$scope.occupationDetails.occupation);
  	  	auraInputService.setSalary($scope.occupationDetails.salary);
  	  	auraInputService.setClientname('metaus')
  	  	auraInputService.setLastName($scope.transferCoverDetails.lastName)
  	  	auraInputService.setFirstName($scope.transferCoverDetails.firstName)
  	  	auraInputService.setDob($scope.transferCoverDetails.dob)
  	  	auraInputService.setExistingTerm(false);
  	  	if($scope.personalDetails.memberType=="Personal"){
  	  		auraInputService.setMemberType("INDUSTRY OCCUPATION")
  	  	}else{
  	  		auraInputService.setMemberType("None")
  	  	}
  	  	
  		 getAuraTransferData.requestObj($scope.urlList.auraTransferDataUrl).then(function(response) {
  	  		$scope.auraResponseDataList = response.data.questions;
  	  		angular.forEach($scope.auraResponseDataList, function(Object) {
  				$scope.sectionname = Object.questionAlias.substring(3);

  				});
  	  	});
  	
    	 
    	 submitAura.requestObj($scope.urlList.submitAuraUrl).then(function(response) {
    		 $scope.transferCoverAuraDetails = response.data;
    	 });
    	 
    };

	/*$scope.checkAckStateTR = function(){
    	$timeout(function(){
    		ackCheckTRDD = $('#DutyOfDisclosureLabelTR').hasClass('active');
        	ackCheckTRPP = $('#privacyPolicyLabelTR').hasClass('active');
        	ackCheckTRGC = $('#generalConsentLabelTR').hasClass('active');

        	if(ackCheckTRDD){
        		$scope.TRDDackFlag = false;
        	}else{
        		$scope.TRDDackFlag = true;
        	}

        	if(ackCheckTRPP){
        		$scope.TRPPackFlag = false;
        	}else{
        		$scope.TRPPackFlag = true;
        	}

        	if(ackCheckTRGC){
        		$scope.TRGCackFlag = false;
        	}else{
        		$scope.TRGCackFlag = true;
        	}
    	}, 10);
    };*/

	/*$scope.checkAckStateDDTR = function(){
    	$timeout(function(){
    		ackCheckTRDD = $('#DutyOfDisclosureLabelTR').hasClass('active');
        	if(ackCheckTRDD){
        		$scope.TRDDackFlag = false;
        	}else{
        		$scope.TRDDackFlag = true;
        	}
    	}, 10);
    };

    $scope.checkAckStatePPTR = function(){
    	$timeout(function(){
    		ackCheckTRPP = $('#privacyPolicyLabelTR').hasClass('active');
        	if(ackCheckTRPP){
        		$scope.TRPPackFlag = false;
        	}else{
        		$scope.TRPPackFlag = true;
        	}
    	}, 10);
    };*/

    $scope.checkAckStateGCTR = function(){
    	$timeout(function(){
    		ackCheckTRGC = $('#generalConsentLabelTR').hasClass('active');
        	if(ackCheckTRGC){
        		$scope.TRGCackFlag = false;
        	}else{
        		$scope.TRGCackFlag = true;
        	}
    	}, 10);
    };


    }]);
/*Transfer Summary Page Controller Ends*/
