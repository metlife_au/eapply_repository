/**
 * 
 */
package au.com.metlife.eapply.b2b.service;

import au.com.metlife.eapply.b2b.model.Eappstring;
import au.com.metlife.eapply.b2b.xsd.beans.Request;

/**
 * @author akishore
 *
 */
public interface B2bService {
	
	Eappstring generateToken(String input, String outToken);
	Eappstring retriveClientData(String token);

}
