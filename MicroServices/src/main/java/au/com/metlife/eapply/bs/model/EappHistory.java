package au.com.metlife.eapply.bs.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the AURADETAILS database table.
 * 
 */
@Entity
@Table(name = "EAPPHISTORY", schema="EAPPDB")
public class EappHistory implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="EAPPHISTORY_SEQ")
	@SequenceGenerator(name="EAPPHISTORY_SEQ",sequenceName = "EAPPDB.EAPPHISTORY_SEQ", initialValue=1, allocationSize=100)
	private long id;
	@Column(length=50)
	private String appnumber;
	@Column(length=10)
	private String appdecision;	

	/*@Column(nullable=false, length=47)
	private String eapplicationid;*/

	@Column(length=100)
	private String clientrefnumber;
	@Column(length=50)
	private String fundid;

	public String getFundid() {
		return fundid;
	}

	public void setFundid(String fundid) {
		this.fundid = fundid;
	}

	@Column(length=50)
	private String picscaseid;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAppnumber() {
		return appnumber;
	}

	public void setAppnumber(String appnumber) {
		this.appnumber = appnumber;
	}

	public String getAppdecision() {
		return appdecision;
	}

	public void setAppdecision(String appdecision) {
		this.appdecision = appdecision;
	}

	public String getClientrefnumber() {
		return clientrefnumber;
	}

	public void setClientrefnumber(String clientrefnumber) {
		this.clientrefnumber = clientrefnumber;
	}

	

	public String getPicscaseid() {
		return picscaseid;
	}

	public void setPicscaseid(String picscaseid) {
		this.picscaseid = picscaseid;
	}

	public String getJobnumber() {
		return jobnumber;
	}

	public void setJobnumber(String jobnumber) {
		this.jobnumber = jobnumber;
	}

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}

	public String getRectypeinfo() {
		return rectypeinfo;
	}

	public void setRectypeinfo(String rectypeinfo) {
		this.rectypeinfo = rectypeinfo;
	}

	@Column(length=50)
	private String jobnumber;
	
	private String metadata;
	private String status;	
	private Timestamp createdate;
	private String rectypeinfo;	

	public EappHistory() {
		//Do nothing as written for future use
	}


	

}