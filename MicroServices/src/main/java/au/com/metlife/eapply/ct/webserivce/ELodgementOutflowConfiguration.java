package au.com.metlife.eapply.ct.webserivce;


import org.apache.axis2.description.Parameter;
import org.apache.rampart.handler.config.OutflowConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.metlife.eapply.ct.exception.MetlifeException;


public class ELodgementOutflowConfiguration {
	private ELodgementOutflowConfiguration(){
		
	}
	private static final Logger log = LoggerFactory.getLogger(ELodgementOutflowConfiguration.class);
	/*private static final String PACKAGE_NAME = ELodgementOutflowConfiguration.class
			.getPackage().getName();

	private static final String CLASS_NAME = ELodgementOutflowConfiguration.class.getName();
	*//**
	 * Fetch the properties of Configuration interms of Parameter for the given
	 * username through OutflowConfiguration Class
	 * 
	 * @param username
	 *            is of type String.
	 * @return param is of type Parameter with the username and Password type
	 *         settings.
	 * @throws MetlifeException
	 */
	public static Parameter getOutflowConfiguration(String username) throws MetlifeException {
		log.info("Get out flow configuration start");
		OutflowConfiguration ofc = null;
		
		try{
	    ofc = new OutflowConfiguration();
		ofc.setActionItems("UsernameToken Timestamp");
		ofc.setPasswordType("PasswordText");
		ofc.setUser(username);
		}catch (Exception e) {
			
		//		log.error("Error in get out flow configuration : {}",e.getMessage());
			throw new MetlifeException();
		
		}
		
		log.info("Get out flow configuration start");
		return ofc.getProperty();
	}

}

