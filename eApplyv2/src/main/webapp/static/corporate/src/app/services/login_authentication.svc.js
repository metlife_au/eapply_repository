// Auth service will be placed here
angular.module('CorporateApp').service('inputDataService',function(){
	var _self= this;
	_self.setCoverDetails= function (temp) {
        _self.CoverDetails= temp;
	};
	_self.getCoverDetails= function () {
		return _self.CoverDetails;
	};
});

angular.module('CorporateApp').factory('fetchCategoryDetailsService',['$http','$q','fetchTokenNumSvc', function($http, $q, fetchTokenNumSvc){
	var object={};
	object.getCategoryDetails= function(url,fundCode,category){
		var defer= $q.defer();
		$http.get(url,{
			headers:{
        'Content-Type': 'application/json',
        'Authorization':fetchTokenNumSvc.getTokenId()
			},
			params:{
				'fundCode': fundCode,
				'category': category
			}
		}).then(function(res){
			object= res.data;
			defer.resolve(res);
		},function(err){
			console.log('Something went wrong file fecthing category details...'+JSON.stringify(err));
			defer.reject(err);
		});
		return defer.promise;
	};
	return object;
}]); 
angular.module('restAPI')
.service('fetchTokenNumSvc',function(){
  var _self= this;
  _self.setTokenId= function(tempToken){
    _self.tokenId= tempToken;
  };
  _self.getTokenId= function() {
    return _self.tokenId;
  };
});

angular.module('CorporateApp').service('calculateFUL', ['$http', '$q','fetchTokenNumSvc', function($http, $q,fetchTokenNumSvc) {
	var factory = {};
	factory.calculate = function(url, data){
		var defer = $q.defer();
		$http.post(url,data,{
			 headers : {
                 'Content-Type': 'application/json',
                 'Authorization':fetchTokenNumSvc.getTokenId()
             }
		}).then(function(res) {
			factory = res.data;
			defer.resolve(res);
		}, function(err) {
			console.log('Error while calculation FUL..' + JSON.stringify(err));
			defer.reject(err);
		});
		return defer.promise;
	};
	return factory;
}]);
angular.module('restAPI')
.service('fetchAppNumberSvc', function() {
  var _self = this;
  _self.setAppNumber = function(appnum) {
    _self.UniqueAppNum = appnum;
  };

  _self.getAppNumber = function() {
    return _self.UniqueAppNum;
  };
});

angular.module('restAPI')
.service('fetchIndustryList', function(fetchQuoteSvc, $q) {
  var _self = this;
  _self.industryOptions = {};
  _self.getObj = function(url) {
    var defer = $q.defer();
    fetchQuoteSvc.getList(url, 'HOST').then(function(res) {
      _self.setIndustryOptions(res.data);
      defer.resolve(res.data);
    }, function(err){
      defer.reject(err);
      console.info('Error while fetching industry list ' + JSON.stringify(err));
    });
    return defer.promise;
  };

  _self.getIndustryOptions= function() {
      return _self.industryOptions;
  };

  _self.setIndustryOptions= function(obj) {
      _self.industryOptions= obj;
  };

  _self.getIndustryName= function(industryCode) {
    var selectedIndustry = _self.industryOptions.filter(function(obj) {
      return industryCode=== obj.key;
    });
    return selectedIndustry[0].value;
  };

});

angular.module('restAPI')
.service('fetchQuoteSvc',['$http', '$q','fetchTokenNumSvc', function($http, $q,fetchTokenNumSvc){
  var _self= this;
  _self.getList= function(url, fundCode) {
    var defer = $q.defer();
    	$http.get(url, {
    		headers:{
    			'Content-Type': 'application/json',
				'Authorization':fetchTokenNumSvc.getTokenId()
    		}, params:{
    			'fundCode': fundCode
    		}}).then(function(res){
    			defer.resolve(res);
    		}, function(err){
    			console.log('Error while getting industries ' + JSON.stringify(err));
    			defer.reject(err);
    		});
    	return defer.promise;
	};
}]);

angular.module('restAPI')
.service('fetchOccupationSvc', ['$http', '$q','fetchTokenNumSvc', function($http, $q,fetchTokenNumSvc) {
	var _self = this;
	_self.getOccupationList = function(url,fundId,induCode) {
		var defer = $q.defer();
		$http.get(url, {
			headers:{
				'Content-Type': 'application/json',
				'Authorization':fetchTokenNumSvc.getTokenId()
			},params:{
				'fundId': fundId,
				'induCode': induCode
			}}).then(function(res) {
				defer.resolve(res);
			}, function(err) {
				console.log('Error while getting occupations ' + JSON.stringify(err));
				defer.reject(err);
			});
		return defer.promise;
	};
}]);
angular.module('CorporateApp')
.service('saveEapplyData', function($http, $q,fetchTokenNumSvc) {
  var _self = this;
  _self.reqObj = function (url, data) {
    var defer = $q.defer();
    $http.post(url, data,{
    	headers:{
    		'Authorization':  fetchTokenNumSvc.getTokenId()
		}
    }).then(function(response) {
    	defer.resolve(response);
    }, function(response) {
    	defer.reject(response);
    });
    return defer.promise;
  };
});
angular.module('CorporateApp').factory('printPageService',['$http','$q','fetchTokenNumSvc',function($http,$q,fetchTokenNumSvc){
	var object={};
	object.getPrintObject= function(url,data,token){
		var defer= $q.defer();
		$http.post(url,data,{
			headers: {
				'Content-Type': 'application/json',
				'Authorization':fetchTokenNumSvc.getTokenId()
			},
		}).then(function(res){
			object= res.data;
			defer.resolve(res);
		},function(err){
			console.log('Something went wrong file fecthing category details...'+JSON.stringify(err));
			defer.reject(err);
		});
		return defer.promise;
	};
	return object;
}]); 
angular.module('CorporateApp').factory('DownloadPDFService',['$http','$q','fetchTokenNumSvc', function($http, $q, fetchTokenNumSvc){
    var object = {};
    object.download = function(url,fileName){
        var defer = $q.defer();
        $http.post(url, {},{ 		
			headers: {
        'Content-Type': 'application/json',
        'Authorization':fetchTokenNumSvc.getTokenId()
			}, params:{
	            'file_name': fileName
			},responseType: 'arraybuffer',
			transformResponse: function (data) {
	            var pdf;
	            if (data) {
	                pdf = new Blob([data], {
	                    type: 'application/pdf'
	                });
	            }
	            return {
	                response: pdf
	            };
        }}).then(function(res){
        	object = res.data;
            defer.resolve(res);
        }, function(err){
            console.log('Error while getting pdf data' + JSON.stringify(err));
            defer.reject(err);
        });
        return defer.promise;
    };
    return object;
}]);
angular.module('CorporateApp').factory('CheckSavedAppService',['$http','$q','fetchTokenNumSvc', function($http,$q,fetchTokenNumSvc){
    var factory = {};
    factory.checkSavedApps = function(url,fundCode,clientRefNo,manageType){
        var defer = $q.defer();
        $http.get(url, {
        	headers:{
                'Content-Type': 'application/json',
                'Authorization':fetchTokenNumSvc.getTokenId()
        	},params:{
                'fundCode': fundCode,
                'clientRefNo': clientRefNo,
                'manageType':manageType
        	}}).then(function(res){
                factory = res.data;
                defer.resolve(res);
        	}, function(err){
                console.log('Error while getting saved apps' + JSON.stringify(err));
                defer.reject(err);
        	});
        return defer.promise;
    };
    return factory;
}]);

angular.module('CorporateApp').factory('CancelSavedAppService',['$http','$q','fetchTokenNumSvc', function($http,$q,fetchTokenNumSvc){
    var factory = {};
    factory.cancelSavedApps = function(url,applicationNumber){
        var defer = $q.defer();
        $http.post(url, {}, {
        	headers:{
                'Content-Type': 'application/json',
                'Authorization':fetchTokenNumSvc.getTokenId()
        }, params:{
                'applicationNumber': applicationNumber
        }}).then(function(res){
                factory = res.data;
                defer.resolve(res);
        }, function(err){
                console.log('Error while cancelling app' + JSON.stringify(err));
                defer.reject(err);
        });
        return defer.promise;
    };
    return factory;
}]);

angular.module('CorporateApp').service('PersistenceService', function($sessionstorage, fetchAppNumberSvc){
	var thisObject = this;
	thisObject.uploadedFileList = [];
	
	thisObject.setAppNumber = function(appnum){
	    $sessionstorage.setItem('UniqueAppNum',JSON.stringify(appnum));
	};
	
	thisObject.getAppNumber = function(){
	    return fetchAppNumberSvc.getAppNumber();
	};
	
	thisObject.setAppNumToBeRetrieved = function(num){
		$sessionstorage.setItem('AppNumToBeRetrieved',JSON.stringify(num));
	};
	
	thisObject.getAppNumToBeRetrieved = function(){
	    return JSON.parse($sessionstorage.getItem('AppNumToBeRetrieved')) === null ? null : JSON.parse($sessionstorage.getItem('AppNumToBeRetrieved'));
	};
    thisObject.setPDFLocation = function(location){
        $sessionstorage.setItem('PDFLocation',JSON.stringify(location));
};
    thisObject.getPDFLocation = function(){
        try {
            if($sessionstorage.getItem('PDFLocation') === 'undefined') {
                throw {message: 'Not found'};
            } else {
                return JSON.parse($sessionstorage.getItem('PDFLocation'));
            }
        } catch(Error) {
            throw Error;
        }
    };
    thisObject.setNpsUrl = function(url){
        $sessionstorage.setItem('NpsLink',JSON.stringify(url));
};

thisObject.getNpsUrl = function(){
try {
if($sessionstorage.getItem('NpsLink') === 'undefined') {
throw {message: 'Not found'};
} else {
return JSON.parse($sessionstorage.getItem('NpsLink'));
}
} catch(Error) {
throw Error;
}
};

});

angular.module('restAPI')
.service('auraRespSvc', function() {
  var _self = this;
  _self.setResponse = function (tempInput) {
    _self.response = tempInput;
  };
  _self.getResponse = function () {
    return _self.response;
  };
});
angular.module('restAPI')
.service('submitEapplySvc', function($http, $q, fetchTokenNumSvc, mapAppData){
  var _self = this;
  _self.submitObj = function (url) {
    var defer = $q.defer();
    $http.post(url, mapAppData.mapObj(),
    {headers:{'Authorization':  fetchTokenNumSvc.getTokenId()}}).then(function(response) {
      defer.resolve(response);
    }, function(response) {
      defer.reject(response);
    });
    return defer.promise;
  };
});
angular.module('restAPI')
.service('mapAppData',['auraRespSvc', function(auraRespSvc) {
  var _self = this;
  _self.mapObj = function() {
    _self.inputDetails = angular.copy(auraRespSvc.getResponse()) || {};

//    _self.mapObj.contactPrefTime  = 'Morning (9am \55 12pm)';
    _self.mapObj.contactType = 'Mobile';

//    if( _self.inputDetails.contactPrefTime === '2'){
//      _self.mapObj.contactPrefTime  = 'Afternoon (12pm \55 6pm)';
//    }
    if(_self.inputDetails.contactType === '2') {
      _self.mapObj.contactType = 'Home';
    } else if(_self.inputDetails.contactType === '3') {
      _self.mapObj.contactType = 'Work';
    }

    angular.extend(_self.inputDetails, _self.mapObj);
    return _self.inputDetails;
  };
}]);
angular.module('CorporateApp').factory('$fakeStorage', [
 function(){
     function FakeStorage() {}
     FakeStorage.prototype.setItem = function (key, value) {
         this[key] = value;
     };
     FakeStorage.prototype.getItem = function (key) {
         return typeof this[key] === 'undefined' ? null : this[key];
         };
         FakeStorage.prototype.removeItem = function (key) {
             this[key] = undefined;
         };
         FakeStorage.prototype.clear = function(){
             for (var key in this) {
                 if( this.hasOwnProperty(key) )
                 {
                     this.removeItem(key);
                 }
             }
         };
         FakeStorage.prototype.key = function(index){
             return Object.keys(this)[index];
         };
         return new FakeStorage();
     }
 ]);

angular.module('CorporateApp').factory('$sessionstorage', ['$window', '$fakeStorage', function($window, $fakeStorage){
     function isStorageSupported(storageName)
     {
         var testKey = 'test',
             storage = $window[storageName];
         try
         {
             storage.setItem(testKey, '1');
             storage.removeItem(testKey);
             return true;
         }
         catch (error)
         {
             return false;
         }
     }
     var storage = isStorageSupported('sessionStorage') ? $window.sessionStorage : $fakeStorage;
         return {
             set: function(key, value) {
                 storage.setItem(key, value);
             },
             get: function(key, defaultValue) {
                 return storage.getItem(key) || defaultValue;
             },
             setItem: function(key, value) {
                 storage.setItem(key, value);
             },
             getItem: function(key) {
                 return storage.getItem(key);
             },
             remove: function(key){
                 storage.removeItem(key);
             },
             clear: function() {
                 storage.clear();
             },
             key: function(index){
                 storage.key(index);
             }
         };
     }
 ]);
angular.module('CorporateApp').factory('RetrieveAppDetailsService',['$http','$q','fetchTokenNumSvc', function($http,$q,fetchTokenNumSvc){
    var factory = {};
    factory.retrieveAppDetails = function(url,applicationNumber){
        var defer = $q.defer();
        $http.get(url, {
        	headers:{
                'Content-Type': 'application/json',
                'Authorization':fetchTokenNumSvc.getTokenId()
        	},params:{
                'applicationNumber': applicationNumber
        	}}).then(function(res){
                        factory = res.data;
                        defer.resolve(res);
        	},function(err){
                        console.log('Error while retrieving application ' + JSON.stringify(err));
                        defer.reject(err);
        	});
        return defer.promise;
    };
    return factory;
}]);
/* Change for Duplicate Application UW message */
angular.module('CorporateApp').factory('CheckUWDuplicateAppService',['$http','$q','fetchTokenNumSvc', function($http,$q,fetchTokenNumSvc){
    var factory = {};
    factory.checkDuplicateApps= function(url,fundCode,clientRefNo,manageTypeCC,dob,firstName,lastName){
        var defer = $q.defer();
        $http.post(url, {}, {
        	headers:{
                'Content-Type': 'application/json',
                'Authorization':fetchTokenNumSvc.getTokenId()
        	},params:{
            	'fundCode': fundCode,
                'clientRefNo': clientRefNo,
                'manageTypeCC':manageTypeCC,
                'dob': dob,
                'firstName':firstName,
                'lastName': lastName
        	}}).then(function(res){
                factory = res.data;
                defer.resolve(res);
        	},function(err){
                console.log('Error in UW duplicate application ' + JSON.stringify(err));
                defer.reject(err);
        	});
        return defer.promise;
    };
    return factory;
}]);
angular.module('restAPI')
.service('extendAppModel',['fetchAppNumberSvc', function( fetchAppNumberSvc){
    var _self = this;
    _self.extendObj = function(dataObj) {
      /*_self.inputDetails = fetchPersoanlDetailSvc.getMemberDetails() || {};
      _self.personalDetails = _self.inputDetails.personalDetails || {};
      _self.contactDetails = _self.inputDetails.contactDetails || {};*/

      if(dataObj) {
          _self.dataObj = dataObj;
      }/* else {
        _self.dataObj = {occupationDetails:{}};
        _self.dataObj.email = _self.contactDetails.emailAddress;
        _self.dataObj.contactType = _self.contactDetails.prefContact;
        _self.dataObj.contactPrefTime = _self.contactDetails.prefContactTime;
        _self.dataObj.firstName = _self.personalDetails.firstName;
        _self.dataObj.lastName = _self.personalDetails.lastName;
        _self.dataObj.name = _self.personalDetails.firstName+' '+_self.personalDetails.lastName;
        _self.dataObj.appNum = parseInt(fetchAppNumberSvc.getAppNumber());
      }*/
      
      return _self.dataObj;
    };
}]);

angular.module('restAPI')
.service('fetchAuraTransferData', function($http, $q, auraTransferInitiateSvc, auraInputSvc, fetchTokenNumSvc){
  var _self = this;
  _self.requestObj = function (url) {
    var defer = $q.defer();
    $http.post(url, auraInputSvc, {headers:{'Authorization':  fetchTokenNumSvc.getTokenId()}}).then(function(response) {
      defer.resolve(response);
    }, function(response) {
      defer.reject(response);
    });
    return defer.promise;
  };
});

angular.module('restAPI')
.service('auraTransferInitiateSvc', function() {
  var _self = this;
  _self.setInput = function (tempInput) {
  _self.input = tempInput;
  };
  _self.getInput = function () {
  return _self.input;
  };
});

angular.module('restAPI')
.service('auraPostSvc', function($http, $q,auraRespSvc, fetchTokenNumSvc){
  var _self = this;
  _self.reqObj = function (url) {
    var defer = $q.defer();
    $http.post(url,auraRespSvc.getResponse(),
    {headers:{'Authorization':  fetchTokenNumSvc.getTokenId()}}).then(function(response) {

      defer.resolve(response);

    }, function(response) {
      defer.reject(response);
    });
    return defer.promise;
  };
});

angular.module('restAPI')
.service('clientMatchSvc', function($http, $q, auraInputSvc, fetchTokenNumSvc){

  var _self = this;
 _self.reqObj = function (url) {

    var defer = $q.defer();

    $http.post(url, {firstName:auraInputSvc.getFirstName(), surName:auraInputSvc.getLastName(), dob:auraInputSvc.getDob()}).then(function(response) {

      defer.resolve(response);

    }, function(response) {
      defer.reject(response);
    });
    return defer.promise;
  };

});

angular.module('restAPI')
.service('auraInputSvc', function() {

  var _self = this;
 _self.setFund = function (tempInput) {
    _self.fund = tempInput;
  };
   _self.getFund = function () {
     return _self.fund;
  };
   _self.setMode = function (tempInput) {
                   _self.mode = tempInput;
  };
   _self.getMode = function () {
     return _self.mode;
  };
   _self.setName = function (tempInput) {
                   _self.name = tempInput;
  };
   _self.getName = function () {
     return _self.name;
  };
   _self.setAge = function (tempInput) {
                   _self.age = tempInput;
  };
   _self.getAge = function () {
     return _self.age;
  };
    _self.setDeathAmt = function (tempInput) {
                   _self.deathAmt = tempInput;
  };
   _self.getDeathAmt = function () {
     return _self.deathAmt;
  };

    _self.setTpdAmt = function (tempInput) {
                   _self.tpdAmt = tempInput;
  };
   _self.getTpdAmt = function () {
     return _self.tpdAmt;
  };

    _self.setIpAmt = function (tempInput) {
                   _self.ipAmt = tempInput;
  };
   _self.getIpAmt = function () {
     return _self.ipAmt;
  };

    _self.setWaitingPeriod = function (tempInput) {
                   _self.waitingPeriod = tempInput;
  };
   _self.getWaitingPeriod = function () {
     return _self.waitingPeriod;
  };

    _self.setBenefitPeriod = function (tempInput) {
                   _self.benefitPeriod = tempInput;
  };
   _self.getBenefitPeriod = function () {
     return _self.benefitPeriod;
  };
   _self.setAppnumber = function (tempInput) {
                   _self.appnumber = tempInput;
  };
   _self.getAppnumber = function () {
     return _self.appnumber;
  };

    _self.setIndustryOcc = function (tempInput) {
                   _self.industryOcc = tempInput;
  };
   _self.getIndustryOcc = function () {
     return _self.industryOcc;
  };

    _self.setGender = function (tempInput) {
                   _self.gender = tempInput;
  };
   _self.getGender = function () {
     return _self.gender;
  };

    _self.setCountry = function (tempInput) {
                   _self.country = tempInput;
  };
   _self.getCountry = function () {
     return _self.country;
  };

    _self.setSalary = function (tempInput) {
                   _self.salary = tempInput;
  };
   _self.getSalary = function () {
     return _self.salary;
  };

    _self.setFifteenHr = function (tempInput) {
                   _self.fifteenHr = tempInput;
  };
   _self.getFifteenHr = function () {
     return _self.fifteenHr;
  };

    _self.setCompanyId = function (tempInput) {
                   _self.companyId = tempInput;
  };
   _self.getCompanyId = function () {
     return _self.companyId;
  };

    _self.setClientname = function (tempInput) {
                   _self.clientname = tempInput;
  };
   _self.getClientname = function () {
     return _self.clientname;
  };

    _self.setDob = function (tempInput) {
                   _self.dob = tempInput;
  };
   _self.getDob = function () {
     return _self.dob;
  };

    _self.setFirstName = function (tempInput) {
                   _self.firstName = tempInput;
  };
   _self.getFirstName = function () {
     return _self.firstName;
  };
   _self.setLastName = function (tempInput) {
                   _self.lastName = tempInput;
  };
   _self.getLastName = function () {
     return _self.lastName;
  };
   _self.setSpecialTerm = function (tempInput) {
                   _self.specialTerm = tempInput;
  };
   _self.getSpecialTerm = function () {
     return _self.specialTerm;
  };
   _self.setExistingTerm = function (tempInput) {
                   _self.existingTerm = tempInput;
  };
   _self.getExistingTerm = function () {
     return _self.existingTerm;
  };

    _self.setMemberType = function (tempInput) {
                   _self.memberType = tempInput;
  };
   _self.getMemberType = function () {
     return _self.memberType;
  };
   _self.setSmoker = function (tempInput) {
	    _self.smoker = tempInput;
	  };
	   _self.getSmoker = function () {
	     return _self.smoker;
	  };

 });

angular.module('restAPI')
.service('submitAuraSvc', function($http, $q, auraInputSvc, fetchTokenNumSvc){
  var _self = this;
  _self.requestObj = function (url) {
    var defer = $q.defer();

    $http.post(url,auraInputSvc,
    {headers:{'Authorization':  fetchTokenNumSvc.getTokenId()}}).then(function(response) {

    defer.resolve(response);
    }, function(response) {
    defer.reject(response);
    });
    return defer.promise;
  };
});



angular.module('restAPI')
.service('auraRespSvc', function() {
  var _self = this;
  _self.setResponse = function (tempInput) {
    _self.response = tempInput;
  };
  _self.getResponse = function () {
    return _self.response;
  };
});

angular.module('restAPI')
.service('submitEapplySvc', function($http, $q, fetchTokenNumSvc, mapAppData){
  var _self = this;
  _self.submitObj = function (url) {
    var defer = $q.defer();
    $http.post(url, mapAppData.mapObj(),
    {headers:{'Authorization':  fetchTokenNumSvc.getTokenId()}}).then(function(response) {
      defer.resolve(response);
    }, function(response) {
      defer.reject(response);
    });
    return defer.promise;
  };
});