package au.com.metlife.eapply.calculator.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InsurCalculatorVO implements Serializable{
	
private Integer age;
	
	private String gender;
	
	private String fifteenHrsQAns;
	
	private String fundId;
	
	private String demoFundId;
	
	private Integer retirementAge;
	
	private Integer updatedRetirementAge;
	
	private BigDecimal inflationRate;
	
	private BigDecimal updatedInflationRate;
	
	private BigDecimal realInterestRate;
	
	private BigDecimal updatedRealInterestRate;
	
	private BigDecimal superContribRate;
	
	private BigDecimal updatedSuperContribRate;
	
	private BigDecimal ipBenefitPaidToSA;
	
	private BigDecimal updatedIpBenefitPaidToSA;
	
	private BigDecimal ipReplaceRatio;
	
	private BigDecimal updatedIpReplaceRatio;
	
	private BigDecimal funeralCost;
	
	private BigDecimal updatedFuneralCost;
	
	private BigDecimal annualMedAndNursingCost;
	
	private BigDecimal updatedAnnualMedAndNursingCost;
	
	private Integer dependentSuppAge;
	
	private Integer updatedDependentSuppAge;
	
	private String ipWaitingPeriod;
	
	private String ipBenefitPeriod;
	
	private String industryCode;
	
	private String industryOcc;
	
	private BigDecimal grossSalVal;
	
	private String incomeSalPeriodVal;
	
	private BigDecimal partnerGrossSalVal;
	
	private String incomePartnerSalPeriodVal;
	
	private BigDecimal mortageRepaymentVal;
	
	private String mortageRepaymentPeriodVal;
	
	private BigDecimal expensesVal;
	
	private String expenses;
	
	private BigDecimal debtVal;
	
	private boolean disableExpenses;
	
	private boolean disableDebits;
	
	private String q8_PROFF_Q_Value=null;
	
	private String q9_PROFF_Q_Value=null;
	
	private String q10_PROFF_Q_Value=null;
	
	private BigDecimal recommendedIpCover;
	
	private BigDecimal requiredIpCover;
	
	private BigDecimal recommendedIpCoverOld;
	
	private BigDecimal recommendedDeathCover;
	
	private BigDecimal recommendedDeathCoverOld;
	
	private BigDecimal requiredDeathCover;
	
	private BigDecimal recommendedTpdCover;
	
	private BigDecimal recommendedTpdCoverOld;
	
	private BigDecimal requiredTpdCover;
	
	private BigDecimal deathCoverAmount;
	
	private BigDecimal tpdCoverAmount;
	
	private BigDecimal ipCoverAmount;
	
	private BigDecimal deathCost = BigDecimal.ZERO;
	
	private BigDecimal tpdCost= BigDecimal.ZERO;
	
	private BigDecimal ipCost= BigDecimal.ZERO;
	
	private BigDecimal totalCost= BigDecimal.ZERO;
	
	private List<KidsVO> kidsList= new ArrayList<KidsVO>();
	
	private String depenChildVal;
	
	private boolean enabledButton;
	
	private String estimatedPremiumIncmProtecVal="0.00";
	
	/*Used for SUper annuation calculation for TPD and DEATH*/
	
	private List<SuperAnnuationVO> superAnnuationList=null;
	
	private BigDecimal totalSuperAmt;
	
	private Map<String, BigDecimal> minSuperMap=null;
	
	/*Used for SUper annuation calculation for TPD and DEATH*/
	
	/*Used for replace expenses calculation for TPD*/
	
	private BigDecimal adultFactor;
	
	private BigDecimal childFactor;
	
	private BigDecimal miscFactor;
	
	private boolean livingWithSpouse;
	
	private String livingWithSpouseVal;
	
	private BigDecimal ratioOfCover;
	
	private BigDecimal otherExpensesForTPD;
	
	private BigDecimal childExpenseRatio;
	
	private BigDecimal adultExpenseRatio;
	
	private BigDecimal miscExpenseRatio;
	
	private BigDecimal averageTaxRate;
	
	private BigDecimal ipForTPD;
	
	private boolean excludeIP;
	
	private BigDecimal ttlTPDExpenses;
	
	private BigDecimal ttlTpdNpvAmt;
	
	private int kidsCount;
	
	private BigDecimal childWeight;
	
	private BigDecimal adultWeight;
	
	private BigDecimal miscWeight;
	
	private BigDecimal totalWeight;
	
	private int ipBenefitPerVal;
	
	private BigDecimal ttlDeathNpvAmt;
	
	private BigDecimal ttlNursingNpvAmt;
	
	/*Used for replace expenses calculation for TPD*/
	
	private boolean includeNursingCosts;
	
	private List<ExpensesVO> otherExpensesList=null;
	
	private List<ExpensesVO> totalDebitsList=null;
	
	private String deathInsuranceCategory;
	
	private String tpdInsuranceCategory;
	
	private String ipInsuranceCategory;
	
	private String industryName;
	
	private String deathCoverType=null;
	
	private String tpdCoverType=null;
	
	private String ipCoverType=null;
	
	private String askmanual=null;
	
	private String spendTimeOutSide=null;
	
	private String workDuties2=null;
	
	private String tertiaryQualification=null;
	
	private String otherOccupation = null;
	
	private boolean deathRecommendedCvrExceeded=false;
	
	private boolean tpdRecommendedCvrExceeded=false;
	
	private boolean ipRecommendedCvrExceeded=false;
	
	private boolean deathRecommendedLessThanTpd=false;
	
	private boolean ipNotEligibleDueToAge=false;
	
	private boolean ipNotEligbileDueToSal=false;
	
	private BigDecimal ipThresholdLimit=null;
	
	private boolean checkBoxVal=false;
	
	private boolean ipRequriedEnterdByUser=false;
	
	private boolean ipReuiredUnitsRounded=false;
	
	private boolean displayCostInPdf=false;
	
	//Added for RWD functionality
	private boolean genderMale=false;
	
	private boolean genderFemale=false;
	
	private boolean partnerspouseYes=false;
	
	private boolean partnerspouseNo=false;
	
	private boolean dependentchildYes=false;
	
	private boolean dependentchildNo=false;
	
	private boolean fifteenhourYes=false;
	
	private boolean fifteenhourNo=false;
	
	private boolean hazardEnvYes=false;
	
	private boolean hazardEnvNo=false;
	
	private boolean spendTimeOutSideYes=false;
	
	private boolean spendTimeOutSideNo=false;
	
	private boolean askmanualQuestionYes= false;
	
	private boolean askmanualQuestionNo= false;
	
	private boolean workDuties2Yes=false;
	
	private boolean workDuties2No=false;
	
	private boolean  tertiaryQualificationYes=false;
	
	private boolean  tertiaryQualificationNo=false;
	
	private boolean  q8_PROFF_Q_ValueYes=false;
	
	private boolean  q8_PROFF_Q_ValueNo=false;
	
	private boolean  q9_PROFF_Q_ValueYes=false;
	
	private boolean  q9_PROFF_Q_ValueNo=false;
	
	private boolean q10_PROFF_Q_ValueYes=false;
	
	private boolean q10_PROFF_Q_ValueNo=false;
	
	private String mortgageQue = null;
	
	private boolean mortgageYes = false;
	
	private boolean mortgageNo = false;
	
	private String state = null;
	
	private String notManualWorkQue = null;
	
	private boolean notManualWorkYes = false;
	
	private boolean notManualWorkNo = false;
	
	private String profByGovntQue = null;
	
	private boolean profByGovntYes = false;
	
	private boolean profByGovntNo = false;
	
	private String managmntRoleQue = null;
	
	private boolean managmntRoleYes = false;
	
	private boolean managmntRoleNo = false;
	
	private Boolean disableIpSection = false;
	
	public Boolean getDisableIpSection() {
		return disableIpSection;
	}


	public void setDisableIpSection(Boolean disableIpSection) {
		this.disableIpSection = disableIpSection;
	}


	public boolean isManagmntRoleNo() {
		return managmntRoleNo;
	}


	public void setManagmntRoleNo(boolean managmntRoleNo) {
		this.managmntRoleNo = managmntRoleNo;
	}


	public String getManagmntRoleQue() {
		return managmntRoleQue;
	}


	public void setManagmntRoleQue(String managmntRoleQue) {
		this.managmntRoleQue = managmntRoleQue;
	}


	public boolean isManagmntRoleYes() {
		return managmntRoleYes;
	}


	public void setManagmntRoleYes(boolean managmntRoleYes) {
		this.managmntRoleYes = managmntRoleYes;
	}


	public boolean isNotManualWorkNo() {
		return notManualWorkNo;
	}


	public void setNotManualWorkNo(boolean notManualWorkNo) {
		this.notManualWorkNo = notManualWorkNo;
	}


	public String getNotManualWorkQue() {
		return notManualWorkQue;
	}


	public void setNotManualWorkQue(String notManualWorkQue) {
		this.notManualWorkQue = notManualWorkQue;
	}


	public boolean isNotManualWorkYes() {
		return notManualWorkYes;
	}


	public void setNotManualWorkYes(boolean notManualWorkYes) {
		this.notManualWorkYes = notManualWorkYes;
	}


	public boolean isProfByGovntNo() {
		return profByGovntNo;
	}


	public void setProfByGovntNo(boolean profByGovntNo) {
		this.profByGovntNo = profByGovntNo;
	}


	public String getProfByGovntQue() {
		return profByGovntQue;
	}


	public void setProfByGovntQue(String profByGovntQue) {
		this.profByGovntQue = profByGovntQue;
	}


	public boolean isProfByGovntYes() {
		return profByGovntYes;
	}


	public void setProfByGovntYes(boolean profByGovntYes) {
		this.profByGovntYes = profByGovntYes;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public boolean isDisplayCostInPdf() {
		return displayCostInPdf;
	}


	public void setDisplayCostInPdf(boolean displayCostInPdf) {
		this.displayCostInPdf = displayCostInPdf;
	}


	public BigDecimal getIpThresholdLimit() {
		return ipThresholdLimit;
	}


	public void setIpThresholdLimit(BigDecimal ipThresholdLimit) {
		this.ipThresholdLimit = ipThresholdLimit;
	}


	public boolean isIpNotEligbileDueToSal() {
		return ipNotEligbileDueToSal;
	}


	public void setIpNotEligbileDueToSal(boolean ipNotEligbileDueToSal) {
		this.ipNotEligbileDueToSal = ipNotEligbileDueToSal;
	}


	public boolean isIpNotEligibleDueToAge() {
		return ipNotEligibleDueToAge;
	}


	public void setIpNotEligibleDueToAge(boolean ipNotEligibleDueToAge) {
		this.ipNotEligibleDueToAge = ipNotEligibleDueToAge;
	}


	public boolean isDeathRecommendedLessThanTpd() {
		return deathRecommendedLessThanTpd;
	}


	public void setDeathRecommendedLessThanTpd(boolean deathRecommendedLessThanTpd) {
		this.deathRecommendedLessThanTpd = deathRecommendedLessThanTpd;
	}


	public boolean isIpRecommendedCvrExceeded() {
		return ipRecommendedCvrExceeded;
	}


	public void setIpRecommendedCvrExceeded(boolean ipRecommendedCvrExceeded) {
		this.ipRecommendedCvrExceeded = ipRecommendedCvrExceeded;
	}


	public boolean isTpdRecommendedCvrExceeded() {
		return tpdRecommendedCvrExceeded;
	}


	public void setTpdRecommendedCvrExceeded(boolean tpdRecommendedCvrExceeded) {
		this.tpdRecommendedCvrExceeded = tpdRecommendedCvrExceeded;
	}


	public String getAskmanual() {
		return askmanual;
	}


	public void setAskmanual(String askmanual) {
		this.askmanual = askmanual;
	}


	public String getSpendTimeOutSide() {
		return spendTimeOutSide;
	}


	public void setSpendTimeOutSide(String spendTimeOutSide) {
		this.spendTimeOutSide = spendTimeOutSide;
	}


	public String getTertiaryQualification() {
		return tertiaryQualification;
	}


	public void setTertiaryQualification(String tertiaryQualification) {
		this.tertiaryQualification = tertiaryQualification;
	}


	public String getWorkDuties2() {
		return workDuties2;
	}


	public void setWorkDuties2(String workDuties2) {
		this.workDuties2 = workDuties2;
	}


	public String getIndustryName() {
		return industryName;
	}


	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}


	public List<ExpensesVO> getOtherExpensesList() {
		return otherExpensesList;
	}


	public void setOtherExpensesList(List<ExpensesVO> otherExpensesList) {
		this.otherExpensesList = otherExpensesList;
	}


	public List<ExpensesVO> getTotalDebitsList() {
		return totalDebitsList;
	}


	public void setTotalDebitsList(List<ExpensesVO> totalDebitsList) {
		this.totalDebitsList = totalDebitsList;
	}


	public boolean isIncludeNursingCosts() {
		return includeNursingCosts;
	}


	public void setIncludeNursingCosts(boolean includeNursingCosts) {
		this.includeNursingCosts = includeNursingCosts;
	}


	public int getIpBenefitPerVal() {
		if(this.ipBenefitPeriod!=null){
			if(this.ipBenefitPeriod.equalsIgnoreCase("2 Years")){
				ipBenefitPerVal=2;
			}else if(this.ipBenefitPeriod.equalsIgnoreCase("5 Years")){
				ipBenefitPerVal=5;
			}else if(this.ipBenefitPeriod.equalsIgnoreCase("Age 65")){
				if(this.age!=null){
					/*SRKR: if it is 2 years/ 5 Yeears/X years we need to use the same value. 
					 * if it is upt to age65 or 67 we need to subract the currente age from that to caluclate the actual benefit
					 * eg. currente age is 35 and benefit selected is Age 65 then ipBenefitvalu should be 65-35
					 * */
					ipBenefitPerVal=(65-this.age);
				}
			}
		}
		return ipBenefitPerVal;
	}


	public void setIpBenefitPerVal(int ipBenefitPerVal) {
		this.ipBenefitPerVal = ipBenefitPerVal;
	}


	public BigDecimal getTotalWeight() {
		return totalWeight;
	}


	public void setTotalWeight(BigDecimal totalWeight) {
		this.totalWeight = totalWeight;
	}


	public BigDecimal getAdultWeight() {
		return adultWeight;
	}


	public void setAdultWeight(BigDecimal adultWeight) {
		this.adultWeight = adultWeight;
	}


	public BigDecimal getChildWeight() {
		return childWeight;
	}


	public void setChildWeight(BigDecimal childWeight) {
		this.childWeight = childWeight;
	}


	public BigDecimal getMiscWeight() {
		return miscWeight;
	}


	public void setMiscWeight(BigDecimal miscWeight) {
		this.miscWeight = miscWeight;
	}


	public int getKidsCount() {
		return kidsCount;
	}


	public void setKidsCount(int kidsCount) {
		this.kidsCount = kidsCount;
	}


	public BigDecimal getAdultFactor() {
		return adultFactor;
	}


	public void setAdultFactor(BigDecimal adultFactor) {
		this.adultFactor = adultFactor;
	}


	public BigDecimal getChildFactor() {
		return childFactor;
	}


	public void setChildFactor(BigDecimal childFactor) {
		this.childFactor = childFactor;
	}


	public BigDecimal getMiscFactor() {
		return miscFactor;
	}


	public void setMiscFactor(BigDecimal miscFactor) {
		this.miscFactor = miscFactor;
	}


	public InsurCalculatorVO(){
		minSuperMap=new HashMap<String, BigDecimal>();
		minSuperMap.put("2013", new BigDecimal("9.25"));
		minSuperMap.put("2014", new BigDecimal("9.50"));
		minSuperMap.put("2015", new BigDecimal("9.50"));
		minSuperMap.put("2016", new BigDecimal("9.50"));
		minSuperMap.put("2017", new BigDecimal("9.50"));
		minSuperMap.put("2018", new BigDecimal("9.50"));
		minSuperMap.put("2019", new BigDecimal("9.50"));
		minSuperMap.put("2020", new BigDecimal("9.50"));
		minSuperMap.put("2021", new BigDecimal("10.00"));		
		minSuperMap.put("2022", new BigDecimal("10.50"));
		minSuperMap.put("2023", new BigDecimal("11.00"));
		minSuperMap.put("2024", new BigDecimal("11.50"));
		minSuperMap.put("2025", new BigDecimal("12.00"));
		minSuperMap.put("2026", new BigDecimal("12.00"));		
		minSuperMap.put("DEFAULT", new BigDecimal("12.00"));
	}


	public BigDecimal getDeathCost() {
		return deathCost;
	}

	public void setDeathCost(BigDecimal deathCost) {
		this.deathCost = deathCost;
	}

	public BigDecimal getIpCost() {
		return ipCost;
	}

	public void setIpCost(BigDecimal ipCost) {
		this.ipCost = ipCost;
	}

	public BigDecimal getRecommendedDeathCover() {
		return recommendedDeathCover;
	}

	public void setRecommendedDeathCover(BigDecimal recommendedDeathCover) {
		this.recommendedDeathCover = recommendedDeathCover;
	}

	public BigDecimal getRecommendedTpdCover() {
		return recommendedTpdCover;
	}

	public void setRecommendedTpdCover(BigDecimal recommendedTpdCover) {
		this.recommendedTpdCover = recommendedTpdCover;
	}

	public BigDecimal getRequiredDeathCover() {
		return requiredDeathCover;
	}

	public void setRequiredDeathCover(BigDecimal requiredDeathCover) {
		this.requiredDeathCover = requiredDeathCover;
	}

	public BigDecimal getRequiredTpdCover() {
		return requiredTpdCover;
	}

	public void setRequiredTpdCover(BigDecimal requiredTpdCover) {
		this.requiredTpdCover = requiredTpdCover;
	}

	public BigDecimal getTpdCost() {
		return tpdCost;
	}

	public void setTpdCost(BigDecimal tpdCost) {
		this.tpdCost = tpdCost;
	}

	public BigDecimal getDebtVal() {
		return debtVal;
	}

	public void setDebtVal(BigDecimal debtVal) {
		this.debtVal = debtVal;
	}

	public String getExpenses() {
		return expenses;
	}

	public void setExpenses(String expenses) {
		this.expenses = expenses;
	}

	public BigDecimal getExpensesVal() {
		return expensesVal;
	}

	public void setExpensesVal(BigDecimal expensesVal) {
		this.expensesVal = expensesVal;
	}

	public BigDecimal getGrossSalVal() {
		return grossSalVal;
	}

	public void setGrossSalVal(BigDecimal grossSalVal) {
		this.grossSalVal = grossSalVal;
	}

	public String getIncomePartnerSalPeriodVal() {
		return incomePartnerSalPeriodVal;
	}

	public void setIncomePartnerSalPeriodVal(String incomePartnerSalPeriodVal) {
		this.incomePartnerSalPeriodVal = incomePartnerSalPeriodVal;
	}

	public String getIncomeSalPeriodVal() {
		return incomeSalPeriodVal;
	}

	public void setIncomeSalPeriodVal(String incomeSalPeriodVal) {
		this.incomeSalPeriodVal = incomeSalPeriodVal;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getIndustryOcc() {
		return industryOcc;
	}

	public void setIndustryOcc(String industryOcc) {
		this.industryOcc = industryOcc;
	}

	public String getIpBenefitPeriod() {
		return ipBenefitPeriod;
	}


	public void setIpBenefitPeriod(String ipBenefitPeriod) {
		this.ipBenefitPeriod = ipBenefitPeriod;
	}


	public String getIpWaitingPeriod() {
		return ipWaitingPeriod;
	}


	public void setIpWaitingPeriod(String ipWaitingPeriod) {
		this.ipWaitingPeriod = ipWaitingPeriod;
	}


	public String getMortageRepaymentPeriodVal() {
		return mortageRepaymentPeriodVal;
	}

	public void setMortageRepaymentPeriodVal(String mortageRepaymentPeriodVal) {
		this.mortageRepaymentPeriodVal = mortageRepaymentPeriodVal;
	}

	public BigDecimal getMortageRepaymentVal() {
		return mortageRepaymentVal;
	}

	public void setMortageRepaymentVal(BigDecimal mortageRepaymentVal) {
		this.mortageRepaymentVal = mortageRepaymentVal;
	}

	public BigDecimal getPartnerGrossSalVal() {
		return partnerGrossSalVal;
	}

	public void setPartnerGrossSalVal(BigDecimal partnerGrossSalVal) {
		this.partnerGrossSalVal = partnerGrossSalVal;
	}

	public BigDecimal getAnnualMedAndNursingCost() {
		return annualMedAndNursingCost;
	}

	public void setAnnualMedAndNursingCost(BigDecimal annualMedAndNursingCost) {
		this.annualMedAndNursingCost = annualMedAndNursingCost;
	}

	public Integer getDependentSuppAge() {
		return dependentSuppAge;
	}

	public void setDependentSuppAge(Integer dependentSuppAge) {
		this.dependentSuppAge = dependentSuppAge;
	}

	public String getFundId() {
		return fundId;
	}

	public void setFundId(String fundId) {
		this.fundId = fundId;
	}

	public BigDecimal getFuneralCost() {
		return funeralCost;
	}

	public void setFuneralCost(BigDecimal funeralCost) {
		this.funeralCost = funeralCost;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public BigDecimal getInflationRate() {
		return inflationRate;
	}

	public void setInflationRate(BigDecimal inflationRate) {
		this.inflationRate = inflationRate;
	}

	public BigDecimal getIpBenefitPaidToSA() {
		return ipBenefitPaidToSA;
	}

	public void setIpBenefitPaidToSA(BigDecimal ipBenefitPaidToSA) {
		this.ipBenefitPaidToSA = ipBenefitPaidToSA;
	}

	public BigDecimal getIpReplaceRatio() {
		return ipReplaceRatio;
	}

	public void setIpReplaceRatio(BigDecimal ipReplaceRatio) {
		this.ipReplaceRatio = ipReplaceRatio;
	}

	public BigDecimal getRealInterestRate() {
		return realInterestRate;
	}

	public void setRealInterestRate(BigDecimal realInterestRate) {
		this.realInterestRate = realInterestRate;
	}

	public Integer getRetirementAge() {
		return retirementAge;
	}

	public void setRetirementAge(Integer retirementAge) {
		this.retirementAge = retirementAge;
	}

	public BigDecimal getSuperContribRate() {
		return superContribRate;
	}

	public void setSuperContribRate(BigDecimal superContribRate) {
		this.superContribRate = superContribRate;
	}

	public BigDecimal getRecommendedIpCover() {
		return recommendedIpCover;
	}

	public void setRecommendedIpCover(BigDecimal recommendedIpCover) {
		this.recommendedIpCover = recommendedIpCover;
	}

	public BigDecimal getRequiredIpCover() {
		return requiredIpCover;
	}

	public void setRequiredIpCover(BigDecimal requiredIpCover) {
		this.requiredIpCover = requiredIpCover;
	}

	public List<SuperAnnuationVO> getSuperAnnuationList() {
		return superAnnuationList;
	}

	public void setSuperAnnuationList(List<SuperAnnuationVO> superAnnuationList) {
		this.superAnnuationList = superAnnuationList;
	}

	public BigDecimal getTotalSuperAmt() {
		return totalSuperAmt;
	}

	public void setTotalSuperAmt(BigDecimal totalSuperAmt) {
		this.totalSuperAmt = totalSuperAmt;
	}


	public Map<String, BigDecimal> getMinSuperMap() {
		return minSuperMap;
	}


	public void setMinSuperMap(Map<String, BigDecimal> minSuperMap) {
		this.minSuperMap = minSuperMap;
	}


	public BigDecimal getAverageTaxRate() {
		return averageTaxRate;
	}


	public void setAverageTaxRate(BigDecimal averageTaxRate) {
		this.averageTaxRate = averageTaxRate;
	}


	public BigDecimal getChildExpenseRatio() {
		return childExpenseRatio;
	}


	public void setChildExpenseRatio(BigDecimal childExpenseRatio) {
		this.childExpenseRatio = childExpenseRatio;
	}


	public boolean isExcludeIP() {
		return excludeIP;
	}


	public void setExcludeIP(boolean excludeIP) {
		this.excludeIP = excludeIP;
	}


	public BigDecimal getIpForTPD() {
		return ipForTPD;
	}


	public void setIpForTPD(BigDecimal ipForTPD) {
		this.ipForTPD = ipForTPD;
	}


	public boolean isLivingWithSpouse() {
		if(this.livingWithSpouseVal==null || (this.livingWithSpouseVal!=null && this.livingWithSpouseVal.equalsIgnoreCase("No"))){
			livingWithSpouse=false;
		}else{
			livingWithSpouse=true;
		}
		return livingWithSpouse;
	}


	public void setLivingWithSpouse(boolean livingWithSpouse) {
		this.livingWithSpouse = livingWithSpouse;
	}


	public BigDecimal getOtherExpensesForTPD() {
		return otherExpensesForTPD;
	}


	public void setOtherExpensesForTPD(BigDecimal otherExpensesForTPD) {
		this.otherExpensesForTPD = otherExpensesForTPD;
	}


	public BigDecimal getRatioOfCover() {
		return ratioOfCover;
	}


	public void setRatioOfCover(BigDecimal ratioOfCover) {
		this.ratioOfCover = ratioOfCover;
	}


	public BigDecimal getTtlTPDExpenses() {
		return ttlTPDExpenses;
	}


	public void setTtlTPDExpenses(BigDecimal ttlTPDExpenses) {
		this.ttlTPDExpenses = ttlTPDExpenses;
	}


	public BigDecimal getTtlTpdNpvAmt() {
		return ttlTpdNpvAmt;
	}


	public void setTtlTpdNpvAmt(BigDecimal ttlTpdNpvAmt) {
		this.ttlTpdNpvAmt = ttlTpdNpvAmt;
	}


	public BigDecimal getAdultExpenseRatio() {
		return adultExpenseRatio;
	}


	public void setAdultExpenseRatio(BigDecimal adultExpenseRatio) {
		this.adultExpenseRatio = adultExpenseRatio;
	}


	public BigDecimal getMiscExpenseRatio() {
		return miscExpenseRatio;
	}


	public void setMiscExpenseRatio(BigDecimal miscExpenseRatio) {
		this.miscExpenseRatio = miscExpenseRatio;
	}


	public List<KidsVO> getKidsList() {
		return kidsList;
	}


	public void setKidsList(List<KidsVO> kidsList) {
		this.kidsList = kidsList;
	}


	public BigDecimal getTtlDeathNpvAmt() {
		return ttlDeathNpvAmt;
	}


	public void setTtlDeathNpvAmt(BigDecimal ttlDeathNpvAmt) {
		this.ttlDeathNpvAmt = ttlDeathNpvAmt;
	}


	public BigDecimal getTtlNursingNpvAmt() {
		return ttlNursingNpvAmt;
	}


	public void setTtlNursingNpvAmt(BigDecimal ttlNursingNpvAmt) {
		this.ttlNursingNpvAmt = ttlNursingNpvAmt;
	}


	public String getLivingWithSpouseVal() {
		return livingWithSpouseVal;
	}


	public void setLivingWithSpouseVal(String livingWithSpouseVal) {
		this.livingWithSpouseVal = livingWithSpouseVal;
	}


	public Integer getAge() {
		return age;
	}


	public void setAge(Integer age) {
		this.age = age;
	}
	
	public String getDepenChildVal() {
		return depenChildVal;
	}
	
	public void setDepenChildVal(String depenChildVal) {
		this.depenChildVal = depenChildVal;
	}

	public String getEstimatedPremiumIncmProtecVal() {
		return estimatedPremiumIncmProtecVal;
	}

	public void setEstimatedPremiumIncmProtecVal(
			String estimatedPremiumIncmProtecVal) {
		this.estimatedPremiumIncmProtecVal = estimatedPremiumIncmProtecVal;
	}

	public boolean isEnabledButton() {
		return enabledButton;
	}

	public void setEnabledButton(boolean enabledButton) {
		this.enabledButton = enabledButton;
	}


	public BigDecimal getTotalCost() {
		this.totalCost=(getDeathCost().add(getTpdCost())).add(getIpCost());
		return totalCost;
	}


	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}


	public String getQ10_PROFF_Q_Value() {
		return q10_PROFF_Q_Value;
	}


	public void setQ10_PROFF_Q_Value(String value) {
		q10_PROFF_Q_Value = value;
	}


	public String getQ8_PROFF_Q_Value() {
		return q8_PROFF_Q_Value;
	}


	public void setQ8_PROFF_Q_Value(String value) {
		q8_PROFF_Q_Value = value;
	}


	public String getQ9_PROFF_Q_Value() {
		return q9_PROFF_Q_Value;
	}


	public void setQ9_PROFF_Q_Value(String value) {
		q9_PROFF_Q_Value = value;
	}


	public BigDecimal getRecommendedDeathCoverOld() {
		return recommendedDeathCoverOld;
	}


	public void setRecommendedDeathCoverOld(BigDecimal recommendedDeathCoverOld) {
		this.recommendedDeathCoverOld = recommendedDeathCoverOld;
	}


	public BigDecimal getRecommendedIpCoverOld() {
		return recommendedIpCoverOld;
	}


	public void setRecommendedIpCoverOld(BigDecimal recommendedIpCoverOld) {
		this.recommendedIpCoverOld = recommendedIpCoverOld;
	}


	public BigDecimal getRecommendedTpdCoverOld() {
		return recommendedTpdCoverOld;
	}


	public void setRecommendedTpdCoverOld(BigDecimal recommendedTpdCoverOld) {
		this.recommendedTpdCoverOld = recommendedTpdCoverOld;
	}


	public boolean isDisableDebits() {
		return disableDebits;
	}


	public void setDisableDebits(boolean disableDebits) {
		this.disableDebits = disableDebits;
	}


	public boolean isDisableExpenses() {
		return disableExpenses;
	}


	public void setDisableExpenses(boolean disableExpenses) {
		this.disableExpenses = disableExpenses;
	}


	public String getDeathInsuranceCategory() {
		return deathInsuranceCategory;
	}


	public void setDeathInsuranceCategory(String deathInsuranceCategory) {
		this.deathInsuranceCategory = deathInsuranceCategory;
	}


	public String getIpInsuranceCategory() {
		return ipInsuranceCategory;
	}


	public void setIpInsuranceCategory(String ipInsuranceCategory) {
		this.ipInsuranceCategory = ipInsuranceCategory;
	}


	public String getTpdInsuranceCategory() {
		return tpdInsuranceCategory;
	}


	public void setTpdInsuranceCategory(String tpdInsuranceCategory) {
		this.tpdInsuranceCategory = tpdInsuranceCategory;
	}


	public BigDecimal getUpdatedAnnualMedAndNursingCost() {
		return updatedAnnualMedAndNursingCost;
	}


	public void setUpdatedAnnualMedAndNursingCost(
			BigDecimal updatedAnnualMedAndNursingCost) {
		this.updatedAnnualMedAndNursingCost = updatedAnnualMedAndNursingCost;
	}


	public Integer getUpdatedDependentSuppAge() {
		return updatedDependentSuppAge;
	}


	public void setUpdatedDependentSuppAge(Integer updatedDependentSuppAge) {
		this.updatedDependentSuppAge = updatedDependentSuppAge;
	}


	public BigDecimal getUpdatedFuneralCost() {
		return updatedFuneralCost;
	}


	public void setUpdatedFuneralCost(BigDecimal updatedFuneralCost) {
		this.updatedFuneralCost = updatedFuneralCost;
	}


	public BigDecimal getUpdatedInflationRate() {
		return updatedInflationRate;
	}


	public void setUpdatedInflationRate(BigDecimal updatedInflationRate) {
		this.updatedInflationRate = updatedInflationRate;
	}


	public BigDecimal getUpdatedIpBenefitPaidToSA() {
		return updatedIpBenefitPaidToSA;
	}


	public void setUpdatedIpBenefitPaidToSA(BigDecimal updatedIpBenefitPaidToSA) {
		this.updatedIpBenefitPaidToSA = updatedIpBenefitPaidToSA;
	}


	public BigDecimal getUpdatedIpReplaceRatio() {
		return updatedIpReplaceRatio;
	}


	public void setUpdatedIpReplaceRatio(BigDecimal updatedIpReplaceRatio) {
		this.updatedIpReplaceRatio = updatedIpReplaceRatio;
	}


	public BigDecimal getUpdatedRealInterestRate() {
		return updatedRealInterestRate;
	}


	public void setUpdatedRealInterestRate(BigDecimal updatedRealInterestRate) {
		this.updatedRealInterestRate = updatedRealInterestRate;
	}


	public Integer getUpdatedRetirementAge() {
		return updatedRetirementAge;
	}


	public void setUpdatedRetirementAge(Integer updatedRetirementAge) {
		this.updatedRetirementAge = updatedRetirementAge;
	}


	public BigDecimal getUpdatedSuperContribRate() {
		return updatedSuperContribRate;
	}


	public void setUpdatedSuperContribRate(BigDecimal updatedSuperContribRate) {
		this.updatedSuperContribRate = updatedSuperContribRate;
	}


	public String getDeathCoverType() {
		return deathCoverType;
	}


	public void setDeathCoverType(String deathCoverType) {
		this.deathCoverType = deathCoverType;
	}


	public String getIpCoverType() {
		return ipCoverType;
	}


	public void setIpCoverType(String ipCoverType) {
		this.ipCoverType = ipCoverType;
	}


	public String getTpdCoverType() {
		return tpdCoverType;
	}


	public void setTpdCoverType(String tpdCoverType) {
		this.tpdCoverType = tpdCoverType;
	}


	public BigDecimal getDeathCoverAmount() {
		return deathCoverAmount;
	}


	public void setDeathCoverAmount(BigDecimal deathCoverAmount) {
		this.deathCoverAmount = deathCoverAmount;
	}


	public BigDecimal getIpCoverAmount() {
		return ipCoverAmount;
	}


	public void setIpCoverAmount(BigDecimal ipCoverAmount) {
		this.ipCoverAmount = ipCoverAmount;
	}


	public BigDecimal getTpdCoverAmount() {
		return tpdCoverAmount;
	}


	public void setTpdCoverAmount(BigDecimal tpdCoverAmount) {
		this.tpdCoverAmount = tpdCoverAmount;
	}


	public String getOtherOccupation() {
		return otherOccupation;
	}


	public void setOtherOccupation(String otherOccupation) {
		this.otherOccupation = otherOccupation;
	}


	public boolean isCheckBoxVal() {
		return checkBoxVal;
	}


	public void setCheckBoxVal(boolean checkBoxVal) {
		this.checkBoxVal = checkBoxVal;
	}


	public String getDemoFundId() {
		return demoFundId;
	}


	public void setDemoFundId(String demoFundId) {
		this.demoFundId = demoFundId;
	}


	public String getFifteenHrsQAns() {
		return fifteenHrsQAns;
	}


	public void setFifteenHrsQAns(String fifteenHrsQAns) {
		this.fifteenHrsQAns = fifteenHrsQAns;
	}


	public boolean isIpRequriedEnterdByUser() {
		return ipRequriedEnterdByUser;
	}


	public void setIpRequriedEnterdByUser(boolean ipRequriedEnterdByUser) {
		this.ipRequriedEnterdByUser = ipRequriedEnterdByUser;
	}


	public boolean isIpReuiredUnitsRounded() {
		return ipReuiredUnitsRounded;
	}


	public void setIpReuiredUnitsRounded(boolean ipReuiredUnitsRounded) {
		this.ipReuiredUnitsRounded = ipReuiredUnitsRounded;
	}


	public boolean isDeathRecommendedCvrExceeded() {
		return deathRecommendedCvrExceeded;
	}


	public void setDeathRecommendedCvrExceeded(boolean deathRecommendedCvrExceeded) {
		this.deathRecommendedCvrExceeded = deathRecommendedCvrExceeded;
	}


	public boolean isDependentchildNo() {
		return dependentchildNo;
	}


	public void setDependentchildNo(boolean dependentchildNo) {
		this.dependentchildNo = dependentchildNo;
	}


	public boolean isDependentchildYes() {
		return dependentchildYes;
	}


	public void setDependentchildYes(boolean dependentchildYes) {
		this.dependentchildYes = dependentchildYes;
	}


	public boolean isFifteenhourNo() {
		return fifteenhourNo;
	}


	public void setFifteenhourNo(boolean fifteenhourNo) {
		this.fifteenhourNo = fifteenhourNo;
	}


	public boolean isFifteenhourYes() {
		return fifteenhourYes;
	}


	public void setFifteenhourYes(boolean fifteenhourYes) {
		this.fifteenhourYes = fifteenhourYes;
	}

	public boolean isHazardEnvYes() {
		return hazardEnvYes;
	}


	public void setHazardEnvYes(boolean hazardEnvYes) {
		this.hazardEnvYes = hazardEnvYes;
	}


	public boolean isHazardEnvNo() {
		return hazardEnvNo;
	}


	public void setHazardEnvNo(boolean hazardEnvNo) {
		this.hazardEnvNo = hazardEnvNo;
	}


	public boolean isGenderFemale() {
		return genderFemale;
	}


	public void setGenderFemale(boolean genderFemale) {
		this.genderFemale = genderFemale;
	}


	public boolean isGenderMale() {
		return genderMale;
	}


	public void setGenderMale(boolean genderMale) {
		this.genderMale = genderMale;
	}


	public boolean isPartnerspouseNo() {
		return partnerspouseNo;
	}


	public void setPartnerspouseNo(boolean partnerspouseNo) {
		this.partnerspouseNo = partnerspouseNo;
	}


	public boolean isPartnerspouseYes() {
		return partnerspouseYes;
	}


	public void setPartnerspouseYes(boolean partnerspouseYes) {
		this.partnerspouseYes = partnerspouseYes;
	}


	public boolean isQ10_PROFF_Q_ValueNo() {
		return q10_PROFF_Q_ValueNo;
	}


	public void setQ10_PROFF_Q_ValueNo(boolean valueNo) {
		q10_PROFF_Q_ValueNo = valueNo;
	}


	public boolean isQ10_PROFF_Q_ValueYes() {
		return q10_PROFF_Q_ValueYes;
	}


	public void setQ10_PROFF_Q_ValueYes(boolean valueYes) {
		q10_PROFF_Q_ValueYes = valueYes;
	}


	public boolean isQ8_PROFF_Q_ValueNo() {
		return q8_PROFF_Q_ValueNo;
	}


	public void setQ8_PROFF_Q_ValueNo(boolean valueNo) {
		q8_PROFF_Q_ValueNo = valueNo;
	}


	public boolean isQ8_PROFF_Q_ValueYes() {
		return q8_PROFF_Q_ValueYes;
	}


	public void setQ8_PROFF_Q_ValueYes(boolean valueYes) {
		q8_PROFF_Q_ValueYes = valueYes;
	}


	public boolean isQ9_PROFF_Q_ValueNo() {
		return q9_PROFF_Q_ValueNo;
	}


	public void setQ9_PROFF_Q_ValueNo(boolean valueNo) {
		q9_PROFF_Q_ValueNo = valueNo;
	}


	public boolean isQ9_PROFF_Q_ValueYes() {
		return q9_PROFF_Q_ValueYes;
	}


	public void setQ9_PROFF_Q_ValueYes(boolean valueYes) {
		q9_PROFF_Q_ValueYes = valueYes;
	}


	public boolean isSpendTimeOutSideNo() {
		return spendTimeOutSideNo;
	}


	public void setSpendTimeOutSideNo(boolean spendTimeOutSideNo) {
		this.spendTimeOutSideNo = spendTimeOutSideNo;
	}


	public boolean isSpendTimeOutSideYes() {
		return spendTimeOutSideYes;
	}


	public void setSpendTimeOutSideYes(boolean spendTimeOutSideYes) {
		this.spendTimeOutSideYes = spendTimeOutSideYes;
	}


	public boolean isWorkDuties2No() {
		return workDuties2No;
	}


	public void setWorkDuties2No(boolean workDuties2No) {
		this.workDuties2No = workDuties2No;
	}


	public boolean isWorkDuties2Yes() {
		return workDuties2Yes;
	}


	public void setWorkDuties2Yes(boolean workDuties2Yes) {
		this.workDuties2Yes = workDuties2Yes;
	}


	public boolean isTertiaryQualificationNo() {
		return tertiaryQualificationNo;
	}


	public void setTertiaryQualificationNo(boolean tertiaryQualificationNo) {
		this.tertiaryQualificationNo = tertiaryQualificationNo;
	}


	public boolean isTertiaryQualificationYes() {
		return tertiaryQualificationYes;
	}


	public void setTertiaryQualificationYes(boolean tertiaryQualificationYes) {
		this.tertiaryQualificationYes = tertiaryQualificationYes;
	}


	public boolean isAskmanualQuestionNo() {
		return askmanualQuestionNo;
	}


	public void setAskmanualQuestionNo(boolean askmanualQuestionNo) {
		this.askmanualQuestionNo = askmanualQuestionNo;
	}


	public boolean isAskmanualQuestionYes() {
		return askmanualQuestionYes;
	}


	public void setAskmanualQuestionYes(boolean askmanualQuestionYes) {
		this.askmanualQuestionYes = askmanualQuestionYes;
	}


	public boolean isMortgageNo() {
		return mortgageNo;
	}


	public void setMortgageNo(boolean mortgageNo) {
		this.mortgageNo = mortgageNo;
	}


	public boolean isMortgageYes() {
		return mortgageYes;
	}


	public void setMortgageYes(boolean mortgageYes) {
		this.mortgageYes = mortgageYes;
	}


	public String getMortgageQue() {
		return mortgageQue;
	}


	public void setMortgageQue(String mortgageQue) {
		this.mortgageQue = mortgageQue;
	}
	


}
