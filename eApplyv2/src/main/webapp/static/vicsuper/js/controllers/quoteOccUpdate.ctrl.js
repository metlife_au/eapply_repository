/* Work Rating Controller,Progressive and Mandatory validations Starts  */
VicsuperApp.controller('quoteoccupdate',['$scope','$routeParams','$rootScope', '$location','$http','$timeout','$window','appNumberService','QuoteService','OccupationService','NewOccupationService','auraInputService','persoanlDetailService','deathCoverService','tpdCoverService','ipCoverService','fetchAppNumberSvc','PersistenceService','ngDialog','auraResponseService','urlService','saveEapply','RetrieveAppDetailsService','MaxLimitService','CalculateService','DownloadPDFService','printQuotePage','$q','$filter','APP_CONSTANTS',
                                          function($scope,$routeParams,$rootScope,$location,$http,$timeout,$window,appNumberService,QuoteService,OccupationService,NewOccupationService,auraInputService,persoanlDetailService,deathCoverService,tpdCoverService,ipCoverService,fetchAppNumberSvc,PersistenceService ,ngDialog,auraResponseService,urlService,saveEapply,RetrieveAppDetailsService,MaxLimitService, CalculateService,DownloadPDFService,printQuotePage,$q,$filter,APP_CONSTANTS){
	
	/* Code for appD starts */
	var pageTracker = null;
	if(ADRUM) {
		pageTracker = new ADRUM.events.VPageView();
		pageTracker.start();
	}

	$scope.$on('$destroy', function() {
		pageTracker.end();
		ADRUM.report(pageTracker);
	});
	/* Code for appD ends */
	
	
	$scope.urlList = urlService.getUrlList();
    $scope.phoneNumbrUpdate = /^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2|4|3|7|8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/;
    $scope.emailFormatUpdate = APP_CONSTANTS.emailFormat;
    var deathOccDBCategory, tpdOccDBCategory, ipOccDBCategory;
    var annualSalForOccUpgradeVal;
    var fetchAppnum = true;
	var appNum;
	var dcWeeklyCost, tpdWeeklyCost, ipWeeklyCost, totalCost;
	var dcCoverAmount, tpdCoverAmount, ipCoverAmount;
	var deathCoverType,tpdCoverType,ipCoverType;
	var deathUpgrade = false,
		tpdUpgrade = false,
		ipUpgrade = false;
	var checkAppNum = false;
	$scope.disableGender = false;
	var  standard = 'general';
	var  whitecolor = 'white collar';
	var  ownoccupation = 'own occupation';
	var  professional = 'professional';
	
	$scope.preferredContactType='';
	$scope.contactTypeOptions = ["Home phone", "Work phone", "Mobile"];

	$rootScope.$broadcast('enablepointer');

	/*Error Flags*/
	$scope.dodFlagErr = null;
	$scope.privacyFlagErr = null;
	$scope.prevOtherOcc = null;

	var inputDetails = persoanlDetailService.getMemberDetails();
	$scope.personalDetails = inputDetails.personalDetails;

$scope.checkAppNumber = function(appNumCheck)
	{
	 var defer = $q.defer();
	 if(appNumCheck == undefined || appNumCheck == '' || appNumCheck == null)
		{
	 appNumCheck = PersistenceService.getAppNumber();
		}
	if(appNumCheck == undefined || appNumCheck == '' || appNumCheck == null)
	{
	appNumberService.requestObj($scope.urlList.appNumUrl).then(function(response){
		PersistenceService.setAppNumber(response.data);
		fetchAppNumberSvc.setAppNumber(response.data);
		appNum = response.data;
		defer.resolve({});
	 });
	}
	else
		{
		appNum = appNumCheck;
		defer.resolve({});
		}
	return defer.promise;
	};

//    QuoteService.getList($scope.urlList.quoteUrl,"VICT").then(function(res){
//    	$scope.IndustryOptions = res.data;
//    }, function(err){
//    	console.log("Error while fetching industry options " + JSON.stringify(err));
//    });

//    $scope.getOccupations = function() {
//    	if($scope.otherOccupationObj){
//    		$scope.otherOccupationObj.workRatingotherOccupation = '';
//    	}
//    	if(!$scope.workRatingIndustry){
//    		$scope.workRatingIndustry = '';
//    	}
//    	OccupationService.getOccupationList($scope.urlList.occupationUrl,"VICT",$scope.workRatingIndustry).then(function(res){
//    		$scope.OccupationList = res.data;
//    		$scope.workRatingoccupation = "";
//    	}, function(err){
//    		console.log("Error while fetching occupation options " + JSON.stringify(err));
//    	});
//    };
    
//    $scope.getOtherOccupationAS = function(entered) {
//
//	    return $http.get('./occupation.json').then(function(response) {
//	      $scope.occupationList=[];
//        if(response.data.Other) {
//	        for (var key in response.data.Other) {
//	              var obj={};
//	              obj.id=key;
//	               obj.name=response.data.Other[key];
//                 //if(obj.name.indexOf(entered) > -1) {
//                  $scope.occupationList.push(obj.name);
//                 //}
//	               
//	        }
//	      }
//        return $filter('filter')($scope.occupationList, entered);
//	    }, function(err){
//	      console.info("Error while fetching occupations " + JSON.stringify(err));
//	    });
//	    
//	  };
	
	
	
	

	MaxLimitService.getMaxLimits($scope.urlList.maxLimitUrl,"VICT",inputDetails.memberType,"UWCOVER").then(function(res){
		var limits = res.data;
		annualSalForOccUpgradeVal = limits[0].annualSalForUpgradeVal;
	}, function(error){
		console.info('Something went wrong while fetching limits ' + error);
	});

    $scope.go = function ( path ) {
  	  $location.path( path );
  	};



	$scope.coltwo = false;
  	// $scope.toggleTwo = function(flag) {
  	// 	$scope.coltwo = flag;
   //      occupationVal++;
   //      $("a[data-target='#collapseTwo']").click();
   //  };

   /* Check if your is allowed to proceed to the next accordion */
  	// TBC
  	// Need to revisit, need better implementation
  	$scope.isCollapsible = function(targetEle, event) {
  		if( targetEle == 'accordionprivacy' && !$('#dodLabel').hasClass('active')) {
  			if($('#dodLabel').is(':visible'))
	  			$scope.dodFlagErr = true;
  			event.stopPropagation();
  			return false;
  		} else if( targetEle == 'collapseOne' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active'))) {
  			if($('#privacyLabel').is(':visible'))
	  			$scope.privacyFlagErr = true;
  			event.stopPropagation();
  			return false;
  		}  else if( targetEle == 'collapseTwo' && (!$('#dodLabel').hasClass('active') || !$('#privacyLabel').hasClass('active') || $("#collapseOne form").hasClass('ng-invalid'))) {
  			if($("#collapseOne form").is(':visible'))
	  				$scope.workRatingFormSubmit($scope.workRatingContactForm);
  			event.stopPropagation();
  			return false;
  		}
  	}

  	$scope.toggleTwo = function(checkFlag) {
        $scope.coltwo = checkFlag;
        if((checkFlag && $('#collapseTwo').hasClass('collapse in')) || (!checkFlag && !$('#collapseTwo').hasClass('collapse in')))
        	return false;
        $("a[data-target='#collapseTwo']").click(); /* Can be improved */
    };

    $scope.privacyCol = false;
    var dodCheck;
    var privacyCheck;
    var dodVal = 0;
    var privacyVal = 0;
    var contactVal = 0;
    var occupationVal = 0;
    $scope.togglePrivacy = function(flag) {
        $scope.privacyCol = flag;
        $("a[data-target='#collapseprivacy']").click();

    };
    $scope.toggleContact = function(checkFlag) {
        $scope.contactCol = checkFlag;
        if((checkFlag && $('#collapseOne').hasClass('collapse in')) || (!checkFlag && !$('#collapseOne').hasClass('collapse in')))
        	return false;
        $("a[data-target='#collapseOne']").click(); /* Can be improved */

    };
    $scope.checkDodState = function(){
    	$timeout(function() {
    		$scope.dodFlagErr = $scope.dodFlagErr == null ? !$('#dodLabel').hasClass('active') : !$scope.dodFlagErr;
    		if($('#dodLabel').hasClass('active')) {
    			$scope.togglePrivacy(true);
    		} else {
    			$scope.togglePrivacy(false);
    			$scope.toggleContact(false);
    			$scope.toggleTwo(false);
    		}
    	}, 1);
    };

    $scope.checkPrivacyState  = function(){
	    $timeout(function() {
    		$scope.privacyFlagErr = $scope.privacyFlagErr == null ? !$('#privacyLabel').hasClass('active') : !$scope.privacyFlagErr;
    		if($('#privacyLabel').hasClass('active')) {
    			$scope.toggleContact(true);
    		} else {
    			$scope.toggleContact(false);
    			$scope.toggleTwo(false);
    		}
    	}, 1);
    };
    $scope.navigateToLandingPage = function (){
    	/*if(window.confirm('Are you sure you want to navigate to Home Page?')){
    		$location.path("/landing");
    	}*/
    	ngDialog.openConfirm({
            template:'<div class="ngdialog-content"><div class="modal-body"><!-- Row starts --><div class="row  rowcustom  "><div class="col-sm-8"><p> Are you sure you want to navigate to Home Page?</p> </div> </div></div></br><div class="ngdialog-footer mt0"><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="confirm()">Ok</button> <button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-click="closeThisDialog(\'oncancel\')">Cancel</button></div></div>',
            plain: true,
            className: 'ngdialog-theme-plain custom-width'
        }).then(function(){
        	$location.path("/landing");
        }, function(e){
        	if(e=='oncancel'){
        		return false;
        	}
        });
    }


    $scope.deathCoverTransDetails = deathCoverService.getDeathCover();
    $scope.tpdCoverTransDetails = tpdCoverService.getTpdCover();
	$scope.ipCoverTransDetails = ipCoverService.getIpCover();
	
	var deathoccRating = $scope.deathCoverTransDetails.occRating;
	if (deathoccRating) {
		if (deathoccRating.toLowerCase() == standard){
			deathoccRating = 1;
		}
		else if (deathoccRating.toLowerCase() == whitecolor || deathoccRating.toLowerCase() == ownoccupation){
			deathoccRating = 2;
		}
		else if (deathoccRating.toLowerCase() == professional){
			deathoccRating = 3;
		}
	}
	
	var tpdoccRating = $scope.tpdCoverTransDetails.occRating;
	if (tpdoccRating) {			
		if (tpdoccRating.toLowerCase() == standard){
			tpdoccRating = 1;
		}
		else if (tpdoccRating.toLowerCase() == whitecolor || tpdoccRating.toLowerCase() == ownoccupation){
			tpdoccRating = 2;
		}
		else if (tpdoccRating.toLowerCase() == professional){
			tpdoccRating = 3;
		}
	}
	
	var ipoccRating = $scope.ipCoverTransDetails.occRating;
	if (ipoccRating) {			
		if (ipoccRating.toLowerCase() == standard){
			ipoccRating = 1;
		}
		else if (ipoccRating.toLowerCase() == whitecolor || ipoccRating.toLowerCase() == ownoccupation){
			ipoccRating = 2;
		}
		else if (ipoccRating.toLowerCase() == professional){
			ipoccRating = 3;
		}
	}
	
	$scope.inputMsgOccRating = Math.max(deathoccRating,tpdoccRating,ipoccRating);		
	
	
	$scope.deathOccupationCategory = $scope.inputMsgOccRating || 'General';
	$scope.tpdOccupationCategory = $scope.inputMsgOccRating || 'General';
	$scope.ipOccupationCategory = $scope.inputMsgOccRating || 'General';
	
	$scope.waitingPeriodOptions = ["30 Days", "60 Days", "90 Days"];
    $scope.benefitPeriodOptions = ['2 Years', 'Age 65'];

    $scope.ipCoverTransDetails.waitingPeriod = $scope.waitingPeriodOptions.indexOf($scope.ipCoverTransDetails.waitingPeriod) > -1 ? $scope.ipCoverTransDetails.waitingPeriod : '90 Days';
    $scope.ipCoverTransDetails.benefitPeriod = $scope.waitingPeriodOptions.indexOf($scope.ipCoverTransDetails.benefitPeriod) > -1 ? $scope.ipCoverTransDetails.benefitPeriod : '2 Years';

	
	
	 if($scope.deathCoverTransDetails.type == '1'){
		deathCoverType = 'DcUnitised';
	 }else if($scope.deathCoverTransDetails.type == '2'){
		deathCoverType = 'DcFixed';
	 }

	if($scope.tpdCoverTransDetails.type == '1'){
		tpdCoverType = 'TPDUnitised';
	}else if($scope.tpdCoverTransDetails.type == '2'){
		tpdCoverType = 'TPDFixed';
	}
	$scope.otherOccupationObj = {'workRatingotherOccupation': ''};

	if(inputDetails && inputDetails.contactDetails.emailAddress){
		$scope.email = inputDetails.contactDetails.emailAddress;
	}
	if(inputDetails && inputDetails.contactDetails.prefContactTime){
		if(inputDetails.contactDetails.prefContactTime == "1"){
			$scope.time= "Morning (9am - 12pm)";
		}else{
			$scope.time= "Afternoon (12pm - 6pm)";
		}
	}
	
	if($scope.personalDetails.gender == null || $scope.personalDetails.gender == ""){
		$scope.gender ='';
	}else{
		$scope.gender = $scope.personalDetails.gender;
	}
	
	if($scope.gender && (($scope.gender).toLowerCase() === 'female' || ($scope.gender).toLowerCase() === 'male' ))
	{
	$scope.disableGender = true;
	}
	else
	{
	$scope.disableGender = false;
	}
	
	if(inputDetails.contactDetails.prefContact == null || inputDetails.contactDetails.prefContact == "")
	{
	inputDetails.contactDetails.prefContact = 1;
	}
	if(inputDetails && inputDetails.contactDetails.prefContact){
		if(inputDetails.contactDetails.prefContact == "1"){
			$scope.preferredContactType= "Mobile";
			$scope.updatePhone = inputDetails.contactDetails.mobilePhone;
		}else if(inputDetails.contactDetails.prefContact == "2"){
			$scope.preferredContactType= "Home phone";
			$scope.updatePhone = inputDetails.contactDetails.homePhone;
		}else if(inputDetails.contactDetails.prefContact == "3"){
			$scope.preferredContactType= "Work phone";
			$scope.updatePhone = inputDetails.contactDetails.workPhone;
		}
   }
	$scope.changePrefContactType = function(){
		if($scope.preferredContactType == "Home phone"){
			$scope.updatePhone = inputDetails.contactDetails.homePhone;
		}else if($scope.preferredContactType == "Work phone"){
			$scope.updatePhone = inputDetails.contactDetails.workPhone;
		}else if($scope.preferredContactType == "Mobile"){
			$scope.updatePhone = inputDetails.contactDetails.mobilePhone;
		} else {
			$scope.updatePhone = '';
		}
	}
	
    //var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years')) + 1;
    //As part of the vicsuper,it should calculate premium based on current age.
    var anb = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years'));

   //Progressive validation
    var workRatingFormFields = ['workRatingEmail', 'wrkRatingPrefContactNo','workRatingPrefTime','gender'];



    $scope.getOccCategoryFromDB = function(){
    	/*if( $scope.prevOtherOcc !== $scope.otherOccupationObj.workRatingotherOccupation){*/
	    	/*if($scope.otherOccupationObj){
	    		$scope.otherOccupationObj.workRatingotherOccupation = '';
	    	}*/
//	    	if($scope.workRatingoccupation != undefined || $scope.otherOccupationObj.workRatingotherOccupation != undefined){
//	    		/*$scope.workRatingWithinOffcQue = null;
//	    		$scope.workRatingTertQue = null;
//	    		$scope.workRatingHazardousQue = null;
//	    		$scope.workRatingOutsideOffice = null;*/
//	    		
//		    	/*var occName = $scope.workRatingIndustry + ":" + $scope.workRatingoccupation;*/    		
//	    		if($scope.workRatingoccupation != undefined && ($scope.otherOccupationObj.workRatingotherOccupation == null || $scope.otherOccupationObj.workRatingotherOccupation == '')){
//	    			var occName = $scope.workRatingIndustry + ":" + $scope.workRatingoccupation;
//	    		}else if ($scope.otherOccupationObj.workRatingotherOccupation != undefined){
//	    			/*$scope.prevOtherOcc = $scope.otherOccupation;*/
//	    			if(($scope.OccupationList.find(o => o.occupationName === $scope.otherOccupationObj.workRatingotherOccupation))!== undefined){
//	    				var occName = $scope.workRatingIndustry + ":" + $scope.otherOccupationObj.workRatingotherOccupation;
//	    			}else{
//	    				var occName = $scope.workRatingIndustry + ":" + $scope.workRatingoccupation;
//	    			}
//	    			
//	    		}
//	    		
//		    	NewOccupationService.getOccupation($scope.urlList.newOccupationUrl, "VICT", occName).then(function(res){
//		    		if(deathCoverType == 'DcFixed'){
//		    			deathOccDBCategory = res.data[0].deathfixedcategeory;
//		    		}else if(deathCoverType == 'DcUnitised'){
//		    			deathOccDBCategory = res.data[0].deathunitcategeory;
//		    		}
//		    		if(tpdCoverType == 'TPDFixed'){
//		    			tpdOccDBCategory = res.data[0].tpdfixedcategeory;
//		    		}else if(tpdCoverType == 'TPDUnitised'){
//		    			tpdOccDBCategory = res.data[0].tpdunitcategeory;
//		    		}
//		    		ipOccDBCategory = res.data[0].ipfixedcategeory;
//		    		$scope.renderOccupationQuestions();
//		    	}, function(err){
//		    		console.info("Error while getting transfer category from DB " + JSON.stringify(err));
//		    	});
//	    	}
    	/*}*/
    };

    $scope.renderOccupationQuestions = function(){
	  	/*if($scope.fifteenHrsQuestion == 'Yes'){*/
//	  		if($scope.OccupationList || $scope.otherOccupationObj.workRatingotherOccupation){
//	  			var selectedOcc = $scope.OccupationList.filter(function(obj){
//		  			/*return obj.occupationName == $scope.workRatingoccupation;*/
//	  				if($scope.workRatingoccupation && $scope.otherOccupationObj.workRatingotherOccupation == ''){
//	                    return obj.occupationName == $scope.workRatingoccupation;
//		           	}else if($scope.otherOccupationObj.workRatingotherOccupation && (($scope.OccupationList.find(o => o.occupationName === $scope.otherOccupationObj.workRatingotherOccupation))!== undefined)){
//		           		return obj.occupationName == $scope.otherOccupationObj.workRatingotherOccupation;
//		           	}else{
//		           		return obj.occupationName == $scope.workRatingoccupation;
//		           	}
//		  		});
//		  		var selectedOccObj = selectedOcc[0];
//
//		  		if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'false'){
//		  			$scope.showWithinOfficeOccQuestion = true;
//		  		    $scope.showTertiaryOccQuestion = true;
//		  		    $scope.showHazardousOccQuestion = false;
//		  		    $scope.showOutsideOfficeOccQuestion = false;
//		  			workRatingOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzWrkUpdateQuestion','workRatingIndustry','workRatingoccupation','workRatingWithinOffcQue','workRatingTertQue','workRatingAnnualSal'];
//		  			if($scope.workRatingWithinOffcQue == 'Yes' && $scope.workRatingTertQue == 'Yes' && $scope.workRatingAnnualSal && parseFloat($scope.workRatingAnnualSal) >= parseFloat(annualSalForOccUpgradeVal)){
//	  		    		$scope.deathOccupationCategory = 'Professional';
//	  		    		$scope.tpdOccupationCategory = 'Professional';
//	  		    		$scope.ipOccupationCategory = 'Professional';
//	  		    	} else{
//	  		    		$scope.deathOccupationCategory = deathOccDBCategory;
//	  		    		$scope.tpdOccupationCategory = tpdOccDBCategory;
//	  		    		$scope.ipOccupationCategory = ipOccDBCategory;
//	  		    	}
//		  		} else if(selectedOccObj.professionalFlag.toLowerCase() == 'true' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
//		  			$scope.showWithinOfficeOccQuestion = false;
//		  		    $scope.showTertiaryOccQuestion = false;
//		  		    $scope.showHazardousOccQuestion = true;
//		  		    $scope.showOutsideOfficeOccQuestion = true;
//		  		    workRatingOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzWrkUpdateQuestion','workRatingIndustry','workRatingoccupation','workRatingHazardousQue','workRatingOutsideOffice','workRatingAnnualSal'];
//		  		    if($scope.workRatingHazardousQue == 'No' && $scope.workRatingOutsideOffice == 'No'){
//		  		    	$scope.showWithinOfficeOccQuestion = true;
//		  	  		    $scope.showTertiaryOccQuestion = true;
//			  	  	    workRatingOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzWrkUpdateQuestion','workRatingIndustry','workRatingoccupation','workRatingHazardousQue','workRatingOutsideOffice','workRatingWithinOffcQue','workRatingTertQue','workRatingAnnualSal'];
//			  	  	$scope.deathOccupationCategory = 'White Collar';
//  		    		$scope.tpdOccupationCategory = 'White Collar';
//  		    		$scope.ipOccupationCategory = 'White Collar';
//  		    		if($scope.workRatingWithinOffcQue == 'Yes' && $scope.workRatingTertQue == 'Yes' && $scope.workRatingAnnualSal && parseFloat($scope.workRatingAnnualSal) >= parseFloat(annualSalForOccUpgradeVal)){
//	  		    		$scope.deathOccupationCategory = 'Professional';
//	  		    		$scope.tpdOccupationCategory = 'Professional';
//	  		    		$scope.ipOccupationCategory = 'Professional';
//	  		    	} else{
//	  		    		$scope.deathOccupationCategory = 'White Collar';
//	  		    		$scope.tpdOccupationCategory = 'White Collar';
//	  		    		$scope.ipOccupationCategory = 'White Collar';
//	  		    	}
//		  		    } else{
//		  		    	$scope.showWithinOfficeOccQuestion = false;
//		  	  		    $scope.showTertiaryOccQuestion = false;
//			  	  		workRatingOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzWrkUpdateQuestion','workRatingIndustry','workRatingoccupation','workRatingHazardousQue','workRatingOutsideOffice','workRatingAnnualSal'];
//			  	     	$scope.deathOccupationCategory = deathOccDBCategory;
//			  	        $scope.tpdOccupationCategory = tpdOccDBCategory;
//			  	        $scope.ipOccupationCategory = ipOccDBCategory;
//		  		    }
//		  		} else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'true'){
//		  			$scope.showWithinOfficeOccQuestion = false;
//		  		    $scope.showTertiaryOccQuestion = false;
//		  		    $scope.showHazardousOccQuestion = true;
//		  		    $scope.showOutsideOfficeOccQuestion = true;
//		  		    workRatingOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzWrkUpdateQuestion','workRatingIndustry','workRatingoccupation','workRatingHazardousQue','workRatingOutsideOffice','workRatingAnnualSal'];
//		  		  if($scope.workRatingHazardousQue == 'No' && $scope.workRatingOutsideOffice == 'No'){
//		  			     $scope.deathOccupationCategory = 'White Collar';
//  		    		     $scope.tpdOccupationCategory = 'White Collar';
//  		    		     $scope.ipOccupationCategory = 'White Collar';
//		  		  } else{
//		  			     $scope.deathOccupationCategory = deathOccDBCategory;
//		  	             $scope.tpdOccupationCategory = tpdOccDBCategory;
//		  	             $scope.ipOccupationCategory = ipOccDBCategory;
//		  		  }
//		  		} else if(selectedOccObj.professionalFlag.toLowerCase() == 'false' && selectedOccObj.manualFlag.toLowerCase() == 'false'){
//		  			$scope.showWithinOfficeOccQuestion = false;
//		  		    $scope.showTertiaryOccQuestion = false;
//		  		    $scope.showHazardousOccQuestion = false;
//		  		    $scope.showOutsideOfficeOccQuestion = false;
//		  		    workRatingOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzWrkUpdateQuestion','workRatingIndustry','workRatingoccupation','workRatingAnnualSal'];
//		  		    $scope.deathOccupationCategory = deathOccDBCategory;
//	  	            $scope.tpdOccupationCategory = tpdOccDBCategory;
//	  	            $scope.ipOccupationCategory = ipOccDBCategory;
//		  		}
//	  		}
//	  	/*}*/ else{
//	  		$scope.showWithinOfficeOccQuestion = false;
//  		    $scope.showTertiaryOccQuestion = false;
//  		    $scope.showHazardousOccQuestion = false;
//  		    $scope.showOutsideOfficeOccQuestion = false;
//  		    workRatingOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzWrkUpdateQuestion','workRatingIndustry','workRatingoccupation','workRatingAnnualSal'];
//  		  if(deathOccDBCategory == 'White Collar'){
//	  		    $scope.deathOccupationCategory = 'Standard';
//		    } else if(deathOccDBCategory == 'Professional'){
//		    	$scope.deathOccupationCategory = 'Professional';
//		    } else{
//		    	$scope.deathOccupationCategory = 'Standard';
//		    }
//
//		    if(tpdOccDBCategory == 'White Collar'){
//	  		    $scope.tpdOccupationCategory = 'Standard';
//		    } else if(tpdOccDBCategory == 'Professional'){
//		    	$scope.tpdOccupationCategory = 'Professional';
//		    } else{
//		    	$scope.tpdOccupationCategory = 'Standard';
//		    }
//
//		  if(ipOccDBCategory == 'White Collar'){
//	  		    $scope.ipOccupationCategory = 'Standard';
//		    } else if(ipOccDBCategory == 'Professional'){
//		    	$scope.ipOccupationCategory = 'Professional';
//		    } else{
//		    	$scope.ipOccupationCategory = 'Standard';
//		    }
//	  	}
    	
	  		
    	
    	
    	
	  		//$scope.OccupationFormFields = ['fifteenHrsQuestion','ownBussinessQuestion','areyouperCitzQuestion','industry','occupation','annualSalary'];
	  		$scope.workRatingOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzWrkUpdateQuestion','occRating1a','occRating1b','occRating2','workRatingAnnualSal'];
            
	        $scope.occQustnsRating = 1; 
	        $scope.overAllOccRating = 1;
	       
	    	if (($scope.occRating1a 
	    			&& $scope.occRating1a == 'Yes') 
	    				&&($scope.occRating2 && $scope.occRating2 == 'Yes') 
	    			&& $scope.workRatingAnnualSal > 100000){
	    		$scope.occQustnsRating=3;
	    	} else if (($scope.occRating1a && $scope.occRating1a == 'Yes') ||
	    			($scope.occRating1b && $scope.occRating1b == 'Yes')){
	    		$scope.occQustnsRating = 2;
	    	}   else {
	    		$scope.occQustnsRating = 1;
	    	}   	
	        
	        if ($scope.inputMsgOccRating && $scope.occQustnsRating){
	        	$scope.overAllOccRating = Math.max($scope.inputMsgOccRating,$scope.occQustnsRating);
	        } else if ($scope.occQustnsRating){
	        	$scope.overAllOccRating = $scope.occQustnsRating;
	        } 
	      
	        if ($scope.overAllOccRating == 3){
	        	$scope.deathOccupationCategory = 'Professional';
	            $scope.tpdOccupationCategory = 'Professional';
	            $scope.ipOccupationCategory = 'Professional';
	        	
	        } else if ($scope.overAllOccRating == 2) {
	        	$scope.deathOccupationCategory = 'White Collar';
	        	$scope.tpdOccupationCategory = 'White Collar';
	            $scope.ipOccupationCategory = 'White Collar';
	        } else {
	        	$scope.deathOccupationCategory = 'General';
	        	$scope.tpdOccupationCategory = 'General';
	            $scope.ipOccupationCategory = 'General';
	        }
	  		
	  		
  	};
	 var workRatingOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzWrkUpdateQuestion','occRating1a','occRating1b','occRating2','workRatingAnnualSal'];
     var workRatingOthrOccupationFormFields = ['fifteenHrsQuestion','areyouperCitzWrkUpdateQuestion','workRatingIndustry','workRatingoccupation','workRatingotherOccupation','workRatingAnnualSal'];

     $scope.checkPreviousMandatoryFieldsFrWorkRating  = function (workRatingElementName,workRatingFormName){
      	var WorkRatingformFields;
      	if(workRatingFormName == 'workRatingContactForm'){
      		WorkRatingformFields = workRatingFormFields;
    	}
      	else if(workRatingFormName == 'workRatingOccupForm'){
    		//if($scope.workRatingoccupation != undefined && $scope.workRatingoccupation == 'Other'){
    		//	WorkRatingformFields = workRatingOthrOccupationFormFields;
    		//} else{
    			WorkRatingformFields = workRatingOccupationFormFields;
    		//}
    			/*Vicsuper - occupation rating changes starts*/
    		      if(workRatingElementName == 'occRating1a' && $scope.occRating1a == 'Yes') {
    		    	  $scope.occRating1b = 'No'
    		      }
    		      if(workRatingElementName == 'occRating1b' && $scope.occRating1b == 'Yes') {
    		    	  $scope.occRating1a = 'No'
    		      }
    		      if (workRatingElementName == 'occRating1a' || workRatingElementName == 'occRating1b' || workRatingElementName == 'occRating2') {
    		    	  $scope.renderOccupationQuestions();
    		      }
    		      /*Vicsuper - occupation rating changes ends*/
    	}
        var inx = WorkRatingformFields.indexOf(workRatingElementName);
        if(inx > 0){
          for(var i = 0; i < inx ; i++){
            $scope[workRatingFormName][WorkRatingformFields[i]].$touched = true;
          }
        }
      };

   // Validate fields "on continue"
 	  // appNum = PersistenceService.getAppNumber();
     $scope.workRatingFormSubmit =  function (form){
        if(!form.$valid){
      	//  alert("invalid>>"+$scope["workRatingContactForm"].$invalid);
      	  form.$submitted=true;
      	  if(form.$name == 'workRatingContactForm')
      	    $scope.toggleTwo(false);
  	    }else{
  		  if(form.$name == 'workRatingContactForm'){
  			if(!checkAppNum)
			{
			if(fetchAppnum){
    			fetchAppnum = false;
    			appNum = PersistenceService.getAppNumber();
    		}
			}
      	    $scope.toggleTwo(true);
  		  }else if(form.$name == 'workRatingOccupForm'){
  			
  			    $scope.goToAura();
  			    $rootScope.$broadcast('disablepointer');
  		     //	$scope.continueWorkRatingPage();
   		  }
       }
             // console.log("Form Validation");
     };

     $scope.invalidSalAmount = false;
	    $scope.checkValidSalary = function(){
	    	if(parseInt($scope.workRatingAnnualSal) > 1000000){
	  			$scope.invalidSalAmount = true;
	  		} else{
	  			$scope.invalidSalAmount = false;
	  		}
	    	$scope.renderOccupationQuestions();
	    };

     $scope.auraDisabled = false;
     
     $scope.continueWorkRatingPage = function(print){
    	 $scope.renderOccupationQuestions();
//    	 $scope.validCategories = ['standard', 'white collar', 'professional', 'heavy blue collar'];
//    	 var occDeathRating = $scope.deathCoverTransDetails.occRating||"";
//    	 occDeathRating=occDeathRating.toLowerCase();
//    	 var occTpdRating = $scope.tpdCoverTransDetails.occRating||"standard";
//    	 occTpdRating=occTpdRating.toLowerCase();
//    	 
//    	 
//    	 
//    	 if(deathCoverType == 'DcFixed'){
//    		 occDeathRating = $scope.validCategories.indexOf(occDeathRating) > -1 ? occDeathRating : 'heavy blue collar';
// 		}else if(deathCoverType == 'DcUnitised'){
// 			occDeathRating = $scope.validCategories.indexOf(occDeathRating) > -1 ? occDeathRating : 'standard';
// 		}
// 		if(tpdCoverType == 'TPDFixed'){
// 			occTpdRating = $scope.validCategories.indexOf(occTpdRating) > -1 ? occTpdRating : 'heavy blue collar';
// 		}else if(tpdCoverType == 'TPDUnitised'){
// 			occTpdRating = $scope.validCategories.indexOf(occTpdRating) > -1 ? occTpdRating : 'standard';
// 		}
//    	 
//    	 
//    	 if($scope.deathOccupationCategory == "Professional" && (occDeathRating == "standard" || occDeathRating == "white collar")){
//    		 deathUpgrade = true;
//    	 } else if($scope.deathOccupationCategory == "White Collar" && occDeathRating == "standard"){
//    		 deathUpgrade = true;
//    	 }
//    	 
//    	 if($scope.tpdOccupationCategory == "Professional" && (occTpdRating == "standard" || occTpdRating == "white collar")){
//    		 tpdUpgrade = true;
//    	 } else if($scope.tpdOccupationCategory == "White Collar" && occTpdRating == "standard"){
//    		 tpdUpgrade = true;
//    	 }
//    	 var occIpRating = $scope.ipCoverTransDetails.occRating||"standard";
//    	 occIpRating=occIpRating.toLowerCase();
//    	 occIpRating = $scope.validCategories.indexOf(occIpRating) > -1 ? occIpRating : 'standard';
//    	 if($scope.ipOccupationCategory == "Professional" && (occIpRating == "standard" || occIpRating == "white collar")){
//    		 ipUpgrade = true;
//    	 } else if($scope.ipOccupationCategory == "White Collar" && occIpRating == "standard"){
//    		 ipUpgrade = true;
//    	 }
    	 
    	 
    	 $scope.age = parseInt(moment().diff(moment($scope.personalDetails.dateOfBirth, 'DD-MM-YYYY'), 'years'));
    	 
    	 if(($scope.areyouperCitzWrkUpdateQuestion).toLowerCase() === 'no')
    		 {
    		 if ($scope.inputMsgOccRating == 3){
 	        		$scope.deathOccupationCategory = 'Professional';
 	            	$scope.tpdOccupationCategory = 'Professional';
 	            	$scope.ipOccupationCategory = 'Professional';
 	        	} else if ($scope.inputMsgOccRating == 2) {
 	        		$scope.deathOccupationCategory = 'White Collar';
 	        		$scope.tpdOccupationCategory = 'White Collar';
 	        		$scope.ipOccupationCategory = 'White Collar';
 	        	} else {
 	        		$scope.deathOccupationCategory = 'General';
 	        		$scope.tpdOccupationCategory = 'General';
 	        		$scope.ipOccupationCategory = 'General';
 	        	}
    		}
    	 
    	 
    	 var ruleModel = {
         		"age": anb,
         		"fundCode": "VICT",
         		"gender": $scope.gender,
         		"deathOccCategory": $scope.deathOccupationCategory,
         		"tpdOccCategory": $scope.tpdOccupationCategory,
         		"ipOccCategory": $scope.ipOccupationCategory,
         		"smoker": false,
         		"deathFixedCost": null,
         		"deathUnitsCost": null,
         		"tpdFixedCost": null,
         		"tpdUnitsCost": null,
         		"ipUnits": null,
         		"ipUnitsCost": null,
         		"premiumFrequency": "Weekly",
         		"memberType": null,
         		"manageType": "UWCOVER",
         		"ipCoverType": "IpFixed",
         		"ipWaitingPeriod": $scope.ipCoverTransDetails.waitingPeriod,
         		"ipBenefitPeriod": $scope.ipCoverTransDetails.benefitPeriod
         	};
    	 if($scope.deathCoverTransDetails.type == '1'){
 			ruleModel.deathCoverType = 'DcUnitised';
 			ruleModel.deathUnits = parseInt($scope.deathCoverTransDetails.units);
 		} else if($scope.deathCoverTransDetails.type == '2'){
 			ruleModel.deathCoverType = 'DcFixed';
 			ruleModel.deathFixedAmount =  parseInt($scope.deathCoverTransDetails.amount);
 		}

 		if($scope.tpdCoverTransDetails.type == '1'){
 			ruleModel.tpdCoverType = 'TPDUnitised';
 			ruleModel.tpdUnits = parseInt($scope.tpdCoverTransDetails.units);
 		} else if($scope.tpdCoverTransDetails.type == '2'){
 			ruleModel.tpdCoverType = 'TPDFixed';
 			ruleModel.tpdFixedAmount =  parseInt($scope.tpdCoverTransDetails.amount);
 		}
 		
 		if($scope.age < 14 || $scope.age > 64)
 			{
 			ruleModel.ipFixedAmount = 0;
 			}
 		else
 			{
 			ruleModel.ipFixedAmount = $scope.ipCoverTransDetails.amount;
 			}

 		CalculateService.calculate(ruleModel,$scope.urlList.calculateUrl).then(function(res){
 			var premium = res.data;
			for(var i = 0; i < premium.length; i++){
    			if(premium[i].coverType == 'DcFixed' || premium[i].coverType == 'DcUnitised'){
    				dcWeeklyCost = premium[i].cost;
    				dcCoverAmount = premium[i].coverAmount;
    			} else if(premium[i].coverType == 'TPDFixed' || premium[i].coverType == 'TPDUnitised'){
    				tpdWeeklyCost = premium[i].cost;
    				tpdCoverAmount = premium[i].coverAmount;
    			} else if(premium[i].coverType == 'IpFixed' || premium[i].coverType == 'IpUnitised'){
    				ipWeeklyCost = premium[i].cost;
    				ipCoverAmount = premium[i].coverAmount;
    			}
    		}
			totalCost = parseFloat(dcWeeklyCost) + parseFloat(tpdWeeklyCost) + parseFloat(ipWeeklyCost);
			$scope.auraDisabled = true;  			
  			$scope.overAllOccRating = Math.max($scope.inputMsgOccRating,$scope.occQustnsRating);
  			if (parseInt($scope.overAllOccRating) > parseInt($scope.inputMsgOccRating)){
  				$scope.auraDisabled = false;
  			}
  			if(($scope.areyouperCitzWrkUpdateQuestion).toLowerCase() === 'no')
  				{
  				$scope.auraDisabled = true;
  				}
  			$scope.checkAppNumber(appNum).then(function() {
				$scope.saveDataForPersistence();
	  			$location.path('/workRatingSummary/1');
			}, function(err) {
                //console.log(err);
            });
//			if(!print){
//				if(deathUpgrade || tpdUpgrade || ipUpgrade){
//	  				$scope.auraDisabled = false;
//	  				$scope.saveDataForPersistence();
//	      	  		$location.path('/auraocc/1');
//	      		}else{
//	      			$scope.auraDisabled = true;
//	      			$scope.saveDataForPersistence();
//	      			$location.path('/workRatingSummary/1');
//	      		}
//			}
 		}, function(err){
 			console.info("Error while calculating premium " + JSON.stringify(err));
 		});
     };

	$scope.saveDataForPersistence = function(){
		$scope.personalDetails = persoanlDetailService.getMemberDetails().personalDetails;
    	var coverObj = {};
    	var coverStateObj = {};
    	var occObj={};
//    	var selectedIndustry = $scope.IndustryOptions.filter(function(obj){
//    		return $scope.workRatingIndustry == obj.key;
//    	});

    	coverObj['name'] = $scope.personalDetails.firstName+" "+$scope.personalDetails.lastName;
    	coverObj['dob'] = $scope.personalDetails.dateOfBirth;
    	coverObj['email'] = $scope.email;
    	occObj['gender'] = $scope.gender;
    	coverObj['contactType']=$scope.preferredContactType;
    	coverObj['contactPhone'] = $scope.updatePhone;
    	coverObj['contactPrefTime'] = $scope.time;
    	

    	coverObj['deathOccCategory'] = $scope.deathOccupationCategory;
    	coverObj['tpdOccCategory'] = $scope.tpdOccupationCategory;
    	coverObj['ipOccCategory'] = $scope.ipOccupationCategory;
    	coverObj['auraDisabled'] = $scope.auraDisabled;

    	occObj['fifteenHr'] = $scope.fifteenHrsQuestion;
    	
    	occObj['occRating1a'] = $scope.occRating1a;
    	occObj['occRating1b'] = $scope.occRating1b;
    	occObj['occRating2'] = $scope.occRating2;    	
    	
//    	occObj['industryName'] = selectedIndustry.length ? selectedIndustry[0].value : '';
//    	occObj['industryCode'] = selectedIndustry.length ? selectedIndustry[0].key : '';
//    	occObj['occupation'] = $scope.workRatingoccupation;
    	
    	//reset values
    	/*occObj['withinOfficeQue'] = "";
    	occObj['tertiaryQue']= "";
    	occObj['managementRoleQue']= "";
    	occObj['hazardousQue']="";*/
    	
    	if( !($scope.showWithinOfficeOccQuestion && $scope.showTertiaryOccQuestion)){
            $scope.workRatingWithinOffcQue = null;
    		$scope.workRatingTertQue = null;
      	}
        if(!$scope.showHazardousOccQuestion && !$scope.showOutsideOfficeOccQuestion){
        	$scope.workRatingHazardousQue = null;
    		$scope.workRatingOutsideOffice = null;
      	}
    	
    	coverStateObj['showWithinOfficeOccQuestion'] = $scope.showWithinOfficeOccQuestion;
        coverStateObj['showTertiaryOccQuestion'] =  $scope.showTertiaryOccQuestion;
        coverStateObj['showHazardousOccQuestion'] = $scope.showHazardousOccQuestion;
        coverStateObj['showOutsideOfficeOccQuestion'] = $scope.showOutsideOfficeOccQuestion;
    	
    	/*if($scope.showWithinOfficeOccQuestion){*/
    		occObj['withinOfficeQue']= $scope.workRatingWithinOffcQue;
    	/*}*/
    	/*if($scope.showTertiaryOccQuestion){*/
    		occObj['tertiaryQue']= $scope.workRatingTertQue;
    	/*}*/
    	/*if($scope.showOutsideOfficeOccQuestion){*/
    		occObj['managementRoleQue']= $scope.workRatingOutsideOffice;
    	/*}*/
    	/*if($scope.showHazardousOccQuestion){*/
    		occObj['hazardousQue']=$scope.workRatingHazardousQue;
    	/*}*/
    	
    	occObj['salary'] = $scope.workRatingAnnualSal;
    	occObj['otherOccupation'] = $scope.otherOccupationObj.workRatingotherOccupation;
    	occObj['citizenQue'] = $scope.areyouperCitzWrkUpdateQuestion;

    	coverObj['deathAmt'] = parseFloat($scope.deathCoverTransDetails.amount);
    	coverObj['tpdAmt'] = parseFloat($scope.tpdCoverTransDetails.amount);
    	coverObj['ipAmt'] = parseFloat($scope.ipCoverTransDetails.amount);
    	coverObj['waitingPeriod'] = $scope.ipCoverTransDetails.waitingPeriod;
    	coverObj['benefitPeriod'] = $scope.ipCoverTransDetails.benefitPeriod;
    	coverObj['appNum'] = appNum;
    	coverObj['dodCheck'] = dodCheck;
        coverObj['privacyCheck'] = privacyCheck;
    	/*coverObj['ackCheckbox'] = updateAckCheck;*/
    	coverObj['lastSavedOn'] = 'QuoteUpdatePage';
    	coverObj['age'] = anb;
        coverObj['manageType'] = 'UWCOVER';
        coverObj['partnerCode'] = 'VICT';
        coverObj['firstName'] = $scope.personalDetails.firstName;
        coverObj['lastName'] = $scope.personalDetails.lastName;
        coverObj['totalPremium'] = parseFloat(totalCost);
        coverObj['deathCoverPremium'] = parseFloat(dcWeeklyCost);
        coverObj['tpdCoverPremium'] = parseFloat(tpdWeeklyCost);
        coverObj['ipCoverPremium'] = parseFloat(ipWeeklyCost);
        coverObj['deathNewAmt'] = parseFloat(dcCoverAmount);
        coverObj['tpdNewAmt'] = parseFloat(tpdCoverAmount);
        coverObj['ipNewAmt'] = parseFloat(ipCoverAmount);
        coverObj['deathCoverType'] = deathCoverType;
    	coverObj['tpdCoverType'] = tpdCoverType;
		coverObj['ipCoverType'] = 'IpUnitised';
		coverObj['freqCostType'] = 'Weekly';


        

    	PersistenceService.setworkRatingCoverDetails(coverObj);
    	PersistenceService.setWorkRatingCoverOccDetails(occObj);
    	PersistenceService.setworkRatingCoverStateDetails(coverStateObj);



    };

    $scope.goToAura = function (){
    	if(this.workRatingContactForm.$valid && this.workRatingOccupForm.$valid && !$scope.invalidSalAmount){
	    	$scope.continueWorkRatingPage(false);
    	}
    };

    $scope.saveWorkRating = function (){
    	
    	$scope.checkAppNumber(appNum).then(function() {
    	$scope.saveAndExitPopUp('<STRONG> Thank you, your application has been successfully saved.</STRONG><BR><BR> Your application reference no. is <STRONG>'+appNum+'</STRONG><BR><BR> Keep this number handy in case you need to retrieve your application later.<BR><BR>');
    	$scope.saveDataForPersistence();
		var workRatingObject =  PersistenceService.getworkRatingCoverDetails();
		var workRatingOccDetails = PersistenceService.getWorkRatingCoverOccDetails();
	    var personalDetails = persoanlDetailService.getMemberDetails();
	    if(workRatingObject != null && workRatingOccDetails != null && personalDetails != null){
	    	var details={};
			details.addnlDeathCoverDetails = {};
			details.addnlTpdCoverDetails = {};
			details.addnlIpCoverDetails = {};
			details.occupationDetails=workRatingOccDetails;
			details.addnlDeathCoverDetails.deathCoverPremium = workRatingObject.deathCoverPremium;
			details.addnlTpdCoverDetails.tpdCoverPremium = workRatingObject.tpdCoverPremium;
			details.addnlIpCoverDetails.ipCoverPremium = workRatingObject.ipCoverPremium;
			var temp = angular.extend(details,workRatingObject)
    	    var saveWorkRatingObject = angular.extend(temp,personalDetails);
	    	auraResponseService.setResponse(saveWorkRatingObject)
	        saveEapply.reqObj($scope.urlList.saveEapplyUrl).then(function(response) {
	                //console.log(response.data)
	        },function(err){
	        	//console.log("Something went wrong while saving"+JSON.stringify(err));
	        });
	    }
    	}, function(err) {
            //console.log(err);
        });
    };

    $scope.saveAndExitPopUp = function (hhText) {

		var dialog1 = ngDialog.open({
			    template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Application saved </h4><!-- Row starts --><div class="row  rowcustom  ">  <div class="col-sm-12">  <p class="aligncenter">  </p><div id="tips_text">'+hhText+'</div>  <p></p>  </div></div><!-- Row ends --></div><div class="ngdialog-buttons aligncenter"><button type="button" class="btn btn-primary" ng-dialog="secondDialogId" ng-dialog-class="ngdialog-theme-plain" ng-dialog-controller="SecondModalCtrl" ng-dialog-close-previous="" ng-click="preCloseCallback()">Finish &amp; Close Window </button></div></div>',
				className: 'ngdialog-theme-plain custom-width',
				preCloseCallback: function(value) {
				       var url = "/landing"
				       $location.path( url );
				       return true
				},
				plain: true
		});
		dialog1.closePromise.then(function (data) {
			//console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};
  $scope.clickToOpen = function (hhText) {

		var dialog = ngDialog.open({
			/*template: '<p>'+hhText+'</p>' +
				'<div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="closeThisDialog(1)">Close Me</button></div>',*/
				template: '<div class="ngdialog-content"><div class="modal-body"><h4 class="modal-title aligncenter" id="myModalLabel"> Helpful hints</h4><!-- Row starts --><div class="row rowcustom" style="margin:0px -35px;"><div class="col-sm-12"><p class="aligncenter"></p><div id="tips_text">'+hhText+'</div><p></p></div></div><!-- Row ends --></div><div class="row"><div class="col-sm-4"></div><div class="col-sm-4 col-xs-12"><button type="button" class="btn btn-primary" ng-dialog-class="ngdialog-theme-plain" ng-click="closeThisDialog()">Close</button></div><div class="col-sm-4"></div></div></div>',
				className: 'ngdialog-theme-plain',
				plain: true
		});
		dialog.closePromise.then(function (data) {
			//console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
		});
	};

    if($routeParams.mode == 2){
    	var existingWorkRatingDetails = PersistenceService.getworkRatingCoverDetails();
    	var workRatingOccDetails=PersistenceService.getWorkRatingCoverOccDetails();
    	var stateDetails = PersistenceService.getworkRatingCoverStateDetails();
    	if(!existingWorkRatingDetails || !workRatingOccDetails) {
    		$location.path("/quoteoccchange/1");
    		return false;
    	}

    	$scope.email = existingWorkRatingDetails.email;
    	$scope.preferredContactType=existingWorkRatingDetails.contactType;
    	$scope.updatePhone = existingWorkRatingDetails.contactPhone;
    	$scope.time = existingWorkRatingDetails.contactPrefTime;
    	$scope.auraDisabled = existingWorkRatingDetails.auraDisabled;

    	$scope.fifteenHrsQuestion = workRatingOccDetails.fifteenHr;
    	$scope.areyouperCitzWrkUpdateQuestion = workRatingOccDetails.citizenQue;
    	
    	$scope.occRating1a = workRatingOccDetails.occRating1a;
    	$scope.occRating1b = workRatingOccDetails.occRating1b;
    	$scope.occRating2 = workRatingOccDetails.occRating2;
    	    	
    	
    	//$scope.workRatingIndustry = workRatingOccDetails.industryCode;
    	//$scope.workRatingoccupation = workRatingOccDetails.occupation;
    	$scope.workRatingWithinOffcQue = workRatingOccDetails.withinOfficeQue;
    	$scope.workRatingTertQue = workRatingOccDetails.tertiaryQue;
    	$scope.workRatingOutsideOffice = workRatingOccDetails.managementRoleQue;
    	$scope.workRatingHazardousQue=workRatingOccDetails.hazardousQue;
    	$scope.gender = workRatingOccDetails.gender;
    	$scope.workRatingAnnualSal = workRatingOccDetails.salary;
    	$scope.otherOccupationObj.workRatingotherOccupation = workRatingOccDetails.otherOccupation;

    	$scope.showWithinOfficeOccQuestion = stateDetails.showWithinOfficeOccQuestion;
        $scope.showTertiaryOccQuestion = stateDetails.showTertiaryOccQuestion;
        $scope.showHazardousOccQuestion = stateDetails.showHazardousOccQuestion;
        $scope.showOutsideOfficeOccQuestion = stateDetails.showOutsideOfficeOccQuestion;

    	appNum = existingWorkRatingDetails.appNum;
    	dodCheck = existingWorkRatingDetails.dodCheck;
    	privacyCheck = existingWorkRatingDetails.privacyCheck;
    	updateAckCheck = existingWorkRatingDetails.ackCheckbox;
    	checkAppNum=true;
    	
    	if(updateAckCheck){
    	    $timeout(function(){
    			$('#ackLabel').addClass('active');
    			   });
    			 }

		$('#dodLabel').addClass('active');
		$('#privacyLabel').addClass('active');

//    	OccupationService.getOccupationList($scope.urlList.occupationUrl,"VICT",$scope.workRatingIndustry).then(function(res){
//    	//OccupationService.getOccupationList({fundId:"VICT", induCode:$scope.workRatingIndustry}, function(occupationList){
//        	$scope.OccupationList = res.data;
//        	var temp = $scope.OccupationList.filter(function(obj){
//        		return obj.occupationName == workRatingOccDetails.occupation;
//        	});
//        	$scope.workRatingoccupation = temp[0].occupationName;
//        	$scope.getOccCategoryFromDB();
//        }, function(err){
//        	//console.log("Error while fetching occupation options " + JSON.stringify(err));
//        });
    	$scope.togglePrivacy(true);
		$scope.toggleContact(true);
    	$scope.toggleTwo(true);
    }

    if($routeParams.mode == 3){
        	var num = PersistenceService.getAppNumToBeRetrieved();

        	RetrieveAppDetailsService.retrieveAppDetails($scope.urlList.retrieveAppUrl,num).then(function(res){
        		var result = res.data[0];

        		$scope.email = result.email;
            	$scope.preferredContactType=result.contactType;
            	$scope.updatePhone = result.contactPhone;
            	$scope.time = result.contactPrefTime;
            	$scope.auraDisabled = result.auraDisabled;

            	$scope.fifteenHrsQuestion = result.occupationDetails.fifteenHr;
            	$scope.areyouperCitzWrkUpdateQuestion = result.occupationDetails.citizenQue;
            	$scope.occRating1a = result.occupationDetails.occRating1a;
            	$scope.occRating1b = result.occupationDetails.occRating1b;
            	$scope.occRating2 = result.occupationDetails.occRating2;
            	//$scope.workRatingIndustry = result.occupationDetails.industryCode;
            	//$scope.workRatingoccupation = result.occupationDetails.occupation;
            	$scope.workRatingWithinOffcQue = result.occupationDetails.withinOfficeQue;
            	$scope.workRatingTertQue = result.occupationDetails.tertiaryQue;
            	$scope.workRatingOutsideOffice = result.occupationDetails.managementRoleQue;
            	$scope.workRatingHazardousQue = result.occupationDetails.hazardousQue;
            	$scope.gender = result.occupationDetails.gender;
            	$scope.workRatingAnnualSal = result.occupationDetails.salary;
            	$scope.otherOccupationObj.workRatingotherOccupation = result.occupationDetails.otherOccupation;

            	appNum = result.appNum;
            	dodCheck = result.dodCheck;
            	privacyCheck = result.privacyCheck;
            	updateAckCheck = result.ackCheckbox;
            	checkAppNum=true;

            	if(updateAckCheck){
            	    $timeout(function(){
            	    	$('#ackLabel').addClass('active');
    			   },1);
		        }

    			$('#dodLabel').addClass('active');
    			$('#privacyLabel').addClass('active');

//            	OccupationService.getOccupationList($scope.urlList.occupationUrl,"VICT",$scope.workRatingIndustry).then(function(res){
//            	//OccupationService.getOccupationList({fundId:"VICT", induCode:$scope.workRatingIndustry}, function(occupationList){
//                	$scope.OccupationList = res.data;
//                	var temp = $scope.OccupationList.filter(function(obj){
//                		return obj.occupationName == result.occupationDetails.occupation;
//                	});
//                	$scope.workRatingoccupation = temp[0].occupationName;
//                	$scope.getOccCategoryFromDB();
//                }, function(err){
//                	console.log("Error while fetching occupation options " + JSON.stringify(err));
//                });

            	$scope.togglePrivacy(true);
        		$scope.toggleContact(true);
            	$scope.toggleTwo(true);


        	},function(err){
        		//console.log("Something went wrong while retrieving"+ JSON.stringify(err));
        	})
        }

    $scope.generatePDF = function(){
    	$scope.continueWorkRatingPage(true);
    	$scope.saveDataForPersistence();
		var workRatingObjectPrint =  PersistenceService.getworkRatingCoverDetails();
		var workRatingOccDetailsPrint = PersistenceService.getWorkRatingCoverOccDetails();
	    var personalDetailsPrint = persoanlDetailService.getMemberDetails();
	    if(workRatingObjectPrint != null && workRatingOccDetailsPrint != null && personalDetailsPrint[0] != null){
	    	var details={};
			details.occupationDetails=workRatingOccDetailsPrint;
			var temp = angular.extend(details,workRatingObjectPrint)
    	    var printWorkRatingObject = angular.extend(temp,personalDetailsPrint[0]);
	    	auraResponseService.setResponse(printWorkRatingObject);

	    	printQuotePage.reqObj($scope.urlList.printQuotePage).then(function(response){
	    		 PersistenceService.setPDFLocation(response.data.clientPDFLocation);
	    		 $scope.downloadPDF();
	        },function(err){
	        	//console.log("Something went wrong while saving"+JSON.stringify(err));
	        });
	    }
    };

    $scope.downloadPDF = function(){
	    var pdfLocation =null;
	    var filename = null;
	   	var a = null;
    	pdfLocation = PersistenceService.getPDFLocation();
    	//console.log(pdfLocation+"pdfLocation");
    	filename = pdfLocation.substring(pdfLocation.lastIndexOf('/')+1);
  		a = document.createElement("a");
  	    document.body.appendChild(a);
  	    DownloadPDFService.download($scope.urlList.downloadUrl,pdfLocation).then(function(res){
  	    //DownloadPDFService.download({file_name: pdfLocation}, function(res){
				if (navigator.appVersion.toString().indexOf('.NET') > 0) { // for IE browser
					var extension = res.data.response.type;
		        	  extension = extension.substring(extension.lastIndexOf('/')+1);
		        	  filename = filename+"."+extension;
   	            window.navigator.msSaveBlob(res.data.response,filename);
   	        }else{
	  	        var fileURL = URL.createObjectURL(res.data.response);
	  	        a.href = fileURL;
	  	        a.download = filename;
	  	        a.click();
   	        }
  		}, function(err){
  			//console.log("Error downloading the PDF " + err);
  		});
  	};

}]);
