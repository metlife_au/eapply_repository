package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class ALIHealthDetailsPageObjects extends PageObjectLibrary{
		
	
	public static final By chklast3yrsNone = getObjectIdentifaction("ALI.health.3yrs.none.chkbox.css");	
	public static final By chkmedicaladviceNone = getObjectIdentifaction("ALI.health.diagnosed.none.chkbox.css");
	
	//Exclusion
	public static final By chklastdiagnosedBone = getObjectIdentifaction("ALI.health.abturhealth.medicaladvice.bone.checkbox.css");	
	public static final By chklastdiagnosedStrain = getObjectIdentifaction("ALI.health.abturhealth.medicaladvice.strain.checkbox.css");
	public static final By chklastdiagnosedAnkle = getObjectIdentifaction("ALI.health.abturhealth.medicaladvice.strain.ankles.checkbox.css");
	public static final By drplastdiagnosedImpact = getObjectIdentifaction("ALI.health.abturhealth.medicaladvice.strain.ankles.impact.drpdown.css");
	public static final By btnlastdiagnosedSelect = getObjectIdentifaction("ALI.health.abturhealth.medicaladvice.strain.ankles.impact.btn.css");
	public static final By rdlastdiagnosedCurrent = getObjectIdentifaction("ALI.health.abturhealth.medicaladvice.strain.ankles.impact.radio.css");
	
	// Loadings
	public static final By chklast3yrsLung = getObjectIdentifaction("ALI.health.3yrs.lung.chkbox.css");	
	public static final By chklast3yrsAsthma = getObjectIdentifaction("ALI.health.3yrs.lung.asthma.chkbox.css");
	public static final By rdlast3yrsAttention = getObjectIdentifaction("ALI.health.3yrs.lung.asthma.attention.yes.radio.css");
	public static final By rdlast3yrsCondition = getObjectIdentifaction("ALI.health.3yrs.lung.asthma.condition.moderate.radio.css");
	
	
	public static final By chkfamilyhistoryNone = getObjectIdentifaction("ALI.health.familyhistory.none.chkbox.css");
	public static final By chklifestyleNo = getObjectIdentifaction("ALI.health.lifestyle.no.radio.css");
	
	public static final By txtalchohol = getObjectIdentifaction("ALI.health.alchohol.txt.id");
	public static final By btnEnter= getObjectIdentifaction("ALI.health.enter.btn.css");
	
	public static final By rdalchoholreduceNo = getObjectIdentifaction("ALI.health.alchohol.reduce.no.radio.css");
	public static final By rdhivriskNo = getObjectIdentifaction("ALI.health.hiv.risk.no.radio.css");
	
	public static final By rdgeneralNo = getObjectIdentifaction("ALI.health.general.no.radio.css");
	public static final By rdinsuranceexclusionNo = getObjectIdentifaction("ALI.health.insurancehistory.no.radio.css");
	public static final By btnNext = getObjectIdentifaction("ALI.health.next.btn.css");

}
