/**
 * 
 */
package au.com.metlife.eapply.corporate.serviceImpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Service;

import au.com.metlife.eapply.EtoolKitException;
import au.com.metlife.eapply.b2b.utility.BlowfishUtil;
import au.com.metlife.eapply.bs.constants.MetlifeInstitutionalConstants;
import au.com.metlife.eapply.bs.model.Eapplication;
import au.com.metlife.eapply.bs.model.EapplicationRepository;
import au.com.metlife.eapply.bs.model.EapplyInput;
import au.com.metlife.eapply.bs.utility.EmailHelper;
import au.com.metlife.eapply.business.entity.CorpPartner;
import au.com.metlife.eapply.constants.AppErrorCodes;
import au.com.metlife.eapply.constants.EtoolkitConstants;
import au.com.metlife.eapply.corporate.business.CorpEntityRequest;
import au.com.metlife.eapply.corporate.business.CorpRequest;
import au.com.metlife.eapply.corporate.business.CorpResponse;
import au.com.metlife.eapply.corporate.business.DashboardResponse;
import au.com.metlife.eapply.corporate.service.CorpService;
import au.com.metlife.eapply.helper.CorpHelper;
import au.com.metlife.eapply.helper.CorporateHelper;
import au.com.metlife.eapply.quote.utility.SystemProperty;
import au.com.metlife.eapply.repositories.CorporateFundCatRepo;
import au.com.metlife.eapply.repositories.CorporateFundRepo;
import au.com.metlife.eapply.repositories.MemberFileUploadRepo;
import au.com.metlife.eapply.repositories.model.CorporateFund;
import au.com.metlife.eapply.repositories.model.CorporateFundCat;
import au.com.metlife.eapply.repositories.model.MemberFileUpload;

/**
 * @author 599063
 *
 */
@Service
public class CorpServiceImpl implements CorpService {

	private static final Logger logger = LoggerFactory.getLogger(CorpServiceImpl.class);
	SystemProperty sys;
	@Autowired
	EntityManager entity;
	@Autowired
	private CorporateFundRepo corpFundRepo;
	@Autowired
	private CorporateFundCatRepo corpFundCatRepo;
	@Autowired
	private EapplicationRepository eapplication;
	@Autowired
	private MemberFileUploadRepo memDetails;

	@Override
	@Transactional
	public CorpResponse getOnboardedFunds(CorpRequest request) {

		try {

			String fundCode = request.getParameter(EtoolkitConstants.FUND_CODE);
			String fundName = request.getParameter(EtoolkitConstants.FUND_NAME);
			if (null != fundCode && null != fundName) {
				/*
				 * 1. Retrieve fund details from using fund code
				 */
				CorporateFund corpFund = corpFundRepo.findByFundCode(fundCode);
				/*
				 * 2. Return response
				 */
				if (null != corpFund) {
					return CorpHelper.processFundInformationResponse(corpFund);
				} else {
					return CorpHelper.processNewFundDetails(fundCode, fundName);
				}
			} else {
				throw new EtoolKitException(EtoolkitConstants.ERR_BAD_INPUT_DATA,
						EtoolkitConstants.ERRMSG_INVALID_REQUEST, AppErrorCodes.BAD_REQUEST_MISSING_DATA);
			}

			/*
			 * 3. Handle exceptions
			 */
		} catch (EtoolKitException ex) {
			logger.error(EtoolkitConstants.ETOOLKITEXCEPTION + ex.getErrorMsg());
			throw ex;
		} catch (DataAccessResourceFailureException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_RESOURCE_FAILURE_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATABASE_NOT_AVAILABLE,
					EtoolkitConstants.ERRMSG_DATABASE_NOT_AVAILABLE, ex);
		} catch (DataAccessException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATA_ACCESS,
					String.format(EtoolkitConstants.ERRMSG_DATA_ACCESS, ex.getMessage()), ex);
		} catch (Exception ex) {
			logger.error(ex.toString() + "::" + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_UNCATEGORIZED, ex.getMessage(), ex);
		}
	}

	@Override
	@Transactional
	public CorpResponse saveCorporateFund(CorpRequest request) {

		try {
			/*
			 * 1. Get request data from CorpRequest
			 */
			String isEdit = request.getParameter(EtoolkitConstants.MODE_EDIT);
			String userId = request.getParameter(EtoolkitConstants.USERID);
			String fundCode = request.getParameter(EtoolkitConstants.FUND_CODE);

			CorpEntityRequest entityRequest = request.getRequestData();

			/*
			 * 2. Validate request data and do save or update
			 */
			if (validateRequestData(entityRequest) && fundCode != null && !fundCode.isEmpty() && userId != null
					&& !userId.isEmpty()) {
				if (isEdit.equalsIgnoreCase("true")) {
					/*
					 * Code for updating existing fund
					 */
					return updateCorporateFund(entityRequest, userId);

				} else {
					/*
					 * Code for new fund save
					 */
					return saveCorporateFund(entityRequest, fundCode, userId);
				}
			} else {
				throw new EtoolKitException(EtoolkitConstants.ERR_BAD_INPUT_DATA,
						EtoolkitConstants.ERRMSG_INVALID_REQUEST);
			}
		} catch (EtoolKitException ex) {
			logger.error(EtoolkitConstants.ETOOLKITEXCEPTION + ex.getErrorMsg());
			throw ex;
		} catch (NumberFormatException ex) {
			logger.error(EtoolkitConstants.NUMBER_FORMAT_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_BAD_INPUT_DATA, EtoolkitConstants.ERRMSG_INVALID_REQUEST,
					ex);
		} catch (DataAccessResourceFailureException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_RESOURCE_FAILURE_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATABASE_NOT_AVAILABLE,
					EtoolkitConstants.ERRMSG_DATABASE_NOT_AVAILABLE, ex);
		} catch (DataAccessException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATA_ACCESS,
					String.format(EtoolkitConstants.ERRMSG_DATA_ACCESS, ex.getMessage()), ex);
		} catch (Exception ex) {
			logger.error(ex.toString() + "::" + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_UNCATEGORIZED, ex.getMessage(), ex);
		}

	}

	/**
	 * Saves new fund details
	 * 
	 * @param entityRequest
	 * @param fundCode
	 * @param userId
	 * @return
	 */
	private CorpResponse saveCorporateFund(CorpEntityRequest request, String fundCode, String userId) {

		/*
		 * 1. Process the input payload data ans populate the DB entities
		 */
		CorporateFund corpFund = CorpHelper.processFundInformationRequest(request, fundCode, userId);
		/*
		 * 2. Save the DB entities to the database
		 */
		CorporateFund savedCorpFund = corpFundRepo.saveAndFlush(corpFund);
		/*
		 * 3. Process the saved DB entities to build the response and return it
		 */
		return CorpHelper.processFundInformationResponse(savedCorpFund);
	}

	/**
	 * Updates the existing fund details
	 * 
	 * @param entityRequest
	 * @param userId
	 * @return CorpResponse
	 */
	@Transactional
	private CorpResponse updateCorporateFund(CorpEntityRequest request, String userId) {
		CorporateFund fund = null;
		CorpPartner partner = request.getPartner();
		if (null != partner.getFundCode() && null != partner.getId()) {

			fund = corpFundRepo.findOne(new Integer(partner.getId()));

			if (null != fund) {
				CorporateFund corpFund = CorpHelper.processFundInformationRequestForUpdate(request, fund, userId);
				fund = corpFundRepo.saveAndFlush(corpFund);
			}

		}

		return CorpHelper.processFundInformationResponse(fund);
	}

	/**
	 * Method to validate if the request is not empty
	 * 
	 * @param request
	 * @return boolean
	 */
	private boolean validateRequestData(CorpEntityRequest request) {
		return (request != null && (request.getMember() != null || request.getPartner() != null));
	}

	@Override
	public CorpResponse getFundCategoryInformation(CorpRequest request) {
		try {

			String fundCode = request.getParameter(EtoolkitConstants.FUND_CODE);
			Integer fundCategory = Integer.valueOf(request.getParameter(EtoolkitConstants.FUND_CATEGORY));

			/*
			 * 1. Retrieve fund details from using fund code
			 */
			CorporateFund corpFund = corpFundRepo.findByFundCodeAndCorpFundCategoriesCategory(fundCode, fundCategory);

			List<CorporateFundCat> corpFundCategories = corpFundCatRepo.findByCorporateFundIdAndCategory(corpFund,
					fundCategory);
			if (null != corpFundCategories) {
				corpFund.setCorpFundCategories(corpFundCategories);
			}
			/*
			 * 2. Return response
			 */
			if (null != corpFund) {
				return CorpHelper.processFundInformationResponse(corpFund);
			} else {
				throw new EtoolKitException(EtoolkitConstants.ERR_BAD_INPUT_DATA,
						EtoolkitConstants.ERRMSG_INVALID_REQUEST);
			}

			/*
			 * 3. Handle exceptions
			 */
		} catch (EtoolKitException ex) {
			logger.error(EtoolkitConstants.ETOOLKITEXCEPTION + ex.getErrorMsg());
			throw ex;
		} catch (DataAccessResourceFailureException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_RESOURCE_FAILURE_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATABASE_NOT_AVAILABLE,
					EtoolkitConstants.ERRMSG_DATABASE_NOT_AVAILABLE, ex);
		} catch (DataAccessException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATA_ACCESS,
					String.format(EtoolkitConstants.ERRMSG_DATA_ACCESS, ex.getMessage()), ex);
		} catch (Exception ex) {
			logger.error(ex.toString() + "::" + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_UNCATEGORIZED, ex.getMessage(), ex);
		}
	}

	@Override
	public List<DashboardResponse> getMemberInformation(String fundCode, String status, String firstName,
			String lastName, String applicationNumber, Date fromDate, Date toDate) {
		try {
			/*
			 * 1. Retrieve Member details using Dashboard Search Parameters
			 */
			List<DashboardResponse> dashboardresp = new ArrayList<>();
			CriteriaBuilder qb = entity.getCriteriaBuilder();
			CriteriaQuery cq = qb.createQuery();
			Root<MemberFileUpload> mem = cq.from(MemberFileUpload.class);
			List<Predicate> predicates = new ArrayList<Predicate>();
			if (!CorporateHelper.isNullOrEmpty(fundCode)) {
				predicates.add(qb.equal(mem.get("fundId"), fundCode));
			}
			if (!CorporateHelper.isNullOrEmpty(status)) {
				if (status.equalsIgnoreCase(EtoolkitConstants.STATUS_ACTIVE)) {
					predicates.add(qb.isNull(mem.get("status")));
				}
				if (!status.equalsIgnoreCase(EtoolkitConstants.STATUS_ALL)
						&& !status.equalsIgnoreCase(EtoolkitConstants.STATUS_ACTIVE)) {
					predicates.add(qb.equal(mem.get("status"), status));
				}
			}
			if (!CorporateHelper.isNullOrEmpty(firstName)) {
				predicates.add(qb.like(qb.lower(mem.get("firstName")), "%"+firstName.toLowerCase()+"%"));
			}
			if (!CorporateHelper.isNullOrEmpty(lastName)) {
				predicates.add(qb.like(qb.lower(mem.get("surname")), "%"+lastName.toLowerCase()+"%"));
			}
			if (!CorporateHelper.isNullOrEmpty(applicationNumber)) {
				predicates.add(qb.equal(mem.get("applicationNumber"), applicationNumber));
			}

			if (fromDate != null && toDate != null) {
				if(fromDate.equals(toDate)) {
					toDate=addoneday(toDate);
					predicates.add(qb.between(mem.get("createdDate"), fromDate, toDate));
				}
				else {
					toDate=addoneday(toDate);
				predicates.add(qb.between(mem.get("createdDate"), fromDate, toDate));
				}
			}
			// query itself
			cq.select(mem).where(predicates.toArray(new Predicate[] {}));
			// execute query and fetch result
			List<MemberFileUpload> memberDetails  = entity.createQuery(cq).setMaxResults(500).getResultList();

			if (memberDetails != null) {
				for (MemberFileUpload memdetails : memberDetails) {
					DashboardResponse dashboardres = new DashboardResponse();
					dashboardres.setApplicationNumber(memdetails.getApplicationNumber());
					dashboardres.setFirstName(memdetails.getFirstName());
					dashboardres.setLastName(memdetails.getSurname());
					dashboardres.setDate(dateConverter(memdetails.getCreatedDate()));
					dashboardres.setEmail(memdetails.getEmail());
					dashboardres.setFundCode(memdetails.getFundId());
					if (CorporateHelper.isNullOrEmpty(memdetails.getStatus())) {
						dashboardres.setStatus(EtoolkitConstants.STATUS_ACTIVE);
					} else {
						dashboardres.setStatus(memdetails.getStatus());
					}
					dashboardresp.add(dashboardres);
				}
				return dashboardresp;
			} else {
				throw new EtoolKitException(EtoolkitConstants.ERR_BAD_INPUT_DATA,
						EtoolkitConstants.ERRMSG_INVALID_REQUEST);
			}

			/*
			 * 3. Handle exceptions
			 */
		} catch (EtoolKitException ex) {
			logger.error(EtoolkitConstants.ETOOLKITEXCEPTION + ex.getErrorMsg());
			throw ex;
		} catch (DataAccessResourceFailureException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_RESOURCE_FAILURE_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATABASE_NOT_AVAILABLE,
					EtoolkitConstants.ERRMSG_DATABASE_NOT_AVAILABLE, ex);
		} catch (DataAccessException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATA_ACCESS,
					String.format(EtoolkitConstants.ERRMSG_DATA_ACCESS, ex.getMessage()), ex);
		} catch (Exception ex) {
			logger.error(ex.toString() + "::" + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_UNCATEGORIZED, ex.getMessage(), ex);
		}
	}

	@Override
	@Transactional
	public DashboardResponse updateDB(String fundCode, String status, String firstName, String lastName,
			String applicationNumber) {
		try {
			/*
			 * 1. Update Member Application Status for Dashboard
			 */
			DashboardResponse dashboardres = new DashboardResponse();
			MemberFileUpload upload  = memDetails.findByApplicationNumberAndSurname(applicationNumber, lastName);
			Eapplication app =  eapplication.findByApplicationumberAndApplicationstatus(applicationNumber, status);
			if (upload != null && app != null) {
				if (upload.getApplicationNumber().equalsIgnoreCase(app.getApplicationumber())) {
					upload.setCreatedDate(new Timestamp(new Date().getTime()));
					upload.setStatus(EtoolkitConstants.STATUS_PENDING);
					app.setApplicationstatus(EtoolkitConstants.STATUS_PENDING);
					app.setCreatedate(new Timestamp(new Date().getTime()));
					upload = memDetails.saveAndFlush(upload);
					app = eapplication.saveAndFlush(app);
					if (upload != null && app != null) {
						dashboardres.setApplicationNumber(upload.getApplicationNumber());
						dashboardres.setStatus(upload.getStatus());
						dashboardres.setFirstName(upload.getFirstName());
						dashboardres.setLastName(upload.getSurname());
						dashboardres.setDate(dateConverter(upload.getCreatedDate()));
						dashboardres.setFundCode(upload.getFundId());
						dashboardres.setEmail(upload.getEmail());
					}
				}
			} else {
				if(upload!=null) {
				upload.setStatus(EtoolkitConstants.STATUS_ACTIVE);
				upload = memDetails.saveAndFlush(upload);
				dashboardres.setApplicationNumber(upload.getApplicationNumber());
				dashboardres.setStatus(upload.getStatus());
				dashboardres.setFirstName(upload.getFirstName());
				dashboardres.setLastName(upload.getSurname());
				dashboardres.setDate(dateConverter(upload.getCreatedDate()));
				dashboardres.setFundCode(upload.getFundId());
				dashboardres.setEmail(upload.getEmail());
				}
			}
			return dashboardres;

			/*
			 * 3. Handle exceptions
			 */
		} catch (EtoolKitException ex) {
			logger.error(EtoolkitConstants.ETOOLKITEXCEPTION + ex.getErrorMsg());
			throw ex;
		} catch (DataAccessResourceFailureException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_RESOURCE_FAILURE_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATABASE_NOT_AVAILABLE,
					EtoolkitConstants.ERRMSG_DATABASE_NOT_AVAILABLE, ex);
		} catch (DataAccessException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATA_ACCESS,
					String.format(EtoolkitConstants.ERRMSG_DATA_ACCESS, ex.getMessage()), ex);
		} catch (Exception ex) {
			logger.error(ex.toString() + "::" + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_UNCATEGORIZED, ex.getMessage(), ex);
		}
	}

	@Override
	public DashboardResponse sendEmail(String fundCode, String emailid, String firstName, String lastName,
			String applicationNumber, String authid) {
		String fundName = null;
		HashMap<String, String> placeHolders = new HashMap<>();
		String dollar = "$";
		String zero = "0.0";
		String inputIdentifier = null;
		String emailID = null;
		String inputdata = null;
		ArrayList mailList = new ArrayList();
		String sdataenc = null;
		String brokerNumber = null;
		au.com.metlife.eapply.email.MetLifeEmailVO eVO = new au.com.metlife.eapply.email.MetLifeEmailVO();
		Properties property = new Properties();
		InputStream url = null;
		String subject = null;
		NumberFormat formatter=null;
		formatter = new DecimalFormat("#,###,###.00");
		try {
			sys = SystemProperty.getInstance();
			File file = new File(sys.getProperty(EtoolkitConstants.DASHBOARD_FUNDCODE));
			url = new FileInputStream(file);
			property.load(url);
			logger.info("getPropertyFile>>" + sys.getProperty(EtoolkitConstants.DASHBOARD_FUNDCODE));
			/*
			 * 1. Update Member Application Status for Dashboard
			 */
			DashboardResponse dashboardres = new DashboardResponse();

			CorporateFund corpFund = corpFundRepo.findByFundCode(fundCode);
			MemberFileUpload upload = memDetails.findByApplicationNumberAndSurname(applicationNumber, lastName);
			if (corpFund != null) {
				fundName = corpFund.getPartnerName();
				brokerNumber=corpFund.getBrokercontactNum();
			}
			if (upload != null) {
				subject = property.getProperty(EtoolkitConstants.DASHBOARDMAIL_PROP_SUBJECT)
						.replace(EtoolkitConstants.MAIL_FUNDCODE, fundName);
				eVO.setFirstName(firstName);
				eVO.setLastName(lastName);
				eVO.setFundCode(upload.getFundId());
				eVO.setEmailTemplateId(property.getProperty(EtoolkitConstants.DASHBOARD__PROP_MAIL_BODY));
				eVO.setFromSender(property.getProperty(EtoolkitConstants.FROM_ADDRESS));
				placeHolders.put(EtoolkitConstants.DASHBOARDMAIL_SUBJECT, subject);
				placeHolders.put(EtoolkitConstants.DASHBOARD_MAIL_FUNDCODE, fundName);
				placeHolders.put(EtoolkitConstants.SMTP_HOST, property.getProperty(EtoolkitConstants.PROP_EMAIL_HOST));
				placeHolders.put(EtoolkitConstants.SMTP_PORT, property.getProperty(EtoolkitConstants.PROP_EMAIL_PORT));
				placeHolders.put(EtoolkitConstants.DASHBOARD_MAIL_APPLICATIONNUMBER, upload.getApplicationNumber());
				placeHolders.put(EtoolkitConstants.MEMBER_FIRSTNAME, eVO.getFirstName());
				placeHolders.put(EtoolkitConstants.MEMBER_LASTNAME, eVO.getLastName());
				if (null != upload.getTotalDeathCover() && upload.getTotalDeathCover() != 0) {
					placeHolders.put(EtoolkitConstants.MEMBER_TOTAL_DEATHCOVER,
							new StringBuilder().append(dollar).append(formatter.format(new BigDecimal(upload.getTotalDeathCover()).doubleValue())).toString());
				} else {
					eVO.setEmailTemplateId(property.getProperty(EtoolkitConstants.DASHBOARD__PROP_MAIL_BODY).replace(
							EtoolkitConstants.MEMBER_TOTAL_DEATHCOVER,
							new StringBuilder().append(dollar).append(zero).toString()));
					placeHolders.put(EtoolkitConstants.MEMBER_TOTAL_DEATHCOVER,
							new StringBuilder().append(dollar).append(zero).toString());
				}
				if (null != upload.getEligibleDeathCover() && upload.getEligibleDeathCover() != 0) {
					placeHolders.put(EtoolkitConstants.MEMBER_ELIGIBLE_DEATHCOVER,
							new StringBuilder().append(dollar).append(formatter.format(new BigDecimal(upload.getEligibleDeathCover()).doubleValue())).toString());
				} else {
					placeHolders.put(EtoolkitConstants.MEMBER_ELIGIBLE_DEATHCOVER,
							new StringBuilder().append(dollar).append(zero).toString());
				}
				if (null != upload.getTotalTpdCover() && upload.getTotalTpdCover() != 0) {
					placeHolders.put(EtoolkitConstants.MEMBER_TOTAL_TPDCOVER,
							new StringBuilder().append(dollar).append(formatter.format(new BigDecimal(upload.getTotalTpdCover()).doubleValue())).toString());
				} else {
					placeHolders.put(EtoolkitConstants.MEMBER_TOTAL_TPDCOVER,
							new StringBuilder().append(dollar).append(zero).toString());
				}
				if (null != upload.getEligibleTpdCover() && upload.getEligibleTpdCover() != 0) {
					placeHolders.put(EtoolkitConstants.MEMBER_ELIGIBLE_TPDCOVER,
							new StringBuilder().append(dollar).append(formatter.format(new BigDecimal(upload.getEligibleTpdCover()).doubleValue())).toString());
				} else {
					placeHolders.put(EtoolkitConstants.MEMBER_ELIGIBLE_TPDCOVER,
							new StringBuilder().append(dollar).append(zero).toString());
				}
				if (null != upload.getTotalIpCover() && upload.getTotalIpCover() != 0) {
					placeHolders.put(EtoolkitConstants.MEMBER_TOTAL_IPCOVER,
							new StringBuilder().append(dollar).append(formatter.format(new BigDecimal(upload.getTotalIpCover()).doubleValue())).toString());
				} else {
					placeHolders.put(EtoolkitConstants.MEMBER_TOTAL_IPCOVER,
							new StringBuilder().append(dollar).append(zero).toString());
				}
				if (null != upload.getEligibleIpCover() && upload.getEligibleIpCover() != 0) {
					placeHolders.put(EtoolkitConstants.MEMBER_ELIGIBLE_IPCOVER,
							new StringBuilder().append(dollar).append(formatter.format(new BigDecimal(upload.getEligibleIpCover()).doubleValue())).toString());
				} else {
					placeHolders.put(EtoolkitConstants.MEMBER_ELIGIBLE_IPCOVER,
							new StringBuilder().append(dollar).append(zero).toString());
				}

				placeHolders.forEach((k, v) -> logger.info("Key = " + k + ", Value = " + v));
				eVO.setSubject(subject);
				if (!CorporateHelper.isNullOrEmpty(upload.getApplicationNumber()) && null != upload.getDob()
						&& !CorporateHelper.isNullOrEmpty(upload.getSurname())) {
					String sdata = new StringBuilder().append(upload.getApplicationNumber()).append(":").append(upload.getDob())
							.append(":").append(upload.getSurname()).toString();
					sdataenc = BlowfishUtil.encrypt(MetlifeInstitutionalConstants.GEN_KEY, sdata);
				}
				if (!CorporateHelper.isNullOrEmpty(upload.getFundId())) {
					inputIdentifier = BlowfishUtil.encrypt(MetlifeInstitutionalConstants.GEN_KEY, upload.getFundId());
				}
				inputdata = BlowfishUtil.encrypt(MetlifeInstitutionalConstants.GEN_KEY, String.valueOf(upload.getId()));

				if (property.getProperty(EtoolkitConstants.APPLICATION_URL).contains(EtoolkitConstants.URl_INPUT_IDN)
						&& null != inputIdentifier && null != inputdata && null != sdataenc) {
					eVO.setEmailTemplateId(property.getProperty(EtoolkitConstants.DASHBOARDBODY)
							.replace(EtoolkitConstants.BODY_URL,
									property.getProperty(EtoolkitConstants.APPLICATION_URL)
											.replace(EtoolkitConstants.URl_INPUT_IDN, inputIdentifier)
											.replace(EtoolkitConstants.URL_INPUT_DATA, inputdata)
											.replace(EtoolkitConstants.URL_S_DATA, sdataenc))
							.replace(EtoolkitConstants.DASHBOARD_TOTAL_DEATH_COVER,
									placeHolders.get(EtoolkitConstants.MEMBER_TOTAL_DEATHCOVER))
							.replace(EtoolkitConstants.DASHBOARD_ELIGIBLE_DEATH_COVER,
									placeHolders.get(EtoolkitConstants.MEMBER_ELIGIBLE_DEATHCOVER))
							.replace(EtoolkitConstants.DASHBOARD_TOTAL_TPD_COVER,
									placeHolders.get(EtoolkitConstants.MEMBER_TOTAL_TPDCOVER))
							.replace(EtoolkitConstants.DASHBOARD_ELIGIBLE_TPD_COVER,
									placeHolders.get(EtoolkitConstants.MEMBER_ELIGIBLE_TPDCOVER))
							.replace(EtoolkitConstants.DASHBOARD_TOTAL_IP_COVER,
									placeHolders.get(EtoolkitConstants.MEMBER_TOTAL_IPCOVER))
							.replace(EtoolkitConstants.DASHBOARD_ELIGIBLE_IP_COVER,
									placeHolders.get(EtoolkitConstants.MEMBER_ELIGIBLE_IPCOVER))
							.replace(EtoolkitConstants.MAIL_FUNDCODE, fundName)
					.replace(EtoolkitConstants.MAIL_BROKERNUMBER, brokerNumber));
				}
				placeHolders.put(EtoolkitConstants.URL, property.getProperty(EtoolkitConstants.APPLICATION_URL));
				eVO.setFromSender(property.getProperty(EtoolkitConstants.FROM_ADDRESS));
				emailID = upload.getEmail();
				mailList.add(emailID);
				eVO.setToRecipents(mailList);
				eVO.setHtmlEmails(true);
				eVO.setCcReciepent(null);
				eVO.setEmailDataElementMap(placeHolders);

				String token = EmailHelper.callEmailService(eVO, authid, property);
				if (!CorporateHelper.isNullOrEmpty(token)) {
					dashboardres.setStatus(upload.getStatus());
					dashboardres.setFirstName(upload.getFirstName());
					dashboardres.setLastName(upload.getSurname());
					dashboardres.setDate(dateConverter(upload.getCreatedDate()));
					dashboardres.setFundCode(upload.getFundId());
					dashboardres.setEmail(upload.getEmail());
				}
			}
			return dashboardres;

			/*
			 * 3. Handle exceptions
			 */
		} catch (EtoolKitException ex) {
			logger.error(EtoolkitConstants.ETOOLKITEXCEPTION + ex.getErrorMsg());
			throw ex;
		} catch (DataAccessResourceFailureException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_RESOURCE_FAILURE_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATABASE_NOT_AVAILABLE,
					EtoolkitConstants.ERRMSG_DATABASE_NOT_AVAILABLE, ex);
		} catch (DataAccessException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATA_ACCESS,
					String.format(EtoolkitConstants.ERRMSG_DATA_ACCESS, ex.getMessage()), ex);
		} catch (Exception ex) {
			logger.error(ex.toString() + "::" + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_UNCATEGORIZED, ex.getMessage(), ex);
		}
	}

	@Override
	@Transactional
	public DashboardResponse saveCorporateApp(Long appNum,String lastName) {

		try {
			DashboardResponse response = new DashboardResponse();
			MemberFileUpload upload = memDetails.findByApplicationNumberAndSurname(appNum.toString(),lastName);
			if(upload!=null) {
			upload.setStatus(EtoolkitConstants.STATUS_PENDING);
			upload.setCreatedDate(new Timestamp(new Date().getTime()));
			upload = memDetails.saveAndFlush(upload);
			response.setStatus(upload.getStatus());
			response.setFirstName(upload.getFirstName());
			response.setLastName(upload.getSurname());
			response.setDate(dateConverter(upload.getCreatedDate()));
			response.setFundCode(upload.getFundId());
			response.setEmail(upload.getEmail());
			}
			return response;

			/*
			 * 3. Handle exceptions
			 */
		} catch (EtoolKitException ex) {
			logger.error(EtoolkitConstants.ETOOLKITEXCEPTION + ex.getErrorMsg());
			throw ex;
		} catch (DataAccessResourceFailureException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_RESOURCE_FAILURE_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATABASE_NOT_AVAILABLE,
					EtoolkitConstants.ERRMSG_DATABASE_NOT_AVAILABLE, ex);
		} catch (DataAccessException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATA_ACCESS,
					String.format(EtoolkitConstants.ERRMSG_DATA_ACCESS, ex.getMessage()), ex);
		} catch (Exception ex) {
			logger.error(ex.toString() + "::" + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_UNCATEGORIZED, ex.getMessage(), ex);
		}
	}

	@Override
	@Transactional
	public DashboardResponse submitCorporateApp(Long appNum,String lastName) {

		try {
			DashboardResponse response = new DashboardResponse();
			MemberFileUpload upload = memDetails.findByApplicationNumberAndSurname(appNum.toString(),lastName);
			if(upload!=null) {
			upload.setStatus(EtoolkitConstants.STATUS_SUBMITTED);
			upload.setCreatedDate(new Timestamp(new Date().getTime()));
			upload = memDetails.saveAndFlush(upload);
				response.setStatus(upload.getStatus());
				response.setFirstName(upload.getFirstName());
				response.setLastName(upload.getSurname());
				response.setDate(dateConverter(upload.getCreatedDate()));
				response.setFundCode(upload.getFundId());
				response.setEmail(upload.getEmail());
			}
			return response;

			/*
			 * 3. Handle exceptions
			 */
		} catch (EtoolKitException ex) {
			logger.error(EtoolkitConstants.ETOOLKITEXCEPTION + ex.getErrorMsg());
			throw ex;
		} catch (DataAccessResourceFailureException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_RESOURCE_FAILURE_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATABASE_NOT_AVAILABLE,
					EtoolkitConstants.ERRMSG_DATABASE_NOT_AVAILABLE, ex);
		} catch (DataAccessException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATA_ACCESS,
					String.format(EtoolkitConstants.ERRMSG_DATA_ACCESS, ex.getMessage()), ex);
		} catch (Exception ex) {
			logger.error(ex.toString() + "::" + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_UNCATEGORIZED, ex.getMessage(), ex);
		}
	}

	@Override
	@Transactional
	public CorpResponse expireCorpApp(String appNum) {

		try {
			CorpResponse response = new CorpResponse();
			MemberFileUpload upload = memDetails.findByApplicationNumber(appNum);
			if (upload != null) {
				upload.setStatus(EtoolkitConstants.STATUS_EXPIRED);
				upload.setCreatedDate(new Timestamp(new Date().getTime()));
				memDetails.saveAndFlush(upload);
			}
			return response;

			/*
			 * 3. Handle exceptions
			 */
		} catch (EtoolKitException ex) {
			logger.error(EtoolkitConstants.ETOOLKITEXCEPTION + ex.getErrorMsg());
			throw ex;
		} catch (DataAccessResourceFailureException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_RESOURCE_FAILURE_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATABASE_NOT_AVAILABLE,
					EtoolkitConstants.ERRMSG_DATABASE_NOT_AVAILABLE, ex);
		} catch (DataAccessException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATA_ACCESS,
					String.format(EtoolkitConstants.ERRMSG_DATA_ACCESS, ex.getMessage()), ex);
		} catch (Exception ex) {
			logger.error(ex.toString() + "::" + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_UNCATEGORIZED, ex.getMessage(), ex);
		}
	}

	public String dateConverter(Date today) {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String reportDate = df.format(today);
		return reportDate;
	}
	public java.sql.Date addoneday(Date toDate) {
	Calendar cal = Calendar.getInstance();
	cal.setTime(toDate);
	cal.add(Calendar.DAY_OF_YEAR,1);
	cal.set(Calendar.HOUR_OF_DAY, 0);
	cal.set(Calendar.MINUTE, 0);
	cal.set(Calendar.SECOND, 0);
	cal.set(Calendar.MILLISECOND, 0);
	return	new java.sql.Date(cal.getTimeInMillis());
	}
	
	public CorporateFund getFundDetails(String fundCode) {
		CorporateFund corpFund = corpFundRepo.findByFundCode(fundCode);
		if(corpFund!=null) {
			return corpFund;
		}
		else {
			throw new EtoolKitException(EtoolkitConstants.ERR_BAD_INPUT_DATA,
					EtoolkitConstants.ERRMSG_INVALID_REQUEST, AppErrorCodes.BAD_REQUEST_MISSING_DATA);
		}

	}
	
	public CorporateFund getFundCategoryDetails(String fundCode,String category) {
		CorporateFund corpFund = corpFundRepo.findByFundCode(fundCode);
		
		List<CorporateFundCat> corpFundCategories = corpFundCatRepo.findByCorporateFundIdAndCategory(corpFund,
				Integer.valueOf(category));
		if (null != corpFundCategories) {
			corpFund.setCorpFundCategories(corpFundCategories);
		}
		if(corpFund!=null) {
			return corpFund;
		}
		else {
			throw new EtoolKitException(EtoolkitConstants.ERR_BAD_INPUT_DATA,
					EtoolkitConstants.ERRMSG_INVALID_REQUEST, AppErrorCodes.BAD_REQUEST_MISSING_DATA);
		}

	}
	public CorpResponse sendSaveEmail(EapplyInput eapplyInput,String authid) {
		String fundName = null;
		HashMap<String, String> placeHolders = new HashMap<>();
		String dollar = "$";
		String zero = "0.0";
		String inputIdentifier = null;
		String emailID = null;
		String inputdata = null;
		ArrayList mailList = new ArrayList();
		String sdataenc = null;
		au.com.metlife.eapply.email.MetLifeEmailVO eVO = new au.com.metlife.eapply.email.MetLifeEmailVO();
		Properties property = new Properties();
		InputStream url = null;
		String subject = null;
		String lastName= null;
		String firstName= null;
		NumberFormat formatter=null;
		formatter = new DecimalFormat("#,###,###.00");
	
		try {
			sys = SystemProperty.getInstance();
			File file = new File(sys.getProperty(EtoolkitConstants.DASHBOARD_FUNDCODE));
			url = new FileInputStream(file);
			property.load(url);
			logger.info("getPropertyFile>>" + sys.getProperty(EtoolkitConstants.DASHBOARD_FUNDCODE));
			/*
			 * 1. Update Member Application Status for Dashboard
			 */
			CorpResponse response = new CorpResponse();

			CorporateFund corpFund = corpFundRepo.findByFundCode(eapplyInput.getCorpFundCode());
			 if(null!=eapplyInput.getPersonalDetails().getLastName()) {
				 lastName=eapplyInput.getPersonalDetails().getLastName();
			 }
			 else {
				 lastName= eapplyInput.getLastName();
			 }
			 if(null!=eapplyInput.getPersonalDetails().getFirstName()) {
				 firstName=eapplyInput.getPersonalDetails().getFirstName();
			 }
			 else {
				 firstName=eapplyInput.getFirstName();
			 }
			MemberFileUpload upload = memDetails.findByApplicationNumberAndSurname(eapplyInput.getAppNum().toString(), lastName);
			if (corpFund != null) {
				fundName = corpFund.getPartnerName();
			}
			if (upload != null) {
				subject = property.getProperty(EtoolkitConstants.SAVE_EMAIL_SUBJECT)
						.replace(EtoolkitConstants.MAIL_FUNDCODE, fundName);
				eVO.setFirstName(firstName);
				eVO.setLastName(lastName);
				eVO.setFundCode(upload.getFundId());
				eVO.setEmailTemplateId(property.getProperty(EtoolkitConstants.SAVE_EMAIL));
				eVO.setFromSender(property.getProperty(EtoolkitConstants.SAVE_FROM_SENDER));
				placeHolders.put(EtoolkitConstants.DASHBOARDMAIL_SUBJECT, subject);
				placeHolders.put(EtoolkitConstants.DASHBOARD_MAIL_FUNDCODE, fundName);
				placeHolders.put(EtoolkitConstants.SMTP_HOST, property.getProperty(EtoolkitConstants.PROP_EMAIL_HOST));
				placeHolders.put(EtoolkitConstants.SMTP_PORT, property.getProperty(EtoolkitConstants.PROP_EMAIL_PORT));
				placeHolders.put(EtoolkitConstants.DASHBOARD_MAIL_APPLICATIONNUMBER, upload.getApplicationNumber());
				placeHolders.put(EtoolkitConstants.MEMBER_FIRSTNAME, eVO.getFirstName());
				placeHolders.put(EtoolkitConstants.MEMBER_LASTNAME, eVO.getLastName());
				if (null != upload.getTotalDeathCover() && upload.getTotalDeathCover() != 0) {
					placeHolders.put(EtoolkitConstants.MEMBER_TOTAL_DEATHCOVER,
							new StringBuilder().append(dollar).append(formatter.format(new BigDecimal(upload.getTotalDeathCover()).doubleValue())).toString());
				} else {
					eVO.setEmailTemplateId(property.getProperty(EtoolkitConstants.DASHBOARD__PROP_MAIL_BODY).replace(
							EtoolkitConstants.MEMBER_TOTAL_DEATHCOVER,
							new StringBuilder().append(dollar).append(zero).toString()));
					placeHolders.put(EtoolkitConstants.MEMBER_TOTAL_DEATHCOVER,
							new StringBuilder().append(dollar).append(zero).toString());
				}
				if (null != upload.getEligibleDeathCover() && upload.getEligibleDeathCover() != 0) {
					placeHolders.put(EtoolkitConstants.MEMBER_ELIGIBLE_DEATHCOVER,
							new StringBuilder().append(dollar).append(formatter.format(new BigDecimal(upload.getEligibleDeathCover()).doubleValue())).toString());
				} else {
					placeHolders.put(EtoolkitConstants.MEMBER_ELIGIBLE_DEATHCOVER,
							new StringBuilder().append(dollar).append(zero).toString());
				}
				if (null != upload.getTotalTpdCover() && upload.getTotalTpdCover() != 0) {
					placeHolders.put(EtoolkitConstants.MEMBER_TOTAL_TPDCOVER,
							new StringBuilder().append(dollar).append(formatter.format(new BigDecimal(upload.getTotalTpdCover()).doubleValue())).toString());
				} else {
					placeHolders.put(EtoolkitConstants.MEMBER_TOTAL_TPDCOVER,
							new StringBuilder().append(dollar).append(zero).toString());
				}
				if (null != upload.getEligibleTpdCover() && upload.getEligibleTpdCover() != 0) {
					placeHolders.put(EtoolkitConstants.MEMBER_ELIGIBLE_TPDCOVER,
							new StringBuilder().append(dollar).append(formatter.format(new BigDecimal(upload.getEligibleTpdCover()).doubleValue())).toString());
				} else {
					placeHolders.put(EtoolkitConstants.MEMBER_ELIGIBLE_TPDCOVER,
							new StringBuilder().append(dollar).append(zero).toString());
				}
				if (null != upload.getTotalIpCover() && upload.getTotalIpCover() != 0) {
					placeHolders.put(EtoolkitConstants.MEMBER_TOTAL_IPCOVER,
							new StringBuilder().append(dollar).append(formatter.format(new BigDecimal(upload.getTotalIpCover()).doubleValue())).toString());
				} else {
					placeHolders.put(EtoolkitConstants.MEMBER_TOTAL_IPCOVER,
							new StringBuilder().append(dollar).append(zero).toString());
				}
				if (null != upload.getEligibleIpCover() && upload.getEligibleIpCover() != 0) {
					placeHolders.put(EtoolkitConstants.MEMBER_ELIGIBLE_IPCOVER,
							new StringBuilder().append(dollar).append(formatter.format(new BigDecimal(upload.getEligibleIpCover()).doubleValue())).toString());
				} else {
					placeHolders.put(EtoolkitConstants.MEMBER_ELIGIBLE_IPCOVER,
							new StringBuilder().append(dollar).append(zero).toString());
				}

				placeHolders.forEach((k, v) -> logger.info("Key = " + k + ", Value = " + v));
				eVO.setSubject(subject);
				if (!CorporateHelper.isNullOrEmpty(upload.getApplicationNumber()) && null != upload.getDob()
						&& !CorporateHelper.isNullOrEmpty(upload.getSurname())) {
					String sdata = new StringBuilder().append(upload.getApplicationNumber()).append(":").append(upload.getDob())
							.append(":").append(upload.getSurname()).toString();
					sdataenc = BlowfishUtil.encrypt(MetlifeInstitutionalConstants.GEN_KEY, sdata);
				}
				if (!CorporateHelper.isNullOrEmpty(upload.getFundId())) {
					inputIdentifier = BlowfishUtil.encrypt(MetlifeInstitutionalConstants.GEN_KEY, upload.getFundId());
				}
				inputdata = BlowfishUtil.encrypt(MetlifeInstitutionalConstants.GEN_KEY, String.valueOf(upload.getId()));

				if (property.getProperty(EtoolkitConstants.EVIEW_APPLICATION_URL).contains(EtoolkitConstants.URl_INPUT_IDN)
						&& null != inputIdentifier && null != inputdata && null != sdataenc) {
					eVO.setEmailTemplateId(property.getProperty(EtoolkitConstants.SAVE_EMAIL)
							.replace(EtoolkitConstants.BODY_URL,
									property.getProperty(EtoolkitConstants.APPLICATION_URL)
											.replace(EtoolkitConstants.URl_INPUT_IDN, inputIdentifier)
											.replace(EtoolkitConstants.URL_INPUT_DATA, inputdata)
											.replace(EtoolkitConstants.URL_S_DATA, sdataenc))
							.replace(EtoolkitConstants.DASHBOARD_TOTAL_DEATH_COVER,
									placeHolders.get(EtoolkitConstants.MEMBER_TOTAL_DEATHCOVER))
							.replace(EtoolkitConstants.DASHBOARD_ELIGIBLE_DEATH_COVER,
									placeHolders.get(EtoolkitConstants.MEMBER_ELIGIBLE_DEATHCOVER))
							.replace(EtoolkitConstants.DASHBOARD_TOTAL_TPD_COVER,
									placeHolders.get(EtoolkitConstants.MEMBER_TOTAL_TPDCOVER))
							.replace(EtoolkitConstants.DASHBOARD_ELIGIBLE_TPD_COVER,
									placeHolders.get(EtoolkitConstants.MEMBER_ELIGIBLE_TPDCOVER))
							.replace(EtoolkitConstants.DASHBOARD_TOTAL_IP_COVER,
									placeHolders.get(EtoolkitConstants.MEMBER_TOTAL_IPCOVER))
							.replace(EtoolkitConstants.DASHBOARD_ELIGIBLE_IP_COVER,
									placeHolders.get(EtoolkitConstants.MEMBER_ELIGIBLE_IPCOVER))
							.replace(EtoolkitConstants.MAIL_FUNDCODE, fundName).replace(EtoolkitConstants.BROKER_NUMBER, corpFund.getBrokercontactNum().toString()));
				}
				placeHolders.put(EtoolkitConstants.URL, property.getProperty(EtoolkitConstants.APPLICATION_URL));
				eVO.setFromSender(property.getProperty(EtoolkitConstants.SAVE_FROM_SENDER));
				emailID = upload.getEmail();
				mailList.add(emailID);
				eVO.setToRecipents(mailList);
				eVO.setCcReciepent(null);
				eVO.setHtmlEmails(true);
				eVO.setEmailDataElementMap(placeHolders);
				String token = EmailHelper.callEmailService(eVO, authid, property);
			}
			else {
				subject = property.getProperty(EtoolkitConstants.SAVE_EMAIL_SUBJECT)
						.replace(EtoolkitConstants.MAIL_FUNDCODE, fundName);
				eVO.setFirstName(firstName);
				eVO.setLastName(lastName);
				eVO.setFundCode(eapplyInput.getCorpFundCode());
				eVO.setEmailTemplateId(property.getProperty(EtoolkitConstants.SAVE_EMAIL));
				eVO.setFromSender(property.getProperty(EtoolkitConstants.SAVE_FROM_SENDER));
				placeHolders.put(EtoolkitConstants.DASHBOARDMAIL_SUBJECT, subject);
				placeHolders.put(EtoolkitConstants.DASHBOARD_MAIL_FUNDCODE, fundName);
				placeHolders.put(EtoolkitConstants.SMTP_HOST, property.getProperty(EtoolkitConstants.PROP_EMAIL_HOST));
				placeHolders.put(EtoolkitConstants.SMTP_PORT, property.getProperty(EtoolkitConstants.PROP_EMAIL_PORT));
				placeHolders.put(EtoolkitConstants.DASHBOARD_MAIL_APPLICATIONNUMBER, eapplyInput.getAppNum().toString());
				placeHolders.put(EtoolkitConstants.MEMBER_FIRSTNAME, eVO.getFirstName());
				placeHolders.put(EtoolkitConstants.MEMBER_LASTNAME, eVO.getLastName());
				placeHolders.forEach((k, v) -> logger.info("Key = " + k + ", Value = " + v));
				eVO.setSubject(subject);
				if (!CorporateHelper.isNullOrEmpty(eapplyInput.getAppNum().toString()) && null != eapplyInput.getDob()
						&& !CorporateHelper.isNullOrEmpty(lastName)) {
					String sdata = new StringBuilder().append(eapplyInput.getAppNum().toString()).append(":").append(eapplyInput.getDob())
							.append(":").append(lastName).append(":").append(firstName).toString();
					sdataenc = BlowfishUtil.encrypt(MetlifeInstitutionalConstants.GEN_KEY, sdata);
				}
				if (!CorporateHelper.isNullOrEmpty(eapplyInput.getCorpFundCode())) {
					inputIdentifier = BlowfishUtil.encrypt(MetlifeInstitutionalConstants.GEN_KEY, new StringBuilder().append(eapplyInput.getCorpFundCode()).append(":").append(eapplyInput.getProductnumber()).append(":").append(eapplyInput.getContactDetails().getFundEmailAddress()).toString());
				}
				inputdata = BlowfishUtil.encrypt(MetlifeInstitutionalConstants.GEN_KEY, String.valueOf(eapplyInput.getClientRefNumber()));

				if (property.getProperty(EtoolkitConstants.EVIEW_APPLICATION_URL).contains(EtoolkitConstants.URl_INPUT_IDN)
						&& null != inputIdentifier && null != inputdata && null != sdataenc) {
					eVO.setEmailTemplateId(property.getProperty(EtoolkitConstants.SAVE_EMAIL)
							.replace(EtoolkitConstants.BODY_URL,
									property.getProperty(EtoolkitConstants.EVIEW_APPLICATION_URL)
											.replace(EtoolkitConstants.URl_INPUT_IDN, inputIdentifier)
											.replace(EtoolkitConstants.URL_INPUT_DATA, inputdata)
											.replace(EtoolkitConstants.URL_S_DATA, sdataenc))
							.replace(EtoolkitConstants.MAIL_FUNDCODE, fundName)
							.replace(EtoolkitConstants.BROKER_NUMBER, corpFund.getBrokercontactNum()));
				}
				placeHolders.put(EtoolkitConstants.URL, property.getProperty(EtoolkitConstants.APPLICATION_URL));
				eVO.setFromSender(property.getProperty(EtoolkitConstants.SAVE_FROM_SENDER));
				emailID = eapplyInput.getEmail();
				mailList.add(emailID);
				eVO.setToRecipents(mailList);
				eVO.setCcReciepent(null);
				eVO.setHtmlEmails(true);
				eVO.setEmailDataElementMap(placeHolders);
			String token = EmailHelper.callEmailService(eVO, authid, property);
			}
			return response;
		} 
		/*
		 * 3. Handle exceptions
		 */
		
		catch (EtoolKitException ex) {
			logger.error(EtoolkitConstants.ETOOLKITEXCEPTION + ex.getErrorMsg());
			throw ex;
		} catch (DataAccessResourceFailureException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_RESOURCE_FAILURE_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATABASE_NOT_AVAILABLE,
					EtoolkitConstants.ERRMSG_DATABASE_NOT_AVAILABLE, ex);
		} catch (DataAccessException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATA_ACCESS,
					String.format(EtoolkitConstants.ERRMSG_DATA_ACCESS, ex.getMessage()), ex);
		} catch (Exception ex) {
			logger.error(ex.toString() + "::" + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_UNCATEGORIZED, ex.getMessage(), ex);
		}
		
	}
	public CorpResponse sendFundSaveEmail(EapplyInput eapplyInput,String authid) {
		ArrayList mailList = new ArrayList();
		String fundName = null;
		au.com.metlife.eapply.email.MetLifeEmailVO eVO = new au.com.metlife.eapply.email.MetLifeEmailVO();
		Properties property = new Properties();
		InputStream url = null;
		String subject = null;
		String emailID = null;
		String lastName=null;
		String firstName=null;
		HashMap<String, String> placeHolders = new HashMap<>();
		
		try {
			sys = SystemProperty.getInstance();
			File file = new File(sys.getProperty(EtoolkitConstants.DASHBOARD_FUNDCODE));
			url = new FileInputStream(file);
			property.load(url);
			logger.info("getPropertyFile>>" + sys.getProperty(EtoolkitConstants.DASHBOARD_FUNDCODE));
			CorpResponse response = new CorpResponse();
			/*
			 * 1. Update Member Application Status for Dashboard
			 */
			 if(null!=eapplyInput.getPersonalDetails().getLastName()) {
				 lastName=eapplyInput.getPersonalDetails().getLastName();
			 }
			 else {
				 lastName= eapplyInput.getLastName();
			 }
			 if(null!=eapplyInput.getPersonalDetails().getFirstName()) {
				 firstName=eapplyInput.getPersonalDetails().getFirstName();
			 }
			 else {
				 firstName=eapplyInput.getFirstName();
			 }
			CorporateFund corpFund = corpFundRepo.findByFundCode(eapplyInput.getCorpFundCode());
			MemberFileUpload upload = memDetails.findByApplicationNumberAndSurname(eapplyInput.getAppNum().toString(), lastName);
			if (corpFund != null) {
				fundName = corpFund.getPartnerName();
			}
			if (upload != null) {
				subject = property.getProperty(EtoolkitConstants.SAVE_EMAIL_SUBJECT)
						.replace(EtoolkitConstants.MAIL_FUNDCODE, fundName);
				eVO.setFirstName(firstName);
				eVO.setLastName(lastName);
				eVO.setFundCode(upload.getFundId());
				eVO.setEmailTemplateId(property.getProperty(EtoolkitConstants.SAVE_EMAIL_BODY_BROKER));
				eVO.setFromSender(property.getProperty(EtoolkitConstants.SAVE_FROM_SENDER));
				placeHolders.put(EtoolkitConstants.DASHBOARDMAIL_SUBJECT, subject);
				placeHolders.put(EtoolkitConstants.DASHBOARD_MAIL_FUNDCODE, fundName);
				placeHolders.put(EtoolkitConstants.SMTP_HOST, property.getProperty(EtoolkitConstants.PROP_EMAIL_HOST));
				placeHolders.put(EtoolkitConstants.SMTP_PORT, property.getProperty(EtoolkitConstants.PROP_EMAIL_PORT));
				placeHolders.put(EtoolkitConstants.DASHBOARD_MAIL_APPLICATIONNUMBER,  upload.getApplicationNumber());
				placeHolders.put(EtoolkitConstants.MEMBER_FIRSTNAME, eVO.getFirstName());
				placeHolders.put(EtoolkitConstants.MEMBER_LASTNAME, eVO.getLastName());
				placeHolders.forEach((k, v) -> logger.info("Key = " + k + ", Value = " + v));
				eVO.setSubject(subject);
				eVO.setFromSender(property.getProperty(EtoolkitConstants.SAVE_FROM_SENDER));
				emailID = upload.getBrokerEmail();
				mailList.add(emailID);
				eVO.setToRecipents(mailList);
				eVO.setCcReciepent(null);
				eVO.setHtmlEmails(true);
				eVO.setEmailDataElementMap(placeHolders);
				String token = EmailHelper.callEmailService(eVO, authid, property);
			}
			else {
				subject = property.getProperty(EtoolkitConstants.SAVE_EMAIL_SUBJECT)
						.replace(EtoolkitConstants.MAIL_FUNDCODE, fundName);
				eVO.setFirstName(firstName);
				eVO.setLastName(lastName);
				eVO.setFundCode(eapplyInput.getCorpFundCode());
				eVO.setEmailTemplateId(property.getProperty(EtoolkitConstants.SAVE_EMAIL_BODY_BROKER));
				eVO.setFromSender(property.getProperty(EtoolkitConstants.SAVE_FROM_SENDER));
				placeHolders.put(EtoolkitConstants.DASHBOARDMAIL_SUBJECT, subject);
				placeHolders.put(EtoolkitConstants.DASHBOARD_MAIL_FUNDCODE, fundName);
				placeHolders.put(EtoolkitConstants.SMTP_HOST, property.getProperty(EtoolkitConstants.PROP_EMAIL_HOST));
				placeHolders.put(EtoolkitConstants.SMTP_PORT, property.getProperty(EtoolkitConstants.PROP_EMAIL_PORT));
				placeHolders.put(EtoolkitConstants.DASHBOARD_MAIL_APPLICATIONNUMBER, eapplyInput.getAppNum().toString());
				placeHolders.put(EtoolkitConstants.MEMBER_FIRSTNAME, eVO.getFirstName());
				placeHolders.put(EtoolkitConstants.MEMBER_LASTNAME, eVO.getLastName());
				placeHolders.forEach((k, v) -> logger.info("Key = " + k + ", Value = " + v));
				eVO.setSubject(subject);
				eVO.setFromSender(property.getProperty(EtoolkitConstants.SAVE_FROM_SENDER));
				emailID = eapplyInput.getContactDetails().getFundEmailAddress();
				mailList.add(emailID);
				eVO.setToRecipents(mailList);
				eVO.setCcReciepent(null);
				eVO.setHtmlEmails(true);
				eVO.setEmailDataElementMap(placeHolders);
				String token = EmailHelper.callEmailService(eVO, authid, property);
			}
			return response;
	}

		catch (EtoolKitException ex) {
			logger.error(EtoolkitConstants.ETOOLKITEXCEPTION + ex.getErrorMsg());
			throw ex;
		} catch (DataAccessResourceFailureException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_RESOURCE_FAILURE_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATABASE_NOT_AVAILABLE,
					EtoolkitConstants.ERRMSG_DATABASE_NOT_AVAILABLE, ex);
		} catch (DataAccessException ex) {
			logger.error(EtoolkitConstants.DATAACCESS_EXCEPTION + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_DATA_ACCESS,
					String.format(EtoolkitConstants.ERRMSG_DATA_ACCESS, ex.getMessage()), ex);
		} catch (Exception ex) {
			logger.error(ex.toString() + "::" + ex.getMessage());
			throw new EtoolKitException(EtoolkitConstants.ERR_UNCATEGORIZED, ex.getMessage(), ex);
		}
		
}
	
}
