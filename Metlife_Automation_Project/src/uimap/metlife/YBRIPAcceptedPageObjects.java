package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class YBRIPAcceptedPageObjects extends PageObjectLibrary{
		
	public static final By btnfinish = getObjectIdentifaction("YBRIP.accepted.submit.btn.css");
}
