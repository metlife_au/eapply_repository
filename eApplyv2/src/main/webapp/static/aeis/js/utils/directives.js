AeisApp.directive('loading', ['$http', function ($http) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {
        scope.isLoading = function () {
          return $http.pendingRequests.length > 0;
        };
        scope.$watch(scope.isLoading, function (value) {
          if (value) {
            element.removeClass('ng-hide');
          } else {
            element.addClass('ng-hide');
          }
        });
      }
    };
}]);
AeisApp.directive('phoneOnly', function(){
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function(scope, element, attrs, modelCtrl) {

            modelCtrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue ? inputValue.replace(/[^\d.-]/g,'') : null;

                if (transformedInput!=inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });

            $(element).on("keyup", function() {
            	var TempVal=$(this).val().replace(" ","");
             	var regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
            	if( !regex.test(TempVal)) {
            		// element.controller('ngModel').$setValidity('required', false);
            		// element.controller('ngModel').$touched = true;
            	}

            })
        }
    };
});

AeisApp.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});


AeisApp.directive('dateOnly', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9/]/gi, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

AeisApp.directive("limitTo", [function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            var limit = parseInt(attrs.limitTo);
            angular.element(elem).on("keypress", function(e) {
                if (this.value.length == limit) e.preventDefault();
            });
        }
    }
}]);

AeisApp.directive('alphaNumeric', function() {

	 return {
	        require: 'ngModel',
	        restrict: 'A',
	        link: function(scope, element, attrs, modelCtrl) {

	            modelCtrl.$parsers.push(function (inputValue) {
	                var transformedInput = inputValue ? inputValue.replace(/[^0-9a-zA-Z]/gi, '') : null;

	                if (transformedInput!=inputValue) {
	                    modelCtrl.$setViewValue(transformedInput);
	                    modelCtrl.$render();
	                }

	                return transformedInput;
	            });
	        }
	    };
  });

AeisApp.directive('alphaNumericWithSpace', function() {

	 return {
	        require: 'ngModel',
	        restrict: 'A',
	        link: function(scope, element, attrs, modelCtrl) {

	            modelCtrl.$parsers.push(function (inputValue) {
	                var transformedInput = inputValue ? inputValue.replace(/[^a-zA-Z0-9\s]/gi, '') : null;

	                if (transformedInput!=inputValue) {
	                    modelCtrl.$setViewValue(transformedInput);
	                    modelCtrl.$render();
	                }

	                return transformedInput;
	            });
	        }
	    };
 });

AeisApp.directive('helpText', function() {
	  return {
	    restrict: 'E',
	    scope: {
	      show: '=',
	      msg: '='
	    },
	    replace: true,
	    transclude: true,
	    link: function(scope, element, attrs) {
	      scope.dialogStyle = {};
	      if (attrs.width)
	        scope.dialogStyle.width = attrs.width;
	      if (attrs.height)
	        scope.dialogStyle.height = attrs.height;
	      scope.hideModal = function() {
	        scope.show = false;
	      };
	    },
	    template: "<div class='ng-modal' ng-show='show'><div class='ng-modal-overlay' ng-click='hideModal()'></div><div class='ng-modal-dialog' ng-style='dialogStyle'><div class='ng-modal-close' ng-click='hideModal()'>X</div><div class='ng-modal-dialog-content'><h4 class='modal-title aligncenter' id='myModalLabel'> Helpful hints</h4><div class='row  rowcustom'><div class='col-sm-12'><p class='aligncenter'><div id='tips_text'>{{msg}}</div></p></div></div><div class='modal-footer mt0 pt0'><div class='row'><div class='col-sm-4'></div><div class='col-sm-4 col-xs-12'><a ng-click='hideModal()'><button class='btn btn-primary w100p' type='button'>Close</button></a> </div><div class='col-sm-4'></div> </div></div></div></div></div>"
	  };
	});

AeisApp.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        restrict: 'A',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;


            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue)
            });


            ctrl.$parsers.unshift(function (viewValue) {
                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                if(plainNumber)
                  elem.val($filter(attrs.format)(plainNumber));
                else
                  elem.val(plainNumber);
                return plainNumber;
            });
        }
    };
}]);

AeisApp.directive('ngClickAccessible', function(){
	return {
		restrict:'A',
		link: function(scope,elm,attr,ctrl){
			elm.bind('keyup', function(e){
				var pressedKey = e.keyCode;
				if(pressedKey == 13 && attr.ngClick){
					var targetId = e.target.id;
					$('#'+targetId).trigger('click');
				}
			});
		}
	};
});

AeisApp.directive('flagPointerActions', function() {
    return {
        restrict: 'AE',
        link: function($scope,elem,attrs) {

            $scope.$on('disablepointer', function() {
                elem.css('display', 'block')
            });

            $scope.$on('enablepointer', function() {
                elem.css('display', 'none')
            })
        }
    }
});
