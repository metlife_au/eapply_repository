package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class YBRCoverDetailPageObjects extends PageObjectLibrary{
		
	public static final By txtareaget = getObjectIdentifaction("ybr.coverdetails.textarea.css");
	public static final By txtareainput = getObjectIdentifaction("ybr.coverdetails.textarea1.id");
	public static final By btnNext = getObjectIdentifaction("ybr.coverdetails.next.btn.css");

}
