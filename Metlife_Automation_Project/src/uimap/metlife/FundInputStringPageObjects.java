package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

public class FundInputStringPageObjects extends PageObjectLibrary {
	public static final By txtInputString = getObjectIdentifaction("sfps.inputstring.txt.id");
	public static final By drpdownInputIdetifier = getObjectIdentifaction("sfps.inputidentifier.dropdown.id");
	public static final By btnPlainText = getObjectIdentifaction("sfps.plaintext.btn.css");
	public static final By btnNewApplication = getObjectIdentifaction("sfps.newapplication.btn.css");
	public static final By btnNewApplicaion = getObjectIdentifaction("ingdir.payloadpage.newapplication.button.css");
	public static final By btnNFSFNewApplication = getObjectIdentifaction("payloadpage.newapplication.button.css");
	
}
