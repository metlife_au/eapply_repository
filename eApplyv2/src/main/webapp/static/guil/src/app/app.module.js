(function(angular) {
	'use strict';
	var guilApp = angular.module('guildApp', ['ui.router', 'ngDialog','guil.ui.constants',
			'Metlife.guil.uicomponents', 'Metlife.guil.uicomponentsheader',
			'Metlife.guil.directives','restAPI','guilAPI','appDataModel','guilFilters','ngFileUpload','sessionTimeOut','eApplyInterceptor','ngSanitize']);
})(angular);
