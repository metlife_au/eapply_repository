package au.com.metlife.eapply.underwriting.utils;

import java.util.Iterator;

import au.com.metlife.eapply.underwriting.response.model.Decision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuraProcessDecisionHelper {
	private AuraProcessDecisionHelper(){
		
	}
	private static final Logger log = LoggerFactory.getLogger(AuraProcessDecisionHelper.class);
	
	public static String getReasons(Decision decision){
		log.info("Get reasons start");
		 String [] reasons = null;
		 StringBuilder declineReasons = null;
		 reasons = decision.getReasons();
			declineReasons = new StringBuilder(); 
			if(null!=reasons && reasons.length>0){
				for(int resonItr=0; resonItr<reasons.length;resonItr++){
					if(reasons[resonItr]!=null){                       									
						declineReasons.append(reasons[resonItr]);
						declineReasons.append(", ");	                                      								
					}
				}
			}
			/*if(declineReasons!=null){
				log.info("Get reasons finish");
				return declineReasons.toString();
			}else{
				log.info("Get reasons finish");
				return null;
			}*/
			log.info("Get reasons finish");
			return declineReasons.toString();	
			
	}
	
	
	public static String getAuraDclReason(Decision decision){
		log.info("Get aura decline reason start");
		StringBuilder declineOrgReasons = null;
		/*if(decision.getOrgReason()!=null && decision.getOrgReason().size()>0){*/
		if(decision.getOrgReason()!=null && !(decision.getOrgReason().isEmpty())){
			declineOrgReasons = new StringBuilder(); 
			for (Iterator iter = decision.getOrgReason().iterator(); iter
			.hasNext();) {
				String element = (String) iter.next();
				if(null!=element){
					if("BMI_Term".equalsIgnoreCase(element) || "BMI_TPD".equalsIgnoreCase(element) || "BMI_IP".equalsIgnoreCase(element)){
						declineOrgReasons.append("Height/weight ratio");
					}else{
						declineOrgReasons.append(element);
					}	                                                        					
					declineOrgReasons.append("##");
				}
		
			}
			/*coversDTO.setCoverAuraDclReason(declineOrgReasons.toString());*/
		}
		if(declineOrgReasons!=null){
			log.info("Get aura decline reason finish");
			return declineOrgReasons.toString();
		}else{
			log.info("Get aura decline reason finish");
			return null;
		}
			
	}
	
	public static String getExclusions(Decision decision){
		log.info("Get exclusions start");
		String exclusionValue = null;
		/*if (null != decision.getListExclusion() && decision.getListExclusion().size() > 0) {*/
		if (null != decision.getListExclusion() && !(decision.getListExclusion().isEmpty())) {
			for (int itrExclusion = 0; itrExclusion < decision.getListExclusion().size(); itrExclusion++) {
				if(decision.getListExclusion().size()==1){
					exclusionValue = ""+decision.getListExclusion().get(itrExclusion).getText();
				}else if (itrExclusion == 0){
                     exclusionValue = ""+decision.getListExclusion().get(itrExclusion).getText()+"<br><br>"; 
                    
    			 }                        
                 else{
                     if(decision.getListExclusion().size() == itrExclusion){
                             exclusionValue = exclusionValue + "\n"+decision.getListExclusion().get(itrExclusion).getText(); 
                     }else{
                             exclusionValue = exclusionValue + "\n"+decision.getListExclusion().get(itrExclusion).getText()+"<br><br>";
                     }	    	                                                                    
                 }  
    			
			}
			
		}
		log.info("Get exclusions finish");
		return exclusionValue;
	}

}
