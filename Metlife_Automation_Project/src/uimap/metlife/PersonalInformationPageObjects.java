/**
 * 
 */
package uimap.metlife;

import org.openqa.selenium.By;

import supportlibraries.PageObjectLibrary;

/**
 * @author sampath
 *
 */
public class PersonalInformationPageObjects extends PageObjectLibrary {

	/**
	 * 
	 */
	public PersonalInformationPageObjects() {
		// TODO Auto-generated constructor stub
		
	}	
	public static final By listTitle = getObjectIdentifaction("pwc.personalinformation.title.dropdown.id");
	public static final By txtConfidentialEmailId = getObjectIdentifaction("pwc.personalinformation.confidential.email.text.id");
	public static final By txtConfirmEmail = getObjectIdentifaction("pwc.personalinformation.confirm.email.text.id");
	public static final By txtPreferredContact = getObjectIdentifaction("pwc.personalinformation.preferred.contact.text.id");
	public static final By lstPreferredContactTime = getObjectIdentifaction("pwc.personalinformation.preferred.contacttime.dropdown.id");
	public static final By btnSubmit = getObjectIdentifaction("pwc.personalinformation.submit.btn.css");

}
