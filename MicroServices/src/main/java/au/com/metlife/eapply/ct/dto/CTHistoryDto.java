package au.com.metlife.eapply.ct.dto;

import java.util.Date;

public class CTHistoryDto {
	
	  private String claimNum;	 
	  private String fundName;
	  private String status;
	  private String activityName;	 
	  private Date accessedDate;
	  
	    public String getClaimNum() {
			return claimNum;
		}
		public void setClaimNum(String claimNum) {
			this.claimNum = claimNum;
		}
		public String getFundName() {
			return fundName;
		}
		public void setFundName(String fundName) {
			this.fundName = fundName;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getActivityName() {
			return activityName;
		}
		public void setActivityName(String activityName) {
			this.activityName = activityName;
		}
		public Date getAccessedDate() {
			return accessedDate;
		}
		public void setAccessedDate(Date accessedDate) {
			this.accessedDate = accessedDate;
		}
}
