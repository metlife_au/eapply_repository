package au.com.metlife.eapply.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import au.com.metlife.eapply.repositories.model.CorporateFund;
import au.com.metlife.eapply.repositories.model.CorporateFundCat;

public interface CorporateFundCatRepo extends JpaRepository<CorporateFundCat, Integer>{

	List<CorporateFundCat> findByCorporateFundIdAndCategory(CorporateFund corporateFundId,Integer Category);

}
