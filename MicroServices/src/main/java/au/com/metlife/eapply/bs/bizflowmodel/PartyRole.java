package au.com.metlife.eapply.bs.bizflowmodel;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyRole")

@XmlRootElement
public class PartyRole {
	

	
	private ContactProfile ContactProfile;
	private List<CitizenshipList> CitizenshipList;

	public List<CitizenshipList> getCitizenshipList() {
		return CitizenshipList;
	}
	public void setCitizenshipList(List<CitizenshipList> citizenshipList) {
		CitizenshipList = citizenshipList;
	}
	public ContactProfile getContactProfile() {
		return ContactProfile;
	}
	public void setContactProfile(ContactProfile contactProfile) {
		ContactProfile = contactProfile;
	}
	

	
	
}
